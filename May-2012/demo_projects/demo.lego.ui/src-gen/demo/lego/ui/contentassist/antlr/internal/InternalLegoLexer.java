package demo.lego.ui.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalLegoLexer extends Lexer {
    public static final int RULE_ID=6;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_POS_1=5;
    public static final int RULE_ONE=4;
    public static final int EOF=-1;
    public static final int RULE_SL_COMMENT=10;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__19=19;
    public static final int RULE_STRING=8;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=7;
    public static final int RULE_WS=11;

    // delegates
    // delegators

    public InternalLegoLexer() {;} 
    public InternalLegoLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalLegoLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g"; }

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:11:7: ( 'DEBUT' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:11:9: 'DEBUT'
            {
            match("DEBUT"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:12:7: ( 'FIN' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:12:9: 'FIN'
            {
            match("FIN"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:13:7: ( 'ferme la pince' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:13:9: 'ferme la pince'
            {
            match("ferme la pince"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:14:7: ( 'ouvre la pince' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:14:9: 'ouvre la pince'
            {
            match("ouvre la pince"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:15:7: ( 'recule de' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:15:9: 'recule de'
            {
            match("recule de"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:16:7: ( 'cm' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:16:9: 'cm'
            {
            match("cm"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:17:7: ( 'avance de' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:17:9: 'avance de'
            {
            match("avance de"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:18:7: ( 'de' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:18:9: 'de'
            {
            match("de"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:19:7: ( 'degr\\u00E9' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:19:9: 'degr\\u00E9'
            {
            match("degr\u00E9"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:20:7: ( 'degr\\u00E9s' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:20:9: 'degr\\u00E9s'
            {
            match("degr\u00E9s"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:21:7: ( 'tourne \\u00E0 gauche' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:21:9: 'tourne \\u00E0 gauche'
            {
            match("tourne \u00E0 gauche"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:22:7: ( 'tourne \\u00E0 droite' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:22:9: 'tourne \\u00E0 droite'
            {
            match("tourne \u00E0 droite"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "RULE_ONE"
    public final void mRULE_ONE() throws RecognitionException {
        try {
            int _type = RULE_ONE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1352:10: ( '1' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1352:12: '1'
            {
            match('1'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ONE"

    // $ANTLR start "RULE_POS_1"
    public final void mRULE_POS_1() throws RecognitionException {
        try {
            int _type = RULE_POS_1;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1354:12: ( ( '2' .. '9' | '1' .. '9' ( '0' .. '9' )+ ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1354:14: ( '2' .. '9' | '1' .. '9' ( '0' .. '9' )+ )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1354:14: ( '2' .. '9' | '1' .. '9' ( '0' .. '9' )+ )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( ((LA2_0>='2' && LA2_0<='9')) ) {
                int LA2_1 = input.LA(2);

                if ( ((LA2_1>='0' && LA2_1<='9')) ) {
                    alt2=2;
                }
                else {
                    alt2=1;}
            }
            else if ( (LA2_0=='1') ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1354:15: '2' .. '9'
                    {
                    matchRange('2','9'); 

                    }
                    break;
                case 2 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1354:24: '1' .. '9' ( '0' .. '9' )+
                    {
                    matchRange('1','9'); 
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1354:33: ( '0' .. '9' )+
                    int cnt1=0;
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( ((LA1_0>='0' && LA1_0<='9')) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1354:34: '0' .. '9'
                    	    {
                    	    matchRange('0','9'); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt1 >= 1 ) break loop1;
                                EarlyExitException eee =
                                    new EarlyExitException(1, input);
                                throw eee;
                        }
                        cnt1++;
                    } while (true);


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_POS_1"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1356:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1356:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1356:11: ( '^' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0=='^') ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1356:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1356:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>='0' && LA4_0<='9')||(LA4_0>='A' && LA4_0<='Z')||LA4_0=='_'||(LA4_0>='a' && LA4_0<='z')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1358:10: ( ( '0' .. '9' )+ )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1358:12: ( '0' .. '9' )+
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1358:12: ( '0' .. '9' )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='0' && LA5_0<='9')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1358:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1360:13: ( ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1360:15: ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1360:15: ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0=='\"') ) {
                alt8=1;
            }
            else if ( (LA8_0=='\'') ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1360:16: '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1360:20: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop6:
                    do {
                        int alt6=3;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0=='\\') ) {
                            alt6=1;
                        }
                        else if ( ((LA6_0>='\u0000' && LA6_0<='!')||(LA6_0>='#' && LA6_0<='[')||(LA6_0>=']' && LA6_0<='\uFFFF')) ) {
                            alt6=2;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1360:21: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' )
                    	    {
                    	    match('\\'); 
                    	    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||(input.LA(1)>='t' && input.LA(1)<='u') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;
                    	case 2 :
                    	    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1360:66: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1360:86: '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1360:91: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop7:
                    do {
                        int alt7=3;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0=='\\') ) {
                            alt7=1;
                        }
                        else if ( ((LA7_0>='\u0000' && LA7_0<='&')||(LA7_0>='(' && LA7_0<='[')||(LA7_0>=']' && LA7_0<='\uFFFF')) ) {
                            alt7=2;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1360:92: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' )
                    	    {
                    	    match('\\'); 
                    	    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||(input.LA(1)>='t' && input.LA(1)<='u') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;
                    	case 2 :
                    	    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1360:137: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1362:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1362:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1362:24: ( options {greedy=false; } : . )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0=='*') ) {
                    int LA9_1 = input.LA(2);

                    if ( (LA9_1=='/') ) {
                        alt9=2;
                    }
                    else if ( ((LA9_1>='\u0000' && LA9_1<='.')||(LA9_1>='0' && LA9_1<='\uFFFF')) ) {
                        alt9=1;
                    }


                }
                else if ( ((LA9_0>='\u0000' && LA9_0<=')')||(LA9_0>='+' && LA9_0<='\uFFFF')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1362:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1364:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1364:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1364:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>='\u0000' && LA10_0<='\t')||(LA10_0>='\u000B' && LA10_0<='\f')||(LA10_0>='\u000E' && LA10_0<='\uFFFF')) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1364:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1364:40: ( ( '\\r' )? '\\n' )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0=='\n'||LA12_0=='\r') ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1364:41: ( '\\r' )? '\\n'
                    {
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1364:41: ( '\\r' )?
                    int alt11=2;
                    int LA11_0 = input.LA(1);

                    if ( (LA11_0=='\r') ) {
                        alt11=1;
                    }
                    switch (alt11) {
                        case 1 :
                            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1364:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1366:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1366:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1366:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt13=0;
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( ((LA13_0>='\t' && LA13_0<='\n')||LA13_0=='\r'||LA13_0==' ') ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt13 >= 1 ) break loop13;
                        EarlyExitException eee =
                            new EarlyExitException(13, input);
                        throw eee;
                }
                cnt13++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1368:16: ( . )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1368:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:8: ( T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | RULE_ONE | RULE_POS_1 | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt14=21;
        alt14 = dfa14.predict(input);
        switch (alt14) {
            case 1 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:10: T__13
                {
                mT__13(); 

                }
                break;
            case 2 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:16: T__14
                {
                mT__14(); 

                }
                break;
            case 3 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:22: T__15
                {
                mT__15(); 

                }
                break;
            case 4 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:28: T__16
                {
                mT__16(); 

                }
                break;
            case 5 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:34: T__17
                {
                mT__17(); 

                }
                break;
            case 6 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:40: T__18
                {
                mT__18(); 

                }
                break;
            case 7 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:46: T__19
                {
                mT__19(); 

                }
                break;
            case 8 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:52: T__20
                {
                mT__20(); 

                }
                break;
            case 9 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:58: T__21
                {
                mT__21(); 

                }
                break;
            case 10 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:64: T__22
                {
                mT__22(); 

                }
                break;
            case 11 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:70: T__23
                {
                mT__23(); 

                }
                break;
            case 12 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:76: T__24
                {
                mT__24(); 

                }
                break;
            case 13 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:82: RULE_ONE
                {
                mRULE_ONE(); 

                }
                break;
            case 14 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:91: RULE_POS_1
                {
                mRULE_POS_1(); 

                }
                break;
            case 15 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:102: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 16 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:110: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 17 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:119: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 18 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:131: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 19 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:147: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 20 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:163: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 21 :
                // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1:171: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA14 dfa14 = new DFA14(this);
    static final String DFA14_eotS =
        "\1\uffff\11\25\1\37\1\40\1\23\2\uffff\3\23\2\uffff\1\25\1\uffff"+
        "\4\25\1\53\1\25\1\56\1\25\1\40\7\uffff\1\25\1\61\3\25\1\uffff\2"+
        "\25\1\uffff\2\25\1\uffff\6\25\1\77\4\25\1\105\1\25\3\uffff\2\25"+
        "\2\uffff\1\25\7\uffff";
    static final String DFA14_eofS =
        "\116\uffff";
    static final String DFA14_minS =
        "\1\0\1\105\1\111\1\145\1\165\1\145\1\155\1\166\1\145\1\157\2\60"+
        "\1\101\2\uffff\2\0\1\52\2\uffff\1\102\1\uffff\1\116\1\162\1\166"+
        "\1\143\1\60\1\141\1\60\1\165\1\60\7\uffff\1\125\1\60\1\155\1\162"+
        "\1\165\1\uffff\1\156\1\162\1\uffff\1\162\1\124\1\uffff\2\145\1\154"+
        "\1\143\1\u00e9\1\156\1\60\2\40\2\145\1\163\1\145\3\uffff\2\40\2"+
        "\uffff\1\40\2\uffff\1\u00e0\1\40\1\144\2\uffff";
    static final String DFA14_maxS =
        "\1\uffff\1\105\1\111\1\145\1\165\1\145\1\155\1\166\1\145\1\157\2"+
        "\71\1\172\2\uffff\2\uffff\1\57\2\uffff\1\102\1\uffff\1\116\1\162"+
        "\1\166\1\143\1\172\1\141\1\172\1\165\1\71\7\uffff\1\125\1\172\1"+
        "\155\1\162\1\165\1\uffff\1\156\1\162\1\uffff\1\162\1\124\1\uffff"+
        "\2\145\1\154\1\143\1\u00e9\1\156\1\172\2\40\2\145\1\163\1\145\3"+
        "\uffff\2\40\2\uffff\1\40\2\uffff\1\u00e0\1\40\1\147\2\uffff";
    static final String DFA14_acceptS =
        "\15\uffff\1\17\1\20\3\uffff\1\24\1\25\1\uffff\1\17\11\uffff\1\15"+
        "\1\16\1\20\1\21\1\22\1\23\1\24\5\uffff\1\6\2\uffff\1\10\2\uffff"+
        "\1\2\15\uffff\1\1\1\3\1\4\2\uffff\1\12\1\11\1\uffff\1\5\1\7\3\uffff"+
        "\1\13\1\14";
    static final String DFA14_specialS =
        "\1\2\16\uffff\1\1\1\0\75\uffff}>";
    static final String[] DFA14_transitionS = {
            "\11\23\2\22\2\23\1\22\22\23\1\22\1\23\1\17\4\23\1\20\7\23\1"+
            "\21\1\16\1\12\10\13\7\23\3\15\1\1\1\15\1\2\24\15\3\23\1\14\1"+
            "\15\1\23\1\7\1\15\1\6\1\10\1\15\1\3\10\15\1\4\2\15\1\5\1\15"+
            "\1\11\6\15\uff85\23",
            "\1\24",
            "\1\26",
            "\1\27",
            "\1\30",
            "\1\31",
            "\1\32",
            "\1\33",
            "\1\34",
            "\1\35",
            "\12\36",
            "\12\36",
            "\32\25\4\uffff\1\25\1\uffff\32\25",
            "",
            "",
            "\0\42",
            "\0\42",
            "\1\43\4\uffff\1\44",
            "",
            "",
            "\1\46",
            "",
            "\1\47",
            "\1\50",
            "\1\51",
            "\1\52",
            "\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff\32\25",
            "\1\54",
            "\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff\6\25\1\55\23\25",
            "\1\57",
            "\12\36",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\60",
            "\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff\32\25",
            "\1\62",
            "\1\63",
            "\1\64",
            "",
            "\1\65",
            "\1\66",
            "",
            "\1\67",
            "\1\70",
            "",
            "\1\71",
            "\1\72",
            "\1\73",
            "\1\74",
            "\1\75",
            "\1\76",
            "\12\25\7\uffff\32\25\4\uffff\1\25\1\uffff\32\25",
            "\1\100",
            "\1\101",
            "\1\102",
            "\1\103",
            "\1\104",
            "\1\106",
            "",
            "",
            "",
            "\1\107",
            "\1\110",
            "",
            "",
            "\1\111",
            "",
            "",
            "\1\112",
            "\1\113",
            "\1\115\2\uffff\1\114",
            "",
            ""
    };

    static final short[] DFA14_eot = DFA.unpackEncodedString(DFA14_eotS);
    static final short[] DFA14_eof = DFA.unpackEncodedString(DFA14_eofS);
    static final char[] DFA14_min = DFA.unpackEncodedStringToUnsignedChars(DFA14_minS);
    static final char[] DFA14_max = DFA.unpackEncodedStringToUnsignedChars(DFA14_maxS);
    static final short[] DFA14_accept = DFA.unpackEncodedString(DFA14_acceptS);
    static final short[] DFA14_special = DFA.unpackEncodedString(DFA14_specialS);
    static final short[][] DFA14_transition;

    static {
        int numStates = DFA14_transitionS.length;
        DFA14_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA14_transition[i] = DFA.unpackEncodedString(DFA14_transitionS[i]);
        }
    }

    class DFA14 extends DFA {

        public DFA14(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 14;
            this.eot = DFA14_eot;
            this.eof = DFA14_eof;
            this.min = DFA14_min;
            this.max = DFA14_max;
            this.accept = DFA14_accept;
            this.special = DFA14_special;
            this.transition = DFA14_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | RULE_ONE | RULE_POS_1 | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA14_16 = input.LA(1);

                        s = -1;
                        if ( ((LA14_16>='\u0000' && LA14_16<='\uFFFF')) ) {s = 34;}

                        else s = 19;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA14_15 = input.LA(1);

                        s = -1;
                        if ( ((LA14_15>='\u0000' && LA14_15<='\uFFFF')) ) {s = 34;}

                        else s = 19;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA14_0 = input.LA(1);

                        s = -1;
                        if ( (LA14_0=='D') ) {s = 1;}

                        else if ( (LA14_0=='F') ) {s = 2;}

                        else if ( (LA14_0=='f') ) {s = 3;}

                        else if ( (LA14_0=='o') ) {s = 4;}

                        else if ( (LA14_0=='r') ) {s = 5;}

                        else if ( (LA14_0=='c') ) {s = 6;}

                        else if ( (LA14_0=='a') ) {s = 7;}

                        else if ( (LA14_0=='d') ) {s = 8;}

                        else if ( (LA14_0=='t') ) {s = 9;}

                        else if ( (LA14_0=='1') ) {s = 10;}

                        else if ( ((LA14_0>='2' && LA14_0<='9')) ) {s = 11;}

                        else if ( (LA14_0=='^') ) {s = 12;}

                        else if ( ((LA14_0>='A' && LA14_0<='C')||LA14_0=='E'||(LA14_0>='G' && LA14_0<='Z')||LA14_0=='_'||LA14_0=='b'||LA14_0=='e'||(LA14_0>='g' && LA14_0<='n')||(LA14_0>='p' && LA14_0<='q')||LA14_0=='s'||(LA14_0>='u' && LA14_0<='z')) ) {s = 13;}

                        else if ( (LA14_0=='0') ) {s = 14;}

                        else if ( (LA14_0=='\"') ) {s = 15;}

                        else if ( (LA14_0=='\'') ) {s = 16;}

                        else if ( (LA14_0=='/') ) {s = 17;}

                        else if ( ((LA14_0>='\t' && LA14_0<='\n')||LA14_0=='\r'||LA14_0==' ') ) {s = 18;}

                        else if ( ((LA14_0>='\u0000' && LA14_0<='\b')||(LA14_0>='\u000B' && LA14_0<='\f')||(LA14_0>='\u000E' && LA14_0<='\u001F')||LA14_0=='!'||(LA14_0>='#' && LA14_0<='&')||(LA14_0>='(' && LA14_0<='.')||(LA14_0>=':' && LA14_0<='@')||(LA14_0>='[' && LA14_0<=']')||LA14_0=='`'||(LA14_0>='{' && LA14_0<='\uFFFF')) ) {s = 19;}

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 14, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}