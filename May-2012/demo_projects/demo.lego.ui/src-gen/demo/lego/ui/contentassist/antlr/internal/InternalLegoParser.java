package demo.lego.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import demo.lego.services.LegoGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalLegoParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ONE", "RULE_POS_1", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'DEBUT'", "'FIN'", "'ferme la pince'", "'ouvre la pince'", "'recule de'", "'cm'", "'avance de'", "'de'", "'degr\\u00E9'", "'degr\\u00E9s'", "'tourne \\u00E0 gauche'", "'tourne \\u00E0 droite'"
    };
    public static final int RULE_ID=6;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_POS_1=5;
    public static final int RULE_ONE=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__19=19;
    public static final int RULE_STRING=8;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=7;
    public static final int RULE_WS=11;

    // delegates
    // delegators


        public InternalLegoParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalLegoParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalLegoParser.tokenNames; }
    public String getGrammarFileName() { return "../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g"; }


     
     	private LegoGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(LegoGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleInstructionsSet"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:60:1: entryRuleInstructionsSet : ruleInstructionsSet EOF ;
    public final void entryRuleInstructionsSet() throws RecognitionException {
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:61:1: ( ruleInstructionsSet EOF )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:62:1: ruleInstructionsSet EOF
            {
             before(grammarAccess.getInstructionsSetRule()); 
            pushFollow(FOLLOW_ruleInstructionsSet_in_entryRuleInstructionsSet61);
            ruleInstructionsSet();

            state._fsp--;

             after(grammarAccess.getInstructionsSetRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstructionsSet68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInstructionsSet"


    // $ANTLR start "ruleInstructionsSet"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:69:1: ruleInstructionsSet : ( ( rule__InstructionsSet__Group__0 ) ) ;
    public final void ruleInstructionsSet() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:73:2: ( ( ( rule__InstructionsSet__Group__0 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:74:1: ( ( rule__InstructionsSet__Group__0 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:74:1: ( ( rule__InstructionsSet__Group__0 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:75:1: ( rule__InstructionsSet__Group__0 )
            {
             before(grammarAccess.getInstructionsSetAccess().getGroup()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:76:1: ( rule__InstructionsSet__Group__0 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:76:2: rule__InstructionsSet__Group__0
            {
            pushFollow(FOLLOW_rule__InstructionsSet__Group__0_in_ruleInstructionsSet94);
            rule__InstructionsSet__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInstructionsSetAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInstructionsSet"


    // $ANTLR start "entryRuleInstruction"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:88:1: entryRuleInstruction : ruleInstruction EOF ;
    public final void entryRuleInstruction() throws RecognitionException {
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:89:1: ( ruleInstruction EOF )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:90:1: ruleInstruction EOF
            {
             before(grammarAccess.getInstructionRule()); 
            pushFollow(FOLLOW_ruleInstruction_in_entryRuleInstruction121);
            ruleInstruction();

            state._fsp--;

             after(grammarAccess.getInstructionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstruction128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInstruction"


    // $ANTLR start "ruleInstruction"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:97:1: ruleInstruction : ( ( rule__Instruction__Alternatives ) ) ;
    public final void ruleInstruction() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:101:2: ( ( ( rule__Instruction__Alternatives ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:102:1: ( ( rule__Instruction__Alternatives ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:102:1: ( ( rule__Instruction__Alternatives ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:103:1: ( rule__Instruction__Alternatives )
            {
             before(grammarAccess.getInstructionAccess().getAlternatives()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:104:1: ( rule__Instruction__Alternatives )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:104:2: rule__Instruction__Alternatives
            {
            pushFollow(FOLLOW_rule__Instruction__Alternatives_in_ruleInstruction154);
            rule__Instruction__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getInstructionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInstruction"


    // $ANTLR start "entryRuleClose"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:116:1: entryRuleClose : ruleClose EOF ;
    public final void entryRuleClose() throws RecognitionException {
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:117:1: ( ruleClose EOF )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:118:1: ruleClose EOF
            {
             before(grammarAccess.getCloseRule()); 
            pushFollow(FOLLOW_ruleClose_in_entryRuleClose181);
            ruleClose();

            state._fsp--;

             after(grammarAccess.getCloseRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleClose188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClose"


    // $ANTLR start "ruleClose"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:125:1: ruleClose : ( ( rule__Close__Group__0 ) ) ;
    public final void ruleClose() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:129:2: ( ( ( rule__Close__Group__0 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:130:1: ( ( rule__Close__Group__0 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:130:1: ( ( rule__Close__Group__0 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:131:1: ( rule__Close__Group__0 )
            {
             before(grammarAccess.getCloseAccess().getGroup()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:132:1: ( rule__Close__Group__0 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:132:2: rule__Close__Group__0
            {
            pushFollow(FOLLOW_rule__Close__Group__0_in_ruleClose214);
            rule__Close__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCloseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClose"


    // $ANTLR start "entryRuleOpen"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:144:1: entryRuleOpen : ruleOpen EOF ;
    public final void entryRuleOpen() throws RecognitionException {
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:145:1: ( ruleOpen EOF )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:146:1: ruleOpen EOF
            {
             before(grammarAccess.getOpenRule()); 
            pushFollow(FOLLOW_ruleOpen_in_entryRuleOpen241);
            ruleOpen();

            state._fsp--;

             after(grammarAccess.getOpenRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOpen248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOpen"


    // $ANTLR start "ruleOpen"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:153:1: ruleOpen : ( ( rule__Open__Group__0 ) ) ;
    public final void ruleOpen() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:157:2: ( ( ( rule__Open__Group__0 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:158:1: ( ( rule__Open__Group__0 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:158:1: ( ( rule__Open__Group__0 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:159:1: ( rule__Open__Group__0 )
            {
             before(grammarAccess.getOpenAccess().getGroup()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:160:1: ( rule__Open__Group__0 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:160:2: rule__Open__Group__0
            {
            pushFollow(FOLLOW_rule__Open__Group__0_in_ruleOpen274);
            rule__Open__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOpenAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOpen"


    // $ANTLR start "entryRuleBackward"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:172:1: entryRuleBackward : ruleBackward EOF ;
    public final void entryRuleBackward() throws RecognitionException {
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:173:1: ( ruleBackward EOF )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:174:1: ruleBackward EOF
            {
             before(grammarAccess.getBackwardRule()); 
            pushFollow(FOLLOW_ruleBackward_in_entryRuleBackward301);
            ruleBackward();

            state._fsp--;

             after(grammarAccess.getBackwardRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBackward308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBackward"


    // $ANTLR start "ruleBackward"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:181:1: ruleBackward : ( ( rule__Backward__Group__0 ) ) ;
    public final void ruleBackward() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:185:2: ( ( ( rule__Backward__Group__0 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:186:1: ( ( rule__Backward__Group__0 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:186:1: ( ( rule__Backward__Group__0 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:187:1: ( rule__Backward__Group__0 )
            {
             before(grammarAccess.getBackwardAccess().getGroup()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:188:1: ( rule__Backward__Group__0 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:188:2: rule__Backward__Group__0
            {
            pushFollow(FOLLOW_rule__Backward__Group__0_in_ruleBackward334);
            rule__Backward__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBackwardAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBackward"


    // $ANTLR start "entryRuleForward"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:200:1: entryRuleForward : ruleForward EOF ;
    public final void entryRuleForward() throws RecognitionException {
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:201:1: ( ruleForward EOF )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:202:1: ruleForward EOF
            {
             before(grammarAccess.getForwardRule()); 
            pushFollow(FOLLOW_ruleForward_in_entryRuleForward361);
            ruleForward();

            state._fsp--;

             after(grammarAccess.getForwardRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleForward368); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleForward"


    // $ANTLR start "ruleForward"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:209:1: ruleForward : ( ( rule__Forward__Group__0 ) ) ;
    public final void ruleForward() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:213:2: ( ( ( rule__Forward__Group__0 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:214:1: ( ( rule__Forward__Group__0 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:214:1: ( ( rule__Forward__Group__0 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:215:1: ( rule__Forward__Group__0 )
            {
             before(grammarAccess.getForwardAccess().getGroup()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:216:1: ( rule__Forward__Group__0 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:216:2: rule__Forward__Group__0
            {
            pushFollow(FOLLOW_rule__Forward__Group__0_in_ruleForward394);
            rule__Forward__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getForwardAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleForward"


    // $ANTLR start "entryRuleTurn"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:228:1: entryRuleTurn : ruleTurn EOF ;
    public final void entryRuleTurn() throws RecognitionException {
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:229:1: ( ruleTurn EOF )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:230:1: ruleTurn EOF
            {
             before(grammarAccess.getTurnRule()); 
            pushFollow(FOLLOW_ruleTurn_in_entryRuleTurn421);
            ruleTurn();

            state._fsp--;

             after(grammarAccess.getTurnRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTurn428); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTurn"


    // $ANTLR start "ruleTurn"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:237:1: ruleTurn : ( ( rule__Turn__Group__0 ) ) ;
    public final void ruleTurn() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:241:2: ( ( ( rule__Turn__Group__0 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:242:1: ( ( rule__Turn__Group__0 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:242:1: ( ( rule__Turn__Group__0 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:243:1: ( rule__Turn__Group__0 )
            {
             before(grammarAccess.getTurnAccess().getGroup()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:244:1: ( rule__Turn__Group__0 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:244:2: rule__Turn__Group__0
            {
            pushFollow(FOLLOW_rule__Turn__Group__0_in_ruleTurn454);
            rule__Turn__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTurnAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTurn"


    // $ANTLR start "entryRuleTurnCCW"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:256:1: entryRuleTurnCCW : ruleTurnCCW EOF ;
    public final void entryRuleTurnCCW() throws RecognitionException {
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:257:1: ( ruleTurnCCW EOF )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:258:1: ruleTurnCCW EOF
            {
             before(grammarAccess.getTurnCCWRule()); 
            pushFollow(FOLLOW_ruleTurnCCW_in_entryRuleTurnCCW481);
            ruleTurnCCW();

            state._fsp--;

             after(grammarAccess.getTurnCCWRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTurnCCW488); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTurnCCW"


    // $ANTLR start "ruleTurnCCW"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:265:1: ruleTurnCCW : ( ( rule__TurnCCW__Group__0 ) ) ;
    public final void ruleTurnCCW() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:269:2: ( ( ( rule__TurnCCW__Group__0 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:270:1: ( ( rule__TurnCCW__Group__0 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:270:1: ( ( rule__TurnCCW__Group__0 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:271:1: ( rule__TurnCCW__Group__0 )
            {
             before(grammarAccess.getTurnCCWAccess().getGroup()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:272:1: ( rule__TurnCCW__Group__0 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:272:2: rule__TurnCCW__Group__0
            {
            pushFollow(FOLLOW_rule__TurnCCW__Group__0_in_ruleTurnCCW514);
            rule__TurnCCW__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTurnCCWAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTurnCCW"


    // $ANTLR start "entryRuleTurnCW"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:284:1: entryRuleTurnCW : ruleTurnCW EOF ;
    public final void entryRuleTurnCW() throws RecognitionException {
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:285:1: ( ruleTurnCW EOF )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:286:1: ruleTurnCW EOF
            {
             before(grammarAccess.getTurnCWRule()); 
            pushFollow(FOLLOW_ruleTurnCW_in_entryRuleTurnCW541);
            ruleTurnCW();

            state._fsp--;

             after(grammarAccess.getTurnCWRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTurnCW548); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTurnCW"


    // $ANTLR start "ruleTurnCW"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:293:1: ruleTurnCW : ( ( rule__TurnCW__Group__0 ) ) ;
    public final void ruleTurnCW() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:297:2: ( ( ( rule__TurnCW__Group__0 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:298:1: ( ( rule__TurnCW__Group__0 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:298:1: ( ( rule__TurnCW__Group__0 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:299:1: ( rule__TurnCW__Group__0 )
            {
             before(grammarAccess.getTurnCWAccess().getGroup()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:300:1: ( rule__TurnCW__Group__0 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:300:2: rule__TurnCW__Group__0
            {
            pushFollow(FOLLOW_rule__TurnCW__Group__0_in_ruleTurnCW574);
            rule__TurnCW__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTurnCWAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTurnCW"


    // $ANTLR start "rule__Instruction__Alternatives"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:312:1: rule__Instruction__Alternatives : ( ( ruleBackward ) | ( ruleForward ) | ( ruleTurn ) | ( ruleOpen ) | ( ruleClose ) );
    public final void rule__Instruction__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:316:1: ( ( ruleBackward ) | ( ruleForward ) | ( ruleTurn ) | ( ruleOpen ) | ( ruleClose ) )
            int alt1=5;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt1=1;
                }
                break;
            case 19:
                {
                alt1=2;
                }
                break;
            case 23:
            case 24:
                {
                alt1=3;
                }
                break;
            case 16:
                {
                alt1=4;
                }
                break;
            case 15:
                {
                alt1=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:317:1: ( ruleBackward )
                    {
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:317:1: ( ruleBackward )
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:318:1: ruleBackward
                    {
                     before(grammarAccess.getInstructionAccess().getBackwardParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleBackward_in_rule__Instruction__Alternatives610);
                    ruleBackward();

                    state._fsp--;

                     after(grammarAccess.getInstructionAccess().getBackwardParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:323:6: ( ruleForward )
                    {
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:323:6: ( ruleForward )
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:324:1: ruleForward
                    {
                     before(grammarAccess.getInstructionAccess().getForwardParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleForward_in_rule__Instruction__Alternatives627);
                    ruleForward();

                    state._fsp--;

                     after(grammarAccess.getInstructionAccess().getForwardParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:329:6: ( ruleTurn )
                    {
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:329:6: ( ruleTurn )
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:330:1: ruleTurn
                    {
                     before(grammarAccess.getInstructionAccess().getTurnParserRuleCall_2()); 
                    pushFollow(FOLLOW_ruleTurn_in_rule__Instruction__Alternatives644);
                    ruleTurn();

                    state._fsp--;

                     after(grammarAccess.getInstructionAccess().getTurnParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:335:6: ( ruleOpen )
                    {
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:335:6: ( ruleOpen )
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:336:1: ruleOpen
                    {
                     before(grammarAccess.getInstructionAccess().getOpenParserRuleCall_3()); 
                    pushFollow(FOLLOW_ruleOpen_in_rule__Instruction__Alternatives661);
                    ruleOpen();

                    state._fsp--;

                     after(grammarAccess.getInstructionAccess().getOpenParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:341:6: ( ruleClose )
                    {
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:341:6: ( ruleClose )
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:342:1: ruleClose
                    {
                     before(grammarAccess.getInstructionAccess().getCloseParserRuleCall_4()); 
                    pushFollow(FOLLOW_ruleClose_in_rule__Instruction__Alternatives678);
                    ruleClose();

                    state._fsp--;

                     after(grammarAccess.getInstructionAccess().getCloseParserRuleCall_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instruction__Alternatives"


    // $ANTLR start "rule__Backward__DistanceAlternatives_1_0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:352:1: rule__Backward__DistanceAlternatives_1_0 : ( ( RULE_ONE ) | ( RULE_POS_1 ) );
    public final void rule__Backward__DistanceAlternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:356:1: ( ( RULE_ONE ) | ( RULE_POS_1 ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_ONE) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_POS_1) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:357:1: ( RULE_ONE )
                    {
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:357:1: ( RULE_ONE )
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:358:1: RULE_ONE
                    {
                     before(grammarAccess.getBackwardAccess().getDistanceONETerminalRuleCall_1_0_0()); 
                    match(input,RULE_ONE,FOLLOW_RULE_ONE_in_rule__Backward__DistanceAlternatives_1_0710); 
                     after(grammarAccess.getBackwardAccess().getDistanceONETerminalRuleCall_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:363:6: ( RULE_POS_1 )
                    {
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:363:6: ( RULE_POS_1 )
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:364:1: RULE_POS_1
                    {
                     before(grammarAccess.getBackwardAccess().getDistancePOS_1TerminalRuleCall_1_0_1()); 
                    match(input,RULE_POS_1,FOLLOW_RULE_POS_1_in_rule__Backward__DistanceAlternatives_1_0727); 
                     after(grammarAccess.getBackwardAccess().getDistancePOS_1TerminalRuleCall_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Backward__DistanceAlternatives_1_0"


    // $ANTLR start "rule__Forward__DistanceAlternatives_1_0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:374:1: rule__Forward__DistanceAlternatives_1_0 : ( ( RULE_ONE ) | ( RULE_POS_1 ) );
    public final void rule__Forward__DistanceAlternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:378:1: ( ( RULE_ONE ) | ( RULE_POS_1 ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ONE) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_POS_1) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:379:1: ( RULE_ONE )
                    {
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:379:1: ( RULE_ONE )
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:380:1: RULE_ONE
                    {
                     before(grammarAccess.getForwardAccess().getDistanceONETerminalRuleCall_1_0_0()); 
                    match(input,RULE_ONE,FOLLOW_RULE_ONE_in_rule__Forward__DistanceAlternatives_1_0759); 
                     after(grammarAccess.getForwardAccess().getDistanceONETerminalRuleCall_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:385:6: ( RULE_POS_1 )
                    {
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:385:6: ( RULE_POS_1 )
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:386:1: RULE_POS_1
                    {
                     before(grammarAccess.getForwardAccess().getDistancePOS_1TerminalRuleCall_1_0_1()); 
                    match(input,RULE_POS_1,FOLLOW_RULE_POS_1_in_rule__Forward__DistanceAlternatives_1_0776); 
                     after(grammarAccess.getForwardAccess().getDistancePOS_1TerminalRuleCall_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forward__DistanceAlternatives_1_0"


    // $ANTLR start "rule__Turn__Alternatives_0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:396:1: rule__Turn__Alternatives_0 : ( ( ruleTurnCW ) | ( ruleTurnCCW ) );
    public final void rule__Turn__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:400:1: ( ( ruleTurnCW ) | ( ruleTurnCCW ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==24) ) {
                alt4=1;
            }
            else if ( (LA4_0==23) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:401:1: ( ruleTurnCW )
                    {
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:401:1: ( ruleTurnCW )
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:402:1: ruleTurnCW
                    {
                     before(grammarAccess.getTurnAccess().getTurnCWParserRuleCall_0_0()); 
                    pushFollow(FOLLOW_ruleTurnCW_in_rule__Turn__Alternatives_0808);
                    ruleTurnCW();

                    state._fsp--;

                     after(grammarAccess.getTurnAccess().getTurnCWParserRuleCall_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:407:6: ( ruleTurnCCW )
                    {
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:407:6: ( ruleTurnCCW )
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:408:1: ruleTurnCCW
                    {
                     before(grammarAccess.getTurnAccess().getTurnCCWParserRuleCall_0_1()); 
                    pushFollow(FOLLOW_ruleTurnCCW_in_rule__Turn__Alternatives_0825);
                    ruleTurnCCW();

                    state._fsp--;

                     after(grammarAccess.getTurnAccess().getTurnCCWParserRuleCall_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Alternatives_0"


    // $ANTLR start "rule__Turn__Alternatives_1_1"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:418:1: rule__Turn__Alternatives_1_1 : ( ( ( rule__Turn__Group_1_1_0__0 ) ) | ( ( rule__Turn__Group_1_1_1__0 ) ) );
    public final void rule__Turn__Alternatives_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:422:1: ( ( ( rule__Turn__Group_1_1_0__0 ) ) | ( ( rule__Turn__Group_1_1_1__0 ) ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ONE) ) {
                alt5=1;
            }
            else if ( (LA5_0==RULE_POS_1) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:423:1: ( ( rule__Turn__Group_1_1_0__0 ) )
                    {
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:423:1: ( ( rule__Turn__Group_1_1_0__0 ) )
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:424:1: ( rule__Turn__Group_1_1_0__0 )
                    {
                     before(grammarAccess.getTurnAccess().getGroup_1_1_0()); 
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:425:1: ( rule__Turn__Group_1_1_0__0 )
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:425:2: rule__Turn__Group_1_1_0__0
                    {
                    pushFollow(FOLLOW_rule__Turn__Group_1_1_0__0_in_rule__Turn__Alternatives_1_1857);
                    rule__Turn__Group_1_1_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getTurnAccess().getGroup_1_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:429:6: ( ( rule__Turn__Group_1_1_1__0 ) )
                    {
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:429:6: ( ( rule__Turn__Group_1_1_1__0 ) )
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:430:1: ( rule__Turn__Group_1_1_1__0 )
                    {
                     before(grammarAccess.getTurnAccess().getGroup_1_1_1()); 
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:431:1: ( rule__Turn__Group_1_1_1__0 )
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:431:2: rule__Turn__Group_1_1_1__0
                    {
                    pushFollow(FOLLOW_rule__Turn__Group_1_1_1__0_in_rule__Turn__Alternatives_1_1875);
                    rule__Turn__Group_1_1_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getTurnAccess().getGroup_1_1_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Alternatives_1_1"


    // $ANTLR start "rule__InstructionsSet__Group__0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:442:1: rule__InstructionsSet__Group__0 : rule__InstructionsSet__Group__0__Impl rule__InstructionsSet__Group__1 ;
    public final void rule__InstructionsSet__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:446:1: ( rule__InstructionsSet__Group__0__Impl rule__InstructionsSet__Group__1 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:447:2: rule__InstructionsSet__Group__0__Impl rule__InstructionsSet__Group__1
            {
            pushFollow(FOLLOW_rule__InstructionsSet__Group__0__Impl_in_rule__InstructionsSet__Group__0906);
            rule__InstructionsSet__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__InstructionsSet__Group__1_in_rule__InstructionsSet__Group__0909);
            rule__InstructionsSet__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstructionsSet__Group__0"


    // $ANTLR start "rule__InstructionsSet__Group__0__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:454:1: rule__InstructionsSet__Group__0__Impl : ( () ) ;
    public final void rule__InstructionsSet__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:458:1: ( ( () ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:459:1: ( () )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:459:1: ( () )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:460:1: ()
            {
             before(grammarAccess.getInstructionsSetAccess().getInstructionsSetAction_0()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:461:1: ()
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:463:1: 
            {
            }

             after(grammarAccess.getInstructionsSetAccess().getInstructionsSetAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstructionsSet__Group__0__Impl"


    // $ANTLR start "rule__InstructionsSet__Group__1"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:473:1: rule__InstructionsSet__Group__1 : rule__InstructionsSet__Group__1__Impl rule__InstructionsSet__Group__2 ;
    public final void rule__InstructionsSet__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:477:1: ( rule__InstructionsSet__Group__1__Impl rule__InstructionsSet__Group__2 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:478:2: rule__InstructionsSet__Group__1__Impl rule__InstructionsSet__Group__2
            {
            pushFollow(FOLLOW_rule__InstructionsSet__Group__1__Impl_in_rule__InstructionsSet__Group__1967);
            rule__InstructionsSet__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__InstructionsSet__Group__2_in_rule__InstructionsSet__Group__1970);
            rule__InstructionsSet__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstructionsSet__Group__1"


    // $ANTLR start "rule__InstructionsSet__Group__1__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:485:1: rule__InstructionsSet__Group__1__Impl : ( 'DEBUT' ) ;
    public final void rule__InstructionsSet__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:489:1: ( ( 'DEBUT' ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:490:1: ( 'DEBUT' )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:490:1: ( 'DEBUT' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:491:1: 'DEBUT'
            {
             before(grammarAccess.getInstructionsSetAccess().getDEBUTKeyword_1()); 
            match(input,13,FOLLOW_13_in_rule__InstructionsSet__Group__1__Impl998); 
             after(grammarAccess.getInstructionsSetAccess().getDEBUTKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstructionsSet__Group__1__Impl"


    // $ANTLR start "rule__InstructionsSet__Group__2"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:504:1: rule__InstructionsSet__Group__2 : rule__InstructionsSet__Group__2__Impl rule__InstructionsSet__Group__3 ;
    public final void rule__InstructionsSet__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:508:1: ( rule__InstructionsSet__Group__2__Impl rule__InstructionsSet__Group__3 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:509:2: rule__InstructionsSet__Group__2__Impl rule__InstructionsSet__Group__3
            {
            pushFollow(FOLLOW_rule__InstructionsSet__Group__2__Impl_in_rule__InstructionsSet__Group__21029);
            rule__InstructionsSet__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__InstructionsSet__Group__3_in_rule__InstructionsSet__Group__21032);
            rule__InstructionsSet__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstructionsSet__Group__2"


    // $ANTLR start "rule__InstructionsSet__Group__2__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:516:1: rule__InstructionsSet__Group__2__Impl : ( ( rule__InstructionsSet__InstructionsAssignment_2 )* ) ;
    public final void rule__InstructionsSet__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:520:1: ( ( ( rule__InstructionsSet__InstructionsAssignment_2 )* ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:521:1: ( ( rule__InstructionsSet__InstructionsAssignment_2 )* )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:521:1: ( ( rule__InstructionsSet__InstructionsAssignment_2 )* )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:522:1: ( rule__InstructionsSet__InstructionsAssignment_2 )*
            {
             before(grammarAccess.getInstructionsSetAccess().getInstructionsAssignment_2()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:523:1: ( rule__InstructionsSet__InstructionsAssignment_2 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>=15 && LA6_0<=17)||LA6_0==19||(LA6_0>=23 && LA6_0<=24)) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:523:2: rule__InstructionsSet__InstructionsAssignment_2
            	    {
            	    pushFollow(FOLLOW_rule__InstructionsSet__InstructionsAssignment_2_in_rule__InstructionsSet__Group__2__Impl1059);
            	    rule__InstructionsSet__InstructionsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getInstructionsSetAccess().getInstructionsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstructionsSet__Group__2__Impl"


    // $ANTLR start "rule__InstructionsSet__Group__3"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:533:1: rule__InstructionsSet__Group__3 : rule__InstructionsSet__Group__3__Impl ;
    public final void rule__InstructionsSet__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:537:1: ( rule__InstructionsSet__Group__3__Impl )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:538:2: rule__InstructionsSet__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__InstructionsSet__Group__3__Impl_in_rule__InstructionsSet__Group__31090);
            rule__InstructionsSet__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstructionsSet__Group__3"


    // $ANTLR start "rule__InstructionsSet__Group__3__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:544:1: rule__InstructionsSet__Group__3__Impl : ( 'FIN' ) ;
    public final void rule__InstructionsSet__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:548:1: ( ( 'FIN' ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:549:1: ( 'FIN' )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:549:1: ( 'FIN' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:550:1: 'FIN'
            {
             before(grammarAccess.getInstructionsSetAccess().getFINKeyword_3()); 
            match(input,14,FOLLOW_14_in_rule__InstructionsSet__Group__3__Impl1118); 
             after(grammarAccess.getInstructionsSetAccess().getFINKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstructionsSet__Group__3__Impl"


    // $ANTLR start "rule__Close__Group__0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:571:1: rule__Close__Group__0 : rule__Close__Group__0__Impl rule__Close__Group__1 ;
    public final void rule__Close__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:575:1: ( rule__Close__Group__0__Impl rule__Close__Group__1 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:576:2: rule__Close__Group__0__Impl rule__Close__Group__1
            {
            pushFollow(FOLLOW_rule__Close__Group__0__Impl_in_rule__Close__Group__01157);
            rule__Close__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Close__Group__1_in_rule__Close__Group__01160);
            rule__Close__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Close__Group__0"


    // $ANTLR start "rule__Close__Group__0__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:583:1: rule__Close__Group__0__Impl : ( 'ferme la pince' ) ;
    public final void rule__Close__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:587:1: ( ( 'ferme la pince' ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:588:1: ( 'ferme la pince' )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:588:1: ( 'ferme la pince' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:589:1: 'ferme la pince'
            {
             before(grammarAccess.getCloseAccess().getFermeLaPinceKeyword_0()); 
            match(input,15,FOLLOW_15_in_rule__Close__Group__0__Impl1188); 
             after(grammarAccess.getCloseAccess().getFermeLaPinceKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Close__Group__0__Impl"


    // $ANTLR start "rule__Close__Group__1"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:602:1: rule__Close__Group__1 : rule__Close__Group__1__Impl ;
    public final void rule__Close__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:606:1: ( rule__Close__Group__1__Impl )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:607:2: rule__Close__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Close__Group__1__Impl_in_rule__Close__Group__11219);
            rule__Close__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Close__Group__1"


    // $ANTLR start "rule__Close__Group__1__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:613:1: rule__Close__Group__1__Impl : ( () ) ;
    public final void rule__Close__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:617:1: ( ( () ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:618:1: ( () )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:618:1: ( () )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:619:1: ()
            {
             before(grammarAccess.getCloseAccess().getCloseAction_1()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:620:1: ()
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:622:1: 
            {
            }

             after(grammarAccess.getCloseAccess().getCloseAction_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Close__Group__1__Impl"


    // $ANTLR start "rule__Open__Group__0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:636:1: rule__Open__Group__0 : rule__Open__Group__0__Impl rule__Open__Group__1 ;
    public final void rule__Open__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:640:1: ( rule__Open__Group__0__Impl rule__Open__Group__1 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:641:2: rule__Open__Group__0__Impl rule__Open__Group__1
            {
            pushFollow(FOLLOW_rule__Open__Group__0__Impl_in_rule__Open__Group__01281);
            rule__Open__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Open__Group__1_in_rule__Open__Group__01284);
            rule__Open__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Open__Group__0"


    // $ANTLR start "rule__Open__Group__0__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:648:1: rule__Open__Group__0__Impl : ( 'ouvre la pince' ) ;
    public final void rule__Open__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:652:1: ( ( 'ouvre la pince' ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:653:1: ( 'ouvre la pince' )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:653:1: ( 'ouvre la pince' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:654:1: 'ouvre la pince'
            {
             before(grammarAccess.getOpenAccess().getOuvreLaPinceKeyword_0()); 
            match(input,16,FOLLOW_16_in_rule__Open__Group__0__Impl1312); 
             after(grammarAccess.getOpenAccess().getOuvreLaPinceKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Open__Group__0__Impl"


    // $ANTLR start "rule__Open__Group__1"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:667:1: rule__Open__Group__1 : rule__Open__Group__1__Impl ;
    public final void rule__Open__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:671:1: ( rule__Open__Group__1__Impl )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:672:2: rule__Open__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Open__Group__1__Impl_in_rule__Open__Group__11343);
            rule__Open__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Open__Group__1"


    // $ANTLR start "rule__Open__Group__1__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:678:1: rule__Open__Group__1__Impl : ( () ) ;
    public final void rule__Open__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:682:1: ( ( () ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:683:1: ( () )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:683:1: ( () )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:684:1: ()
            {
             before(grammarAccess.getOpenAccess().getOpenAction_1()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:685:1: ()
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:687:1: 
            {
            }

             after(grammarAccess.getOpenAccess().getOpenAction_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Open__Group__1__Impl"


    // $ANTLR start "rule__Backward__Group__0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:701:1: rule__Backward__Group__0 : rule__Backward__Group__0__Impl rule__Backward__Group__1 ;
    public final void rule__Backward__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:705:1: ( rule__Backward__Group__0__Impl rule__Backward__Group__1 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:706:2: rule__Backward__Group__0__Impl rule__Backward__Group__1
            {
            pushFollow(FOLLOW_rule__Backward__Group__0__Impl_in_rule__Backward__Group__01405);
            rule__Backward__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Backward__Group__1_in_rule__Backward__Group__01408);
            rule__Backward__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Backward__Group__0"


    // $ANTLR start "rule__Backward__Group__0__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:713:1: rule__Backward__Group__0__Impl : ( 'recule de' ) ;
    public final void rule__Backward__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:717:1: ( ( 'recule de' ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:718:1: ( 'recule de' )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:718:1: ( 'recule de' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:719:1: 'recule de'
            {
             before(grammarAccess.getBackwardAccess().getReculeDeKeyword_0()); 
            match(input,17,FOLLOW_17_in_rule__Backward__Group__0__Impl1436); 
             after(grammarAccess.getBackwardAccess().getReculeDeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Backward__Group__0__Impl"


    // $ANTLR start "rule__Backward__Group__1"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:732:1: rule__Backward__Group__1 : rule__Backward__Group__1__Impl rule__Backward__Group__2 ;
    public final void rule__Backward__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:736:1: ( rule__Backward__Group__1__Impl rule__Backward__Group__2 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:737:2: rule__Backward__Group__1__Impl rule__Backward__Group__2
            {
            pushFollow(FOLLOW_rule__Backward__Group__1__Impl_in_rule__Backward__Group__11467);
            rule__Backward__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Backward__Group__2_in_rule__Backward__Group__11470);
            rule__Backward__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Backward__Group__1"


    // $ANTLR start "rule__Backward__Group__1__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:744:1: rule__Backward__Group__1__Impl : ( ( rule__Backward__DistanceAssignment_1 ) ) ;
    public final void rule__Backward__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:748:1: ( ( ( rule__Backward__DistanceAssignment_1 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:749:1: ( ( rule__Backward__DistanceAssignment_1 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:749:1: ( ( rule__Backward__DistanceAssignment_1 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:750:1: ( rule__Backward__DistanceAssignment_1 )
            {
             before(grammarAccess.getBackwardAccess().getDistanceAssignment_1()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:751:1: ( rule__Backward__DistanceAssignment_1 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:751:2: rule__Backward__DistanceAssignment_1
            {
            pushFollow(FOLLOW_rule__Backward__DistanceAssignment_1_in_rule__Backward__Group__1__Impl1497);
            rule__Backward__DistanceAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBackwardAccess().getDistanceAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Backward__Group__1__Impl"


    // $ANTLR start "rule__Backward__Group__2"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:761:1: rule__Backward__Group__2 : rule__Backward__Group__2__Impl ;
    public final void rule__Backward__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:765:1: ( rule__Backward__Group__2__Impl )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:766:2: rule__Backward__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Backward__Group__2__Impl_in_rule__Backward__Group__21527);
            rule__Backward__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Backward__Group__2"


    // $ANTLR start "rule__Backward__Group__2__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:772:1: rule__Backward__Group__2__Impl : ( 'cm' ) ;
    public final void rule__Backward__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:776:1: ( ( 'cm' ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:777:1: ( 'cm' )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:777:1: ( 'cm' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:778:1: 'cm'
            {
             before(grammarAccess.getBackwardAccess().getCmKeyword_2()); 
            match(input,18,FOLLOW_18_in_rule__Backward__Group__2__Impl1555); 
             after(grammarAccess.getBackwardAccess().getCmKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Backward__Group__2__Impl"


    // $ANTLR start "rule__Forward__Group__0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:797:1: rule__Forward__Group__0 : rule__Forward__Group__0__Impl rule__Forward__Group__1 ;
    public final void rule__Forward__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:801:1: ( rule__Forward__Group__0__Impl rule__Forward__Group__1 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:802:2: rule__Forward__Group__0__Impl rule__Forward__Group__1
            {
            pushFollow(FOLLOW_rule__Forward__Group__0__Impl_in_rule__Forward__Group__01592);
            rule__Forward__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Forward__Group__1_in_rule__Forward__Group__01595);
            rule__Forward__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forward__Group__0"


    // $ANTLR start "rule__Forward__Group__0__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:809:1: rule__Forward__Group__0__Impl : ( 'avance de' ) ;
    public final void rule__Forward__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:813:1: ( ( 'avance de' ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:814:1: ( 'avance de' )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:814:1: ( 'avance de' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:815:1: 'avance de'
            {
             before(grammarAccess.getForwardAccess().getAvanceDeKeyword_0()); 
            match(input,19,FOLLOW_19_in_rule__Forward__Group__0__Impl1623); 
             after(grammarAccess.getForwardAccess().getAvanceDeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forward__Group__0__Impl"


    // $ANTLR start "rule__Forward__Group__1"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:828:1: rule__Forward__Group__1 : rule__Forward__Group__1__Impl rule__Forward__Group__2 ;
    public final void rule__Forward__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:832:1: ( rule__Forward__Group__1__Impl rule__Forward__Group__2 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:833:2: rule__Forward__Group__1__Impl rule__Forward__Group__2
            {
            pushFollow(FOLLOW_rule__Forward__Group__1__Impl_in_rule__Forward__Group__11654);
            rule__Forward__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Forward__Group__2_in_rule__Forward__Group__11657);
            rule__Forward__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forward__Group__1"


    // $ANTLR start "rule__Forward__Group__1__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:840:1: rule__Forward__Group__1__Impl : ( ( rule__Forward__DistanceAssignment_1 ) ) ;
    public final void rule__Forward__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:844:1: ( ( ( rule__Forward__DistanceAssignment_1 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:845:1: ( ( rule__Forward__DistanceAssignment_1 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:845:1: ( ( rule__Forward__DistanceAssignment_1 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:846:1: ( rule__Forward__DistanceAssignment_1 )
            {
             before(grammarAccess.getForwardAccess().getDistanceAssignment_1()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:847:1: ( rule__Forward__DistanceAssignment_1 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:847:2: rule__Forward__DistanceAssignment_1
            {
            pushFollow(FOLLOW_rule__Forward__DistanceAssignment_1_in_rule__Forward__Group__1__Impl1684);
            rule__Forward__DistanceAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getForwardAccess().getDistanceAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forward__Group__1__Impl"


    // $ANTLR start "rule__Forward__Group__2"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:857:1: rule__Forward__Group__2 : rule__Forward__Group__2__Impl ;
    public final void rule__Forward__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:861:1: ( rule__Forward__Group__2__Impl )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:862:2: rule__Forward__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Forward__Group__2__Impl_in_rule__Forward__Group__21714);
            rule__Forward__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forward__Group__2"


    // $ANTLR start "rule__Forward__Group__2__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:868:1: rule__Forward__Group__2__Impl : ( 'cm' ) ;
    public final void rule__Forward__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:872:1: ( ( 'cm' ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:873:1: ( 'cm' )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:873:1: ( 'cm' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:874:1: 'cm'
            {
             before(grammarAccess.getForwardAccess().getCmKeyword_2()); 
            match(input,18,FOLLOW_18_in_rule__Forward__Group__2__Impl1742); 
             after(grammarAccess.getForwardAccess().getCmKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forward__Group__2__Impl"


    // $ANTLR start "rule__Turn__Group__0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:893:1: rule__Turn__Group__0 : rule__Turn__Group__0__Impl rule__Turn__Group__1 ;
    public final void rule__Turn__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:897:1: ( rule__Turn__Group__0__Impl rule__Turn__Group__1 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:898:2: rule__Turn__Group__0__Impl rule__Turn__Group__1
            {
            pushFollow(FOLLOW_rule__Turn__Group__0__Impl_in_rule__Turn__Group__01779);
            rule__Turn__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Turn__Group__1_in_rule__Turn__Group__01782);
            rule__Turn__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group__0"


    // $ANTLR start "rule__Turn__Group__0__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:905:1: rule__Turn__Group__0__Impl : ( ( rule__Turn__Alternatives_0 ) ) ;
    public final void rule__Turn__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:909:1: ( ( ( rule__Turn__Alternatives_0 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:910:1: ( ( rule__Turn__Alternatives_0 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:910:1: ( ( rule__Turn__Alternatives_0 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:911:1: ( rule__Turn__Alternatives_0 )
            {
             before(grammarAccess.getTurnAccess().getAlternatives_0()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:912:1: ( rule__Turn__Alternatives_0 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:912:2: rule__Turn__Alternatives_0
            {
            pushFollow(FOLLOW_rule__Turn__Alternatives_0_in_rule__Turn__Group__0__Impl1809);
            rule__Turn__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getTurnAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group__0__Impl"


    // $ANTLR start "rule__Turn__Group__1"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:922:1: rule__Turn__Group__1 : rule__Turn__Group__1__Impl ;
    public final void rule__Turn__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:926:1: ( rule__Turn__Group__1__Impl )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:927:2: rule__Turn__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Turn__Group__1__Impl_in_rule__Turn__Group__11839);
            rule__Turn__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group__1"


    // $ANTLR start "rule__Turn__Group__1__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:933:1: rule__Turn__Group__1__Impl : ( ( rule__Turn__Group_1__0 )? ) ;
    public final void rule__Turn__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:937:1: ( ( ( rule__Turn__Group_1__0 )? ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:938:1: ( ( rule__Turn__Group_1__0 )? )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:938:1: ( ( rule__Turn__Group_1__0 )? )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:939:1: ( rule__Turn__Group_1__0 )?
            {
             before(grammarAccess.getTurnAccess().getGroup_1()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:940:1: ( rule__Turn__Group_1__0 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==20) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:940:2: rule__Turn__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__Turn__Group_1__0_in_rule__Turn__Group__1__Impl1866);
                    rule__Turn__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTurnAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group__1__Impl"


    // $ANTLR start "rule__Turn__Group_1__0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:954:1: rule__Turn__Group_1__0 : rule__Turn__Group_1__0__Impl rule__Turn__Group_1__1 ;
    public final void rule__Turn__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:958:1: ( rule__Turn__Group_1__0__Impl rule__Turn__Group_1__1 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:959:2: rule__Turn__Group_1__0__Impl rule__Turn__Group_1__1
            {
            pushFollow(FOLLOW_rule__Turn__Group_1__0__Impl_in_rule__Turn__Group_1__01901);
            rule__Turn__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Turn__Group_1__1_in_rule__Turn__Group_1__01904);
            rule__Turn__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group_1__0"


    // $ANTLR start "rule__Turn__Group_1__0__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:966:1: rule__Turn__Group_1__0__Impl : ( 'de' ) ;
    public final void rule__Turn__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:970:1: ( ( 'de' ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:971:1: ( 'de' )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:971:1: ( 'de' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:972:1: 'de'
            {
             before(grammarAccess.getTurnAccess().getDeKeyword_1_0()); 
            match(input,20,FOLLOW_20_in_rule__Turn__Group_1__0__Impl1932); 
             after(grammarAccess.getTurnAccess().getDeKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group_1__0__Impl"


    // $ANTLR start "rule__Turn__Group_1__1"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:985:1: rule__Turn__Group_1__1 : rule__Turn__Group_1__1__Impl ;
    public final void rule__Turn__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:989:1: ( rule__Turn__Group_1__1__Impl )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:990:2: rule__Turn__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Turn__Group_1__1__Impl_in_rule__Turn__Group_1__11963);
            rule__Turn__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group_1__1"


    // $ANTLR start "rule__Turn__Group_1__1__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:996:1: rule__Turn__Group_1__1__Impl : ( ( rule__Turn__Alternatives_1_1 ) ) ;
    public final void rule__Turn__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1000:1: ( ( ( rule__Turn__Alternatives_1_1 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1001:1: ( ( rule__Turn__Alternatives_1_1 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1001:1: ( ( rule__Turn__Alternatives_1_1 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1002:1: ( rule__Turn__Alternatives_1_1 )
            {
             before(grammarAccess.getTurnAccess().getAlternatives_1_1()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1003:1: ( rule__Turn__Alternatives_1_1 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1003:2: rule__Turn__Alternatives_1_1
            {
            pushFollow(FOLLOW_rule__Turn__Alternatives_1_1_in_rule__Turn__Group_1__1__Impl1990);
            rule__Turn__Alternatives_1_1();

            state._fsp--;


            }

             after(grammarAccess.getTurnAccess().getAlternatives_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group_1__1__Impl"


    // $ANTLR start "rule__Turn__Group_1_1_0__0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1017:1: rule__Turn__Group_1_1_0__0 : rule__Turn__Group_1_1_0__0__Impl rule__Turn__Group_1_1_0__1 ;
    public final void rule__Turn__Group_1_1_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1021:1: ( rule__Turn__Group_1_1_0__0__Impl rule__Turn__Group_1_1_0__1 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1022:2: rule__Turn__Group_1_1_0__0__Impl rule__Turn__Group_1_1_0__1
            {
            pushFollow(FOLLOW_rule__Turn__Group_1_1_0__0__Impl_in_rule__Turn__Group_1_1_0__02024);
            rule__Turn__Group_1_1_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Turn__Group_1_1_0__1_in_rule__Turn__Group_1_1_0__02027);
            rule__Turn__Group_1_1_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group_1_1_0__0"


    // $ANTLR start "rule__Turn__Group_1_1_0__0__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1029:1: rule__Turn__Group_1_1_0__0__Impl : ( ( rule__Turn__AngleAssignment_1_1_0_0 ) ) ;
    public final void rule__Turn__Group_1_1_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1033:1: ( ( ( rule__Turn__AngleAssignment_1_1_0_0 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1034:1: ( ( rule__Turn__AngleAssignment_1_1_0_0 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1034:1: ( ( rule__Turn__AngleAssignment_1_1_0_0 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1035:1: ( rule__Turn__AngleAssignment_1_1_0_0 )
            {
             before(grammarAccess.getTurnAccess().getAngleAssignment_1_1_0_0()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1036:1: ( rule__Turn__AngleAssignment_1_1_0_0 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1036:2: rule__Turn__AngleAssignment_1_1_0_0
            {
            pushFollow(FOLLOW_rule__Turn__AngleAssignment_1_1_0_0_in_rule__Turn__Group_1_1_0__0__Impl2054);
            rule__Turn__AngleAssignment_1_1_0_0();

            state._fsp--;


            }

             after(grammarAccess.getTurnAccess().getAngleAssignment_1_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group_1_1_0__0__Impl"


    // $ANTLR start "rule__Turn__Group_1_1_0__1"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1046:1: rule__Turn__Group_1_1_0__1 : rule__Turn__Group_1_1_0__1__Impl ;
    public final void rule__Turn__Group_1_1_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1050:1: ( rule__Turn__Group_1_1_0__1__Impl )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1051:2: rule__Turn__Group_1_1_0__1__Impl
            {
            pushFollow(FOLLOW_rule__Turn__Group_1_1_0__1__Impl_in_rule__Turn__Group_1_1_0__12084);
            rule__Turn__Group_1_1_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group_1_1_0__1"


    // $ANTLR start "rule__Turn__Group_1_1_0__1__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1057:1: rule__Turn__Group_1_1_0__1__Impl : ( 'degr\\u00E9' ) ;
    public final void rule__Turn__Group_1_1_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1061:1: ( ( 'degr\\u00E9' ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1062:1: ( 'degr\\u00E9' )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1062:1: ( 'degr\\u00E9' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1063:1: 'degr\\u00E9'
            {
             before(grammarAccess.getTurnAccess().getDegrKeyword_1_1_0_1()); 
            match(input,21,FOLLOW_21_in_rule__Turn__Group_1_1_0__1__Impl2112); 
             after(grammarAccess.getTurnAccess().getDegrKeyword_1_1_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group_1_1_0__1__Impl"


    // $ANTLR start "rule__Turn__Group_1_1_1__0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1080:1: rule__Turn__Group_1_1_1__0 : rule__Turn__Group_1_1_1__0__Impl rule__Turn__Group_1_1_1__1 ;
    public final void rule__Turn__Group_1_1_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1084:1: ( rule__Turn__Group_1_1_1__0__Impl rule__Turn__Group_1_1_1__1 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1085:2: rule__Turn__Group_1_1_1__0__Impl rule__Turn__Group_1_1_1__1
            {
            pushFollow(FOLLOW_rule__Turn__Group_1_1_1__0__Impl_in_rule__Turn__Group_1_1_1__02147);
            rule__Turn__Group_1_1_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Turn__Group_1_1_1__1_in_rule__Turn__Group_1_1_1__02150);
            rule__Turn__Group_1_1_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group_1_1_1__0"


    // $ANTLR start "rule__Turn__Group_1_1_1__0__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1092:1: rule__Turn__Group_1_1_1__0__Impl : ( ( rule__Turn__AngleAssignment_1_1_1_0 ) ) ;
    public final void rule__Turn__Group_1_1_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1096:1: ( ( ( rule__Turn__AngleAssignment_1_1_1_0 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1097:1: ( ( rule__Turn__AngleAssignment_1_1_1_0 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1097:1: ( ( rule__Turn__AngleAssignment_1_1_1_0 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1098:1: ( rule__Turn__AngleAssignment_1_1_1_0 )
            {
             before(grammarAccess.getTurnAccess().getAngleAssignment_1_1_1_0()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1099:1: ( rule__Turn__AngleAssignment_1_1_1_0 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1099:2: rule__Turn__AngleAssignment_1_1_1_0
            {
            pushFollow(FOLLOW_rule__Turn__AngleAssignment_1_1_1_0_in_rule__Turn__Group_1_1_1__0__Impl2177);
            rule__Turn__AngleAssignment_1_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getTurnAccess().getAngleAssignment_1_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group_1_1_1__0__Impl"


    // $ANTLR start "rule__Turn__Group_1_1_1__1"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1109:1: rule__Turn__Group_1_1_1__1 : rule__Turn__Group_1_1_1__1__Impl ;
    public final void rule__Turn__Group_1_1_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1113:1: ( rule__Turn__Group_1_1_1__1__Impl )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1114:2: rule__Turn__Group_1_1_1__1__Impl
            {
            pushFollow(FOLLOW_rule__Turn__Group_1_1_1__1__Impl_in_rule__Turn__Group_1_1_1__12207);
            rule__Turn__Group_1_1_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group_1_1_1__1"


    // $ANTLR start "rule__Turn__Group_1_1_1__1__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1120:1: rule__Turn__Group_1_1_1__1__Impl : ( 'degr\\u00E9s' ) ;
    public final void rule__Turn__Group_1_1_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1124:1: ( ( 'degr\\u00E9s' ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1125:1: ( 'degr\\u00E9s' )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1125:1: ( 'degr\\u00E9s' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1126:1: 'degr\\u00E9s'
            {
             before(grammarAccess.getTurnAccess().getDegrSKeyword_1_1_1_1()); 
            match(input,22,FOLLOW_22_in_rule__Turn__Group_1_1_1__1__Impl2235); 
             after(grammarAccess.getTurnAccess().getDegrSKeyword_1_1_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__Group_1_1_1__1__Impl"


    // $ANTLR start "rule__TurnCCW__Group__0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1143:1: rule__TurnCCW__Group__0 : rule__TurnCCW__Group__0__Impl rule__TurnCCW__Group__1 ;
    public final void rule__TurnCCW__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1147:1: ( rule__TurnCCW__Group__0__Impl rule__TurnCCW__Group__1 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1148:2: rule__TurnCCW__Group__0__Impl rule__TurnCCW__Group__1
            {
            pushFollow(FOLLOW_rule__TurnCCW__Group__0__Impl_in_rule__TurnCCW__Group__02270);
            rule__TurnCCW__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TurnCCW__Group__1_in_rule__TurnCCW__Group__02273);
            rule__TurnCCW__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCCW__Group__0"


    // $ANTLR start "rule__TurnCCW__Group__0__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1155:1: rule__TurnCCW__Group__0__Impl : ( 'tourne \\u00E0 gauche' ) ;
    public final void rule__TurnCCW__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1159:1: ( ( 'tourne \\u00E0 gauche' ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1160:1: ( 'tourne \\u00E0 gauche' )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1160:1: ( 'tourne \\u00E0 gauche' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1161:1: 'tourne \\u00E0 gauche'
            {
             before(grammarAccess.getTurnCCWAccess().getTourneGaucheKeyword_0()); 
            match(input,23,FOLLOW_23_in_rule__TurnCCW__Group__0__Impl2301); 
             after(grammarAccess.getTurnCCWAccess().getTourneGaucheKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCCW__Group__0__Impl"


    // $ANTLR start "rule__TurnCCW__Group__1"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1174:1: rule__TurnCCW__Group__1 : rule__TurnCCW__Group__1__Impl ;
    public final void rule__TurnCCW__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1178:1: ( rule__TurnCCW__Group__1__Impl )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1179:2: rule__TurnCCW__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__TurnCCW__Group__1__Impl_in_rule__TurnCCW__Group__12332);
            rule__TurnCCW__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCCW__Group__1"


    // $ANTLR start "rule__TurnCCW__Group__1__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1185:1: rule__TurnCCW__Group__1__Impl : ( () ) ;
    public final void rule__TurnCCW__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1189:1: ( ( () ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1190:1: ( () )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1190:1: ( () )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1191:1: ()
            {
             before(grammarAccess.getTurnCCWAccess().getTurnCCWAction_1()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1192:1: ()
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1194:1: 
            {
            }

             after(grammarAccess.getTurnCCWAccess().getTurnCCWAction_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCCW__Group__1__Impl"


    // $ANTLR start "rule__TurnCW__Group__0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1208:1: rule__TurnCW__Group__0 : rule__TurnCW__Group__0__Impl rule__TurnCW__Group__1 ;
    public final void rule__TurnCW__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1212:1: ( rule__TurnCW__Group__0__Impl rule__TurnCW__Group__1 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1213:2: rule__TurnCW__Group__0__Impl rule__TurnCW__Group__1
            {
            pushFollow(FOLLOW_rule__TurnCW__Group__0__Impl_in_rule__TurnCW__Group__02394);
            rule__TurnCW__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TurnCW__Group__1_in_rule__TurnCW__Group__02397);
            rule__TurnCW__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCW__Group__0"


    // $ANTLR start "rule__TurnCW__Group__0__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1220:1: rule__TurnCW__Group__0__Impl : ( 'tourne \\u00E0 droite' ) ;
    public final void rule__TurnCW__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1224:1: ( ( 'tourne \\u00E0 droite' ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1225:1: ( 'tourne \\u00E0 droite' )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1225:1: ( 'tourne \\u00E0 droite' )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1226:1: 'tourne \\u00E0 droite'
            {
             before(grammarAccess.getTurnCWAccess().getTourneDroiteKeyword_0()); 
            match(input,24,FOLLOW_24_in_rule__TurnCW__Group__0__Impl2425); 
             after(grammarAccess.getTurnCWAccess().getTourneDroiteKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCW__Group__0__Impl"


    // $ANTLR start "rule__TurnCW__Group__1"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1239:1: rule__TurnCW__Group__1 : rule__TurnCW__Group__1__Impl ;
    public final void rule__TurnCW__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1243:1: ( rule__TurnCW__Group__1__Impl )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1244:2: rule__TurnCW__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__TurnCW__Group__1__Impl_in_rule__TurnCW__Group__12456);
            rule__TurnCW__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCW__Group__1"


    // $ANTLR start "rule__TurnCW__Group__1__Impl"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1250:1: rule__TurnCW__Group__1__Impl : ( () ) ;
    public final void rule__TurnCW__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1254:1: ( ( () ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1255:1: ( () )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1255:1: ( () )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1256:1: ()
            {
             before(grammarAccess.getTurnCWAccess().getTurnCWAction_1()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1257:1: ()
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1259:1: 
            {
            }

             after(grammarAccess.getTurnCWAccess().getTurnCWAction_1()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCW__Group__1__Impl"


    // $ANTLR start "rule__InstructionsSet__InstructionsAssignment_2"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1274:1: rule__InstructionsSet__InstructionsAssignment_2 : ( ruleInstruction ) ;
    public final void rule__InstructionsSet__InstructionsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1278:1: ( ( ruleInstruction ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1279:1: ( ruleInstruction )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1279:1: ( ruleInstruction )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1280:1: ruleInstruction
            {
             before(grammarAccess.getInstructionsSetAccess().getInstructionsInstructionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleInstruction_in_rule__InstructionsSet__InstructionsAssignment_22523);
            ruleInstruction();

            state._fsp--;

             after(grammarAccess.getInstructionsSetAccess().getInstructionsInstructionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InstructionsSet__InstructionsAssignment_2"


    // $ANTLR start "rule__Backward__DistanceAssignment_1"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1289:1: rule__Backward__DistanceAssignment_1 : ( ( rule__Backward__DistanceAlternatives_1_0 ) ) ;
    public final void rule__Backward__DistanceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1293:1: ( ( ( rule__Backward__DistanceAlternatives_1_0 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1294:1: ( ( rule__Backward__DistanceAlternatives_1_0 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1294:1: ( ( rule__Backward__DistanceAlternatives_1_0 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1295:1: ( rule__Backward__DistanceAlternatives_1_0 )
            {
             before(grammarAccess.getBackwardAccess().getDistanceAlternatives_1_0()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1296:1: ( rule__Backward__DistanceAlternatives_1_0 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1296:2: rule__Backward__DistanceAlternatives_1_0
            {
            pushFollow(FOLLOW_rule__Backward__DistanceAlternatives_1_0_in_rule__Backward__DistanceAssignment_12554);
            rule__Backward__DistanceAlternatives_1_0();

            state._fsp--;


            }

             after(grammarAccess.getBackwardAccess().getDistanceAlternatives_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Backward__DistanceAssignment_1"


    // $ANTLR start "rule__Forward__DistanceAssignment_1"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1305:1: rule__Forward__DistanceAssignment_1 : ( ( rule__Forward__DistanceAlternatives_1_0 ) ) ;
    public final void rule__Forward__DistanceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1309:1: ( ( ( rule__Forward__DistanceAlternatives_1_0 ) ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1310:1: ( ( rule__Forward__DistanceAlternatives_1_0 ) )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1310:1: ( ( rule__Forward__DistanceAlternatives_1_0 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1311:1: ( rule__Forward__DistanceAlternatives_1_0 )
            {
             before(grammarAccess.getForwardAccess().getDistanceAlternatives_1_0()); 
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1312:1: ( rule__Forward__DistanceAlternatives_1_0 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1312:2: rule__Forward__DistanceAlternatives_1_0
            {
            pushFollow(FOLLOW_rule__Forward__DistanceAlternatives_1_0_in_rule__Forward__DistanceAssignment_12587);
            rule__Forward__DistanceAlternatives_1_0();

            state._fsp--;


            }

             after(grammarAccess.getForwardAccess().getDistanceAlternatives_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forward__DistanceAssignment_1"


    // $ANTLR start "rule__Turn__AngleAssignment_1_1_0_0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1321:1: rule__Turn__AngleAssignment_1_1_0_0 : ( RULE_ONE ) ;
    public final void rule__Turn__AngleAssignment_1_1_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1325:1: ( ( RULE_ONE ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1326:1: ( RULE_ONE )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1326:1: ( RULE_ONE )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1327:1: RULE_ONE
            {
             before(grammarAccess.getTurnAccess().getAngleONETerminalRuleCall_1_1_0_0_0()); 
            match(input,RULE_ONE,FOLLOW_RULE_ONE_in_rule__Turn__AngleAssignment_1_1_0_02620); 
             after(grammarAccess.getTurnAccess().getAngleONETerminalRuleCall_1_1_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__AngleAssignment_1_1_0_0"


    // $ANTLR start "rule__Turn__AngleAssignment_1_1_1_0"
    // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1336:1: rule__Turn__AngleAssignment_1_1_1_0 : ( RULE_POS_1 ) ;
    public final void rule__Turn__AngleAssignment_1_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1340:1: ( ( RULE_POS_1 ) )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1341:1: ( RULE_POS_1 )
            {
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1341:1: ( RULE_POS_1 )
            // ../demo.lego.ui/src-gen/demo/lego/ui/contentassist/antlr/internal/InternalLego.g:1342:1: RULE_POS_1
            {
             before(grammarAccess.getTurnAccess().getAnglePOS_1TerminalRuleCall_1_1_1_0_0()); 
            match(input,RULE_POS_1,FOLLOW_RULE_POS_1_in_rule__Turn__AngleAssignment_1_1_1_02651); 
             after(grammarAccess.getTurnAccess().getAnglePOS_1TerminalRuleCall_1_1_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Turn__AngleAssignment_1_1_1_0"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleInstructionsSet_in_entryRuleInstructionsSet61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstructionsSet68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InstructionsSet__Group__0_in_ruleInstructionsSet94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstruction_in_entryRuleInstruction121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstruction128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Instruction__Alternatives_in_ruleInstruction154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClose_in_entryRuleClose181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleClose188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Close__Group__0_in_ruleClose214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpen_in_entryRuleOpen241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOpen248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Open__Group__0_in_ruleOpen274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBackward_in_entryRuleBackward301 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBackward308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Backward__Group__0_in_ruleBackward334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForward_in_entryRuleForward361 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleForward368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Forward__Group__0_in_ruleForward394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurn_in_entryRuleTurn421 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTurn428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__Group__0_in_ruleTurn454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurnCCW_in_entryRuleTurnCCW481 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTurnCCW488 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCCW__Group__0_in_ruleTurnCCW514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurnCW_in_entryRuleTurnCW541 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTurnCW548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCW__Group__0_in_ruleTurnCW574 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBackward_in_rule__Instruction__Alternatives610 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForward_in_rule__Instruction__Alternatives627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurn_in_rule__Instruction__Alternatives644 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpen_in_rule__Instruction__Alternatives661 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClose_in_rule__Instruction__Alternatives678 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ONE_in_rule__Backward__DistanceAlternatives_1_0710 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_POS_1_in_rule__Backward__DistanceAlternatives_1_0727 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ONE_in_rule__Forward__DistanceAlternatives_1_0759 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_POS_1_in_rule__Forward__DistanceAlternatives_1_0776 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurnCW_in_rule__Turn__Alternatives_0808 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurnCCW_in_rule__Turn__Alternatives_0825 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__Group_1_1_0__0_in_rule__Turn__Alternatives_1_1857 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__Group_1_1_1__0_in_rule__Turn__Alternatives_1_1875 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InstructionsSet__Group__0__Impl_in_rule__InstructionsSet__Group__0906 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__InstructionsSet__Group__1_in_rule__InstructionsSet__Group__0909 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InstructionsSet__Group__1__Impl_in_rule__InstructionsSet__Group__1967 = new BitSet(new long[]{0x00000000018BC000L});
    public static final BitSet FOLLOW_rule__InstructionsSet__Group__2_in_rule__InstructionsSet__Group__1970 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__InstructionsSet__Group__1__Impl998 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InstructionsSet__Group__2__Impl_in_rule__InstructionsSet__Group__21029 = new BitSet(new long[]{0x00000000018BC000L});
    public static final BitSet FOLLOW_rule__InstructionsSet__Group__3_in_rule__InstructionsSet__Group__21032 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InstructionsSet__InstructionsAssignment_2_in_rule__InstructionsSet__Group__2__Impl1059 = new BitSet(new long[]{0x00000000018B8002L});
    public static final BitSet FOLLOW_rule__InstructionsSet__Group__3__Impl_in_rule__InstructionsSet__Group__31090 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__InstructionsSet__Group__3__Impl1118 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Close__Group__0__Impl_in_rule__Close__Group__01157 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Close__Group__1_in_rule__Close__Group__01160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Close__Group__0__Impl1188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Close__Group__1__Impl_in_rule__Close__Group__11219 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Open__Group__0__Impl_in_rule__Open__Group__01281 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Open__Group__1_in_rule__Open__Group__01284 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Open__Group__0__Impl1312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Open__Group__1__Impl_in_rule__Open__Group__11343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Backward__Group__0__Impl_in_rule__Backward__Group__01405 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_rule__Backward__Group__1_in_rule__Backward__Group__01408 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Backward__Group__0__Impl1436 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Backward__Group__1__Impl_in_rule__Backward__Group__11467 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__Backward__Group__2_in_rule__Backward__Group__11470 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Backward__DistanceAssignment_1_in_rule__Backward__Group__1__Impl1497 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Backward__Group__2__Impl_in_rule__Backward__Group__21527 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Backward__Group__2__Impl1555 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Forward__Group__0__Impl_in_rule__Forward__Group__01592 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_rule__Forward__Group__1_in_rule__Forward__Group__01595 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__Forward__Group__0__Impl1623 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Forward__Group__1__Impl_in_rule__Forward__Group__11654 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__Forward__Group__2_in_rule__Forward__Group__11657 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Forward__DistanceAssignment_1_in_rule__Forward__Group__1__Impl1684 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Forward__Group__2__Impl_in_rule__Forward__Group__21714 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Forward__Group__2__Impl1742 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__Group__0__Impl_in_rule__Turn__Group__01779 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_rule__Turn__Group__1_in_rule__Turn__Group__01782 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__Alternatives_0_in_rule__Turn__Group__0__Impl1809 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__Group__1__Impl_in_rule__Turn__Group__11839 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__Group_1__0_in_rule__Turn__Group__1__Impl1866 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__Group_1__0__Impl_in_rule__Turn__Group_1__01901 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_rule__Turn__Group_1__1_in_rule__Turn__Group_1__01904 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Turn__Group_1__0__Impl1932 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__Group_1__1__Impl_in_rule__Turn__Group_1__11963 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__Alternatives_1_1_in_rule__Turn__Group_1__1__Impl1990 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__Group_1_1_0__0__Impl_in_rule__Turn__Group_1_1_0__02024 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__Turn__Group_1_1_0__1_in_rule__Turn__Group_1_1_0__02027 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__AngleAssignment_1_1_0_0_in_rule__Turn__Group_1_1_0__0__Impl2054 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__Group_1_1_0__1__Impl_in_rule__Turn__Group_1_1_0__12084 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Turn__Group_1_1_0__1__Impl2112 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__Group_1_1_1__0__Impl_in_rule__Turn__Group_1_1_1__02147 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__Turn__Group_1_1_1__1_in_rule__Turn__Group_1_1_1__02150 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__AngleAssignment_1_1_1_0_in_rule__Turn__Group_1_1_1__0__Impl2177 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Turn__Group_1_1_1__1__Impl_in_rule__Turn__Group_1_1_1__12207 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Turn__Group_1_1_1__1__Impl2235 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCCW__Group__0__Impl_in_rule__TurnCCW__Group__02270 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__TurnCCW__Group__1_in_rule__TurnCCW__Group__02273 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__TurnCCW__Group__0__Impl2301 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCCW__Group__1__Impl_in_rule__TurnCCW__Group__12332 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCW__Group__0__Impl_in_rule__TurnCW__Group__02394 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__TurnCW__Group__1_in_rule__TurnCW__Group__02397 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__TurnCW__Group__0__Impl2425 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCW__Group__1__Impl_in_rule__TurnCW__Group__12456 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstruction_in_rule__InstructionsSet__InstructionsAssignment_22523 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Backward__DistanceAlternatives_1_0_in_rule__Backward__DistanceAssignment_12554 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Forward__DistanceAlternatives_1_0_in_rule__Forward__DistanceAssignment_12587 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ONE_in_rule__Turn__AngleAssignment_1_1_0_02620 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_POS_1_in_rule__Turn__AngleAssignment_1_1_1_02651 = new BitSet(new long[]{0x0000000000000002L});

}