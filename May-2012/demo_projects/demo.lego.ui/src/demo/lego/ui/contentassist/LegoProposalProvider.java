package demo.lego.ui.contentassist;

import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;

import com.google.inject.Inject;

import demo.lego.services.LegoGrammarAccess;

/**
 * see http://www.eclipse.org/Xtext/documentation/latest/xtext.html#contentAssist on how to customize content assistant
 */
public class LegoProposalProvider extends AbstractLegoProposalProvider {
	@Inject LegoGrammarAccess grammarAccess;
	private String prefix = "";
	@Override
	public void completeKeyword (Keyword keyword, ContentAssistContext context, ICompletionProposalAcceptor acceptor) {
		if (grammarAccess.getTurnAccess().getDeKeyword_1_0().equals(keyword) && prefix.isEmpty()) return; // 'de' keyword in 'Turn' rule
		prefix = context.getPrefix(); // memorize, because context changes ...
		super.completeKeyword(keyword, context, acceptor);
	}
}
