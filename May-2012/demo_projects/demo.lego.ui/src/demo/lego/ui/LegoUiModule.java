package demo.lego.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtext.ui.editor.XtextEditor;

import com.google.inject.Binder;

import demo.lego.ui.editor.LegoEditor;

/**
 * Use this class to register components to be used within the IDE.
 */
public class LegoUiModule extends demo.lego.ui.AbstractLegoUiModule {

	public LegoUiModule(AbstractUIPlugin plugin) {
		super(plugin);
	}

	public void configure(Binder binder) {
		super.configure(binder);
		binder.bind(XtextEditor.class).to(LegoEditor.class);
	}
/*
	public Class<? extends AbstractEditStrategyProvider> bindAbstractEditStrategyProvider() {
		return DefaultAutoEditStrategyProvider.class;
	}

	public Class<? extends IResourceForEditorInputFactory> bindIResourceForEditorInputFactory() {
		return ResourceForIEditorInputFactory.class;
	}

	public Class<? extends IResourceSetProvider> bindIResourceSetProvider() {
		return SimpleResourceSetProvider.class;
	}

	public com.google.inject.Provider<org.eclipse.xtext.resource.containers.IAllContainersState> provideIAllContainersState() {
		return org.eclipse.xtext.ui.shared.Access.getWorkspaceProjectsState();
	}

	public static class NoFolding implements IFoldingRegionProvider {
		public Collection<FoldedPosition> getFoldingRegions(IXtextDocument xtextDocument) {
			return Collections.emptyList();
		}
	}

	public Class<? extends IFoldingRegionProvider> bindIFoldingRegionProvider() {
		return NoFolding.class;
	}
*/
}
