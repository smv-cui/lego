package demo.lego.ui.editor;

import org.apache.log4j.Logger;

import demo.commons.LogManager;
import demo.commons.UnclosableEditor;

public class LegoEditor extends UnclosableEditor {
	private static final Logger LOG = LogManager.INSTANCE.getLogger(LegoEditor.class.getName());

	public LegoEditor() {
		super();
		if (LOG.isDebugEnabled()) LOG.debug("Call constructor method");
	}
}
