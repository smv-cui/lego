<?xml version="1.0" encoding="UTF-8"?>
<?eclipse version="3.0"?>

<plugin>

    <extension
            point="org.eclipse.ui.editors">
        <editor
            class="demo.lego.ui.LegoExecutableExtensionFactory:org.eclipse.xtext.ui.editor.XtextEditor"
            contributorClass="org.eclipse.ui.editors.text.TextEditorActionContributor"
            default="true"
            extensions="lego"
            id="demo.lego.Lego"
            name="Lego Editor">
        </editor>
    </extension>
    <extension
        point="org.eclipse.ui.handlers">
        <handler
            class="demo.lego.ui.LegoExecutableExtensionFactory:org.eclipse.xtext.ui.editor.hyperlinking.OpenDeclarationHandler"
            commandId="org.eclipse.xtext.ui.editor.hyperlinking.OpenDeclaration">
            <activeWhen>
                <reference
                    definitionId="demo.lego.Lego.Editor.opened">
                </reference>
            </activeWhen>
        </handler>
        <handler
            class="demo.lego.ui.LegoExecutableExtensionFactory:org.eclipse.xtext.ui.editor.handler.ValidateActionHandler"
            commandId="demo.lego.Lego.validate">
         <activeWhen>
            <reference
                    definitionId="demo.lego.Lego.Editor.opened">
            </reference>
         </activeWhen>
      </handler>
    </extension>
    <extension point="org.eclipse.core.expressions.definitions">
        <definition id="demo.lego.Lego.Editor.opened">
            <and>
                <reference definitionId="isActiveEditorAnInstanceOfXtextEditor"/>
                <with variable="activeEditor">
                    <test property="org.eclipse.xtext.ui.editor.XtextEditor.languageName" 
                        value="demo.lego.Lego" 
                        forcePluginActivation="true"/>
                </with>        
            </and>
        </definition>
    </extension>
    <extension
            point="org.eclipse.ui.preferencePages">
        <page
            class="demo.lego.ui.LegoExecutableExtensionFactory:org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage"
            id="demo.lego.Lego"
            name="Lego">
            <keywordReference id="demo.lego.ui.keyword_Lego"/>
        </page>
        <page
            category="demo.lego.Lego"
            class="demo.lego.ui.LegoExecutableExtensionFactory:org.eclipse.xtext.ui.editor.syntaxcoloring.SyntaxColoringPreferencePage"
            id="demo.lego.Lego.coloring"
            name="Syntax Coloring">
            <keywordReference id="demo.lego.ui.keyword_Lego"/>
        </page>
        <page
            category="demo.lego.Lego"
            class="demo.lego.ui.LegoExecutableExtensionFactory:org.eclipse.xtext.ui.editor.templates.XtextTemplatePreferencePage"
            id="demo.lego.Lego.templates"
            name="Templates">
            <keywordReference id="demo.lego.ui.keyword_Lego"/>
        </page>
    </extension>
    <extension
            point="org.eclipse.ui.propertyPages">
        <page
            class="demo.lego.ui.LegoExecutableExtensionFactory:org.eclipse.xtext.ui.editor.preferences.LanguageRootPreferencePage"
            id="demo.lego.Lego"
            name="Lego">
            <keywordReference id="demo.lego.ui.keyword_Lego"/>
            <enabledWhen>
	            <adapt type="org.eclipse.core.resources.IProject"/>
			</enabledWhen>
	        <filter name="projectNature" value="org.eclipse.xtext.ui.shared.xtextNature"/>
        </page>
    </extension>
    <extension
        point="org.eclipse.ui.keywords">
        <keyword
            id="demo.lego.ui.keyword_Lego"
            label="Lego"/>
    </extension>
    <extension
         point="org.eclipse.ui.commands">
      <command
            description="Trigger expensive validation"
            id="demo.lego.Lego.validate"
            name="Validate">
      </command>
    </extension>
    <extension point="org.eclipse.ui.menus">
        <menuContribution
            locationURI="popup:#TextEditorContext?after=group.edit">
             <command
                 commandId="demo.lego.Lego.validate"
                 style="push"
                 tooltip="Trigger expensive validation">
            <visibleWhen checkEnabled="false">
                <reference
                    definitionId="demo.lego.Lego.Editor.opened">
                </reference>
            </visibleWhen>
         </command>  
         </menuContribution>
    </extension>
    <extension point="org.eclipse.ui.menus">
		<menuContribution locationURI="popup:#TextEditorContext?endof=group.find">
			<command commandId="org.eclipse.xtext.ui.editor.FindReferences">
				<visibleWhen checkEnabled="false">
                	<reference definitionId="demo.lego.Lego.Editor.opened">
                	</reference>
            	</visibleWhen>
			</command>
		</menuContribution>
	</extension>
	<extension point="org.eclipse.ui.handlers">
	    <handler
            class="demo.lego.ui.LegoExecutableExtensionFactory:org.eclipse.xtext.ui.editor.findrefs.FindReferencesHandler"
            commandId="org.eclipse.xtext.ui.editor.FindReferences">
            <activeWhen>
                <reference
                    definitionId="demo.lego.Lego.Editor.opened">
                </reference>
            </activeWhen>
        </handler>
    </extension>   

<!-- adding resource factories -->

	<extension
		point="org.eclipse.emf.ecore.extension_parser">
		<parser
			class="demo.lego.ui.LegoExecutableExtensionFactory:org.eclipse.xtext.resource.IResourceFactory"
			type="lego">
		</parser>
	</extension>
	<extension point="org.eclipse.xtext.extension_resourceServiceProvider">
        <resourceServiceProvider
            class="demo.lego.ui.LegoExecutableExtensionFactory:org.eclipse.xtext.ui.resource.IResourceUIServiceProvider"
            uriExtension="lego">
        </resourceServiceProvider>
    </extension>



	<!-- Quick Outline -->
	<extension
		point="org.eclipse.ui.handlers">
		<handler 
			class="demo.lego.ui.LegoExecutableExtensionFactory:org.eclipse.xtext.ui.editor.outline.quickoutline.ShowQuickOutlineActionHandler"
			commandId="org.eclipse.xtext.ui.editor.outline.QuickOutline">
			<activeWhen>
				<reference
					definitionId="demo.lego.Lego.Editor.opened">
				</reference>
			</activeWhen>
		</handler>
	</extension>
	<extension
		point="org.eclipse.ui.commands">
		<command
			description="Open the quick outline."
			id="org.eclipse.xtext.ui.editor.outline.QuickOutline"
			name="Quick Outline">
		</command>
	</extension>
	<extension point="org.eclipse.ui.menus">
		<menuContribution
			locationURI="popup:#TextEditorContext?after=group.open">
			<command commandId="org.eclipse.xtext.ui.editor.outline.QuickOutline"
				style="push"
				tooltip="Open Quick Outline">
				<visibleWhen checkEnabled="false">
					<reference definitionId="demo.lego.Lego.Editor.opened"/>
				</visibleWhen>
			</command>
		</menuContribution>
	</extension>

</plugin>
