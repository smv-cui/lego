/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Number Obj Var</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.NumberObjVar#getObj <em>Obj</em>}</li>
 *   <li>{@link demo.scratch.NumberObjVar#getVar <em>Var</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getNumberObjVar()
 * @model
 * @generated
 */
public interface NumberObjVar extends demo.scratch.Number
{
	/**
	 * Returns the value of the '<em><b>Obj</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Obj</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Obj</em>' containment reference.
	 * @see #setObj(IObject)
	 * @see demo.scratch.ScratchPackage#getNumberObjVar_Obj()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	IObject getObj();

	/**
	 * Sets the value of the '{@link demo.scratch.NumberObjVar#getObj <em>Obj</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Obj</em>' containment reference.
	 * @see #getObj()
	 * @generated
	 */
	void setObj(IObject value);

	/**
	 * Returns the value of the '<em><b>Var</b></em>' attribute.
	 * The literals are from the enumeration {@link demo.scratch.ObjectVariableEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var</em>' attribute.
	 * @see demo.scratch.ObjectVariableEnum
	 * @see #setVar(ObjectVariableEnum)
	 * @see demo.scratch.ScratchPackage#getNumberObjVar_Var()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ObjectVariableEnum getVar();

	/**
	 * Sets the value of the '{@link demo.scratch.NumberObjVar#getVar <em>Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var</em>' attribute.
	 * @see demo.scratch.ObjectVariableEnum
	 * @see #getVar()
	 * @generated
	 */
	void setVar(ObjectVariableEnum value);

} // NumberObjVar
