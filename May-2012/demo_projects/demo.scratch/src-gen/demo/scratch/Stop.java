/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stop</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.Stop#isAll <em>All</em>}</li>
 *   <li>{@link demo.scratch.Stop#isScript <em>Script</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getStop()
 * @model
 * @generated
 */
public interface Stop extends Control
{
	/**
	 * Returns the value of the '<em><b>All</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All</em>' attribute.
	 * @see #setAll(boolean)
	 * @see demo.scratch.ScratchPackage#getStop_All()
	 * @model default="false" required="true" ordered="false"
	 * @generated
	 */
	boolean isAll();

	/**
	 * Sets the value of the '{@link demo.scratch.Stop#isAll <em>All</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>All</em>' attribute.
	 * @see #isAll()
	 * @generated
	 */
	void setAll(boolean value);

	/**
	 * Returns the value of the '<em><b>Script</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Script</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Script</em>' attribute.
	 * @see #setScript(boolean)
	 * @see demo.scratch.ScratchPackage#getStop_Script()
	 * @model default="false" required="true" ordered="false"
	 * @generated
	 */
	boolean isScript();

	/**
	 * Sets the value of the '{@link demo.scratch.Stop#isScript <em>Script</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Script</em>' attribute.
	 * @see #isScript()
	 * @generated
	 */
	void setScript(boolean value);

} // Stop
