/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.ObjectRef#getObject <em>Object</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getObjectRef()
 * @model
 * @generated
 */
public interface ObjectRef extends IObject
{
	/**
	 * Returns the value of the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' reference.
	 * @see #setObject(demo.scratch.Object)
	 * @see demo.scratch.ScratchPackage#getObjectRef_Object()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	demo.scratch.Object getObject();

	/**
	 * Sets the value of the '{@link demo.scratch.ObjectRef#getObject <em>Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' reference.
	 * @see #getObject()
	 * @generated
	 */
	void setObject(demo.scratch.Object value);

} // ObjectRef
