/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Number Var Y</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see demo.scratch.ScratchPackage#getNumberVarY()
 * @model
 * @generated
 */
public interface NumberVarY extends demo.scratch.Number
{
} // NumberVarY
