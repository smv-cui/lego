/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Wait Delay</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.WaitDelay#getDelay <em>Delay</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getWaitDelay()
 * @model
 * @generated
 */
public interface WaitDelay extends Wait
{
	/**
	 * Returns the value of the '<em><b>Delay</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay</em>' containment reference.
	 * @see #setDelay(demo.scratch.Number)
	 * @see demo.scratch.ScratchPackage#getWaitDelay_Delay()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	demo.scratch.Number getDelay();

	/**
	 * Sets the value of the '{@link demo.scratch.WaitDelay#getDelay <em>Delay</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delay</em>' containment reference.
	 * @see #getDelay()
	 * @generated
	 */
	void setDelay(demo.scratch.Number value);

} // WaitDelay
