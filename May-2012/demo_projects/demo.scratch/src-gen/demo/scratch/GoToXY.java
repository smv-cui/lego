/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Go To XY</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.GoToXY#getX <em>X</em>}</li>
 *   <li>{@link demo.scratch.GoToXY#getY <em>Y</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getGoToXY()
 * @model
 * @generated
 */
public interface GoToXY extends Motion
{
	/**
	 * Returns the value of the '<em><b>X</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>X</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X</em>' containment reference.
	 * @see #setX(demo.scratch.Number)
	 * @see demo.scratch.ScratchPackage#getGoToXY_X()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	demo.scratch.Number getX();

	/**
	 * Sets the value of the '{@link demo.scratch.GoToXY#getX <em>X</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X</em>' containment reference.
	 * @see #getX()
	 * @generated
	 */
	void setX(demo.scratch.Number value);

	/**
	 * Returns the value of the '<em><b>Y</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Y</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Y</em>' containment reference.
	 * @see #setY(demo.scratch.Number)
	 * @see demo.scratch.ScratchPackage#getGoToXY_Y()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	demo.scratch.Number getY();

	/**
	 * Sets the value of the '{@link demo.scratch.GoToXY#getY <em>Y</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Y</em>' containment reference.
	 * @see #getY()
	 * @generated
	 */
	void setY(demo.scratch.Number value);

} // GoToXY
