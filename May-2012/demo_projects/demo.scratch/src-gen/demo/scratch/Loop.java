/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Loop</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.Loop#getBlocks <em>Blocks</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getLoop()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Loop extends Control
{
	/**
	 * Returns the value of the '<em><b>Blocks</b></em>' containment reference list.
	 * The list contents are of type {@link demo.scratch.Block}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Blocks</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Blocks</em>' containment reference list.
	 * @see demo.scratch.ScratchPackage#getLoop_Blocks()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Block> getBlocks();

} // Loop
