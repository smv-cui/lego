/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Repeat N</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.RepeatN#getN <em>N</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getRepeatN()
 * @model
 * @generated
 */
public interface RepeatN extends Loop
{
	/**
	 * Returns the value of the '<em><b>N</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>N</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>N</em>' containment reference.
	 * @see #setN(demo.scratch.Number)
	 * @see demo.scratch.ScratchPackage#getRepeatN_N()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	demo.scratch.Number getN();

	/**
	 * Sets the value of the '{@link demo.scratch.RepeatN#getN <em>N</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>N</em>' containment reference.
	 * @see #getN()
	 * @generated
	 */
	void setN(demo.scratch.Number value);

} // RepeatN
