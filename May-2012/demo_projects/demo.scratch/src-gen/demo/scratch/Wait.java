/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Wait</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see demo.scratch.ScratchPackage#getWait()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Wait extends Control
{
} // Wait
