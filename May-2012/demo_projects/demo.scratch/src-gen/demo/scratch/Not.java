/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Not</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.Not#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getNot()
 * @model
 * @generated
 */
public interface Not extends Binary
{
	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(Binary)
	 * @see demo.scratch.ScratchPackage#getNot_Value()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	Binary getValue();

	/**
	 * Sets the value of the '{@link demo.scratch.Not#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Binary value);

} // Not
