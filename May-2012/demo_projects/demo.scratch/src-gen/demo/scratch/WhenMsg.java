/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>When Msg</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.WhenMsg#getMsg <em>Msg</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getWhenMsg()
 * @model
 * @generated
 */
public interface WhenMsg extends When
{
	/**
	 * Returns the value of the '<em><b>Msg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Msg</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Msg</em>' containment reference.
	 * @see #setMsg(Message)
	 * @see demo.scratch.ScratchPackage#getWhenMsg_Msg()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	Message getMsg();

	/**
	 * Sets the value of the '{@link demo.scratch.WhenMsg#getMsg <em>Msg</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Msg</em>' containment reference.
	 * @see #getMsg()
	 * @generated
	 */
	void setMsg(Message value);

} // WhenMsg
