/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Touchs Color</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.TouchsColor#getColor <em>Color</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getTouchsColor()
 * @model
 * @generated
 */
public interface TouchsColor extends Binary
{
	/**
	 * Returns the value of the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Color</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' attribute.
	 * @see #setColor(String)
	 * @see demo.scratch.ScratchPackage#getTouchsColor_Color()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getColor();

	/**
	 * Sets the value of the '{@link demo.scratch.TouchsColor#getColor <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' attribute.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(String value);

} // TouchsColor
