/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Move</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.Move#getSteps <em>Steps</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getMove()
 * @model
 * @generated
 */
public interface Move extends Motion
{
	/**
	 * Returns the value of the '<em><b>Steps</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Steps</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Steps</em>' containment reference.
	 * @see #setSteps(demo.scratch.Number)
	 * @see demo.scratch.ScratchPackage#getMove_Steps()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	demo.scratch.Number getSteps();

	/**
	 * Sets the value of the '{@link demo.scratch.Move#getSteps <em>Steps</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Steps</em>' containment reference.
	 * @see #getSteps()
	 * @generated
	 */
	void setSteps(demo.scratch.Number value);

} // Move
