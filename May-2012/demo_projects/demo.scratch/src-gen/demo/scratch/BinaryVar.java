/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Var</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.BinaryVar#getVar <em>Var</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getBinaryVar()
 * @model
 * @generated
 */
public interface BinaryVar extends Binary
{
	/**
	 * Returns the value of the '<em><b>Var</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var</em>' attribute.
	 * @see #setVar(String)
	 * @see demo.scratch.ScratchPackage#getBinaryVar_Var()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getVar();

	/**
	 * Sets the value of the '{@link demo.scratch.BinaryVar#getVar <em>Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var</em>' attribute.
	 * @see #getVar()
	 * @generated
	 */
	void setVar(String value);

} // BinaryVar
