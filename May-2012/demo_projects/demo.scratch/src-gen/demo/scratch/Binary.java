/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see demo.scratch.ScratchPackage#getBinary()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Binary extends Operator
{
} // Binary
