/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Number Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.NumberValue#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getNumberValue()
 * @model
 * @generated
 */
public interface NumberValue extends demo.scratch.Number
{
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(float)
	 * @see demo.scratch.ScratchPackage#getNumberValue_Value()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	float getValue();

	/**
	 * Sets the value of the '{@link demo.scratch.NumberValue#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(float value);

} // NumberValue
