/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Predef</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.ObjectPredef#getObject <em>Object</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getObjectPredef()
 * @model
 * @generated
 */
public interface ObjectPredef extends IObject
{
	/**
	 * Returns the value of the '<em><b>Object</b></em>' attribute.
	 * The literals are from the enumeration {@link demo.scratch.ObjectPredefEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' attribute.
	 * @see demo.scratch.ObjectPredefEnum
	 * @see #setObject(ObjectPredefEnum)
	 * @see demo.scratch.ScratchPackage#getObjectPredef_Object()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ObjectPredefEnum getObject();

	/**
	 * Sets the value of the '{@link demo.scratch.ObjectPredef#getObject <em>Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' attribute.
	 * @see demo.scratch.ObjectPredefEnum
	 * @see #getObject()
	 * @generated
	 */
	void setObject(ObjectPredefEnum value);

} // ObjectPredef
