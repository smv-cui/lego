/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Broadcast</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.Broadcast#isWait <em>Wait</em>}</li>
 *   <li>{@link demo.scratch.Broadcast#getMsg <em>Msg</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getBroadcast()
 * @model
 * @generated
 */
public interface Broadcast extends Control
{
	/**
	 * Returns the value of the '<em><b>Wait</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wait</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wait</em>' attribute.
	 * @see #setWait(boolean)
	 * @see demo.scratch.ScratchPackage#getBroadcast_Wait()
	 * @model default="false" required="true" ordered="false"
	 * @generated
	 */
	boolean isWait();

	/**
	 * Sets the value of the '{@link demo.scratch.Broadcast#isWait <em>Wait</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wait</em>' attribute.
	 * @see #isWait()
	 * @generated
	 */
	void setWait(boolean value);

	/**
	 * Returns the value of the '<em><b>Msg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Msg</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Msg</em>' containment reference.
	 * @see #setMsg(Message)
	 * @see demo.scratch.ScratchPackage#getBroadcast_Msg()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	Message getMsg();

	/**
	 * Sets the value of the '{@link demo.scratch.Broadcast#getMsg <em>Msg</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Msg</em>' containment reference.
	 * @see #getMsg()
	 * @generated
	 */
	void setMsg(Message value);

} // Broadcast
