/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Number Var X</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see demo.scratch.ScratchPackage#getNumberVarX()
 * @model
 * @generated
 */
public interface NumberVarX extends demo.scratch.Number
{
} // NumberVarX
