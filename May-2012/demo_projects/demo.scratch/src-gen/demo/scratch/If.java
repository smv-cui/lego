/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.If#getCond <em>Cond</em>}</li>
 *   <li>{@link demo.scratch.If#getTrue <em>True</em>}</li>
 *   <li>{@link demo.scratch.If#getFalse <em>False</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getIf()
 * @model
 * @generated
 */
public interface If extends Control
{
	/**
	 * Returns the value of the '<em><b>Cond</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cond</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cond</em>' containment reference.
	 * @see #setCond(Binary)
	 * @see demo.scratch.ScratchPackage#getIf_Cond()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	Binary getCond();

	/**
	 * Sets the value of the '{@link demo.scratch.If#getCond <em>Cond</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cond</em>' containment reference.
	 * @see #getCond()
	 * @generated
	 */
	void setCond(Binary value);

	/**
	 * Returns the value of the '<em><b>True</b></em>' containment reference list.
	 * The list contents are of type {@link demo.scratch.Block}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>True</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>True</em>' containment reference list.
	 * @see demo.scratch.ScratchPackage#getIf_True()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Block> getTrue();

	/**
	 * Returns the value of the '<em><b>False</b></em>' containment reference list.
	 * The list contents are of type {@link demo.scratch.Block}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>False</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>False</em>' containment reference list.
	 * @see demo.scratch.ScratchPackage#getIf_False()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Block> getFalse();

} // If
