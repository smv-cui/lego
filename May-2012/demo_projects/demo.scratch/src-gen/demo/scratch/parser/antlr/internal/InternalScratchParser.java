package demo.scratch.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import demo.scratch.services.ScratchGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalScratchParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_NUM", "RULE_STRING", "RULE_WS", "RULE_ANY_OTHER", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "'project'", "'object'", "'script'", "'end'", "'when'", "'green flag clicked'", "'I receive'", "'forever'", "'if'", "'repeat'", "'until'", "'stop'", "'all'", "'wait'", "'secs'", "'broadcast'", "'and wait'", "'else'", "'move'", "'steps'", "'turn cw'", "'degrees'", "'turn ccw'", "'point in direction'", "'point towards'", "'go to'", "'x:'", "'y:'", "'glide'", "'secs to x:'", "'change x by'", "'change y by'", "'set x to'", "'set y to'", "'if on edge, bounce'", "'('", "')'", "'$'", "'x position'", "'y position'", "'direction'", "'of'", "'touching'", "'color'", "'?'", "'<'", "'>'", "'='", "'and'", "'or'", "'not'", "'hide'", "'show'", "'say'", "'for'", "'play'", "'sound'", "'mouse-pointer'"
    };
    public static final int T__68=68;
    public static final int RULE_ID=4;
    public static final int T__66=66;
    public static final int T__67=67;
    public static final int T__64=64;
    public static final int T__29=29;
    public static final int T__65=65;
    public static final int T__28=28;
    public static final int T__62=62;
    public static final int T__27=27;
    public static final int T__63=63;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=8;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int T__61=61;
    public static final int T__60=60;
    public static final int EOF=-1;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__19=19;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__16=16;
    public static final int T__52=52;
    public static final int T__15=15;
    public static final int T__53=53;
    public static final int T__18=18;
    public static final int T__54=54;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int T__59=59;
    public static final int T__50=50;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_SL_COMMENT=10;
    public static final int RULE_ML_COMMENT=9;
    public static final int RULE_NUM=5;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_STRING=6;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_WS=7;

    // delegates
    // delegators


        public InternalScratchParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalScratchParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalScratchParser.tokenNames; }
    public String getGrammarFileName() { return "../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g"; }



    /*
      This grammar contains a lot of empty actions to work around a bug in ANTLR.
      Otherwise the ANTLR tool will create synpreds that cannot be compiled in some rare cases.
    */
     
     	private ScratchGrammarAccess grammarAccess;
     	
        public InternalScratchParser(TokenStream input, ScratchGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Project";	
       	}
       	
       	@Override
       	protected ScratchGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleProject"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:74:1: entryRuleProject returns [EObject current=null] : iv_ruleProject= ruleProject EOF ;
    public final EObject entryRuleProject() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProject = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:75:2: (iv_ruleProject= ruleProject EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:76:2: iv_ruleProject= ruleProject EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getProjectRule()); 
            }
            pushFollow(FOLLOW_ruleProject_in_entryRuleProject81);
            iv_ruleProject=ruleProject();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleProject; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleProject91); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProject"


    // $ANTLR start "ruleProject"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:83:1: ruleProject returns [EObject current=null] : (otherlv_0= 'project' ( (lv_name_1_0= RULE_ID ) ) ( (lv_objects_2_0= ruleObject ) )* ) ;
    public final EObject ruleProject() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        EObject lv_objects_2_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:86:28: ( (otherlv_0= 'project' ( (lv_name_1_0= RULE_ID ) ) ( (lv_objects_2_0= ruleObject ) )* ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:87:1: (otherlv_0= 'project' ( (lv_name_1_0= RULE_ID ) ) ( (lv_objects_2_0= ruleObject ) )* )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:87:1: (otherlv_0= 'project' ( (lv_name_1_0= RULE_ID ) ) ( (lv_objects_2_0= ruleObject ) )* )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:87:3: otherlv_0= 'project' ( (lv_name_1_0= RULE_ID ) ) ( (lv_objects_2_0= ruleObject ) )*
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_ruleProject128); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getProjectAccess().getProjectKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:91:1: ( (lv_name_1_0= RULE_ID ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:92:1: (lv_name_1_0= RULE_ID )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:92:1: (lv_name_1_0= RULE_ID )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:93:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleProject145); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getProjectAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getProjectRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:109:2: ( (lv_objects_2_0= ruleObject ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==12) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:110:1: (lv_objects_2_0= ruleObject )
            	    {
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:110:1: (lv_objects_2_0= ruleObject )
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:111:3: lv_objects_2_0= ruleObject
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getProjectAccess().getObjectsObjectParserRuleCall_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleObject_in_ruleProject171);
            	    lv_objects_2_0=ruleObject();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getProjectRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"objects",
            	              		lv_objects_2_0, 
            	              		"Object");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProject"


    // $ANTLR start "entryRuleObject"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:135:1: entryRuleObject returns [EObject current=null] : iv_ruleObject= ruleObject EOF ;
    public final EObject entryRuleObject() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleObject = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:136:2: (iv_ruleObject= ruleObject EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:137:2: iv_ruleObject= ruleObject EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getObjectRule()); 
            }
            pushFollow(FOLLOW_ruleObject_in_entryRuleObject208);
            iv_ruleObject=ruleObject();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleObject; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleObject218); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleObject"


    // $ANTLR start "ruleObject"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:144:1: ruleObject returns [EObject current=null] : (otherlv_0= 'object' ( (lv_name_1_0= RULE_ID ) ) ( (lv_scripts_2_0= ruleScript ) )* ) ;
    public final EObject ruleObject() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        EObject lv_scripts_2_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:147:28: ( (otherlv_0= 'object' ( (lv_name_1_0= RULE_ID ) ) ( (lv_scripts_2_0= ruleScript ) )* ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:148:1: (otherlv_0= 'object' ( (lv_name_1_0= RULE_ID ) ) ( (lv_scripts_2_0= ruleScript ) )* )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:148:1: (otherlv_0= 'object' ( (lv_name_1_0= RULE_ID ) ) ( (lv_scripts_2_0= ruleScript ) )* )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:148:3: otherlv_0= 'object' ( (lv_name_1_0= RULE_ID ) ) ( (lv_scripts_2_0= ruleScript ) )*
            {
            otherlv_0=(Token)match(input,12,FOLLOW_12_in_ruleObject255); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getObjectAccess().getObjectKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:152:1: ( (lv_name_1_0= RULE_ID ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:153:1: (lv_name_1_0= RULE_ID )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:153:1: (lv_name_1_0= RULE_ID )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:154:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleObject272); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_1_0, grammarAccess.getObjectAccess().getNameIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getObjectRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_1_0, 
                      		"ID");
              	    
            }

            }


            }

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:170:2: ( (lv_scripts_2_0= ruleScript ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==13) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:171:1: (lv_scripts_2_0= ruleScript )
            	    {
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:171:1: (lv_scripts_2_0= ruleScript )
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:172:3: lv_scripts_2_0= ruleScript
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getObjectAccess().getScriptsScriptParserRuleCall_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleScript_in_ruleObject298);
            	    lv_scripts_2_0=ruleScript();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getObjectRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"scripts",
            	              		lv_scripts_2_0, 
            	              		"Script");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObject"


    // $ANTLR start "entryRuleIObject"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:196:1: entryRuleIObject returns [EObject current=null] : iv_ruleIObject= ruleIObject EOF ;
    public final EObject entryRuleIObject() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIObject = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:197:2: (iv_ruleIObject= ruleIObject EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:198:2: iv_ruleIObject= ruleIObject EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIObjectRule()); 
            }
            pushFollow(FOLLOW_ruleIObject_in_entryRuleIObject335);
            iv_ruleIObject=ruleIObject();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIObject; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIObject345); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIObject"


    // $ANTLR start "ruleIObject"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:205:1: ruleIObject returns [EObject current=null] : (this_Ref_0= ruleRef | this_Predef_1= rulePredef ) ;
    public final EObject ruleIObject() throws RecognitionException {
        EObject current = null;

        EObject this_Ref_0 = null;

        EObject this_Predef_1 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:208:28: ( (this_Ref_0= ruleRef | this_Predef_1= rulePredef ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:209:1: (this_Ref_0= ruleRef | this_Predef_1= rulePredef )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:209:1: (this_Ref_0= ruleRef | this_Predef_1= rulePredef )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID) ) {
                alt3=1;
            }
            else if ( (LA3_0==68) ) {
                alt3=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:210:2: this_Ref_0= ruleRef
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getIObjectAccess().getRefParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleRef_in_ruleIObject395);
                    this_Ref_0=ruleRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Ref_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:223:2: this_Predef_1= rulePredef
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getIObjectAccess().getPredefParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_rulePredef_in_ruleIObject425);
                    this_Predef_1=rulePredef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Predef_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIObject"


    // $ANTLR start "entryRuleRef"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:242:1: entryRuleRef returns [EObject current=null] : iv_ruleRef= ruleRef EOF ;
    public final EObject entryRuleRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRef = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:243:2: (iv_ruleRef= ruleRef EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:244:2: iv_ruleRef= ruleRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRefRule()); 
            }
            pushFollow(FOLLOW_ruleRef_in_entryRuleRef460);
            iv_ruleRef=ruleRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRef; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleRef470); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRef"


    // $ANTLR start "ruleRef"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:251:1: ruleRef returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:254:28: ( ( (otherlv_0= RULE_ID ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:255:1: ( (otherlv_0= RULE_ID ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:255:1: ( (otherlv_0= RULE_ID ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:256:1: (otherlv_0= RULE_ID )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:256:1: (otherlv_0= RULE_ID )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:257:3: otherlv_0= RULE_ID
            {
            if ( state.backtracking==0 ) {
               
              		  /* */ 
              		
            }
            if ( state.backtracking==0 ) {

              			if (current==null) {
              	            current = createModelElement(grammarAccess.getRefRule());
              	        }
                      
            }
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRef518); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              		newLeafNode(otherlv_0, grammarAccess.getRefAccess().getObjectObjectCrossReference_0()); 
              	
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRef"


    // $ANTLR start "entryRulePredef"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:279:1: entryRulePredef returns [EObject current=null] : iv_rulePredef= rulePredef EOF ;
    public final EObject entryRulePredef() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePredef = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:280:2: (iv_rulePredef= rulePredef EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:281:2: iv_rulePredef= rulePredef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPredefRule()); 
            }
            pushFollow(FOLLOW_rulePredef_in_entryRulePredef553);
            iv_rulePredef=rulePredef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePredef; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePredef563); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePredef"


    // $ANTLR start "rulePredef"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:288:1: rulePredef returns [EObject current=null] : ( (lv_object_0_0= ruleObjectPredefEnum ) ) ;
    public final EObject rulePredef() throws RecognitionException {
        EObject current = null;

        Enumerator lv_object_0_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:291:28: ( ( (lv_object_0_0= ruleObjectPredefEnum ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:292:1: ( (lv_object_0_0= ruleObjectPredefEnum ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:292:1: ( (lv_object_0_0= ruleObjectPredefEnum ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:293:1: (lv_object_0_0= ruleObjectPredefEnum )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:293:1: (lv_object_0_0= ruleObjectPredefEnum )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:294:3: lv_object_0_0= ruleObjectPredefEnum
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getPredefAccess().getObjectObjectPredefEnumEnumRuleCall_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleObjectPredefEnum_in_rulePredef608);
            lv_object_0_0=ruleObjectPredefEnum();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getPredefRule());
              	        }
                     		set(
                     			current, 
                     			"object",
                      		lv_object_0_0, 
                      		"ObjectPredefEnum");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePredef"


    // $ANTLR start "entryRuleScript"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:318:1: entryRuleScript returns [EObject current=null] : iv_ruleScript= ruleScript EOF ;
    public final EObject entryRuleScript() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleScript = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:319:2: (iv_ruleScript= ruleScript EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:320:2: iv_ruleScript= ruleScript EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getScriptRule()); 
            }
            pushFollow(FOLLOW_ruleScript_in_entryRuleScript643);
            iv_ruleScript=ruleScript();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleScript; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleScript653); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleScript"


    // $ANTLR start "ruleScript"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:327:1: ruleScript returns [EObject current=null] : (otherlv_0= 'script' () ( (lv_when_2_0= ruleWhen ) )? ( (lv_blocks_3_0= ruleBlock ) )* otherlv_4= 'end' ) ;
    public final EObject ruleScript() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_4=null;
        EObject lv_when_2_0 = null;

        EObject lv_blocks_3_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:330:28: ( (otherlv_0= 'script' () ( (lv_when_2_0= ruleWhen ) )? ( (lv_blocks_3_0= ruleBlock ) )* otherlv_4= 'end' ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:331:1: (otherlv_0= 'script' () ( (lv_when_2_0= ruleWhen ) )? ( (lv_blocks_3_0= ruleBlock ) )* otherlv_4= 'end' )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:331:1: (otherlv_0= 'script' () ( (lv_when_2_0= ruleWhen ) )? ( (lv_blocks_3_0= ruleBlock ) )* otherlv_4= 'end' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:331:3: otherlv_0= 'script' () ( (lv_when_2_0= ruleWhen ) )? ( (lv_blocks_3_0= ruleBlock ) )* otherlv_4= 'end'
            {
            otherlv_0=(Token)match(input,13,FOLLOW_13_in_ruleScript690); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getScriptAccess().getScriptKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:335:1: ()
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:336:2: 
            {
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getScriptAccess().getScriptAction_1(),
                          current);
                  
            }

            }

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:344:2: ( (lv_when_2_0= ruleWhen ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==15) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:345:1: (lv_when_2_0= ruleWhen )
                    {
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:345:1: (lv_when_2_0= ruleWhen )
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:346:3: lv_when_2_0= ruleWhen
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getScriptAccess().getWhenWhenParserRuleCall_2_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleWhen_in_ruleScript723);
                    lv_when_2_0=ruleWhen();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getScriptRule());
                      	        }
                             		set(
                             			current, 
                             			"when",
                              		lv_when_2_0, 
                              		"When");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }
                    break;

            }

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:362:3: ( (lv_blocks_3_0= ruleBlock ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>=18 && LA5_0<=20)||LA5_0==22||LA5_0==24||LA5_0==26||LA5_0==29||LA5_0==31||(LA5_0>=33 && LA5_0<=36)||LA5_0==39||(LA5_0>=41 && LA5_0<=45)||(LA5_0>=62 && LA5_0<=64)||LA5_0==66) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:363:1: (lv_blocks_3_0= ruleBlock )
            	    {
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:363:1: (lv_blocks_3_0= ruleBlock )
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:364:3: lv_blocks_3_0= ruleBlock
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getScriptAccess().getBlocksBlockParserRuleCall_3_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleBlock_in_ruleScript745);
            	    lv_blocks_3_0=ruleBlock();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getScriptRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"blocks",
            	              		lv_blocks_3_0, 
            	              		"Block");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_4=(Token)match(input,14,FOLLOW_14_in_ruleScript758); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getScriptAccess().getEndKeyword_4());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScript"


    // $ANTLR start "entryRuleWhen"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:392:1: entryRuleWhen returns [EObject current=null] : iv_ruleWhen= ruleWhen EOF ;
    public final EObject entryRuleWhen() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWhen = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:393:2: (iv_ruleWhen= ruleWhen EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:394:2: iv_ruleWhen= ruleWhen EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWhenRule()); 
            }
            pushFollow(FOLLOW_ruleWhen_in_entryRuleWhen794);
            iv_ruleWhen=ruleWhen();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWhen; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleWhen804); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWhen"


    // $ANTLR start "ruleWhen"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:401:1: ruleWhen returns [EObject current=null] : (otherlv_0= 'when' (this_WhenFlag_1= ruleWhenFlag | this_WhenMsg_2= ruleWhenMsg ) ) ;
    public final EObject ruleWhen() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject this_WhenFlag_1 = null;

        EObject this_WhenMsg_2 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:404:28: ( (otherlv_0= 'when' (this_WhenFlag_1= ruleWhenFlag | this_WhenMsg_2= ruleWhenMsg ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:405:1: (otherlv_0= 'when' (this_WhenFlag_1= ruleWhenFlag | this_WhenMsg_2= ruleWhenMsg ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:405:1: (otherlv_0= 'when' (this_WhenFlag_1= ruleWhenFlag | this_WhenMsg_2= ruleWhenMsg ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:405:3: otherlv_0= 'when' (this_WhenFlag_1= ruleWhenFlag | this_WhenMsg_2= ruleWhenMsg )
            {
            otherlv_0=(Token)match(input,15,FOLLOW_15_in_ruleWhen841); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getWhenAccess().getWhenKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:409:1: (this_WhenFlag_1= ruleWhenFlag | this_WhenMsg_2= ruleWhenMsg )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==16) ) {
                alt6=1;
            }
            else if ( (LA6_0==17) ) {
                alt6=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:410:2: this_WhenFlag_1= ruleWhenFlag
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getWhenAccess().getWhenFlagParserRuleCall_1_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleWhenFlag_in_ruleWhen867);
                    this_WhenFlag_1=ruleWhenFlag();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_WhenFlag_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:423:2: this_WhenMsg_2= ruleWhenMsg
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getWhenAccess().getWhenMsgParserRuleCall_1_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleWhenMsg_in_ruleWhen897);
                    this_WhenMsg_2=ruleWhenMsg();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_WhenMsg_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWhen"


    // $ANTLR start "entryRuleWhenFlag"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:442:1: entryRuleWhenFlag returns [EObject current=null] : iv_ruleWhenFlag= ruleWhenFlag EOF ;
    public final EObject entryRuleWhenFlag() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWhenFlag = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:443:2: (iv_ruleWhenFlag= ruleWhenFlag EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:444:2: iv_ruleWhenFlag= ruleWhenFlag EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWhenFlagRule()); 
            }
            pushFollow(FOLLOW_ruleWhenFlag_in_entryRuleWhenFlag933);
            iv_ruleWhenFlag=ruleWhenFlag();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWhenFlag; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleWhenFlag943); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWhenFlag"


    // $ANTLR start "ruleWhenFlag"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:451:1: ruleWhenFlag returns [EObject current=null] : (otherlv_0= 'green flag clicked' () ) ;
    public final EObject ruleWhenFlag() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:454:28: ( (otherlv_0= 'green flag clicked' () ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:455:1: (otherlv_0= 'green flag clicked' () )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:455:1: (otherlv_0= 'green flag clicked' () )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:455:3: otherlv_0= 'green flag clicked' ()
            {
            otherlv_0=(Token)match(input,16,FOLLOW_16_in_ruleWhenFlag980); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getWhenFlagAccess().getGreenFlagClickedKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:459:1: ()
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:460:2: 
            {
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getWhenFlagAccess().getWhenFlagAction_1(),
                          current);
                  
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWhenFlag"


    // $ANTLR start "entryRuleWhenMsg"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:476:1: entryRuleWhenMsg returns [EObject current=null] : iv_ruleWhenMsg= ruleWhenMsg EOF ;
    public final EObject entryRuleWhenMsg() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWhenMsg = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:477:2: (iv_ruleWhenMsg= ruleWhenMsg EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:478:2: iv_ruleWhenMsg= ruleWhenMsg EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWhenMsgRule()); 
            }
            pushFollow(FOLLOW_ruleWhenMsg_in_entryRuleWhenMsg1028);
            iv_ruleWhenMsg=ruleWhenMsg();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWhenMsg; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleWhenMsg1038); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWhenMsg"


    // $ANTLR start "ruleWhenMsg"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:485:1: ruleWhenMsg returns [EObject current=null] : (otherlv_0= 'I receive' ( (lv_msg_1_0= ruleMessage ) ) ) ;
    public final EObject ruleWhenMsg() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_msg_1_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:488:28: ( (otherlv_0= 'I receive' ( (lv_msg_1_0= ruleMessage ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:489:1: (otherlv_0= 'I receive' ( (lv_msg_1_0= ruleMessage ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:489:1: (otherlv_0= 'I receive' ( (lv_msg_1_0= ruleMessage ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:489:3: otherlv_0= 'I receive' ( (lv_msg_1_0= ruleMessage ) )
            {
            otherlv_0=(Token)match(input,17,FOLLOW_17_in_ruleWhenMsg1075); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getWhenMsgAccess().getIReceiveKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:493:1: ( (lv_msg_1_0= ruleMessage ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:494:1: (lv_msg_1_0= ruleMessage )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:494:1: (lv_msg_1_0= ruleMessage )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:495:3: lv_msg_1_0= ruleMessage
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getWhenMsgAccess().getMsgMessageParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleMessage_in_ruleWhenMsg1096);
            lv_msg_1_0=ruleMessage();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getWhenMsgRule());
              	        }
                     		set(
                     			current, 
                     			"msg",
                      		lv_msg_1_0, 
                      		"Message");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWhenMsg"


    // $ANTLR start "entryRuleMessage"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:519:1: entryRuleMessage returns [EObject current=null] : iv_ruleMessage= ruleMessage EOF ;
    public final EObject entryRuleMessage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMessage = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:520:2: (iv_ruleMessage= ruleMessage EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:521:2: iv_ruleMessage= ruleMessage EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMessageRule()); 
            }
            pushFollow(FOLLOW_ruleMessage_in_entryRuleMessage1132);
            iv_ruleMessage=ruleMessage();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMessage; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMessage1142); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMessage"


    // $ANTLR start "ruleMessage"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:528:1: ruleMessage returns [EObject current=null] : ( (lv_name_0_0= RULE_ID ) ) ;
    public final EObject ruleMessage() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;

         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:531:28: ( ( (lv_name_0_0= RULE_ID ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:532:1: ( (lv_name_0_0= RULE_ID ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:532:1: ( (lv_name_0_0= RULE_ID ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:533:1: (lv_name_0_0= RULE_ID )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:533:1: (lv_name_0_0= RULE_ID )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:534:3: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleMessage1183); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_name_0_0, grammarAccess.getMessageAccess().getNameIDTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getMessageRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"name",
                      		lv_name_0_0, 
                      		"ID");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMessage"


    // $ANTLR start "entryRuleBlock"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:558:1: entryRuleBlock returns [EObject current=null] : iv_ruleBlock= ruleBlock EOF ;
    public final EObject entryRuleBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBlock = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:559:2: (iv_ruleBlock= ruleBlock EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:560:2: iv_ruleBlock= ruleBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBlockRule()); 
            }
            pushFollow(FOLLOW_ruleBlock_in_entryRuleBlock1223);
            iv_ruleBlock=ruleBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBlock; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBlock1233); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBlock"


    // $ANTLR start "ruleBlock"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:567:1: ruleBlock returns [EObject current=null] : (this_Stop_0= ruleStop | this_Broadcast_1= ruleBroadcast | this_Wait_2= ruleWait | this_If_3= ruleIf | this_Loop_4= ruleLoop | this_Move_5= ruleMove | this_TurnCW_6= ruleTurnCW | this_TurnCCW_7= ruleTurnCCW | this_PointDirection_8= rulePointDirection | this_PointTowards_9= rulePointTowards | this_GoToXY_10= ruleGoToXY | this_GoTo_11= ruleGoTo | this_Glide_12= ruleGlide | this_ChangeX_13= ruleChangeX | this_ChangeY_14= ruleChangeY | this_SetX_15= ruleSetX | this_SetY_16= ruleSetY | this_IfEdge_17= ruleIfEdge | this_Hide_18= ruleHide | this_Show_19= ruleShow | this_Say_20= ruleSay | this_Play_21= rulePlay ) ;
    public final EObject ruleBlock() throws RecognitionException {
        EObject current = null;

        EObject this_Stop_0 = null;

        EObject this_Broadcast_1 = null;

        EObject this_Wait_2 = null;

        EObject this_If_3 = null;

        EObject this_Loop_4 = null;

        EObject this_Move_5 = null;

        EObject this_TurnCW_6 = null;

        EObject this_TurnCCW_7 = null;

        EObject this_PointDirection_8 = null;

        EObject this_PointTowards_9 = null;

        EObject this_GoToXY_10 = null;

        EObject this_GoTo_11 = null;

        EObject this_Glide_12 = null;

        EObject this_ChangeX_13 = null;

        EObject this_ChangeY_14 = null;

        EObject this_SetX_15 = null;

        EObject this_SetY_16 = null;

        EObject this_IfEdge_17 = null;

        EObject this_Hide_18 = null;

        EObject this_Show_19 = null;

        EObject this_Say_20 = null;

        EObject this_Play_21 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:570:28: ( (this_Stop_0= ruleStop | this_Broadcast_1= ruleBroadcast | this_Wait_2= ruleWait | this_If_3= ruleIf | this_Loop_4= ruleLoop | this_Move_5= ruleMove | this_TurnCW_6= ruleTurnCW | this_TurnCCW_7= ruleTurnCCW | this_PointDirection_8= rulePointDirection | this_PointTowards_9= rulePointTowards | this_GoToXY_10= ruleGoToXY | this_GoTo_11= ruleGoTo | this_Glide_12= ruleGlide | this_ChangeX_13= ruleChangeX | this_ChangeY_14= ruleChangeY | this_SetX_15= ruleSetX | this_SetY_16= ruleSetY | this_IfEdge_17= ruleIfEdge | this_Hide_18= ruleHide | this_Show_19= ruleShow | this_Say_20= ruleSay | this_Play_21= rulePlay ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:571:1: (this_Stop_0= ruleStop | this_Broadcast_1= ruleBroadcast | this_Wait_2= ruleWait | this_If_3= ruleIf | this_Loop_4= ruleLoop | this_Move_5= ruleMove | this_TurnCW_6= ruleTurnCW | this_TurnCCW_7= ruleTurnCCW | this_PointDirection_8= rulePointDirection | this_PointTowards_9= rulePointTowards | this_GoToXY_10= ruleGoToXY | this_GoTo_11= ruleGoTo | this_Glide_12= ruleGlide | this_ChangeX_13= ruleChangeX | this_ChangeY_14= ruleChangeY | this_SetX_15= ruleSetX | this_SetY_16= ruleSetY | this_IfEdge_17= ruleIfEdge | this_Hide_18= ruleHide | this_Show_19= ruleShow | this_Say_20= ruleSay | this_Play_21= rulePlay )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:571:1: (this_Stop_0= ruleStop | this_Broadcast_1= ruleBroadcast | this_Wait_2= ruleWait | this_If_3= ruleIf | this_Loop_4= ruleLoop | this_Move_5= ruleMove | this_TurnCW_6= ruleTurnCW | this_TurnCCW_7= ruleTurnCCW | this_PointDirection_8= rulePointDirection | this_PointTowards_9= rulePointTowards | this_GoToXY_10= ruleGoToXY | this_GoTo_11= ruleGoTo | this_Glide_12= ruleGlide | this_ChangeX_13= ruleChangeX | this_ChangeY_14= ruleChangeY | this_SetX_15= ruleSetX | this_SetY_16= ruleSetY | this_IfEdge_17= ruleIfEdge | this_Hide_18= ruleHide | this_Show_19= ruleShow | this_Say_20= ruleSay | this_Play_21= rulePlay )
            int alt7=22;
            alt7 = dfa7.predict(input);
            switch (alt7) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:572:2: this_Stop_0= ruleStop
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getStopParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleStop_in_ruleBlock1283);
                    this_Stop_0=ruleStop();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Stop_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:585:2: this_Broadcast_1= ruleBroadcast
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getBroadcastParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleBroadcast_in_ruleBlock1313);
                    this_Broadcast_1=ruleBroadcast();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Broadcast_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:598:2: this_Wait_2= ruleWait
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getWaitParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleWait_in_ruleBlock1343);
                    this_Wait_2=ruleWait();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Wait_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:611:2: this_If_3= ruleIf
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getIfParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIf_in_ruleBlock1373);
                    this_If_3=ruleIf();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_If_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:624:2: this_Loop_4= ruleLoop
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getLoopParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_ruleLoop_in_ruleBlock1403);
                    this_Loop_4=ruleLoop();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Loop_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:637:2: this_Move_5= ruleMove
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getMoveParserRuleCall_5()); 
                          
                    }
                    pushFollow(FOLLOW_ruleMove_in_ruleBlock1433);
                    this_Move_5=ruleMove();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Move_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 7 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:650:2: this_TurnCW_6= ruleTurnCW
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getTurnCWParserRuleCall_6()); 
                          
                    }
                    pushFollow(FOLLOW_ruleTurnCW_in_ruleBlock1463);
                    this_TurnCW_6=ruleTurnCW();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TurnCW_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 8 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:663:2: this_TurnCCW_7= ruleTurnCCW
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getTurnCCWParserRuleCall_7()); 
                          
                    }
                    pushFollow(FOLLOW_ruleTurnCCW_in_ruleBlock1493);
                    this_TurnCCW_7=ruleTurnCCW();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TurnCCW_7; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 9 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:676:2: this_PointDirection_8= rulePointDirection
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getPointDirectionParserRuleCall_8()); 
                          
                    }
                    pushFollow(FOLLOW_rulePointDirection_in_ruleBlock1523);
                    this_PointDirection_8=rulePointDirection();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_PointDirection_8; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 10 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:689:2: this_PointTowards_9= rulePointTowards
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getPointTowardsParserRuleCall_9()); 
                          
                    }
                    pushFollow(FOLLOW_rulePointTowards_in_ruleBlock1553);
                    this_PointTowards_9=rulePointTowards();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_PointTowards_9; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 11 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:702:2: this_GoToXY_10= ruleGoToXY
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getGoToXYParserRuleCall_10()); 
                          
                    }
                    pushFollow(FOLLOW_ruleGoToXY_in_ruleBlock1583);
                    this_GoToXY_10=ruleGoToXY();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_GoToXY_10; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 12 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:715:2: this_GoTo_11= ruleGoTo
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getGoToParserRuleCall_11()); 
                          
                    }
                    pushFollow(FOLLOW_ruleGoTo_in_ruleBlock1613);
                    this_GoTo_11=ruleGoTo();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_GoTo_11; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 13 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:728:2: this_Glide_12= ruleGlide
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getGlideParserRuleCall_12()); 
                          
                    }
                    pushFollow(FOLLOW_ruleGlide_in_ruleBlock1643);
                    this_Glide_12=ruleGlide();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Glide_12; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 14 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:741:2: this_ChangeX_13= ruleChangeX
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getChangeXParserRuleCall_13()); 
                          
                    }
                    pushFollow(FOLLOW_ruleChangeX_in_ruleBlock1673);
                    this_ChangeX_13=ruleChangeX();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ChangeX_13; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 15 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:754:2: this_ChangeY_14= ruleChangeY
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getChangeYParserRuleCall_14()); 
                          
                    }
                    pushFollow(FOLLOW_ruleChangeY_in_ruleBlock1703);
                    this_ChangeY_14=ruleChangeY();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_ChangeY_14; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 16 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:767:2: this_SetX_15= ruleSetX
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getSetXParserRuleCall_15()); 
                          
                    }
                    pushFollow(FOLLOW_ruleSetX_in_ruleBlock1733);
                    this_SetX_15=ruleSetX();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_SetX_15; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 17 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:780:2: this_SetY_16= ruleSetY
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getSetYParserRuleCall_16()); 
                          
                    }
                    pushFollow(FOLLOW_ruleSetY_in_ruleBlock1763);
                    this_SetY_16=ruleSetY();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_SetY_16; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 18 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:793:2: this_IfEdge_17= ruleIfEdge
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getIfEdgeParserRuleCall_17()); 
                          
                    }
                    pushFollow(FOLLOW_ruleIfEdge_in_ruleBlock1793);
                    this_IfEdge_17=ruleIfEdge();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_IfEdge_17; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 19 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:806:2: this_Hide_18= ruleHide
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getHideParserRuleCall_18()); 
                          
                    }
                    pushFollow(FOLLOW_ruleHide_in_ruleBlock1823);
                    this_Hide_18=ruleHide();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Hide_18; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 20 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:819:2: this_Show_19= ruleShow
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getShowParserRuleCall_19()); 
                          
                    }
                    pushFollow(FOLLOW_ruleShow_in_ruleBlock1853);
                    this_Show_19=ruleShow();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Show_19; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 21 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:832:2: this_Say_20= ruleSay
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getSayParserRuleCall_20()); 
                          
                    }
                    pushFollow(FOLLOW_ruleSay_in_ruleBlock1883);
                    this_Say_20=ruleSay();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Say_20; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 22 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:845:2: this_Play_21= rulePlay
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBlockAccess().getPlayParserRuleCall_21()); 
                          
                    }
                    pushFollow(FOLLOW_rulePlay_in_ruleBlock1913);
                    this_Play_21=rulePlay();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Play_21; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBlock"


    // $ANTLR start "entryRuleLoop"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:864:1: entryRuleLoop returns [EObject current=null] : iv_ruleLoop= ruleLoop EOF ;
    public final EObject entryRuleLoop() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLoop = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:865:2: (iv_ruleLoop= ruleLoop EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:866:2: iv_ruleLoop= ruleLoop EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLoopRule()); 
            }
            pushFollow(FOLLOW_ruleLoop_in_entryRuleLoop1948);
            iv_ruleLoop=ruleLoop();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLoop; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLoop1958); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLoop"


    // $ANTLR start "ruleLoop"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:873:1: ruleLoop returns [EObject current=null] : ( (this_Forever_0= ruleForever | this_RepeatN_1= ruleRepeatN | this_RepeatUntil_2= ruleRepeatUntil ) ( (lv_blocks_3_0= ruleBlock ) )* otherlv_4= 'end' ) ;
    public final EObject ruleLoop() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;
        EObject this_Forever_0 = null;

        EObject this_RepeatN_1 = null;

        EObject this_RepeatUntil_2 = null;

        EObject lv_blocks_3_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:876:28: ( ( (this_Forever_0= ruleForever | this_RepeatN_1= ruleRepeatN | this_RepeatUntil_2= ruleRepeatUntil ) ( (lv_blocks_3_0= ruleBlock ) )* otherlv_4= 'end' ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:877:1: ( (this_Forever_0= ruleForever | this_RepeatN_1= ruleRepeatN | this_RepeatUntil_2= ruleRepeatUntil ) ( (lv_blocks_3_0= ruleBlock ) )* otherlv_4= 'end' )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:877:1: ( (this_Forever_0= ruleForever | this_RepeatN_1= ruleRepeatN | this_RepeatUntil_2= ruleRepeatUntil ) ( (lv_blocks_3_0= ruleBlock ) )* otherlv_4= 'end' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:877:2: (this_Forever_0= ruleForever | this_RepeatN_1= ruleRepeatN | this_RepeatUntil_2= ruleRepeatUntil ) ( (lv_blocks_3_0= ruleBlock ) )* otherlv_4= 'end'
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:877:2: (this_Forever_0= ruleForever | this_RepeatN_1= ruleRepeatN | this_RepeatUntil_2= ruleRepeatUntil )
            int alt8=3;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==18) ) {
                alt8=1;
            }
            else if ( (LA8_0==20) ) {
                int LA8_2 = input.LA(2);

                if ( (LA8_2==46) ) {
                    alt8=2;
                }
                else if ( (LA8_2==21) ) {
                    alt8=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 8, 2, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:878:2: this_Forever_0= ruleForever
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLoopAccess().getForeverParserRuleCall_0_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleForever_in_ruleLoop2009);
                    this_Forever_0=ruleForever();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Forever_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:891:2: this_RepeatN_1= ruleRepeatN
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLoopAccess().getRepeatNParserRuleCall_0_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleRepeatN_in_ruleLoop2039);
                    this_RepeatN_1=ruleRepeatN();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_RepeatN_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:904:2: this_RepeatUntil_2= ruleRepeatUntil
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getLoopAccess().getRepeatUntilParserRuleCall_0_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleRepeatUntil_in_ruleLoop2069);
                    this_RepeatUntil_2=ruleRepeatUntil();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_RepeatUntil_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:915:2: ( (lv_blocks_3_0= ruleBlock ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>=18 && LA9_0<=20)||LA9_0==22||LA9_0==24||LA9_0==26||LA9_0==29||LA9_0==31||(LA9_0>=33 && LA9_0<=36)||LA9_0==39||(LA9_0>=41 && LA9_0<=45)||(LA9_0>=62 && LA9_0<=64)||LA9_0==66) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:916:1: (lv_blocks_3_0= ruleBlock )
            	    {
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:916:1: (lv_blocks_3_0= ruleBlock )
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:917:3: lv_blocks_3_0= ruleBlock
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getLoopAccess().getBlocksBlockParserRuleCall_1_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleBlock_in_ruleLoop2090);
            	    lv_blocks_3_0=ruleBlock();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getLoopRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"blocks",
            	              		lv_blocks_3_0, 
            	              		"Block");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            otherlv_4=(Token)match(input,14,FOLLOW_14_in_ruleLoop2103); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getLoopAccess().getEndKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLoop"


    // $ANTLR start "entryRuleForever"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:945:1: entryRuleForever returns [EObject current=null] : iv_ruleForever= ruleForever EOF ;
    public final EObject entryRuleForever() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleForever = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:946:2: (iv_ruleForever= ruleForever EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:947:2: iv_ruleForever= ruleForever EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getForeverRule()); 
            }
            pushFollow(FOLLOW_ruleForever_in_entryRuleForever2139);
            iv_ruleForever=ruleForever();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleForever; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleForever2149); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleForever"


    // $ANTLR start "ruleForever"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:954:1: ruleForever returns [EObject current=null] : (otherlv_0= 'forever' () (otherlv_2= 'if' ( (lv_cond_3_0= ruleBinaryTerminal ) ) )? ) ;
    public final EObject ruleForever() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_cond_3_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:957:28: ( (otherlv_0= 'forever' () (otherlv_2= 'if' ( (lv_cond_3_0= ruleBinaryTerminal ) ) )? ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:958:1: (otherlv_0= 'forever' () (otherlv_2= 'if' ( (lv_cond_3_0= ruleBinaryTerminal ) ) )? )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:958:1: (otherlv_0= 'forever' () (otherlv_2= 'if' ( (lv_cond_3_0= ruleBinaryTerminal ) ) )? )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:958:3: otherlv_0= 'forever' () (otherlv_2= 'if' ( (lv_cond_3_0= ruleBinaryTerminal ) ) )?
            {
            otherlv_0=(Token)match(input,18,FOLLOW_18_in_ruleForever2186); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getForeverAccess().getForeverKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:962:1: ()
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:963:2: 
            {
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getForeverAccess().getForeverAction_1(),
                          current);
                  
            }

            }

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:971:2: (otherlv_2= 'if' ( (lv_cond_3_0= ruleBinaryTerminal ) ) )?
            int alt10=2;
            alt10 = dfa10.predict(input);
            switch (alt10) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:971:4: otherlv_2= 'if' ( (lv_cond_3_0= ruleBinaryTerminal ) )
                    {
                    otherlv_2=(Token)match(input,19,FOLLOW_19_in_ruleForever2211); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getForeverAccess().getIfKeyword_2_0());
                          
                    }
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:975:1: ( (lv_cond_3_0= ruleBinaryTerminal ) )
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:976:1: (lv_cond_3_0= ruleBinaryTerminal )
                    {
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:976:1: (lv_cond_3_0= ruleBinaryTerminal )
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:977:3: lv_cond_3_0= ruleBinaryTerminal
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getForeverAccess().getCondBinaryTerminalParserRuleCall_2_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleBinaryTerminal_in_ruleForever2232);
                    lv_cond_3_0=ruleBinaryTerminal();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getForeverRule());
                      	        }
                             		set(
                             			current, 
                             			"cond",
                              		lv_cond_3_0, 
                              		"BinaryTerminal");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleForever"


    // $ANTLR start "entryRuleRepeatN"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1001:1: entryRuleRepeatN returns [EObject current=null] : iv_ruleRepeatN= ruleRepeatN EOF ;
    public final EObject entryRuleRepeatN() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRepeatN = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1002:2: (iv_ruleRepeatN= ruleRepeatN EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1003:2: iv_ruleRepeatN= ruleRepeatN EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRepeatNRule()); 
            }
            pushFollow(FOLLOW_ruleRepeatN_in_entryRuleRepeatN2270);
            iv_ruleRepeatN=ruleRepeatN();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRepeatN; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleRepeatN2280); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRepeatN"


    // $ANTLR start "ruleRepeatN"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1010:1: ruleRepeatN returns [EObject current=null] : (otherlv_0= 'repeat' ( (lv_n_1_0= ruleNumberTerminal ) ) ) ;
    public final EObject ruleRepeatN() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_n_1_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1013:28: ( (otherlv_0= 'repeat' ( (lv_n_1_0= ruleNumberTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1014:1: (otherlv_0= 'repeat' ( (lv_n_1_0= ruleNumberTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1014:1: (otherlv_0= 'repeat' ( (lv_n_1_0= ruleNumberTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1014:3: otherlv_0= 'repeat' ( (lv_n_1_0= ruleNumberTerminal ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_20_in_ruleRepeatN2317); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getRepeatNAccess().getRepeatKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1018:1: ( (lv_n_1_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1019:1: (lv_n_1_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1019:1: (lv_n_1_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1020:3: lv_n_1_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getRepeatNAccess().getNNumberTerminalParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleRepeatN2338);
            lv_n_1_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getRepeatNRule());
              	        }
                     		set(
                     			current, 
                     			"n",
                      		lv_n_1_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRepeatN"


    // $ANTLR start "entryRuleRepeatUntil"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1044:1: entryRuleRepeatUntil returns [EObject current=null] : iv_ruleRepeatUntil= ruleRepeatUntil EOF ;
    public final EObject entryRuleRepeatUntil() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRepeatUntil = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1045:2: (iv_ruleRepeatUntil= ruleRepeatUntil EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1046:2: iv_ruleRepeatUntil= ruleRepeatUntil EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRepeatUntilRule()); 
            }
            pushFollow(FOLLOW_ruleRepeatUntil_in_entryRuleRepeatUntil2374);
            iv_ruleRepeatUntil=ruleRepeatUntil();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRepeatUntil; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleRepeatUntil2384); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRepeatUntil"


    // $ANTLR start "ruleRepeatUntil"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1053:1: ruleRepeatUntil returns [EObject current=null] : (otherlv_0= 'repeat' otherlv_1= 'until' ( (lv_cond_2_0= ruleBinaryTerminal ) ) ) ;
    public final EObject ruleRepeatUntil() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        EObject lv_cond_2_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1056:28: ( (otherlv_0= 'repeat' otherlv_1= 'until' ( (lv_cond_2_0= ruleBinaryTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1057:1: (otherlv_0= 'repeat' otherlv_1= 'until' ( (lv_cond_2_0= ruleBinaryTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1057:1: (otherlv_0= 'repeat' otherlv_1= 'until' ( (lv_cond_2_0= ruleBinaryTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1057:3: otherlv_0= 'repeat' otherlv_1= 'until' ( (lv_cond_2_0= ruleBinaryTerminal ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_20_in_ruleRepeatUntil2421); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getRepeatUntilAccess().getRepeatKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,21,FOLLOW_21_in_ruleRepeatUntil2433); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getRepeatUntilAccess().getUntilKeyword_1());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1065:1: ( (lv_cond_2_0= ruleBinaryTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1066:1: (lv_cond_2_0= ruleBinaryTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1066:1: (lv_cond_2_0= ruleBinaryTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1067:3: lv_cond_2_0= ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getRepeatUntilAccess().getCondBinaryTerminalParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_ruleRepeatUntil2454);
            lv_cond_2_0=ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getRepeatUntilRule());
              	        }
                     		set(
                     			current, 
                     			"cond",
                      		lv_cond_2_0, 
                      		"BinaryTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRepeatUntil"


    // $ANTLR start "entryRuleStop"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1091:1: entryRuleStop returns [EObject current=null] : iv_ruleStop= ruleStop EOF ;
    public final EObject entryRuleStop() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStop = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1092:2: (iv_ruleStop= ruleStop EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1093:2: iv_ruleStop= ruleStop EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStopRule()); 
            }
            pushFollow(FOLLOW_ruleStop_in_entryRuleStop2490);
            iv_ruleStop=ruleStop();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStop; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStop2500); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStop"


    // $ANTLR start "ruleStop"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1100:1: ruleStop returns [EObject current=null] : (otherlv_0= 'stop' ( ( (lv_all_1_0= 'all' ) ) | ( (lv_script_2_0= 'script' ) ) ) ) ;
    public final EObject ruleStop() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_all_1_0=null;
        Token lv_script_2_0=null;

         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1103:28: ( (otherlv_0= 'stop' ( ( (lv_all_1_0= 'all' ) ) | ( (lv_script_2_0= 'script' ) ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1104:1: (otherlv_0= 'stop' ( ( (lv_all_1_0= 'all' ) ) | ( (lv_script_2_0= 'script' ) ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1104:1: (otherlv_0= 'stop' ( ( (lv_all_1_0= 'all' ) ) | ( (lv_script_2_0= 'script' ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1104:3: otherlv_0= 'stop' ( ( (lv_all_1_0= 'all' ) ) | ( (lv_script_2_0= 'script' ) ) )
            {
            otherlv_0=(Token)match(input,22,FOLLOW_22_in_ruleStop2537); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getStopAccess().getStopKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1108:1: ( ( (lv_all_1_0= 'all' ) ) | ( (lv_script_2_0= 'script' ) ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==23) ) {
                alt11=1;
            }
            else if ( (LA11_0==13) ) {
                alt11=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1108:2: ( (lv_all_1_0= 'all' ) )
                    {
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1108:2: ( (lv_all_1_0= 'all' ) )
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1109:1: (lv_all_1_0= 'all' )
                    {
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1109:1: (lv_all_1_0= 'all' )
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1110:3: lv_all_1_0= 'all'
                    {
                    lv_all_1_0=(Token)match(input,23,FOLLOW_23_in_ruleStop2556); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_all_1_0, grammarAccess.getStopAccess().getAllAllKeyword_1_0_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getStopRule());
                      	        }
                             		setWithLastConsumed(current, "all", true, "all");
                      	    
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1124:6: ( (lv_script_2_0= 'script' ) )
                    {
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1124:6: ( (lv_script_2_0= 'script' ) )
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1125:1: (lv_script_2_0= 'script' )
                    {
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1125:1: (lv_script_2_0= 'script' )
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1126:3: lv_script_2_0= 'script'
                    {
                    lv_script_2_0=(Token)match(input,13,FOLLOW_13_in_ruleStop2593); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_script_2_0, grammarAccess.getStopAccess().getScriptScriptKeyword_1_1_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getStopRule());
                      	        }
                             		setWithLastConsumed(current, "script", true, "script");
                      	    
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStop"


    // $ANTLR start "entryRuleWait"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1147:1: entryRuleWait returns [EObject current=null] : iv_ruleWait= ruleWait EOF ;
    public final EObject entryRuleWait() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWait = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1148:2: (iv_ruleWait= ruleWait EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1149:2: iv_ruleWait= ruleWait EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWaitRule()); 
            }
            pushFollow(FOLLOW_ruleWait_in_entryRuleWait2643);
            iv_ruleWait=ruleWait();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWait; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleWait2653); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWait"


    // $ANTLR start "ruleWait"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1156:1: ruleWait returns [EObject current=null] : (otherlv_0= 'wait' (this_WaitDelay_1= ruleWaitDelay | this_WaitUntil_2= ruleWaitUntil ) ) ;
    public final EObject ruleWait() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject this_WaitDelay_1 = null;

        EObject this_WaitUntil_2 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1159:28: ( (otherlv_0= 'wait' (this_WaitDelay_1= ruleWaitDelay | this_WaitUntil_2= ruleWaitUntil ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1160:1: (otherlv_0= 'wait' (this_WaitDelay_1= ruleWaitDelay | this_WaitUntil_2= ruleWaitUntil ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1160:1: (otherlv_0= 'wait' (this_WaitDelay_1= ruleWaitDelay | this_WaitUntil_2= ruleWaitUntil ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1160:3: otherlv_0= 'wait' (this_WaitDelay_1= ruleWaitDelay | this_WaitUntil_2= ruleWaitUntil )
            {
            otherlv_0=(Token)match(input,24,FOLLOW_24_in_ruleWait2690); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getWaitAccess().getWaitKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1164:1: (this_WaitDelay_1= ruleWaitDelay | this_WaitUntil_2= ruleWaitUntil )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==46) ) {
                alt12=1;
            }
            else if ( (LA12_0==21) ) {
                alt12=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1165:2: this_WaitDelay_1= ruleWaitDelay
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getWaitAccess().getWaitDelayParserRuleCall_1_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleWaitDelay_in_ruleWait2716);
                    this_WaitDelay_1=ruleWaitDelay();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_WaitDelay_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1178:2: this_WaitUntil_2= ruleWaitUntil
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getWaitAccess().getWaitUntilParserRuleCall_1_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleWaitUntil_in_ruleWait2746);
                    this_WaitUntil_2=ruleWaitUntil();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_WaitUntil_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWait"


    // $ANTLR start "entryRuleWaitDelay"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1197:1: entryRuleWaitDelay returns [EObject current=null] : iv_ruleWaitDelay= ruleWaitDelay EOF ;
    public final EObject entryRuleWaitDelay() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWaitDelay = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1198:2: (iv_ruleWaitDelay= ruleWaitDelay EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1199:2: iv_ruleWaitDelay= ruleWaitDelay EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWaitDelayRule()); 
            }
            pushFollow(FOLLOW_ruleWaitDelay_in_entryRuleWaitDelay2782);
            iv_ruleWaitDelay=ruleWaitDelay();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWaitDelay; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleWaitDelay2792); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWaitDelay"


    // $ANTLR start "ruleWaitDelay"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1206:1: ruleWaitDelay returns [EObject current=null] : ( ( (lv_delay_0_0= ruleNumberTerminal ) ) otherlv_1= 'secs' ) ;
    public final EObject ruleWaitDelay() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_delay_0_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1209:28: ( ( ( (lv_delay_0_0= ruleNumberTerminal ) ) otherlv_1= 'secs' ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1210:1: ( ( (lv_delay_0_0= ruleNumberTerminal ) ) otherlv_1= 'secs' )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1210:1: ( ( (lv_delay_0_0= ruleNumberTerminal ) ) otherlv_1= 'secs' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1210:2: ( (lv_delay_0_0= ruleNumberTerminal ) ) otherlv_1= 'secs'
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1210:2: ( (lv_delay_0_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1211:1: (lv_delay_0_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1211:1: (lv_delay_0_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1212:3: lv_delay_0_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getWaitDelayAccess().getDelayNumberTerminalParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleWaitDelay2838);
            lv_delay_0_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getWaitDelayRule());
              	        }
                     		set(
                     			current, 
                     			"delay",
                      		lv_delay_0_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,25,FOLLOW_25_in_ruleWaitDelay2850); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getWaitDelayAccess().getSecsKeyword_1());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWaitDelay"


    // $ANTLR start "entryRuleWaitUntil"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1240:1: entryRuleWaitUntil returns [EObject current=null] : iv_ruleWaitUntil= ruleWaitUntil EOF ;
    public final EObject entryRuleWaitUntil() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWaitUntil = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1241:2: (iv_ruleWaitUntil= ruleWaitUntil EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1242:2: iv_ruleWaitUntil= ruleWaitUntil EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getWaitUntilRule()); 
            }
            pushFollow(FOLLOW_ruleWaitUntil_in_entryRuleWaitUntil2886);
            iv_ruleWaitUntil=ruleWaitUntil();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleWaitUntil; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleWaitUntil2896); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWaitUntil"


    // $ANTLR start "ruleWaitUntil"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1249:1: ruleWaitUntil returns [EObject current=null] : (otherlv_0= 'until' ( (lv_cond_1_0= ruleBinaryTerminal ) ) ) ;
    public final EObject ruleWaitUntil() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_cond_1_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1252:28: ( (otherlv_0= 'until' ( (lv_cond_1_0= ruleBinaryTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1253:1: (otherlv_0= 'until' ( (lv_cond_1_0= ruleBinaryTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1253:1: (otherlv_0= 'until' ( (lv_cond_1_0= ruleBinaryTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1253:3: otherlv_0= 'until' ( (lv_cond_1_0= ruleBinaryTerminal ) )
            {
            otherlv_0=(Token)match(input,21,FOLLOW_21_in_ruleWaitUntil2933); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getWaitUntilAccess().getUntilKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1257:1: ( (lv_cond_1_0= ruleBinaryTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1258:1: (lv_cond_1_0= ruleBinaryTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1258:1: (lv_cond_1_0= ruleBinaryTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1259:3: lv_cond_1_0= ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getWaitUntilAccess().getCondBinaryTerminalParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_ruleWaitUntil2954);
            lv_cond_1_0=ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getWaitUntilRule());
              	        }
                     		set(
                     			current, 
                     			"cond",
                      		lv_cond_1_0, 
                      		"BinaryTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWaitUntil"


    // $ANTLR start "entryRuleBroadcast"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1283:1: entryRuleBroadcast returns [EObject current=null] : iv_ruleBroadcast= ruleBroadcast EOF ;
    public final EObject entryRuleBroadcast() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBroadcast = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1284:2: (iv_ruleBroadcast= ruleBroadcast EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1285:2: iv_ruleBroadcast= ruleBroadcast EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBroadcastRule()); 
            }
            pushFollow(FOLLOW_ruleBroadcast_in_entryRuleBroadcast2990);
            iv_ruleBroadcast=ruleBroadcast();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBroadcast; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBroadcast3000); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBroadcast"


    // $ANTLR start "ruleBroadcast"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1292:1: ruleBroadcast returns [EObject current=null] : (otherlv_0= 'broadcast' ( (lv_msg_1_0= ruleMessage ) ) ( (lv_wait_2_0= 'and wait' ) )? ) ;
    public final EObject ruleBroadcast() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_wait_2_0=null;
        EObject lv_msg_1_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1295:28: ( (otherlv_0= 'broadcast' ( (lv_msg_1_0= ruleMessage ) ) ( (lv_wait_2_0= 'and wait' ) )? ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1296:1: (otherlv_0= 'broadcast' ( (lv_msg_1_0= ruleMessage ) ) ( (lv_wait_2_0= 'and wait' ) )? )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1296:1: (otherlv_0= 'broadcast' ( (lv_msg_1_0= ruleMessage ) ) ( (lv_wait_2_0= 'and wait' ) )? )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1296:3: otherlv_0= 'broadcast' ( (lv_msg_1_0= ruleMessage ) ) ( (lv_wait_2_0= 'and wait' ) )?
            {
            otherlv_0=(Token)match(input,26,FOLLOW_26_in_ruleBroadcast3037); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getBroadcastAccess().getBroadcastKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1300:1: ( (lv_msg_1_0= ruleMessage ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1301:1: (lv_msg_1_0= ruleMessage )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1301:1: (lv_msg_1_0= ruleMessage )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1302:3: lv_msg_1_0= ruleMessage
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getBroadcastAccess().getMsgMessageParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleMessage_in_ruleBroadcast3058);
            lv_msg_1_0=ruleMessage();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getBroadcastRule());
              	        }
                     		set(
                     			current, 
                     			"msg",
                      		lv_msg_1_0, 
                      		"Message");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1318:2: ( (lv_wait_2_0= 'and wait' ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==27) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1319:1: (lv_wait_2_0= 'and wait' )
                    {
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1319:1: (lv_wait_2_0= 'and wait' )
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1320:3: lv_wait_2_0= 'and wait'
                    {
                    lv_wait_2_0=(Token)match(input,27,FOLLOW_27_in_ruleBroadcast3076); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              newLeafNode(lv_wait_2_0, grammarAccess.getBroadcastAccess().getWaitAndWaitKeyword_2_0());
                          
                    }
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElement(grammarAccess.getBroadcastRule());
                      	        }
                             		setWithLastConsumed(current, "wait", true, "and wait");
                      	    
                    }

                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBroadcast"


    // $ANTLR start "entryRuleIf"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1341:1: entryRuleIf returns [EObject current=null] : iv_ruleIf= ruleIf EOF ;
    public final EObject entryRuleIf() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIf = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1342:2: (iv_ruleIf= ruleIf EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1343:2: iv_ruleIf= ruleIf EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIfRule()); 
            }
            pushFollow(FOLLOW_ruleIf_in_entryRuleIf3126);
            iv_ruleIf=ruleIf();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIf; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIf3136); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIf"


    // $ANTLR start "ruleIf"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1350:1: ruleIf returns [EObject current=null] : ( (otherlv_0= 'if' ( (lv_cond_1_0= ruleBinaryTerminal ) ) ( (lv_true_2_0= ruleBlock ) )* ) (otherlv_3= 'else' ( (lv_false_4_0= ruleBlock ) )* )? otherlv_5= 'end' ) ;
    public final EObject ruleIf() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_cond_1_0 = null;

        EObject lv_true_2_0 = null;

        EObject lv_false_4_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1353:28: ( ( (otherlv_0= 'if' ( (lv_cond_1_0= ruleBinaryTerminal ) ) ( (lv_true_2_0= ruleBlock ) )* ) (otherlv_3= 'else' ( (lv_false_4_0= ruleBlock ) )* )? otherlv_5= 'end' ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1354:1: ( (otherlv_0= 'if' ( (lv_cond_1_0= ruleBinaryTerminal ) ) ( (lv_true_2_0= ruleBlock ) )* ) (otherlv_3= 'else' ( (lv_false_4_0= ruleBlock ) )* )? otherlv_5= 'end' )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1354:1: ( (otherlv_0= 'if' ( (lv_cond_1_0= ruleBinaryTerminal ) ) ( (lv_true_2_0= ruleBlock ) )* ) (otherlv_3= 'else' ( (lv_false_4_0= ruleBlock ) )* )? otherlv_5= 'end' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1354:2: (otherlv_0= 'if' ( (lv_cond_1_0= ruleBinaryTerminal ) ) ( (lv_true_2_0= ruleBlock ) )* ) (otherlv_3= 'else' ( (lv_false_4_0= ruleBlock ) )* )? otherlv_5= 'end'
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1354:2: (otherlv_0= 'if' ( (lv_cond_1_0= ruleBinaryTerminal ) ) ( (lv_true_2_0= ruleBlock ) )* )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1354:4: otherlv_0= 'if' ( (lv_cond_1_0= ruleBinaryTerminal ) ) ( (lv_true_2_0= ruleBlock ) )*
            {
            otherlv_0=(Token)match(input,19,FOLLOW_19_in_ruleIf3174); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getIfAccess().getIfKeyword_0_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1358:1: ( (lv_cond_1_0= ruleBinaryTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1359:1: (lv_cond_1_0= ruleBinaryTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1359:1: (lv_cond_1_0= ruleBinaryTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1360:3: lv_cond_1_0= ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getIfAccess().getCondBinaryTerminalParserRuleCall_0_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_ruleIf3195);
            lv_cond_1_0=ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getIfRule());
              	        }
                     		set(
                     			current, 
                     			"cond",
                      		lv_cond_1_0, 
                      		"BinaryTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1376:2: ( (lv_true_2_0= ruleBlock ) )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( ((LA14_0>=18 && LA14_0<=20)||LA14_0==22||LA14_0==24||LA14_0==26||LA14_0==29||LA14_0==31||(LA14_0>=33 && LA14_0<=36)||LA14_0==39||(LA14_0>=41 && LA14_0<=45)||(LA14_0>=62 && LA14_0<=64)||LA14_0==66) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1377:1: (lv_true_2_0= ruleBlock )
            	    {
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1377:1: (lv_true_2_0= ruleBlock )
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1378:3: lv_true_2_0= ruleBlock
            	    {
            	    if ( state.backtracking==0 ) {
            	       
            	      	        newCompositeNode(grammarAccess.getIfAccess().getTrueBlockParserRuleCall_0_2_0()); 
            	      	    
            	    }
            	    pushFollow(FOLLOW_ruleBlock_in_ruleIf3216);
            	    lv_true_2_0=ruleBlock();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      	        if (current==null) {
            	      	            current = createModelElementForParent(grammarAccess.getIfRule());
            	      	        }
            	             		add(
            	             			current, 
            	             			"true",
            	              		lv_true_2_0, 
            	              		"Block");
            	      	        afterParserOrEnumRuleCall();
            	      	    
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);


            }

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1394:4: (otherlv_3= 'else' ( (lv_false_4_0= ruleBlock ) )* )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==28) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1394:6: otherlv_3= 'else' ( (lv_false_4_0= ruleBlock ) )*
                    {
                    otherlv_3=(Token)match(input,28,FOLLOW_28_in_ruleIf3231); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_3, grammarAccess.getIfAccess().getElseKeyword_1_0());
                          
                    }
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1398:1: ( (lv_false_4_0= ruleBlock ) )*
                    loop15:
                    do {
                        int alt15=2;
                        int LA15_0 = input.LA(1);

                        if ( ((LA15_0>=18 && LA15_0<=20)||LA15_0==22||LA15_0==24||LA15_0==26||LA15_0==29||LA15_0==31||(LA15_0>=33 && LA15_0<=36)||LA15_0==39||(LA15_0>=41 && LA15_0<=45)||(LA15_0>=62 && LA15_0<=64)||LA15_0==66) ) {
                            alt15=1;
                        }


                        switch (alt15) {
                    	case 1 :
                    	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1399:1: (lv_false_4_0= ruleBlock )
                    	    {
                    	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1399:1: (lv_false_4_0= ruleBlock )
                    	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1400:3: lv_false_4_0= ruleBlock
                    	    {
                    	    if ( state.backtracking==0 ) {
                    	       
                    	      	        newCompositeNode(grammarAccess.getIfAccess().getFalseBlockParserRuleCall_1_1_0()); 
                    	      	    
                    	    }
                    	    pushFollow(FOLLOW_ruleBlock_in_ruleIf3252);
                    	    lv_false_4_0=ruleBlock();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      	        if (current==null) {
                    	      	            current = createModelElementForParent(grammarAccess.getIfRule());
                    	      	        }
                    	             		add(
                    	             			current, 
                    	             			"false",
                    	              		lv_false_4_0, 
                    	              		"Block");
                    	      	        afterParserOrEnumRuleCall();
                    	      	    
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop15;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,14,FOLLOW_14_in_ruleIf3267); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_5, grammarAccess.getIfAccess().getEndKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIf"


    // $ANTLR start "entryRuleMove"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1428:1: entryRuleMove returns [EObject current=null] : iv_ruleMove= ruleMove EOF ;
    public final EObject entryRuleMove() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMove = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1429:2: (iv_ruleMove= ruleMove EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1430:2: iv_ruleMove= ruleMove EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMoveRule()); 
            }
            pushFollow(FOLLOW_ruleMove_in_entryRuleMove3303);
            iv_ruleMove=ruleMove();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMove; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMove3313); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMove"


    // $ANTLR start "ruleMove"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1437:1: ruleMove returns [EObject current=null] : (otherlv_0= 'move' ( (lv_steps_1_0= ruleNumberTerminal ) ) otherlv_2= 'steps' ) ;
    public final EObject ruleMove() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_steps_1_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1440:28: ( (otherlv_0= 'move' ( (lv_steps_1_0= ruleNumberTerminal ) ) otherlv_2= 'steps' ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1441:1: (otherlv_0= 'move' ( (lv_steps_1_0= ruleNumberTerminal ) ) otherlv_2= 'steps' )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1441:1: (otherlv_0= 'move' ( (lv_steps_1_0= ruleNumberTerminal ) ) otherlv_2= 'steps' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1441:3: otherlv_0= 'move' ( (lv_steps_1_0= ruleNumberTerminal ) ) otherlv_2= 'steps'
            {
            otherlv_0=(Token)match(input,29,FOLLOW_29_in_ruleMove3350); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getMoveAccess().getMoveKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1445:1: ( (lv_steps_1_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1446:1: (lv_steps_1_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1446:1: (lv_steps_1_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1447:3: lv_steps_1_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getMoveAccess().getStepsNumberTerminalParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleMove3371);
            lv_steps_1_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getMoveRule());
              	        }
                     		set(
                     			current, 
                     			"steps",
                      		lv_steps_1_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,30,FOLLOW_30_in_ruleMove3383); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getMoveAccess().getStepsKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMove"


    // $ANTLR start "entryRuleTurnCW"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1475:1: entryRuleTurnCW returns [EObject current=null] : iv_ruleTurnCW= ruleTurnCW EOF ;
    public final EObject entryRuleTurnCW() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTurnCW = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1476:2: (iv_ruleTurnCW= ruleTurnCW EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1477:2: iv_ruleTurnCW= ruleTurnCW EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTurnCWRule()); 
            }
            pushFollow(FOLLOW_ruleTurnCW_in_entryRuleTurnCW3419);
            iv_ruleTurnCW=ruleTurnCW();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTurnCW; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTurnCW3429); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTurnCW"


    // $ANTLR start "ruleTurnCW"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1484:1: ruleTurnCW returns [EObject current=null] : (otherlv_0= 'turn cw' ( (lv_angle_1_0= ruleNumberTerminal ) ) otherlv_2= 'degrees' ) ;
    public final EObject ruleTurnCW() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_angle_1_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1487:28: ( (otherlv_0= 'turn cw' ( (lv_angle_1_0= ruleNumberTerminal ) ) otherlv_2= 'degrees' ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1488:1: (otherlv_0= 'turn cw' ( (lv_angle_1_0= ruleNumberTerminal ) ) otherlv_2= 'degrees' )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1488:1: (otherlv_0= 'turn cw' ( (lv_angle_1_0= ruleNumberTerminal ) ) otherlv_2= 'degrees' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1488:3: otherlv_0= 'turn cw' ( (lv_angle_1_0= ruleNumberTerminal ) ) otherlv_2= 'degrees'
            {
            otherlv_0=(Token)match(input,31,FOLLOW_31_in_ruleTurnCW3466); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTurnCWAccess().getTurnCwKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1492:1: ( (lv_angle_1_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1493:1: (lv_angle_1_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1493:1: (lv_angle_1_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1494:3: lv_angle_1_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getTurnCWAccess().getAngleNumberTerminalParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleTurnCW3487);
            lv_angle_1_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getTurnCWRule());
              	        }
                     		set(
                     			current, 
                     			"angle",
                      		lv_angle_1_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,32,FOLLOW_32_in_ruleTurnCW3499); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getTurnCWAccess().getDegreesKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTurnCW"


    // $ANTLR start "entryRuleTurnCCW"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1522:1: entryRuleTurnCCW returns [EObject current=null] : iv_ruleTurnCCW= ruleTurnCCW EOF ;
    public final EObject entryRuleTurnCCW() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTurnCCW = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1523:2: (iv_ruleTurnCCW= ruleTurnCCW EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1524:2: iv_ruleTurnCCW= ruleTurnCCW EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTurnCCWRule()); 
            }
            pushFollow(FOLLOW_ruleTurnCCW_in_entryRuleTurnCCW3535);
            iv_ruleTurnCCW=ruleTurnCCW();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTurnCCW; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTurnCCW3545); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTurnCCW"


    // $ANTLR start "ruleTurnCCW"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1531:1: ruleTurnCCW returns [EObject current=null] : (otherlv_0= 'turn ccw' ( (lv_angle_1_0= ruleNumberTerminal ) ) otherlv_2= 'degrees' ) ;
    public final EObject ruleTurnCCW() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_angle_1_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1534:28: ( (otherlv_0= 'turn ccw' ( (lv_angle_1_0= ruleNumberTerminal ) ) otherlv_2= 'degrees' ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1535:1: (otherlv_0= 'turn ccw' ( (lv_angle_1_0= ruleNumberTerminal ) ) otherlv_2= 'degrees' )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1535:1: (otherlv_0= 'turn ccw' ( (lv_angle_1_0= ruleNumberTerminal ) ) otherlv_2= 'degrees' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1535:3: otherlv_0= 'turn ccw' ( (lv_angle_1_0= ruleNumberTerminal ) ) otherlv_2= 'degrees'
            {
            otherlv_0=(Token)match(input,33,FOLLOW_33_in_ruleTurnCCW3582); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTurnCCWAccess().getTurnCcwKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1539:1: ( (lv_angle_1_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1540:1: (lv_angle_1_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1540:1: (lv_angle_1_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1541:3: lv_angle_1_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getTurnCCWAccess().getAngleNumberTerminalParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleTurnCCW3603);
            lv_angle_1_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getTurnCCWRule());
              	        }
                     		set(
                     			current, 
                     			"angle",
                      		lv_angle_1_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,32,FOLLOW_32_in_ruleTurnCCW3615); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getTurnCCWAccess().getDegreesKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTurnCCW"


    // $ANTLR start "entryRulePointDirection"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1569:1: entryRulePointDirection returns [EObject current=null] : iv_rulePointDirection= rulePointDirection EOF ;
    public final EObject entryRulePointDirection() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePointDirection = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1570:2: (iv_rulePointDirection= rulePointDirection EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1571:2: iv_rulePointDirection= rulePointDirection EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPointDirectionRule()); 
            }
            pushFollow(FOLLOW_rulePointDirection_in_entryRulePointDirection3651);
            iv_rulePointDirection=rulePointDirection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePointDirection; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePointDirection3661); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePointDirection"


    // $ANTLR start "rulePointDirection"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1578:1: rulePointDirection returns [EObject current=null] : (otherlv_0= 'point in direction' ( (lv_angle_1_0= ruleNumberTerminal ) ) ) ;
    public final EObject rulePointDirection() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_angle_1_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1581:28: ( (otherlv_0= 'point in direction' ( (lv_angle_1_0= ruleNumberTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1582:1: (otherlv_0= 'point in direction' ( (lv_angle_1_0= ruleNumberTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1582:1: (otherlv_0= 'point in direction' ( (lv_angle_1_0= ruleNumberTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1582:3: otherlv_0= 'point in direction' ( (lv_angle_1_0= ruleNumberTerminal ) )
            {
            otherlv_0=(Token)match(input,34,FOLLOW_34_in_rulePointDirection3698); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getPointDirectionAccess().getPointInDirectionKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1586:1: ( (lv_angle_1_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1587:1: (lv_angle_1_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1587:1: (lv_angle_1_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1588:3: lv_angle_1_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getPointDirectionAccess().getAngleNumberTerminalParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rulePointDirection3719);
            lv_angle_1_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getPointDirectionRule());
              	        }
                     		set(
                     			current, 
                     			"angle",
                      		lv_angle_1_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePointDirection"


    // $ANTLR start "entryRulePointTowards"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1612:1: entryRulePointTowards returns [EObject current=null] : iv_rulePointTowards= rulePointTowards EOF ;
    public final EObject entryRulePointTowards() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePointTowards = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1613:2: (iv_rulePointTowards= rulePointTowards EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1614:2: iv_rulePointTowards= rulePointTowards EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPointTowardsRule()); 
            }
            pushFollow(FOLLOW_rulePointTowards_in_entryRulePointTowards3755);
            iv_rulePointTowards=rulePointTowards();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePointTowards; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePointTowards3765); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePointTowards"


    // $ANTLR start "rulePointTowards"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1621:1: rulePointTowards returns [EObject current=null] : (otherlv_0= 'point towards' ( (lv_object_1_0= ruleIObject ) ) ) ;
    public final EObject rulePointTowards() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_object_1_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1624:28: ( (otherlv_0= 'point towards' ( (lv_object_1_0= ruleIObject ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1625:1: (otherlv_0= 'point towards' ( (lv_object_1_0= ruleIObject ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1625:1: (otherlv_0= 'point towards' ( (lv_object_1_0= ruleIObject ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1625:3: otherlv_0= 'point towards' ( (lv_object_1_0= ruleIObject ) )
            {
            otherlv_0=(Token)match(input,35,FOLLOW_35_in_rulePointTowards3802); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getPointTowardsAccess().getPointTowardsKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1629:1: ( (lv_object_1_0= ruleIObject ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1630:1: (lv_object_1_0= ruleIObject )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1630:1: (lv_object_1_0= ruleIObject )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1631:3: lv_object_1_0= ruleIObject
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getPointTowardsAccess().getObjectIObjectParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleIObject_in_rulePointTowards3823);
            lv_object_1_0=ruleIObject();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getPointTowardsRule());
              	        }
                     		set(
                     			current, 
                     			"object",
                      		lv_object_1_0, 
                      		"IObject");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePointTowards"


    // $ANTLR start "entryRuleGoToXY"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1655:1: entryRuleGoToXY returns [EObject current=null] : iv_ruleGoToXY= ruleGoToXY EOF ;
    public final EObject entryRuleGoToXY() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGoToXY = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1656:2: (iv_ruleGoToXY= ruleGoToXY EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1657:2: iv_ruleGoToXY= ruleGoToXY EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGoToXYRule()); 
            }
            pushFollow(FOLLOW_ruleGoToXY_in_entryRuleGoToXY3859);
            iv_ruleGoToXY=ruleGoToXY();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGoToXY; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGoToXY3869); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGoToXY"


    // $ANTLR start "ruleGoToXY"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1664:1: ruleGoToXY returns [EObject current=null] : (otherlv_0= 'go to' otherlv_1= 'x:' ( (lv_x_2_0= ruleNumberTerminal ) ) otherlv_3= 'y:' ( (lv_y_4_0= ruleNumberTerminal ) ) ) ;
    public final EObject ruleGoToXY() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_x_2_0 = null;

        EObject lv_y_4_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1667:28: ( (otherlv_0= 'go to' otherlv_1= 'x:' ( (lv_x_2_0= ruleNumberTerminal ) ) otherlv_3= 'y:' ( (lv_y_4_0= ruleNumberTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1668:1: (otherlv_0= 'go to' otherlv_1= 'x:' ( (lv_x_2_0= ruleNumberTerminal ) ) otherlv_3= 'y:' ( (lv_y_4_0= ruleNumberTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1668:1: (otherlv_0= 'go to' otherlv_1= 'x:' ( (lv_x_2_0= ruleNumberTerminal ) ) otherlv_3= 'y:' ( (lv_y_4_0= ruleNumberTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1668:3: otherlv_0= 'go to' otherlv_1= 'x:' ( (lv_x_2_0= ruleNumberTerminal ) ) otherlv_3= 'y:' ( (lv_y_4_0= ruleNumberTerminal ) )
            {
            otherlv_0=(Token)match(input,36,FOLLOW_36_in_ruleGoToXY3906); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getGoToXYAccess().getGoToKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,37,FOLLOW_37_in_ruleGoToXY3918); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getGoToXYAccess().getXKeyword_1());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1676:1: ( (lv_x_2_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1677:1: (lv_x_2_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1677:1: (lv_x_2_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1678:3: lv_x_2_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getGoToXYAccess().getXNumberTerminalParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleGoToXY3939);
            lv_x_2_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getGoToXYRule());
              	        }
                     		set(
                     			current, 
                     			"x",
                      		lv_x_2_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,38,FOLLOW_38_in_ruleGoToXY3951); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getGoToXYAccess().getYKeyword_3());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1698:1: ( (lv_y_4_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1699:1: (lv_y_4_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1699:1: (lv_y_4_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1700:3: lv_y_4_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getGoToXYAccess().getYNumberTerminalParserRuleCall_4_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleGoToXY3972);
            lv_y_4_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getGoToXYRule());
              	        }
                     		set(
                     			current, 
                     			"y",
                      		lv_y_4_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGoToXY"


    // $ANTLR start "entryRuleGoTo"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1724:1: entryRuleGoTo returns [EObject current=null] : iv_ruleGoTo= ruleGoTo EOF ;
    public final EObject entryRuleGoTo() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGoTo = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1725:2: (iv_ruleGoTo= ruleGoTo EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1726:2: iv_ruleGoTo= ruleGoTo EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGoToRule()); 
            }
            pushFollow(FOLLOW_ruleGoTo_in_entryRuleGoTo4008);
            iv_ruleGoTo=ruleGoTo();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGoTo; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGoTo4018); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGoTo"


    // $ANTLR start "ruleGoTo"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1733:1: ruleGoTo returns [EObject current=null] : (otherlv_0= 'go to' ( (lv_object_1_0= ruleIObject ) ) ) ;
    public final EObject ruleGoTo() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_object_1_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1736:28: ( (otherlv_0= 'go to' ( (lv_object_1_0= ruleIObject ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1737:1: (otherlv_0= 'go to' ( (lv_object_1_0= ruleIObject ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1737:1: (otherlv_0= 'go to' ( (lv_object_1_0= ruleIObject ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1737:3: otherlv_0= 'go to' ( (lv_object_1_0= ruleIObject ) )
            {
            otherlv_0=(Token)match(input,36,FOLLOW_36_in_ruleGoTo4055); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getGoToAccess().getGoToKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1741:1: ( (lv_object_1_0= ruleIObject ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1742:1: (lv_object_1_0= ruleIObject )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1742:1: (lv_object_1_0= ruleIObject )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1743:3: lv_object_1_0= ruleIObject
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getGoToAccess().getObjectIObjectParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleIObject_in_ruleGoTo4076);
            lv_object_1_0=ruleIObject();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getGoToRule());
              	        }
                     		set(
                     			current, 
                     			"object",
                      		lv_object_1_0, 
                      		"IObject");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGoTo"


    // $ANTLR start "entryRuleGlide"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1767:1: entryRuleGlide returns [EObject current=null] : iv_ruleGlide= ruleGlide EOF ;
    public final EObject entryRuleGlide() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGlide = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1768:2: (iv_ruleGlide= ruleGlide EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1769:2: iv_ruleGlide= ruleGlide EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGlideRule()); 
            }
            pushFollow(FOLLOW_ruleGlide_in_entryRuleGlide4112);
            iv_ruleGlide=ruleGlide();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGlide; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGlide4122); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGlide"


    // $ANTLR start "ruleGlide"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1776:1: ruleGlide returns [EObject current=null] : (otherlv_0= 'glide' ( (lv_duration_1_0= ruleNumberTerminal ) ) otherlv_2= 'secs to x:' ( (lv_x_3_0= ruleNumberTerminal ) ) otherlv_4= 'y:' ( (lv_y_5_0= ruleNumberTerminal ) ) ) ;
    public final EObject ruleGlide() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_duration_1_0 = null;

        EObject lv_x_3_0 = null;

        EObject lv_y_5_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1779:28: ( (otherlv_0= 'glide' ( (lv_duration_1_0= ruleNumberTerminal ) ) otherlv_2= 'secs to x:' ( (lv_x_3_0= ruleNumberTerminal ) ) otherlv_4= 'y:' ( (lv_y_5_0= ruleNumberTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1780:1: (otherlv_0= 'glide' ( (lv_duration_1_0= ruleNumberTerminal ) ) otherlv_2= 'secs to x:' ( (lv_x_3_0= ruleNumberTerminal ) ) otherlv_4= 'y:' ( (lv_y_5_0= ruleNumberTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1780:1: (otherlv_0= 'glide' ( (lv_duration_1_0= ruleNumberTerminal ) ) otherlv_2= 'secs to x:' ( (lv_x_3_0= ruleNumberTerminal ) ) otherlv_4= 'y:' ( (lv_y_5_0= ruleNumberTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1780:3: otherlv_0= 'glide' ( (lv_duration_1_0= ruleNumberTerminal ) ) otherlv_2= 'secs to x:' ( (lv_x_3_0= ruleNumberTerminal ) ) otherlv_4= 'y:' ( (lv_y_5_0= ruleNumberTerminal ) )
            {
            otherlv_0=(Token)match(input,39,FOLLOW_39_in_ruleGlide4159); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getGlideAccess().getGlideKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1784:1: ( (lv_duration_1_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1785:1: (lv_duration_1_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1785:1: (lv_duration_1_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1786:3: lv_duration_1_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getGlideAccess().getDurationNumberTerminalParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleGlide4180);
            lv_duration_1_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getGlideRule());
              	        }
                     		set(
                     			current, 
                     			"duration",
                      		lv_duration_1_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_2=(Token)match(input,40,FOLLOW_40_in_ruleGlide4192); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getGlideAccess().getSecsToXKeyword_2());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1806:1: ( (lv_x_3_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1807:1: (lv_x_3_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1807:1: (lv_x_3_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1808:3: lv_x_3_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getGlideAccess().getXNumberTerminalParserRuleCall_3_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleGlide4213);
            lv_x_3_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getGlideRule());
              	        }
                     		set(
                     			current, 
                     			"x",
                      		lv_x_3_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_4=(Token)match(input,38,FOLLOW_38_in_ruleGlide4225); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_4, grammarAccess.getGlideAccess().getYKeyword_4());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1828:1: ( (lv_y_5_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1829:1: (lv_y_5_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1829:1: (lv_y_5_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1830:3: lv_y_5_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getGlideAccess().getYNumberTerminalParserRuleCall_5_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleGlide4246);
            lv_y_5_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getGlideRule());
              	        }
                     		set(
                     			current, 
                     			"y",
                      		lv_y_5_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGlide"


    // $ANTLR start "entryRuleChangeX"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1854:1: entryRuleChangeX returns [EObject current=null] : iv_ruleChangeX= ruleChangeX EOF ;
    public final EObject entryRuleChangeX() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleChangeX = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1855:2: (iv_ruleChangeX= ruleChangeX EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1856:2: iv_ruleChangeX= ruleChangeX EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getChangeXRule()); 
            }
            pushFollow(FOLLOW_ruleChangeX_in_entryRuleChangeX4282);
            iv_ruleChangeX=ruleChangeX();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleChangeX; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleChangeX4292); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleChangeX"


    // $ANTLR start "ruleChangeX"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1863:1: ruleChangeX returns [EObject current=null] : (otherlv_0= 'change x by' ( (lv_x_1_0= ruleNumberTerminal ) ) ) ;
    public final EObject ruleChangeX() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_x_1_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1866:28: ( (otherlv_0= 'change x by' ( (lv_x_1_0= ruleNumberTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1867:1: (otherlv_0= 'change x by' ( (lv_x_1_0= ruleNumberTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1867:1: (otherlv_0= 'change x by' ( (lv_x_1_0= ruleNumberTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1867:3: otherlv_0= 'change x by' ( (lv_x_1_0= ruleNumberTerminal ) )
            {
            otherlv_0=(Token)match(input,41,FOLLOW_41_in_ruleChangeX4329); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getChangeXAccess().getChangeXByKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1871:1: ( (lv_x_1_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1872:1: (lv_x_1_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1872:1: (lv_x_1_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1873:3: lv_x_1_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getChangeXAccess().getXNumberTerminalParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleChangeX4350);
            lv_x_1_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getChangeXRule());
              	        }
                     		set(
                     			current, 
                     			"x",
                      		lv_x_1_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleChangeX"


    // $ANTLR start "entryRuleChangeY"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1897:1: entryRuleChangeY returns [EObject current=null] : iv_ruleChangeY= ruleChangeY EOF ;
    public final EObject entryRuleChangeY() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleChangeY = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1898:2: (iv_ruleChangeY= ruleChangeY EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1899:2: iv_ruleChangeY= ruleChangeY EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getChangeYRule()); 
            }
            pushFollow(FOLLOW_ruleChangeY_in_entryRuleChangeY4386);
            iv_ruleChangeY=ruleChangeY();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleChangeY; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleChangeY4396); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleChangeY"


    // $ANTLR start "ruleChangeY"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1906:1: ruleChangeY returns [EObject current=null] : (otherlv_0= 'change y by' ( (lv_y_1_0= ruleNumberTerminal ) ) ) ;
    public final EObject ruleChangeY() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_y_1_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1909:28: ( (otherlv_0= 'change y by' ( (lv_y_1_0= ruleNumberTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1910:1: (otherlv_0= 'change y by' ( (lv_y_1_0= ruleNumberTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1910:1: (otherlv_0= 'change y by' ( (lv_y_1_0= ruleNumberTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1910:3: otherlv_0= 'change y by' ( (lv_y_1_0= ruleNumberTerminal ) )
            {
            otherlv_0=(Token)match(input,42,FOLLOW_42_in_ruleChangeY4433); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getChangeYAccess().getChangeYByKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1914:1: ( (lv_y_1_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1915:1: (lv_y_1_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1915:1: (lv_y_1_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1916:3: lv_y_1_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getChangeYAccess().getYNumberTerminalParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleChangeY4454);
            lv_y_1_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getChangeYRule());
              	        }
                     		set(
                     			current, 
                     			"y",
                      		lv_y_1_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleChangeY"


    // $ANTLR start "entryRuleSetX"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1940:1: entryRuleSetX returns [EObject current=null] : iv_ruleSetX= ruleSetX EOF ;
    public final EObject entryRuleSetX() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSetX = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1941:2: (iv_ruleSetX= ruleSetX EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1942:2: iv_ruleSetX= ruleSetX EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSetXRule()); 
            }
            pushFollow(FOLLOW_ruleSetX_in_entryRuleSetX4490);
            iv_ruleSetX=ruleSetX();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSetX; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSetX4500); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSetX"


    // $ANTLR start "ruleSetX"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1949:1: ruleSetX returns [EObject current=null] : (otherlv_0= 'set x to' ( (lv_x_1_0= ruleNumberTerminal ) ) ) ;
    public final EObject ruleSetX() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_x_1_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1952:28: ( (otherlv_0= 'set x to' ( (lv_x_1_0= ruleNumberTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1953:1: (otherlv_0= 'set x to' ( (lv_x_1_0= ruleNumberTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1953:1: (otherlv_0= 'set x to' ( (lv_x_1_0= ruleNumberTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1953:3: otherlv_0= 'set x to' ( (lv_x_1_0= ruleNumberTerminal ) )
            {
            otherlv_0=(Token)match(input,43,FOLLOW_43_in_ruleSetX4537); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getSetXAccess().getSetXToKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1957:1: ( (lv_x_1_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1958:1: (lv_x_1_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1958:1: (lv_x_1_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1959:3: lv_x_1_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getSetXAccess().getXNumberTerminalParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleSetX4558);
            lv_x_1_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getSetXRule());
              	        }
                     		set(
                     			current, 
                     			"x",
                      		lv_x_1_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSetX"


    // $ANTLR start "entryRuleSetY"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1983:1: entryRuleSetY returns [EObject current=null] : iv_ruleSetY= ruleSetY EOF ;
    public final EObject entryRuleSetY() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSetY = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1984:2: (iv_ruleSetY= ruleSetY EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1985:2: iv_ruleSetY= ruleSetY EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSetYRule()); 
            }
            pushFollow(FOLLOW_ruleSetY_in_entryRuleSetY4594);
            iv_ruleSetY=ruleSetY();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSetY; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSetY4604); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSetY"


    // $ANTLR start "ruleSetY"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1992:1: ruleSetY returns [EObject current=null] : (otherlv_0= 'set y to' ( (lv_y_1_0= ruleNumberTerminal ) ) ) ;
    public final EObject ruleSetY() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_y_1_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1995:28: ( (otherlv_0= 'set y to' ( (lv_y_1_0= ruleNumberTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1996:1: (otherlv_0= 'set y to' ( (lv_y_1_0= ruleNumberTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1996:1: (otherlv_0= 'set y to' ( (lv_y_1_0= ruleNumberTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1996:3: otherlv_0= 'set y to' ( (lv_y_1_0= ruleNumberTerminal ) )
            {
            otherlv_0=(Token)match(input,44,FOLLOW_44_in_ruleSetY4641); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getSetYAccess().getSetYToKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2000:1: ( (lv_y_1_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2001:1: (lv_y_1_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2001:1: (lv_y_1_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2002:3: lv_y_1_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getSetYAccess().getYNumberTerminalParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleSetY4662);
            lv_y_1_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getSetYRule());
              	        }
                     		set(
                     			current, 
                     			"y",
                      		lv_y_1_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSetY"


    // $ANTLR start "entryRuleIfEdge"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2026:1: entryRuleIfEdge returns [EObject current=null] : iv_ruleIfEdge= ruleIfEdge EOF ;
    public final EObject entryRuleIfEdge() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIfEdge = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2027:2: (iv_ruleIfEdge= ruleIfEdge EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2028:2: iv_ruleIfEdge= ruleIfEdge EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIfEdgeRule()); 
            }
            pushFollow(FOLLOW_ruleIfEdge_in_entryRuleIfEdge4698);
            iv_ruleIfEdge=ruleIfEdge();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIfEdge; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIfEdge4708); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIfEdge"


    // $ANTLR start "ruleIfEdge"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2035:1: ruleIfEdge returns [EObject current=null] : (otherlv_0= 'if on edge, bounce' () ) ;
    public final EObject ruleIfEdge() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2038:28: ( (otherlv_0= 'if on edge, bounce' () ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2039:1: (otherlv_0= 'if on edge, bounce' () )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2039:1: (otherlv_0= 'if on edge, bounce' () )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2039:3: otherlv_0= 'if on edge, bounce' ()
            {
            otherlv_0=(Token)match(input,45,FOLLOW_45_in_ruleIfEdge4745); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getIfEdgeAccess().getIfOnEdgeBounceKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2043:1: ()
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2044:2: 
            {
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getIfEdgeAccess().getIfEdgeAction_1(),
                          current);
                  
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIfEdge"


    // $ANTLR start "entryRuleNumber"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2060:1: entryRuleNumber returns [EObject current=null] : iv_ruleNumber= ruleNumber EOF ;
    public final EObject entryRuleNumber() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumber = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2061:2: (iv_ruleNumber= ruleNumber EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2062:2: iv_ruleNumber= ruleNumber EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNumberRule()); 
            }
            pushFollow(FOLLOW_ruleNumber_in_entryRuleNumber4793);
            iv_ruleNumber=ruleNumber();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNumber; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumber4803); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumber"


    // $ANTLR start "ruleNumber"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2069:1: ruleNumber returns [EObject current=null] : (this_NumberValue_0= ruleNumberValue | this_NumberObjVar_1= ruleNumberObjVar | this_NumberVar_2= ruleNumberVar | this_NumberVarX_3= ruleNumberVarX | this_NumberVarY_4= ruleNumberVarY | this_NumberVarD_5= ruleNumberVarD ) ;
    public final EObject ruleNumber() throws RecognitionException {
        EObject current = null;

        EObject this_NumberValue_0 = null;

        EObject this_NumberObjVar_1 = null;

        EObject this_NumberVar_2 = null;

        EObject this_NumberVarX_3 = null;

        EObject this_NumberVarY_4 = null;

        EObject this_NumberVarD_5 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2072:28: ( (this_NumberValue_0= ruleNumberValue | this_NumberObjVar_1= ruleNumberObjVar | this_NumberVar_2= ruleNumberVar | this_NumberVarX_3= ruleNumberVarX | this_NumberVarY_4= ruleNumberVarY | this_NumberVarD_5= ruleNumberVarD ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2073:1: (this_NumberValue_0= ruleNumberValue | this_NumberObjVar_1= ruleNumberObjVar | this_NumberVar_2= ruleNumberVar | this_NumberVarX_3= ruleNumberVarX | this_NumberVarY_4= ruleNumberVarY | this_NumberVarD_5= ruleNumberVarD )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2073:1: (this_NumberValue_0= ruleNumberValue | this_NumberObjVar_1= ruleNumberObjVar | this_NumberVar_2= ruleNumberVar | this_NumberVarX_3= ruleNumberVarX | this_NumberVarY_4= ruleNumberVarY | this_NumberVarD_5= ruleNumberVarD )
            int alt17=6;
            alt17 = dfa17.predict(input);
            switch (alt17) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2074:2: this_NumberValue_0= ruleNumberValue
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getNumberAccess().getNumberValueParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleNumberValue_in_ruleNumber4853);
                    this_NumberValue_0=ruleNumberValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_NumberValue_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2087:2: this_NumberObjVar_1= ruleNumberObjVar
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getNumberAccess().getNumberObjVarParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleNumberObjVar_in_ruleNumber4883);
                    this_NumberObjVar_1=ruleNumberObjVar();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_NumberObjVar_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2100:2: this_NumberVar_2= ruleNumberVar
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getNumberAccess().getNumberVarParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleNumberVar_in_ruleNumber4913);
                    this_NumberVar_2=ruleNumberVar();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_NumberVar_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2113:2: this_NumberVarX_3= ruleNumberVarX
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getNumberAccess().getNumberVarXParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleNumberVarX_in_ruleNumber4943);
                    this_NumberVarX_3=ruleNumberVarX();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_NumberVarX_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2126:2: this_NumberVarY_4= ruleNumberVarY
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getNumberAccess().getNumberVarYParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_ruleNumberVarY_in_ruleNumber4973);
                    this_NumberVarY_4=ruleNumberVarY();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_NumberVarY_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2139:2: this_NumberVarD_5= ruleNumberVarD
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getNumberAccess().getNumberVarDParserRuleCall_5()); 
                          
                    }
                    pushFollow(FOLLOW_ruleNumberVarD_in_ruleNumber5003);
                    this_NumberVarD_5=ruleNumberVarD();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_NumberVarD_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumber"


    // $ANTLR start "entryRuleNumberTerminal"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2158:1: entryRuleNumberTerminal returns [EObject current=null] : iv_ruleNumberTerminal= ruleNumberTerminal EOF ;
    public final EObject entryRuleNumberTerminal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumberTerminal = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2159:2: (iv_ruleNumberTerminal= ruleNumberTerminal EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2160:2: iv_ruleNumberTerminal= ruleNumberTerminal EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNumberTerminalRule()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_entryRuleNumberTerminal5038);
            iv_ruleNumberTerminal=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNumberTerminal; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumberTerminal5048); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumberTerminal"


    // $ANTLR start "ruleNumberTerminal"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2167:1: ruleNumberTerminal returns [EObject current=null] : (otherlv_0= '(' this_Number_1= ruleNumber otherlv_2= ')' ) ;
    public final EObject ruleNumberTerminal() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject this_Number_1 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2170:28: ( (otherlv_0= '(' this_Number_1= ruleNumber otherlv_2= ')' ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2171:1: (otherlv_0= '(' this_Number_1= ruleNumber otherlv_2= ')' )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2171:1: (otherlv_0= '(' this_Number_1= ruleNumber otherlv_2= ')' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2171:3: otherlv_0= '(' this_Number_1= ruleNumber otherlv_2= ')'
            {
            otherlv_0=(Token)match(input,46,FOLLOW_46_in_ruleNumberTerminal5085); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getNumberTerminalAccess().getLeftParenthesisKeyword_0());
                  
            }
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getNumberTerminalAccess().getNumberParserRuleCall_1()); 
                  
            }
            pushFollow(FOLLOW_ruleNumber_in_ruleNumberTerminal5110);
            this_Number_1=ruleNumber();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_Number_1; 
                      afterParserOrEnumRuleCall();
                  
            }
            otherlv_2=(Token)match(input,47,FOLLOW_47_in_ruleNumberTerminal5121); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getNumberTerminalAccess().getRightParenthesisKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumberTerminal"


    // $ANTLR start "entryRuleNumberValue"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2199:1: entryRuleNumberValue returns [EObject current=null] : iv_ruleNumberValue= ruleNumberValue EOF ;
    public final EObject entryRuleNumberValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumberValue = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2200:2: (iv_ruleNumberValue= ruleNumberValue EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2201:2: iv_ruleNumberValue= ruleNumberValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNumberValueRule()); 
            }
            pushFollow(FOLLOW_ruleNumberValue_in_entryRuleNumberValue5157);
            iv_ruleNumberValue=ruleNumberValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNumberValue; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumberValue5167); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumberValue"


    // $ANTLR start "ruleNumberValue"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2208:1: ruleNumberValue returns [EObject current=null] : ( (lv_value_0_0= RULE_NUM ) ) ;
    public final EObject ruleNumberValue() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;

         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2211:28: ( ( (lv_value_0_0= RULE_NUM ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2212:1: ( (lv_value_0_0= RULE_NUM ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2212:1: ( (lv_value_0_0= RULE_NUM ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2213:1: (lv_value_0_0= RULE_NUM )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2213:1: (lv_value_0_0= RULE_NUM )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2214:3: lv_value_0_0= RULE_NUM
            {
            lv_value_0_0=(Token)match(input,RULE_NUM,FOLLOW_RULE_NUM_in_ruleNumberValue5208); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_value_0_0, grammarAccess.getNumberValueAccess().getValueNUMTerminalRuleCall_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getNumberValueRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"value",
                      		lv_value_0_0, 
                      		"NUM");
              	    
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumberValue"


    // $ANTLR start "entryRuleNumberVar"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2238:1: entryRuleNumberVar returns [EObject current=null] : iv_ruleNumberVar= ruleNumberVar EOF ;
    public final EObject entryRuleNumberVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumberVar = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2239:2: (iv_ruleNumberVar= ruleNumberVar EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2240:2: iv_ruleNumberVar= ruleNumberVar EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNumberVarRule()); 
            }
            pushFollow(FOLLOW_ruleNumberVar_in_entryRuleNumberVar5248);
            iv_ruleNumberVar=ruleNumberVar();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNumberVar; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumberVar5258); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumberVar"


    // $ANTLR start "ruleNumberVar"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2247:1: ruleNumberVar returns [EObject current=null] : (otherlv_0= '$' ( (lv_var_1_0= RULE_ID ) ) ) ;
    public final EObject ruleNumberVar() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_var_1_0=null;

         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2250:28: ( (otherlv_0= '$' ( (lv_var_1_0= RULE_ID ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2251:1: (otherlv_0= '$' ( (lv_var_1_0= RULE_ID ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2251:1: (otherlv_0= '$' ( (lv_var_1_0= RULE_ID ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2251:3: otherlv_0= '$' ( (lv_var_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,48,FOLLOW_48_in_ruleNumberVar5295); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getNumberVarAccess().getDollarSignKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2255:1: ( (lv_var_1_0= RULE_ID ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2256:1: (lv_var_1_0= RULE_ID )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2256:1: (lv_var_1_0= RULE_ID )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2257:3: lv_var_1_0= RULE_ID
            {
            lv_var_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleNumberVar5312); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_var_1_0, grammarAccess.getNumberVarAccess().getVarIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getNumberVarRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"var",
                      		lv_var_1_0, 
                      		"ID");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumberVar"


    // $ANTLR start "entryRuleNumberVarX"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2281:1: entryRuleNumberVarX returns [EObject current=null] : iv_ruleNumberVarX= ruleNumberVarX EOF ;
    public final EObject entryRuleNumberVarX() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumberVarX = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2282:2: (iv_ruleNumberVarX= ruleNumberVarX EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2283:2: iv_ruleNumberVarX= ruleNumberVarX EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNumberVarXRule()); 
            }
            pushFollow(FOLLOW_ruleNumberVarX_in_entryRuleNumberVarX5353);
            iv_ruleNumberVarX=ruleNumberVarX();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNumberVarX; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumberVarX5363); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumberVarX"


    // $ANTLR start "ruleNumberVarX"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2290:1: ruleNumberVarX returns [EObject current=null] : (otherlv_0= 'x position' () ) ;
    public final EObject ruleNumberVarX() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2293:28: ( (otherlv_0= 'x position' () ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2294:1: (otherlv_0= 'x position' () )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2294:1: (otherlv_0= 'x position' () )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2294:3: otherlv_0= 'x position' ()
            {
            otherlv_0=(Token)match(input,49,FOLLOW_49_in_ruleNumberVarX5400); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getNumberVarXAccess().getXPositionKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2298:1: ()
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2299:2: 
            {
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getNumberVarXAccess().getNumberVarXAction_1(),
                          current);
                  
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumberVarX"


    // $ANTLR start "entryRuleNumberVarY"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2315:1: entryRuleNumberVarY returns [EObject current=null] : iv_ruleNumberVarY= ruleNumberVarY EOF ;
    public final EObject entryRuleNumberVarY() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumberVarY = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2316:2: (iv_ruleNumberVarY= ruleNumberVarY EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2317:2: iv_ruleNumberVarY= ruleNumberVarY EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNumberVarYRule()); 
            }
            pushFollow(FOLLOW_ruleNumberVarY_in_entryRuleNumberVarY5448);
            iv_ruleNumberVarY=ruleNumberVarY();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNumberVarY; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumberVarY5458); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumberVarY"


    // $ANTLR start "ruleNumberVarY"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2324:1: ruleNumberVarY returns [EObject current=null] : (otherlv_0= 'y position' () ) ;
    public final EObject ruleNumberVarY() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2327:28: ( (otherlv_0= 'y position' () ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2328:1: (otherlv_0= 'y position' () )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2328:1: (otherlv_0= 'y position' () )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2328:3: otherlv_0= 'y position' ()
            {
            otherlv_0=(Token)match(input,50,FOLLOW_50_in_ruleNumberVarY5495); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getNumberVarYAccess().getYPositionKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2332:1: ()
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2333:2: 
            {
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getNumberVarYAccess().getNumberVarYAction_1(),
                          current);
                  
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumberVarY"


    // $ANTLR start "entryRuleNumberVarD"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2349:1: entryRuleNumberVarD returns [EObject current=null] : iv_ruleNumberVarD= ruleNumberVarD EOF ;
    public final EObject entryRuleNumberVarD() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumberVarD = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2350:2: (iv_ruleNumberVarD= ruleNumberVarD EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2351:2: iv_ruleNumberVarD= ruleNumberVarD EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNumberVarDRule()); 
            }
            pushFollow(FOLLOW_ruleNumberVarD_in_entryRuleNumberVarD5543);
            iv_ruleNumberVarD=ruleNumberVarD();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNumberVarD; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumberVarD5553); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumberVarD"


    // $ANTLR start "ruleNumberVarD"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2358:1: ruleNumberVarD returns [EObject current=null] : (otherlv_0= 'direction' () ) ;
    public final EObject ruleNumberVarD() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2361:28: ( (otherlv_0= 'direction' () ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2362:1: (otherlv_0= 'direction' () )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2362:1: (otherlv_0= 'direction' () )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2362:3: otherlv_0= 'direction' ()
            {
            otherlv_0=(Token)match(input,51,FOLLOW_51_in_ruleNumberVarD5590); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getNumberVarDAccess().getDirectionKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2366:1: ()
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2367:2: 
            {
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getNumberVarDAccess().getNumberVarDAction_1(),
                          current);
                  
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumberVarD"


    // $ANTLR start "entryRuleNumberObjVar"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2383:1: entryRuleNumberObjVar returns [EObject current=null] : iv_ruleNumberObjVar= ruleNumberObjVar EOF ;
    public final EObject entryRuleNumberObjVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNumberObjVar = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2384:2: (iv_ruleNumberObjVar= ruleNumberObjVar EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2385:2: iv_ruleNumberObjVar= ruleNumberObjVar EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNumberObjVarRule()); 
            }
            pushFollow(FOLLOW_ruleNumberObjVar_in_entryRuleNumberObjVar5638);
            iv_ruleNumberObjVar=ruleNumberObjVar();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNumberObjVar; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumberObjVar5648); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNumberObjVar"


    // $ANTLR start "ruleNumberObjVar"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2392:1: ruleNumberObjVar returns [EObject current=null] : ( ( (lv_var_0_0= ruleObjectVariableEnum ) ) otherlv_1= 'of' ( (lv_obj_2_0= ruleIObject ) ) ) ;
    public final EObject ruleNumberObjVar() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Enumerator lv_var_0_0 = null;

        EObject lv_obj_2_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2395:28: ( ( ( (lv_var_0_0= ruleObjectVariableEnum ) ) otherlv_1= 'of' ( (lv_obj_2_0= ruleIObject ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2396:1: ( ( (lv_var_0_0= ruleObjectVariableEnum ) ) otherlv_1= 'of' ( (lv_obj_2_0= ruleIObject ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2396:1: ( ( (lv_var_0_0= ruleObjectVariableEnum ) ) otherlv_1= 'of' ( (lv_obj_2_0= ruleIObject ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2396:2: ( (lv_var_0_0= ruleObjectVariableEnum ) ) otherlv_1= 'of' ( (lv_obj_2_0= ruleIObject ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2396:2: ( (lv_var_0_0= ruleObjectVariableEnum ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2397:1: (lv_var_0_0= ruleObjectVariableEnum )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2397:1: (lv_var_0_0= ruleObjectVariableEnum )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2398:3: lv_var_0_0= ruleObjectVariableEnum
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getNumberObjVarAccess().getVarObjectVariableEnumEnumRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleObjectVariableEnum_in_ruleNumberObjVar5694);
            lv_var_0_0=ruleObjectVariableEnum();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getNumberObjVarRule());
              	        }
                     		set(
                     			current, 
                     			"var",
                      		lv_var_0_0, 
                      		"ObjectVariableEnum");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,52,FOLLOW_52_in_ruleNumberObjVar5706); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getNumberObjVarAccess().getOfKeyword_1());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2418:1: ( (lv_obj_2_0= ruleIObject ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2419:1: (lv_obj_2_0= ruleIObject )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2419:1: (lv_obj_2_0= ruleIObject )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2420:3: lv_obj_2_0= ruleIObject
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getNumberObjVarAccess().getObjIObjectParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleIObject_in_ruleNumberObjVar5727);
            lv_obj_2_0=ruleIObject();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getNumberObjVarRule());
              	        }
                     		set(
                     			current, 
                     			"obj",
                      		lv_obj_2_0, 
                      		"IObject");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNumberObjVar"


    // $ANTLR start "entryRuleBinary"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2444:1: entryRuleBinary returns [EObject current=null] : iv_ruleBinary= ruleBinary EOF ;
    public final EObject entryRuleBinary() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBinary = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2445:2: (iv_ruleBinary= ruleBinary EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2446:2: iv_ruleBinary= ruleBinary EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBinaryRule()); 
            }
            pushFollow(FOLLOW_ruleBinary_in_entryRuleBinary5763);
            iv_ruleBinary=ruleBinary();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBinary; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBinary5773); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinary"


    // $ANTLR start "ruleBinary"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2453:1: ruleBinary returns [EObject current=null] : (this_Or_0= ruleOr | this_And_1= ruleAnd | this_Lt_2= ruleLt | this_Gt_3= ruleGt | this_Eq_4= ruleEq | this_Not_5= ruleNot | this_TouchsColor_6= ruleTouchsColor | this_BinaryVar_7= ruleBinaryVar ) ;
    public final EObject ruleBinary() throws RecognitionException {
        EObject current = null;

        EObject this_Or_0 = null;

        EObject this_And_1 = null;

        EObject this_Lt_2 = null;

        EObject this_Gt_3 = null;

        EObject this_Eq_4 = null;

        EObject this_Not_5 = null;

        EObject this_TouchsColor_6 = null;

        EObject this_BinaryVar_7 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2456:28: ( (this_Or_0= ruleOr | this_And_1= ruleAnd | this_Lt_2= ruleLt | this_Gt_3= ruleGt | this_Eq_4= ruleEq | this_Not_5= ruleNot | this_TouchsColor_6= ruleTouchsColor | this_BinaryVar_7= ruleBinaryVar ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2457:1: (this_Or_0= ruleOr | this_And_1= ruleAnd | this_Lt_2= ruleLt | this_Gt_3= ruleGt | this_Eq_4= ruleEq | this_Not_5= ruleNot | this_TouchsColor_6= ruleTouchsColor | this_BinaryVar_7= ruleBinaryVar )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2457:1: (this_Or_0= ruleOr | this_And_1= ruleAnd | this_Lt_2= ruleLt | this_Gt_3= ruleGt | this_Eq_4= ruleEq | this_Not_5= ruleNot | this_TouchsColor_6= ruleTouchsColor | this_BinaryVar_7= ruleBinaryVar )
            int alt18=8;
            alt18 = dfa18.predict(input);
            switch (alt18) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2458:2: this_Or_0= ruleOr
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBinaryAccess().getOrParserRuleCall_0()); 
                          
                    }
                    pushFollow(FOLLOW_ruleOr_in_ruleBinary5823);
                    this_Or_0=ruleOr();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Or_0; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 2 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2471:2: this_And_1= ruleAnd
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBinaryAccess().getAndParserRuleCall_1()); 
                          
                    }
                    pushFollow(FOLLOW_ruleAnd_in_ruleBinary5853);
                    this_And_1=ruleAnd();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_And_1; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 3 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2484:2: this_Lt_2= ruleLt
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBinaryAccess().getLtParserRuleCall_2()); 
                          
                    }
                    pushFollow(FOLLOW_ruleLt_in_ruleBinary5883);
                    this_Lt_2=ruleLt();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Lt_2; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 4 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2497:2: this_Gt_3= ruleGt
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBinaryAccess().getGtParserRuleCall_3()); 
                          
                    }
                    pushFollow(FOLLOW_ruleGt_in_ruleBinary5913);
                    this_Gt_3=ruleGt();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Gt_3; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 5 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2510:2: this_Eq_4= ruleEq
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBinaryAccess().getEqParserRuleCall_4()); 
                          
                    }
                    pushFollow(FOLLOW_ruleEq_in_ruleBinary5943);
                    this_Eq_4=ruleEq();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Eq_4; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 6 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2523:2: this_Not_5= ruleNot
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBinaryAccess().getNotParserRuleCall_5()); 
                          
                    }
                    pushFollow(FOLLOW_ruleNot_in_ruleBinary5973);
                    this_Not_5=ruleNot();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_Not_5; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 7 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2536:2: this_TouchsColor_6= ruleTouchsColor
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBinaryAccess().getTouchsColorParserRuleCall_6()); 
                          
                    }
                    pushFollow(FOLLOW_ruleTouchsColor_in_ruleBinary6003);
                    this_TouchsColor_6=ruleTouchsColor();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_TouchsColor_6; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;
                case 8 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2549:2: this_BinaryVar_7= ruleBinaryVar
                    {
                    if ( state.backtracking==0 ) {
                       
                      	  /* */ 
                      	
                    }
                    if ( state.backtracking==0 ) {
                       
                              newCompositeNode(grammarAccess.getBinaryAccess().getBinaryVarParserRuleCall_7()); 
                          
                    }
                    pushFollow(FOLLOW_ruleBinaryVar_in_ruleBinary6033);
                    this_BinaryVar_7=ruleBinaryVar();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {
                       
                              current = this_BinaryVar_7; 
                              afterParserOrEnumRuleCall();
                          
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinary"


    // $ANTLR start "entryRuleBinaryTerminal"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2568:1: entryRuleBinaryTerminal returns [EObject current=null] : iv_ruleBinaryTerminal= ruleBinaryTerminal EOF ;
    public final EObject entryRuleBinaryTerminal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBinaryTerminal = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2569:2: (iv_ruleBinaryTerminal= ruleBinaryTerminal EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2570:2: iv_ruleBinaryTerminal= ruleBinaryTerminal EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBinaryTerminalRule()); 
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_entryRuleBinaryTerminal6068);
            iv_ruleBinaryTerminal=ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBinaryTerminal; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBinaryTerminal6078); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinaryTerminal"


    // $ANTLR start "ruleBinaryTerminal"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2577:1: ruleBinaryTerminal returns [EObject current=null] : (otherlv_0= '(' this_Binary_1= ruleBinary otherlv_2= ')' ) ;
    public final EObject ruleBinaryTerminal() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject this_Binary_1 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2580:28: ( (otherlv_0= '(' this_Binary_1= ruleBinary otherlv_2= ')' ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2581:1: (otherlv_0= '(' this_Binary_1= ruleBinary otherlv_2= ')' )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2581:1: (otherlv_0= '(' this_Binary_1= ruleBinary otherlv_2= ')' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2581:3: otherlv_0= '(' this_Binary_1= ruleBinary otherlv_2= ')'
            {
            otherlv_0=(Token)match(input,46,FOLLOW_46_in_ruleBinaryTerminal6115); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getBinaryTerminalAccess().getLeftParenthesisKeyword_0());
                  
            }
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {
               
                      newCompositeNode(grammarAccess.getBinaryTerminalAccess().getBinaryParserRuleCall_1()); 
                  
            }
            pushFollow(FOLLOW_ruleBinary_in_ruleBinaryTerminal6140);
            this_Binary_1=ruleBinary();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               
                      current = this_Binary_1; 
                      afterParserOrEnumRuleCall();
                  
            }
            otherlv_2=(Token)match(input,47,FOLLOW_47_in_ruleBinaryTerminal6151); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_2, grammarAccess.getBinaryTerminalAccess().getRightParenthesisKeyword_2());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinaryTerminal"


    // $ANTLR start "entryRuleBinaryVar"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2609:1: entryRuleBinaryVar returns [EObject current=null] : iv_ruleBinaryVar= ruleBinaryVar EOF ;
    public final EObject entryRuleBinaryVar() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBinaryVar = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2610:2: (iv_ruleBinaryVar= ruleBinaryVar EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2611:2: iv_ruleBinaryVar= ruleBinaryVar EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBinaryVarRule()); 
            }
            pushFollow(FOLLOW_ruleBinaryVar_in_entryRuleBinaryVar6187);
            iv_ruleBinaryVar=ruleBinaryVar();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBinaryVar; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBinaryVar6197); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinaryVar"


    // $ANTLR start "ruleBinaryVar"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2618:1: ruleBinaryVar returns [EObject current=null] : (otherlv_0= '$' ( (lv_var_1_0= RULE_ID ) ) ) ;
    public final EObject ruleBinaryVar() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_var_1_0=null;

         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2621:28: ( (otherlv_0= '$' ( (lv_var_1_0= RULE_ID ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2622:1: (otherlv_0= '$' ( (lv_var_1_0= RULE_ID ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2622:1: (otherlv_0= '$' ( (lv_var_1_0= RULE_ID ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2622:3: otherlv_0= '$' ( (lv_var_1_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,48,FOLLOW_48_in_ruleBinaryVar6234); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getBinaryVarAccess().getDollarSignKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2626:1: ( (lv_var_1_0= RULE_ID ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2627:1: (lv_var_1_0= RULE_ID )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2627:1: (lv_var_1_0= RULE_ID )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2628:3: lv_var_1_0= RULE_ID
            {
            lv_var_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleBinaryVar6251); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_var_1_0, grammarAccess.getBinaryVarAccess().getVarIDTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getBinaryVarRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"var",
                      		lv_var_1_0, 
                      		"ID");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinaryVar"


    // $ANTLR start "entryRuleTouchsColor"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2652:1: entryRuleTouchsColor returns [EObject current=null] : iv_ruleTouchsColor= ruleTouchsColor EOF ;
    public final EObject entryRuleTouchsColor() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTouchsColor = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2653:2: (iv_ruleTouchsColor= ruleTouchsColor EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2654:2: iv_ruleTouchsColor= ruleTouchsColor EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getTouchsColorRule()); 
            }
            pushFollow(FOLLOW_ruleTouchsColor_in_entryRuleTouchsColor6292);
            iv_ruleTouchsColor=ruleTouchsColor();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleTouchsColor; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTouchsColor6302); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTouchsColor"


    // $ANTLR start "ruleTouchsColor"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2661:1: ruleTouchsColor returns [EObject current=null] : (otherlv_0= 'touching' otherlv_1= 'color' ( (lv_color_2_0= RULE_STRING ) ) otherlv_3= '?' ) ;
    public final EObject ruleTouchsColor() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_color_2_0=null;
        Token otherlv_3=null;

         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2664:28: ( (otherlv_0= 'touching' otherlv_1= 'color' ( (lv_color_2_0= RULE_STRING ) ) otherlv_3= '?' ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2665:1: (otherlv_0= 'touching' otherlv_1= 'color' ( (lv_color_2_0= RULE_STRING ) ) otherlv_3= '?' )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2665:1: (otherlv_0= 'touching' otherlv_1= 'color' ( (lv_color_2_0= RULE_STRING ) ) otherlv_3= '?' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2665:3: otherlv_0= 'touching' otherlv_1= 'color' ( (lv_color_2_0= RULE_STRING ) ) otherlv_3= '?'
            {
            otherlv_0=(Token)match(input,53,FOLLOW_53_in_ruleTouchsColor6339); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getTouchsColorAccess().getTouchingKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,54,FOLLOW_54_in_ruleTouchsColor6351); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getTouchsColorAccess().getColorKeyword_1());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2673:1: ( (lv_color_2_0= RULE_STRING ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2674:1: (lv_color_2_0= RULE_STRING )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2674:1: (lv_color_2_0= RULE_STRING )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2675:3: lv_color_2_0= RULE_STRING
            {
            lv_color_2_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleTouchsColor6368); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_color_2_0, grammarAccess.getTouchsColorAccess().getColorSTRINGTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getTouchsColorRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"color",
                      		lv_color_2_0, 
                      		"STRING");
              	    
            }

            }


            }

            otherlv_3=(Token)match(input,55,FOLLOW_55_in_ruleTouchsColor6385); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_3, grammarAccess.getTouchsColorAccess().getQuestionMarkKeyword_3());
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTouchsColor"


    // $ANTLR start "entryRuleLt"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2703:1: entryRuleLt returns [EObject current=null] : iv_ruleLt= ruleLt EOF ;
    public final EObject entryRuleLt() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLt = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2704:2: (iv_ruleLt= ruleLt EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2705:2: iv_ruleLt= ruleLt EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getLtRule()); 
            }
            pushFollow(FOLLOW_ruleLt_in_entryRuleLt6421);
            iv_ruleLt=ruleLt();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleLt; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLt6431); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLt"


    // $ANTLR start "ruleLt"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2712:1: ruleLt returns [EObject current=null] : ( ( (lv_left_0_0= ruleNumberTerminal ) ) otherlv_1= '<' ( (lv_right_2_0= ruleNumberTerminal ) ) ) ;
    public final EObject ruleLt() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_left_0_0 = null;

        EObject lv_right_2_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2715:28: ( ( ( (lv_left_0_0= ruleNumberTerminal ) ) otherlv_1= '<' ( (lv_right_2_0= ruleNumberTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2716:1: ( ( (lv_left_0_0= ruleNumberTerminal ) ) otherlv_1= '<' ( (lv_right_2_0= ruleNumberTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2716:1: ( ( (lv_left_0_0= ruleNumberTerminal ) ) otherlv_1= '<' ( (lv_right_2_0= ruleNumberTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2716:2: ( (lv_left_0_0= ruleNumberTerminal ) ) otherlv_1= '<' ( (lv_right_2_0= ruleNumberTerminal ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2716:2: ( (lv_left_0_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2717:1: (lv_left_0_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2717:1: (lv_left_0_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2718:3: lv_left_0_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLtAccess().getLeftNumberTerminalParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleLt6477);
            lv_left_0_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLtRule());
              	        }
                     		set(
                     			current, 
                     			"left",
                      		lv_left_0_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,56,FOLLOW_56_in_ruleLt6489); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getLtAccess().getLessThanSignKeyword_1());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2738:1: ( (lv_right_2_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2739:1: (lv_right_2_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2739:1: (lv_right_2_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2740:3: lv_right_2_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getLtAccess().getRightNumberTerminalParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleLt6510);
            lv_right_2_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getLtRule());
              	        }
                     		set(
                     			current, 
                     			"right",
                      		lv_right_2_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLt"


    // $ANTLR start "entryRuleGt"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2764:1: entryRuleGt returns [EObject current=null] : iv_ruleGt= ruleGt EOF ;
    public final EObject entryRuleGt() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGt = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2765:2: (iv_ruleGt= ruleGt EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2766:2: iv_ruleGt= ruleGt EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getGtRule()); 
            }
            pushFollow(FOLLOW_ruleGt_in_entryRuleGt6546);
            iv_ruleGt=ruleGt();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleGt; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGt6556); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGt"


    // $ANTLR start "ruleGt"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2773:1: ruleGt returns [EObject current=null] : ( ( (lv_left_0_0= ruleNumberTerminal ) ) otherlv_1= '>' ( (lv_right_2_0= ruleNumberTerminal ) ) ) ;
    public final EObject ruleGt() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_left_0_0 = null;

        EObject lv_right_2_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2776:28: ( ( ( (lv_left_0_0= ruleNumberTerminal ) ) otherlv_1= '>' ( (lv_right_2_0= ruleNumberTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2777:1: ( ( (lv_left_0_0= ruleNumberTerminal ) ) otherlv_1= '>' ( (lv_right_2_0= ruleNumberTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2777:1: ( ( (lv_left_0_0= ruleNumberTerminal ) ) otherlv_1= '>' ( (lv_right_2_0= ruleNumberTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2777:2: ( (lv_left_0_0= ruleNumberTerminal ) ) otherlv_1= '>' ( (lv_right_2_0= ruleNumberTerminal ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2777:2: ( (lv_left_0_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2778:1: (lv_left_0_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2778:1: (lv_left_0_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2779:3: lv_left_0_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getGtAccess().getLeftNumberTerminalParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleGt6602);
            lv_left_0_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getGtRule());
              	        }
                     		set(
                     			current, 
                     			"left",
                      		lv_left_0_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,57,FOLLOW_57_in_ruleGt6614); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getGtAccess().getGreaterThanSignKeyword_1());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2799:1: ( (lv_right_2_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2800:1: (lv_right_2_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2800:1: (lv_right_2_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2801:3: lv_right_2_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getGtAccess().getRightNumberTerminalParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleGt6635);
            lv_right_2_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getGtRule());
              	        }
                     		set(
                     			current, 
                     			"right",
                      		lv_right_2_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGt"


    // $ANTLR start "entryRuleEq"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2825:1: entryRuleEq returns [EObject current=null] : iv_ruleEq= ruleEq EOF ;
    public final EObject entryRuleEq() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEq = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2826:2: (iv_ruleEq= ruleEq EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2827:2: iv_ruleEq= ruleEq EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getEqRule()); 
            }
            pushFollow(FOLLOW_ruleEq_in_entryRuleEq6671);
            iv_ruleEq=ruleEq();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleEq; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEq6681); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEq"


    // $ANTLR start "ruleEq"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2834:1: ruleEq returns [EObject current=null] : ( ( (lv_left_0_0= ruleNumberTerminal ) ) otherlv_1= '=' ( (lv_right_2_0= ruleNumberTerminal ) ) ) ;
    public final EObject ruleEq() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_left_0_0 = null;

        EObject lv_right_2_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2837:28: ( ( ( (lv_left_0_0= ruleNumberTerminal ) ) otherlv_1= '=' ( (lv_right_2_0= ruleNumberTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2838:1: ( ( (lv_left_0_0= ruleNumberTerminal ) ) otherlv_1= '=' ( (lv_right_2_0= ruleNumberTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2838:1: ( ( (lv_left_0_0= ruleNumberTerminal ) ) otherlv_1= '=' ( (lv_right_2_0= ruleNumberTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2838:2: ( (lv_left_0_0= ruleNumberTerminal ) ) otherlv_1= '=' ( (lv_right_2_0= ruleNumberTerminal ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2838:2: ( (lv_left_0_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2839:1: (lv_left_0_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2839:1: (lv_left_0_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2840:3: lv_left_0_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEqAccess().getLeftNumberTerminalParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleEq6727);
            lv_left_0_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getEqRule());
              	        }
                     		set(
                     			current, 
                     			"left",
                      		lv_left_0_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,58,FOLLOW_58_in_ruleEq6739); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getEqAccess().getEqualsSignKeyword_1());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2860:1: ( (lv_right_2_0= ruleNumberTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2861:1: (lv_right_2_0= ruleNumberTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2861:1: (lv_right_2_0= ruleNumberTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2862:3: lv_right_2_0= ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getEqAccess().getRightNumberTerminalParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_ruleEq6760);
            lv_right_2_0=ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getEqRule());
              	        }
                     		set(
                     			current, 
                     			"right",
                      		lv_right_2_0, 
                      		"NumberTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEq"


    // $ANTLR start "entryRuleAnd"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2886:1: entryRuleAnd returns [EObject current=null] : iv_ruleAnd= ruleAnd EOF ;
    public final EObject entryRuleAnd() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAnd = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2887:2: (iv_ruleAnd= ruleAnd EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2888:2: iv_ruleAnd= ruleAnd EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAndRule()); 
            }
            pushFollow(FOLLOW_ruleAnd_in_entryRuleAnd6796);
            iv_ruleAnd=ruleAnd();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAnd; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAnd6806); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2895:1: ruleAnd returns [EObject current=null] : ( ( (lv_left_0_0= ruleBinaryTerminal ) ) otherlv_1= 'and' ( (lv_right_2_0= ruleBinaryTerminal ) ) ) ;
    public final EObject ruleAnd() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_left_0_0 = null;

        EObject lv_right_2_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2898:28: ( ( ( (lv_left_0_0= ruleBinaryTerminal ) ) otherlv_1= 'and' ( (lv_right_2_0= ruleBinaryTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2899:1: ( ( (lv_left_0_0= ruleBinaryTerminal ) ) otherlv_1= 'and' ( (lv_right_2_0= ruleBinaryTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2899:1: ( ( (lv_left_0_0= ruleBinaryTerminal ) ) otherlv_1= 'and' ( (lv_right_2_0= ruleBinaryTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2899:2: ( (lv_left_0_0= ruleBinaryTerminal ) ) otherlv_1= 'and' ( (lv_right_2_0= ruleBinaryTerminal ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2899:2: ( (lv_left_0_0= ruleBinaryTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2900:1: (lv_left_0_0= ruleBinaryTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2900:1: (lv_left_0_0= ruleBinaryTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2901:3: lv_left_0_0= ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getAndAccess().getLeftBinaryTerminalParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_ruleAnd6852);
            lv_left_0_0=ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getAndRule());
              	        }
                     		set(
                     			current, 
                     			"left",
                      		lv_left_0_0, 
                      		"BinaryTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,59,FOLLOW_59_in_ruleAnd6864); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getAndAccess().getAndKeyword_1());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2921:1: ( (lv_right_2_0= ruleBinaryTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2922:1: (lv_right_2_0= ruleBinaryTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2922:1: (lv_right_2_0= ruleBinaryTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2923:3: lv_right_2_0= ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getAndAccess().getRightBinaryTerminalParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_ruleAnd6885);
            lv_right_2_0=ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getAndRule());
              	        }
                     		set(
                     			current, 
                     			"right",
                      		lv_right_2_0, 
                      		"BinaryTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleOr"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2947:1: entryRuleOr returns [EObject current=null] : iv_ruleOr= ruleOr EOF ;
    public final EObject entryRuleOr() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOr = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2948:2: (iv_ruleOr= ruleOr EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2949:2: iv_ruleOr= ruleOr EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOrRule()); 
            }
            pushFollow(FOLLOW_ruleOr_in_entryRuleOr6921);
            iv_ruleOr=ruleOr();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOr; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOr6931); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2956:1: ruleOr returns [EObject current=null] : ( ( (lv_left_0_0= ruleBinaryTerminal ) ) otherlv_1= 'or' ( (lv_right_2_0= ruleBinaryTerminal ) ) ) ;
    public final EObject ruleOr() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_left_0_0 = null;

        EObject lv_right_2_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2959:28: ( ( ( (lv_left_0_0= ruleBinaryTerminal ) ) otherlv_1= 'or' ( (lv_right_2_0= ruleBinaryTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2960:1: ( ( (lv_left_0_0= ruleBinaryTerminal ) ) otherlv_1= 'or' ( (lv_right_2_0= ruleBinaryTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2960:1: ( ( (lv_left_0_0= ruleBinaryTerminal ) ) otherlv_1= 'or' ( (lv_right_2_0= ruleBinaryTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2960:2: ( (lv_left_0_0= ruleBinaryTerminal ) ) otherlv_1= 'or' ( (lv_right_2_0= ruleBinaryTerminal ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2960:2: ( (lv_left_0_0= ruleBinaryTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2961:1: (lv_left_0_0= ruleBinaryTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2961:1: (lv_left_0_0= ruleBinaryTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2962:3: lv_left_0_0= ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getOrAccess().getLeftBinaryTerminalParserRuleCall_0_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_ruleOr6977);
            lv_left_0_0=ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getOrRule());
              	        }
                     		set(
                     			current, 
                     			"left",
                      		lv_left_0_0, 
                      		"BinaryTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }

            otherlv_1=(Token)match(input,60,FOLLOW_60_in_ruleOr6989); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getOrAccess().getOrKeyword_1());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2982:1: ( (lv_right_2_0= ruleBinaryTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2983:1: (lv_right_2_0= ruleBinaryTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2983:1: (lv_right_2_0= ruleBinaryTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2984:3: lv_right_2_0= ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getOrAccess().getRightBinaryTerminalParserRuleCall_2_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_ruleOr7010);
            lv_right_2_0=ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getOrRule());
              	        }
                     		set(
                     			current, 
                     			"right",
                      		lv_right_2_0, 
                      		"BinaryTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleNot"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3008:1: entryRuleNot returns [EObject current=null] : iv_ruleNot= ruleNot EOF ;
    public final EObject entryRuleNot() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNot = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3009:2: (iv_ruleNot= ruleNot EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3010:2: iv_ruleNot= ruleNot EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNotRule()); 
            }
            pushFollow(FOLLOW_ruleNot_in_entryRuleNot7046);
            iv_ruleNot=ruleNot();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNot; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNot7056); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3017:1: ruleNot returns [EObject current=null] : (otherlv_0= 'not' ( (lv_value_1_0= ruleBinaryTerminal ) ) ) ;
    public final EObject ruleNot() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_value_1_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3020:28: ( (otherlv_0= 'not' ( (lv_value_1_0= ruleBinaryTerminal ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3021:1: (otherlv_0= 'not' ( (lv_value_1_0= ruleBinaryTerminal ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3021:1: (otherlv_0= 'not' ( (lv_value_1_0= ruleBinaryTerminal ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3021:3: otherlv_0= 'not' ( (lv_value_1_0= ruleBinaryTerminal ) )
            {
            otherlv_0=(Token)match(input,61,FOLLOW_61_in_ruleNot7093); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getNotAccess().getNotKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3025:1: ( (lv_value_1_0= ruleBinaryTerminal ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3026:1: (lv_value_1_0= ruleBinaryTerminal )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3026:1: (lv_value_1_0= ruleBinaryTerminal )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3027:3: lv_value_1_0= ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               
              	        newCompositeNode(grammarAccess.getNotAccess().getValueBinaryTerminalParserRuleCall_1_0()); 
              	    
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_ruleNot7114);
            lv_value_1_0=ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElementForParent(grammarAccess.getNotRule());
              	        }
                     		set(
                     			current, 
                     			"value",
                      		lv_value_1_0, 
                      		"BinaryTerminal");
              	        afterParserOrEnumRuleCall();
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRuleHide"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3051:1: entryRuleHide returns [EObject current=null] : iv_ruleHide= ruleHide EOF ;
    public final EObject entryRuleHide() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleHide = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3052:2: (iv_ruleHide= ruleHide EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3053:2: iv_ruleHide= ruleHide EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getHideRule()); 
            }
            pushFollow(FOLLOW_ruleHide_in_entryRuleHide7150);
            iv_ruleHide=ruleHide();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleHide; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleHide7160); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleHide"


    // $ANTLR start "ruleHide"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3060:1: ruleHide returns [EObject current=null] : (otherlv_0= 'hide' () ) ;
    public final EObject ruleHide() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3063:28: ( (otherlv_0= 'hide' () ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3064:1: (otherlv_0= 'hide' () )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3064:1: (otherlv_0= 'hide' () )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3064:3: otherlv_0= 'hide' ()
            {
            otherlv_0=(Token)match(input,62,FOLLOW_62_in_ruleHide7197); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getHideAccess().getHideKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3068:1: ()
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3069:2: 
            {
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getHideAccess().getHideAction_1(),
                          current);
                  
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleHide"


    // $ANTLR start "entryRuleShow"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3085:1: entryRuleShow returns [EObject current=null] : iv_ruleShow= ruleShow EOF ;
    public final EObject entryRuleShow() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleShow = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3086:2: (iv_ruleShow= ruleShow EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3087:2: iv_ruleShow= ruleShow EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getShowRule()); 
            }
            pushFollow(FOLLOW_ruleShow_in_entryRuleShow7245);
            iv_ruleShow=ruleShow();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleShow; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleShow7255); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleShow"


    // $ANTLR start "ruleShow"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3094:1: ruleShow returns [EObject current=null] : (otherlv_0= 'show' () ) ;
    public final EObject ruleShow() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3097:28: ( (otherlv_0= 'show' () ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3098:1: (otherlv_0= 'show' () )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3098:1: (otherlv_0= 'show' () )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3098:3: otherlv_0= 'show' ()
            {
            otherlv_0=(Token)match(input,63,FOLLOW_63_in_ruleShow7292); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getShowAccess().getShowKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3102:1: ()
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3103:2: 
            {
            if ( state.backtracking==0 ) {
               
              	  /* */ 
              	
            }
            if ( state.backtracking==0 ) {

                      current = forceCreateModelElement(
                          grammarAccess.getShowAccess().getShowAction_1(),
                          current);
                  
            }

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleShow"


    // $ANTLR start "entryRuleSay"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3119:1: entryRuleSay returns [EObject current=null] : iv_ruleSay= ruleSay EOF ;
    public final EObject entryRuleSay() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSay = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3120:2: (iv_ruleSay= ruleSay EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3121:2: iv_ruleSay= ruleSay EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getSayRule()); 
            }
            pushFollow(FOLLOW_ruleSay_in_entryRuleSay7340);
            iv_ruleSay=ruleSay();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleSay; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSay7350); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSay"


    // $ANTLR start "ruleSay"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3128:1: ruleSay returns [EObject current=null] : (otherlv_0= 'say' ( (lv_sentence_1_0= RULE_STRING ) ) (otherlv_2= 'for' ( (lv_delay_3_0= ruleNumberTerminal ) ) otherlv_4= 'secs' )? ) ;
    public final EObject ruleSay() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_sentence_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_delay_3_0 = null;


         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3131:28: ( (otherlv_0= 'say' ( (lv_sentence_1_0= RULE_STRING ) ) (otherlv_2= 'for' ( (lv_delay_3_0= ruleNumberTerminal ) ) otherlv_4= 'secs' )? ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3132:1: (otherlv_0= 'say' ( (lv_sentence_1_0= RULE_STRING ) ) (otherlv_2= 'for' ( (lv_delay_3_0= ruleNumberTerminal ) ) otherlv_4= 'secs' )? )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3132:1: (otherlv_0= 'say' ( (lv_sentence_1_0= RULE_STRING ) ) (otherlv_2= 'for' ( (lv_delay_3_0= ruleNumberTerminal ) ) otherlv_4= 'secs' )? )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3132:3: otherlv_0= 'say' ( (lv_sentence_1_0= RULE_STRING ) ) (otherlv_2= 'for' ( (lv_delay_3_0= ruleNumberTerminal ) ) otherlv_4= 'secs' )?
            {
            otherlv_0=(Token)match(input,64,FOLLOW_64_in_ruleSay7387); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getSayAccess().getSayKeyword_0());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3136:1: ( (lv_sentence_1_0= RULE_STRING ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3137:1: (lv_sentence_1_0= RULE_STRING )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3137:1: (lv_sentence_1_0= RULE_STRING )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3138:3: lv_sentence_1_0= RULE_STRING
            {
            lv_sentence_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleSay7404); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_sentence_1_0, grammarAccess.getSayAccess().getSentenceSTRINGTerminalRuleCall_1_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getSayRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"sentence",
                      		lv_sentence_1_0, 
                      		"STRING");
              	    
            }

            }


            }

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3154:2: (otherlv_2= 'for' ( (lv_delay_3_0= ruleNumberTerminal ) ) otherlv_4= 'secs' )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==65) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3154:4: otherlv_2= 'for' ( (lv_delay_3_0= ruleNumberTerminal ) ) otherlv_4= 'secs'
                    {
                    otherlv_2=(Token)match(input,65,FOLLOW_65_in_ruleSay7422); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_2, grammarAccess.getSayAccess().getForKeyword_2_0());
                          
                    }
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3158:1: ( (lv_delay_3_0= ruleNumberTerminal ) )
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3159:1: (lv_delay_3_0= ruleNumberTerminal )
                    {
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3159:1: (lv_delay_3_0= ruleNumberTerminal )
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3160:3: lv_delay_3_0= ruleNumberTerminal
                    {
                    if ( state.backtracking==0 ) {
                       
                      	        newCompositeNode(grammarAccess.getSayAccess().getDelayNumberTerminalParserRuleCall_2_1_0()); 
                      	    
                    }
                    pushFollow(FOLLOW_ruleNumberTerminal_in_ruleSay7443);
                    lv_delay_3_0=ruleNumberTerminal();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      	        if (current==null) {
                      	            current = createModelElementForParent(grammarAccess.getSayRule());
                      	        }
                             		set(
                             			current, 
                             			"delay",
                              		lv_delay_3_0, 
                              		"NumberTerminal");
                      	        afterParserOrEnumRuleCall();
                      	    
                    }

                    }


                    }

                    otherlv_4=(Token)match(input,25,FOLLOW_25_in_ruleSay7455); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                          	newLeafNode(otherlv_4, grammarAccess.getSayAccess().getSecsKeyword_2_2());
                          
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSay"


    // $ANTLR start "entryRulePlay"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3188:1: entryRulePlay returns [EObject current=null] : iv_rulePlay= rulePlay EOF ;
    public final EObject entryRulePlay() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePlay = null;


        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3189:2: (iv_rulePlay= rulePlay EOF )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3190:2: iv_rulePlay= rulePlay EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPlayRule()); 
            }
            pushFollow(FOLLOW_rulePlay_in_entryRulePlay7493);
            iv_rulePlay=rulePlay();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePlay; 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePlay7503); if (state.failed) return current;

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePlay"


    // $ANTLR start "rulePlay"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3197:1: rulePlay returns [EObject current=null] : (otherlv_0= 'play' otherlv_1= 'sound' ( (lv_sound_2_0= RULE_ID ) ) ) ;
    public final EObject rulePlay() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_sound_2_0=null;

         enterRule(); 
            
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3200:28: ( (otherlv_0= 'play' otherlv_1= 'sound' ( (lv_sound_2_0= RULE_ID ) ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3201:1: (otherlv_0= 'play' otherlv_1= 'sound' ( (lv_sound_2_0= RULE_ID ) ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3201:1: (otherlv_0= 'play' otherlv_1= 'sound' ( (lv_sound_2_0= RULE_ID ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3201:3: otherlv_0= 'play' otherlv_1= 'sound' ( (lv_sound_2_0= RULE_ID ) )
            {
            otherlv_0=(Token)match(input,66,FOLLOW_66_in_rulePlay7540); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_0, grammarAccess.getPlayAccess().getPlayKeyword_0());
                  
            }
            otherlv_1=(Token)match(input,67,FOLLOW_67_in_rulePlay7552); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                  	newLeafNode(otherlv_1, grammarAccess.getPlayAccess().getSoundKeyword_1());
                  
            }
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3209:1: ( (lv_sound_2_0= RULE_ID ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3210:1: (lv_sound_2_0= RULE_ID )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3210:1: (lv_sound_2_0= RULE_ID )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3211:3: lv_sound_2_0= RULE_ID
            {
            lv_sound_2_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePlay7569); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(lv_sound_2_0, grammarAccess.getPlayAccess().getSoundIDTerminalRuleCall_2_0()); 
              		
            }
            if ( state.backtracking==0 ) {

              	        if (current==null) {
              	            current = createModelElement(grammarAccess.getPlayRule());
              	        }
                     		setWithLastConsumed(
                     			current, 
                     			"sound",
                      		lv_sound_2_0, 
                      		"ID");
              	    
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePlay"


    // $ANTLR start "ruleObjectPredefEnum"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3235:1: ruleObjectPredefEnum returns [Enumerator current=null] : (enumLiteral_0= 'mouse-pointer' ) ;
    public final Enumerator ruleObjectPredefEnum() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;

         enterRule(); 
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3237:28: ( (enumLiteral_0= 'mouse-pointer' ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3238:1: (enumLiteral_0= 'mouse-pointer' )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3238:1: (enumLiteral_0= 'mouse-pointer' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3238:3: enumLiteral_0= 'mouse-pointer'
            {
            enumLiteral_0=(Token)match(input,68,FOLLOW_68_in_ruleObjectPredefEnum7623); if (state.failed) return current;
            if ( state.backtracking==0 ) {

                      current = grammarAccess.getObjectPredefEnumAccess().getMousepointerEnumLiteralDeclaration().getEnumLiteral().getInstance();
                      newLeafNode(enumLiteral_0, grammarAccess.getObjectPredefEnumAccess().getMousepointerEnumLiteralDeclaration()); 
                  
            }

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectPredefEnum"


    // $ANTLR start "ruleObjectVariableEnum"
    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3248:1: ruleObjectVariableEnum returns [Enumerator current=null] : ( (enumLiteral_0= 'x position' ) | (enumLiteral_1= 'y position' ) | (enumLiteral_2= 'direction' ) ) ;
    public final Enumerator ruleObjectVariableEnum() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;

         enterRule(); 
        try {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3250:28: ( ( (enumLiteral_0= 'x position' ) | (enumLiteral_1= 'y position' ) | (enumLiteral_2= 'direction' ) ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3251:1: ( (enumLiteral_0= 'x position' ) | (enumLiteral_1= 'y position' ) | (enumLiteral_2= 'direction' ) )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3251:1: ( (enumLiteral_0= 'x position' ) | (enumLiteral_1= 'y position' ) | (enumLiteral_2= 'direction' ) )
            int alt20=3;
            switch ( input.LA(1) ) {
            case 49:
                {
                alt20=1;
                }
                break;
            case 50:
                {
                alt20=2;
                }
                break;
            case 51:
                {
                alt20=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }

            switch (alt20) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3251:2: (enumLiteral_0= 'x position' )
                    {
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3251:2: (enumLiteral_0= 'x position' )
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3251:4: enumLiteral_0= 'x position'
                    {
                    enumLiteral_0=(Token)match(input,49,FOLLOW_49_in_ruleObjectVariableEnum7667); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getObjectVariableEnumAccess().getXpositionEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_0, grammarAccess.getObjectVariableEnumAccess().getXpositionEnumLiteralDeclaration_0()); 
                          
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3257:6: (enumLiteral_1= 'y position' )
                    {
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3257:6: (enumLiteral_1= 'y position' )
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3257:8: enumLiteral_1= 'y position'
                    {
                    enumLiteral_1=(Token)match(input,50,FOLLOW_50_in_ruleObjectVariableEnum7684); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getObjectVariableEnumAccess().getYpositionEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_1, grammarAccess.getObjectVariableEnumAccess().getYpositionEnumLiteralDeclaration_1()); 
                          
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3263:6: (enumLiteral_2= 'direction' )
                    {
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3263:6: (enumLiteral_2= 'direction' )
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3263:8: enumLiteral_2= 'direction'
                    {
                    enumLiteral_2=(Token)match(input,51,FOLLOW_51_in_ruleObjectVariableEnum7701); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                              current = grammarAccess.getObjectVariableEnumAccess().getDirectionEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                              newLeafNode(enumLiteral_2, grammarAccess.getObjectVariableEnumAccess().getDirectionEnumLiteralDeclaration_2()); 
                          
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {
               leaveRule(); 
            }
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleObjectVariableEnum"

    // $ANTLR start synpred31_InternalScratch
    public final void synpred31_InternalScratch_fragment() throws RecognitionException {   
        Token otherlv_2=null;
        EObject lv_cond_3_0 = null;


        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:971:4: (otherlv_2= 'if' ( (lv_cond_3_0= ruleBinaryTerminal ) ) )
        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:971:4: otherlv_2= 'if' ( (lv_cond_3_0= ruleBinaryTerminal ) )
        {
        otherlv_2=(Token)match(input,19,FOLLOW_19_in_synpred31_InternalScratch2211); if (state.failed) return ;
        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:975:1: ( (lv_cond_3_0= ruleBinaryTerminal ) )
        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:976:1: (lv_cond_3_0= ruleBinaryTerminal )
        {
        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:976:1: (lv_cond_3_0= ruleBinaryTerminal )
        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:977:3: lv_cond_3_0= ruleBinaryTerminal
        {
        if ( state.backtracking==0 ) {
           
          	        newCompositeNode(grammarAccess.getForeverAccess().getCondBinaryTerminalParserRuleCall_2_1_0()); 
          	    
        }
        pushFollow(FOLLOW_ruleBinaryTerminal_in_synpred31_InternalScratch2232);
        lv_cond_3_0=ruleBinaryTerminal();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }
    }
    // $ANTLR end synpred31_InternalScratch

    // $ANTLR start synpred43_InternalScratch
    public final void synpred43_InternalScratch_fragment() throws RecognitionException {   
        EObject this_Or_0 = null;


        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2458:2: (this_Or_0= ruleOr )
        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2458:2: this_Or_0= ruleOr
        {
        if ( state.backtracking==0 ) {
           
          	  /* */ 
          	
        }
        pushFollow(FOLLOW_ruleOr_in_synpred43_InternalScratch5823);
        this_Or_0=ruleOr();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred43_InternalScratch

    // $ANTLR start synpred44_InternalScratch
    public final void synpred44_InternalScratch_fragment() throws RecognitionException {   
        EObject this_And_1 = null;


        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2471:2: (this_And_1= ruleAnd )
        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2471:2: this_And_1= ruleAnd
        {
        if ( state.backtracking==0 ) {
           
          	  /* */ 
          	
        }
        pushFollow(FOLLOW_ruleAnd_in_synpred44_InternalScratch5853);
        this_And_1=ruleAnd();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred44_InternalScratch

    // $ANTLR start synpred45_InternalScratch
    public final void synpred45_InternalScratch_fragment() throws RecognitionException {   
        EObject this_Lt_2 = null;


        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2484:2: (this_Lt_2= ruleLt )
        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2484:2: this_Lt_2= ruleLt
        {
        if ( state.backtracking==0 ) {
           
          	  /* */ 
          	
        }
        pushFollow(FOLLOW_ruleLt_in_synpred45_InternalScratch5883);
        this_Lt_2=ruleLt();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred45_InternalScratch

    // $ANTLR start synpred46_InternalScratch
    public final void synpred46_InternalScratch_fragment() throws RecognitionException {   
        EObject this_Gt_3 = null;


        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2497:2: (this_Gt_3= ruleGt )
        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2497:2: this_Gt_3= ruleGt
        {
        if ( state.backtracking==0 ) {
           
          	  /* */ 
          	
        }
        pushFollow(FOLLOW_ruleGt_in_synpred46_InternalScratch5913);
        this_Gt_3=ruleGt();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred46_InternalScratch

    // $ANTLR start synpred47_InternalScratch
    public final void synpred47_InternalScratch_fragment() throws RecognitionException {   
        EObject this_Eq_4 = null;


        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2510:2: (this_Eq_4= ruleEq )
        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:2510:2: this_Eq_4= ruleEq
        {
        if ( state.backtracking==0 ) {
           
          	  /* */ 
          	
        }
        pushFollow(FOLLOW_ruleEq_in_synpred47_InternalScratch5943);
        this_Eq_4=ruleEq();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred47_InternalScratch

    // Delegated rules

    public final boolean synpred47_InternalScratch() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred47_InternalScratch_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred31_InternalScratch() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred31_InternalScratch_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred44_InternalScratch() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred44_InternalScratch_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred45_InternalScratch() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred45_InternalScratch_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred46_InternalScratch() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred46_InternalScratch_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred43_InternalScratch() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred43_InternalScratch_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA7 dfa7 = new DFA7(this);
    protected DFA10 dfa10 = new DFA10(this);
    protected DFA17 dfa17 = new DFA17(this);
    protected DFA18 dfa18 = new DFA18(this);
    static final String DFA7_eotS =
        "\30\uffff";
    static final String DFA7_eofS =
        "\30\uffff";
    static final String DFA7_minS =
        "\1\22\12\uffff\1\4\14\uffff";
    static final String DFA7_maxS =
        "\1\102\12\uffff\1\104\14\uffff";
    static final String DFA7_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12\1\uffff\1\15"+
        "\1\16\1\17\1\20\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14";
    static final String DFA7_specialS =
        "\30\uffff}>";
    static final String[] DFA7_transitionS = {
            "\1\5\1\4\1\5\1\uffff\1\1\1\uffff\1\3\1\uffff\1\2\2\uffff\1\6"+
            "\1\uffff\1\7\1\uffff\1\10\1\11\1\12\1\13\2\uffff\1\14\1\uffff"+
            "\1\15\1\16\1\17\1\20\1\21\20\uffff\1\22\1\23\1\24\1\uffff\1"+
            "\25",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\27\40\uffff\1\26\36\uffff\1\27",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA7_eot = DFA.unpackEncodedString(DFA7_eotS);
    static final short[] DFA7_eof = DFA.unpackEncodedString(DFA7_eofS);
    static final char[] DFA7_min = DFA.unpackEncodedStringToUnsignedChars(DFA7_minS);
    static final char[] DFA7_max = DFA.unpackEncodedStringToUnsignedChars(DFA7_maxS);
    static final short[] DFA7_accept = DFA.unpackEncodedString(DFA7_acceptS);
    static final short[] DFA7_special = DFA.unpackEncodedString(DFA7_specialS);
    static final short[][] DFA7_transition;

    static {
        int numStates = DFA7_transitionS.length;
        DFA7_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA7_transition[i] = DFA.unpackEncodedString(DFA7_transitionS[i]);
        }
    }

    class DFA7 extends DFA {

        public DFA7(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 7;
            this.eot = DFA7_eot;
            this.eof = DFA7_eof;
            this.min = DFA7_min;
            this.max = DFA7_max;
            this.accept = DFA7_accept;
            this.special = DFA7_special;
            this.transition = DFA7_transition;
        }
        public String getDescription() {
            return "571:1: (this_Stop_0= ruleStop | this_Broadcast_1= ruleBroadcast | this_Wait_2= ruleWait | this_If_3= ruleIf | this_Loop_4= ruleLoop | this_Move_5= ruleMove | this_TurnCW_6= ruleTurnCW | this_TurnCCW_7= ruleTurnCCW | this_PointDirection_8= rulePointDirection | this_PointTowards_9= rulePointTowards | this_GoToXY_10= ruleGoToXY | this_GoTo_11= ruleGoTo | this_Glide_12= ruleGlide | this_ChangeX_13= ruleChangeX | this_ChangeY_14= ruleChangeY | this_SetX_15= ruleSetX | this_SetY_16= ruleSetY | this_IfEdge_17= ruleIfEdge | this_Hide_18= ruleHide | this_Show_19= ruleShow | this_Say_20= ruleSay | this_Play_21= rulePlay )";
        }
    }
    static final String DFA10_eotS =
        "\32\uffff";
    static final String DFA10_eofS =
        "\1\2\31\uffff";
    static final String DFA10_minS =
        "\1\16\1\0\30\uffff";
    static final String DFA10_maxS =
        "\1\102\1\0\30\uffff";
    static final String DFA10_acceptS =
        "\2\uffff\1\2\26\uffff\1\1";
    static final String DFA10_specialS =
        "\1\uffff\1\0\30\uffff}>";
    static final String[] DFA10_transitionS = {
            "\1\2\3\uffff\1\2\1\1\1\2\1\uffff\1\2\1\uffff\1\2\1\uffff\1\2"+
            "\2\uffff\1\2\1\uffff\1\2\1\uffff\4\2\2\uffff\1\2\1\uffff\5\2"+
            "\20\uffff\3\2\1\uffff\1\2",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA10_eot = DFA.unpackEncodedString(DFA10_eotS);
    static final short[] DFA10_eof = DFA.unpackEncodedString(DFA10_eofS);
    static final char[] DFA10_min = DFA.unpackEncodedStringToUnsignedChars(DFA10_minS);
    static final char[] DFA10_max = DFA.unpackEncodedStringToUnsignedChars(DFA10_maxS);
    static final short[] DFA10_accept = DFA.unpackEncodedString(DFA10_acceptS);
    static final short[] DFA10_special = DFA.unpackEncodedString(DFA10_specialS);
    static final short[][] DFA10_transition;

    static {
        int numStates = DFA10_transitionS.length;
        DFA10_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA10_transition[i] = DFA.unpackEncodedString(DFA10_transitionS[i]);
        }
    }

    class DFA10 extends DFA {

        public DFA10(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 10;
            this.eot = DFA10_eot;
            this.eof = DFA10_eof;
            this.min = DFA10_min;
            this.max = DFA10_max;
            this.accept = DFA10_accept;
            this.special = DFA10_special;
            this.transition = DFA10_transition;
        }
        public String getDescription() {
            return "971:2: (otherlv_2= 'if' ( (lv_cond_3_0= ruleBinaryTerminal ) ) )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA10_1 = input.LA(1);

                         
                        int index10_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred31_InternalScratch()) ) {s = 25;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index10_1);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 10, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA17_eotS =
        "\12\uffff";
    static final String DFA17_eofS =
        "\2\uffff\1\7\1\10\1\11\5\uffff";
    static final String DFA17_minS =
        "\1\5\1\uffff\3\57\5\uffff";
    static final String DFA17_maxS =
        "\1\63\1\uffff\3\64\5\uffff";
    static final String DFA17_acceptS =
        "\1\uffff\1\1\3\uffff\1\3\1\2\1\4\1\5\1\6";
    static final String DFA17_specialS =
        "\12\uffff}>";
    static final String[] DFA17_transitionS = {
            "\1\1\52\uffff\1\5\1\2\1\3\1\4",
            "",
            "\1\7\4\uffff\1\6",
            "\1\10\4\uffff\1\6",
            "\1\11\4\uffff\1\6",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA17_eot = DFA.unpackEncodedString(DFA17_eotS);
    static final short[] DFA17_eof = DFA.unpackEncodedString(DFA17_eofS);
    static final char[] DFA17_min = DFA.unpackEncodedStringToUnsignedChars(DFA17_minS);
    static final char[] DFA17_max = DFA.unpackEncodedStringToUnsignedChars(DFA17_maxS);
    static final short[] DFA17_accept = DFA.unpackEncodedString(DFA17_acceptS);
    static final short[] DFA17_special = DFA.unpackEncodedString(DFA17_specialS);
    static final short[][] DFA17_transition;

    static {
        int numStates = DFA17_transitionS.length;
        DFA17_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA17_transition[i] = DFA.unpackEncodedString(DFA17_transitionS[i]);
        }
    }

    class DFA17 extends DFA {

        public DFA17(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 17;
            this.eot = DFA17_eot;
            this.eof = DFA17_eof;
            this.min = DFA17_min;
            this.max = DFA17_max;
            this.accept = DFA17_accept;
            this.special = DFA17_special;
            this.transition = DFA17_transition;
        }
        public String getDescription() {
            return "2073:1: (this_NumberValue_0= ruleNumberValue | this_NumberObjVar_1= ruleNumberObjVar | this_NumberVar_2= ruleNumberVar | this_NumberVarX_3= ruleNumberVarX | this_NumberVarY_4= ruleNumberVarY | this_NumberVarD_5= ruleNumberVarD )";
        }
    }
    static final String DFA18_eotS =
        "\12\uffff";
    static final String DFA18_eofS =
        "\12\uffff";
    static final String DFA18_minS =
        "\1\56\1\0\10\uffff";
    static final String DFA18_maxS =
        "\1\75\1\0\10\uffff";
    static final String DFA18_acceptS =
        "\2\uffff\1\6\1\7\1\10\1\1\1\2\1\3\1\4\1\5";
    static final String DFA18_specialS =
        "\1\uffff\1\0\10\uffff}>";
    static final String[] DFA18_transitionS = {
            "\1\1\1\uffff\1\4\4\uffff\1\3\7\uffff\1\2",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA18_eot = DFA.unpackEncodedString(DFA18_eotS);
    static final short[] DFA18_eof = DFA.unpackEncodedString(DFA18_eofS);
    static final char[] DFA18_min = DFA.unpackEncodedStringToUnsignedChars(DFA18_minS);
    static final char[] DFA18_max = DFA.unpackEncodedStringToUnsignedChars(DFA18_maxS);
    static final short[] DFA18_accept = DFA.unpackEncodedString(DFA18_acceptS);
    static final short[] DFA18_special = DFA.unpackEncodedString(DFA18_specialS);
    static final short[][] DFA18_transition;

    static {
        int numStates = DFA18_transitionS.length;
        DFA18_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA18_transition[i] = DFA.unpackEncodedString(DFA18_transitionS[i]);
        }
    }

    class DFA18 extends DFA {

        public DFA18(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 18;
            this.eot = DFA18_eot;
            this.eof = DFA18_eof;
            this.min = DFA18_min;
            this.max = DFA18_max;
            this.accept = DFA18_accept;
            this.special = DFA18_special;
            this.transition = DFA18_transition;
        }
        public String getDescription() {
            return "2457:1: (this_Or_0= ruleOr | this_And_1= ruleAnd | this_Lt_2= ruleLt | this_Gt_3= ruleGt | this_Eq_4= ruleEq | this_Not_5= ruleNot | this_TouchsColor_6= ruleTouchsColor | this_BinaryVar_7= ruleBinaryVar )";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA18_1 = input.LA(1);

                         
                        int index18_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred43_InternalScratch()) ) {s = 5;}

                        else if ( (synpred44_InternalScratch()) ) {s = 6;}

                        else if ( (synpred45_InternalScratch()) ) {s = 7;}

                        else if ( (synpred46_InternalScratch()) ) {s = 8;}

                        else if ( (synpred47_InternalScratch()) ) {s = 9;}

                         
                        input.seek(index18_1);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 18, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    public static final BitSet FOLLOW_ruleProject_in_entryRuleProject81 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProject91 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleProject128 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleProject145 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_ruleObject_in_ruleProject171 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_ruleObject_in_entryRuleObject208 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleObject218 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_ruleObject255 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleObject272 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_ruleScript_in_ruleObject298 = new BitSet(new long[]{0x0000000000002002L});
    public static final BitSet FOLLOW_ruleIObject_in_entryRuleIObject335 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIObject345 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRef_in_ruleIObject395 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePredef_in_ruleIObject425 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRef_in_entryRuleRef460 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRef470 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRef518 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePredef_in_entryRulePredef553 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePredef563 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleObjectPredefEnum_in_rulePredef608 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleScript_in_entryRuleScript643 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleScript653 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_ruleScript690 = new BitSet(new long[]{0xC0003E9EA55CC000L,0x0000000000000005L});
    public static final BitSet FOLLOW_ruleWhen_in_ruleScript723 = new BitSet(new long[]{0xC0003E9EA55C4000L,0x0000000000000005L});
    public static final BitSet FOLLOW_ruleBlock_in_ruleScript745 = new BitSet(new long[]{0xC0003E9EA55C4000L,0x0000000000000005L});
    public static final BitSet FOLLOW_14_in_ruleScript758 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhen_in_entryRuleWhen794 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWhen804 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleWhen841 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_ruleWhenFlag_in_ruleWhen867 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhenMsg_in_ruleWhen897 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhenFlag_in_entryRuleWhenFlag933 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWhenFlag943 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_ruleWhenFlag980 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhenMsg_in_entryRuleWhenMsg1028 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWhenMsg1038 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_ruleWhenMsg1075 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleMessage_in_ruleWhenMsg1096 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessage_in_entryRuleMessage1132 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMessage1142 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleMessage1183 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBlock_in_entryRuleBlock1223 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBlock1233 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStop_in_ruleBlock1283 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBroadcast_in_ruleBlock1313 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWait_in_ruleBlock1343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIf_in_ruleBlock1373 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLoop_in_ruleBlock1403 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMove_in_ruleBlock1433 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurnCW_in_ruleBlock1463 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurnCCW_in_ruleBlock1493 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePointDirection_in_ruleBlock1523 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePointTowards_in_ruleBlock1553 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGoToXY_in_ruleBlock1583 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGoTo_in_ruleBlock1613 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGlide_in_ruleBlock1643 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleChangeX_in_ruleBlock1673 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleChangeY_in_ruleBlock1703 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSetX_in_ruleBlock1733 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSetY_in_ruleBlock1763 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIfEdge_in_ruleBlock1793 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleHide_in_ruleBlock1823 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleShow_in_ruleBlock1853 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSay_in_ruleBlock1883 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePlay_in_ruleBlock1913 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLoop_in_entryRuleLoop1948 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLoop1958 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForever_in_ruleLoop2009 = new BitSet(new long[]{0xC0003E9EA55C4000L,0x0000000000000005L});
    public static final BitSet FOLLOW_ruleRepeatN_in_ruleLoop2039 = new BitSet(new long[]{0xC0003E9EA55C4000L,0x0000000000000005L});
    public static final BitSet FOLLOW_ruleRepeatUntil_in_ruleLoop2069 = new BitSet(new long[]{0xC0003E9EA55C4000L,0x0000000000000005L});
    public static final BitSet FOLLOW_ruleBlock_in_ruleLoop2090 = new BitSet(new long[]{0xC0003E9EA55C4000L,0x0000000000000005L});
    public static final BitSet FOLLOW_14_in_ruleLoop2103 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForever_in_entryRuleForever2139 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleForever2149 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleForever2186 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_19_in_ruleForever2211 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_ruleForever2232 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepeatN_in_entryRuleRepeatN2270 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRepeatN2280 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleRepeatN2317 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleRepeatN2338 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepeatUntil_in_entryRuleRepeatUntil2374 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRepeatUntil2384 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleRepeatUntil2421 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleRepeatUntil2433 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_ruleRepeatUntil2454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStop_in_entryRuleStop2490 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStop2500 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleStop2537 = new BitSet(new long[]{0x0000000000802000L});
    public static final BitSet FOLLOW_23_in_ruleStop2556 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_ruleStop2593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWait_in_entryRuleWait2643 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWait2653 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleWait2690 = new BitSet(new long[]{0x0000400000200000L});
    public static final BitSet FOLLOW_ruleWaitDelay_in_ruleWait2716 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWaitUntil_in_ruleWait2746 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWaitDelay_in_entryRuleWaitDelay2782 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWaitDelay2792 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleWaitDelay2838 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleWaitDelay2850 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWaitUntil_in_entryRuleWaitUntil2886 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWaitUntil2896 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleWaitUntil2933 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_ruleWaitUntil2954 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBroadcast_in_entryRuleBroadcast2990 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBroadcast3000 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_ruleBroadcast3037 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_ruleMessage_in_ruleBroadcast3058 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_27_in_ruleBroadcast3076 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIf_in_entryRuleIf3126 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIf3136 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleIf3174 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_ruleIf3195 = new BitSet(new long[]{0xC0003E9EB55C4000L,0x0000000000000005L});
    public static final BitSet FOLLOW_ruleBlock_in_ruleIf3216 = new BitSet(new long[]{0xC0003E9EB55C4000L,0x0000000000000005L});
    public static final BitSet FOLLOW_28_in_ruleIf3231 = new BitSet(new long[]{0xC0003E9EA55C4000L,0x0000000000000005L});
    public static final BitSet FOLLOW_ruleBlock_in_ruleIf3252 = new BitSet(new long[]{0xC0003E9EA55C4000L,0x0000000000000005L});
    public static final BitSet FOLLOW_14_in_ruleIf3267 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMove_in_entryRuleMove3303 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMove3313 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_ruleMove3350 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleMove3371 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_30_in_ruleMove3383 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurnCW_in_entryRuleTurnCW3419 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTurnCW3429 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_ruleTurnCW3466 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleTurnCW3487 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_ruleTurnCW3499 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurnCCW_in_entryRuleTurnCCW3535 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTurnCCW3545 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_ruleTurnCCW3582 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleTurnCCW3603 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_32_in_ruleTurnCCW3615 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePointDirection_in_entryRulePointDirection3651 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePointDirection3661 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_rulePointDirection3698 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rulePointDirection3719 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePointTowards_in_entryRulePointTowards3755 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePointTowards3765 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_rulePointTowards3802 = new BitSet(new long[]{0x0000000000000010L,0x0000000000000010L});
    public static final BitSet FOLLOW_ruleIObject_in_rulePointTowards3823 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGoToXY_in_entryRuleGoToXY3859 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGoToXY3869 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_ruleGoToXY3906 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_37_in_ruleGoToXY3918 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleGoToXY3939 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38_in_ruleGoToXY3951 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleGoToXY3972 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGoTo_in_entryRuleGoTo4008 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGoTo4018 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_ruleGoTo4055 = new BitSet(new long[]{0x0000000000000010L,0x0000000000000010L});
    public static final BitSet FOLLOW_ruleIObject_in_ruleGoTo4076 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGlide_in_entryRuleGlide4112 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGlide4122 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_ruleGlide4159 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleGlide4180 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_40_in_ruleGlide4192 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleGlide4213 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_38_in_ruleGlide4225 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleGlide4246 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleChangeX_in_entryRuleChangeX4282 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleChangeX4292 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_ruleChangeX4329 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleChangeX4350 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleChangeY_in_entryRuleChangeY4386 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleChangeY4396 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_ruleChangeY4433 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleChangeY4454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSetX_in_entryRuleSetX4490 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSetX4500 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_ruleSetX4537 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleSetX4558 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSetY_in_entryRuleSetY4594 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSetY4604 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_ruleSetY4641 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleSetY4662 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIfEdge_in_entryRuleIfEdge4698 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIfEdge4708 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_ruleIfEdge4745 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumber_in_entryRuleNumber4793 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumber4803 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberValue_in_ruleNumber4853 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberObjVar_in_ruleNumber4883 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVar_in_ruleNumber4913 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVarX_in_ruleNumber4943 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVarY_in_ruleNumber4973 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVarD_in_ruleNumber5003 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_entryRuleNumberTerminal5038 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumberTerminal5048 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_ruleNumberTerminal5085 = new BitSet(new long[]{0x000F000000000020L});
    public static final BitSet FOLLOW_ruleNumber_in_ruleNumberTerminal5110 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_ruleNumberTerminal5121 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberValue_in_entryRuleNumberValue5157 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumberValue5167 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_NUM_in_ruleNumberValue5208 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVar_in_entryRuleNumberVar5248 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumberVar5258 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_ruleNumberVar5295 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleNumberVar5312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVarX_in_entryRuleNumberVarX5353 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumberVarX5363 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_ruleNumberVarX5400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVarY_in_entryRuleNumberVarY5448 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumberVarY5458 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_ruleNumberVarY5495 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVarD_in_entryRuleNumberVarD5543 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumberVarD5553 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_51_in_ruleNumberVarD5590 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberObjVar_in_entryRuleNumberObjVar5638 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumberObjVar5648 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleObjectVariableEnum_in_ruleNumberObjVar5694 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_52_in_ruleNumberObjVar5706 = new BitSet(new long[]{0x0000000000000010L,0x0000000000000010L});
    public static final BitSet FOLLOW_ruleIObject_in_ruleNumberObjVar5727 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinary_in_entryRuleBinary5763 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBinary5773 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOr_in_ruleBinary5823 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_in_ruleBinary5853 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLt_in_ruleBinary5883 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGt_in_ruleBinary5913 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEq_in_ruleBinary5943 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNot_in_ruleBinary5973 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTouchsColor_in_ruleBinary6003 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryVar_in_ruleBinary6033 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_entryRuleBinaryTerminal6068 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBinaryTerminal6078 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_ruleBinaryTerminal6115 = new BitSet(new long[]{0x2021400000000000L});
    public static final BitSet FOLLOW_ruleBinary_in_ruleBinaryTerminal6140 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_ruleBinaryTerminal6151 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryVar_in_entryRuleBinaryVar6187 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBinaryVar6197 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_ruleBinaryVar6234 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleBinaryVar6251 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTouchsColor_in_entryRuleTouchsColor6292 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTouchsColor6302 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_53_in_ruleTouchsColor6339 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_54_in_ruleTouchsColor6351 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleTouchsColor6368 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_55_in_ruleTouchsColor6385 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLt_in_entryRuleLt6421 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLt6431 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleLt6477 = new BitSet(new long[]{0x0100000000000000L});
    public static final BitSet FOLLOW_56_in_ruleLt6489 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleLt6510 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGt_in_entryRuleGt6546 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGt6556 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleGt6602 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_57_in_ruleGt6614 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleGt6635 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEq_in_entryRuleEq6671 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEq6681 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleEq6727 = new BitSet(new long[]{0x0400000000000000L});
    public static final BitSet FOLLOW_58_in_ruleEq6739 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleEq6760 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_in_entryRuleAnd6796 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAnd6806 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_ruleAnd6852 = new BitSet(new long[]{0x0800000000000000L});
    public static final BitSet FOLLOW_59_in_ruleAnd6864 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_ruleAnd6885 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOr_in_entryRuleOr6921 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOr6931 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_ruleOr6977 = new BitSet(new long[]{0x1000000000000000L});
    public static final BitSet FOLLOW_60_in_ruleOr6989 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_ruleOr7010 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNot_in_entryRuleNot7046 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNot7056 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_61_in_ruleNot7093 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_ruleNot7114 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleHide_in_entryRuleHide7150 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleHide7160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_62_in_ruleHide7197 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleShow_in_entryRuleShow7245 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleShow7255 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_63_in_ruleShow7292 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSay_in_entryRuleSay7340 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSay7350 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_64_in_ruleSay7387 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleSay7404 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000002L});
    public static final BitSet FOLLOW_65_in_ruleSay7422 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_ruleSay7443 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25_in_ruleSay7455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePlay_in_entryRulePlay7493 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePlay7503 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_66_in_rulePlay7540 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_67_in_rulePlay7552 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePlay7569 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_68_in_ruleObjectPredefEnum7623 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_ruleObjectVariableEnum7667 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_ruleObjectVariableEnum7684 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_51_in_ruleObjectVariableEnum7701 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_synpred31_InternalScratch2211 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_synpred31_InternalScratch2232 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOr_in_synpred43_InternalScratch5823 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_in_synpred44_InternalScratch5853 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLt_in_synpred45_InternalScratch5883 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGt_in_synpred46_InternalScratch5913 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEq_in_synpred47_InternalScratch5943 = new BitSet(new long[]{0x0000000000000002L});

}