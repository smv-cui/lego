package demo.scratch.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalScratchLexer extends Lexer {
    public static final int T__68=68;
    public static final int RULE_ID=4;
    public static final int T__66=66;
    public static final int T__67=67;
    public static final int T__29=29;
    public static final int T__64=64;
    public static final int T__28=28;
    public static final int T__65=65;
    public static final int T__27=27;
    public static final int T__62=62;
    public static final int T__26=26;
    public static final int T__63=63;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=8;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int T__61=61;
    public static final int EOF=-1;
    public static final int T__60=60;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__19=19;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__16=16;
    public static final int T__52=52;
    public static final int T__15=15;
    public static final int T__53=53;
    public static final int T__18=18;
    public static final int T__54=54;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int T__59=59;
    public static final int T__50=50;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_SL_COMMENT=10;
    public static final int RULE_NUM=5;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_STRING=6;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_WS=7;

    // delegates
    // delegators

    public InternalScratchLexer() {;} 
    public InternalScratchLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalScratchLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g"; }

    // $ANTLR start "T__11"
    public final void mT__11() throws RecognitionException {
        try {
            int _type = T__11;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:11:7: ( 'project' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:11:9: 'project'
            {
            match("project"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__11"

    // $ANTLR start "T__12"
    public final void mT__12() throws RecognitionException {
        try {
            int _type = T__12;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:12:7: ( 'object' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:12:9: 'object'
            {
            match("object"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:13:7: ( 'script' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:13:9: 'script'
            {
            match("script"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:14:7: ( 'end' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:14:9: 'end'
            {
            match("end"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:15:7: ( 'when' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:15:9: 'when'
            {
            match("when"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:16:7: ( 'green flag clicked' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:16:9: 'green flag clicked'
            {
            match("green flag clicked"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:17:7: ( 'I receive' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:17:9: 'I receive'
            {
            match("I receive"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:18:7: ( 'forever' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:18:9: 'forever'
            {
            match("forever"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:19:7: ( 'if' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:19:9: 'if'
            {
            match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:20:7: ( 'repeat' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:20:9: 'repeat'
            {
            match("repeat"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:21:7: ( 'until' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:21:9: 'until'
            {
            match("until"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:22:7: ( 'stop' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:22:9: 'stop'
            {
            match("stop"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:23:7: ( 'all' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:23:9: 'all'
            {
            match("all"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:24:7: ( 'wait' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:24:9: 'wait'
            {
            match("wait"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:25:7: ( 'secs' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:25:9: 'secs'
            {
            match("secs"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:26:7: ( 'broadcast' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:26:9: 'broadcast'
            {
            match("broadcast"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:27:7: ( 'and wait' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:27:9: 'and wait'
            {
            match("and wait"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:28:7: ( 'else' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:28:9: 'else'
            {
            match("else"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:29:7: ( 'move' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:29:9: 'move'
            {
            match("move"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:30:7: ( 'steps' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:30:9: 'steps'
            {
            match("steps"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:31:7: ( 'turn cw' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:31:9: 'turn cw'
            {
            match("turn cw"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:32:7: ( 'degrees' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:32:9: 'degrees'
            {
            match("degrees"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:33:7: ( 'turn ccw' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:33:9: 'turn ccw'
            {
            match("turn ccw"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:34:7: ( 'point in direction' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:34:9: 'point in direction'
            {
            match("point in direction"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:35:7: ( 'point towards' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:35:9: 'point towards'
            {
            match("point towards"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:36:7: ( 'go to' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:36:9: 'go to'
            {
            match("go to"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:37:7: ( 'x:' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:37:9: 'x:'
            {
            match("x:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:38:7: ( 'y:' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:38:9: 'y:'
            {
            match("y:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:39:7: ( 'glide' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:39:9: 'glide'
            {
            match("glide"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:40:7: ( 'secs to x:' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:40:9: 'secs to x:'
            {
            match("secs to x:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:41:7: ( 'change x by' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:41:9: 'change x by'
            {
            match("change x by"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:42:7: ( 'change y by' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:42:9: 'change y by'
            {
            match("change y by"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "T__43"
    public final void mT__43() throws RecognitionException {
        try {
            int _type = T__43;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:43:7: ( 'set x to' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:43:9: 'set x to'
            {
            match("set x to"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__43"

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:44:7: ( 'set y to' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:44:9: 'set y to'
            {
            match("set y to"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:45:7: ( 'if on edge, bounce' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:45:9: 'if on edge, bounce'
            {
            match("if on edge, bounce"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "T__46"
    public final void mT__46() throws RecognitionException {
        try {
            int _type = T__46;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:46:7: ( '(' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:46:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__46"

    // $ANTLR start "T__47"
    public final void mT__47() throws RecognitionException {
        try {
            int _type = T__47;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:47:7: ( ')' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:47:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__47"

    // $ANTLR start "T__48"
    public final void mT__48() throws RecognitionException {
        try {
            int _type = T__48;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:48:7: ( '$' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:48:9: '$'
            {
            match('$'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__48"

    // $ANTLR start "T__49"
    public final void mT__49() throws RecognitionException {
        try {
            int _type = T__49;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:49:7: ( 'x position' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:49:9: 'x position'
            {
            match("x position"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__49"

    // $ANTLR start "T__50"
    public final void mT__50() throws RecognitionException {
        try {
            int _type = T__50;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:50:7: ( 'y position' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:50:9: 'y position'
            {
            match("y position"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__50"

    // $ANTLR start "T__51"
    public final void mT__51() throws RecognitionException {
        try {
            int _type = T__51;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:51:7: ( 'direction' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:51:9: 'direction'
            {
            match("direction"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__51"

    // $ANTLR start "T__52"
    public final void mT__52() throws RecognitionException {
        try {
            int _type = T__52;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:52:7: ( 'of' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:52:9: 'of'
            {
            match("of"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__52"

    // $ANTLR start "T__53"
    public final void mT__53() throws RecognitionException {
        try {
            int _type = T__53;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:53:7: ( 'touching' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:53:9: 'touching'
            {
            match("touching"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__53"

    // $ANTLR start "T__54"
    public final void mT__54() throws RecognitionException {
        try {
            int _type = T__54;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:54:7: ( 'color' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:54:9: 'color'
            {
            match("color"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__54"

    // $ANTLR start "T__55"
    public final void mT__55() throws RecognitionException {
        try {
            int _type = T__55;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:55:7: ( '?' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:55:9: '?'
            {
            match('?'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__55"

    // $ANTLR start "T__56"
    public final void mT__56() throws RecognitionException {
        try {
            int _type = T__56;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:56:7: ( '<' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:56:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__56"

    // $ANTLR start "T__57"
    public final void mT__57() throws RecognitionException {
        try {
            int _type = T__57;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:57:7: ( '>' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:57:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__57"

    // $ANTLR start "T__58"
    public final void mT__58() throws RecognitionException {
        try {
            int _type = T__58;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:58:7: ( '=' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:58:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__58"

    // $ANTLR start "T__59"
    public final void mT__59() throws RecognitionException {
        try {
            int _type = T__59;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:59:7: ( 'and' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:59:9: 'and'
            {
            match("and"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__59"

    // $ANTLR start "T__60"
    public final void mT__60() throws RecognitionException {
        try {
            int _type = T__60;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:60:7: ( 'or' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:60:9: 'or'
            {
            match("or"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__60"

    // $ANTLR start "T__61"
    public final void mT__61() throws RecognitionException {
        try {
            int _type = T__61;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:61:7: ( 'not' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:61:9: 'not'
            {
            match("not"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__61"

    // $ANTLR start "T__62"
    public final void mT__62() throws RecognitionException {
        try {
            int _type = T__62;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:62:7: ( 'hide' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:62:9: 'hide'
            {
            match("hide"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__62"

    // $ANTLR start "T__63"
    public final void mT__63() throws RecognitionException {
        try {
            int _type = T__63;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:63:7: ( 'show' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:63:9: 'show'
            {
            match("show"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__63"

    // $ANTLR start "T__64"
    public final void mT__64() throws RecognitionException {
        try {
            int _type = T__64;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:64:7: ( 'say' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:64:9: 'say'
            {
            match("say"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__64"

    // $ANTLR start "T__65"
    public final void mT__65() throws RecognitionException {
        try {
            int _type = T__65;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:65:7: ( 'for' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:65:9: 'for'
            {
            match("for"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__65"

    // $ANTLR start "T__66"
    public final void mT__66() throws RecognitionException {
        try {
            int _type = T__66;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:66:7: ( 'play' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:66:9: 'play'
            {
            match("play"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__66"

    // $ANTLR start "T__67"
    public final void mT__67() throws RecognitionException {
        try {
            int _type = T__67;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:67:7: ( 'sound' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:67:9: 'sound'
            {
            match("sound"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__67"

    // $ANTLR start "T__68"
    public final void mT__68() throws RecognitionException {
        try {
            int _type = T__68;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:68:7: ( 'mouse-pointer' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:68:9: 'mouse-pointer'
            {
            match("mouse-pointer"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__68"

    // $ANTLR start "RULE_NUM"
    public final void mRULE_NUM() throws RecognitionException {
        try {
            int _type = RULE_NUM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3272:10: ( ( '-' )? ( '0' .. '9' )+ ( '.' ( '0' .. '9' )+ )? )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3272:12: ( '-' )? ( '0' .. '9' )+ ( '.' ( '0' .. '9' )+ )?
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3272:12: ( '-' )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='-') ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3272:12: '-'
                    {
                    match('-'); 

                    }
                    break;

            }

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3272:17: ( '0' .. '9' )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>='0' && LA2_0<='9')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3272:18: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3272:29: ( '.' ( '0' .. '9' )+ )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='.') ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3272:30: '.' ( '0' .. '9' )+
                    {
                    match('.'); 
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3272:34: ( '0' .. '9' )+
                    int cnt3=0;
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( ((LA3_0>='0' && LA3_0<='9')) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3272:35: '0' .. '9'
                    	    {
                    	    matchRange('0','9'); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt3 >= 1 ) break loop3;
                                EarlyExitException eee =
                                    new EarlyExitException(3, input);
                                throw eee;
                        }
                        cnt3++;
                    } while (true);


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NUM"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3274:9: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3274:11: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3274:35: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='0' && LA5_0<='9')||(LA5_0>='A' && LA5_0<='Z')||LA5_0=='_'||(LA5_0>='a' && LA5_0<='z')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3276:13: ( ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3276:15: ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3276:15: ( '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0=='\"') ) {
                alt8=1;
            }
            else if ( (LA8_0=='\'') ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3276:16: '\"' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3276:20: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop6:
                    do {
                        int alt6=3;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0=='\\') ) {
                            alt6=1;
                        }
                        else if ( ((LA6_0>='\u0000' && LA6_0<='!')||(LA6_0>='#' && LA6_0<='[')||(LA6_0>=']' && LA6_0<='\uFFFF')) ) {
                            alt6=2;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3276:21: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' )
                    	    {
                    	    match('\\'); 
                    	    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||(input.LA(1)>='t' && input.LA(1)<='u') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;
                    	case 2 :
                    	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3276:66: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3276:86: '\\'' ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3276:91: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' ) | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop7:
                    do {
                        int alt7=3;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0=='\\') ) {
                            alt7=1;
                        }
                        else if ( ((LA7_0>='\u0000' && LA7_0<='&')||(LA7_0>='(' && LA7_0<='[')||(LA7_0>=']' && LA7_0<='\uFFFF')) ) {
                            alt7=2;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3276:92: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | 'u' | '\"' | '\\'' | '\\\\' )
                    	    {
                    	    match('\\'); 
                    	    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||(input.LA(1)>='t' && input.LA(1)<='u') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;
                    	case 2 :
                    	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3276:137: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3278:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3278:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3278:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>='\t' && LA9_0<='\n')||LA9_0=='\r'||LA9_0==' ') ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3280:16: ( . )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3280:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3282:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3282:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3282:24: ( options {greedy=false; } : . )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0=='*') ) {
                    int LA10_1 = input.LA(2);

                    if ( (LA10_1=='/') ) {
                        alt10=2;
                    }
                    else if ( ((LA10_1>='\u0000' && LA10_1<='.')||(LA10_1>='0' && LA10_1<='\uFFFF')) ) {
                        alt10=1;
                    }


                }
                else if ( ((LA10_0>='\u0000' && LA10_0<=')')||(LA10_0>='+' && LA10_0<='\uFFFF')) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3282:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3284:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3284:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3284:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>='\u0000' && LA11_0<='\t')||(LA11_0>='\u000B' && LA11_0<='\f')||(LA11_0>='\u000E' && LA11_0<='\uFFFF')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3284:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3284:40: ( ( '\\r' )? '\\n' )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0=='\n'||LA13_0=='\r') ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3284:41: ( '\\r' )? '\\n'
                    {
                    // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3284:41: ( '\\r' )?
                    int alt12=2;
                    int LA12_0 = input.LA(1);

                    if ( (LA12_0=='\r') ) {
                        alt12=1;
                    }
                    switch (alt12) {
                        case 1 :
                            // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:3284:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    public void mTokens() throws RecognitionException {
        // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:8: ( T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | RULE_NUM | RULE_ID | RULE_STRING | RULE_WS | RULE_ANY_OTHER | RULE_ML_COMMENT | RULE_SL_COMMENT )
        int alt14=65;
        alt14 = dfa14.predict(input);
        switch (alt14) {
            case 1 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:10: T__11
                {
                mT__11(); 

                }
                break;
            case 2 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:16: T__12
                {
                mT__12(); 

                }
                break;
            case 3 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:22: T__13
                {
                mT__13(); 

                }
                break;
            case 4 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:28: T__14
                {
                mT__14(); 

                }
                break;
            case 5 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:34: T__15
                {
                mT__15(); 

                }
                break;
            case 6 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:40: T__16
                {
                mT__16(); 

                }
                break;
            case 7 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:46: T__17
                {
                mT__17(); 

                }
                break;
            case 8 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:52: T__18
                {
                mT__18(); 

                }
                break;
            case 9 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:58: T__19
                {
                mT__19(); 

                }
                break;
            case 10 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:64: T__20
                {
                mT__20(); 

                }
                break;
            case 11 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:70: T__21
                {
                mT__21(); 

                }
                break;
            case 12 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:76: T__22
                {
                mT__22(); 

                }
                break;
            case 13 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:82: T__23
                {
                mT__23(); 

                }
                break;
            case 14 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:88: T__24
                {
                mT__24(); 

                }
                break;
            case 15 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:94: T__25
                {
                mT__25(); 

                }
                break;
            case 16 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:100: T__26
                {
                mT__26(); 

                }
                break;
            case 17 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:106: T__27
                {
                mT__27(); 

                }
                break;
            case 18 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:112: T__28
                {
                mT__28(); 

                }
                break;
            case 19 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:118: T__29
                {
                mT__29(); 

                }
                break;
            case 20 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:124: T__30
                {
                mT__30(); 

                }
                break;
            case 21 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:130: T__31
                {
                mT__31(); 

                }
                break;
            case 22 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:136: T__32
                {
                mT__32(); 

                }
                break;
            case 23 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:142: T__33
                {
                mT__33(); 

                }
                break;
            case 24 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:148: T__34
                {
                mT__34(); 

                }
                break;
            case 25 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:154: T__35
                {
                mT__35(); 

                }
                break;
            case 26 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:160: T__36
                {
                mT__36(); 

                }
                break;
            case 27 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:166: T__37
                {
                mT__37(); 

                }
                break;
            case 28 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:172: T__38
                {
                mT__38(); 

                }
                break;
            case 29 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:178: T__39
                {
                mT__39(); 

                }
                break;
            case 30 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:184: T__40
                {
                mT__40(); 

                }
                break;
            case 31 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:190: T__41
                {
                mT__41(); 

                }
                break;
            case 32 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:196: T__42
                {
                mT__42(); 

                }
                break;
            case 33 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:202: T__43
                {
                mT__43(); 

                }
                break;
            case 34 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:208: T__44
                {
                mT__44(); 

                }
                break;
            case 35 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:214: T__45
                {
                mT__45(); 

                }
                break;
            case 36 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:220: T__46
                {
                mT__46(); 

                }
                break;
            case 37 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:226: T__47
                {
                mT__47(); 

                }
                break;
            case 38 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:232: T__48
                {
                mT__48(); 

                }
                break;
            case 39 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:238: T__49
                {
                mT__49(); 

                }
                break;
            case 40 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:244: T__50
                {
                mT__50(); 

                }
                break;
            case 41 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:250: T__51
                {
                mT__51(); 

                }
                break;
            case 42 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:256: T__52
                {
                mT__52(); 

                }
                break;
            case 43 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:262: T__53
                {
                mT__53(); 

                }
                break;
            case 44 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:268: T__54
                {
                mT__54(); 

                }
                break;
            case 45 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:274: T__55
                {
                mT__55(); 

                }
                break;
            case 46 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:280: T__56
                {
                mT__56(); 

                }
                break;
            case 47 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:286: T__57
                {
                mT__57(); 

                }
                break;
            case 48 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:292: T__58
                {
                mT__58(); 

                }
                break;
            case 49 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:298: T__59
                {
                mT__59(); 

                }
                break;
            case 50 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:304: T__60
                {
                mT__60(); 

                }
                break;
            case 51 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:310: T__61
                {
                mT__61(); 

                }
                break;
            case 52 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:316: T__62
                {
                mT__62(); 

                }
                break;
            case 53 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:322: T__63
                {
                mT__63(); 

                }
                break;
            case 54 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:328: T__64
                {
                mT__64(); 

                }
                break;
            case 55 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:334: T__65
                {
                mT__65(); 

                }
                break;
            case 56 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:340: T__66
                {
                mT__66(); 

                }
                break;
            case 57 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:346: T__67
                {
                mT__67(); 

                }
                break;
            case 58 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:352: T__68
                {
                mT__68(); 

                }
                break;
            case 59 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:358: RULE_NUM
                {
                mRULE_NUM(); 

                }
                break;
            case 60 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:367: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 61 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:375: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 62 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:387: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 63 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:395: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;
            case 64 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:410: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 65 :
                // ../demo.scratch/src-gen/demo/scratch/parser/antlr/internal/InternalScratch.g:1:426: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;

        }

    }


    protected DFA14 dfa14 = new DFA14(this);
    static final String DFA14_eotS =
        "\1\uffff\23\50\7\uffff\2\50\1\44\2\uffff\2\44\1\uffff\1\44\1\uffff"+
        "\3\50\1\uffff\1\50\1\136\1\137\15\50\1\uffff\1\50\1\161\12\50\4"+
        "\uffff\2\50\7\uffff\2\50\5\uffff\4\50\2\uffff\6\50\1\u008b\1\50"+
        "\1\u008d\4\50\1\uffff\1\50\1\u0094\2\uffff\2\50\1\u0097\1\u0099"+
        "\11\50\1\u00a3\3\50\1\u00a7\2\50\1\u00aa\1\50\1\u00ad\1\uffff\1"+
        "\u00b0\1\uffff\1\50\1\uffff\1\u00b2\1\u00b3\1\u00b4\3\50\1\uffff"+
        "\2\50\3\uffff\1\50\1\u00bb\7\50\1\uffff\1\u00c3\2\50\1\uffff\2\50"+
        "\1\uffff\1\u00c8\5\uffff\1\u00c9\3\uffff\1\50\1\u00cb\2\50\1\u00ce"+
        "\1\50\1\uffff\1\50\1\uffff\4\50\1\u00d6\1\uffff\1\50\1\uffff\1\u00da"+
        "\1\u00db\4\uffff\1\50\1\u00dd\1\uffff\1\50\2\uffff\4\50\1\uffff"+
        "\1\u00e5\4\uffff\1\u00e6\1\uffff\1\50\2\uffff\1\50\1\u00e9\1\50"+
        "\3\uffff\1\50\1\u00ee\1\uffff\1\50\2\uffff\1\u00f0\1\uffff\1\u00f1"+
        "\2\uffff";
    static final String DFA14_eofS =
        "\u00f2\uffff";
    static final String DFA14_minS =
        "\1\0\1\154\1\142\1\141\1\154\1\141\1\154\1\40\1\157\1\146\1\145"+
        "\1\156\1\154\1\162\2\157\1\145\2\40\1\150\7\uffff\1\157\1\151\1"+
        "\60\2\uffff\2\0\1\uffff\1\52\1\uffff\1\157\1\151\1\141\1\uffff\1"+
        "\152\2\60\1\162\1\145\1\143\1\157\1\171\1\165\1\144\1\163\1\145"+
        "\1\151\1\145\1\40\1\151\1\uffff\1\162\1\40\1\160\1\164\1\154\1\144"+
        "\1\157\1\165\1\162\1\165\1\147\1\162\4\uffff\1\141\1\154\7\uffff"+
        "\1\164\1\144\5\uffff\1\152\1\156\1\171\1\145\2\uffff\1\151\2\160"+
        "\1\163\1\40\1\167\1\60\1\156\1\60\1\145\1\156\1\164\1\145\1\uffff"+
        "\1\144\1\60\2\uffff\1\145\1\151\1\60\1\40\1\141\1\145\1\163\1\156"+
        "\1\143\1\162\1\145\1\156\1\157\1\60\2\145\1\164\1\60\1\143\1\160"+
        "\1\60\1\163\1\40\1\170\1\60\1\uffff\1\144\1\uffff\3\60\1\156\1\145"+
        "\1\166\1\uffff\1\141\1\154\3\uffff\1\144\1\60\1\145\1\40\1\150\1"+
        "\145\1\143\1\147\1\162\1\uffff\1\60\1\143\1\40\1\uffff\2\164\1\uffff"+
        "\1\60\5\uffff\1\60\3\uffff\1\40\1\60\1\145\1\164\1\60\1\143\1\uffff"+
        "\1\55\1\143\1\151\1\145\1\164\1\145\1\60\1\uffff\1\164\1\151\2\60"+
        "\4\uffff\1\162\1\60\1\uffff\1\141\1\uffff\1\143\1\156\1\163\1\151"+
        "\1\40\1\uffff\1\60\4\uffff\1\60\1\uffff\1\163\2\uffff\1\147\1\60"+
        "\1\157\1\170\2\uffff\1\164\1\60\1\uffff\1\156\2\uffff\1\60\1\uffff"+
        "\1\60\2\uffff";
    static final String DFA14_maxS =
        "\1\uffff\2\162\1\164\1\156\1\150\1\162\1\40\1\157\1\146\1\145\2"+
        "\156\1\162\1\157\1\165\1\151\2\72\1\157\7\uffff\1\157\1\151\1\71"+
        "\2\uffff\2\uffff\1\uffff\1\57\1\uffff\1\157\1\151\1\141\1\uffff"+
        "\1\152\2\172\1\162\1\157\1\164\1\157\1\171\1\165\1\144\1\163\1\145"+
        "\1\151\1\145\1\40\1\151\1\uffff\1\162\1\172\1\160\1\164\1\154\1"+
        "\144\1\157\1\166\1\162\1\165\1\147\1\162\4\uffff\1\141\1\154\7\uffff"+
        "\1\164\1\144\5\uffff\1\152\1\156\1\171\1\145\2\uffff\1\151\2\160"+
        "\1\163\1\40\1\167\1\172\1\156\1\172\1\145\1\156\1\164\1\145\1\uffff"+
        "\1\144\1\172\2\uffff\1\145\1\151\2\172\1\141\1\145\1\163\1\156\1"+
        "\143\1\162\1\145\1\156\1\157\1\172\2\145\1\164\1\172\1\143\1\160"+
        "\1\172\1\163\1\172\1\171\1\172\1\uffff\1\144\1\uffff\3\172\1\156"+
        "\1\145\1\166\1\uffff\1\141\1\154\3\uffff\1\144\1\172\1\145\1\40"+
        "\1\150\1\145\1\143\1\147\1\162\1\uffff\1\172\1\143\1\40\1\uffff"+
        "\2\164\1\uffff\1\172\5\uffff\1\172\3\uffff\1\40\1\172\1\145\1\164"+
        "\1\172\1\143\1\uffff\1\55\1\143\1\151\1\145\1\164\1\145\1\172\1"+
        "\uffff\2\164\2\172\4\uffff\1\162\1\172\1\uffff\1\141\1\uffff\1\167"+
        "\1\156\1\163\1\151\1\40\1\uffff\1\172\4\uffff\1\172\1\uffff\1\163"+
        "\2\uffff\1\147\1\172\1\157\1\171\2\uffff\1\164\1\172\1\uffff\1\156"+
        "\2\uffff\1\172\1\uffff\1\172\2\uffff";
    static final String DFA14_acceptS =
        "\24\uffff\1\44\1\45\1\46\1\55\1\56\1\57\1\60\3\uffff\1\73\1\74\2"+
        "\uffff\1\76\1\uffff\1\77\3\uffff\1\74\20\uffff\1\7\14\uffff\1\33"+
        "\1\47\1\34\1\50\2\uffff\1\44\1\45\1\46\1\55\1\56\1\57\1\60\2\uffff"+
        "\1\73\1\75\1\76\1\100\1\101\4\uffff\1\52\1\62\15\uffff\1\32\2\uffff"+
        "\1\43\1\11\31\uffff\1\66\1\uffff\1\4\6\uffff\1\67\2\uffff\1\15\1"+
        "\21\1\61\11\uffff\1\63\3\uffff\1\70\2\uffff\1\14\1\uffff\1\36\1"+
        "\17\1\41\1\42\1\65\1\uffff\1\22\1\5\1\16\6\uffff\1\23\7\uffff\1"+
        "\64\4\uffff\1\24\1\71\1\6\1\35\2\uffff\1\13\1\uffff\1\72\5\uffff"+
        "\1\54\1\uffff\1\30\1\31\1\2\1\3\1\uffff\1\12\1\uffff\1\25\1\27\4"+
        "\uffff\1\1\1\10\2\uffff\1\26\1\uffff\1\37\1\40\1\uffff\1\53\1\uffff"+
        "\1\20\1\51";
    static final String DFA14_specialS =
        "\1\1\37\uffff\1\0\1\2\u00d0\uffff}>";
    static final String[] DFA14_transitionS = {
            "\11\44\2\42\2\44\1\42\22\44\1\42\1\44\1\40\1\44\1\26\2\44\1"+
            "\41\1\24\1\25\3\44\1\35\1\44\1\43\12\36\2\44\1\30\1\32\1\31"+
            "\1\27\1\44\10\37\1\7\21\37\4\44\1\37\1\44\1\14\1\15\1\23\1\20"+
            "\1\4\1\10\1\6\1\34\1\11\3\37\1\16\1\33\1\2\1\1\1\37\1\12\1\3"+
            "\1\17\1\13\1\37\1\5\1\21\1\22\1\37\uff85\44",
            "\1\47\2\uffff\1\46\2\uffff\1\45",
            "\1\51\3\uffff\1\52\13\uffff\1\53",
            "\1\60\1\uffff\1\54\1\uffff\1\56\2\uffff\1\57\6\uffff\1\61\4"+
            "\uffff\1\55",
            "\1\63\1\uffff\1\62",
            "\1\65\6\uffff\1\64",
            "\1\70\2\uffff\1\67\2\uffff\1\66",
            "\1\71",
            "\1\72",
            "\1\73",
            "\1\74",
            "\1\75",
            "\1\76\1\uffff\1\77",
            "\1\100",
            "\1\101",
            "\1\103\5\uffff\1\102",
            "\1\104\3\uffff\1\105",
            "\1\107\31\uffff\1\106",
            "\1\111\31\uffff\1\110",
            "\1\112\6\uffff\1\113",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\123",
            "\1\124",
            "\12\125",
            "",
            "",
            "\0\126",
            "\0\126",
            "",
            "\1\130\4\uffff\1\131",
            "",
            "\1\132",
            "\1\133",
            "\1\134",
            "",
            "\1\135",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\140",
            "\1\142\11\uffff\1\141",
            "\1\143\20\uffff\1\144",
            "\1\145",
            "\1\146",
            "\1\147",
            "\1\150",
            "\1\151",
            "\1\152",
            "\1\153",
            "\1\154",
            "\1\155",
            "\1\156",
            "",
            "\1\157",
            "\1\160\17\uffff\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32"+
            "\50",
            "\1\162",
            "\1\163",
            "\1\164",
            "\1\165",
            "\1\166",
            "\1\170\1\167",
            "\1\171",
            "\1\172",
            "\1\173",
            "\1\174",
            "",
            "",
            "",
            "",
            "\1\175",
            "\1\176",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\177",
            "\1\u0080",
            "",
            "",
            "",
            "",
            "",
            "\1\u0081",
            "\1\u0082",
            "\1\u0083",
            "\1\u0084",
            "",
            "",
            "\1\u0085",
            "\1\u0086",
            "\1\u0087",
            "\1\u0088",
            "\1\u0089",
            "\1\u008a",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u008c",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u008e",
            "\1\u008f",
            "\1\u0090",
            "\1\u0091",
            "",
            "\1\u0092",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\4\50\1\u0093\25\50",
            "",
            "",
            "\1\u0095",
            "\1\u0096",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u0098\17\uffff\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff"+
            "\32\50",
            "\1\u009a",
            "\1\u009b",
            "\1\u009c",
            "\1\u009d",
            "\1\u009e",
            "\1\u009f",
            "\1\u00a0",
            "\1\u00a1",
            "\1\u00a2",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00a4",
            "\1\u00a5",
            "\1\u00a6",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00a8",
            "\1\u00a9",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00ab",
            "\1\u00ac\17\uffff\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff"+
            "\32\50",
            "\1\u00ae\1\u00af",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "\1\u00b1",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00b5",
            "\1\u00b6",
            "\1\u00b7",
            "",
            "\1\u00b8",
            "\1\u00b9",
            "",
            "",
            "",
            "\1\u00ba",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00bc",
            "\1\u00bd",
            "\1\u00be",
            "\1\u00bf",
            "\1\u00c0",
            "\1\u00c1",
            "\1\u00c2",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00c4",
            "\1\u00c5",
            "",
            "\1\u00c6",
            "\1\u00c7",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "",
            "",
            "",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "",
            "",
            "\1\u00ca",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00cc",
            "\1\u00cd",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00cf",
            "",
            "\1\u00d0",
            "\1\u00d1",
            "\1\u00d2",
            "\1\u00d3",
            "\1\u00d4",
            "\1\u00d5",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "\1\u00d7",
            "\1\u00d8\12\uffff\1\u00d9",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "",
            "",
            "",
            "\1\u00dc",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "\1\u00de",
            "",
            "\1\u00e0\23\uffff\1\u00df",
            "\1\u00e1",
            "\1\u00e2",
            "\1\u00e3",
            "\1\u00e4",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "",
            "",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "\1\u00e7",
            "",
            "",
            "\1\u00e8",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00ea",
            "\1\u00eb\1\u00ec",
            "",
            "",
            "\1\u00ed",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "\1\u00ef",
            "",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            ""
    };

    static final short[] DFA14_eot = DFA.unpackEncodedString(DFA14_eotS);
    static final short[] DFA14_eof = DFA.unpackEncodedString(DFA14_eofS);
    static final char[] DFA14_min = DFA.unpackEncodedStringToUnsignedChars(DFA14_minS);
    static final char[] DFA14_max = DFA.unpackEncodedStringToUnsignedChars(DFA14_maxS);
    static final short[] DFA14_accept = DFA.unpackEncodedString(DFA14_acceptS);
    static final short[] DFA14_special = DFA.unpackEncodedString(DFA14_specialS);
    static final short[][] DFA14_transition;

    static {
        int numStates = DFA14_transitionS.length;
        DFA14_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA14_transition[i] = DFA.unpackEncodedString(DFA14_transitionS[i]);
        }
    }

    class DFA14 extends DFA {

        public DFA14(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 14;
            this.eot = DFA14_eot;
            this.eof = DFA14_eof;
            this.min = DFA14_min;
            this.max = DFA14_max;
            this.accept = DFA14_accept;
            this.special = DFA14_special;
            this.transition = DFA14_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | RULE_NUM | RULE_ID | RULE_STRING | RULE_WS | RULE_ANY_OTHER | RULE_ML_COMMENT | RULE_SL_COMMENT );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA14_32 = input.LA(1);

                        s = -1;
                        if ( ((LA14_32>='\u0000' && LA14_32<='\uFFFF')) ) {s = 86;}

                        else s = 36;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA14_0 = input.LA(1);

                        s = -1;
                        if ( (LA14_0=='p') ) {s = 1;}

                        else if ( (LA14_0=='o') ) {s = 2;}

                        else if ( (LA14_0=='s') ) {s = 3;}

                        else if ( (LA14_0=='e') ) {s = 4;}

                        else if ( (LA14_0=='w') ) {s = 5;}

                        else if ( (LA14_0=='g') ) {s = 6;}

                        else if ( (LA14_0=='I') ) {s = 7;}

                        else if ( (LA14_0=='f') ) {s = 8;}

                        else if ( (LA14_0=='i') ) {s = 9;}

                        else if ( (LA14_0=='r') ) {s = 10;}

                        else if ( (LA14_0=='u') ) {s = 11;}

                        else if ( (LA14_0=='a') ) {s = 12;}

                        else if ( (LA14_0=='b') ) {s = 13;}

                        else if ( (LA14_0=='m') ) {s = 14;}

                        else if ( (LA14_0=='t') ) {s = 15;}

                        else if ( (LA14_0=='d') ) {s = 16;}

                        else if ( (LA14_0=='x') ) {s = 17;}

                        else if ( (LA14_0=='y') ) {s = 18;}

                        else if ( (LA14_0=='c') ) {s = 19;}

                        else if ( (LA14_0=='(') ) {s = 20;}

                        else if ( (LA14_0==')') ) {s = 21;}

                        else if ( (LA14_0=='$') ) {s = 22;}

                        else if ( (LA14_0=='?') ) {s = 23;}

                        else if ( (LA14_0=='<') ) {s = 24;}

                        else if ( (LA14_0=='>') ) {s = 25;}

                        else if ( (LA14_0=='=') ) {s = 26;}

                        else if ( (LA14_0=='n') ) {s = 27;}

                        else if ( (LA14_0=='h') ) {s = 28;}

                        else if ( (LA14_0=='-') ) {s = 29;}

                        else if ( ((LA14_0>='0' && LA14_0<='9')) ) {s = 30;}

                        else if ( ((LA14_0>='A' && LA14_0<='H')||(LA14_0>='J' && LA14_0<='Z')||LA14_0=='_'||(LA14_0>='j' && LA14_0<='l')||LA14_0=='q'||LA14_0=='v'||LA14_0=='z') ) {s = 31;}

                        else if ( (LA14_0=='\"') ) {s = 32;}

                        else if ( (LA14_0=='\'') ) {s = 33;}

                        else if ( ((LA14_0>='\t' && LA14_0<='\n')||LA14_0=='\r'||LA14_0==' ') ) {s = 34;}

                        else if ( (LA14_0=='/') ) {s = 35;}

                        else if ( ((LA14_0>='\u0000' && LA14_0<='\b')||(LA14_0>='\u000B' && LA14_0<='\f')||(LA14_0>='\u000E' && LA14_0<='\u001F')||LA14_0=='!'||LA14_0=='#'||(LA14_0>='%' && LA14_0<='&')||(LA14_0>='*' && LA14_0<=',')||LA14_0=='.'||(LA14_0>=':' && LA14_0<=';')||LA14_0=='@'||(LA14_0>='[' && LA14_0<='^')||LA14_0=='`'||(LA14_0>='{' && LA14_0<='\uFFFF')) ) {s = 36;}

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA14_33 = input.LA(1);

                        s = -1;
                        if ( ((LA14_33>='\u0000' && LA14_33<='\uFFFF')) ) {s = 86;}

                        else s = 36;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 14, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}