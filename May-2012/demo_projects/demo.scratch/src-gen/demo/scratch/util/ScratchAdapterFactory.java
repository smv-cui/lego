/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch.util;

import demo.scratch.And;
import demo.scratch.Binary;
import demo.scratch.BinaryVar;
import demo.scratch.Block;
import demo.scratch.Broadcast;
import demo.scratch.ChangeX;
import demo.scratch.ChangeY;
import demo.scratch.Control;
import demo.scratch.Eq;
import demo.scratch.Forever;
import demo.scratch.Glide;
import demo.scratch.GoTo;
import demo.scratch.GoToXY;
import demo.scratch.Gt;
import demo.scratch.Hide;
import demo.scratch.IObject;
import demo.scratch.If;
import demo.scratch.IfEdge;
import demo.scratch.Looks;
import demo.scratch.Loop;
import demo.scratch.Lt;
import demo.scratch.Message;
import demo.scratch.Motion;
import demo.scratch.Move;
import demo.scratch.Not;
import demo.scratch.NumberObjVar;
import demo.scratch.NumberValue;
import demo.scratch.NumberVar;
import demo.scratch.NumberVarD;
import demo.scratch.NumberVarX;
import demo.scratch.NumberVarY;
import demo.scratch.ObjectPredef;
import demo.scratch.ObjectRef;
import demo.scratch.Operator;
import demo.scratch.Or;
import demo.scratch.Play;
import demo.scratch.PointDirection;
import demo.scratch.PointTowards;
import demo.scratch.Project;
import demo.scratch.RepeatN;
import demo.scratch.RepeatUntil;
import demo.scratch.Say;
import demo.scratch.ScratchPackage;
import demo.scratch.Script;
import demo.scratch.SetX;
import demo.scratch.SetY;
import demo.scratch.Show;
import demo.scratch.Sound;
import demo.scratch.Stop;
import demo.scratch.TouchsColor;
import demo.scratch.TurnCCW;
import demo.scratch.TurnCW;
import demo.scratch.Wait;
import demo.scratch.WaitDelay;
import demo.scratch.WaitUntil;
import demo.scratch.When;
import demo.scratch.WhenFlag;
import demo.scratch.WhenMsg;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see demo.scratch.ScratchPackage
 * @generated
 */
public class ScratchAdapterFactory extends AdapterFactoryImpl
{
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ScratchPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScratchAdapterFactory()
	{
		if (modelPackage == null)
		{
			modelPackage = ScratchPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object)
	{
		if (object == modelPackage)
		{
			return true;
		}
		if (object instanceof EObject)
		{
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScratchSwitch<Adapter> modelSwitch =
		new ScratchSwitch<Adapter>()
		{
			@Override
			public Adapter caseProject(Project object)
			{
				return createProjectAdapter();
			}
			@Override
			public Adapter caseObject(demo.scratch.Object object)
			{
				return createObjectAdapter();
			}
			@Override
			public Adapter caseScript(Script object)
			{
				return createScriptAdapter();
			}
			@Override
			public Adapter caseBlock(Block object)
			{
				return createBlockAdapter();
			}
			@Override
			public Adapter caseIObject(IObject object)
			{
				return createIObjectAdapter();
			}
			@Override
			public Adapter caseObjectRef(ObjectRef object)
			{
				return createObjectRefAdapter();
			}
			@Override
			public Adapter caseObjectPredef(ObjectPredef object)
			{
				return createObjectPredefAdapter();
			}
			@Override
			public Adapter caseWhen(When object)
			{
				return createWhenAdapter();
			}
			@Override
			public Adapter caseWhenFlag(WhenFlag object)
			{
				return createWhenFlagAdapter();
			}
			@Override
			public Adapter caseWhenMsg(WhenMsg object)
			{
				return createWhenMsgAdapter();
			}
			@Override
			public Adapter caseMessage(Message object)
			{
				return createMessageAdapter();
			}
			@Override
			public Adapter caseMotion(Motion object)
			{
				return createMotionAdapter();
			}
			@Override
			public Adapter caseIfEdge(IfEdge object)
			{
				return createIfEdgeAdapter();
			}
			@Override
			public Adapter caseMove(Move object)
			{
				return createMoveAdapter();
			}
			@Override
			public Adapter caseTurnCW(TurnCW object)
			{
				return createTurnCWAdapter();
			}
			@Override
			public Adapter caseTurnCCW(TurnCCW object)
			{
				return createTurnCCWAdapter();
			}
			@Override
			public Adapter casePointDirection(PointDirection object)
			{
				return createPointDirectionAdapter();
			}
			@Override
			public Adapter casePointTowards(PointTowards object)
			{
				return createPointTowardsAdapter();
			}
			@Override
			public Adapter caseChangeX(ChangeX object)
			{
				return createChangeXAdapter();
			}
			@Override
			public Adapter caseChangeY(ChangeY object)
			{
				return createChangeYAdapter();
			}
			@Override
			public Adapter caseSetX(SetX object)
			{
				return createSetXAdapter();
			}
			@Override
			public Adapter caseSetY(SetY object)
			{
				return createSetYAdapter();
			}
			@Override
			public Adapter caseGoToXY(GoToXY object)
			{
				return createGoToXYAdapter();
			}
			@Override
			public Adapter caseGoTo(GoTo object)
			{
				return createGoToAdapter();
			}
			@Override
			public Adapter caseGlide(Glide object)
			{
				return createGlideAdapter();
			}
			@Override
			public Adapter caseControl(Control object)
			{
				return createControlAdapter();
			}
			@Override
			public Adapter caseIf(If object)
			{
				return createIfAdapter();
			}
			@Override
			public Adapter caseBroadcast(Broadcast object)
			{
				return createBroadcastAdapter();
			}
			@Override
			public Adapter caseStop(Stop object)
			{
				return createStopAdapter();
			}
			@Override
			public Adapter caseWait(Wait object)
			{
				return createWaitAdapter();
			}
			@Override
			public Adapter caseWaitDelay(WaitDelay object)
			{
				return createWaitDelayAdapter();
			}
			@Override
			public Adapter caseWaitUntil(WaitUntil object)
			{
				return createWaitUntilAdapter();
			}
			@Override
			public Adapter caseLoop(Loop object)
			{
				return createLoopAdapter();
			}
			@Override
			public Adapter caseForever(Forever object)
			{
				return createForeverAdapter();
			}
			@Override
			public Adapter caseRepeatN(RepeatN object)
			{
				return createRepeatNAdapter();
			}
			@Override
			public Adapter caseRepeatUntil(RepeatUntil object)
			{
				return createRepeatUntilAdapter();
			}
			@Override
			public Adapter caseOperator(Operator object)
			{
				return createOperatorAdapter();
			}
			@Override
			public Adapter caseNumber(demo.scratch.Number object)
			{
				return createNumberAdapter();
			}
			@Override
			public Adapter caseBinary(Binary object)
			{
				return createBinaryAdapter();
			}
			@Override
			public Adapter caseNumberValue(NumberValue object)
			{
				return createNumberValueAdapter();
			}
			@Override
			public Adapter caseNumberObjVar(NumberObjVar object)
			{
				return createNumberObjVarAdapter();
			}
			@Override
			public Adapter caseNumberVar(NumberVar object)
			{
				return createNumberVarAdapter();
			}
			@Override
			public Adapter caseNumberVarX(NumberVarX object)
			{
				return createNumberVarXAdapter();
			}
			@Override
			public Adapter caseNumberVarY(NumberVarY object)
			{
				return createNumberVarYAdapter();
			}
			@Override
			public Adapter caseNumberVarD(NumberVarD object)
			{
				return createNumberVarDAdapter();
			}
			@Override
			public Adapter caseBinaryVar(BinaryVar object)
			{
				return createBinaryVarAdapter();
			}
			@Override
			public Adapter caseLt(Lt object)
			{
				return createLtAdapter();
			}
			@Override
			public Adapter caseGt(Gt object)
			{
				return createGtAdapter();
			}
			@Override
			public Adapter caseEq(Eq object)
			{
				return createEqAdapter();
			}
			@Override
			public Adapter caseAnd(And object)
			{
				return createAndAdapter();
			}
			@Override
			public Adapter caseOr(Or object)
			{
				return createOrAdapter();
			}
			@Override
			public Adapter caseNot(Not object)
			{
				return createNotAdapter();
			}
			@Override
			public Adapter caseTouchsColor(TouchsColor object)
			{
				return createTouchsColorAdapter();
			}
			@Override
			public Adapter caseLooks(Looks object)
			{
				return createLooksAdapter();
			}
			@Override
			public Adapter caseHide(Hide object)
			{
				return createHideAdapter();
			}
			@Override
			public Adapter caseShow(Show object)
			{
				return createShowAdapter();
			}
			@Override
			public Adapter caseSay(Say object)
			{
				return createSayAdapter();
			}
			@Override
			public Adapter caseSound(Sound object)
			{
				return createSoundAdapter();
			}
			@Override
			public Adapter casePlay(Play object)
			{
				return createPlayAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object)
			{
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target)
	{
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Project <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Project
	 * @generated
	 */
	public Adapter createProjectAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Object <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Object
	 * @generated
	 */
	public Adapter createObjectAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Script <em>Script</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Script
	 * @generated
	 */
	public Adapter createScriptAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Block <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Block
	 * @generated
	 */
	public Adapter createBlockAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.IObject <em>IObject</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.IObject
	 * @generated
	 */
	public Adapter createIObjectAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.ObjectRef <em>Object Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.ObjectRef
	 * @generated
	 */
	public Adapter createObjectRefAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.ObjectPredef <em>Object Predef</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.ObjectPredef
	 * @generated
	 */
	public Adapter createObjectPredefAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.When <em>When</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.When
	 * @generated
	 */
	public Adapter createWhenAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.WhenFlag <em>When Flag</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.WhenFlag
	 * @generated
	 */
	public Adapter createWhenFlagAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.WhenMsg <em>When Msg</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.WhenMsg
	 * @generated
	 */
	public Adapter createWhenMsgAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Message <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Message
	 * @generated
	 */
	public Adapter createMessageAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Motion <em>Motion</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Motion
	 * @generated
	 */
	public Adapter createMotionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.IfEdge <em>If Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.IfEdge
	 * @generated
	 */
	public Adapter createIfEdgeAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Move <em>Move</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Move
	 * @generated
	 */
	public Adapter createMoveAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.TurnCW <em>Turn CW</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.TurnCW
	 * @generated
	 */
	public Adapter createTurnCWAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.TurnCCW <em>Turn CCW</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.TurnCCW
	 * @generated
	 */
	public Adapter createTurnCCWAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.PointDirection <em>Point Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.PointDirection
	 * @generated
	 */
	public Adapter createPointDirectionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.PointTowards <em>Point Towards</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.PointTowards
	 * @generated
	 */
	public Adapter createPointTowardsAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.ChangeX <em>Change X</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.ChangeX
	 * @generated
	 */
	public Adapter createChangeXAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.ChangeY <em>Change Y</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.ChangeY
	 * @generated
	 */
	public Adapter createChangeYAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.SetX <em>Set X</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.SetX
	 * @generated
	 */
	public Adapter createSetXAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.SetY <em>Set Y</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.SetY
	 * @generated
	 */
	public Adapter createSetYAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.GoToXY <em>Go To XY</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.GoToXY
	 * @generated
	 */
	public Adapter createGoToXYAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.GoTo <em>Go To</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.GoTo
	 * @generated
	 */
	public Adapter createGoToAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Glide <em>Glide</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Glide
	 * @generated
	 */
	public Adapter createGlideAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Control <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Control
	 * @generated
	 */
	public Adapter createControlAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.If <em>If</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.If
	 * @generated
	 */
	public Adapter createIfAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Broadcast <em>Broadcast</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Broadcast
	 * @generated
	 */
	public Adapter createBroadcastAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Stop <em>Stop</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Stop
	 * @generated
	 */
	public Adapter createStopAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Wait <em>Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Wait
	 * @generated
	 */
	public Adapter createWaitAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.WaitDelay <em>Wait Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.WaitDelay
	 * @generated
	 */
	public Adapter createWaitDelayAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.WaitUntil <em>Wait Until</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.WaitUntil
	 * @generated
	 */
	public Adapter createWaitUntilAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Loop <em>Loop</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Loop
	 * @generated
	 */
	public Adapter createLoopAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Forever <em>Forever</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Forever
	 * @generated
	 */
	public Adapter createForeverAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.RepeatN <em>Repeat N</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.RepeatN
	 * @generated
	 */
	public Adapter createRepeatNAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.RepeatUntil <em>Repeat Until</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.RepeatUntil
	 * @generated
	 */
	public Adapter createRepeatUntilAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Operator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Operator
	 * @generated
	 */
	public Adapter createOperatorAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Number <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Number
	 * @generated
	 */
	public Adapter createNumberAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Binary <em>Binary</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Binary
	 * @generated
	 */
	public Adapter createBinaryAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.NumberValue <em>Number Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.NumberValue
	 * @generated
	 */
	public Adapter createNumberValueAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.NumberObjVar <em>Number Obj Var</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.NumberObjVar
	 * @generated
	 */
	public Adapter createNumberObjVarAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.NumberVar <em>Number Var</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.NumberVar
	 * @generated
	 */
	public Adapter createNumberVarAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.NumberVarX <em>Number Var X</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.NumberVarX
	 * @generated
	 */
	public Adapter createNumberVarXAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.NumberVarY <em>Number Var Y</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.NumberVarY
	 * @generated
	 */
	public Adapter createNumberVarYAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.NumberVarD <em>Number Var D</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.NumberVarD
	 * @generated
	 */
	public Adapter createNumberVarDAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.BinaryVar <em>Binary Var</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.BinaryVar
	 * @generated
	 */
	public Adapter createBinaryVarAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Lt <em>Lt</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Lt
	 * @generated
	 */
	public Adapter createLtAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Gt <em>Gt</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Gt
	 * @generated
	 */
	public Adapter createGtAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Eq <em>Eq</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Eq
	 * @generated
	 */
	public Adapter createEqAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.And <em>And</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.And
	 * @generated
	 */
	public Adapter createAndAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Or <em>Or</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Or
	 * @generated
	 */
	public Adapter createOrAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Not <em>Not</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Not
	 * @generated
	 */
	public Adapter createNotAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.TouchsColor <em>Touchs Color</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.TouchsColor
	 * @generated
	 */
	public Adapter createTouchsColorAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Looks <em>Looks</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Looks
	 * @generated
	 */
	public Adapter createLooksAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Hide <em>Hide</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Hide
	 * @generated
	 */
	public Adapter createHideAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Show <em>Show</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Show
	 * @generated
	 */
	public Adapter createShowAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Say <em>Say</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Say
	 * @generated
	 */
	public Adapter createSayAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Sound <em>Sound</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Sound
	 * @generated
	 */
	public Adapter createSoundAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.scratch.Play <em>Play</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.scratch.Play
	 * @generated
	 */
	public Adapter createPlayAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter()
	{
		return null;
	}

} //ScratchAdapterFactory
