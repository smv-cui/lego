/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch.util;

import demo.scratch.And;
import demo.scratch.Binary;
import demo.scratch.BinaryVar;
import demo.scratch.Block;
import demo.scratch.Broadcast;
import demo.scratch.ChangeX;
import demo.scratch.ChangeY;
import demo.scratch.Control;
import demo.scratch.Eq;
import demo.scratch.Forever;
import demo.scratch.Glide;
import demo.scratch.GoTo;
import demo.scratch.GoToXY;
import demo.scratch.Gt;
import demo.scratch.Hide;
import demo.scratch.IObject;
import demo.scratch.If;
import demo.scratch.IfEdge;
import demo.scratch.Looks;
import demo.scratch.Loop;
import demo.scratch.Lt;
import demo.scratch.Message;
import demo.scratch.Motion;
import demo.scratch.Move;
import demo.scratch.Not;
import demo.scratch.NumberObjVar;
import demo.scratch.NumberValue;
import demo.scratch.NumberVar;
import demo.scratch.NumberVarD;
import demo.scratch.NumberVarX;
import demo.scratch.NumberVarY;
import demo.scratch.ObjectPredef;
import demo.scratch.ObjectRef;
import demo.scratch.Operator;
import demo.scratch.Or;
import demo.scratch.Play;
import demo.scratch.PointDirection;
import demo.scratch.PointTowards;
import demo.scratch.Project;
import demo.scratch.RepeatN;
import demo.scratch.RepeatUntil;
import demo.scratch.Say;
import demo.scratch.ScratchPackage;
import demo.scratch.Script;
import demo.scratch.SetX;
import demo.scratch.SetY;
import demo.scratch.Show;
import demo.scratch.Sound;
import demo.scratch.Stop;
import demo.scratch.TouchsColor;
import demo.scratch.TurnCCW;
import demo.scratch.TurnCW;
import demo.scratch.Wait;
import demo.scratch.WaitDelay;
import demo.scratch.WaitUntil;
import demo.scratch.When;
import demo.scratch.WhenFlag;
import demo.scratch.WhenMsg;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see demo.scratch.ScratchPackage
 * @generated
 */
public class ScratchSwitch<T>
{
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ScratchPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScratchSwitch()
	{
		if (modelPackage == null)
		{
			modelPackage = ScratchPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject)
	{
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject)
	{
		if (theEClass.eContainer() == modelPackage)
		{
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else
		{
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject)
	{
		switch (classifierID)
		{
			case ScratchPackage.PROJECT:
			{
				Project project = (Project)theEObject;
				T result = caseProject(project);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.OBJECT:
			{
				demo.scratch.Object object = (demo.scratch.Object)theEObject;
				T result = caseObject(object);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.SCRIPT:
			{
				Script script = (Script)theEObject;
				T result = caseScript(script);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.BLOCK:
			{
				Block block = (Block)theEObject;
				T result = caseBlock(block);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.IOBJECT:
			{
				IObject iObject = (IObject)theEObject;
				T result = caseIObject(iObject);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.OBJECT_REF:
			{
				ObjectRef objectRef = (ObjectRef)theEObject;
				T result = caseObjectRef(objectRef);
				if (result == null) result = caseIObject(objectRef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.OBJECT_PREDEF:
			{
				ObjectPredef objectPredef = (ObjectPredef)theEObject;
				T result = caseObjectPredef(objectPredef);
				if (result == null) result = caseIObject(objectPredef);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.WHEN:
			{
				When when = (When)theEObject;
				T result = caseWhen(when);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.WHEN_FLAG:
			{
				WhenFlag whenFlag = (WhenFlag)theEObject;
				T result = caseWhenFlag(whenFlag);
				if (result == null) result = caseWhen(whenFlag);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.WHEN_MSG:
			{
				WhenMsg whenMsg = (WhenMsg)theEObject;
				T result = caseWhenMsg(whenMsg);
				if (result == null) result = caseWhen(whenMsg);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.MESSAGE:
			{
				Message message = (Message)theEObject;
				T result = caseMessage(message);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.MOTION:
			{
				Motion motion = (Motion)theEObject;
				T result = caseMotion(motion);
				if (result == null) result = caseBlock(motion);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.IF_EDGE:
			{
				IfEdge ifEdge = (IfEdge)theEObject;
				T result = caseIfEdge(ifEdge);
				if (result == null) result = caseMotion(ifEdge);
				if (result == null) result = caseBlock(ifEdge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.MOVE:
			{
				Move move = (Move)theEObject;
				T result = caseMove(move);
				if (result == null) result = caseMotion(move);
				if (result == null) result = caseBlock(move);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.TURN_CW:
			{
				TurnCW turnCW = (TurnCW)theEObject;
				T result = caseTurnCW(turnCW);
				if (result == null) result = caseMotion(turnCW);
				if (result == null) result = caseBlock(turnCW);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.TURN_CCW:
			{
				TurnCCW turnCCW = (TurnCCW)theEObject;
				T result = caseTurnCCW(turnCCW);
				if (result == null) result = caseMotion(turnCCW);
				if (result == null) result = caseBlock(turnCCW);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.POINT_DIRECTION:
			{
				PointDirection pointDirection = (PointDirection)theEObject;
				T result = casePointDirection(pointDirection);
				if (result == null) result = caseMotion(pointDirection);
				if (result == null) result = caseBlock(pointDirection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.POINT_TOWARDS:
			{
				PointTowards pointTowards = (PointTowards)theEObject;
				T result = casePointTowards(pointTowards);
				if (result == null) result = caseMotion(pointTowards);
				if (result == null) result = caseBlock(pointTowards);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.CHANGE_X:
			{
				ChangeX changeX = (ChangeX)theEObject;
				T result = caseChangeX(changeX);
				if (result == null) result = caseMotion(changeX);
				if (result == null) result = caseBlock(changeX);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.CHANGE_Y:
			{
				ChangeY changeY = (ChangeY)theEObject;
				T result = caseChangeY(changeY);
				if (result == null) result = caseMotion(changeY);
				if (result == null) result = caseBlock(changeY);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.SET_X:
			{
				SetX setX = (SetX)theEObject;
				T result = caseSetX(setX);
				if (result == null) result = caseMotion(setX);
				if (result == null) result = caseBlock(setX);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.SET_Y:
			{
				SetY setY = (SetY)theEObject;
				T result = caseSetY(setY);
				if (result == null) result = caseMotion(setY);
				if (result == null) result = caseBlock(setY);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.GO_TO_XY:
			{
				GoToXY goToXY = (GoToXY)theEObject;
				T result = caseGoToXY(goToXY);
				if (result == null) result = caseMotion(goToXY);
				if (result == null) result = caseBlock(goToXY);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.GO_TO:
			{
				GoTo goTo = (GoTo)theEObject;
				T result = caseGoTo(goTo);
				if (result == null) result = caseMotion(goTo);
				if (result == null) result = caseBlock(goTo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.GLIDE:
			{
				Glide glide = (Glide)theEObject;
				T result = caseGlide(glide);
				if (result == null) result = caseMotion(glide);
				if (result == null) result = caseBlock(glide);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.CONTROL:
			{
				Control control = (Control)theEObject;
				T result = caseControl(control);
				if (result == null) result = caseBlock(control);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.IF:
			{
				If if_ = (If)theEObject;
				T result = caseIf(if_);
				if (result == null) result = caseControl(if_);
				if (result == null) result = caseBlock(if_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.BROADCAST:
			{
				Broadcast broadcast = (Broadcast)theEObject;
				T result = caseBroadcast(broadcast);
				if (result == null) result = caseControl(broadcast);
				if (result == null) result = caseBlock(broadcast);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.STOP:
			{
				Stop stop = (Stop)theEObject;
				T result = caseStop(stop);
				if (result == null) result = caseControl(stop);
				if (result == null) result = caseBlock(stop);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.WAIT:
			{
				Wait wait = (Wait)theEObject;
				T result = caseWait(wait);
				if (result == null) result = caseControl(wait);
				if (result == null) result = caseBlock(wait);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.WAIT_DELAY:
			{
				WaitDelay waitDelay = (WaitDelay)theEObject;
				T result = caseWaitDelay(waitDelay);
				if (result == null) result = caseWait(waitDelay);
				if (result == null) result = caseControl(waitDelay);
				if (result == null) result = caseBlock(waitDelay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.WAIT_UNTIL:
			{
				WaitUntil waitUntil = (WaitUntil)theEObject;
				T result = caseWaitUntil(waitUntil);
				if (result == null) result = caseWait(waitUntil);
				if (result == null) result = caseControl(waitUntil);
				if (result == null) result = caseBlock(waitUntil);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.LOOP:
			{
				Loop loop = (Loop)theEObject;
				T result = caseLoop(loop);
				if (result == null) result = caseControl(loop);
				if (result == null) result = caseBlock(loop);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.FOREVER:
			{
				Forever forever = (Forever)theEObject;
				T result = caseForever(forever);
				if (result == null) result = caseLoop(forever);
				if (result == null) result = caseControl(forever);
				if (result == null) result = caseBlock(forever);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.REPEAT_N:
			{
				RepeatN repeatN = (RepeatN)theEObject;
				T result = caseRepeatN(repeatN);
				if (result == null) result = caseLoop(repeatN);
				if (result == null) result = caseControl(repeatN);
				if (result == null) result = caseBlock(repeatN);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.REPEAT_UNTIL:
			{
				RepeatUntil repeatUntil = (RepeatUntil)theEObject;
				T result = caseRepeatUntil(repeatUntil);
				if (result == null) result = caseLoop(repeatUntil);
				if (result == null) result = caseControl(repeatUntil);
				if (result == null) result = caseBlock(repeatUntil);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.OPERATOR:
			{
				Operator operator = (Operator)theEObject;
				T result = caseOperator(operator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.NUMBER:
			{
				demo.scratch.Number number = (demo.scratch.Number)theEObject;
				T result = caseNumber(number);
				if (result == null) result = caseOperator(number);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.BINARY:
			{
				Binary binary = (Binary)theEObject;
				T result = caseBinary(binary);
				if (result == null) result = caseOperator(binary);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.NUMBER_VALUE:
			{
				NumberValue numberValue = (NumberValue)theEObject;
				T result = caseNumberValue(numberValue);
				if (result == null) result = caseNumber(numberValue);
				if (result == null) result = caseOperator(numberValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.NUMBER_OBJ_VAR:
			{
				NumberObjVar numberObjVar = (NumberObjVar)theEObject;
				T result = caseNumberObjVar(numberObjVar);
				if (result == null) result = caseNumber(numberObjVar);
				if (result == null) result = caseOperator(numberObjVar);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.NUMBER_VAR:
			{
				NumberVar numberVar = (NumberVar)theEObject;
				T result = caseNumberVar(numberVar);
				if (result == null) result = caseNumber(numberVar);
				if (result == null) result = caseOperator(numberVar);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.NUMBER_VAR_X:
			{
				NumberVarX numberVarX = (NumberVarX)theEObject;
				T result = caseNumberVarX(numberVarX);
				if (result == null) result = caseNumber(numberVarX);
				if (result == null) result = caseOperator(numberVarX);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.NUMBER_VAR_Y:
			{
				NumberVarY numberVarY = (NumberVarY)theEObject;
				T result = caseNumberVarY(numberVarY);
				if (result == null) result = caseNumber(numberVarY);
				if (result == null) result = caseOperator(numberVarY);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.NUMBER_VAR_D:
			{
				NumberVarD numberVarD = (NumberVarD)theEObject;
				T result = caseNumberVarD(numberVarD);
				if (result == null) result = caseNumber(numberVarD);
				if (result == null) result = caseOperator(numberVarD);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.BINARY_VAR:
			{
				BinaryVar binaryVar = (BinaryVar)theEObject;
				T result = caseBinaryVar(binaryVar);
				if (result == null) result = caseBinary(binaryVar);
				if (result == null) result = caseOperator(binaryVar);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.LT:
			{
				Lt lt = (Lt)theEObject;
				T result = caseLt(lt);
				if (result == null) result = caseBinary(lt);
				if (result == null) result = caseOperator(lt);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.GT:
			{
				Gt gt = (Gt)theEObject;
				T result = caseGt(gt);
				if (result == null) result = caseBinary(gt);
				if (result == null) result = caseOperator(gt);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.EQ:
			{
				Eq eq = (Eq)theEObject;
				T result = caseEq(eq);
				if (result == null) result = caseBinary(eq);
				if (result == null) result = caseOperator(eq);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.AND:
			{
				And and = (And)theEObject;
				T result = caseAnd(and);
				if (result == null) result = caseBinary(and);
				if (result == null) result = caseOperator(and);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.OR:
			{
				Or or = (Or)theEObject;
				T result = caseOr(or);
				if (result == null) result = caseBinary(or);
				if (result == null) result = caseOperator(or);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.NOT:
			{
				Not not = (Not)theEObject;
				T result = caseNot(not);
				if (result == null) result = caseBinary(not);
				if (result == null) result = caseOperator(not);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.TOUCHS_COLOR:
			{
				TouchsColor touchsColor = (TouchsColor)theEObject;
				T result = caseTouchsColor(touchsColor);
				if (result == null) result = caseBinary(touchsColor);
				if (result == null) result = caseOperator(touchsColor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.LOOKS:
			{
				Looks looks = (Looks)theEObject;
				T result = caseLooks(looks);
				if (result == null) result = caseBlock(looks);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.HIDE:
			{
				Hide hide = (Hide)theEObject;
				T result = caseHide(hide);
				if (result == null) result = caseLooks(hide);
				if (result == null) result = caseBlock(hide);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.SHOW:
			{
				Show show = (Show)theEObject;
				T result = caseShow(show);
				if (result == null) result = caseLooks(show);
				if (result == null) result = caseBlock(show);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.SAY:
			{
				Say say = (Say)theEObject;
				T result = caseSay(say);
				if (result == null) result = caseLooks(say);
				if (result == null) result = caseBlock(say);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.SOUND:
			{
				Sound sound = (Sound)theEObject;
				T result = caseSound(sound);
				if (result == null) result = caseBlock(sound);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScratchPackage.PLAY:
			{
				Play play = (Play)theEObject;
				T result = casePlay(play);
				if (result == null) result = caseSound(play);
				if (result == null) result = caseBlock(play);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Project</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Project</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProject(Project object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObject(demo.scratch.Object object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Script</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Script</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScript(Script object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBlock(Block object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIObject(IObject object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object Ref</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object Ref</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObjectRef(ObjectRef object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object Predef</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object Predef</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObjectPredef(ObjectPredef object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>When</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>When</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWhen(When object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>When Flag</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>When Flag</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWhenFlag(WhenFlag object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>When Msg</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>When Msg</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWhenMsg(WhenMsg object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Message</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Message</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMessage(Message object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Motion</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Motion</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMotion(Motion object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If Edge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If Edge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfEdge(IfEdge object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Move</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Move</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMove(Move object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Turn CW</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Turn CW</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTurnCW(TurnCW object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Turn CCW</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Turn CCW</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTurnCCW(TurnCCW object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Point Direction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Point Direction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePointDirection(PointDirection object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Point Towards</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Point Towards</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePointTowards(PointTowards object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change X</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change X</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeX(ChangeX object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Y</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Y</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeY(ChangeY object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Set X</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Set X</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSetX(SetX object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Set Y</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Set Y</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSetY(SetY object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Go To XY</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Go To XY</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGoToXY(GoToXY object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Go To</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Go To</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGoTo(GoTo object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Glide</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Glide</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGlide(Glide object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Control</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Control</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseControl(Control object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIf(If object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Broadcast</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Broadcast</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBroadcast(Broadcast object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Stop</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Stop</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStop(Stop object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wait</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wait</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWait(Wait object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wait Delay</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wait Delay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWaitDelay(WaitDelay object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Wait Until</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Wait Until</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWaitUntil(WaitUntil object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Loop</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Loop</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLoop(Loop object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Forever</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Forever</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseForever(Forever object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Repeat N</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Repeat N</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRepeatN(RepeatN object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Repeat Until</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Repeat Until</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRepeatUntil(RepeatUntil object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperator(Operator object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Number</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Number</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNumber(demo.scratch.Number object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinary(Binary object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Number Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Number Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNumberValue(NumberValue object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Number Obj Var</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Number Obj Var</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNumberObjVar(NumberObjVar object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Number Var</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Number Var</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNumberVar(NumberVar object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Number Var X</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Number Var X</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNumberVarX(NumberVarX object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Number Var Y</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Number Var Y</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNumberVarY(NumberVarY object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Number Var D</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Number Var D</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNumberVarD(NumberVarD object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Binary Var</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Binary Var</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBinaryVar(BinaryVar object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lt</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lt</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLt(Lt object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gt</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gt</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGt(Gt object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Eq</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Eq</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEq(Eq object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>And</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>And</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnd(And object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Or</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Or</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOr(Or object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Not</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Not</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNot(Not object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Touchs Color</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Touchs Color</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTouchsColor(TouchsColor object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Looks</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Looks</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLooks(Looks object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hide</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hide</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHide(Hide object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Show</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Show</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShow(Show object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Say</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Say</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSay(Say object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sound</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sound</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSound(Sound object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Play</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Play</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePlay(Play object)
	{
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object)
	{
		return null;
	}

} //ScratchSwitch
