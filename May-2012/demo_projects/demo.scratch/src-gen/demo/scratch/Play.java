/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Play</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.Play#getSound <em>Sound</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getPlay()
 * @model
 * @generated
 */
public interface Play extends Sound
{
	/**
	 * Returns the value of the '<em><b>Sound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sound</em>' attribute.
	 * @see #setSound(String)
	 * @see demo.scratch.ScratchPackage#getPlay_Sound()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getSound();

	/**
	 * Sets the value of the '{@link demo.scratch.Play#getSound <em>Sound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sound</em>' attribute.
	 * @see #getSound()
	 * @generated
	 */
	void setSound(String value);

} // Play
