/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Point Direction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.PointDirection#getAngle <em>Angle</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getPointDirection()
 * @model
 * @generated
 */
public interface PointDirection extends Motion
{
	/**
	 * Returns the value of the '<em><b>Angle</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Angle</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Angle</em>' containment reference.
	 * @see #setAngle(demo.scratch.Number)
	 * @see demo.scratch.ScratchPackage#getPointDirection_Angle()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	demo.scratch.Number getAngle();

	/**
	 * Sets the value of the '{@link demo.scratch.PointDirection#getAngle <em>Angle</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Angle</em>' containment reference.
	 * @see #getAngle()
	 * @generated
	 */
	void setAngle(demo.scratch.Number value);

} // PointDirection
