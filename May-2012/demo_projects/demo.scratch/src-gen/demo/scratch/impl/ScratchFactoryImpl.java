/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch.impl;

import demo.scratch.And;
import demo.scratch.BinaryVar;
import demo.scratch.Broadcast;
import demo.scratch.ChangeX;
import demo.scratch.ChangeY;
import demo.scratch.Eq;
import demo.scratch.Forever;
import demo.scratch.Glide;
import demo.scratch.GoTo;
import demo.scratch.GoToXY;
import demo.scratch.Gt;
import demo.scratch.Hide;
import demo.scratch.If;
import demo.scratch.IfEdge;
import demo.scratch.Lt;
import demo.scratch.Message;
import demo.scratch.Move;
import demo.scratch.Not;
import demo.scratch.NumberObjVar;
import demo.scratch.NumberValue;
import demo.scratch.NumberVar;
import demo.scratch.NumberVarD;
import demo.scratch.NumberVarX;
import demo.scratch.NumberVarY;
import demo.scratch.ObjectPredef;
import demo.scratch.ObjectPredefEnum;
import demo.scratch.ObjectRef;
import demo.scratch.ObjectVariableEnum;
import demo.scratch.Or;
import demo.scratch.Play;
import demo.scratch.PointDirection;
import demo.scratch.PointTowards;
import demo.scratch.Project;
import demo.scratch.RepeatN;
import demo.scratch.RepeatUntil;
import demo.scratch.Say;
import demo.scratch.ScratchFactory;
import demo.scratch.ScratchPackage;
import demo.scratch.Script;
import demo.scratch.SetX;
import demo.scratch.SetY;
import demo.scratch.Show;
import demo.scratch.Stop;
import demo.scratch.TouchsColor;
import demo.scratch.TurnCCW;
import demo.scratch.TurnCW;
import demo.scratch.WaitDelay;
import demo.scratch.WaitUntil;
import demo.scratch.WhenFlag;
import demo.scratch.WhenMsg;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScratchFactoryImpl extends EFactoryImpl implements ScratchFactory
{
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ScratchFactory init()
	{
		try
		{
			ScratchFactory theScratchFactory = (ScratchFactory)EPackage.Registry.INSTANCE.getEFactory("http://smv.unige.ch/Scratch/2012"); 
			if (theScratchFactory != null)
			{
				return theScratchFactory;
			}
		}
		catch (Exception exception)
		{
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ScratchFactoryImplCustom();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScratchFactoryImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass)
	{
		switch (eClass.getClassifierID())
		{
			case ScratchPackage.PROJECT: return createProject();
			case ScratchPackage.OBJECT: return createObject();
			case ScratchPackage.SCRIPT: return createScript();
			case ScratchPackage.OBJECT_REF: return createObjectRef();
			case ScratchPackage.OBJECT_PREDEF: return createObjectPredef();
			case ScratchPackage.WHEN_FLAG: return createWhenFlag();
			case ScratchPackage.WHEN_MSG: return createWhenMsg();
			case ScratchPackage.MESSAGE: return createMessage();
			case ScratchPackage.IF_EDGE: return createIfEdge();
			case ScratchPackage.MOVE: return createMove();
			case ScratchPackage.TURN_CW: return createTurnCW();
			case ScratchPackage.TURN_CCW: return createTurnCCW();
			case ScratchPackage.POINT_DIRECTION: return createPointDirection();
			case ScratchPackage.POINT_TOWARDS: return createPointTowards();
			case ScratchPackage.CHANGE_X: return createChangeX();
			case ScratchPackage.CHANGE_Y: return createChangeY();
			case ScratchPackage.SET_X: return createSetX();
			case ScratchPackage.SET_Y: return createSetY();
			case ScratchPackage.GO_TO_XY: return createGoToXY();
			case ScratchPackage.GO_TO: return createGoTo();
			case ScratchPackage.GLIDE: return createGlide();
			case ScratchPackage.IF: return createIf();
			case ScratchPackage.BROADCAST: return createBroadcast();
			case ScratchPackage.STOP: return createStop();
			case ScratchPackage.WAIT_DELAY: return createWaitDelay();
			case ScratchPackage.WAIT_UNTIL: return createWaitUntil();
			case ScratchPackage.FOREVER: return createForever();
			case ScratchPackage.REPEAT_N: return createRepeatN();
			case ScratchPackage.REPEAT_UNTIL: return createRepeatUntil();
			case ScratchPackage.NUMBER_VALUE: return createNumberValue();
			case ScratchPackage.NUMBER_OBJ_VAR: return createNumberObjVar();
			case ScratchPackage.NUMBER_VAR: return createNumberVar();
			case ScratchPackage.NUMBER_VAR_X: return createNumberVarX();
			case ScratchPackage.NUMBER_VAR_Y: return createNumberVarY();
			case ScratchPackage.NUMBER_VAR_D: return createNumberVarD();
			case ScratchPackage.BINARY_VAR: return createBinaryVar();
			case ScratchPackage.LT: return createLt();
			case ScratchPackage.GT: return createGt();
			case ScratchPackage.EQ: return createEq();
			case ScratchPackage.AND: return createAnd();
			case ScratchPackage.OR: return createOr();
			case ScratchPackage.NOT: return createNot();
			case ScratchPackage.TOUCHS_COLOR: return createTouchsColor();
			case ScratchPackage.HIDE: return createHide();
			case ScratchPackage.SHOW: return createShow();
			case ScratchPackage.SAY: return createSay();
			case ScratchPackage.PLAY: return createPlay();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue)
	{
		switch (eDataType.getClassifierID())
		{
			case ScratchPackage.OBJECT_PREDEF_ENUM:
				return createObjectPredefEnumFromString(eDataType, initialValue);
			case ScratchPackage.OBJECT_VARIABLE_ENUM:
				return createObjectVariableEnumFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue)
	{
		switch (eDataType.getClassifierID())
		{
			case ScratchPackage.OBJECT_PREDEF_ENUM:
				return convertObjectPredefEnumToString(eDataType, instanceValue);
			case ScratchPackage.OBJECT_VARIABLE_ENUM:
				return convertObjectVariableEnumToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Project createProject()
	{
		ProjectImplCustom project = new ProjectImplCustom();
		return project;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public demo.scratch.Object createObject()
	{
		ObjectImplCustom object = new ObjectImplCustom();
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Script createScript()
	{
		ScriptImplCustom script = new ScriptImplCustom();
		return script;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectRef createObjectRef()
	{
		ObjectRefImplCustom objectRef = new ObjectRefImplCustom();
		return objectRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectPredef createObjectPredef()
	{
		ObjectPredefImplCustom objectPredef = new ObjectPredefImplCustom();
		return objectPredef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhenFlag createWhenFlag()
	{
		WhenFlagImplCustom whenFlag = new WhenFlagImplCustom();
		return whenFlag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhenMsg createWhenMsg()
	{
		WhenMsgImplCustom whenMsg = new WhenMsgImplCustom();
		return whenMsg;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message createMessage()
	{
		MessageImplCustom message = new MessageImplCustom();
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfEdge createIfEdge()
	{
		IfEdgeImplCustom ifEdge = new IfEdgeImplCustom();
		return ifEdge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Move createMove()
	{
		MoveImplCustom move = new MoveImplCustom();
		return move;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TurnCW createTurnCW()
	{
		TurnCWImplCustom turnCW = new TurnCWImplCustom();
		return turnCW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TurnCCW createTurnCCW()
	{
		TurnCCWImplCustom turnCCW = new TurnCCWImplCustom();
		return turnCCW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PointDirection createPointDirection()
	{
		PointDirectionImplCustom pointDirection = new PointDirectionImplCustom();
		return pointDirection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PointTowards createPointTowards()
	{
		PointTowardsImplCustom pointTowards = new PointTowardsImplCustom();
		return pointTowards;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeX createChangeX()
	{
		ChangeXImplCustom changeX = new ChangeXImplCustom();
		return changeX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeY createChangeY()
	{
		ChangeYImplCustom changeY = new ChangeYImplCustom();
		return changeY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SetX createSetX()
	{
		SetXImplCustom setX = new SetXImplCustom();
		return setX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SetY createSetY()
	{
		SetYImplCustom setY = new SetYImplCustom();
		return setY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoToXY createGoToXY()
	{
		GoToXYImplCustom goToXY = new GoToXYImplCustom();
		return goToXY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoTo createGoTo()
	{
		GoToImplCustom goTo = new GoToImplCustom();
		return goTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Glide createGlide()
	{
		GlideImplCustom glide = new GlideImplCustom();
		return glide;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public If createIf()
	{
		IfImplCustom if_ = new IfImplCustom();
		return if_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Broadcast createBroadcast()
	{
		BroadcastImplCustom broadcast = new BroadcastImplCustom();
		return broadcast;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Stop createStop()
	{
		StopImplCustom stop = new StopImplCustom();
		return stop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WaitDelay createWaitDelay()
	{
		WaitDelayImplCustom waitDelay = new WaitDelayImplCustom();
		return waitDelay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WaitUntil createWaitUntil()
	{
		WaitUntilImplCustom waitUntil = new WaitUntilImplCustom();
		return waitUntil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Forever createForever()
	{
		ForeverImplCustom forever = new ForeverImplCustom();
		return forever;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RepeatN createRepeatN()
	{
		RepeatNImplCustom repeatN = new RepeatNImplCustom();
		return repeatN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RepeatUntil createRepeatUntil()
	{
		RepeatUntilImplCustom repeatUntil = new RepeatUntilImplCustom();
		return repeatUntil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumberValue createNumberValue()
	{
		NumberValueImplCustom numberValue = new NumberValueImplCustom();
		return numberValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumberObjVar createNumberObjVar()
	{
		NumberObjVarImplCustom numberObjVar = new NumberObjVarImplCustom();
		return numberObjVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumberVar createNumberVar()
	{
		NumberVarImplCustom numberVar = new NumberVarImplCustom();
		return numberVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumberVarX createNumberVarX()
	{
		NumberVarXImplCustom numberVarX = new NumberVarXImplCustom();
		return numberVarX;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumberVarY createNumberVarY()
	{
		NumberVarYImplCustom numberVarY = new NumberVarYImplCustom();
		return numberVarY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NumberVarD createNumberVarD()
	{
		NumberVarDImplCustom numberVarD = new NumberVarDImplCustom();
		return numberVarD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryVar createBinaryVar()
	{
		BinaryVarImplCustom binaryVar = new BinaryVarImplCustom();
		return binaryVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lt createLt()
	{
		LtImplCustom lt = new LtImplCustom();
		return lt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Gt createGt()
	{
		GtImplCustom gt = new GtImplCustom();
		return gt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Eq createEq()
	{
		EqImplCustom eq = new EqImplCustom();
		return eq;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public And createAnd()
	{
		AndImplCustom and = new AndImplCustom();
		return and;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Or createOr()
	{
		OrImplCustom or = new OrImplCustom();
		return or;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Not createNot()
	{
		NotImplCustom not = new NotImplCustom();
		return not;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TouchsColor createTouchsColor()
	{
		TouchsColorImplCustom touchsColor = new TouchsColorImplCustom();
		return touchsColor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Hide createHide()
	{
		HideImplCustom hide = new HideImplCustom();
		return hide;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Show createShow()
	{
		ShowImplCustom show = new ShowImplCustom();
		return show;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Say createSay()
	{
		SayImplCustom say = new SayImplCustom();
		return say;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Play createPlay()
	{
		PlayImplCustom play = new PlayImplCustom();
		return play;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectPredefEnum createObjectPredefEnumFromString(EDataType eDataType, String initialValue)
	{
		ObjectPredefEnum result = ObjectPredefEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertObjectPredefEnumToString(EDataType eDataType, Object instanceValue)
	{
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectVariableEnum createObjectVariableEnumFromString(EDataType eDataType, String initialValue)
	{
		ObjectVariableEnum result = ObjectVariableEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertObjectVariableEnumToString(EDataType eDataType, Object instanceValue)
	{
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScratchPackage getScratchPackage()
	{
		return (ScratchPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ScratchPackage getPackage()
	{
		return ScratchPackage.eINSTANCE;
	}

} //ScratchFactoryImpl
