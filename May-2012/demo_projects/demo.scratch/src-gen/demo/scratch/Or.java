/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Or</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.Or#getLeft <em>Left</em>}</li>
 *   <li>{@link demo.scratch.Or#getRight <em>Right</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getOr()
 * @model
 * @generated
 */
public interface Or extends Binary
{
	/**
	 * Returns the value of the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left</em>' containment reference.
	 * @see #setLeft(Binary)
	 * @see demo.scratch.ScratchPackage#getOr_Left()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	Binary getLeft();

	/**
	 * Sets the value of the '{@link demo.scratch.Or#getLeft <em>Left</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left</em>' containment reference.
	 * @see #getLeft()
	 * @generated
	 */
	void setLeft(Binary value);

	/**
	 * Returns the value of the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' containment reference.
	 * @see #setRight(Binary)
	 * @see demo.scratch.ScratchPackage#getOr_Right()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	Binary getRight();

	/**
	 * Sets the value of the '{@link demo.scratch.Or#getRight <em>Right</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' containment reference.
	 * @see #getRight()
	 * @generated
	 */
	void setRight(Binary value);

} // Or
