/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see demo.scratch.ScratchPackage#getOperator()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Operator extends EObject
{
} // Operator
