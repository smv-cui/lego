/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Number Var D</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see demo.scratch.ScratchPackage#getNumberVarD()
 * @model
 * @generated
 */
public interface NumberVarD extends demo.scratch.Number
{
} // NumberVarD
