/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Change X</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.ChangeX#getX <em>X</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getChangeX()
 * @model
 * @generated
 */
public interface ChangeX extends Motion
{
	/**
	 * Returns the value of the '<em><b>X</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>X</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>X</em>' containment reference.
	 * @see #setX(demo.scratch.Number)
	 * @see demo.scratch.ScratchPackage#getChangeX_X()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	demo.scratch.Number getX();

	/**
	 * Sets the value of the '{@link demo.scratch.ChangeX#getX <em>X</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X</em>' containment reference.
	 * @see #getX()
	 * @generated
	 */
	void setX(demo.scratch.Number value);

} // ChangeX
