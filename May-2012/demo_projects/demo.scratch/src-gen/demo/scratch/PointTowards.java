/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Point Towards</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.PointTowards#getObject <em>Object</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getPointTowards()
 * @model
 * @generated
 */
public interface PointTowards extends Motion
{
	/**
	 * Returns the value of the '<em><b>Object</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' containment reference.
	 * @see #setObject(IObject)
	 * @see demo.scratch.ScratchPackage#getPointTowards_Object()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	IObject getObject();

	/**
	 * Sets the value of the '{@link demo.scratch.PointTowards#getObject <em>Object</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' containment reference.
	 * @see #getObject()
	 * @generated
	 */
	void setObject(IObject value);

} // PointTowards
