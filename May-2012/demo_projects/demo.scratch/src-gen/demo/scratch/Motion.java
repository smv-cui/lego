/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Motion</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see demo.scratch.ScratchPackage#getMotion()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Motion extends Block
{
} // Motion
