/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Repeat Until</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.scratch.RepeatUntil#getCond <em>Cond</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.scratch.ScratchPackage#getRepeatUntil()
 * @model
 * @generated
 */
public interface RepeatUntil extends Loop
{
	/**
	 * Returns the value of the '<em><b>Cond</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cond</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cond</em>' containment reference.
	 * @see #setCond(Binary)
	 * @see demo.scratch.ScratchPackage#getRepeatUntil_Cond()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	Binary getCond();

	/**
	 * Sets the value of the '{@link demo.scratch.RepeatUntil#getCond <em>Cond</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cond</em>' containment reference.
	 * @see #getCond()
	 * @generated
	 */
	void setCond(Binary value);

} // RepeatUntil
