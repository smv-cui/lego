/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see demo.scratch.ScratchFactory
 * @model kind="package"
 * @generated
 */
public interface ScratchPackage extends EPackage
{
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "scratch";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://smv.unige.ch/Scratch/2012";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Scratch";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ScratchPackage eINSTANCE = demo.scratch.impl.ScratchPackageImpl.init();

	/**
	 * The meta object id for the '{@link demo.scratch.impl.ProjectImpl <em>Project</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.ProjectImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getProject()
	 * @generated
	 */
	int PROJECT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Objects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT__OBJECTS = 1;

	/**
	 * The number of structural features of the '<em>Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROJECT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.ObjectImpl <em>Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.ObjectImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getObject()
	 * @generated
	 */
	int OBJECT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Scripts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT__SCRIPTS = 1;

	/**
	 * The number of structural features of the '<em>Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.ScriptImpl <em>Script</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.ScriptImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getScript()
	 * @generated
	 */
	int SCRIPT = 2;

	/**
	 * The feature id for the '<em><b>When</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT__WHEN = 0;

	/**
	 * The feature id for the '<em><b>Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT__BLOCKS = 1;

	/**
	 * The number of structural features of the '<em>Script</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link demo.scratch.Block <em>Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.Block
	 * @see demo.scratch.impl.ScratchPackageImpl#getBlock()
	 * @generated
	 */
	int BLOCK = 3;

	/**
	 * The number of structural features of the '<em>Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link demo.scratch.IObject <em>IObject</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.IObject
	 * @see demo.scratch.impl.ScratchPackageImpl#getIObject()
	 * @generated
	 */
	int IOBJECT = 4;

	/**
	 * The number of structural features of the '<em>IObject</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IOBJECT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.ObjectRefImpl <em>Object Ref</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.ObjectRefImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getObjectRef()
	 * @generated
	 */
	int OBJECT_REF = 5;

	/**
	 * The feature id for the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_REF__OBJECT = IOBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Object Ref</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_REF_FEATURE_COUNT = IOBJECT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.ObjectPredefImpl <em>Object Predef</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.ObjectPredefImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getObjectPredef()
	 * @generated
	 */
	int OBJECT_PREDEF = 6;

	/**
	 * The feature id for the '<em><b>Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PREDEF__OBJECT = IOBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Object Predef</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_PREDEF_FEATURE_COUNT = IOBJECT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.When <em>When</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.When
	 * @see demo.scratch.impl.ScratchPackageImpl#getWhen()
	 * @generated
	 */
	int WHEN = 7;

	/**
	 * The number of structural features of the '<em>When</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.WhenFlagImpl <em>When Flag</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.WhenFlagImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getWhenFlag()
	 * @generated
	 */
	int WHEN_FLAG = 8;

	/**
	 * The number of structural features of the '<em>When Flag</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_FLAG_FEATURE_COUNT = WHEN_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.WhenMsgImpl <em>When Msg</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.WhenMsgImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getWhenMsg()
	 * @generated
	 */
	int WHEN_MSG = 9;

	/**
	 * The feature id for the '<em><b>Msg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_MSG__MSG = WHEN_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>When Msg</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHEN_MSG_FEATURE_COUNT = WHEN_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.MessageImpl <em>Message</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.MessageImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getMessage()
	 * @generated
	 */
	int MESSAGE = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE__NAME = 0;

	/**
	 * The number of structural features of the '<em>Message</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link demo.scratch.Motion <em>Motion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.Motion
	 * @see demo.scratch.impl.ScratchPackageImpl#getMotion()
	 * @generated
	 */
	int MOTION = 11;

	/**
	 * The number of structural features of the '<em>Motion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOTION_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.IfEdgeImpl <em>If Edge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.IfEdgeImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getIfEdge()
	 * @generated
	 */
	int IF_EDGE = 12;

	/**
	 * The number of structural features of the '<em>If Edge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_EDGE_FEATURE_COUNT = MOTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.MoveImpl <em>Move</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.MoveImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getMove()
	 * @generated
	 */
	int MOVE = 13;

	/**
	 * The feature id for the '<em><b>Steps</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVE__STEPS = MOTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Move</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVE_FEATURE_COUNT = MOTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.TurnCWImpl <em>Turn CW</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.TurnCWImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getTurnCW()
	 * @generated
	 */
	int TURN_CW = 14;

	/**
	 * The feature id for the '<em><b>Angle</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TURN_CW__ANGLE = MOTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Turn CW</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TURN_CW_FEATURE_COUNT = MOTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.TurnCCWImpl <em>Turn CCW</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.TurnCCWImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getTurnCCW()
	 * @generated
	 */
	int TURN_CCW = 15;

	/**
	 * The feature id for the '<em><b>Angle</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TURN_CCW__ANGLE = MOTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Turn CCW</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TURN_CCW_FEATURE_COUNT = MOTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.PointDirectionImpl <em>Point Direction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.PointDirectionImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getPointDirection()
	 * @generated
	 */
	int POINT_DIRECTION = 16;

	/**
	 * The feature id for the '<em><b>Angle</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_DIRECTION__ANGLE = MOTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Point Direction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_DIRECTION_FEATURE_COUNT = MOTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.PointTowardsImpl <em>Point Towards</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.PointTowardsImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getPointTowards()
	 * @generated
	 */
	int POINT_TOWARDS = 17;

	/**
	 * The feature id for the '<em><b>Object</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_TOWARDS__OBJECT = MOTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Point Towards</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POINT_TOWARDS_FEATURE_COUNT = MOTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.ChangeXImpl <em>Change X</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.ChangeXImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getChangeX()
	 * @generated
	 */
	int CHANGE_X = 18;

	/**
	 * The feature id for the '<em><b>X</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_X__X = MOTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Change X</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_X_FEATURE_COUNT = MOTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.ChangeYImpl <em>Change Y</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.ChangeYImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getChangeY()
	 * @generated
	 */
	int CHANGE_Y = 19;

	/**
	 * The feature id for the '<em><b>Y</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_Y__Y = MOTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Change Y</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_Y_FEATURE_COUNT = MOTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.SetXImpl <em>Set X</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.SetXImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getSetX()
	 * @generated
	 */
	int SET_X = 20;

	/**
	 * The feature id for the '<em><b>X</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_X__X = MOTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Set X</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_X_FEATURE_COUNT = MOTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.SetYImpl <em>Set Y</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.SetYImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getSetY()
	 * @generated
	 */
	int SET_Y = 21;

	/**
	 * The feature id for the '<em><b>Y</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_Y__Y = MOTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Set Y</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SET_Y_FEATURE_COUNT = MOTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.GoToXYImpl <em>Go To XY</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.GoToXYImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getGoToXY()
	 * @generated
	 */
	int GO_TO_XY = 22;

	/**
	 * The feature id for the '<em><b>X</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GO_TO_XY__X = MOTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GO_TO_XY__Y = MOTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Go To XY</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GO_TO_XY_FEATURE_COUNT = MOTION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.GoToImpl <em>Go To</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.GoToImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getGoTo()
	 * @generated
	 */
	int GO_TO = 23;

	/**
	 * The feature id for the '<em><b>Object</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GO_TO__OBJECT = MOTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Go To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GO_TO_FEATURE_COUNT = MOTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.GlideImpl <em>Glide</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.GlideImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getGlide()
	 * @generated
	 */
	int GLIDE = 24;

	/**
	 * The feature id for the '<em><b>X</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLIDE__X = MOTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Y</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLIDE__Y = MOTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Duration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLIDE__DURATION = MOTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Glide</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLIDE_FEATURE_COUNT = MOTION_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link demo.scratch.Control <em>Control</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.Control
	 * @see demo.scratch.impl.ScratchPackageImpl#getControl()
	 * @generated
	 */
	int CONTROL = 25;

	/**
	 * The number of structural features of the '<em>Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.IfImpl <em>If</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.IfImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getIf()
	 * @generated
	 */
	int IF = 26;

	/**
	 * The feature id for the '<em><b>Cond</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF__COND = CONTROL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>True</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF__TRUE = CONTROL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>False</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF__FALSE = CONTROL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>If</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_FEATURE_COUNT = CONTROL_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.BroadcastImpl <em>Broadcast</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.BroadcastImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getBroadcast()
	 * @generated
	 */
	int BROADCAST = 27;

	/**
	 * The feature id for the '<em><b>Wait</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST__WAIT = CONTROL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Msg</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST__MSG = CONTROL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Broadcast</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BROADCAST_FEATURE_COUNT = CONTROL_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.StopImpl <em>Stop</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.StopImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getStop()
	 * @generated
	 */
	int STOP = 28;

	/**
	 * The feature id for the '<em><b>All</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP__ALL = CONTROL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Script</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP__SCRIPT = CONTROL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Stop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STOP_FEATURE_COUNT = CONTROL_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link demo.scratch.Wait <em>Wait</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.Wait
	 * @see demo.scratch.impl.ScratchPackageImpl#getWait()
	 * @generated
	 */
	int WAIT = 29;

	/**
	 * The number of structural features of the '<em>Wait</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_FEATURE_COUNT = CONTROL_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.WaitDelayImpl <em>Wait Delay</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.WaitDelayImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getWaitDelay()
	 * @generated
	 */
	int WAIT_DELAY = 30;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_DELAY__DELAY = WAIT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Wait Delay</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_DELAY_FEATURE_COUNT = WAIT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.WaitUntilImpl <em>Wait Until</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.WaitUntilImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getWaitUntil()
	 * @generated
	 */
	int WAIT_UNTIL = 31;

	/**
	 * The feature id for the '<em><b>Cond</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_UNTIL__COND = WAIT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Wait Until</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WAIT_UNTIL_FEATURE_COUNT = WAIT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.Loop <em>Loop</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.Loop
	 * @see demo.scratch.impl.ScratchPackageImpl#getLoop()
	 * @generated
	 */
	int LOOP = 32;

	/**
	 * The feature id for the '<em><b>Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP__BLOCKS = CONTROL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Loop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_FEATURE_COUNT = CONTROL_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.ForeverImpl <em>Forever</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.ForeverImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getForever()
	 * @generated
	 */
	int FOREVER = 33;

	/**
	 * The feature id for the '<em><b>Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOREVER__BLOCKS = LOOP__BLOCKS;

	/**
	 * The feature id for the '<em><b>Cond</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOREVER__COND = LOOP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Forever</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOREVER_FEATURE_COUNT = LOOP_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.RepeatNImpl <em>Repeat N</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.RepeatNImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getRepeatN()
	 * @generated
	 */
	int REPEAT_N = 34;

	/**
	 * The feature id for the '<em><b>Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_N__BLOCKS = LOOP__BLOCKS;

	/**
	 * The feature id for the '<em><b>N</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_N__N = LOOP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Repeat N</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_N_FEATURE_COUNT = LOOP_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.RepeatUntilImpl <em>Repeat Until</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.RepeatUntilImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getRepeatUntil()
	 * @generated
	 */
	int REPEAT_UNTIL = 35;

	/**
	 * The feature id for the '<em><b>Blocks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_UNTIL__BLOCKS = LOOP__BLOCKS;

	/**
	 * The feature id for the '<em><b>Cond</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_UNTIL__COND = LOOP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Repeat Until</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPEAT_UNTIL_FEATURE_COUNT = LOOP_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.Operator <em>Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.Operator
	 * @see demo.scratch.impl.ScratchPackageImpl#getOperator()
	 * @generated
	 */
	int OPERATOR = 36;

	/**
	 * The number of structural features of the '<em>Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATOR_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link demo.scratch.Number <em>Number</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.Number
	 * @see demo.scratch.impl.ScratchPackageImpl#getNumber()
	 * @generated
	 */
	int NUMBER = 37;

	/**
	 * The number of structural features of the '<em>Number</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_FEATURE_COUNT = OPERATOR_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.scratch.Binary <em>Binary</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.Binary
	 * @see demo.scratch.impl.ScratchPackageImpl#getBinary()
	 * @generated
	 */
	int BINARY = 38;

	/**
	 * The number of structural features of the '<em>Binary</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_FEATURE_COUNT = OPERATOR_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.NumberValueImpl <em>Number Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.NumberValueImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getNumberValue()
	 * @generated
	 */
	int NUMBER_VALUE = 39;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_VALUE__VALUE = NUMBER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Number Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_VALUE_FEATURE_COUNT = NUMBER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.NumberObjVarImpl <em>Number Obj Var</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.NumberObjVarImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getNumberObjVar()
	 * @generated
	 */
	int NUMBER_OBJ_VAR = 40;

	/**
	 * The feature id for the '<em><b>Obj</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_OBJ_VAR__OBJ = NUMBER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Var</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_OBJ_VAR__VAR = NUMBER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Number Obj Var</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_OBJ_VAR_FEATURE_COUNT = NUMBER_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.NumberVarImpl <em>Number Var</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.NumberVarImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getNumberVar()
	 * @generated
	 */
	int NUMBER_VAR = 41;

	/**
	 * The feature id for the '<em><b>Var</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_VAR__VAR = NUMBER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Number Var</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_VAR_FEATURE_COUNT = NUMBER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.NumberVarXImpl <em>Number Var X</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.NumberVarXImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getNumberVarX()
	 * @generated
	 */
	int NUMBER_VAR_X = 42;

	/**
	 * The number of structural features of the '<em>Number Var X</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_VAR_X_FEATURE_COUNT = NUMBER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.NumberVarYImpl <em>Number Var Y</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.NumberVarYImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getNumberVarY()
	 * @generated
	 */
	int NUMBER_VAR_Y = 43;

	/**
	 * The number of structural features of the '<em>Number Var Y</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_VAR_Y_FEATURE_COUNT = NUMBER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.NumberVarDImpl <em>Number Var D</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.NumberVarDImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getNumberVarD()
	 * @generated
	 */
	int NUMBER_VAR_D = 44;

	/**
	 * The number of structural features of the '<em>Number Var D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NUMBER_VAR_D_FEATURE_COUNT = NUMBER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.BinaryVarImpl <em>Binary Var</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.BinaryVarImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getBinaryVar()
	 * @generated
	 */
	int BINARY_VAR = 45;

	/**
	 * The feature id for the '<em><b>Var</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_VAR__VAR = BINARY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Binary Var</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_VAR_FEATURE_COUNT = BINARY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.LtImpl <em>Lt</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.LtImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getLt()
	 * @generated
	 */
	int LT = 46;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LT__LEFT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LT__RIGHT = BINARY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Lt</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LT_FEATURE_COUNT = BINARY_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.GtImpl <em>Gt</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.GtImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getGt()
	 * @generated
	 */
	int GT = 47;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GT__LEFT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GT__RIGHT = BINARY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Gt</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GT_FEATURE_COUNT = BINARY_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.EqImpl <em>Eq</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.EqImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getEq()
	 * @generated
	 */
	int EQ = 48;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQ__LEFT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQ__RIGHT = BINARY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Eq</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EQ_FEATURE_COUNT = BINARY_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.AndImpl <em>And</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.AndImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getAnd()
	 * @generated
	 */
	int AND = 49;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND__LEFT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND__RIGHT = BINARY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>And</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_FEATURE_COUNT = BINARY_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.OrImpl <em>Or</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.OrImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getOr()
	 * @generated
	 */
	int OR = 50;

	/**
	 * The feature id for the '<em><b>Left</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR__LEFT = BINARY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR__RIGHT = BINARY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Or</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_FEATURE_COUNT = BINARY_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.NotImpl <em>Not</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.NotImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getNot()
	 * @generated
	 */
	int NOT = 51;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT__VALUE = BINARY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Not</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOT_FEATURE_COUNT = BINARY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.TouchsColorImpl <em>Touchs Color</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.TouchsColorImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getTouchsColor()
	 * @generated
	 */
	int TOUCHS_COLOR = 52;

	/**
	 * The feature id for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOUCHS_COLOR__COLOR = BINARY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Touchs Color</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOUCHS_COLOR_FEATURE_COUNT = BINARY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.Looks <em>Looks</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.Looks
	 * @see demo.scratch.impl.ScratchPackageImpl#getLooks()
	 * @generated
	 */
	int LOOKS = 53;

	/**
	 * The number of structural features of the '<em>Looks</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOKS_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.HideImpl <em>Hide</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.HideImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getHide()
	 * @generated
	 */
	int HIDE = 54;

	/**
	 * The number of structural features of the '<em>Hide</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HIDE_FEATURE_COUNT = LOOKS_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.ShowImpl <em>Show</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.ShowImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getShow()
	 * @generated
	 */
	int SHOW = 55;

	/**
	 * The number of structural features of the '<em>Show</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHOW_FEATURE_COUNT = LOOKS_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.SayImpl <em>Say</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.SayImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getSay()
	 * @generated
	 */
	int SAY = 56;

	/**
	 * The feature id for the '<em><b>Sentence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAY__SENTENCE = LOOKS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAY__DELAY = LOOKS_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Say</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAY_FEATURE_COUNT = LOOKS_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link demo.scratch.Sound <em>Sound</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.Sound
	 * @see demo.scratch.impl.ScratchPackageImpl#getSound()
	 * @generated
	 */
	int SOUND = 57;

	/**
	 * The number of structural features of the '<em>Sound</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOUND_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.scratch.impl.PlayImpl <em>Play</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.impl.PlayImpl
	 * @see demo.scratch.impl.ScratchPackageImpl#getPlay()
	 * @generated
	 */
	int PLAY = 58;

	/**
	 * The feature id for the '<em><b>Sound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAY__SOUND = SOUND_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Play</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAY_FEATURE_COUNT = SOUND_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.scratch.ObjectPredefEnum <em>Object Predef Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.ObjectPredefEnum
	 * @see demo.scratch.impl.ScratchPackageImpl#getObjectPredefEnum()
	 * @generated
	 */
	int OBJECT_PREDEF_ENUM = 59;

	/**
	 * The meta object id for the '{@link demo.scratch.ObjectVariableEnum <em>Object Variable Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.scratch.ObjectVariableEnum
	 * @see demo.scratch.impl.ScratchPackageImpl#getObjectVariableEnum()
	 * @generated
	 */
	int OBJECT_VARIABLE_ENUM = 60;


	/**
	 * Returns the meta object for class '{@link demo.scratch.Project <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Project</em>'.
	 * @see demo.scratch.Project
	 * @generated
	 */
	EClass getProject();

	/**
	 * Returns the meta object for the attribute '{@link demo.scratch.Project#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see demo.scratch.Project#getName()
	 * @see #getProject()
	 * @generated
	 */
	EAttribute getProject_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link demo.scratch.Project#getObjects <em>Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Objects</em>'.
	 * @see demo.scratch.Project#getObjects()
	 * @see #getProject()
	 * @generated
	 */
	EReference getProject_Objects();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Object <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object</em>'.
	 * @see demo.scratch.Object
	 * @generated
	 */
	EClass getObject();

	/**
	 * Returns the meta object for the attribute '{@link demo.scratch.Object#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see demo.scratch.Object#getName()
	 * @see #getObject()
	 * @generated
	 */
	EAttribute getObject_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link demo.scratch.Object#getScripts <em>Scripts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Scripts</em>'.
	 * @see demo.scratch.Object#getScripts()
	 * @see #getObject()
	 * @generated
	 */
	EReference getObject_Scripts();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Script <em>Script</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Script</em>'.
	 * @see demo.scratch.Script
	 * @generated
	 */
	EClass getScript();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Script#getWhen <em>When</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>When</em>'.
	 * @see demo.scratch.Script#getWhen()
	 * @see #getScript()
	 * @generated
	 */
	EReference getScript_When();

	/**
	 * Returns the meta object for the containment reference list '{@link demo.scratch.Script#getBlocks <em>Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Blocks</em>'.
	 * @see demo.scratch.Script#getBlocks()
	 * @see #getScript()
	 * @generated
	 */
	EReference getScript_Blocks();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Block <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block</em>'.
	 * @see demo.scratch.Block
	 * @generated
	 */
	EClass getBlock();

	/**
	 * Returns the meta object for class '{@link demo.scratch.IObject <em>IObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IObject</em>'.
	 * @see demo.scratch.IObject
	 * @generated
	 */
	EClass getIObject();

	/**
	 * Returns the meta object for class '{@link demo.scratch.ObjectRef <em>Object Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Ref</em>'.
	 * @see demo.scratch.ObjectRef
	 * @generated
	 */
	EClass getObjectRef();

	/**
	 * Returns the meta object for the reference '{@link demo.scratch.ObjectRef#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object</em>'.
	 * @see demo.scratch.ObjectRef#getObject()
	 * @see #getObjectRef()
	 * @generated
	 */
	EReference getObjectRef_Object();

	/**
	 * Returns the meta object for class '{@link demo.scratch.ObjectPredef <em>Object Predef</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Predef</em>'.
	 * @see demo.scratch.ObjectPredef
	 * @generated
	 */
	EClass getObjectPredef();

	/**
	 * Returns the meta object for the attribute '{@link demo.scratch.ObjectPredef#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Object</em>'.
	 * @see demo.scratch.ObjectPredef#getObject()
	 * @see #getObjectPredef()
	 * @generated
	 */
	EAttribute getObjectPredef_Object();

	/**
	 * Returns the meta object for class '{@link demo.scratch.When <em>When</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>When</em>'.
	 * @see demo.scratch.When
	 * @generated
	 */
	EClass getWhen();

	/**
	 * Returns the meta object for class '{@link demo.scratch.WhenFlag <em>When Flag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>When Flag</em>'.
	 * @see demo.scratch.WhenFlag
	 * @generated
	 */
	EClass getWhenFlag();

	/**
	 * Returns the meta object for class '{@link demo.scratch.WhenMsg <em>When Msg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>When Msg</em>'.
	 * @see demo.scratch.WhenMsg
	 * @generated
	 */
	EClass getWhenMsg();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.WhenMsg#getMsg <em>Msg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Msg</em>'.
	 * @see demo.scratch.WhenMsg#getMsg()
	 * @see #getWhenMsg()
	 * @generated
	 */
	EReference getWhenMsg_Msg();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Message <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message</em>'.
	 * @see demo.scratch.Message
	 * @generated
	 */
	EClass getMessage();

	/**
	 * Returns the meta object for the attribute '{@link demo.scratch.Message#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see demo.scratch.Message#getName()
	 * @see #getMessage()
	 * @generated
	 */
	EAttribute getMessage_Name();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Motion <em>Motion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Motion</em>'.
	 * @see demo.scratch.Motion
	 * @generated
	 */
	EClass getMotion();

	/**
	 * Returns the meta object for class '{@link demo.scratch.IfEdge <em>If Edge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If Edge</em>'.
	 * @see demo.scratch.IfEdge
	 * @generated
	 */
	EClass getIfEdge();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Move <em>Move</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Move</em>'.
	 * @see demo.scratch.Move
	 * @generated
	 */
	EClass getMove();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Move#getSteps <em>Steps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Steps</em>'.
	 * @see demo.scratch.Move#getSteps()
	 * @see #getMove()
	 * @generated
	 */
	EReference getMove_Steps();

	/**
	 * Returns the meta object for class '{@link demo.scratch.TurnCW <em>Turn CW</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Turn CW</em>'.
	 * @see demo.scratch.TurnCW
	 * @generated
	 */
	EClass getTurnCW();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.TurnCW#getAngle <em>Angle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Angle</em>'.
	 * @see demo.scratch.TurnCW#getAngle()
	 * @see #getTurnCW()
	 * @generated
	 */
	EReference getTurnCW_Angle();

	/**
	 * Returns the meta object for class '{@link demo.scratch.TurnCCW <em>Turn CCW</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Turn CCW</em>'.
	 * @see demo.scratch.TurnCCW
	 * @generated
	 */
	EClass getTurnCCW();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.TurnCCW#getAngle <em>Angle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Angle</em>'.
	 * @see demo.scratch.TurnCCW#getAngle()
	 * @see #getTurnCCW()
	 * @generated
	 */
	EReference getTurnCCW_Angle();

	/**
	 * Returns the meta object for class '{@link demo.scratch.PointDirection <em>Point Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Point Direction</em>'.
	 * @see demo.scratch.PointDirection
	 * @generated
	 */
	EClass getPointDirection();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.PointDirection#getAngle <em>Angle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Angle</em>'.
	 * @see demo.scratch.PointDirection#getAngle()
	 * @see #getPointDirection()
	 * @generated
	 */
	EReference getPointDirection_Angle();

	/**
	 * Returns the meta object for class '{@link demo.scratch.PointTowards <em>Point Towards</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Point Towards</em>'.
	 * @see demo.scratch.PointTowards
	 * @generated
	 */
	EClass getPointTowards();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.PointTowards#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object</em>'.
	 * @see demo.scratch.PointTowards#getObject()
	 * @see #getPointTowards()
	 * @generated
	 */
	EReference getPointTowards_Object();

	/**
	 * Returns the meta object for class '{@link demo.scratch.ChangeX <em>Change X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Change X</em>'.
	 * @see demo.scratch.ChangeX
	 * @generated
	 */
	EClass getChangeX();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.ChangeX#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>X</em>'.
	 * @see demo.scratch.ChangeX#getX()
	 * @see #getChangeX()
	 * @generated
	 */
	EReference getChangeX_X();

	/**
	 * Returns the meta object for class '{@link demo.scratch.ChangeY <em>Change Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Change Y</em>'.
	 * @see demo.scratch.ChangeY
	 * @generated
	 */
	EClass getChangeY();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.ChangeY#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Y</em>'.
	 * @see demo.scratch.ChangeY#getY()
	 * @see #getChangeY()
	 * @generated
	 */
	EReference getChangeY_Y();

	/**
	 * Returns the meta object for class '{@link demo.scratch.SetX <em>Set X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Set X</em>'.
	 * @see demo.scratch.SetX
	 * @generated
	 */
	EClass getSetX();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.SetX#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>X</em>'.
	 * @see demo.scratch.SetX#getX()
	 * @see #getSetX()
	 * @generated
	 */
	EReference getSetX_X();

	/**
	 * Returns the meta object for class '{@link demo.scratch.SetY <em>Set Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Set Y</em>'.
	 * @see demo.scratch.SetY
	 * @generated
	 */
	EClass getSetY();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.SetY#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Y</em>'.
	 * @see demo.scratch.SetY#getY()
	 * @see #getSetY()
	 * @generated
	 */
	EReference getSetY_Y();

	/**
	 * Returns the meta object for class '{@link demo.scratch.GoToXY <em>Go To XY</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Go To XY</em>'.
	 * @see demo.scratch.GoToXY
	 * @generated
	 */
	EClass getGoToXY();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.GoToXY#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>X</em>'.
	 * @see demo.scratch.GoToXY#getX()
	 * @see #getGoToXY()
	 * @generated
	 */
	EReference getGoToXY_X();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.GoToXY#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Y</em>'.
	 * @see demo.scratch.GoToXY#getY()
	 * @see #getGoToXY()
	 * @generated
	 */
	EReference getGoToXY_Y();

	/**
	 * Returns the meta object for class '{@link demo.scratch.GoTo <em>Go To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Go To</em>'.
	 * @see demo.scratch.GoTo
	 * @generated
	 */
	EClass getGoTo();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.GoTo#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Object</em>'.
	 * @see demo.scratch.GoTo#getObject()
	 * @see #getGoTo()
	 * @generated
	 */
	EReference getGoTo_Object();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Glide <em>Glide</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Glide</em>'.
	 * @see demo.scratch.Glide
	 * @generated
	 */
	EClass getGlide();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Glide#getX <em>X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>X</em>'.
	 * @see demo.scratch.Glide#getX()
	 * @see #getGlide()
	 * @generated
	 */
	EReference getGlide_X();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Glide#getY <em>Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Y</em>'.
	 * @see demo.scratch.Glide#getY()
	 * @see #getGlide()
	 * @generated
	 */
	EReference getGlide_Y();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Glide#getDuration <em>Duration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Duration</em>'.
	 * @see demo.scratch.Glide#getDuration()
	 * @see #getGlide()
	 * @generated
	 */
	EReference getGlide_Duration();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Control <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control</em>'.
	 * @see demo.scratch.Control
	 * @generated
	 */
	EClass getControl();

	/**
	 * Returns the meta object for class '{@link demo.scratch.If <em>If</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If</em>'.
	 * @see demo.scratch.If
	 * @generated
	 */
	EClass getIf();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.If#getCond <em>Cond</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cond</em>'.
	 * @see demo.scratch.If#getCond()
	 * @see #getIf()
	 * @generated
	 */
	EReference getIf_Cond();

	/**
	 * Returns the meta object for the containment reference list '{@link demo.scratch.If#getTrue <em>True</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>True</em>'.
	 * @see demo.scratch.If#getTrue()
	 * @see #getIf()
	 * @generated
	 */
	EReference getIf_True();

	/**
	 * Returns the meta object for the containment reference list '{@link demo.scratch.If#getFalse <em>False</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>False</em>'.
	 * @see demo.scratch.If#getFalse()
	 * @see #getIf()
	 * @generated
	 */
	EReference getIf_False();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Broadcast <em>Broadcast</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Broadcast</em>'.
	 * @see demo.scratch.Broadcast
	 * @generated
	 */
	EClass getBroadcast();

	/**
	 * Returns the meta object for the attribute '{@link demo.scratch.Broadcast#isWait <em>Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wait</em>'.
	 * @see demo.scratch.Broadcast#isWait()
	 * @see #getBroadcast()
	 * @generated
	 */
	EAttribute getBroadcast_Wait();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Broadcast#getMsg <em>Msg</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Msg</em>'.
	 * @see demo.scratch.Broadcast#getMsg()
	 * @see #getBroadcast()
	 * @generated
	 */
	EReference getBroadcast_Msg();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Stop <em>Stop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stop</em>'.
	 * @see demo.scratch.Stop
	 * @generated
	 */
	EClass getStop();

	/**
	 * Returns the meta object for the attribute '{@link demo.scratch.Stop#isAll <em>All</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>All</em>'.
	 * @see demo.scratch.Stop#isAll()
	 * @see #getStop()
	 * @generated
	 */
	EAttribute getStop_All();

	/**
	 * Returns the meta object for the attribute '{@link demo.scratch.Stop#isScript <em>Script</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Script</em>'.
	 * @see demo.scratch.Stop#isScript()
	 * @see #getStop()
	 * @generated
	 */
	EAttribute getStop_Script();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Wait <em>Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Wait</em>'.
	 * @see demo.scratch.Wait
	 * @generated
	 */
	EClass getWait();

	/**
	 * Returns the meta object for class '{@link demo.scratch.WaitDelay <em>Wait Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Wait Delay</em>'.
	 * @see demo.scratch.WaitDelay
	 * @generated
	 */
	EClass getWaitDelay();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.WaitDelay#getDelay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Delay</em>'.
	 * @see demo.scratch.WaitDelay#getDelay()
	 * @see #getWaitDelay()
	 * @generated
	 */
	EReference getWaitDelay_Delay();

	/**
	 * Returns the meta object for class '{@link demo.scratch.WaitUntil <em>Wait Until</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Wait Until</em>'.
	 * @see demo.scratch.WaitUntil
	 * @generated
	 */
	EClass getWaitUntil();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.WaitUntil#getCond <em>Cond</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cond</em>'.
	 * @see demo.scratch.WaitUntil#getCond()
	 * @see #getWaitUntil()
	 * @generated
	 */
	EReference getWaitUntil_Cond();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Loop <em>Loop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Loop</em>'.
	 * @see demo.scratch.Loop
	 * @generated
	 */
	EClass getLoop();

	/**
	 * Returns the meta object for the containment reference list '{@link demo.scratch.Loop#getBlocks <em>Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Blocks</em>'.
	 * @see demo.scratch.Loop#getBlocks()
	 * @see #getLoop()
	 * @generated
	 */
	EReference getLoop_Blocks();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Forever <em>Forever</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Forever</em>'.
	 * @see demo.scratch.Forever
	 * @generated
	 */
	EClass getForever();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Forever#getCond <em>Cond</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cond</em>'.
	 * @see demo.scratch.Forever#getCond()
	 * @see #getForever()
	 * @generated
	 */
	EReference getForever_Cond();

	/**
	 * Returns the meta object for class '{@link demo.scratch.RepeatN <em>Repeat N</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Repeat N</em>'.
	 * @see demo.scratch.RepeatN
	 * @generated
	 */
	EClass getRepeatN();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.RepeatN#getN <em>N</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>N</em>'.
	 * @see demo.scratch.RepeatN#getN()
	 * @see #getRepeatN()
	 * @generated
	 */
	EReference getRepeatN_N();

	/**
	 * Returns the meta object for class '{@link demo.scratch.RepeatUntil <em>Repeat Until</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Repeat Until</em>'.
	 * @see demo.scratch.RepeatUntil
	 * @generated
	 */
	EClass getRepeatUntil();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.RepeatUntil#getCond <em>Cond</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cond</em>'.
	 * @see demo.scratch.RepeatUntil#getCond()
	 * @see #getRepeatUntil()
	 * @generated
	 */
	EReference getRepeatUntil_Cond();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Operator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operator</em>'.
	 * @see demo.scratch.Operator
	 * @generated
	 */
	EClass getOperator();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Number <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Number</em>'.
	 * @see demo.scratch.Number
	 * @generated
	 */
	EClass getNumber();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Binary <em>Binary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary</em>'.
	 * @see demo.scratch.Binary
	 * @generated
	 */
	EClass getBinary();

	/**
	 * Returns the meta object for class '{@link demo.scratch.NumberValue <em>Number Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Number Value</em>'.
	 * @see demo.scratch.NumberValue
	 * @generated
	 */
	EClass getNumberValue();

	/**
	 * Returns the meta object for the attribute '{@link demo.scratch.NumberValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see demo.scratch.NumberValue#getValue()
	 * @see #getNumberValue()
	 * @generated
	 */
	EAttribute getNumberValue_Value();

	/**
	 * Returns the meta object for class '{@link demo.scratch.NumberObjVar <em>Number Obj Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Number Obj Var</em>'.
	 * @see demo.scratch.NumberObjVar
	 * @generated
	 */
	EClass getNumberObjVar();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.NumberObjVar#getObj <em>Obj</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Obj</em>'.
	 * @see demo.scratch.NumberObjVar#getObj()
	 * @see #getNumberObjVar()
	 * @generated
	 */
	EReference getNumberObjVar_Obj();

	/**
	 * Returns the meta object for the attribute '{@link demo.scratch.NumberObjVar#getVar <em>Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var</em>'.
	 * @see demo.scratch.NumberObjVar#getVar()
	 * @see #getNumberObjVar()
	 * @generated
	 */
	EAttribute getNumberObjVar_Var();

	/**
	 * Returns the meta object for class '{@link demo.scratch.NumberVar <em>Number Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Number Var</em>'.
	 * @see demo.scratch.NumberVar
	 * @generated
	 */
	EClass getNumberVar();

	/**
	 * Returns the meta object for the attribute '{@link demo.scratch.NumberVar#getVar <em>Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var</em>'.
	 * @see demo.scratch.NumberVar#getVar()
	 * @see #getNumberVar()
	 * @generated
	 */
	EAttribute getNumberVar_Var();

	/**
	 * Returns the meta object for class '{@link demo.scratch.NumberVarX <em>Number Var X</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Number Var X</em>'.
	 * @see demo.scratch.NumberVarX
	 * @generated
	 */
	EClass getNumberVarX();

	/**
	 * Returns the meta object for class '{@link demo.scratch.NumberVarY <em>Number Var Y</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Number Var Y</em>'.
	 * @see demo.scratch.NumberVarY
	 * @generated
	 */
	EClass getNumberVarY();

	/**
	 * Returns the meta object for class '{@link demo.scratch.NumberVarD <em>Number Var D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Number Var D</em>'.
	 * @see demo.scratch.NumberVarD
	 * @generated
	 */
	EClass getNumberVarD();

	/**
	 * Returns the meta object for class '{@link demo.scratch.BinaryVar <em>Binary Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Var</em>'.
	 * @see demo.scratch.BinaryVar
	 * @generated
	 */
	EClass getBinaryVar();

	/**
	 * Returns the meta object for the attribute '{@link demo.scratch.BinaryVar#getVar <em>Var</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var</em>'.
	 * @see demo.scratch.BinaryVar#getVar()
	 * @see #getBinaryVar()
	 * @generated
	 */
	EAttribute getBinaryVar_Var();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Lt <em>Lt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lt</em>'.
	 * @see demo.scratch.Lt
	 * @generated
	 */
	EClass getLt();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Lt#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see demo.scratch.Lt#getLeft()
	 * @see #getLt()
	 * @generated
	 */
	EReference getLt_Left();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Lt#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see demo.scratch.Lt#getRight()
	 * @see #getLt()
	 * @generated
	 */
	EReference getLt_Right();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Gt <em>Gt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gt</em>'.
	 * @see demo.scratch.Gt
	 * @generated
	 */
	EClass getGt();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Gt#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see demo.scratch.Gt#getLeft()
	 * @see #getGt()
	 * @generated
	 */
	EReference getGt_Left();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Gt#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see demo.scratch.Gt#getRight()
	 * @see #getGt()
	 * @generated
	 */
	EReference getGt_Right();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Eq <em>Eq</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Eq</em>'.
	 * @see demo.scratch.Eq
	 * @generated
	 */
	EClass getEq();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Eq#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see demo.scratch.Eq#getLeft()
	 * @see #getEq()
	 * @generated
	 */
	EReference getEq_Left();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Eq#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see demo.scratch.Eq#getRight()
	 * @see #getEq()
	 * @generated
	 */
	EReference getEq_Right();

	/**
	 * Returns the meta object for class '{@link demo.scratch.And <em>And</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>And</em>'.
	 * @see demo.scratch.And
	 * @generated
	 */
	EClass getAnd();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.And#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see demo.scratch.And#getLeft()
	 * @see #getAnd()
	 * @generated
	 */
	EReference getAnd_Left();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.And#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see demo.scratch.And#getRight()
	 * @see #getAnd()
	 * @generated
	 */
	EReference getAnd_Right();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Or <em>Or</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Or</em>'.
	 * @see demo.scratch.Or
	 * @generated
	 */
	EClass getOr();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Or#getLeft <em>Left</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left</em>'.
	 * @see demo.scratch.Or#getLeft()
	 * @see #getOr()
	 * @generated
	 */
	EReference getOr_Left();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Or#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see demo.scratch.Or#getRight()
	 * @see #getOr()
	 * @generated
	 */
	EReference getOr_Right();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Not <em>Not</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Not</em>'.
	 * @see demo.scratch.Not
	 * @generated
	 */
	EClass getNot();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Not#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see demo.scratch.Not#getValue()
	 * @see #getNot()
	 * @generated
	 */
	EReference getNot_Value();

	/**
	 * Returns the meta object for class '{@link demo.scratch.TouchsColor <em>Touchs Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Touchs Color</em>'.
	 * @see demo.scratch.TouchsColor
	 * @generated
	 */
	EClass getTouchsColor();

	/**
	 * Returns the meta object for the attribute '{@link demo.scratch.TouchsColor#getColor <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Color</em>'.
	 * @see demo.scratch.TouchsColor#getColor()
	 * @see #getTouchsColor()
	 * @generated
	 */
	EAttribute getTouchsColor_Color();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Looks <em>Looks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Looks</em>'.
	 * @see demo.scratch.Looks
	 * @generated
	 */
	EClass getLooks();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Hide <em>Hide</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hide</em>'.
	 * @see demo.scratch.Hide
	 * @generated
	 */
	EClass getHide();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Show <em>Show</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Show</em>'.
	 * @see demo.scratch.Show
	 * @generated
	 */
	EClass getShow();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Say <em>Say</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Say</em>'.
	 * @see demo.scratch.Say
	 * @generated
	 */
	EClass getSay();

	/**
	 * Returns the meta object for the attribute '{@link demo.scratch.Say#getSentence <em>Sentence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sentence</em>'.
	 * @see demo.scratch.Say#getSentence()
	 * @see #getSay()
	 * @generated
	 */
	EAttribute getSay_Sentence();

	/**
	 * Returns the meta object for the containment reference '{@link demo.scratch.Say#getDelay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Delay</em>'.
	 * @see demo.scratch.Say#getDelay()
	 * @see #getSay()
	 * @generated
	 */
	EReference getSay_Delay();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Sound <em>Sound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sound</em>'.
	 * @see demo.scratch.Sound
	 * @generated
	 */
	EClass getSound();

	/**
	 * Returns the meta object for class '{@link demo.scratch.Play <em>Play</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Play</em>'.
	 * @see demo.scratch.Play
	 * @generated
	 */
	EClass getPlay();

	/**
	 * Returns the meta object for the attribute '{@link demo.scratch.Play#getSound <em>Sound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sound</em>'.
	 * @see demo.scratch.Play#getSound()
	 * @see #getPlay()
	 * @generated
	 */
	EAttribute getPlay_Sound();

	/**
	 * Returns the meta object for enum '{@link demo.scratch.ObjectPredefEnum <em>Object Predef Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Object Predef Enum</em>'.
	 * @see demo.scratch.ObjectPredefEnum
	 * @generated
	 */
	EEnum getObjectPredefEnum();

	/**
	 * Returns the meta object for enum '{@link demo.scratch.ObjectVariableEnum <em>Object Variable Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Object Variable Enum</em>'.
	 * @see demo.scratch.ObjectVariableEnum
	 * @generated
	 */
	EEnum getObjectVariableEnum();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ScratchFactory getScratchFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals
	{
		/**
		 * The meta object literal for the '{@link demo.scratch.impl.ProjectImpl <em>Project</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.ProjectImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getProject()
		 * @generated
		 */
		EClass PROJECT = eINSTANCE.getProject();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROJECT__NAME = eINSTANCE.getProject_Name();

		/**
		 * The meta object literal for the '<em><b>Objects</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROJECT__OBJECTS = eINSTANCE.getProject_Objects();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.ObjectImpl <em>Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.ObjectImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getObject()
		 * @generated
		 */
		EClass OBJECT = eINSTANCE.getObject();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT__NAME = eINSTANCE.getObject_Name();

		/**
		 * The meta object literal for the '<em><b>Scripts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT__SCRIPTS = eINSTANCE.getObject_Scripts();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.ScriptImpl <em>Script</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.ScriptImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getScript()
		 * @generated
		 */
		EClass SCRIPT = eINSTANCE.getScript();

		/**
		 * The meta object literal for the '<em><b>When</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCRIPT__WHEN = eINSTANCE.getScript_When();

		/**
		 * The meta object literal for the '<em><b>Blocks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCRIPT__BLOCKS = eINSTANCE.getScript_Blocks();

		/**
		 * The meta object literal for the '{@link demo.scratch.Block <em>Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.Block
		 * @see demo.scratch.impl.ScratchPackageImpl#getBlock()
		 * @generated
		 */
		EClass BLOCK = eINSTANCE.getBlock();

		/**
		 * The meta object literal for the '{@link demo.scratch.IObject <em>IObject</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.IObject
		 * @see demo.scratch.impl.ScratchPackageImpl#getIObject()
		 * @generated
		 */
		EClass IOBJECT = eINSTANCE.getIObject();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.ObjectRefImpl <em>Object Ref</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.ObjectRefImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getObjectRef()
		 * @generated
		 */
		EClass OBJECT_REF = eINSTANCE.getObjectRef();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_REF__OBJECT = eINSTANCE.getObjectRef_Object();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.ObjectPredefImpl <em>Object Predef</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.ObjectPredefImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getObjectPredef()
		 * @generated
		 */
		EClass OBJECT_PREDEF = eINSTANCE.getObjectPredef();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBJECT_PREDEF__OBJECT = eINSTANCE.getObjectPredef_Object();

		/**
		 * The meta object literal for the '{@link demo.scratch.When <em>When</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.When
		 * @see demo.scratch.impl.ScratchPackageImpl#getWhen()
		 * @generated
		 */
		EClass WHEN = eINSTANCE.getWhen();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.WhenFlagImpl <em>When Flag</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.WhenFlagImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getWhenFlag()
		 * @generated
		 */
		EClass WHEN_FLAG = eINSTANCE.getWhenFlag();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.WhenMsgImpl <em>When Msg</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.WhenMsgImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getWhenMsg()
		 * @generated
		 */
		EClass WHEN_MSG = eINSTANCE.getWhenMsg();

		/**
		 * The meta object literal for the '<em><b>Msg</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHEN_MSG__MSG = eINSTANCE.getWhenMsg_Msg();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.MessageImpl <em>Message</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.MessageImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getMessage()
		 * @generated
		 */
		EClass MESSAGE = eINSTANCE.getMessage();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE__NAME = eINSTANCE.getMessage_Name();

		/**
		 * The meta object literal for the '{@link demo.scratch.Motion <em>Motion</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.Motion
		 * @see demo.scratch.impl.ScratchPackageImpl#getMotion()
		 * @generated
		 */
		EClass MOTION = eINSTANCE.getMotion();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.IfEdgeImpl <em>If Edge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.IfEdgeImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getIfEdge()
		 * @generated
		 */
		EClass IF_EDGE = eINSTANCE.getIfEdge();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.MoveImpl <em>Move</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.MoveImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getMove()
		 * @generated
		 */
		EClass MOVE = eINSTANCE.getMove();

		/**
		 * The meta object literal for the '<em><b>Steps</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOVE__STEPS = eINSTANCE.getMove_Steps();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.TurnCWImpl <em>Turn CW</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.TurnCWImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getTurnCW()
		 * @generated
		 */
		EClass TURN_CW = eINSTANCE.getTurnCW();

		/**
		 * The meta object literal for the '<em><b>Angle</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TURN_CW__ANGLE = eINSTANCE.getTurnCW_Angle();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.TurnCCWImpl <em>Turn CCW</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.TurnCCWImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getTurnCCW()
		 * @generated
		 */
		EClass TURN_CCW = eINSTANCE.getTurnCCW();

		/**
		 * The meta object literal for the '<em><b>Angle</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TURN_CCW__ANGLE = eINSTANCE.getTurnCCW_Angle();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.PointDirectionImpl <em>Point Direction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.PointDirectionImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getPointDirection()
		 * @generated
		 */
		EClass POINT_DIRECTION = eINSTANCE.getPointDirection();

		/**
		 * The meta object literal for the '<em><b>Angle</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POINT_DIRECTION__ANGLE = eINSTANCE.getPointDirection_Angle();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.PointTowardsImpl <em>Point Towards</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.PointTowardsImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getPointTowards()
		 * @generated
		 */
		EClass POINT_TOWARDS = eINSTANCE.getPointTowards();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POINT_TOWARDS__OBJECT = eINSTANCE.getPointTowards_Object();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.ChangeXImpl <em>Change X</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.ChangeXImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getChangeX()
		 * @generated
		 */
		EClass CHANGE_X = eINSTANCE.getChangeX();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHANGE_X__X = eINSTANCE.getChangeX_X();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.ChangeYImpl <em>Change Y</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.ChangeYImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getChangeY()
		 * @generated
		 */
		EClass CHANGE_Y = eINSTANCE.getChangeY();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHANGE_Y__Y = eINSTANCE.getChangeY_Y();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.SetXImpl <em>Set X</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.SetXImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getSetX()
		 * @generated
		 */
		EClass SET_X = eINSTANCE.getSetX();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SET_X__X = eINSTANCE.getSetX_X();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.SetYImpl <em>Set Y</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.SetYImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getSetY()
		 * @generated
		 */
		EClass SET_Y = eINSTANCE.getSetY();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SET_Y__Y = eINSTANCE.getSetY_Y();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.GoToXYImpl <em>Go To XY</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.GoToXYImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getGoToXY()
		 * @generated
		 */
		EClass GO_TO_XY = eINSTANCE.getGoToXY();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GO_TO_XY__X = eINSTANCE.getGoToXY_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GO_TO_XY__Y = eINSTANCE.getGoToXY_Y();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.GoToImpl <em>Go To</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.GoToImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getGoTo()
		 * @generated
		 */
		EClass GO_TO = eINSTANCE.getGoTo();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GO_TO__OBJECT = eINSTANCE.getGoTo_Object();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.GlideImpl <em>Glide</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.GlideImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getGlide()
		 * @generated
		 */
		EClass GLIDE = eINSTANCE.getGlide();

		/**
		 * The meta object literal for the '<em><b>X</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GLIDE__X = eINSTANCE.getGlide_X();

		/**
		 * The meta object literal for the '<em><b>Y</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GLIDE__Y = eINSTANCE.getGlide_Y();

		/**
		 * The meta object literal for the '<em><b>Duration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GLIDE__DURATION = eINSTANCE.getGlide_Duration();

		/**
		 * The meta object literal for the '{@link demo.scratch.Control <em>Control</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.Control
		 * @see demo.scratch.impl.ScratchPackageImpl#getControl()
		 * @generated
		 */
		EClass CONTROL = eINSTANCE.getControl();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.IfImpl <em>If</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.IfImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getIf()
		 * @generated
		 */
		EClass IF = eINSTANCE.getIf();

		/**
		 * The meta object literal for the '<em><b>Cond</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF__COND = eINSTANCE.getIf_Cond();

		/**
		 * The meta object literal for the '<em><b>True</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF__TRUE = eINSTANCE.getIf_True();

		/**
		 * The meta object literal for the '<em><b>False</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF__FALSE = eINSTANCE.getIf_False();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.BroadcastImpl <em>Broadcast</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.BroadcastImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getBroadcast()
		 * @generated
		 */
		EClass BROADCAST = eINSTANCE.getBroadcast();

		/**
		 * The meta object literal for the '<em><b>Wait</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BROADCAST__WAIT = eINSTANCE.getBroadcast_Wait();

		/**
		 * The meta object literal for the '<em><b>Msg</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BROADCAST__MSG = eINSTANCE.getBroadcast_Msg();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.StopImpl <em>Stop</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.StopImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getStop()
		 * @generated
		 */
		EClass STOP = eINSTANCE.getStop();

		/**
		 * The meta object literal for the '<em><b>All</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STOP__ALL = eINSTANCE.getStop_All();

		/**
		 * The meta object literal for the '<em><b>Script</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STOP__SCRIPT = eINSTANCE.getStop_Script();

		/**
		 * The meta object literal for the '{@link demo.scratch.Wait <em>Wait</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.Wait
		 * @see demo.scratch.impl.ScratchPackageImpl#getWait()
		 * @generated
		 */
		EClass WAIT = eINSTANCE.getWait();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.WaitDelayImpl <em>Wait Delay</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.WaitDelayImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getWaitDelay()
		 * @generated
		 */
		EClass WAIT_DELAY = eINSTANCE.getWaitDelay();

		/**
		 * The meta object literal for the '<em><b>Delay</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WAIT_DELAY__DELAY = eINSTANCE.getWaitDelay_Delay();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.WaitUntilImpl <em>Wait Until</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.WaitUntilImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getWaitUntil()
		 * @generated
		 */
		EClass WAIT_UNTIL = eINSTANCE.getWaitUntil();

		/**
		 * The meta object literal for the '<em><b>Cond</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WAIT_UNTIL__COND = eINSTANCE.getWaitUntil_Cond();

		/**
		 * The meta object literal for the '{@link demo.scratch.Loop <em>Loop</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.Loop
		 * @see demo.scratch.impl.ScratchPackageImpl#getLoop()
		 * @generated
		 */
		EClass LOOP = eINSTANCE.getLoop();

		/**
		 * The meta object literal for the '<em><b>Blocks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOOP__BLOCKS = eINSTANCE.getLoop_Blocks();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.ForeverImpl <em>Forever</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.ForeverImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getForever()
		 * @generated
		 */
		EClass FOREVER = eINSTANCE.getForever();

		/**
		 * The meta object literal for the '<em><b>Cond</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOREVER__COND = eINSTANCE.getForever_Cond();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.RepeatNImpl <em>Repeat N</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.RepeatNImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getRepeatN()
		 * @generated
		 */
		EClass REPEAT_N = eINSTANCE.getRepeatN();

		/**
		 * The meta object literal for the '<em><b>N</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_N__N = eINSTANCE.getRepeatN_N();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.RepeatUntilImpl <em>Repeat Until</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.RepeatUntilImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getRepeatUntil()
		 * @generated
		 */
		EClass REPEAT_UNTIL = eINSTANCE.getRepeatUntil();

		/**
		 * The meta object literal for the '<em><b>Cond</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPEAT_UNTIL__COND = eINSTANCE.getRepeatUntil_Cond();

		/**
		 * The meta object literal for the '{@link demo.scratch.Operator <em>Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.Operator
		 * @see demo.scratch.impl.ScratchPackageImpl#getOperator()
		 * @generated
		 */
		EClass OPERATOR = eINSTANCE.getOperator();

		/**
		 * The meta object literal for the '{@link demo.scratch.Number <em>Number</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.Number
		 * @see demo.scratch.impl.ScratchPackageImpl#getNumber()
		 * @generated
		 */
		EClass NUMBER = eINSTANCE.getNumber();

		/**
		 * The meta object literal for the '{@link demo.scratch.Binary <em>Binary</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.Binary
		 * @see demo.scratch.impl.ScratchPackageImpl#getBinary()
		 * @generated
		 */
		EClass BINARY = eINSTANCE.getBinary();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.NumberValueImpl <em>Number Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.NumberValueImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getNumberValue()
		 * @generated
		 */
		EClass NUMBER_VALUE = eINSTANCE.getNumberValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NUMBER_VALUE__VALUE = eINSTANCE.getNumberValue_Value();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.NumberObjVarImpl <em>Number Obj Var</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.NumberObjVarImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getNumberObjVar()
		 * @generated
		 */
		EClass NUMBER_OBJ_VAR = eINSTANCE.getNumberObjVar();

		/**
		 * The meta object literal for the '<em><b>Obj</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NUMBER_OBJ_VAR__OBJ = eINSTANCE.getNumberObjVar_Obj();

		/**
		 * The meta object literal for the '<em><b>Var</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NUMBER_OBJ_VAR__VAR = eINSTANCE.getNumberObjVar_Var();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.NumberVarImpl <em>Number Var</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.NumberVarImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getNumberVar()
		 * @generated
		 */
		EClass NUMBER_VAR = eINSTANCE.getNumberVar();

		/**
		 * The meta object literal for the '<em><b>Var</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NUMBER_VAR__VAR = eINSTANCE.getNumberVar_Var();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.NumberVarXImpl <em>Number Var X</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.NumberVarXImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getNumberVarX()
		 * @generated
		 */
		EClass NUMBER_VAR_X = eINSTANCE.getNumberVarX();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.NumberVarYImpl <em>Number Var Y</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.NumberVarYImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getNumberVarY()
		 * @generated
		 */
		EClass NUMBER_VAR_Y = eINSTANCE.getNumberVarY();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.NumberVarDImpl <em>Number Var D</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.NumberVarDImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getNumberVarD()
		 * @generated
		 */
		EClass NUMBER_VAR_D = eINSTANCE.getNumberVarD();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.BinaryVarImpl <em>Binary Var</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.BinaryVarImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getBinaryVar()
		 * @generated
		 */
		EClass BINARY_VAR = eINSTANCE.getBinaryVar();

		/**
		 * The meta object literal for the '<em><b>Var</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_VAR__VAR = eINSTANCE.getBinaryVar_Var();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.LtImpl <em>Lt</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.LtImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getLt()
		 * @generated
		 */
		EClass LT = eINSTANCE.getLt();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LT__LEFT = eINSTANCE.getLt_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LT__RIGHT = eINSTANCE.getLt_Right();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.GtImpl <em>Gt</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.GtImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getGt()
		 * @generated
		 */
		EClass GT = eINSTANCE.getGt();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GT__LEFT = eINSTANCE.getGt_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GT__RIGHT = eINSTANCE.getGt_Right();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.EqImpl <em>Eq</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.EqImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getEq()
		 * @generated
		 */
		EClass EQ = eINSTANCE.getEq();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQ__LEFT = eINSTANCE.getEq_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EQ__RIGHT = eINSTANCE.getEq_Right();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.AndImpl <em>And</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.AndImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getAnd()
		 * @generated
		 */
		EClass AND = eINSTANCE.getAnd();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AND__LEFT = eINSTANCE.getAnd_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AND__RIGHT = eINSTANCE.getAnd_Right();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.OrImpl <em>Or</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.OrImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getOr()
		 * @generated
		 */
		EClass OR = eINSTANCE.getOr();

		/**
		 * The meta object literal for the '<em><b>Left</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OR__LEFT = eINSTANCE.getOr_Left();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OR__RIGHT = eINSTANCE.getOr_Right();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.NotImpl <em>Not</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.NotImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getNot()
		 * @generated
		 */
		EClass NOT = eINSTANCE.getNot();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NOT__VALUE = eINSTANCE.getNot_Value();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.TouchsColorImpl <em>Touchs Color</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.TouchsColorImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getTouchsColor()
		 * @generated
		 */
		EClass TOUCHS_COLOR = eINSTANCE.getTouchsColor();

		/**
		 * The meta object literal for the '<em><b>Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TOUCHS_COLOR__COLOR = eINSTANCE.getTouchsColor_Color();

		/**
		 * The meta object literal for the '{@link demo.scratch.Looks <em>Looks</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.Looks
		 * @see demo.scratch.impl.ScratchPackageImpl#getLooks()
		 * @generated
		 */
		EClass LOOKS = eINSTANCE.getLooks();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.HideImpl <em>Hide</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.HideImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getHide()
		 * @generated
		 */
		EClass HIDE = eINSTANCE.getHide();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.ShowImpl <em>Show</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.ShowImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getShow()
		 * @generated
		 */
		EClass SHOW = eINSTANCE.getShow();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.SayImpl <em>Say</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.SayImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getSay()
		 * @generated
		 */
		EClass SAY = eINSTANCE.getSay();

		/**
		 * The meta object literal for the '<em><b>Sentence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SAY__SENTENCE = eINSTANCE.getSay_Sentence();

		/**
		 * The meta object literal for the '<em><b>Delay</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SAY__DELAY = eINSTANCE.getSay_Delay();

		/**
		 * The meta object literal for the '{@link demo.scratch.Sound <em>Sound</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.Sound
		 * @see demo.scratch.impl.ScratchPackageImpl#getSound()
		 * @generated
		 */
		EClass SOUND = eINSTANCE.getSound();

		/**
		 * The meta object literal for the '{@link demo.scratch.impl.PlayImpl <em>Play</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.impl.PlayImpl
		 * @see demo.scratch.impl.ScratchPackageImpl#getPlay()
		 * @generated
		 */
		EClass PLAY = eINSTANCE.getPlay();

		/**
		 * The meta object literal for the '<em><b>Sound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAY__SOUND = eINSTANCE.getPlay_Sound();

		/**
		 * The meta object literal for the '{@link demo.scratch.ObjectPredefEnum <em>Object Predef Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.ObjectPredefEnum
		 * @see demo.scratch.impl.ScratchPackageImpl#getObjectPredefEnum()
		 * @generated
		 */
		EEnum OBJECT_PREDEF_ENUM = eINSTANCE.getObjectPredefEnum();

		/**
		 * The meta object literal for the '{@link demo.scratch.ObjectVariableEnum <em>Object Variable Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.scratch.ObjectVariableEnum
		 * @see demo.scratch.impl.ScratchPackageImpl#getObjectVariableEnum()
		 * @generated
		 */
		EEnum OBJECT_VARIABLE_ENUM = eINSTANCE.getObjectVariableEnum();

	}

} //ScratchPackage
