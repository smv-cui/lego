/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sound</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see demo.scratch.ScratchPackage#getSound()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Sound extends Block
{
} // Sound
