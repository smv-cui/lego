/**
 * <copyright>
 * </copyright>
 *
 */
package demo.scratch;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Looks</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see demo.scratch.ScratchPackage#getLooks()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Looks extends Block
{
} // Looks
