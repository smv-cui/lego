
package demo.scratch;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class ScratchStandaloneSetup extends ScratchStandaloneSetupGenerated{

	public static void doSetup() {
		new ScratchStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

