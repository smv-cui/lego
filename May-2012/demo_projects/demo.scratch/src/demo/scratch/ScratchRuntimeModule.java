package demo.scratch;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
public class ScratchRuntimeModule extends demo.scratch.AbstractScratchRuntimeModule {

	@Override
	public void configure(com.google.inject.Binder binder) {
		super.configure(binder);
	}

	@Override
	public Class<? extends org.eclipse.xtext.linking.ILinker> bindILinker() {
		return org.eclipse.xtext.linking.impl.Linker.class;
	}

	/**
	 * Bind the IStorage2UriMapper in order to open an xtext files in an editor.
	 */
	public Class<? extends org.eclipse.xtext.ui.resource.IStorage2UriMapper> bindIStorage2UriMapper() {
		return org.eclipse.xtext.ui.resource.Storage2UriMapperImpl.class;
	}

	/**
     * Bind the BuilderParticipant in order to invoke the generator during the build.
     */
	public Class<? extends org.eclipse.xtext.builder.IXtextBuilderParticipant> bindIXtextBuilderParticipant() {
		return org.eclipse.xtext.builder.BuilderParticipant.class;
	}

	public Class<? extends org.eclipse.xtext.generator.IGenerator> bindScratchGenerator() {
		return demo.scratch.generator.ScratchGenerator.class;
	}
}
