package demo.scratch.generator;

import demo.scratch.Script;

public interface IScratchGenerator {
	public CharSequence doGenerateLego(final Script script);
}