package demo.scratch.generator

import demo.scratch.Script
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator

class ScratchGenerator implements IGenerator, IScratchGenerator {

	override void doGenerate(Resource resource, IFileSystemAccess fsa) {
		var i = 0;
		for(e: resource.contents.filter(typeof(Script))) {
			i = i + 1;
			 fsa.generateFile(i.toString() + ".scratch", new Lego().generate(e))
    	}
	}

	override doGenerateLego(Script script) {
		new Lego().generate(script)
	}
}
