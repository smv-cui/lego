package demo.scratch.generator

import demo.scratch.Broadcast
import demo.scratch.Move
import demo.scratch.Script
import demo.scratch.TurnCCW
import demo.scratch.TurnCW
import demo.scratch.Block
import demo.scratch.NumberValue

class Lego {

def generate(Script script) '''
DEBUT
«FOR b : script.blocks»
«b.generate»
«ENDFOR»
FIN
'''

def dispatch generate(Broadcast block) {
	val String m = block.msg.name.toLowerCase;
	var String n;
	if (m.equals("close")) n = 'ferme la pince'
	else if (m.equals("open")) n = 'ouvre la pince'
	else throw new Exception("Not implemented yet !")
'''«n»'''
}

def dispatch generate(TurnCW block) {
	var int a = toValue(block.angle).intValue;
	val boolean b = a > 0;
	val boolean c = a == 90 || a == -90;
'''tourne à «if (b) 'droite' else 'gauche'»«IF !c» de «if(b) a else -a» degrés«ENDIF»'''
}

def dispatch generate(TurnCCW block) {
	var int a = toValue(block.angle).intValue;
	val boolean b = a > 0;
	val boolean c = a == 90 || a == -90;
'''tourne à «if (b) 'gauche' else 'droite'»«IF !c» de «if(b) a else -a» degrés«ENDIF»'''
}

def dispatch generate(Move block) {
	var int a = (10 * toValue(block.steps) / 25).intValue;
	val boolean b = a > 0;
'''«if (b) 'avance' else 'recule'» de «if(b) a else -a» cm'''
}

def dispatch generate(Block block) ''''''

def private double toValue(demo.scratch.Number n) {
	var double a;
	if (n instanceof NumberValue) {
		a = (n as NumberValue).value.doubleValue;
	} else {
		throw new Exception("Not implemented yet.")
	}
	return a;
}

}
