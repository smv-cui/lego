module _'Scratch.ecore'
import ecore : 'http://www.eclipse.org/emf/2002/Ecore#/';

package scratch : Scratch = 'http://smv.unige.ch/Scratch/2012'
{
	/****************************************************************************
 	 * Project
 	 */
	class Project
	{
		attribute name : String[1] { !ordered };
		property objects : Object[*] { ordered composes };
	}
	class Object
	{
		attribute name : String[1] { !ordered };
		property scripts : Script[*] { ordered composes };
	}
	class Script
	{
		property when : When[?] { !ordered composes };
		property blocks : Block[*] { ordered composes };
	}
	abstract class Block { interface };
	/*
	 * References to Object : defined or predefined
	 */
	abstract class IObject { interface };
	class ObjectRef extends IObject
	{
		property object : Object[1] { !ordered };
	}
	class ObjectPredef extends IObject
	{
		attribute object : ObjectPredefEnum[1] { !ordered };
	}
	/****************************************************************************
	 * When
	 */
	abstract class When { interface };
	class WhenFlag extends When;
--	class WhenClick extends When
--	{
--		property object : ObjectRef[1] { !ordered composes };
--	}
	class WhenMsg extends When
	{
--		property msg : Message[1] { !ordered };
		property msg : Message[1] { !ordered composes };
	}
--	class WhenKey extends When
--	{
--		attribute key : String[1];
--	}
	class Message
	{
		attribute name : String[1] { !ordered };
	}
	/****************************************************************
	 * Motion
	 */
	abstract class Motion extends Block { interface };
	class IfEdge extends Motion;
	class Move extends Motion
	{
		property steps : Number[1] { !ordered composes };
	}
	class TurnCW extends Motion
	{
		property angle : Number[1] { !ordered composes };
	}
	class TurnCCW extends Motion
	{
		property angle : Number[1] { !ordered composes };
	}
	class PointDirection extends Motion
	{
		property angle : Number[1] { !ordered composes };
	}
	class PointTowards extends Motion
	{
		property object : IObject[1] { !ordered composes };
	}
	class ChangeX extends Motion
	{
		property x : Number[1] { !ordered composes };
	}
	class ChangeY extends Motion
	{
		property y : Number[1] { !ordered composes };
	}
	class SetX extends Motion
	{
		property x : Number[1] { !ordered composes };
	}
	class SetY extends Motion
	{
		property y : Number[1] { !ordered composes };
	}
	class GoToXY extends Motion
	{
		property x : Number[1] { !ordered composes };
		property y : Number[1] { !ordered composes };
	}
	class GoTo extends Motion
	{
		property object : IObject[1] { !ordered composes };
	}
	class Glide extends Motion
	{
		property x : Number[1] { !ordered composes };
		property y : Number[1] { !ordered composes };
		property duration : Number[1] { !ordered composes };
	}
	/****************************************************************
	 * Control
	 */
	abstract class Control extends Block { interface };
	class If extends Control
	{
		property cond     : Binary[1] { !ordered composes };
		property _'true'  : Block[+]  { ordered composes };
		property _'false' : Block[+]  { ordered composes };
	}
	class Broadcast extends Control
	{
		attribute wait : Boolean[1] = 'false' { !ordered };
		property msg : Message[1] { !ordered composes };
	}
	class Stop extends Control
	{
		attribute all : Boolean[1] = 'false' { !ordered };
		attribute script : Boolean[1] = 'false' { !ordered };
	}
	abstract class Wait extends Control { interface };
	class WaitDelay extends Wait
	{
		property delay : Number[1] { !ordered composes };
	}
	class WaitUntil extends Wait
	{
		property cond : Binary[1] { composes };
	}
	abstract class Loop extends Control { interface }
	{
		property blocks : Block[*] { composes };
	}
	class Forever extends Loop
	{
		property cond : Binary[?] { composes };
	}
	class RepeatN extends Loop
	{
		property n : Number[1] { composes };
	}
	class RepeatUntil extends Loop
	{
		property cond : Binary[1] { composes };
	}
	/****************************************************************
	 * Operator
	 */
	abstract class Operator { interface };
	abstract class Number extends Operator { interface };
	abstract class Binary extends Operator { interface };
--	abstractclass Word    extends Operator { interface };
--	abstractclass List    extends Operator { interface };
	class NumberValue extends Number
	{
		attribute value : ecore::EFloat[1];
	}
	class NumberObjVar extends Number
	{
		property obj : IObject[1] { composes };
		property var : ObjectVariableEnum[1] { composes };
	}
	class NumberVar extends Number
	{
		attribute var : String[1];
	}
	class NumberVarX extends Number ;
	class NumberVarY extends Number ;
	class NumberVarD extends Number ;
	class BinaryVar extends Binary
	{
		attribute var : String[1];
	}
--	class WordVar extends Word
--	{
--		attribute var : String[1];
--	}
--	class WordValue extends Word
--	{
--		attribute value : ecore_0::EString[1];
--	}
--	class Add    extends Number, OperatorNN ;
--	class Sub    extends Number, OperatorNN ;
--	class Mul    extends Number, OperatorNN ;
--	class Div    extends Number, OperatorNN ;
--	class Mod    extends Number, OperatorNN ;
--	class Rand   extends Number, OperatorNN ;
--	class Sqrt   extends Number, OperatorN ;
--	class Round  extends Number, OperatorN ;
--	class Length extends Number, OperatorW ;
--	class Join   extends Word,   OperatorWW ;
--	class Letter extends Word,   OperatorNW ;
	class Lt extends Binary
	{
		property left  : Number[1] { composes };
		property right : Number[1] { composes };
	}
	class Gt extends Binary
	{
		property left  : Number[1] { composes };
		property right : Number[1] { composes };
	}
	class Eq extends Binary
	{
		property left  : Number[1] { composes };
		property right : Number[1] { composes };
	}
	class And extends Binary
	{
		property left  : Binary[1] { composes };
		property right : Binary[1] { composes };
	}
	class Or extends Binary
	{
		property left  : Binary[1] { composes };
		property right : Binary[1] { composes };
	}
	class Not extends Binary
	{
		property value : Binary[1] { composes };
	}
	class TouchsColor extends Binary
	{
		attribute color : String[1];
	}
	/****************************************************************
	 * Looks
	 */
	abstract class Looks extends Block { interface };
	class Hide extends Looks;
	class Show extends Looks;
	class Say extends Looks
	{
		attribute sentence : String[1] { !ordered };
		property delay     : Number[1] { !ordered composes };
	}
	/********************************************************************
 	 * Sound
 	 */
	abstract class Sound extends Block { interface };
	class Play extends Sound
	{
		attribute sound : String[1] ;
	}
	/****************************************************************
	 * Sensing
	 * /
	abstract class Sensing extends Block { interface };
	/****************************************************************
	 * Pen
	 * /
	class Pen extends Block { abstract, interface } ;
	class Stamp              ;
	class Clear              ;
	class PenDown            ;
	class PenUp              ;
	class ChangePenColorBy   ;
	class SetPenColorToColor ;
	class SetPenColorTo      ;
	class ChangePenShadeBy   ;
	class SetPenShade        ;
	class ChangePenSize      ;
	class SetPenSize         ;
	/****************************************************************
	 * ObjectPredefEnum
	 */
	enum ObjectPredefEnum { serializable }
	{
		mousepointer { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'mouse-pointer'); }
	}
	/****************************************************************
	 * ObjectVariableEnum
	 */
	enum ObjectVariableEnum { serializable }
	{
		xposition { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'x position'); }
		yposition { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'y position'); }
		direction { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'direction');	 }
	}
	/****************************************************************
	 * Key
	 * /
	enum KeyEnum
	{
		a    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'a key');	}
		b    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'b key');	}
		c    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'c key');	}
		d    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'd key');	}
		_'e' { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'e key');	}
		f    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'f key');	}
		g    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'g key');	}
		h    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'h key');	}
		i    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'i key');	}
		j    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'j key');	}
		k    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'k key');	}
		l    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'l key');	}
		m    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'm key');	}
		n    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'n key');	}
		o    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'o key');	}
		p    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'p key');	}
		q    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'q key');	}
		r    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'r key');	}
		s    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 's key');	}
		t    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 't key');	}
		u    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'u key');	}
		v    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'v key');	}
		w    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'w key');	}
		x    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'x key');	}
		y    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'y key');	}
		z    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'z key');	}
		zero   { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = '0 key');	}
		one    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = '1 key');	}
		two    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = '2 key');	}
		three  { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = '3 key');	}
		four   { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = '4 key');	}
		five   { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = '5 key');	}
		six    { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = '6 key');	}
		seven  { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = '7 key');	}
		height { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = '8 key');	}
		nine   { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = '9 key');	}
		left   { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'left arrow key');	}
		right  { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'right arrow key');	}
		up     { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'up arrow key');	}
		down   { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'down arrow key');	}
		space  { annotation _'http://www.eclipse.org/emf/2002/Ecore'(literal = 'space key');	}
	}
*/
}