package demo.scratch.generator;

import demo.scratch.Block;
import demo.scratch.Broadcast;
import demo.scratch.Message;
import demo.scratch.Move;
import demo.scratch.NumberValue;
import demo.scratch.Script;
import demo.scratch.TurnCCW;
import demo.scratch.TurnCW;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.BooleanExtensions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.IntegerExtensions;

@SuppressWarnings("all")
public class Lego {
  public CharSequence generate(final Script script) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("DEBUT");
    _builder.newLine();
    {
      EList<Block> _blocks = script.getBlocks();
      for(final Block b : _blocks) {
        CharSequence _generate = this.generate(b);
        _builder.append(_generate, "");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("FIN");
    _builder.newLine();
    return _builder;
  }
  
  protected CharSequence _generate(final Broadcast block) {
    try {
      CharSequence _xblockexpression = null;
      {
        Message _msg = block.getMsg();
        String _name = _msg.getName();
        String _lowerCase = _name.toLowerCase();
        final String m = _lowerCase;
        String n = null;
        boolean _equals = m.equals("close");
        if (_equals) {
          n = "ferme la pince";
        } else {
          boolean _equals_1 = m.equals("open");
          if (_equals_1) {
            n = "ouvre la pince";
          } else {
            Exception _exception = new Exception("Not implemented yet !");
            throw _exception;
          }
        }
        StringConcatenation _builder = new StringConcatenation();
        _builder.append(n, "");
        _xblockexpression = (_builder);
      }
      return _xblockexpression;
    } catch (Exception _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  protected CharSequence _generate(final TurnCW block) {
    CharSequence _xblockexpression = null;
    {
      demo.scratch.Number _angle = block.getAngle();
      double _value = this.toValue(_angle);
      int _intValue = Double.valueOf(_value).intValue();
      int a = _intValue;
      boolean _operator_greaterThan = IntegerExtensions.operator_greaterThan(a, 0);
      final boolean b = _operator_greaterThan;
      boolean _operator_or = false;
      boolean _operator_equals = IntegerExtensions.operator_equals(a, 90);
      if (_operator_equals) {
        _operator_or = true;
      } else {
        int _operator_minus = IntegerExtensions.operator_minus(90);
        boolean _operator_equals_1 = IntegerExtensions.operator_equals(a, _operator_minus);
        _operator_or = BooleanExtensions.operator_or(_operator_equals, _operator_equals_1);
      }
      final boolean c = _operator_or;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("tourne \u00E0 ");
      String _xifexpression = null;
      if (b) {
        _xifexpression = "droite";
      } else {
        _xifexpression = "gauche";
      }
      _builder.append(_xifexpression, "");
      {
        boolean _operator_not = BooleanExtensions.operator_not(c);
        if (_operator_not) {
          _builder.append(" de ");
          int _xifexpression_1 = (int) 0;
          if (b) {
            _xifexpression_1 = a;
          } else {
            int _operator_minus_1 = IntegerExtensions.operator_minus(a);
            _xifexpression_1 = _operator_minus_1;
          }
          _builder.append(_xifexpression_1, "");
          _builder.append(" degr\u00E9s");
        }
      }
      _xblockexpression = (_builder);
    }
    return _xblockexpression;
  }
  
  protected CharSequence _generate(final TurnCCW block) {
    CharSequence _xblockexpression = null;
    {
      demo.scratch.Number _angle = block.getAngle();
      double _value = this.toValue(_angle);
      int _intValue = Double.valueOf(_value).intValue();
      int a = _intValue;
      boolean _operator_greaterThan = IntegerExtensions.operator_greaterThan(a, 0);
      final boolean b = _operator_greaterThan;
      boolean _operator_or = false;
      boolean _operator_equals = IntegerExtensions.operator_equals(a, 90);
      if (_operator_equals) {
        _operator_or = true;
      } else {
        int _operator_minus = IntegerExtensions.operator_minus(90);
        boolean _operator_equals_1 = IntegerExtensions.operator_equals(a, _operator_minus);
        _operator_or = BooleanExtensions.operator_or(_operator_equals, _operator_equals_1);
      }
      final boolean c = _operator_or;
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("tourne \u00E0 ");
      String _xifexpression = null;
      if (b) {
        _xifexpression = "gauche";
      } else {
        _xifexpression = "droite";
      }
      _builder.append(_xifexpression, "");
      {
        boolean _operator_not = BooleanExtensions.operator_not(c);
        if (_operator_not) {
          _builder.append(" de ");
          int _xifexpression_1 = (int) 0;
          if (b) {
            _xifexpression_1 = a;
          } else {
            int _operator_minus_1 = IntegerExtensions.operator_minus(a);
            _xifexpression_1 = _operator_minus_1;
          }
          _builder.append(_xifexpression_1, "");
          _builder.append(" degr\u00E9s");
        }
      }
      _xblockexpression = (_builder);
    }
    return _xblockexpression;
  }
  
  protected CharSequence _generate(final Move block) {
    CharSequence _xblockexpression = null;
    {
      demo.scratch.Number _steps = block.getSteps();
      double _value = this.toValue(_steps);
      int _operator_multiply = IntegerExtensions.operator_multiply(Integer.valueOf(10), Double.valueOf(_value));
      int _operator_divide = IntegerExtensions.operator_divide(_operator_multiply, 25);
      int _intValue = Integer.valueOf(_operator_divide).intValue();
      int a = _intValue;
      boolean _operator_greaterThan = IntegerExtensions.operator_greaterThan(a, 0);
      final boolean b = _operator_greaterThan;
      StringConcatenation _builder = new StringConcatenation();
      String _xifexpression = null;
      if (b) {
        _xifexpression = "avance";
      } else {
        _xifexpression = "recule";
      }
      _builder.append(_xifexpression, "");
      _builder.append(" de ");
      int _xifexpression_1 = (int) 0;
      if (b) {
        _xifexpression_1 = a;
      } else {
        int _operator_minus = IntegerExtensions.operator_minus(a);
        _xifexpression_1 = _operator_minus;
      }
      _builder.append(_xifexpression_1, "");
      _builder.append(" cm");
      _xblockexpression = (_builder);
    }
    return _xblockexpression;
  }
  
  protected CharSequence _generate(final Block block) {
    StringConcatenation _builder = new StringConcatenation();
    return _builder;
  }
  
  private double toValue(final demo.scratch.Number n) {
    try {
      {
        double a = 0;
        if ((n instanceof NumberValue)) {
          float _value = ((NumberValue) n).getValue();
          double _doubleValue = Float.valueOf(_value).doubleValue();
          a = _doubleValue;
        } else {
          Exception _exception = new Exception("Not implemented yet.");
          throw _exception;
        }
        return a;
      }
    } catch (Exception _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public CharSequence generate(final Block block) {
    if (block instanceof Broadcast) {
      return _generate((Broadcast)block);
    } else if (block instanceof Move) {
      return _generate((Move)block);
    } else if (block instanceof TurnCCW) {
      return _generate((TurnCCW)block);
    } else if (block instanceof TurnCW) {
      return _generate((TurnCW)block);
    } else if (block != null) {
      return _generate(block);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(block).toString());
    }
  }
}
