package demo.scratch.generator;

import demo.scratch.Script;
import demo.scratch.generator.IScratchGenerator;
import demo.scratch.generator.Lego;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.xbase.lib.IntegerExtensions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

@SuppressWarnings("all")
public class ScratchGenerator implements IGenerator, IScratchGenerator {
  public void doGenerate(final Resource resource, final IFileSystemAccess fsa) {
      int i = 0;
      EList<EObject> _contents = resource.getContents();
      Iterable<Script> _filter = IterableExtensions.<Script>filter(_contents, demo.scratch.Script.class);
      for (final Script e : _filter) {
        {
          int _operator_plus = IntegerExtensions.operator_plus(i, 1);
          i = _operator_plus;
          String _string = Integer.valueOf(i).toString();
          String _operator_plus_1 = StringExtensions.operator_plus(_string, ".scratch");
          Lego _lego = new Lego();
          CharSequence _generate = _lego.generate(e);
          fsa.generateFile(_operator_plus_1, _generate);
        }
      }
  }
  
  public CharSequence doGenerateLego(final Script script) {
    Lego _lego = new Lego();
    CharSequence _generate = _lego.generate(script);
    return _generate;
  }
}
