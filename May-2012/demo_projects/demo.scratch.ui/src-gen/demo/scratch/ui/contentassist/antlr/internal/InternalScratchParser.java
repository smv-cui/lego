package demo.scratch.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import demo.scratch.services.ScratchGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalScratchParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_NUM", "RULE_STRING", "RULE_WS", "RULE_ANY_OTHER", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "'mouse-pointer'", "'x position'", "'y position'", "'direction'", "'project'", "'object'", "'script'", "'end'", "'when'", "'green flag clicked'", "'I receive'", "'forever'", "'if'", "'repeat'", "'until'", "'stop'", "'wait'", "'secs'", "'broadcast'", "'else'", "'move'", "'steps'", "'turn cw'", "'degrees'", "'turn ccw'", "'point in direction'", "'point towards'", "'go to'", "'x:'", "'y:'", "'glide'", "'secs to x:'", "'change x by'", "'change y by'", "'set x to'", "'set y to'", "'if on edge, bounce'", "'('", "')'", "'$'", "'of'", "'touching'", "'color'", "'?'", "'<'", "'>'", "'='", "'and'", "'or'", "'not'", "'hide'", "'show'", "'say'", "'for'", "'play'", "'sound'", "'all'", "'and wait'"
    };
    public static final int T__68=68;
    public static final int RULE_ID=4;
    public static final int T__66=66;
    public static final int T__67=67;
    public static final int T__64=64;
    public static final int T__29=29;
    public static final int T__65=65;
    public static final int T__28=28;
    public static final int T__62=62;
    public static final int T__27=27;
    public static final int T__63=63;
    public static final int T__26=26;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=8;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int T__61=61;
    public static final int T__60=60;
    public static final int EOF=-1;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__19=19;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__16=16;
    public static final int T__52=52;
    public static final int T__15=15;
    public static final int T__53=53;
    public static final int T__18=18;
    public static final int T__54=54;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int T__59=59;
    public static final int T__50=50;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int RULE_SL_COMMENT=10;
    public static final int RULE_ML_COMMENT=9;
    public static final int RULE_NUM=5;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_STRING=6;
    public static final int T__32=32;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int RULE_WS=7;

    // delegates
    // delegators


        public InternalScratchParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalScratchParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalScratchParser.tokenNames; }
    public String getGrammarFileName() { return "../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g"; }


     
     	private ScratchGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(ScratchGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleProject"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:61:1: entryRuleProject : ruleProject EOF ;
    public final void entryRuleProject() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:62:1: ( ruleProject EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:63:1: ruleProject EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProjectRule()); 
            }
            pushFollow(FOLLOW_ruleProject_in_entryRuleProject67);
            ruleProject();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getProjectRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleProject74); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleProject"


    // $ANTLR start "ruleProject"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:70:1: ruleProject : ( ( rule__Project__Group__0 ) ) ;
    public final void ruleProject() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:74:2: ( ( ( rule__Project__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:75:1: ( ( rule__Project__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:75:1: ( ( rule__Project__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:76:1: ( rule__Project__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProjectAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:77:1: ( rule__Project__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:77:2: rule__Project__Group__0
            {
            pushFollow(FOLLOW_rule__Project__Group__0_in_ruleProject100);
            rule__Project__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getProjectAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleProject"


    // $ANTLR start "entryRuleObject"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:89:1: entryRuleObject : ruleObject EOF ;
    public final void entryRuleObject() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:90:1: ( ruleObject EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:91:1: ruleObject EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getObjectRule()); 
            }
            pushFollow(FOLLOW_ruleObject_in_entryRuleObject127);
            ruleObject();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getObjectRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleObject134); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleObject"


    // $ANTLR start "ruleObject"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:98:1: ruleObject : ( ( rule__Object__Group__0 ) ) ;
    public final void ruleObject() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:102:2: ( ( ( rule__Object__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:103:1: ( ( rule__Object__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:103:1: ( ( rule__Object__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:104:1: ( rule__Object__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getObjectAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:105:1: ( rule__Object__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:105:2: rule__Object__Group__0
            {
            pushFollow(FOLLOW_rule__Object__Group__0_in_ruleObject160);
            rule__Object__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getObjectAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleObject"


    // $ANTLR start "entryRuleIObject"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:117:1: entryRuleIObject : ruleIObject EOF ;
    public final void entryRuleIObject() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:118:1: ( ruleIObject EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:119:1: ruleIObject EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIObjectRule()); 
            }
            pushFollow(FOLLOW_ruleIObject_in_entryRuleIObject187);
            ruleIObject();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIObjectRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIObject194); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIObject"


    // $ANTLR start "ruleIObject"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:126:1: ruleIObject : ( ( rule__IObject__Alternatives ) ) ;
    public final void ruleIObject() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:130:2: ( ( ( rule__IObject__Alternatives ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:131:1: ( ( rule__IObject__Alternatives ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:131:1: ( ( rule__IObject__Alternatives ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:132:1: ( rule__IObject__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIObjectAccess().getAlternatives()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:133:1: ( rule__IObject__Alternatives )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:133:2: rule__IObject__Alternatives
            {
            pushFollow(FOLLOW_rule__IObject__Alternatives_in_ruleIObject220);
            rule__IObject__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIObjectAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIObject"


    // $ANTLR start "entryRuleRef"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:145:1: entryRuleRef : ruleRef EOF ;
    public final void entryRuleRef() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:146:1: ( ruleRef EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:147:1: ruleRef EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRefRule()); 
            }
            pushFollow(FOLLOW_ruleRef_in_entryRuleRef247);
            ruleRef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRefRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleRef254); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRef"


    // $ANTLR start "ruleRef"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:154:1: ruleRef : ( ( rule__Ref__ObjectAssignment ) ) ;
    public final void ruleRef() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:158:2: ( ( ( rule__Ref__ObjectAssignment ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:159:1: ( ( rule__Ref__ObjectAssignment ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:159:1: ( ( rule__Ref__ObjectAssignment ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:160:1: ( rule__Ref__ObjectAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRefAccess().getObjectAssignment()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:161:1: ( rule__Ref__ObjectAssignment )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:161:2: rule__Ref__ObjectAssignment
            {
            pushFollow(FOLLOW_rule__Ref__ObjectAssignment_in_ruleRef280);
            rule__Ref__ObjectAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRefAccess().getObjectAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRef"


    // $ANTLR start "entryRulePredef"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:173:1: entryRulePredef : rulePredef EOF ;
    public final void entryRulePredef() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:174:1: ( rulePredef EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:175:1: rulePredef EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPredefRule()); 
            }
            pushFollow(FOLLOW_rulePredef_in_entryRulePredef307);
            rulePredef();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPredefRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePredef314); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePredef"


    // $ANTLR start "rulePredef"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:182:1: rulePredef : ( ( rule__Predef__ObjectAssignment ) ) ;
    public final void rulePredef() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:186:2: ( ( ( rule__Predef__ObjectAssignment ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:187:1: ( ( rule__Predef__ObjectAssignment ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:187:1: ( ( rule__Predef__ObjectAssignment ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:188:1: ( rule__Predef__ObjectAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPredefAccess().getObjectAssignment()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:189:1: ( rule__Predef__ObjectAssignment )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:189:2: rule__Predef__ObjectAssignment
            {
            pushFollow(FOLLOW_rule__Predef__ObjectAssignment_in_rulePredef340);
            rule__Predef__ObjectAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPredefAccess().getObjectAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePredef"


    // $ANTLR start "entryRuleScript"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:201:1: entryRuleScript : ruleScript EOF ;
    public final void entryRuleScript() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:202:1: ( ruleScript EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:203:1: ruleScript EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScriptRule()); 
            }
            pushFollow(FOLLOW_ruleScript_in_entryRuleScript367);
            ruleScript();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getScriptRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleScript374); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleScript"


    // $ANTLR start "ruleScript"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:210:1: ruleScript : ( ( rule__Script__Group__0 ) ) ;
    public final void ruleScript() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:214:2: ( ( ( rule__Script__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:215:1: ( ( rule__Script__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:215:1: ( ( rule__Script__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:216:1: ( rule__Script__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScriptAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:217:1: ( rule__Script__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:217:2: rule__Script__Group__0
            {
            pushFollow(FOLLOW_rule__Script__Group__0_in_ruleScript400);
            rule__Script__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getScriptAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleScript"


    // $ANTLR start "entryRuleWhen"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:229:1: entryRuleWhen : ruleWhen EOF ;
    public final void entryRuleWhen() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:230:1: ( ruleWhen EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:231:1: ruleWhen EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWhenRule()); 
            }
            pushFollow(FOLLOW_ruleWhen_in_entryRuleWhen427);
            ruleWhen();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWhenRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleWhen434); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWhen"


    // $ANTLR start "ruleWhen"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:238:1: ruleWhen : ( ( rule__When__Group__0 ) ) ;
    public final void ruleWhen() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:242:2: ( ( ( rule__When__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:243:1: ( ( rule__When__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:243:1: ( ( rule__When__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:244:1: ( rule__When__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWhenAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:245:1: ( rule__When__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:245:2: rule__When__Group__0
            {
            pushFollow(FOLLOW_rule__When__Group__0_in_ruleWhen460);
            rule__When__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWhenAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWhen"


    // $ANTLR start "entryRuleWhenFlag"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:257:1: entryRuleWhenFlag : ruleWhenFlag EOF ;
    public final void entryRuleWhenFlag() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:258:1: ( ruleWhenFlag EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:259:1: ruleWhenFlag EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWhenFlagRule()); 
            }
            pushFollow(FOLLOW_ruleWhenFlag_in_entryRuleWhenFlag487);
            ruleWhenFlag();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWhenFlagRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleWhenFlag494); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWhenFlag"


    // $ANTLR start "ruleWhenFlag"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:266:1: ruleWhenFlag : ( ( rule__WhenFlag__Group__0 ) ) ;
    public final void ruleWhenFlag() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:270:2: ( ( ( rule__WhenFlag__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:271:1: ( ( rule__WhenFlag__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:271:1: ( ( rule__WhenFlag__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:272:1: ( rule__WhenFlag__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWhenFlagAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:273:1: ( rule__WhenFlag__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:273:2: rule__WhenFlag__Group__0
            {
            pushFollow(FOLLOW_rule__WhenFlag__Group__0_in_ruleWhenFlag520);
            rule__WhenFlag__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWhenFlagAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWhenFlag"


    // $ANTLR start "entryRuleWhenMsg"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:285:1: entryRuleWhenMsg : ruleWhenMsg EOF ;
    public final void entryRuleWhenMsg() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:286:1: ( ruleWhenMsg EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:287:1: ruleWhenMsg EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWhenMsgRule()); 
            }
            pushFollow(FOLLOW_ruleWhenMsg_in_entryRuleWhenMsg547);
            ruleWhenMsg();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWhenMsgRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleWhenMsg554); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWhenMsg"


    // $ANTLR start "ruleWhenMsg"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:294:1: ruleWhenMsg : ( ( rule__WhenMsg__Group__0 ) ) ;
    public final void ruleWhenMsg() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:298:2: ( ( ( rule__WhenMsg__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:299:1: ( ( rule__WhenMsg__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:299:1: ( ( rule__WhenMsg__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:300:1: ( rule__WhenMsg__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWhenMsgAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:301:1: ( rule__WhenMsg__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:301:2: rule__WhenMsg__Group__0
            {
            pushFollow(FOLLOW_rule__WhenMsg__Group__0_in_ruleWhenMsg580);
            rule__WhenMsg__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWhenMsgAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWhenMsg"


    // $ANTLR start "entryRuleMessage"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:313:1: entryRuleMessage : ruleMessage EOF ;
    public final void entryRuleMessage() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:314:1: ( ruleMessage EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:315:1: ruleMessage EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMessageRule()); 
            }
            pushFollow(FOLLOW_ruleMessage_in_entryRuleMessage607);
            ruleMessage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMessageRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMessage614); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMessage"


    // $ANTLR start "ruleMessage"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:322:1: ruleMessage : ( ( rule__Message__NameAssignment ) ) ;
    public final void ruleMessage() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:326:2: ( ( ( rule__Message__NameAssignment ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:327:1: ( ( rule__Message__NameAssignment ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:327:1: ( ( rule__Message__NameAssignment ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:328:1: ( rule__Message__NameAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMessageAccess().getNameAssignment()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:329:1: ( rule__Message__NameAssignment )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:329:2: rule__Message__NameAssignment
            {
            pushFollow(FOLLOW_rule__Message__NameAssignment_in_ruleMessage640);
            rule__Message__NameAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMessageAccess().getNameAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMessage"


    // $ANTLR start "entryRuleBlock"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:341:1: entryRuleBlock : ruleBlock EOF ;
    public final void entryRuleBlock() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:342:1: ( ruleBlock EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:343:1: ruleBlock EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBlockRule()); 
            }
            pushFollow(FOLLOW_ruleBlock_in_entryRuleBlock667);
            ruleBlock();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBlockRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBlock674); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBlock"


    // $ANTLR start "ruleBlock"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:350:1: ruleBlock : ( ( rule__Block__Alternatives ) ) ;
    public final void ruleBlock() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:354:2: ( ( ( rule__Block__Alternatives ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:355:1: ( ( rule__Block__Alternatives ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:355:1: ( ( rule__Block__Alternatives ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:356:1: ( rule__Block__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBlockAccess().getAlternatives()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:357:1: ( rule__Block__Alternatives )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:357:2: rule__Block__Alternatives
            {
            pushFollow(FOLLOW_rule__Block__Alternatives_in_ruleBlock700);
            rule__Block__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBlockAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBlock"


    // $ANTLR start "entryRuleLoop"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:369:1: entryRuleLoop : ruleLoop EOF ;
    public final void entryRuleLoop() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:370:1: ( ruleLoop EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:371:1: ruleLoop EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopRule()); 
            }
            pushFollow(FOLLOW_ruleLoop_in_entryRuleLoop727);
            ruleLoop();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLoop734); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLoop"


    // $ANTLR start "ruleLoop"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:378:1: ruleLoop : ( ( rule__Loop__Group__0 ) ) ;
    public final void ruleLoop() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:382:2: ( ( ( rule__Loop__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:383:1: ( ( rule__Loop__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:383:1: ( ( rule__Loop__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:384:1: ( rule__Loop__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:385:1: ( rule__Loop__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:385:2: rule__Loop__Group__0
            {
            pushFollow(FOLLOW_rule__Loop__Group__0_in_ruleLoop760);
            rule__Loop__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLoop"


    // $ANTLR start "entryRuleForever"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:397:1: entryRuleForever : ruleForever EOF ;
    public final void entryRuleForever() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:398:1: ( ruleForever EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:399:1: ruleForever EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getForeverRule()); 
            }
            pushFollow(FOLLOW_ruleForever_in_entryRuleForever787);
            ruleForever();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getForeverRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleForever794); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleForever"


    // $ANTLR start "ruleForever"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:406:1: ruleForever : ( ( rule__Forever__Group__0 ) ) ;
    public final void ruleForever() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:410:2: ( ( ( rule__Forever__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:411:1: ( ( rule__Forever__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:411:1: ( ( rule__Forever__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:412:1: ( rule__Forever__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getForeverAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:413:1: ( rule__Forever__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:413:2: rule__Forever__Group__0
            {
            pushFollow(FOLLOW_rule__Forever__Group__0_in_ruleForever820);
            rule__Forever__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getForeverAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleForever"


    // $ANTLR start "entryRuleRepeatN"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:425:1: entryRuleRepeatN : ruleRepeatN EOF ;
    public final void entryRuleRepeatN() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:426:1: ( ruleRepeatN EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:427:1: ruleRepeatN EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRepeatNRule()); 
            }
            pushFollow(FOLLOW_ruleRepeatN_in_entryRuleRepeatN847);
            ruleRepeatN();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRepeatNRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleRepeatN854); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRepeatN"


    // $ANTLR start "ruleRepeatN"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:434:1: ruleRepeatN : ( ( rule__RepeatN__Group__0 ) ) ;
    public final void ruleRepeatN() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:438:2: ( ( ( rule__RepeatN__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:439:1: ( ( rule__RepeatN__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:439:1: ( ( rule__RepeatN__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:440:1: ( rule__RepeatN__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRepeatNAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:441:1: ( rule__RepeatN__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:441:2: rule__RepeatN__Group__0
            {
            pushFollow(FOLLOW_rule__RepeatN__Group__0_in_ruleRepeatN880);
            rule__RepeatN__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRepeatNAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRepeatN"


    // $ANTLR start "entryRuleRepeatUntil"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:453:1: entryRuleRepeatUntil : ruleRepeatUntil EOF ;
    public final void entryRuleRepeatUntil() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:454:1: ( ruleRepeatUntil EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:455:1: ruleRepeatUntil EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRepeatUntilRule()); 
            }
            pushFollow(FOLLOW_ruleRepeatUntil_in_entryRuleRepeatUntil907);
            ruleRepeatUntil();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRepeatUntilRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleRepeatUntil914); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRepeatUntil"


    // $ANTLR start "ruleRepeatUntil"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:462:1: ruleRepeatUntil : ( ( rule__RepeatUntil__Group__0 ) ) ;
    public final void ruleRepeatUntil() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:466:2: ( ( ( rule__RepeatUntil__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:467:1: ( ( rule__RepeatUntil__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:467:1: ( ( rule__RepeatUntil__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:468:1: ( rule__RepeatUntil__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRepeatUntilAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:469:1: ( rule__RepeatUntil__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:469:2: rule__RepeatUntil__Group__0
            {
            pushFollow(FOLLOW_rule__RepeatUntil__Group__0_in_ruleRepeatUntil940);
            rule__RepeatUntil__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRepeatUntilAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRepeatUntil"


    // $ANTLR start "entryRuleStop"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:481:1: entryRuleStop : ruleStop EOF ;
    public final void entryRuleStop() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:482:1: ( ruleStop EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:483:1: ruleStop EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStopRule()); 
            }
            pushFollow(FOLLOW_ruleStop_in_entryRuleStop967);
            ruleStop();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStopRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleStop974); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStop"


    // $ANTLR start "ruleStop"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:490:1: ruleStop : ( ( rule__Stop__Group__0 ) ) ;
    public final void ruleStop() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:494:2: ( ( ( rule__Stop__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:495:1: ( ( rule__Stop__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:495:1: ( ( rule__Stop__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:496:1: ( rule__Stop__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStopAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:497:1: ( rule__Stop__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:497:2: rule__Stop__Group__0
            {
            pushFollow(FOLLOW_rule__Stop__Group__0_in_ruleStop1000);
            rule__Stop__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStopAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStop"


    // $ANTLR start "entryRuleWait"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:509:1: entryRuleWait : ruleWait EOF ;
    public final void entryRuleWait() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:510:1: ( ruleWait EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:511:1: ruleWait EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitRule()); 
            }
            pushFollow(FOLLOW_ruleWait_in_entryRuleWait1027);
            ruleWait();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleWait1034); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWait"


    // $ANTLR start "ruleWait"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:518:1: ruleWait : ( ( rule__Wait__Group__0 ) ) ;
    public final void ruleWait() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:522:2: ( ( ( rule__Wait__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:523:1: ( ( rule__Wait__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:523:1: ( ( rule__Wait__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:524:1: ( rule__Wait__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:525:1: ( rule__Wait__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:525:2: rule__Wait__Group__0
            {
            pushFollow(FOLLOW_rule__Wait__Group__0_in_ruleWait1060);
            rule__Wait__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWait"


    // $ANTLR start "entryRuleWaitDelay"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:537:1: entryRuleWaitDelay : ruleWaitDelay EOF ;
    public final void entryRuleWaitDelay() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:538:1: ( ruleWaitDelay EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:539:1: ruleWaitDelay EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitDelayRule()); 
            }
            pushFollow(FOLLOW_ruleWaitDelay_in_entryRuleWaitDelay1087);
            ruleWaitDelay();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitDelayRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleWaitDelay1094); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWaitDelay"


    // $ANTLR start "ruleWaitDelay"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:546:1: ruleWaitDelay : ( ( rule__WaitDelay__Group__0 ) ) ;
    public final void ruleWaitDelay() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:550:2: ( ( ( rule__WaitDelay__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:551:1: ( ( rule__WaitDelay__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:551:1: ( ( rule__WaitDelay__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:552:1: ( rule__WaitDelay__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitDelayAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:553:1: ( rule__WaitDelay__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:553:2: rule__WaitDelay__Group__0
            {
            pushFollow(FOLLOW_rule__WaitDelay__Group__0_in_ruleWaitDelay1120);
            rule__WaitDelay__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitDelayAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWaitDelay"


    // $ANTLR start "entryRuleWaitUntil"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:565:1: entryRuleWaitUntil : ruleWaitUntil EOF ;
    public final void entryRuleWaitUntil() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:566:1: ( ruleWaitUntil EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:567:1: ruleWaitUntil EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitUntilRule()); 
            }
            pushFollow(FOLLOW_ruleWaitUntil_in_entryRuleWaitUntil1147);
            ruleWaitUntil();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitUntilRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleWaitUntil1154); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWaitUntil"


    // $ANTLR start "ruleWaitUntil"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:574:1: ruleWaitUntil : ( ( rule__WaitUntil__Group__0 ) ) ;
    public final void ruleWaitUntil() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:578:2: ( ( ( rule__WaitUntil__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:579:1: ( ( rule__WaitUntil__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:579:1: ( ( rule__WaitUntil__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:580:1: ( rule__WaitUntil__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitUntilAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:581:1: ( rule__WaitUntil__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:581:2: rule__WaitUntil__Group__0
            {
            pushFollow(FOLLOW_rule__WaitUntil__Group__0_in_ruleWaitUntil1180);
            rule__WaitUntil__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitUntilAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWaitUntil"


    // $ANTLR start "entryRuleBroadcast"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:593:1: entryRuleBroadcast : ruleBroadcast EOF ;
    public final void entryRuleBroadcast() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:594:1: ( ruleBroadcast EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:595:1: ruleBroadcast EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBroadcastRule()); 
            }
            pushFollow(FOLLOW_ruleBroadcast_in_entryRuleBroadcast1207);
            ruleBroadcast();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBroadcastRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBroadcast1214); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBroadcast"


    // $ANTLR start "ruleBroadcast"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:602:1: ruleBroadcast : ( ( rule__Broadcast__Group__0 ) ) ;
    public final void ruleBroadcast() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:606:2: ( ( ( rule__Broadcast__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:607:1: ( ( rule__Broadcast__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:607:1: ( ( rule__Broadcast__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:608:1: ( rule__Broadcast__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBroadcastAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:609:1: ( rule__Broadcast__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:609:2: rule__Broadcast__Group__0
            {
            pushFollow(FOLLOW_rule__Broadcast__Group__0_in_ruleBroadcast1240);
            rule__Broadcast__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBroadcastAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBroadcast"


    // $ANTLR start "entryRuleIf"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:621:1: entryRuleIf : ruleIf EOF ;
    public final void entryRuleIf() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:622:1: ( ruleIf EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:623:1: ruleIf EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfRule()); 
            }
            pushFollow(FOLLOW_ruleIf_in_entryRuleIf1267);
            ruleIf();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIf1274); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIf"


    // $ANTLR start "ruleIf"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:630:1: ruleIf : ( ( rule__If__Group__0 ) ) ;
    public final void ruleIf() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:634:2: ( ( ( rule__If__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:635:1: ( ( rule__If__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:635:1: ( ( rule__If__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:636:1: ( rule__If__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:637:1: ( rule__If__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:637:2: rule__If__Group__0
            {
            pushFollow(FOLLOW_rule__If__Group__0_in_ruleIf1300);
            rule__If__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIf"


    // $ANTLR start "entryRuleMove"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:649:1: entryRuleMove : ruleMove EOF ;
    public final void entryRuleMove() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:650:1: ( ruleMove EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:651:1: ruleMove EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMoveRule()); 
            }
            pushFollow(FOLLOW_ruleMove_in_entryRuleMove1327);
            ruleMove();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMoveRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleMove1334); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMove"


    // $ANTLR start "ruleMove"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:658:1: ruleMove : ( ( rule__Move__Group__0 ) ) ;
    public final void ruleMove() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:662:2: ( ( ( rule__Move__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:663:1: ( ( rule__Move__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:663:1: ( ( rule__Move__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:664:1: ( rule__Move__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMoveAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:665:1: ( rule__Move__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:665:2: rule__Move__Group__0
            {
            pushFollow(FOLLOW_rule__Move__Group__0_in_ruleMove1360);
            rule__Move__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMoveAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMove"


    // $ANTLR start "entryRuleTurnCW"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:677:1: entryRuleTurnCW : ruleTurnCW EOF ;
    public final void entryRuleTurnCW() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:678:1: ( ruleTurnCW EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:679:1: ruleTurnCW EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTurnCWRule()); 
            }
            pushFollow(FOLLOW_ruleTurnCW_in_entryRuleTurnCW1387);
            ruleTurnCW();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTurnCWRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTurnCW1394); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTurnCW"


    // $ANTLR start "ruleTurnCW"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:686:1: ruleTurnCW : ( ( rule__TurnCW__Group__0 ) ) ;
    public final void ruleTurnCW() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:690:2: ( ( ( rule__TurnCW__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:691:1: ( ( rule__TurnCW__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:691:1: ( ( rule__TurnCW__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:692:1: ( rule__TurnCW__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTurnCWAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:693:1: ( rule__TurnCW__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:693:2: rule__TurnCW__Group__0
            {
            pushFollow(FOLLOW_rule__TurnCW__Group__0_in_ruleTurnCW1420);
            rule__TurnCW__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTurnCWAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTurnCW"


    // $ANTLR start "entryRuleTurnCCW"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:705:1: entryRuleTurnCCW : ruleTurnCCW EOF ;
    public final void entryRuleTurnCCW() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:706:1: ( ruleTurnCCW EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:707:1: ruleTurnCCW EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTurnCCWRule()); 
            }
            pushFollow(FOLLOW_ruleTurnCCW_in_entryRuleTurnCCW1447);
            ruleTurnCCW();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTurnCCWRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTurnCCW1454); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTurnCCW"


    // $ANTLR start "ruleTurnCCW"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:714:1: ruleTurnCCW : ( ( rule__TurnCCW__Group__0 ) ) ;
    public final void ruleTurnCCW() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:718:2: ( ( ( rule__TurnCCW__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:719:1: ( ( rule__TurnCCW__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:719:1: ( ( rule__TurnCCW__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:720:1: ( rule__TurnCCW__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTurnCCWAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:721:1: ( rule__TurnCCW__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:721:2: rule__TurnCCW__Group__0
            {
            pushFollow(FOLLOW_rule__TurnCCW__Group__0_in_ruleTurnCCW1480);
            rule__TurnCCW__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTurnCCWAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTurnCCW"


    // $ANTLR start "entryRulePointDirection"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:733:1: entryRulePointDirection : rulePointDirection EOF ;
    public final void entryRulePointDirection() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:734:1: ( rulePointDirection EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:735:1: rulePointDirection EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPointDirectionRule()); 
            }
            pushFollow(FOLLOW_rulePointDirection_in_entryRulePointDirection1507);
            rulePointDirection();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPointDirectionRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePointDirection1514); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePointDirection"


    // $ANTLR start "rulePointDirection"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:742:1: rulePointDirection : ( ( rule__PointDirection__Group__0 ) ) ;
    public final void rulePointDirection() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:746:2: ( ( ( rule__PointDirection__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:747:1: ( ( rule__PointDirection__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:747:1: ( ( rule__PointDirection__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:748:1: ( rule__PointDirection__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPointDirectionAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:749:1: ( rule__PointDirection__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:749:2: rule__PointDirection__Group__0
            {
            pushFollow(FOLLOW_rule__PointDirection__Group__0_in_rulePointDirection1540);
            rule__PointDirection__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPointDirectionAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePointDirection"


    // $ANTLR start "entryRulePointTowards"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:761:1: entryRulePointTowards : rulePointTowards EOF ;
    public final void entryRulePointTowards() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:762:1: ( rulePointTowards EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:763:1: rulePointTowards EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPointTowardsRule()); 
            }
            pushFollow(FOLLOW_rulePointTowards_in_entryRulePointTowards1567);
            rulePointTowards();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPointTowardsRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePointTowards1574); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePointTowards"


    // $ANTLR start "rulePointTowards"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:770:1: rulePointTowards : ( ( rule__PointTowards__Group__0 ) ) ;
    public final void rulePointTowards() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:774:2: ( ( ( rule__PointTowards__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:775:1: ( ( rule__PointTowards__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:775:1: ( ( rule__PointTowards__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:776:1: ( rule__PointTowards__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPointTowardsAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:777:1: ( rule__PointTowards__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:777:2: rule__PointTowards__Group__0
            {
            pushFollow(FOLLOW_rule__PointTowards__Group__0_in_rulePointTowards1600);
            rule__PointTowards__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPointTowardsAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePointTowards"


    // $ANTLR start "entryRuleGoToXY"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:789:1: entryRuleGoToXY : ruleGoToXY EOF ;
    public final void entryRuleGoToXY() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:790:1: ( ruleGoToXY EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:791:1: ruleGoToXY EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoToXYRule()); 
            }
            pushFollow(FOLLOW_ruleGoToXY_in_entryRuleGoToXY1627);
            ruleGoToXY();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoToXYRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGoToXY1634); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGoToXY"


    // $ANTLR start "ruleGoToXY"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:798:1: ruleGoToXY : ( ( rule__GoToXY__Group__0 ) ) ;
    public final void ruleGoToXY() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:802:2: ( ( ( rule__GoToXY__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:803:1: ( ( rule__GoToXY__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:803:1: ( ( rule__GoToXY__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:804:1: ( rule__GoToXY__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoToXYAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:805:1: ( rule__GoToXY__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:805:2: rule__GoToXY__Group__0
            {
            pushFollow(FOLLOW_rule__GoToXY__Group__0_in_ruleGoToXY1660);
            rule__GoToXY__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoToXYAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGoToXY"


    // $ANTLR start "entryRuleGoTo"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:817:1: entryRuleGoTo : ruleGoTo EOF ;
    public final void entryRuleGoTo() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:818:1: ( ruleGoTo EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:819:1: ruleGoTo EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoToRule()); 
            }
            pushFollow(FOLLOW_ruleGoTo_in_entryRuleGoTo1687);
            ruleGoTo();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoToRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGoTo1694); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGoTo"


    // $ANTLR start "ruleGoTo"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:826:1: ruleGoTo : ( ( rule__GoTo__Group__0 ) ) ;
    public final void ruleGoTo() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:830:2: ( ( ( rule__GoTo__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:831:1: ( ( rule__GoTo__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:831:1: ( ( rule__GoTo__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:832:1: ( rule__GoTo__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoToAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:833:1: ( rule__GoTo__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:833:2: rule__GoTo__Group__0
            {
            pushFollow(FOLLOW_rule__GoTo__Group__0_in_ruleGoTo1720);
            rule__GoTo__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoToAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGoTo"


    // $ANTLR start "entryRuleGlide"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:845:1: entryRuleGlide : ruleGlide EOF ;
    public final void entryRuleGlide() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:846:1: ( ruleGlide EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:847:1: ruleGlide EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGlideRule()); 
            }
            pushFollow(FOLLOW_ruleGlide_in_entryRuleGlide1747);
            ruleGlide();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGlideRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGlide1754); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGlide"


    // $ANTLR start "ruleGlide"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:854:1: ruleGlide : ( ( rule__Glide__Group__0 ) ) ;
    public final void ruleGlide() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:858:2: ( ( ( rule__Glide__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:859:1: ( ( rule__Glide__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:859:1: ( ( rule__Glide__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:860:1: ( rule__Glide__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGlideAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:861:1: ( rule__Glide__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:861:2: rule__Glide__Group__0
            {
            pushFollow(FOLLOW_rule__Glide__Group__0_in_ruleGlide1780);
            rule__Glide__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGlideAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGlide"


    // $ANTLR start "entryRuleChangeX"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:873:1: entryRuleChangeX : ruleChangeX EOF ;
    public final void entryRuleChangeX() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:874:1: ( ruleChangeX EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:875:1: ruleChangeX EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getChangeXRule()); 
            }
            pushFollow(FOLLOW_ruleChangeX_in_entryRuleChangeX1807);
            ruleChangeX();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getChangeXRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleChangeX1814); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleChangeX"


    // $ANTLR start "ruleChangeX"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:882:1: ruleChangeX : ( ( rule__ChangeX__Group__0 ) ) ;
    public final void ruleChangeX() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:886:2: ( ( ( rule__ChangeX__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:887:1: ( ( rule__ChangeX__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:887:1: ( ( rule__ChangeX__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:888:1: ( rule__ChangeX__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getChangeXAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:889:1: ( rule__ChangeX__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:889:2: rule__ChangeX__Group__0
            {
            pushFollow(FOLLOW_rule__ChangeX__Group__0_in_ruleChangeX1840);
            rule__ChangeX__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getChangeXAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleChangeX"


    // $ANTLR start "entryRuleChangeY"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:901:1: entryRuleChangeY : ruleChangeY EOF ;
    public final void entryRuleChangeY() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:902:1: ( ruleChangeY EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:903:1: ruleChangeY EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getChangeYRule()); 
            }
            pushFollow(FOLLOW_ruleChangeY_in_entryRuleChangeY1867);
            ruleChangeY();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getChangeYRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleChangeY1874); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleChangeY"


    // $ANTLR start "ruleChangeY"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:910:1: ruleChangeY : ( ( rule__ChangeY__Group__0 ) ) ;
    public final void ruleChangeY() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:914:2: ( ( ( rule__ChangeY__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:915:1: ( ( rule__ChangeY__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:915:1: ( ( rule__ChangeY__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:916:1: ( rule__ChangeY__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getChangeYAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:917:1: ( rule__ChangeY__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:917:2: rule__ChangeY__Group__0
            {
            pushFollow(FOLLOW_rule__ChangeY__Group__0_in_ruleChangeY1900);
            rule__ChangeY__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getChangeYAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleChangeY"


    // $ANTLR start "entryRuleSetX"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:929:1: entryRuleSetX : ruleSetX EOF ;
    public final void entryRuleSetX() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:930:1: ( ruleSetX EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:931:1: ruleSetX EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSetXRule()); 
            }
            pushFollow(FOLLOW_ruleSetX_in_entryRuleSetX1927);
            ruleSetX();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSetXRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSetX1934); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSetX"


    // $ANTLR start "ruleSetX"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:938:1: ruleSetX : ( ( rule__SetX__Group__0 ) ) ;
    public final void ruleSetX() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:942:2: ( ( ( rule__SetX__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:943:1: ( ( rule__SetX__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:943:1: ( ( rule__SetX__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:944:1: ( rule__SetX__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSetXAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:945:1: ( rule__SetX__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:945:2: rule__SetX__Group__0
            {
            pushFollow(FOLLOW_rule__SetX__Group__0_in_ruleSetX1960);
            rule__SetX__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSetXAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSetX"


    // $ANTLR start "entryRuleSetY"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:957:1: entryRuleSetY : ruleSetY EOF ;
    public final void entryRuleSetY() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:958:1: ( ruleSetY EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:959:1: ruleSetY EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSetYRule()); 
            }
            pushFollow(FOLLOW_ruleSetY_in_entryRuleSetY1987);
            ruleSetY();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSetYRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSetY1994); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSetY"


    // $ANTLR start "ruleSetY"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:966:1: ruleSetY : ( ( rule__SetY__Group__0 ) ) ;
    public final void ruleSetY() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:970:2: ( ( ( rule__SetY__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:971:1: ( ( rule__SetY__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:971:1: ( ( rule__SetY__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:972:1: ( rule__SetY__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSetYAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:973:1: ( rule__SetY__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:973:2: rule__SetY__Group__0
            {
            pushFollow(FOLLOW_rule__SetY__Group__0_in_ruleSetY2020);
            rule__SetY__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSetYAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSetY"


    // $ANTLR start "entryRuleIfEdge"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:985:1: entryRuleIfEdge : ruleIfEdge EOF ;
    public final void entryRuleIfEdge() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:986:1: ( ruleIfEdge EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:987:1: ruleIfEdge EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfEdgeRule()); 
            }
            pushFollow(FOLLOW_ruleIfEdge_in_entryRuleIfEdge2047);
            ruleIfEdge();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfEdgeRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleIfEdge2054); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIfEdge"


    // $ANTLR start "ruleIfEdge"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:994:1: ruleIfEdge : ( ( rule__IfEdge__Group__0 ) ) ;
    public final void ruleIfEdge() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:998:2: ( ( ( rule__IfEdge__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:999:1: ( ( rule__IfEdge__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:999:1: ( ( rule__IfEdge__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1000:1: ( rule__IfEdge__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfEdgeAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1001:1: ( rule__IfEdge__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1001:2: rule__IfEdge__Group__0
            {
            pushFollow(FOLLOW_rule__IfEdge__Group__0_in_ruleIfEdge2080);
            rule__IfEdge__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfEdgeAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIfEdge"


    // $ANTLR start "entryRuleNumber"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1013:1: entryRuleNumber : ruleNumber EOF ;
    public final void entryRuleNumber() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1014:1: ( ruleNumber EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1015:1: ruleNumber EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberRule()); 
            }
            pushFollow(FOLLOW_ruleNumber_in_entryRuleNumber2107);
            ruleNumber();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumber2114); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNumber"


    // $ANTLR start "ruleNumber"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1022:1: ruleNumber : ( ( rule__Number__Alternatives ) ) ;
    public final void ruleNumber() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1026:2: ( ( ( rule__Number__Alternatives ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1027:1: ( ( rule__Number__Alternatives ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1027:1: ( ( rule__Number__Alternatives ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1028:1: ( rule__Number__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberAccess().getAlternatives()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1029:1: ( rule__Number__Alternatives )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1029:2: rule__Number__Alternatives
            {
            pushFollow(FOLLOW_rule__Number__Alternatives_in_ruleNumber2140);
            rule__Number__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNumber"


    // $ANTLR start "entryRuleNumberTerminal"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1041:1: entryRuleNumberTerminal : ruleNumberTerminal EOF ;
    public final void entryRuleNumberTerminal() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1042:1: ( ruleNumberTerminal EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1043:1: ruleNumberTerminal EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberTerminalRule()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_entryRuleNumberTerminal2167);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberTerminalRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumberTerminal2174); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNumberTerminal"


    // $ANTLR start "ruleNumberTerminal"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1050:1: ruleNumberTerminal : ( ( rule__NumberTerminal__Group__0 ) ) ;
    public final void ruleNumberTerminal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1054:2: ( ( ( rule__NumberTerminal__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1055:1: ( ( rule__NumberTerminal__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1055:1: ( ( rule__NumberTerminal__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1056:1: ( rule__NumberTerminal__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberTerminalAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1057:1: ( rule__NumberTerminal__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1057:2: rule__NumberTerminal__Group__0
            {
            pushFollow(FOLLOW_rule__NumberTerminal__Group__0_in_ruleNumberTerminal2200);
            rule__NumberTerminal__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberTerminalAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNumberTerminal"


    // $ANTLR start "entryRuleNumberValue"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1069:1: entryRuleNumberValue : ruleNumberValue EOF ;
    public final void entryRuleNumberValue() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1070:1: ( ruleNumberValue EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1071:1: ruleNumberValue EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberValueRule()); 
            }
            pushFollow(FOLLOW_ruleNumberValue_in_entryRuleNumberValue2227);
            ruleNumberValue();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberValueRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumberValue2234); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNumberValue"


    // $ANTLR start "ruleNumberValue"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1078:1: ruleNumberValue : ( ( rule__NumberValue__ValueAssignment ) ) ;
    public final void ruleNumberValue() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1082:2: ( ( ( rule__NumberValue__ValueAssignment ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1083:1: ( ( rule__NumberValue__ValueAssignment ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1083:1: ( ( rule__NumberValue__ValueAssignment ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1084:1: ( rule__NumberValue__ValueAssignment )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberValueAccess().getValueAssignment()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1085:1: ( rule__NumberValue__ValueAssignment )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1085:2: rule__NumberValue__ValueAssignment
            {
            pushFollow(FOLLOW_rule__NumberValue__ValueAssignment_in_ruleNumberValue2260);
            rule__NumberValue__ValueAssignment();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberValueAccess().getValueAssignment()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNumberValue"


    // $ANTLR start "entryRuleNumberVar"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1097:1: entryRuleNumberVar : ruleNumberVar EOF ;
    public final void entryRuleNumberVar() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1098:1: ( ruleNumberVar EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1099:1: ruleNumberVar EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarRule()); 
            }
            pushFollow(FOLLOW_ruleNumberVar_in_entryRuleNumberVar2287);
            ruleNumberVar();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumberVar2294); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNumberVar"


    // $ANTLR start "ruleNumberVar"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1106:1: ruleNumberVar : ( ( rule__NumberVar__Group__0 ) ) ;
    public final void ruleNumberVar() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1110:2: ( ( ( rule__NumberVar__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1111:1: ( ( rule__NumberVar__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1111:1: ( ( rule__NumberVar__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1112:1: ( rule__NumberVar__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1113:1: ( rule__NumberVar__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1113:2: rule__NumberVar__Group__0
            {
            pushFollow(FOLLOW_rule__NumberVar__Group__0_in_ruleNumberVar2320);
            rule__NumberVar__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNumberVar"


    // $ANTLR start "entryRuleNumberVarX"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1125:1: entryRuleNumberVarX : ruleNumberVarX EOF ;
    public final void entryRuleNumberVarX() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1126:1: ( ruleNumberVarX EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1127:1: ruleNumberVarX EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarXRule()); 
            }
            pushFollow(FOLLOW_ruleNumberVarX_in_entryRuleNumberVarX2347);
            ruleNumberVarX();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarXRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumberVarX2354); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNumberVarX"


    // $ANTLR start "ruleNumberVarX"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1134:1: ruleNumberVarX : ( ( rule__NumberVarX__Group__0 ) ) ;
    public final void ruleNumberVarX() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1138:2: ( ( ( rule__NumberVarX__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1139:1: ( ( rule__NumberVarX__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1139:1: ( ( rule__NumberVarX__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1140:1: ( rule__NumberVarX__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarXAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1141:1: ( rule__NumberVarX__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1141:2: rule__NumberVarX__Group__0
            {
            pushFollow(FOLLOW_rule__NumberVarX__Group__0_in_ruleNumberVarX2380);
            rule__NumberVarX__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarXAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNumberVarX"


    // $ANTLR start "entryRuleNumberVarY"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1153:1: entryRuleNumberVarY : ruleNumberVarY EOF ;
    public final void entryRuleNumberVarY() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1154:1: ( ruleNumberVarY EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1155:1: ruleNumberVarY EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarYRule()); 
            }
            pushFollow(FOLLOW_ruleNumberVarY_in_entryRuleNumberVarY2407);
            ruleNumberVarY();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarYRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumberVarY2414); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNumberVarY"


    // $ANTLR start "ruleNumberVarY"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1162:1: ruleNumberVarY : ( ( rule__NumberVarY__Group__0 ) ) ;
    public final void ruleNumberVarY() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1166:2: ( ( ( rule__NumberVarY__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1167:1: ( ( rule__NumberVarY__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1167:1: ( ( rule__NumberVarY__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1168:1: ( rule__NumberVarY__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarYAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1169:1: ( rule__NumberVarY__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1169:2: rule__NumberVarY__Group__0
            {
            pushFollow(FOLLOW_rule__NumberVarY__Group__0_in_ruleNumberVarY2440);
            rule__NumberVarY__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarYAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNumberVarY"


    // $ANTLR start "entryRuleNumberVarD"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1181:1: entryRuleNumberVarD : ruleNumberVarD EOF ;
    public final void entryRuleNumberVarD() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1182:1: ( ruleNumberVarD EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1183:1: ruleNumberVarD EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarDRule()); 
            }
            pushFollow(FOLLOW_ruleNumberVarD_in_entryRuleNumberVarD2467);
            ruleNumberVarD();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarDRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumberVarD2474); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNumberVarD"


    // $ANTLR start "ruleNumberVarD"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1190:1: ruleNumberVarD : ( ( rule__NumberVarD__Group__0 ) ) ;
    public final void ruleNumberVarD() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1194:2: ( ( ( rule__NumberVarD__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1195:1: ( ( rule__NumberVarD__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1195:1: ( ( rule__NumberVarD__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1196:1: ( rule__NumberVarD__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarDAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1197:1: ( rule__NumberVarD__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1197:2: rule__NumberVarD__Group__0
            {
            pushFollow(FOLLOW_rule__NumberVarD__Group__0_in_ruleNumberVarD2500);
            rule__NumberVarD__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarDAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNumberVarD"


    // $ANTLR start "entryRuleNumberObjVar"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1209:1: entryRuleNumberObjVar : ruleNumberObjVar EOF ;
    public final void entryRuleNumberObjVar() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1210:1: ( ruleNumberObjVar EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1211:1: ruleNumberObjVar EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberObjVarRule()); 
            }
            pushFollow(FOLLOW_ruleNumberObjVar_in_entryRuleNumberObjVar2527);
            ruleNumberObjVar();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberObjVarRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNumberObjVar2534); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNumberObjVar"


    // $ANTLR start "ruleNumberObjVar"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1218:1: ruleNumberObjVar : ( ( rule__NumberObjVar__Group__0 ) ) ;
    public final void ruleNumberObjVar() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1222:2: ( ( ( rule__NumberObjVar__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1223:1: ( ( rule__NumberObjVar__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1223:1: ( ( rule__NumberObjVar__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1224:1: ( rule__NumberObjVar__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberObjVarAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1225:1: ( rule__NumberObjVar__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1225:2: rule__NumberObjVar__Group__0
            {
            pushFollow(FOLLOW_rule__NumberObjVar__Group__0_in_ruleNumberObjVar2560);
            rule__NumberObjVar__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberObjVarAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNumberObjVar"


    // $ANTLR start "entryRuleBinary"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1237:1: entryRuleBinary : ruleBinary EOF ;
    public final void entryRuleBinary() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1238:1: ( ruleBinary EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1239:1: ruleBinary EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryRule()); 
            }
            pushFollow(FOLLOW_ruleBinary_in_entryRuleBinary2587);
            ruleBinary();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBinary2594); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBinary"


    // $ANTLR start "ruleBinary"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1246:1: ruleBinary : ( ( rule__Binary__Alternatives ) ) ;
    public final void ruleBinary() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1250:2: ( ( ( rule__Binary__Alternatives ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1251:1: ( ( rule__Binary__Alternatives ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1251:1: ( ( rule__Binary__Alternatives ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1252:1: ( rule__Binary__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryAccess().getAlternatives()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1253:1: ( rule__Binary__Alternatives )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1253:2: rule__Binary__Alternatives
            {
            pushFollow(FOLLOW_rule__Binary__Alternatives_in_ruleBinary2620);
            rule__Binary__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinary"


    // $ANTLR start "entryRuleBinaryTerminal"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1265:1: entryRuleBinaryTerminal : ruleBinaryTerminal EOF ;
    public final void entryRuleBinaryTerminal() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1266:1: ( ruleBinaryTerminal EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1267:1: ruleBinaryTerminal EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryTerminalRule()); 
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_entryRuleBinaryTerminal2647);
            ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryTerminalRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBinaryTerminal2654); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBinaryTerminal"


    // $ANTLR start "ruleBinaryTerminal"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1274:1: ruleBinaryTerminal : ( ( rule__BinaryTerminal__Group__0 ) ) ;
    public final void ruleBinaryTerminal() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1278:2: ( ( ( rule__BinaryTerminal__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1279:1: ( ( rule__BinaryTerminal__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1279:1: ( ( rule__BinaryTerminal__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1280:1: ( rule__BinaryTerminal__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryTerminalAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1281:1: ( rule__BinaryTerminal__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1281:2: rule__BinaryTerminal__Group__0
            {
            pushFollow(FOLLOW_rule__BinaryTerminal__Group__0_in_ruleBinaryTerminal2680);
            rule__BinaryTerminal__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryTerminalAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinaryTerminal"


    // $ANTLR start "entryRuleBinaryVar"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1293:1: entryRuleBinaryVar : ruleBinaryVar EOF ;
    public final void entryRuleBinaryVar() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1294:1: ( ruleBinaryVar EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1295:1: ruleBinaryVar EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryVarRule()); 
            }
            pushFollow(FOLLOW_ruleBinaryVar_in_entryRuleBinaryVar2707);
            ruleBinaryVar();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryVarRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleBinaryVar2714); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBinaryVar"


    // $ANTLR start "ruleBinaryVar"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1302:1: ruleBinaryVar : ( ( rule__BinaryVar__Group__0 ) ) ;
    public final void ruleBinaryVar() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1306:2: ( ( ( rule__BinaryVar__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1307:1: ( ( rule__BinaryVar__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1307:1: ( ( rule__BinaryVar__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1308:1: ( rule__BinaryVar__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryVarAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1309:1: ( rule__BinaryVar__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1309:2: rule__BinaryVar__Group__0
            {
            pushFollow(FOLLOW_rule__BinaryVar__Group__0_in_ruleBinaryVar2740);
            rule__BinaryVar__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryVarAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinaryVar"


    // $ANTLR start "entryRuleTouchsColor"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1321:1: entryRuleTouchsColor : ruleTouchsColor EOF ;
    public final void entryRuleTouchsColor() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1322:1: ( ruleTouchsColor EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1323:1: ruleTouchsColor EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTouchsColorRule()); 
            }
            pushFollow(FOLLOW_ruleTouchsColor_in_entryRuleTouchsColor2767);
            ruleTouchsColor();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTouchsColorRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleTouchsColor2774); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTouchsColor"


    // $ANTLR start "ruleTouchsColor"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1330:1: ruleTouchsColor : ( ( rule__TouchsColor__Group__0 ) ) ;
    public final void ruleTouchsColor() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1334:2: ( ( ( rule__TouchsColor__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1335:1: ( ( rule__TouchsColor__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1335:1: ( ( rule__TouchsColor__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1336:1: ( rule__TouchsColor__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTouchsColorAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1337:1: ( rule__TouchsColor__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1337:2: rule__TouchsColor__Group__0
            {
            pushFollow(FOLLOW_rule__TouchsColor__Group__0_in_ruleTouchsColor2800);
            rule__TouchsColor__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTouchsColorAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTouchsColor"


    // $ANTLR start "entryRuleLt"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1349:1: entryRuleLt : ruleLt EOF ;
    public final void entryRuleLt() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1350:1: ( ruleLt EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1351:1: ruleLt EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLtRule()); 
            }
            pushFollow(FOLLOW_ruleLt_in_entryRuleLt2827);
            ruleLt();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLtRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleLt2834); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLt"


    // $ANTLR start "ruleLt"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1358:1: ruleLt : ( ( rule__Lt__Group__0 ) ) ;
    public final void ruleLt() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1362:2: ( ( ( rule__Lt__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1363:1: ( ( rule__Lt__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1363:1: ( ( rule__Lt__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1364:1: ( rule__Lt__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLtAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1365:1: ( rule__Lt__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1365:2: rule__Lt__Group__0
            {
            pushFollow(FOLLOW_rule__Lt__Group__0_in_ruleLt2860);
            rule__Lt__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLtAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLt"


    // $ANTLR start "entryRuleGt"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1377:1: entryRuleGt : ruleGt EOF ;
    public final void entryRuleGt() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1378:1: ( ruleGt EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1379:1: ruleGt EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGtRule()); 
            }
            pushFollow(FOLLOW_ruleGt_in_entryRuleGt2887);
            ruleGt();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGtRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleGt2894); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGt"


    // $ANTLR start "ruleGt"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1386:1: ruleGt : ( ( rule__Gt__Group__0 ) ) ;
    public final void ruleGt() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1390:2: ( ( ( rule__Gt__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1391:1: ( ( rule__Gt__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1391:1: ( ( rule__Gt__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1392:1: ( rule__Gt__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGtAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1393:1: ( rule__Gt__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1393:2: rule__Gt__Group__0
            {
            pushFollow(FOLLOW_rule__Gt__Group__0_in_ruleGt2920);
            rule__Gt__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGtAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGt"


    // $ANTLR start "entryRuleEq"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1405:1: entryRuleEq : ruleEq EOF ;
    public final void entryRuleEq() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1406:1: ( ruleEq EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1407:1: ruleEq EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqRule()); 
            }
            pushFollow(FOLLOW_ruleEq_in_entryRuleEq2947);
            ruleEq();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleEq2954); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEq"


    // $ANTLR start "ruleEq"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1414:1: ruleEq : ( ( rule__Eq__Group__0 ) ) ;
    public final void ruleEq() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1418:2: ( ( ( rule__Eq__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1419:1: ( ( rule__Eq__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1419:1: ( ( rule__Eq__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1420:1: ( rule__Eq__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1421:1: ( rule__Eq__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1421:2: rule__Eq__Group__0
            {
            pushFollow(FOLLOW_rule__Eq__Group__0_in_ruleEq2980);
            rule__Eq__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEq"


    // $ANTLR start "entryRuleAnd"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1433:1: entryRuleAnd : ruleAnd EOF ;
    public final void entryRuleAnd() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1434:1: ( ruleAnd EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1435:1: ruleAnd EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAndRule()); 
            }
            pushFollow(FOLLOW_ruleAnd_in_entryRuleAnd3007);
            ruleAnd();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAndRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleAnd3014); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAnd"


    // $ANTLR start "ruleAnd"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1442:1: ruleAnd : ( ( rule__And__Group__0 ) ) ;
    public final void ruleAnd() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1446:2: ( ( ( rule__And__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1447:1: ( ( rule__And__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1447:1: ( ( rule__And__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1448:1: ( rule__And__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAndAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1449:1: ( rule__And__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1449:2: rule__And__Group__0
            {
            pushFollow(FOLLOW_rule__And__Group__0_in_ruleAnd3040);
            rule__And__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAndAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAnd"


    // $ANTLR start "entryRuleOr"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1461:1: entryRuleOr : ruleOr EOF ;
    public final void entryRuleOr() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1462:1: ( ruleOr EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1463:1: ruleOr EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOrRule()); 
            }
            pushFollow(FOLLOW_ruleOr_in_entryRuleOr3067);
            ruleOr();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getOrRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleOr3074); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOr"


    // $ANTLR start "ruleOr"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1470:1: ruleOr : ( ( rule__Or__Group__0 ) ) ;
    public final void ruleOr() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1474:2: ( ( ( rule__Or__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1475:1: ( ( rule__Or__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1475:1: ( ( rule__Or__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1476:1: ( rule__Or__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOrAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1477:1: ( rule__Or__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1477:2: rule__Or__Group__0
            {
            pushFollow(FOLLOW_rule__Or__Group__0_in_ruleOr3100);
            rule__Or__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getOrAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOr"


    // $ANTLR start "entryRuleNot"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1489:1: entryRuleNot : ruleNot EOF ;
    public final void entryRuleNot() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1490:1: ( ruleNot EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1491:1: ruleNot EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNotRule()); 
            }
            pushFollow(FOLLOW_ruleNot_in_entryRuleNot3127);
            ruleNot();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNotRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleNot3134); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNot"


    // $ANTLR start "ruleNot"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1498:1: ruleNot : ( ( rule__Not__Group__0 ) ) ;
    public final void ruleNot() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1502:2: ( ( ( rule__Not__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1503:1: ( ( rule__Not__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1503:1: ( ( rule__Not__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1504:1: ( rule__Not__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNotAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1505:1: ( rule__Not__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1505:2: rule__Not__Group__0
            {
            pushFollow(FOLLOW_rule__Not__Group__0_in_ruleNot3160);
            rule__Not__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNotAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNot"


    // $ANTLR start "entryRuleHide"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1517:1: entryRuleHide : ruleHide EOF ;
    public final void entryRuleHide() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1518:1: ( ruleHide EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1519:1: ruleHide EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getHideRule()); 
            }
            pushFollow(FOLLOW_ruleHide_in_entryRuleHide3187);
            ruleHide();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getHideRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleHide3194); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleHide"


    // $ANTLR start "ruleHide"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1526:1: ruleHide : ( ( rule__Hide__Group__0 ) ) ;
    public final void ruleHide() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1530:2: ( ( ( rule__Hide__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1531:1: ( ( rule__Hide__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1531:1: ( ( rule__Hide__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1532:1: ( rule__Hide__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getHideAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1533:1: ( rule__Hide__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1533:2: rule__Hide__Group__0
            {
            pushFollow(FOLLOW_rule__Hide__Group__0_in_ruleHide3220);
            rule__Hide__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getHideAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleHide"


    // $ANTLR start "entryRuleShow"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1545:1: entryRuleShow : ruleShow EOF ;
    public final void entryRuleShow() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1546:1: ( ruleShow EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1547:1: ruleShow EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getShowRule()); 
            }
            pushFollow(FOLLOW_ruleShow_in_entryRuleShow3247);
            ruleShow();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getShowRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleShow3254); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleShow"


    // $ANTLR start "ruleShow"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1554:1: ruleShow : ( ( rule__Show__Group__0 ) ) ;
    public final void ruleShow() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1558:2: ( ( ( rule__Show__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1559:1: ( ( rule__Show__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1559:1: ( ( rule__Show__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1560:1: ( rule__Show__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getShowAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1561:1: ( rule__Show__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1561:2: rule__Show__Group__0
            {
            pushFollow(FOLLOW_rule__Show__Group__0_in_ruleShow3280);
            rule__Show__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getShowAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleShow"


    // $ANTLR start "entryRuleSay"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1573:1: entryRuleSay : ruleSay EOF ;
    public final void entryRuleSay() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1574:1: ( ruleSay EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1575:1: ruleSay EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSayRule()); 
            }
            pushFollow(FOLLOW_ruleSay_in_entryRuleSay3307);
            ruleSay();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSayRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRuleSay3314); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSay"


    // $ANTLR start "ruleSay"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1582:1: ruleSay : ( ( rule__Say__Group__0 ) ) ;
    public final void ruleSay() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1586:2: ( ( ( rule__Say__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1587:1: ( ( rule__Say__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1587:1: ( ( rule__Say__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1588:1: ( rule__Say__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSayAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1589:1: ( rule__Say__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1589:2: rule__Say__Group__0
            {
            pushFollow(FOLLOW_rule__Say__Group__0_in_ruleSay3340);
            rule__Say__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSayAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSay"


    // $ANTLR start "entryRulePlay"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1601:1: entryRulePlay : rulePlay EOF ;
    public final void entryRulePlay() throws RecognitionException {
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1602:1: ( rulePlay EOF )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1603:1: rulePlay EOF
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPlayRule()); 
            }
            pushFollow(FOLLOW_rulePlay_in_entryRulePlay3367);
            rulePlay();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPlayRule()); 
            }
            match(input,EOF,FOLLOW_EOF_in_entryRulePlay3374); if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePlay"


    // $ANTLR start "rulePlay"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1610:1: rulePlay : ( ( rule__Play__Group__0 ) ) ;
    public final void rulePlay() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1614:2: ( ( ( rule__Play__Group__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1615:1: ( ( rule__Play__Group__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1615:1: ( ( rule__Play__Group__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1616:1: ( rule__Play__Group__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPlayAccess().getGroup()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1617:1: ( rule__Play__Group__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1617:2: rule__Play__Group__0
            {
            pushFollow(FOLLOW_rule__Play__Group__0_in_rulePlay3400);
            rule__Play__Group__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPlayAccess().getGroup()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePlay"


    // $ANTLR start "ruleObjectPredefEnum"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1630:1: ruleObjectPredefEnum : ( ( 'mouse-pointer' ) ) ;
    public final void ruleObjectPredefEnum() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1634:1: ( ( ( 'mouse-pointer' ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1635:1: ( ( 'mouse-pointer' ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1635:1: ( ( 'mouse-pointer' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1636:1: ( 'mouse-pointer' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getObjectPredefEnumAccess().getMousepointerEnumLiteralDeclaration()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1637:1: ( 'mouse-pointer' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1637:3: 'mouse-pointer'
            {
            match(input,11,FOLLOW_11_in_ruleObjectPredefEnum3438); if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getObjectPredefEnumAccess().getMousepointerEnumLiteralDeclaration()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleObjectPredefEnum"


    // $ANTLR start "ruleObjectVariableEnum"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1650:1: ruleObjectVariableEnum : ( ( rule__ObjectVariableEnum__Alternatives ) ) ;
    public final void ruleObjectVariableEnum() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1654:1: ( ( ( rule__ObjectVariableEnum__Alternatives ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1655:1: ( ( rule__ObjectVariableEnum__Alternatives ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1655:1: ( ( rule__ObjectVariableEnum__Alternatives ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1656:1: ( rule__ObjectVariableEnum__Alternatives )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getObjectVariableEnumAccess().getAlternatives()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1657:1: ( rule__ObjectVariableEnum__Alternatives )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1657:2: rule__ObjectVariableEnum__Alternatives
            {
            pushFollow(FOLLOW_rule__ObjectVariableEnum__Alternatives_in_ruleObjectVariableEnum3476);
            rule__ObjectVariableEnum__Alternatives();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getObjectVariableEnumAccess().getAlternatives()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleObjectVariableEnum"


    // $ANTLR start "rule__IObject__Alternatives"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1668:1: rule__IObject__Alternatives : ( ( ruleRef ) | ( rulePredef ) );
    public final void rule__IObject__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1672:1: ( ( ruleRef ) | ( rulePredef ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_ID) ) {
                alt1=1;
            }
            else if ( (LA1_0==11) ) {
                alt1=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1673:1: ( ruleRef )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1673:1: ( ruleRef )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1674:1: ruleRef
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getIObjectAccess().getRefParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_ruleRef_in_rule__IObject__Alternatives3511);
                    ruleRef();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getIObjectAccess().getRefParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1679:6: ( rulePredef )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1679:6: ( rulePredef )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1680:1: rulePredef
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getIObjectAccess().getPredefParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_rulePredef_in_rule__IObject__Alternatives3528);
                    rulePredef();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getIObjectAccess().getPredefParserRuleCall_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IObject__Alternatives"


    // $ANTLR start "rule__When__Alternatives_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1690:1: rule__When__Alternatives_1 : ( ( ruleWhenFlag ) | ( ruleWhenMsg ) );
    public final void rule__When__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1694:1: ( ( ruleWhenFlag ) | ( ruleWhenMsg ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==20) ) {
                alt2=1;
            }
            else if ( (LA2_0==21) ) {
                alt2=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1695:1: ( ruleWhenFlag )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1695:1: ( ruleWhenFlag )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1696:1: ruleWhenFlag
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getWhenAccess().getWhenFlagParserRuleCall_1_0()); 
                    }
                    pushFollow(FOLLOW_ruleWhenFlag_in_rule__When__Alternatives_13560);
                    ruleWhenFlag();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getWhenAccess().getWhenFlagParserRuleCall_1_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1701:6: ( ruleWhenMsg )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1701:6: ( ruleWhenMsg )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1702:1: ruleWhenMsg
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getWhenAccess().getWhenMsgParserRuleCall_1_1()); 
                    }
                    pushFollow(FOLLOW_ruleWhenMsg_in_rule__When__Alternatives_13577);
                    ruleWhenMsg();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getWhenAccess().getWhenMsgParserRuleCall_1_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__When__Alternatives_1"


    // $ANTLR start "rule__Block__Alternatives"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1712:1: rule__Block__Alternatives : ( ( ruleStop ) | ( ruleBroadcast ) | ( ruleWait ) | ( ruleIf ) | ( ruleLoop ) | ( ruleMove ) | ( ruleTurnCW ) | ( ruleTurnCCW ) | ( rulePointDirection ) | ( rulePointTowards ) | ( ruleGoToXY ) | ( ruleGoTo ) | ( ruleGlide ) | ( ruleChangeX ) | ( ruleChangeY ) | ( ruleSetX ) | ( ruleSetY ) | ( ruleIfEdge ) | ( ruleHide ) | ( ruleShow ) | ( ruleSay ) | ( rulePlay ) );
    public final void rule__Block__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1716:1: ( ( ruleStop ) | ( ruleBroadcast ) | ( ruleWait ) | ( ruleIf ) | ( ruleLoop ) | ( ruleMove ) | ( ruleTurnCW ) | ( ruleTurnCCW ) | ( rulePointDirection ) | ( rulePointTowards ) | ( ruleGoToXY ) | ( ruleGoTo ) | ( ruleGlide ) | ( ruleChangeX ) | ( ruleChangeY ) | ( ruleSetX ) | ( ruleSetY ) | ( ruleIfEdge ) | ( ruleHide ) | ( ruleShow ) | ( ruleSay ) | ( rulePlay ) )
            int alt3=22;
            alt3 = dfa3.predict(input);
            switch (alt3) {
                case 1 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1717:1: ( ruleStop )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1717:1: ( ruleStop )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1718:1: ruleStop
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getStopParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_ruleStop_in_rule__Block__Alternatives3609);
                    ruleStop();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getStopParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1723:6: ( ruleBroadcast )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1723:6: ( ruleBroadcast )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1724:1: ruleBroadcast
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getBroadcastParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_ruleBroadcast_in_rule__Block__Alternatives3626);
                    ruleBroadcast();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getBroadcastParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1729:6: ( ruleWait )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1729:6: ( ruleWait )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1730:1: ruleWait
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getWaitParserRuleCall_2()); 
                    }
                    pushFollow(FOLLOW_ruleWait_in_rule__Block__Alternatives3643);
                    ruleWait();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getWaitParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1735:6: ( ruleIf )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1735:6: ( ruleIf )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1736:1: ruleIf
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getIfParserRuleCall_3()); 
                    }
                    pushFollow(FOLLOW_ruleIf_in_rule__Block__Alternatives3660);
                    ruleIf();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getIfParserRuleCall_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1741:6: ( ruleLoop )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1741:6: ( ruleLoop )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1742:1: ruleLoop
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getLoopParserRuleCall_4()); 
                    }
                    pushFollow(FOLLOW_ruleLoop_in_rule__Block__Alternatives3677);
                    ruleLoop();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getLoopParserRuleCall_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1747:6: ( ruleMove )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1747:6: ( ruleMove )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1748:1: ruleMove
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getMoveParserRuleCall_5()); 
                    }
                    pushFollow(FOLLOW_ruleMove_in_rule__Block__Alternatives3694);
                    ruleMove();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getMoveParserRuleCall_5()); 
                    }

                    }


                    }
                    break;
                case 7 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1753:6: ( ruleTurnCW )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1753:6: ( ruleTurnCW )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1754:1: ruleTurnCW
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getTurnCWParserRuleCall_6()); 
                    }
                    pushFollow(FOLLOW_ruleTurnCW_in_rule__Block__Alternatives3711);
                    ruleTurnCW();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getTurnCWParserRuleCall_6()); 
                    }

                    }


                    }
                    break;
                case 8 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1759:6: ( ruleTurnCCW )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1759:6: ( ruleTurnCCW )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1760:1: ruleTurnCCW
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getTurnCCWParserRuleCall_7()); 
                    }
                    pushFollow(FOLLOW_ruleTurnCCW_in_rule__Block__Alternatives3728);
                    ruleTurnCCW();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getTurnCCWParserRuleCall_7()); 
                    }

                    }


                    }
                    break;
                case 9 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1765:6: ( rulePointDirection )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1765:6: ( rulePointDirection )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1766:1: rulePointDirection
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getPointDirectionParserRuleCall_8()); 
                    }
                    pushFollow(FOLLOW_rulePointDirection_in_rule__Block__Alternatives3745);
                    rulePointDirection();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getPointDirectionParserRuleCall_8()); 
                    }

                    }


                    }
                    break;
                case 10 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1771:6: ( rulePointTowards )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1771:6: ( rulePointTowards )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1772:1: rulePointTowards
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getPointTowardsParserRuleCall_9()); 
                    }
                    pushFollow(FOLLOW_rulePointTowards_in_rule__Block__Alternatives3762);
                    rulePointTowards();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getPointTowardsParserRuleCall_9()); 
                    }

                    }


                    }
                    break;
                case 11 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1777:6: ( ruleGoToXY )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1777:6: ( ruleGoToXY )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1778:1: ruleGoToXY
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getGoToXYParserRuleCall_10()); 
                    }
                    pushFollow(FOLLOW_ruleGoToXY_in_rule__Block__Alternatives3779);
                    ruleGoToXY();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getGoToXYParserRuleCall_10()); 
                    }

                    }


                    }
                    break;
                case 12 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1783:6: ( ruleGoTo )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1783:6: ( ruleGoTo )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1784:1: ruleGoTo
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getGoToParserRuleCall_11()); 
                    }
                    pushFollow(FOLLOW_ruleGoTo_in_rule__Block__Alternatives3796);
                    ruleGoTo();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getGoToParserRuleCall_11()); 
                    }

                    }


                    }
                    break;
                case 13 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1789:6: ( ruleGlide )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1789:6: ( ruleGlide )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1790:1: ruleGlide
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getGlideParserRuleCall_12()); 
                    }
                    pushFollow(FOLLOW_ruleGlide_in_rule__Block__Alternatives3813);
                    ruleGlide();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getGlideParserRuleCall_12()); 
                    }

                    }


                    }
                    break;
                case 14 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1795:6: ( ruleChangeX )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1795:6: ( ruleChangeX )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1796:1: ruleChangeX
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getChangeXParserRuleCall_13()); 
                    }
                    pushFollow(FOLLOW_ruleChangeX_in_rule__Block__Alternatives3830);
                    ruleChangeX();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getChangeXParserRuleCall_13()); 
                    }

                    }


                    }
                    break;
                case 15 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1801:6: ( ruleChangeY )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1801:6: ( ruleChangeY )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1802:1: ruleChangeY
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getChangeYParserRuleCall_14()); 
                    }
                    pushFollow(FOLLOW_ruleChangeY_in_rule__Block__Alternatives3847);
                    ruleChangeY();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getChangeYParserRuleCall_14()); 
                    }

                    }


                    }
                    break;
                case 16 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1807:6: ( ruleSetX )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1807:6: ( ruleSetX )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1808:1: ruleSetX
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getSetXParserRuleCall_15()); 
                    }
                    pushFollow(FOLLOW_ruleSetX_in_rule__Block__Alternatives3864);
                    ruleSetX();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getSetXParserRuleCall_15()); 
                    }

                    }


                    }
                    break;
                case 17 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1813:6: ( ruleSetY )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1813:6: ( ruleSetY )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1814:1: ruleSetY
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getSetYParserRuleCall_16()); 
                    }
                    pushFollow(FOLLOW_ruleSetY_in_rule__Block__Alternatives3881);
                    ruleSetY();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getSetYParserRuleCall_16()); 
                    }

                    }


                    }
                    break;
                case 18 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1819:6: ( ruleIfEdge )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1819:6: ( ruleIfEdge )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1820:1: ruleIfEdge
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getIfEdgeParserRuleCall_17()); 
                    }
                    pushFollow(FOLLOW_ruleIfEdge_in_rule__Block__Alternatives3898);
                    ruleIfEdge();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getIfEdgeParserRuleCall_17()); 
                    }

                    }


                    }
                    break;
                case 19 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1825:6: ( ruleHide )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1825:6: ( ruleHide )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1826:1: ruleHide
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getHideParserRuleCall_18()); 
                    }
                    pushFollow(FOLLOW_ruleHide_in_rule__Block__Alternatives3915);
                    ruleHide();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getHideParserRuleCall_18()); 
                    }

                    }


                    }
                    break;
                case 20 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1831:6: ( ruleShow )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1831:6: ( ruleShow )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1832:1: ruleShow
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getShowParserRuleCall_19()); 
                    }
                    pushFollow(FOLLOW_ruleShow_in_rule__Block__Alternatives3932);
                    ruleShow();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getShowParserRuleCall_19()); 
                    }

                    }


                    }
                    break;
                case 21 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1837:6: ( ruleSay )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1837:6: ( ruleSay )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1838:1: ruleSay
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getSayParserRuleCall_20()); 
                    }
                    pushFollow(FOLLOW_ruleSay_in_rule__Block__Alternatives3949);
                    ruleSay();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getSayParserRuleCall_20()); 
                    }

                    }


                    }
                    break;
                case 22 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1843:6: ( rulePlay )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1843:6: ( rulePlay )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1844:1: rulePlay
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBlockAccess().getPlayParserRuleCall_21()); 
                    }
                    pushFollow(FOLLOW_rulePlay_in_rule__Block__Alternatives3966);
                    rulePlay();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBlockAccess().getPlayParserRuleCall_21()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Block__Alternatives"


    // $ANTLR start "rule__Loop__Alternatives_0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1854:1: rule__Loop__Alternatives_0 : ( ( ruleForever ) | ( ruleRepeatN ) | ( ruleRepeatUntil ) );
    public final void rule__Loop__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1858:1: ( ( ruleForever ) | ( ruleRepeatN ) | ( ruleRepeatUntil ) )
            int alt4=3;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==22) ) {
                alt4=1;
            }
            else if ( (LA4_0==24) ) {
                int LA4_2 = input.LA(2);

                if ( (LA4_2==48) ) {
                    alt4=2;
                }
                else if ( (LA4_2==25) ) {
                    alt4=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return ;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 2, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1859:1: ( ruleForever )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1859:1: ( ruleForever )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1860:1: ruleForever
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getLoopAccess().getForeverParserRuleCall_0_0()); 
                    }
                    pushFollow(FOLLOW_ruleForever_in_rule__Loop__Alternatives_03998);
                    ruleForever();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getLoopAccess().getForeverParserRuleCall_0_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1865:6: ( ruleRepeatN )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1865:6: ( ruleRepeatN )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1866:1: ruleRepeatN
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getLoopAccess().getRepeatNParserRuleCall_0_1()); 
                    }
                    pushFollow(FOLLOW_ruleRepeatN_in_rule__Loop__Alternatives_04015);
                    ruleRepeatN();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getLoopAccess().getRepeatNParserRuleCall_0_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1871:6: ( ruleRepeatUntil )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1871:6: ( ruleRepeatUntil )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1872:1: ruleRepeatUntil
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getLoopAccess().getRepeatUntilParserRuleCall_0_2()); 
                    }
                    pushFollow(FOLLOW_ruleRepeatUntil_in_rule__Loop__Alternatives_04032);
                    ruleRepeatUntil();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getLoopAccess().getRepeatUntilParserRuleCall_0_2()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__Alternatives_0"


    // $ANTLR start "rule__Stop__Alternatives_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1882:1: rule__Stop__Alternatives_1 : ( ( ( rule__Stop__AllAssignment_1_0 ) ) | ( ( rule__Stop__ScriptAssignment_1_1 ) ) );
    public final void rule__Stop__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1886:1: ( ( ( rule__Stop__AllAssignment_1_0 ) ) | ( ( rule__Stop__ScriptAssignment_1_1 ) ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==67) ) {
                alt5=1;
            }
            else if ( (LA5_0==17) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1887:1: ( ( rule__Stop__AllAssignment_1_0 ) )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1887:1: ( ( rule__Stop__AllAssignment_1_0 ) )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1888:1: ( rule__Stop__AllAssignment_1_0 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getStopAccess().getAllAssignment_1_0()); 
                    }
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1889:1: ( rule__Stop__AllAssignment_1_0 )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1889:2: rule__Stop__AllAssignment_1_0
                    {
                    pushFollow(FOLLOW_rule__Stop__AllAssignment_1_0_in_rule__Stop__Alternatives_14064);
                    rule__Stop__AllAssignment_1_0();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getStopAccess().getAllAssignment_1_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1893:6: ( ( rule__Stop__ScriptAssignment_1_1 ) )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1893:6: ( ( rule__Stop__ScriptAssignment_1_1 ) )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1894:1: ( rule__Stop__ScriptAssignment_1_1 )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getStopAccess().getScriptAssignment_1_1()); 
                    }
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1895:1: ( rule__Stop__ScriptAssignment_1_1 )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1895:2: rule__Stop__ScriptAssignment_1_1
                    {
                    pushFollow(FOLLOW_rule__Stop__ScriptAssignment_1_1_in_rule__Stop__Alternatives_14082);
                    rule__Stop__ScriptAssignment_1_1();

                    state._fsp--;
                    if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getStopAccess().getScriptAssignment_1_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stop__Alternatives_1"


    // $ANTLR start "rule__Wait__Alternatives_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1904:1: rule__Wait__Alternatives_1 : ( ( ruleWaitDelay ) | ( ruleWaitUntil ) );
    public final void rule__Wait__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1908:1: ( ( ruleWaitDelay ) | ( ruleWaitUntil ) )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==48) ) {
                alt6=1;
            }
            else if ( (LA6_0==25) ) {
                alt6=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1909:1: ( ruleWaitDelay )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1909:1: ( ruleWaitDelay )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1910:1: ruleWaitDelay
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getWaitAccess().getWaitDelayParserRuleCall_1_0()); 
                    }
                    pushFollow(FOLLOW_ruleWaitDelay_in_rule__Wait__Alternatives_14115);
                    ruleWaitDelay();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getWaitAccess().getWaitDelayParserRuleCall_1_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1915:6: ( ruleWaitUntil )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1915:6: ( ruleWaitUntil )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1916:1: ruleWaitUntil
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getWaitAccess().getWaitUntilParserRuleCall_1_1()); 
                    }
                    pushFollow(FOLLOW_ruleWaitUntil_in_rule__Wait__Alternatives_14132);
                    ruleWaitUntil();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getWaitAccess().getWaitUntilParserRuleCall_1_1()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Wait__Alternatives_1"


    // $ANTLR start "rule__Number__Alternatives"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1926:1: rule__Number__Alternatives : ( ( ruleNumberValue ) | ( ruleNumberObjVar ) | ( ruleNumberVar ) | ( ruleNumberVarX ) | ( ruleNumberVarY ) | ( ruleNumberVarD ) );
    public final void rule__Number__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1930:1: ( ( ruleNumberValue ) | ( ruleNumberObjVar ) | ( ruleNumberVar ) | ( ruleNumberVarX ) | ( ruleNumberVarY ) | ( ruleNumberVarD ) )
            int alt7=6;
            alt7 = dfa7.predict(input);
            switch (alt7) {
                case 1 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1931:1: ( ruleNumberValue )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1931:1: ( ruleNumberValue )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1932:1: ruleNumberValue
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNumberAccess().getNumberValueParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_ruleNumberValue_in_rule__Number__Alternatives4164);
                    ruleNumberValue();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNumberAccess().getNumberValueParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1937:6: ( ruleNumberObjVar )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1937:6: ( ruleNumberObjVar )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1938:1: ruleNumberObjVar
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNumberAccess().getNumberObjVarParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_ruleNumberObjVar_in_rule__Number__Alternatives4181);
                    ruleNumberObjVar();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNumberAccess().getNumberObjVarParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1943:6: ( ruleNumberVar )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1943:6: ( ruleNumberVar )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1944:1: ruleNumberVar
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNumberAccess().getNumberVarParserRuleCall_2()); 
                    }
                    pushFollow(FOLLOW_ruleNumberVar_in_rule__Number__Alternatives4198);
                    ruleNumberVar();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNumberAccess().getNumberVarParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1949:6: ( ruleNumberVarX )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1949:6: ( ruleNumberVarX )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1950:1: ruleNumberVarX
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNumberAccess().getNumberVarXParserRuleCall_3()); 
                    }
                    pushFollow(FOLLOW_ruleNumberVarX_in_rule__Number__Alternatives4215);
                    ruleNumberVarX();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNumberAccess().getNumberVarXParserRuleCall_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1955:6: ( ruleNumberVarY )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1955:6: ( ruleNumberVarY )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1956:1: ruleNumberVarY
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNumberAccess().getNumberVarYParserRuleCall_4()); 
                    }
                    pushFollow(FOLLOW_ruleNumberVarY_in_rule__Number__Alternatives4232);
                    ruleNumberVarY();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNumberAccess().getNumberVarYParserRuleCall_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1961:6: ( ruleNumberVarD )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1961:6: ( ruleNumberVarD )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1962:1: ruleNumberVarD
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getNumberAccess().getNumberVarDParserRuleCall_5()); 
                    }
                    pushFollow(FOLLOW_ruleNumberVarD_in_rule__Number__Alternatives4249);
                    ruleNumberVarD();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getNumberAccess().getNumberVarDParserRuleCall_5()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Number__Alternatives"


    // $ANTLR start "rule__Binary__Alternatives"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1972:1: rule__Binary__Alternatives : ( ( ruleOr ) | ( ruleAnd ) | ( ruleLt ) | ( ruleGt ) | ( ruleEq ) | ( ruleNot ) | ( ruleTouchsColor ) | ( ruleBinaryVar ) );
    public final void rule__Binary__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1976:1: ( ( ruleOr ) | ( ruleAnd ) | ( ruleLt ) | ( ruleGt ) | ( ruleEq ) | ( ruleNot ) | ( ruleTouchsColor ) | ( ruleBinaryVar ) )
            int alt8=8;
            alt8 = dfa8.predict(input);
            switch (alt8) {
                case 1 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1977:1: ( ruleOr )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1977:1: ( ruleOr )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1978:1: ruleOr
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBinaryAccess().getOrParserRuleCall_0()); 
                    }
                    pushFollow(FOLLOW_ruleOr_in_rule__Binary__Alternatives4281);
                    ruleOr();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBinaryAccess().getOrParserRuleCall_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1983:6: ( ruleAnd )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1983:6: ( ruleAnd )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1984:1: ruleAnd
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBinaryAccess().getAndParserRuleCall_1()); 
                    }
                    pushFollow(FOLLOW_ruleAnd_in_rule__Binary__Alternatives4298);
                    ruleAnd();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBinaryAccess().getAndParserRuleCall_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1989:6: ( ruleLt )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1989:6: ( ruleLt )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1990:1: ruleLt
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBinaryAccess().getLtParserRuleCall_2()); 
                    }
                    pushFollow(FOLLOW_ruleLt_in_rule__Binary__Alternatives4315);
                    ruleLt();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBinaryAccess().getLtParserRuleCall_2()); 
                    }

                    }


                    }
                    break;
                case 4 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1995:6: ( ruleGt )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1995:6: ( ruleGt )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1996:1: ruleGt
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBinaryAccess().getGtParserRuleCall_3()); 
                    }
                    pushFollow(FOLLOW_ruleGt_in_rule__Binary__Alternatives4332);
                    ruleGt();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBinaryAccess().getGtParserRuleCall_3()); 
                    }

                    }


                    }
                    break;
                case 5 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2001:6: ( ruleEq )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2001:6: ( ruleEq )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2002:1: ruleEq
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBinaryAccess().getEqParserRuleCall_4()); 
                    }
                    pushFollow(FOLLOW_ruleEq_in_rule__Binary__Alternatives4349);
                    ruleEq();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBinaryAccess().getEqParserRuleCall_4()); 
                    }

                    }


                    }
                    break;
                case 6 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2007:6: ( ruleNot )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2007:6: ( ruleNot )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2008:1: ruleNot
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBinaryAccess().getNotParserRuleCall_5()); 
                    }
                    pushFollow(FOLLOW_ruleNot_in_rule__Binary__Alternatives4366);
                    ruleNot();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBinaryAccess().getNotParserRuleCall_5()); 
                    }

                    }


                    }
                    break;
                case 7 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2013:6: ( ruleTouchsColor )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2013:6: ( ruleTouchsColor )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2014:1: ruleTouchsColor
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBinaryAccess().getTouchsColorParserRuleCall_6()); 
                    }
                    pushFollow(FOLLOW_ruleTouchsColor_in_rule__Binary__Alternatives4383);
                    ruleTouchsColor();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBinaryAccess().getTouchsColorParserRuleCall_6()); 
                    }

                    }


                    }
                    break;
                case 8 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2019:6: ( ruleBinaryVar )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2019:6: ( ruleBinaryVar )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2020:1: ruleBinaryVar
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getBinaryAccess().getBinaryVarParserRuleCall_7()); 
                    }
                    pushFollow(FOLLOW_ruleBinaryVar_in_rule__Binary__Alternatives4400);
                    ruleBinaryVar();

                    state._fsp--;
                    if (state.failed) return ;
                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getBinaryAccess().getBinaryVarParserRuleCall_7()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Alternatives"


    // $ANTLR start "rule__ObjectVariableEnum__Alternatives"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2030:1: rule__ObjectVariableEnum__Alternatives : ( ( ( 'x position' ) ) | ( ( 'y position' ) ) | ( ( 'direction' ) ) );
    public final void rule__ObjectVariableEnum__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2034:1: ( ( ( 'x position' ) ) | ( ( 'y position' ) ) | ( ( 'direction' ) ) )
            int alt9=3;
            switch ( input.LA(1) ) {
            case 12:
                {
                alt9=1;
                }
                break;
            case 13:
                {
                alt9=2;
                }
                break;
            case 14:
                {
                alt9=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return ;}
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2035:1: ( ( 'x position' ) )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2035:1: ( ( 'x position' ) )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2036:1: ( 'x position' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getObjectVariableEnumAccess().getXpositionEnumLiteralDeclaration_0()); 
                    }
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2037:1: ( 'x position' )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2037:3: 'x position'
                    {
                    match(input,12,FOLLOW_12_in_rule__ObjectVariableEnum__Alternatives4433); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getObjectVariableEnumAccess().getXpositionEnumLiteralDeclaration_0()); 
                    }

                    }


                    }
                    break;
                case 2 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2042:6: ( ( 'y position' ) )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2042:6: ( ( 'y position' ) )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2043:1: ( 'y position' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getObjectVariableEnumAccess().getYpositionEnumLiteralDeclaration_1()); 
                    }
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2044:1: ( 'y position' )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2044:3: 'y position'
                    {
                    match(input,13,FOLLOW_13_in_rule__ObjectVariableEnum__Alternatives4454); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getObjectVariableEnumAccess().getYpositionEnumLiteralDeclaration_1()); 
                    }

                    }


                    }
                    break;
                case 3 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2049:6: ( ( 'direction' ) )
                    {
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2049:6: ( ( 'direction' ) )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2050:1: ( 'direction' )
                    {
                    if ( state.backtracking==0 ) {
                       before(grammarAccess.getObjectVariableEnumAccess().getDirectionEnumLiteralDeclaration_2()); 
                    }
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2051:1: ( 'direction' )
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2051:3: 'direction'
                    {
                    match(input,14,FOLLOW_14_in_rule__ObjectVariableEnum__Alternatives4475); if (state.failed) return ;

                    }

                    if ( state.backtracking==0 ) {
                       after(grammarAccess.getObjectVariableEnumAccess().getDirectionEnumLiteralDeclaration_2()); 
                    }

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ObjectVariableEnum__Alternatives"


    // $ANTLR start "rule__Project__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2063:1: rule__Project__Group__0 : rule__Project__Group__0__Impl rule__Project__Group__1 ;
    public final void rule__Project__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2067:1: ( rule__Project__Group__0__Impl rule__Project__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2068:2: rule__Project__Group__0__Impl rule__Project__Group__1
            {
            pushFollow(FOLLOW_rule__Project__Group__0__Impl_in_rule__Project__Group__04508);
            rule__Project__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Project__Group__1_in_rule__Project__Group__04511);
            rule__Project__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Project__Group__0"


    // $ANTLR start "rule__Project__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2075:1: rule__Project__Group__0__Impl : ( 'project' ) ;
    public final void rule__Project__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2079:1: ( ( 'project' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2080:1: ( 'project' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2080:1: ( 'project' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2081:1: 'project'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProjectAccess().getProjectKeyword_0()); 
            }
            match(input,15,FOLLOW_15_in_rule__Project__Group__0__Impl4539); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getProjectAccess().getProjectKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Project__Group__0__Impl"


    // $ANTLR start "rule__Project__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2094:1: rule__Project__Group__1 : rule__Project__Group__1__Impl rule__Project__Group__2 ;
    public final void rule__Project__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2098:1: ( rule__Project__Group__1__Impl rule__Project__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2099:2: rule__Project__Group__1__Impl rule__Project__Group__2
            {
            pushFollow(FOLLOW_rule__Project__Group__1__Impl_in_rule__Project__Group__14570);
            rule__Project__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Project__Group__2_in_rule__Project__Group__14573);
            rule__Project__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Project__Group__1"


    // $ANTLR start "rule__Project__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2106:1: rule__Project__Group__1__Impl : ( ( rule__Project__NameAssignment_1 ) ) ;
    public final void rule__Project__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2110:1: ( ( ( rule__Project__NameAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2111:1: ( ( rule__Project__NameAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2111:1: ( ( rule__Project__NameAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2112:1: ( rule__Project__NameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProjectAccess().getNameAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2113:1: ( rule__Project__NameAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2113:2: rule__Project__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Project__NameAssignment_1_in_rule__Project__Group__1__Impl4600);
            rule__Project__NameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getProjectAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Project__Group__1__Impl"


    // $ANTLR start "rule__Project__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2123:1: rule__Project__Group__2 : rule__Project__Group__2__Impl ;
    public final void rule__Project__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2127:1: ( rule__Project__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2128:2: rule__Project__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Project__Group__2__Impl_in_rule__Project__Group__24630);
            rule__Project__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Project__Group__2"


    // $ANTLR start "rule__Project__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2134:1: rule__Project__Group__2__Impl : ( ( rule__Project__ObjectsAssignment_2 )* ) ;
    public final void rule__Project__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2138:1: ( ( ( rule__Project__ObjectsAssignment_2 )* ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2139:1: ( ( rule__Project__ObjectsAssignment_2 )* )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2139:1: ( ( rule__Project__ObjectsAssignment_2 )* )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2140:1: ( rule__Project__ObjectsAssignment_2 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProjectAccess().getObjectsAssignment_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2141:1: ( rule__Project__ObjectsAssignment_2 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==16) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2141:2: rule__Project__ObjectsAssignment_2
            	    {
            	    pushFollow(FOLLOW_rule__Project__ObjectsAssignment_2_in_rule__Project__Group__2__Impl4657);
            	    rule__Project__ObjectsAssignment_2();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getProjectAccess().getObjectsAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Project__Group__2__Impl"


    // $ANTLR start "rule__Object__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2157:1: rule__Object__Group__0 : rule__Object__Group__0__Impl rule__Object__Group__1 ;
    public final void rule__Object__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2161:1: ( rule__Object__Group__0__Impl rule__Object__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2162:2: rule__Object__Group__0__Impl rule__Object__Group__1
            {
            pushFollow(FOLLOW_rule__Object__Group__0__Impl_in_rule__Object__Group__04694);
            rule__Object__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Object__Group__1_in_rule__Object__Group__04697);
            rule__Object__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__0"


    // $ANTLR start "rule__Object__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2169:1: rule__Object__Group__0__Impl : ( 'object' ) ;
    public final void rule__Object__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2173:1: ( ( 'object' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2174:1: ( 'object' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2174:1: ( 'object' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2175:1: 'object'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getObjectAccess().getObjectKeyword_0()); 
            }
            match(input,16,FOLLOW_16_in_rule__Object__Group__0__Impl4725); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getObjectAccess().getObjectKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__0__Impl"


    // $ANTLR start "rule__Object__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2188:1: rule__Object__Group__1 : rule__Object__Group__1__Impl rule__Object__Group__2 ;
    public final void rule__Object__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2192:1: ( rule__Object__Group__1__Impl rule__Object__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2193:2: rule__Object__Group__1__Impl rule__Object__Group__2
            {
            pushFollow(FOLLOW_rule__Object__Group__1__Impl_in_rule__Object__Group__14756);
            rule__Object__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Object__Group__2_in_rule__Object__Group__14759);
            rule__Object__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__1"


    // $ANTLR start "rule__Object__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2200:1: rule__Object__Group__1__Impl : ( ( rule__Object__NameAssignment_1 ) ) ;
    public final void rule__Object__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2204:1: ( ( ( rule__Object__NameAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2205:1: ( ( rule__Object__NameAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2205:1: ( ( rule__Object__NameAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2206:1: ( rule__Object__NameAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getObjectAccess().getNameAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2207:1: ( rule__Object__NameAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2207:2: rule__Object__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Object__NameAssignment_1_in_rule__Object__Group__1__Impl4786);
            rule__Object__NameAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getObjectAccess().getNameAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__1__Impl"


    // $ANTLR start "rule__Object__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2217:1: rule__Object__Group__2 : rule__Object__Group__2__Impl ;
    public final void rule__Object__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2221:1: ( rule__Object__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2222:2: rule__Object__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Object__Group__2__Impl_in_rule__Object__Group__24816);
            rule__Object__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__2"


    // $ANTLR start "rule__Object__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2228:1: rule__Object__Group__2__Impl : ( ( rule__Object__ScriptsAssignment_2 )* ) ;
    public final void rule__Object__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2232:1: ( ( ( rule__Object__ScriptsAssignment_2 )* ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2233:1: ( ( rule__Object__ScriptsAssignment_2 )* )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2233:1: ( ( rule__Object__ScriptsAssignment_2 )* )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2234:1: ( rule__Object__ScriptsAssignment_2 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getObjectAccess().getScriptsAssignment_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2235:1: ( rule__Object__ScriptsAssignment_2 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==17) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2235:2: rule__Object__ScriptsAssignment_2
            	    {
            	    pushFollow(FOLLOW_rule__Object__ScriptsAssignment_2_in_rule__Object__Group__2__Impl4843);
            	    rule__Object__ScriptsAssignment_2();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getObjectAccess().getScriptsAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__Group__2__Impl"


    // $ANTLR start "rule__Script__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2251:1: rule__Script__Group__0 : rule__Script__Group__0__Impl rule__Script__Group__1 ;
    public final void rule__Script__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2255:1: ( rule__Script__Group__0__Impl rule__Script__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2256:2: rule__Script__Group__0__Impl rule__Script__Group__1
            {
            pushFollow(FOLLOW_rule__Script__Group__0__Impl_in_rule__Script__Group__04880);
            rule__Script__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Script__Group__1_in_rule__Script__Group__04883);
            rule__Script__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Script__Group__0"


    // $ANTLR start "rule__Script__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2263:1: rule__Script__Group__0__Impl : ( 'script' ) ;
    public final void rule__Script__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2267:1: ( ( 'script' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2268:1: ( 'script' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2268:1: ( 'script' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2269:1: 'script'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScriptAccess().getScriptKeyword_0()); 
            }
            match(input,17,FOLLOW_17_in_rule__Script__Group__0__Impl4911); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getScriptAccess().getScriptKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Script__Group__0__Impl"


    // $ANTLR start "rule__Script__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2282:1: rule__Script__Group__1 : rule__Script__Group__1__Impl rule__Script__Group__2 ;
    public final void rule__Script__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2286:1: ( rule__Script__Group__1__Impl rule__Script__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2287:2: rule__Script__Group__1__Impl rule__Script__Group__2
            {
            pushFollow(FOLLOW_rule__Script__Group__1__Impl_in_rule__Script__Group__14942);
            rule__Script__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Script__Group__2_in_rule__Script__Group__14945);
            rule__Script__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Script__Group__1"


    // $ANTLR start "rule__Script__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2294:1: rule__Script__Group__1__Impl : ( () ) ;
    public final void rule__Script__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2298:1: ( ( () ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2299:1: ( () )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2299:1: ( () )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2300:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScriptAccess().getScriptAction_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2301:1: ()
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2303:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getScriptAccess().getScriptAction_1()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Script__Group__1__Impl"


    // $ANTLR start "rule__Script__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2313:1: rule__Script__Group__2 : rule__Script__Group__2__Impl rule__Script__Group__3 ;
    public final void rule__Script__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2317:1: ( rule__Script__Group__2__Impl rule__Script__Group__3 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2318:2: rule__Script__Group__2__Impl rule__Script__Group__3
            {
            pushFollow(FOLLOW_rule__Script__Group__2__Impl_in_rule__Script__Group__25003);
            rule__Script__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Script__Group__3_in_rule__Script__Group__25006);
            rule__Script__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Script__Group__2"


    // $ANTLR start "rule__Script__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2325:1: rule__Script__Group__2__Impl : ( ( rule__Script__WhenAssignment_2 )? ) ;
    public final void rule__Script__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2329:1: ( ( ( rule__Script__WhenAssignment_2 )? ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2330:1: ( ( rule__Script__WhenAssignment_2 )? )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2330:1: ( ( rule__Script__WhenAssignment_2 )? )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2331:1: ( rule__Script__WhenAssignment_2 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScriptAccess().getWhenAssignment_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2332:1: ( rule__Script__WhenAssignment_2 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==19) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2332:2: rule__Script__WhenAssignment_2
                    {
                    pushFollow(FOLLOW_rule__Script__WhenAssignment_2_in_rule__Script__Group__2__Impl5033);
                    rule__Script__WhenAssignment_2();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getScriptAccess().getWhenAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Script__Group__2__Impl"


    // $ANTLR start "rule__Script__Group__3"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2342:1: rule__Script__Group__3 : rule__Script__Group__3__Impl rule__Script__Group__4 ;
    public final void rule__Script__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2346:1: ( rule__Script__Group__3__Impl rule__Script__Group__4 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2347:2: rule__Script__Group__3__Impl rule__Script__Group__4
            {
            pushFollow(FOLLOW_rule__Script__Group__3__Impl_in_rule__Script__Group__35064);
            rule__Script__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Script__Group__4_in_rule__Script__Group__35067);
            rule__Script__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Script__Group__3"


    // $ANTLR start "rule__Script__Group__3__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2354:1: rule__Script__Group__3__Impl : ( ( rule__Script__BlocksAssignment_3 )* ) ;
    public final void rule__Script__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2358:1: ( ( ( rule__Script__BlocksAssignment_3 )* ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2359:1: ( ( rule__Script__BlocksAssignment_3 )* )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2359:1: ( ( rule__Script__BlocksAssignment_3 )* )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2360:1: ( rule__Script__BlocksAssignment_3 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScriptAccess().getBlocksAssignment_3()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2361:1: ( rule__Script__BlocksAssignment_3 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( ((LA13_0>=22 && LA13_0<=24)||(LA13_0>=26 && LA13_0<=27)||LA13_0==29||LA13_0==31||LA13_0==33||(LA13_0>=35 && LA13_0<=38)||LA13_0==41||(LA13_0>=43 && LA13_0<=47)||(LA13_0>=61 && LA13_0<=63)||LA13_0==65) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2361:2: rule__Script__BlocksAssignment_3
            	    {
            	    pushFollow(FOLLOW_rule__Script__BlocksAssignment_3_in_rule__Script__Group__3__Impl5094);
            	    rule__Script__BlocksAssignment_3();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getScriptAccess().getBlocksAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Script__Group__3__Impl"


    // $ANTLR start "rule__Script__Group__4"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2371:1: rule__Script__Group__4 : rule__Script__Group__4__Impl ;
    public final void rule__Script__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2375:1: ( rule__Script__Group__4__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2376:2: rule__Script__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Script__Group__4__Impl_in_rule__Script__Group__45125);
            rule__Script__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Script__Group__4"


    // $ANTLR start "rule__Script__Group__4__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2382:1: rule__Script__Group__4__Impl : ( 'end' ) ;
    public final void rule__Script__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2386:1: ( ( 'end' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2387:1: ( 'end' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2387:1: ( 'end' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2388:1: 'end'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScriptAccess().getEndKeyword_4()); 
            }
            match(input,18,FOLLOW_18_in_rule__Script__Group__4__Impl5153); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getScriptAccess().getEndKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Script__Group__4__Impl"


    // $ANTLR start "rule__When__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2411:1: rule__When__Group__0 : rule__When__Group__0__Impl rule__When__Group__1 ;
    public final void rule__When__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2415:1: ( rule__When__Group__0__Impl rule__When__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2416:2: rule__When__Group__0__Impl rule__When__Group__1
            {
            pushFollow(FOLLOW_rule__When__Group__0__Impl_in_rule__When__Group__05194);
            rule__When__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__When__Group__1_in_rule__When__Group__05197);
            rule__When__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__When__Group__0"


    // $ANTLR start "rule__When__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2423:1: rule__When__Group__0__Impl : ( 'when' ) ;
    public final void rule__When__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2427:1: ( ( 'when' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2428:1: ( 'when' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2428:1: ( 'when' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2429:1: 'when'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWhenAccess().getWhenKeyword_0()); 
            }
            match(input,19,FOLLOW_19_in_rule__When__Group__0__Impl5225); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWhenAccess().getWhenKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__When__Group__0__Impl"


    // $ANTLR start "rule__When__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2442:1: rule__When__Group__1 : rule__When__Group__1__Impl ;
    public final void rule__When__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2446:1: ( rule__When__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2447:2: rule__When__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__When__Group__1__Impl_in_rule__When__Group__15256);
            rule__When__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__When__Group__1"


    // $ANTLR start "rule__When__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2453:1: rule__When__Group__1__Impl : ( ( rule__When__Alternatives_1 ) ) ;
    public final void rule__When__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2457:1: ( ( ( rule__When__Alternatives_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2458:1: ( ( rule__When__Alternatives_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2458:1: ( ( rule__When__Alternatives_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2459:1: ( rule__When__Alternatives_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWhenAccess().getAlternatives_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2460:1: ( rule__When__Alternatives_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2460:2: rule__When__Alternatives_1
            {
            pushFollow(FOLLOW_rule__When__Alternatives_1_in_rule__When__Group__1__Impl5283);
            rule__When__Alternatives_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWhenAccess().getAlternatives_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__When__Group__1__Impl"


    // $ANTLR start "rule__WhenFlag__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2474:1: rule__WhenFlag__Group__0 : rule__WhenFlag__Group__0__Impl rule__WhenFlag__Group__1 ;
    public final void rule__WhenFlag__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2478:1: ( rule__WhenFlag__Group__0__Impl rule__WhenFlag__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2479:2: rule__WhenFlag__Group__0__Impl rule__WhenFlag__Group__1
            {
            pushFollow(FOLLOW_rule__WhenFlag__Group__0__Impl_in_rule__WhenFlag__Group__05317);
            rule__WhenFlag__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__WhenFlag__Group__1_in_rule__WhenFlag__Group__05320);
            rule__WhenFlag__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhenFlag__Group__0"


    // $ANTLR start "rule__WhenFlag__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2486:1: rule__WhenFlag__Group__0__Impl : ( 'green flag clicked' ) ;
    public final void rule__WhenFlag__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2490:1: ( ( 'green flag clicked' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2491:1: ( 'green flag clicked' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2491:1: ( 'green flag clicked' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2492:1: 'green flag clicked'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWhenFlagAccess().getGreenFlagClickedKeyword_0()); 
            }
            match(input,20,FOLLOW_20_in_rule__WhenFlag__Group__0__Impl5348); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWhenFlagAccess().getGreenFlagClickedKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhenFlag__Group__0__Impl"


    // $ANTLR start "rule__WhenFlag__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2505:1: rule__WhenFlag__Group__1 : rule__WhenFlag__Group__1__Impl ;
    public final void rule__WhenFlag__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2509:1: ( rule__WhenFlag__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2510:2: rule__WhenFlag__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__WhenFlag__Group__1__Impl_in_rule__WhenFlag__Group__15379);
            rule__WhenFlag__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhenFlag__Group__1"


    // $ANTLR start "rule__WhenFlag__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2516:1: rule__WhenFlag__Group__1__Impl : ( () ) ;
    public final void rule__WhenFlag__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2520:1: ( ( () ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2521:1: ( () )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2521:1: ( () )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2522:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWhenFlagAccess().getWhenFlagAction_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2523:1: ()
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2525:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWhenFlagAccess().getWhenFlagAction_1()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhenFlag__Group__1__Impl"


    // $ANTLR start "rule__WhenMsg__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2539:1: rule__WhenMsg__Group__0 : rule__WhenMsg__Group__0__Impl rule__WhenMsg__Group__1 ;
    public final void rule__WhenMsg__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2543:1: ( rule__WhenMsg__Group__0__Impl rule__WhenMsg__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2544:2: rule__WhenMsg__Group__0__Impl rule__WhenMsg__Group__1
            {
            pushFollow(FOLLOW_rule__WhenMsg__Group__0__Impl_in_rule__WhenMsg__Group__05441);
            rule__WhenMsg__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__WhenMsg__Group__1_in_rule__WhenMsg__Group__05444);
            rule__WhenMsg__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhenMsg__Group__0"


    // $ANTLR start "rule__WhenMsg__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2551:1: rule__WhenMsg__Group__0__Impl : ( 'I receive' ) ;
    public final void rule__WhenMsg__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2555:1: ( ( 'I receive' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2556:1: ( 'I receive' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2556:1: ( 'I receive' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2557:1: 'I receive'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWhenMsgAccess().getIReceiveKeyword_0()); 
            }
            match(input,21,FOLLOW_21_in_rule__WhenMsg__Group__0__Impl5472); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWhenMsgAccess().getIReceiveKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhenMsg__Group__0__Impl"


    // $ANTLR start "rule__WhenMsg__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2570:1: rule__WhenMsg__Group__1 : rule__WhenMsg__Group__1__Impl ;
    public final void rule__WhenMsg__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2574:1: ( rule__WhenMsg__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2575:2: rule__WhenMsg__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__WhenMsg__Group__1__Impl_in_rule__WhenMsg__Group__15503);
            rule__WhenMsg__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhenMsg__Group__1"


    // $ANTLR start "rule__WhenMsg__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2581:1: rule__WhenMsg__Group__1__Impl : ( ( rule__WhenMsg__MsgAssignment_1 ) ) ;
    public final void rule__WhenMsg__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2585:1: ( ( ( rule__WhenMsg__MsgAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2586:1: ( ( rule__WhenMsg__MsgAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2586:1: ( ( rule__WhenMsg__MsgAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2587:1: ( rule__WhenMsg__MsgAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWhenMsgAccess().getMsgAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2588:1: ( rule__WhenMsg__MsgAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2588:2: rule__WhenMsg__MsgAssignment_1
            {
            pushFollow(FOLLOW_rule__WhenMsg__MsgAssignment_1_in_rule__WhenMsg__Group__1__Impl5530);
            rule__WhenMsg__MsgAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWhenMsgAccess().getMsgAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhenMsg__Group__1__Impl"


    // $ANTLR start "rule__Loop__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2602:1: rule__Loop__Group__0 : rule__Loop__Group__0__Impl rule__Loop__Group__1 ;
    public final void rule__Loop__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2606:1: ( rule__Loop__Group__0__Impl rule__Loop__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2607:2: rule__Loop__Group__0__Impl rule__Loop__Group__1
            {
            pushFollow(FOLLOW_rule__Loop__Group__0__Impl_in_rule__Loop__Group__05564);
            rule__Loop__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Loop__Group__1_in_rule__Loop__Group__05567);
            rule__Loop__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__Group__0"


    // $ANTLR start "rule__Loop__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2614:1: rule__Loop__Group__0__Impl : ( ( rule__Loop__Alternatives_0 ) ) ;
    public final void rule__Loop__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2618:1: ( ( ( rule__Loop__Alternatives_0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2619:1: ( ( rule__Loop__Alternatives_0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2619:1: ( ( rule__Loop__Alternatives_0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2620:1: ( rule__Loop__Alternatives_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopAccess().getAlternatives_0()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2621:1: ( rule__Loop__Alternatives_0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2621:2: rule__Loop__Alternatives_0
            {
            pushFollow(FOLLOW_rule__Loop__Alternatives_0_in_rule__Loop__Group__0__Impl5594);
            rule__Loop__Alternatives_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopAccess().getAlternatives_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__Group__0__Impl"


    // $ANTLR start "rule__Loop__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2631:1: rule__Loop__Group__1 : rule__Loop__Group__1__Impl rule__Loop__Group__2 ;
    public final void rule__Loop__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2635:1: ( rule__Loop__Group__1__Impl rule__Loop__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2636:2: rule__Loop__Group__1__Impl rule__Loop__Group__2
            {
            pushFollow(FOLLOW_rule__Loop__Group__1__Impl_in_rule__Loop__Group__15624);
            rule__Loop__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Loop__Group__2_in_rule__Loop__Group__15627);
            rule__Loop__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__Group__1"


    // $ANTLR start "rule__Loop__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2643:1: rule__Loop__Group__1__Impl : ( ( rule__Loop__BlocksAssignment_1 )* ) ;
    public final void rule__Loop__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2647:1: ( ( ( rule__Loop__BlocksAssignment_1 )* ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2648:1: ( ( rule__Loop__BlocksAssignment_1 )* )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2648:1: ( ( rule__Loop__BlocksAssignment_1 )* )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2649:1: ( rule__Loop__BlocksAssignment_1 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopAccess().getBlocksAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2650:1: ( rule__Loop__BlocksAssignment_1 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( ((LA14_0>=22 && LA14_0<=24)||(LA14_0>=26 && LA14_0<=27)||LA14_0==29||LA14_0==31||LA14_0==33||(LA14_0>=35 && LA14_0<=38)||LA14_0==41||(LA14_0>=43 && LA14_0<=47)||(LA14_0>=61 && LA14_0<=63)||LA14_0==65) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2650:2: rule__Loop__BlocksAssignment_1
            	    {
            	    pushFollow(FOLLOW_rule__Loop__BlocksAssignment_1_in_rule__Loop__Group__1__Impl5654);
            	    rule__Loop__BlocksAssignment_1();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopAccess().getBlocksAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__Group__1__Impl"


    // $ANTLR start "rule__Loop__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2660:1: rule__Loop__Group__2 : rule__Loop__Group__2__Impl ;
    public final void rule__Loop__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2664:1: ( rule__Loop__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2665:2: rule__Loop__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Loop__Group__2__Impl_in_rule__Loop__Group__25685);
            rule__Loop__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__Group__2"


    // $ANTLR start "rule__Loop__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2671:1: rule__Loop__Group__2__Impl : ( 'end' ) ;
    public final void rule__Loop__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2675:1: ( ( 'end' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2676:1: ( 'end' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2676:1: ( 'end' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2677:1: 'end'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopAccess().getEndKeyword_2()); 
            }
            match(input,18,FOLLOW_18_in_rule__Loop__Group__2__Impl5713); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopAccess().getEndKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__Group__2__Impl"


    // $ANTLR start "rule__Forever__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2696:1: rule__Forever__Group__0 : rule__Forever__Group__0__Impl rule__Forever__Group__1 ;
    public final void rule__Forever__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2700:1: ( rule__Forever__Group__0__Impl rule__Forever__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2701:2: rule__Forever__Group__0__Impl rule__Forever__Group__1
            {
            pushFollow(FOLLOW_rule__Forever__Group__0__Impl_in_rule__Forever__Group__05750);
            rule__Forever__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Forever__Group__1_in_rule__Forever__Group__05753);
            rule__Forever__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forever__Group__0"


    // $ANTLR start "rule__Forever__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2708:1: rule__Forever__Group__0__Impl : ( 'forever' ) ;
    public final void rule__Forever__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2712:1: ( ( 'forever' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2713:1: ( 'forever' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2713:1: ( 'forever' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2714:1: 'forever'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getForeverAccess().getForeverKeyword_0()); 
            }
            match(input,22,FOLLOW_22_in_rule__Forever__Group__0__Impl5781); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getForeverAccess().getForeverKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forever__Group__0__Impl"


    // $ANTLR start "rule__Forever__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2727:1: rule__Forever__Group__1 : rule__Forever__Group__1__Impl rule__Forever__Group__2 ;
    public final void rule__Forever__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2731:1: ( rule__Forever__Group__1__Impl rule__Forever__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2732:2: rule__Forever__Group__1__Impl rule__Forever__Group__2
            {
            pushFollow(FOLLOW_rule__Forever__Group__1__Impl_in_rule__Forever__Group__15812);
            rule__Forever__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Forever__Group__2_in_rule__Forever__Group__15815);
            rule__Forever__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forever__Group__1"


    // $ANTLR start "rule__Forever__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2739:1: rule__Forever__Group__1__Impl : ( () ) ;
    public final void rule__Forever__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2743:1: ( ( () ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2744:1: ( () )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2744:1: ( () )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2745:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getForeverAccess().getForeverAction_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2746:1: ()
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2748:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getForeverAccess().getForeverAction_1()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forever__Group__1__Impl"


    // $ANTLR start "rule__Forever__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2758:1: rule__Forever__Group__2 : rule__Forever__Group__2__Impl ;
    public final void rule__Forever__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2762:1: ( rule__Forever__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2763:2: rule__Forever__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Forever__Group__2__Impl_in_rule__Forever__Group__25873);
            rule__Forever__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forever__Group__2"


    // $ANTLR start "rule__Forever__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2769:1: rule__Forever__Group__2__Impl : ( ( rule__Forever__Group_2__0 )? ) ;
    public final void rule__Forever__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2773:1: ( ( ( rule__Forever__Group_2__0 )? ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2774:1: ( ( rule__Forever__Group_2__0 )? )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2774:1: ( ( rule__Forever__Group_2__0 )? )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2775:1: ( rule__Forever__Group_2__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getForeverAccess().getGroup_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2776:1: ( rule__Forever__Group_2__0 )?
            int alt15=2;
            alt15 = dfa15.predict(input);
            switch (alt15) {
                case 1 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2776:2: rule__Forever__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__Forever__Group_2__0_in_rule__Forever__Group__2__Impl5900);
                    rule__Forever__Group_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getForeverAccess().getGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forever__Group__2__Impl"


    // $ANTLR start "rule__Forever__Group_2__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2792:1: rule__Forever__Group_2__0 : rule__Forever__Group_2__0__Impl rule__Forever__Group_2__1 ;
    public final void rule__Forever__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2796:1: ( rule__Forever__Group_2__0__Impl rule__Forever__Group_2__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2797:2: rule__Forever__Group_2__0__Impl rule__Forever__Group_2__1
            {
            pushFollow(FOLLOW_rule__Forever__Group_2__0__Impl_in_rule__Forever__Group_2__05937);
            rule__Forever__Group_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Forever__Group_2__1_in_rule__Forever__Group_2__05940);
            rule__Forever__Group_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forever__Group_2__0"


    // $ANTLR start "rule__Forever__Group_2__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2804:1: rule__Forever__Group_2__0__Impl : ( 'if' ) ;
    public final void rule__Forever__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2808:1: ( ( 'if' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2809:1: ( 'if' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2809:1: ( 'if' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2810:1: 'if'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getForeverAccess().getIfKeyword_2_0()); 
            }
            match(input,23,FOLLOW_23_in_rule__Forever__Group_2__0__Impl5968); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getForeverAccess().getIfKeyword_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forever__Group_2__0__Impl"


    // $ANTLR start "rule__Forever__Group_2__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2823:1: rule__Forever__Group_2__1 : rule__Forever__Group_2__1__Impl ;
    public final void rule__Forever__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2827:1: ( rule__Forever__Group_2__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2828:2: rule__Forever__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Forever__Group_2__1__Impl_in_rule__Forever__Group_2__15999);
            rule__Forever__Group_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forever__Group_2__1"


    // $ANTLR start "rule__Forever__Group_2__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2834:1: rule__Forever__Group_2__1__Impl : ( ( rule__Forever__CondAssignment_2_1 ) ) ;
    public final void rule__Forever__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2838:1: ( ( ( rule__Forever__CondAssignment_2_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2839:1: ( ( rule__Forever__CondAssignment_2_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2839:1: ( ( rule__Forever__CondAssignment_2_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2840:1: ( rule__Forever__CondAssignment_2_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getForeverAccess().getCondAssignment_2_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2841:1: ( rule__Forever__CondAssignment_2_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2841:2: rule__Forever__CondAssignment_2_1
            {
            pushFollow(FOLLOW_rule__Forever__CondAssignment_2_1_in_rule__Forever__Group_2__1__Impl6026);
            rule__Forever__CondAssignment_2_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getForeverAccess().getCondAssignment_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forever__Group_2__1__Impl"


    // $ANTLR start "rule__RepeatN__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2855:1: rule__RepeatN__Group__0 : rule__RepeatN__Group__0__Impl rule__RepeatN__Group__1 ;
    public final void rule__RepeatN__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2859:1: ( rule__RepeatN__Group__0__Impl rule__RepeatN__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2860:2: rule__RepeatN__Group__0__Impl rule__RepeatN__Group__1
            {
            pushFollow(FOLLOW_rule__RepeatN__Group__0__Impl_in_rule__RepeatN__Group__06060);
            rule__RepeatN__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__RepeatN__Group__1_in_rule__RepeatN__Group__06063);
            rule__RepeatN__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepeatN__Group__0"


    // $ANTLR start "rule__RepeatN__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2867:1: rule__RepeatN__Group__0__Impl : ( 'repeat' ) ;
    public final void rule__RepeatN__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2871:1: ( ( 'repeat' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2872:1: ( 'repeat' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2872:1: ( 'repeat' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2873:1: 'repeat'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRepeatNAccess().getRepeatKeyword_0()); 
            }
            match(input,24,FOLLOW_24_in_rule__RepeatN__Group__0__Impl6091); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRepeatNAccess().getRepeatKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepeatN__Group__0__Impl"


    // $ANTLR start "rule__RepeatN__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2886:1: rule__RepeatN__Group__1 : rule__RepeatN__Group__1__Impl ;
    public final void rule__RepeatN__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2890:1: ( rule__RepeatN__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2891:2: rule__RepeatN__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__RepeatN__Group__1__Impl_in_rule__RepeatN__Group__16122);
            rule__RepeatN__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepeatN__Group__1"


    // $ANTLR start "rule__RepeatN__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2897:1: rule__RepeatN__Group__1__Impl : ( ( rule__RepeatN__NAssignment_1 ) ) ;
    public final void rule__RepeatN__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2901:1: ( ( ( rule__RepeatN__NAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2902:1: ( ( rule__RepeatN__NAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2902:1: ( ( rule__RepeatN__NAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2903:1: ( rule__RepeatN__NAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRepeatNAccess().getNAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2904:1: ( rule__RepeatN__NAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2904:2: rule__RepeatN__NAssignment_1
            {
            pushFollow(FOLLOW_rule__RepeatN__NAssignment_1_in_rule__RepeatN__Group__1__Impl6149);
            rule__RepeatN__NAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRepeatNAccess().getNAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepeatN__Group__1__Impl"


    // $ANTLR start "rule__RepeatUntil__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2918:1: rule__RepeatUntil__Group__0 : rule__RepeatUntil__Group__0__Impl rule__RepeatUntil__Group__1 ;
    public final void rule__RepeatUntil__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2922:1: ( rule__RepeatUntil__Group__0__Impl rule__RepeatUntil__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2923:2: rule__RepeatUntil__Group__0__Impl rule__RepeatUntil__Group__1
            {
            pushFollow(FOLLOW_rule__RepeatUntil__Group__0__Impl_in_rule__RepeatUntil__Group__06183);
            rule__RepeatUntil__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__RepeatUntil__Group__1_in_rule__RepeatUntil__Group__06186);
            rule__RepeatUntil__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepeatUntil__Group__0"


    // $ANTLR start "rule__RepeatUntil__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2930:1: rule__RepeatUntil__Group__0__Impl : ( 'repeat' ) ;
    public final void rule__RepeatUntil__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2934:1: ( ( 'repeat' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2935:1: ( 'repeat' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2935:1: ( 'repeat' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2936:1: 'repeat'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRepeatUntilAccess().getRepeatKeyword_0()); 
            }
            match(input,24,FOLLOW_24_in_rule__RepeatUntil__Group__0__Impl6214); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRepeatUntilAccess().getRepeatKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepeatUntil__Group__0__Impl"


    // $ANTLR start "rule__RepeatUntil__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2949:1: rule__RepeatUntil__Group__1 : rule__RepeatUntil__Group__1__Impl rule__RepeatUntil__Group__2 ;
    public final void rule__RepeatUntil__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2953:1: ( rule__RepeatUntil__Group__1__Impl rule__RepeatUntil__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2954:2: rule__RepeatUntil__Group__1__Impl rule__RepeatUntil__Group__2
            {
            pushFollow(FOLLOW_rule__RepeatUntil__Group__1__Impl_in_rule__RepeatUntil__Group__16245);
            rule__RepeatUntil__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__RepeatUntil__Group__2_in_rule__RepeatUntil__Group__16248);
            rule__RepeatUntil__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepeatUntil__Group__1"


    // $ANTLR start "rule__RepeatUntil__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2961:1: rule__RepeatUntil__Group__1__Impl : ( 'until' ) ;
    public final void rule__RepeatUntil__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2965:1: ( ( 'until' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2966:1: ( 'until' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2966:1: ( 'until' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2967:1: 'until'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRepeatUntilAccess().getUntilKeyword_1()); 
            }
            match(input,25,FOLLOW_25_in_rule__RepeatUntil__Group__1__Impl6276); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRepeatUntilAccess().getUntilKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepeatUntil__Group__1__Impl"


    // $ANTLR start "rule__RepeatUntil__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2980:1: rule__RepeatUntil__Group__2 : rule__RepeatUntil__Group__2__Impl ;
    public final void rule__RepeatUntil__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2984:1: ( rule__RepeatUntil__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2985:2: rule__RepeatUntil__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__RepeatUntil__Group__2__Impl_in_rule__RepeatUntil__Group__26307);
            rule__RepeatUntil__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepeatUntil__Group__2"


    // $ANTLR start "rule__RepeatUntil__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2991:1: rule__RepeatUntil__Group__2__Impl : ( ( rule__RepeatUntil__CondAssignment_2 ) ) ;
    public final void rule__RepeatUntil__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2995:1: ( ( ( rule__RepeatUntil__CondAssignment_2 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2996:1: ( ( rule__RepeatUntil__CondAssignment_2 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2996:1: ( ( rule__RepeatUntil__CondAssignment_2 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2997:1: ( rule__RepeatUntil__CondAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRepeatUntilAccess().getCondAssignment_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2998:1: ( rule__RepeatUntil__CondAssignment_2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2998:2: rule__RepeatUntil__CondAssignment_2
            {
            pushFollow(FOLLOW_rule__RepeatUntil__CondAssignment_2_in_rule__RepeatUntil__Group__2__Impl6334);
            rule__RepeatUntil__CondAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRepeatUntilAccess().getCondAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepeatUntil__Group__2__Impl"


    // $ANTLR start "rule__Stop__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3014:1: rule__Stop__Group__0 : rule__Stop__Group__0__Impl rule__Stop__Group__1 ;
    public final void rule__Stop__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3018:1: ( rule__Stop__Group__0__Impl rule__Stop__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3019:2: rule__Stop__Group__0__Impl rule__Stop__Group__1
            {
            pushFollow(FOLLOW_rule__Stop__Group__0__Impl_in_rule__Stop__Group__06370);
            rule__Stop__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Stop__Group__1_in_rule__Stop__Group__06373);
            rule__Stop__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stop__Group__0"


    // $ANTLR start "rule__Stop__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3026:1: rule__Stop__Group__0__Impl : ( 'stop' ) ;
    public final void rule__Stop__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3030:1: ( ( 'stop' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3031:1: ( 'stop' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3031:1: ( 'stop' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3032:1: 'stop'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStopAccess().getStopKeyword_0()); 
            }
            match(input,26,FOLLOW_26_in_rule__Stop__Group__0__Impl6401); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStopAccess().getStopKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stop__Group__0__Impl"


    // $ANTLR start "rule__Stop__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3045:1: rule__Stop__Group__1 : rule__Stop__Group__1__Impl ;
    public final void rule__Stop__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3049:1: ( rule__Stop__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3050:2: rule__Stop__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Stop__Group__1__Impl_in_rule__Stop__Group__16432);
            rule__Stop__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stop__Group__1"


    // $ANTLR start "rule__Stop__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3056:1: rule__Stop__Group__1__Impl : ( ( rule__Stop__Alternatives_1 ) ) ;
    public final void rule__Stop__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3060:1: ( ( ( rule__Stop__Alternatives_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3061:1: ( ( rule__Stop__Alternatives_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3061:1: ( ( rule__Stop__Alternatives_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3062:1: ( rule__Stop__Alternatives_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStopAccess().getAlternatives_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3063:1: ( rule__Stop__Alternatives_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3063:2: rule__Stop__Alternatives_1
            {
            pushFollow(FOLLOW_rule__Stop__Alternatives_1_in_rule__Stop__Group__1__Impl6459);
            rule__Stop__Alternatives_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStopAccess().getAlternatives_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stop__Group__1__Impl"


    // $ANTLR start "rule__Wait__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3077:1: rule__Wait__Group__0 : rule__Wait__Group__0__Impl rule__Wait__Group__1 ;
    public final void rule__Wait__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3081:1: ( rule__Wait__Group__0__Impl rule__Wait__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3082:2: rule__Wait__Group__0__Impl rule__Wait__Group__1
            {
            pushFollow(FOLLOW_rule__Wait__Group__0__Impl_in_rule__Wait__Group__06493);
            rule__Wait__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Wait__Group__1_in_rule__Wait__Group__06496);
            rule__Wait__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Wait__Group__0"


    // $ANTLR start "rule__Wait__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3089:1: rule__Wait__Group__0__Impl : ( 'wait' ) ;
    public final void rule__Wait__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3093:1: ( ( 'wait' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3094:1: ( 'wait' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3094:1: ( 'wait' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3095:1: 'wait'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitAccess().getWaitKeyword_0()); 
            }
            match(input,27,FOLLOW_27_in_rule__Wait__Group__0__Impl6524); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitAccess().getWaitKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Wait__Group__0__Impl"


    // $ANTLR start "rule__Wait__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3108:1: rule__Wait__Group__1 : rule__Wait__Group__1__Impl ;
    public final void rule__Wait__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3112:1: ( rule__Wait__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3113:2: rule__Wait__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Wait__Group__1__Impl_in_rule__Wait__Group__16555);
            rule__Wait__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Wait__Group__1"


    // $ANTLR start "rule__Wait__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3119:1: rule__Wait__Group__1__Impl : ( ( rule__Wait__Alternatives_1 ) ) ;
    public final void rule__Wait__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3123:1: ( ( ( rule__Wait__Alternatives_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3124:1: ( ( rule__Wait__Alternatives_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3124:1: ( ( rule__Wait__Alternatives_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3125:1: ( rule__Wait__Alternatives_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitAccess().getAlternatives_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3126:1: ( rule__Wait__Alternatives_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3126:2: rule__Wait__Alternatives_1
            {
            pushFollow(FOLLOW_rule__Wait__Alternatives_1_in_rule__Wait__Group__1__Impl6582);
            rule__Wait__Alternatives_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitAccess().getAlternatives_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Wait__Group__1__Impl"


    // $ANTLR start "rule__WaitDelay__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3140:1: rule__WaitDelay__Group__0 : rule__WaitDelay__Group__0__Impl rule__WaitDelay__Group__1 ;
    public final void rule__WaitDelay__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3144:1: ( rule__WaitDelay__Group__0__Impl rule__WaitDelay__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3145:2: rule__WaitDelay__Group__0__Impl rule__WaitDelay__Group__1
            {
            pushFollow(FOLLOW_rule__WaitDelay__Group__0__Impl_in_rule__WaitDelay__Group__06616);
            rule__WaitDelay__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__WaitDelay__Group__1_in_rule__WaitDelay__Group__06619);
            rule__WaitDelay__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitDelay__Group__0"


    // $ANTLR start "rule__WaitDelay__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3152:1: rule__WaitDelay__Group__0__Impl : ( ( rule__WaitDelay__DelayAssignment_0 ) ) ;
    public final void rule__WaitDelay__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3156:1: ( ( ( rule__WaitDelay__DelayAssignment_0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3157:1: ( ( rule__WaitDelay__DelayAssignment_0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3157:1: ( ( rule__WaitDelay__DelayAssignment_0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3158:1: ( rule__WaitDelay__DelayAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitDelayAccess().getDelayAssignment_0()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3159:1: ( rule__WaitDelay__DelayAssignment_0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3159:2: rule__WaitDelay__DelayAssignment_0
            {
            pushFollow(FOLLOW_rule__WaitDelay__DelayAssignment_0_in_rule__WaitDelay__Group__0__Impl6646);
            rule__WaitDelay__DelayAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitDelayAccess().getDelayAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitDelay__Group__0__Impl"


    // $ANTLR start "rule__WaitDelay__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3169:1: rule__WaitDelay__Group__1 : rule__WaitDelay__Group__1__Impl ;
    public final void rule__WaitDelay__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3173:1: ( rule__WaitDelay__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3174:2: rule__WaitDelay__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__WaitDelay__Group__1__Impl_in_rule__WaitDelay__Group__16676);
            rule__WaitDelay__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitDelay__Group__1"


    // $ANTLR start "rule__WaitDelay__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3180:1: rule__WaitDelay__Group__1__Impl : ( 'secs' ) ;
    public final void rule__WaitDelay__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3184:1: ( ( 'secs' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3185:1: ( 'secs' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3185:1: ( 'secs' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3186:1: 'secs'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitDelayAccess().getSecsKeyword_1()); 
            }
            match(input,28,FOLLOW_28_in_rule__WaitDelay__Group__1__Impl6704); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitDelayAccess().getSecsKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitDelay__Group__1__Impl"


    // $ANTLR start "rule__WaitUntil__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3203:1: rule__WaitUntil__Group__0 : rule__WaitUntil__Group__0__Impl rule__WaitUntil__Group__1 ;
    public final void rule__WaitUntil__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3207:1: ( rule__WaitUntil__Group__0__Impl rule__WaitUntil__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3208:2: rule__WaitUntil__Group__0__Impl rule__WaitUntil__Group__1
            {
            pushFollow(FOLLOW_rule__WaitUntil__Group__0__Impl_in_rule__WaitUntil__Group__06739);
            rule__WaitUntil__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__WaitUntil__Group__1_in_rule__WaitUntil__Group__06742);
            rule__WaitUntil__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitUntil__Group__0"


    // $ANTLR start "rule__WaitUntil__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3215:1: rule__WaitUntil__Group__0__Impl : ( 'until' ) ;
    public final void rule__WaitUntil__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3219:1: ( ( 'until' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3220:1: ( 'until' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3220:1: ( 'until' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3221:1: 'until'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitUntilAccess().getUntilKeyword_0()); 
            }
            match(input,25,FOLLOW_25_in_rule__WaitUntil__Group__0__Impl6770); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitUntilAccess().getUntilKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitUntil__Group__0__Impl"


    // $ANTLR start "rule__WaitUntil__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3234:1: rule__WaitUntil__Group__1 : rule__WaitUntil__Group__1__Impl ;
    public final void rule__WaitUntil__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3238:1: ( rule__WaitUntil__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3239:2: rule__WaitUntil__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__WaitUntil__Group__1__Impl_in_rule__WaitUntil__Group__16801);
            rule__WaitUntil__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitUntil__Group__1"


    // $ANTLR start "rule__WaitUntil__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3245:1: rule__WaitUntil__Group__1__Impl : ( ( rule__WaitUntil__CondAssignment_1 ) ) ;
    public final void rule__WaitUntil__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3249:1: ( ( ( rule__WaitUntil__CondAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3250:1: ( ( rule__WaitUntil__CondAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3250:1: ( ( rule__WaitUntil__CondAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3251:1: ( rule__WaitUntil__CondAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitUntilAccess().getCondAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3252:1: ( rule__WaitUntil__CondAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3252:2: rule__WaitUntil__CondAssignment_1
            {
            pushFollow(FOLLOW_rule__WaitUntil__CondAssignment_1_in_rule__WaitUntil__Group__1__Impl6828);
            rule__WaitUntil__CondAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitUntilAccess().getCondAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitUntil__Group__1__Impl"


    // $ANTLR start "rule__Broadcast__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3266:1: rule__Broadcast__Group__0 : rule__Broadcast__Group__0__Impl rule__Broadcast__Group__1 ;
    public final void rule__Broadcast__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3270:1: ( rule__Broadcast__Group__0__Impl rule__Broadcast__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3271:2: rule__Broadcast__Group__0__Impl rule__Broadcast__Group__1
            {
            pushFollow(FOLLOW_rule__Broadcast__Group__0__Impl_in_rule__Broadcast__Group__06862);
            rule__Broadcast__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Broadcast__Group__1_in_rule__Broadcast__Group__06865);
            rule__Broadcast__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Broadcast__Group__0"


    // $ANTLR start "rule__Broadcast__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3278:1: rule__Broadcast__Group__0__Impl : ( 'broadcast' ) ;
    public final void rule__Broadcast__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3282:1: ( ( 'broadcast' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3283:1: ( 'broadcast' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3283:1: ( 'broadcast' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3284:1: 'broadcast'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBroadcastAccess().getBroadcastKeyword_0()); 
            }
            match(input,29,FOLLOW_29_in_rule__Broadcast__Group__0__Impl6893); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBroadcastAccess().getBroadcastKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Broadcast__Group__0__Impl"


    // $ANTLR start "rule__Broadcast__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3297:1: rule__Broadcast__Group__1 : rule__Broadcast__Group__1__Impl rule__Broadcast__Group__2 ;
    public final void rule__Broadcast__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3301:1: ( rule__Broadcast__Group__1__Impl rule__Broadcast__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3302:2: rule__Broadcast__Group__1__Impl rule__Broadcast__Group__2
            {
            pushFollow(FOLLOW_rule__Broadcast__Group__1__Impl_in_rule__Broadcast__Group__16924);
            rule__Broadcast__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Broadcast__Group__2_in_rule__Broadcast__Group__16927);
            rule__Broadcast__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Broadcast__Group__1"


    // $ANTLR start "rule__Broadcast__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3309:1: rule__Broadcast__Group__1__Impl : ( ( rule__Broadcast__MsgAssignment_1 ) ) ;
    public final void rule__Broadcast__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3313:1: ( ( ( rule__Broadcast__MsgAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3314:1: ( ( rule__Broadcast__MsgAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3314:1: ( ( rule__Broadcast__MsgAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3315:1: ( rule__Broadcast__MsgAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBroadcastAccess().getMsgAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3316:1: ( rule__Broadcast__MsgAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3316:2: rule__Broadcast__MsgAssignment_1
            {
            pushFollow(FOLLOW_rule__Broadcast__MsgAssignment_1_in_rule__Broadcast__Group__1__Impl6954);
            rule__Broadcast__MsgAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBroadcastAccess().getMsgAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Broadcast__Group__1__Impl"


    // $ANTLR start "rule__Broadcast__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3326:1: rule__Broadcast__Group__2 : rule__Broadcast__Group__2__Impl ;
    public final void rule__Broadcast__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3330:1: ( rule__Broadcast__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3331:2: rule__Broadcast__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Broadcast__Group__2__Impl_in_rule__Broadcast__Group__26984);
            rule__Broadcast__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Broadcast__Group__2"


    // $ANTLR start "rule__Broadcast__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3337:1: rule__Broadcast__Group__2__Impl : ( ( rule__Broadcast__WaitAssignment_2 )? ) ;
    public final void rule__Broadcast__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3341:1: ( ( ( rule__Broadcast__WaitAssignment_2 )? ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3342:1: ( ( rule__Broadcast__WaitAssignment_2 )? )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3342:1: ( ( rule__Broadcast__WaitAssignment_2 )? )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3343:1: ( rule__Broadcast__WaitAssignment_2 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBroadcastAccess().getWaitAssignment_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3344:1: ( rule__Broadcast__WaitAssignment_2 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==68) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3344:2: rule__Broadcast__WaitAssignment_2
                    {
                    pushFollow(FOLLOW_rule__Broadcast__WaitAssignment_2_in_rule__Broadcast__Group__2__Impl7011);
                    rule__Broadcast__WaitAssignment_2();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBroadcastAccess().getWaitAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Broadcast__Group__2__Impl"


    // $ANTLR start "rule__If__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3360:1: rule__If__Group__0 : rule__If__Group__0__Impl rule__If__Group__1 ;
    public final void rule__If__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3364:1: ( rule__If__Group__0__Impl rule__If__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3365:2: rule__If__Group__0__Impl rule__If__Group__1
            {
            pushFollow(FOLLOW_rule__If__Group__0__Impl_in_rule__If__Group__07048);
            rule__If__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__If__Group__1_in_rule__If__Group__07051);
            rule__If__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group__0"


    // $ANTLR start "rule__If__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3372:1: rule__If__Group__0__Impl : ( ( rule__If__Group_0__0 ) ) ;
    public final void rule__If__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3376:1: ( ( ( rule__If__Group_0__0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3377:1: ( ( rule__If__Group_0__0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3377:1: ( ( rule__If__Group_0__0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3378:1: ( rule__If__Group_0__0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfAccess().getGroup_0()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3379:1: ( rule__If__Group_0__0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3379:2: rule__If__Group_0__0
            {
            pushFollow(FOLLOW_rule__If__Group_0__0_in_rule__If__Group__0__Impl7078);
            rule__If__Group_0__0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfAccess().getGroup_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group__0__Impl"


    // $ANTLR start "rule__If__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3389:1: rule__If__Group__1 : rule__If__Group__1__Impl rule__If__Group__2 ;
    public final void rule__If__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3393:1: ( rule__If__Group__1__Impl rule__If__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3394:2: rule__If__Group__1__Impl rule__If__Group__2
            {
            pushFollow(FOLLOW_rule__If__Group__1__Impl_in_rule__If__Group__17108);
            rule__If__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__If__Group__2_in_rule__If__Group__17111);
            rule__If__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group__1"


    // $ANTLR start "rule__If__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3401:1: rule__If__Group__1__Impl : ( ( rule__If__Group_1__0 )? ) ;
    public final void rule__If__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3405:1: ( ( ( rule__If__Group_1__0 )? ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3406:1: ( ( rule__If__Group_1__0 )? )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3406:1: ( ( rule__If__Group_1__0 )? )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3407:1: ( rule__If__Group_1__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfAccess().getGroup_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3408:1: ( rule__If__Group_1__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==30) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3408:2: rule__If__Group_1__0
                    {
                    pushFollow(FOLLOW_rule__If__Group_1__0_in_rule__If__Group__1__Impl7138);
                    rule__If__Group_1__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfAccess().getGroup_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group__1__Impl"


    // $ANTLR start "rule__If__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3418:1: rule__If__Group__2 : rule__If__Group__2__Impl ;
    public final void rule__If__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3422:1: ( rule__If__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3423:2: rule__If__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__If__Group__2__Impl_in_rule__If__Group__27169);
            rule__If__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group__2"


    // $ANTLR start "rule__If__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3429:1: rule__If__Group__2__Impl : ( 'end' ) ;
    public final void rule__If__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3433:1: ( ( 'end' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3434:1: ( 'end' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3434:1: ( 'end' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3435:1: 'end'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfAccess().getEndKeyword_2()); 
            }
            match(input,18,FOLLOW_18_in_rule__If__Group__2__Impl7197); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfAccess().getEndKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group__2__Impl"


    // $ANTLR start "rule__If__Group_0__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3454:1: rule__If__Group_0__0 : rule__If__Group_0__0__Impl rule__If__Group_0__1 ;
    public final void rule__If__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3458:1: ( rule__If__Group_0__0__Impl rule__If__Group_0__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3459:2: rule__If__Group_0__0__Impl rule__If__Group_0__1
            {
            pushFollow(FOLLOW_rule__If__Group_0__0__Impl_in_rule__If__Group_0__07234);
            rule__If__Group_0__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__If__Group_0__1_in_rule__If__Group_0__07237);
            rule__If__Group_0__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group_0__0"


    // $ANTLR start "rule__If__Group_0__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3466:1: rule__If__Group_0__0__Impl : ( 'if' ) ;
    public final void rule__If__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3470:1: ( ( 'if' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3471:1: ( 'if' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3471:1: ( 'if' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3472:1: 'if'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfAccess().getIfKeyword_0_0()); 
            }
            match(input,23,FOLLOW_23_in_rule__If__Group_0__0__Impl7265); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfAccess().getIfKeyword_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group_0__0__Impl"


    // $ANTLR start "rule__If__Group_0__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3485:1: rule__If__Group_0__1 : rule__If__Group_0__1__Impl rule__If__Group_0__2 ;
    public final void rule__If__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3489:1: ( rule__If__Group_0__1__Impl rule__If__Group_0__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3490:2: rule__If__Group_0__1__Impl rule__If__Group_0__2
            {
            pushFollow(FOLLOW_rule__If__Group_0__1__Impl_in_rule__If__Group_0__17296);
            rule__If__Group_0__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__If__Group_0__2_in_rule__If__Group_0__17299);
            rule__If__Group_0__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group_0__1"


    // $ANTLR start "rule__If__Group_0__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3497:1: rule__If__Group_0__1__Impl : ( ( rule__If__CondAssignment_0_1 ) ) ;
    public final void rule__If__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3501:1: ( ( ( rule__If__CondAssignment_0_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3502:1: ( ( rule__If__CondAssignment_0_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3502:1: ( ( rule__If__CondAssignment_0_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3503:1: ( rule__If__CondAssignment_0_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfAccess().getCondAssignment_0_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3504:1: ( rule__If__CondAssignment_0_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3504:2: rule__If__CondAssignment_0_1
            {
            pushFollow(FOLLOW_rule__If__CondAssignment_0_1_in_rule__If__Group_0__1__Impl7326);
            rule__If__CondAssignment_0_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfAccess().getCondAssignment_0_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group_0__1__Impl"


    // $ANTLR start "rule__If__Group_0__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3514:1: rule__If__Group_0__2 : rule__If__Group_0__2__Impl ;
    public final void rule__If__Group_0__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3518:1: ( rule__If__Group_0__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3519:2: rule__If__Group_0__2__Impl
            {
            pushFollow(FOLLOW_rule__If__Group_0__2__Impl_in_rule__If__Group_0__27356);
            rule__If__Group_0__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group_0__2"


    // $ANTLR start "rule__If__Group_0__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3525:1: rule__If__Group_0__2__Impl : ( ( rule__If__TrueAssignment_0_2 )* ) ;
    public final void rule__If__Group_0__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3529:1: ( ( ( rule__If__TrueAssignment_0_2 )* ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3530:1: ( ( rule__If__TrueAssignment_0_2 )* )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3530:1: ( ( rule__If__TrueAssignment_0_2 )* )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3531:1: ( rule__If__TrueAssignment_0_2 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfAccess().getTrueAssignment_0_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3532:1: ( rule__If__TrueAssignment_0_2 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( ((LA18_0>=22 && LA18_0<=24)||(LA18_0>=26 && LA18_0<=27)||LA18_0==29||LA18_0==31||LA18_0==33||(LA18_0>=35 && LA18_0<=38)||LA18_0==41||(LA18_0>=43 && LA18_0<=47)||(LA18_0>=61 && LA18_0<=63)||LA18_0==65) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3532:2: rule__If__TrueAssignment_0_2
            	    {
            	    pushFollow(FOLLOW_rule__If__TrueAssignment_0_2_in_rule__If__Group_0__2__Impl7383);
            	    rule__If__TrueAssignment_0_2();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfAccess().getTrueAssignment_0_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group_0__2__Impl"


    // $ANTLR start "rule__If__Group_1__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3548:1: rule__If__Group_1__0 : rule__If__Group_1__0__Impl rule__If__Group_1__1 ;
    public final void rule__If__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3552:1: ( rule__If__Group_1__0__Impl rule__If__Group_1__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3553:2: rule__If__Group_1__0__Impl rule__If__Group_1__1
            {
            pushFollow(FOLLOW_rule__If__Group_1__0__Impl_in_rule__If__Group_1__07420);
            rule__If__Group_1__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__If__Group_1__1_in_rule__If__Group_1__07423);
            rule__If__Group_1__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group_1__0"


    // $ANTLR start "rule__If__Group_1__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3560:1: rule__If__Group_1__0__Impl : ( 'else' ) ;
    public final void rule__If__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3564:1: ( ( 'else' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3565:1: ( 'else' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3565:1: ( 'else' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3566:1: 'else'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfAccess().getElseKeyword_1_0()); 
            }
            match(input,30,FOLLOW_30_in_rule__If__Group_1__0__Impl7451); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfAccess().getElseKeyword_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group_1__0__Impl"


    // $ANTLR start "rule__If__Group_1__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3579:1: rule__If__Group_1__1 : rule__If__Group_1__1__Impl ;
    public final void rule__If__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3583:1: ( rule__If__Group_1__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3584:2: rule__If__Group_1__1__Impl
            {
            pushFollow(FOLLOW_rule__If__Group_1__1__Impl_in_rule__If__Group_1__17482);
            rule__If__Group_1__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group_1__1"


    // $ANTLR start "rule__If__Group_1__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3590:1: rule__If__Group_1__1__Impl : ( ( rule__If__FalseAssignment_1_1 )* ) ;
    public final void rule__If__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3594:1: ( ( ( rule__If__FalseAssignment_1_1 )* ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3595:1: ( ( rule__If__FalseAssignment_1_1 )* )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3595:1: ( ( rule__If__FalseAssignment_1_1 )* )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3596:1: ( rule__If__FalseAssignment_1_1 )*
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfAccess().getFalseAssignment_1_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3597:1: ( rule__If__FalseAssignment_1_1 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( ((LA19_0>=22 && LA19_0<=24)||(LA19_0>=26 && LA19_0<=27)||LA19_0==29||LA19_0==31||LA19_0==33||(LA19_0>=35 && LA19_0<=38)||LA19_0==41||(LA19_0>=43 && LA19_0<=47)||(LA19_0>=61 && LA19_0<=63)||LA19_0==65) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3597:2: rule__If__FalseAssignment_1_1
            	    {
            	    pushFollow(FOLLOW_rule__If__FalseAssignment_1_1_in_rule__If__Group_1__1__Impl7509);
            	    rule__If__FalseAssignment_1_1();

            	    state._fsp--;
            	    if (state.failed) return ;

            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfAccess().getFalseAssignment_1_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__Group_1__1__Impl"


    // $ANTLR start "rule__Move__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3611:1: rule__Move__Group__0 : rule__Move__Group__0__Impl rule__Move__Group__1 ;
    public final void rule__Move__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3615:1: ( rule__Move__Group__0__Impl rule__Move__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3616:2: rule__Move__Group__0__Impl rule__Move__Group__1
            {
            pushFollow(FOLLOW_rule__Move__Group__0__Impl_in_rule__Move__Group__07544);
            rule__Move__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Move__Group__1_in_rule__Move__Group__07547);
            rule__Move__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Move__Group__0"


    // $ANTLR start "rule__Move__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3623:1: rule__Move__Group__0__Impl : ( 'move' ) ;
    public final void rule__Move__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3627:1: ( ( 'move' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3628:1: ( 'move' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3628:1: ( 'move' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3629:1: 'move'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMoveAccess().getMoveKeyword_0()); 
            }
            match(input,31,FOLLOW_31_in_rule__Move__Group__0__Impl7575); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMoveAccess().getMoveKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Move__Group__0__Impl"


    // $ANTLR start "rule__Move__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3642:1: rule__Move__Group__1 : rule__Move__Group__1__Impl rule__Move__Group__2 ;
    public final void rule__Move__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3646:1: ( rule__Move__Group__1__Impl rule__Move__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3647:2: rule__Move__Group__1__Impl rule__Move__Group__2
            {
            pushFollow(FOLLOW_rule__Move__Group__1__Impl_in_rule__Move__Group__17606);
            rule__Move__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Move__Group__2_in_rule__Move__Group__17609);
            rule__Move__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Move__Group__1"


    // $ANTLR start "rule__Move__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3654:1: rule__Move__Group__1__Impl : ( ( rule__Move__StepsAssignment_1 ) ) ;
    public final void rule__Move__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3658:1: ( ( ( rule__Move__StepsAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3659:1: ( ( rule__Move__StepsAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3659:1: ( ( rule__Move__StepsAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3660:1: ( rule__Move__StepsAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMoveAccess().getStepsAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3661:1: ( rule__Move__StepsAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3661:2: rule__Move__StepsAssignment_1
            {
            pushFollow(FOLLOW_rule__Move__StepsAssignment_1_in_rule__Move__Group__1__Impl7636);
            rule__Move__StepsAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getMoveAccess().getStepsAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Move__Group__1__Impl"


    // $ANTLR start "rule__Move__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3671:1: rule__Move__Group__2 : rule__Move__Group__2__Impl ;
    public final void rule__Move__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3675:1: ( rule__Move__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3676:2: rule__Move__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Move__Group__2__Impl_in_rule__Move__Group__27666);
            rule__Move__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Move__Group__2"


    // $ANTLR start "rule__Move__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3682:1: rule__Move__Group__2__Impl : ( 'steps' ) ;
    public final void rule__Move__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3686:1: ( ( 'steps' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3687:1: ( 'steps' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3687:1: ( 'steps' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3688:1: 'steps'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMoveAccess().getStepsKeyword_2()); 
            }
            match(input,32,FOLLOW_32_in_rule__Move__Group__2__Impl7694); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMoveAccess().getStepsKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Move__Group__2__Impl"


    // $ANTLR start "rule__TurnCW__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3707:1: rule__TurnCW__Group__0 : rule__TurnCW__Group__0__Impl rule__TurnCW__Group__1 ;
    public final void rule__TurnCW__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3711:1: ( rule__TurnCW__Group__0__Impl rule__TurnCW__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3712:2: rule__TurnCW__Group__0__Impl rule__TurnCW__Group__1
            {
            pushFollow(FOLLOW_rule__TurnCW__Group__0__Impl_in_rule__TurnCW__Group__07731);
            rule__TurnCW__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__TurnCW__Group__1_in_rule__TurnCW__Group__07734);
            rule__TurnCW__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCW__Group__0"


    // $ANTLR start "rule__TurnCW__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3719:1: rule__TurnCW__Group__0__Impl : ( 'turn cw' ) ;
    public final void rule__TurnCW__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3723:1: ( ( 'turn cw' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3724:1: ( 'turn cw' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3724:1: ( 'turn cw' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3725:1: 'turn cw'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTurnCWAccess().getTurnCwKeyword_0()); 
            }
            match(input,33,FOLLOW_33_in_rule__TurnCW__Group__0__Impl7762); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTurnCWAccess().getTurnCwKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCW__Group__0__Impl"


    // $ANTLR start "rule__TurnCW__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3738:1: rule__TurnCW__Group__1 : rule__TurnCW__Group__1__Impl rule__TurnCW__Group__2 ;
    public final void rule__TurnCW__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3742:1: ( rule__TurnCW__Group__1__Impl rule__TurnCW__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3743:2: rule__TurnCW__Group__1__Impl rule__TurnCW__Group__2
            {
            pushFollow(FOLLOW_rule__TurnCW__Group__1__Impl_in_rule__TurnCW__Group__17793);
            rule__TurnCW__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__TurnCW__Group__2_in_rule__TurnCW__Group__17796);
            rule__TurnCW__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCW__Group__1"


    // $ANTLR start "rule__TurnCW__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3750:1: rule__TurnCW__Group__1__Impl : ( ( rule__TurnCW__AngleAssignment_1 ) ) ;
    public final void rule__TurnCW__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3754:1: ( ( ( rule__TurnCW__AngleAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3755:1: ( ( rule__TurnCW__AngleAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3755:1: ( ( rule__TurnCW__AngleAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3756:1: ( rule__TurnCW__AngleAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTurnCWAccess().getAngleAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3757:1: ( rule__TurnCW__AngleAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3757:2: rule__TurnCW__AngleAssignment_1
            {
            pushFollow(FOLLOW_rule__TurnCW__AngleAssignment_1_in_rule__TurnCW__Group__1__Impl7823);
            rule__TurnCW__AngleAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTurnCWAccess().getAngleAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCW__Group__1__Impl"


    // $ANTLR start "rule__TurnCW__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3767:1: rule__TurnCW__Group__2 : rule__TurnCW__Group__2__Impl ;
    public final void rule__TurnCW__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3771:1: ( rule__TurnCW__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3772:2: rule__TurnCW__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__TurnCW__Group__2__Impl_in_rule__TurnCW__Group__27853);
            rule__TurnCW__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCW__Group__2"


    // $ANTLR start "rule__TurnCW__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3778:1: rule__TurnCW__Group__2__Impl : ( 'degrees' ) ;
    public final void rule__TurnCW__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3782:1: ( ( 'degrees' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3783:1: ( 'degrees' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3783:1: ( 'degrees' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3784:1: 'degrees'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTurnCWAccess().getDegreesKeyword_2()); 
            }
            match(input,34,FOLLOW_34_in_rule__TurnCW__Group__2__Impl7881); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTurnCWAccess().getDegreesKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCW__Group__2__Impl"


    // $ANTLR start "rule__TurnCCW__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3803:1: rule__TurnCCW__Group__0 : rule__TurnCCW__Group__0__Impl rule__TurnCCW__Group__1 ;
    public final void rule__TurnCCW__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3807:1: ( rule__TurnCCW__Group__0__Impl rule__TurnCCW__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3808:2: rule__TurnCCW__Group__0__Impl rule__TurnCCW__Group__1
            {
            pushFollow(FOLLOW_rule__TurnCCW__Group__0__Impl_in_rule__TurnCCW__Group__07918);
            rule__TurnCCW__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__TurnCCW__Group__1_in_rule__TurnCCW__Group__07921);
            rule__TurnCCW__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCCW__Group__0"


    // $ANTLR start "rule__TurnCCW__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3815:1: rule__TurnCCW__Group__0__Impl : ( 'turn ccw' ) ;
    public final void rule__TurnCCW__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3819:1: ( ( 'turn ccw' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3820:1: ( 'turn ccw' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3820:1: ( 'turn ccw' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3821:1: 'turn ccw'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTurnCCWAccess().getTurnCcwKeyword_0()); 
            }
            match(input,35,FOLLOW_35_in_rule__TurnCCW__Group__0__Impl7949); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTurnCCWAccess().getTurnCcwKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCCW__Group__0__Impl"


    // $ANTLR start "rule__TurnCCW__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3834:1: rule__TurnCCW__Group__1 : rule__TurnCCW__Group__1__Impl rule__TurnCCW__Group__2 ;
    public final void rule__TurnCCW__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3838:1: ( rule__TurnCCW__Group__1__Impl rule__TurnCCW__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3839:2: rule__TurnCCW__Group__1__Impl rule__TurnCCW__Group__2
            {
            pushFollow(FOLLOW_rule__TurnCCW__Group__1__Impl_in_rule__TurnCCW__Group__17980);
            rule__TurnCCW__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__TurnCCW__Group__2_in_rule__TurnCCW__Group__17983);
            rule__TurnCCW__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCCW__Group__1"


    // $ANTLR start "rule__TurnCCW__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3846:1: rule__TurnCCW__Group__1__Impl : ( ( rule__TurnCCW__AngleAssignment_1 ) ) ;
    public final void rule__TurnCCW__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3850:1: ( ( ( rule__TurnCCW__AngleAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3851:1: ( ( rule__TurnCCW__AngleAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3851:1: ( ( rule__TurnCCW__AngleAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3852:1: ( rule__TurnCCW__AngleAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTurnCCWAccess().getAngleAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3853:1: ( rule__TurnCCW__AngleAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3853:2: rule__TurnCCW__AngleAssignment_1
            {
            pushFollow(FOLLOW_rule__TurnCCW__AngleAssignment_1_in_rule__TurnCCW__Group__1__Impl8010);
            rule__TurnCCW__AngleAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTurnCCWAccess().getAngleAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCCW__Group__1__Impl"


    // $ANTLR start "rule__TurnCCW__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3863:1: rule__TurnCCW__Group__2 : rule__TurnCCW__Group__2__Impl ;
    public final void rule__TurnCCW__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3867:1: ( rule__TurnCCW__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3868:2: rule__TurnCCW__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__TurnCCW__Group__2__Impl_in_rule__TurnCCW__Group__28040);
            rule__TurnCCW__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCCW__Group__2"


    // $ANTLR start "rule__TurnCCW__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3874:1: rule__TurnCCW__Group__2__Impl : ( 'degrees' ) ;
    public final void rule__TurnCCW__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3878:1: ( ( 'degrees' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3879:1: ( 'degrees' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3879:1: ( 'degrees' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3880:1: 'degrees'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTurnCCWAccess().getDegreesKeyword_2()); 
            }
            match(input,34,FOLLOW_34_in_rule__TurnCCW__Group__2__Impl8068); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTurnCCWAccess().getDegreesKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCCW__Group__2__Impl"


    // $ANTLR start "rule__PointDirection__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3899:1: rule__PointDirection__Group__0 : rule__PointDirection__Group__0__Impl rule__PointDirection__Group__1 ;
    public final void rule__PointDirection__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3903:1: ( rule__PointDirection__Group__0__Impl rule__PointDirection__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3904:2: rule__PointDirection__Group__0__Impl rule__PointDirection__Group__1
            {
            pushFollow(FOLLOW_rule__PointDirection__Group__0__Impl_in_rule__PointDirection__Group__08105);
            rule__PointDirection__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__PointDirection__Group__1_in_rule__PointDirection__Group__08108);
            rule__PointDirection__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PointDirection__Group__0"


    // $ANTLR start "rule__PointDirection__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3911:1: rule__PointDirection__Group__0__Impl : ( 'point in direction' ) ;
    public final void rule__PointDirection__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3915:1: ( ( 'point in direction' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3916:1: ( 'point in direction' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3916:1: ( 'point in direction' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3917:1: 'point in direction'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPointDirectionAccess().getPointInDirectionKeyword_0()); 
            }
            match(input,36,FOLLOW_36_in_rule__PointDirection__Group__0__Impl8136); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPointDirectionAccess().getPointInDirectionKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PointDirection__Group__0__Impl"


    // $ANTLR start "rule__PointDirection__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3930:1: rule__PointDirection__Group__1 : rule__PointDirection__Group__1__Impl ;
    public final void rule__PointDirection__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3934:1: ( rule__PointDirection__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3935:2: rule__PointDirection__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__PointDirection__Group__1__Impl_in_rule__PointDirection__Group__18167);
            rule__PointDirection__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PointDirection__Group__1"


    // $ANTLR start "rule__PointDirection__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3941:1: rule__PointDirection__Group__1__Impl : ( ( rule__PointDirection__AngleAssignment_1 ) ) ;
    public final void rule__PointDirection__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3945:1: ( ( ( rule__PointDirection__AngleAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3946:1: ( ( rule__PointDirection__AngleAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3946:1: ( ( rule__PointDirection__AngleAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3947:1: ( rule__PointDirection__AngleAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPointDirectionAccess().getAngleAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3948:1: ( rule__PointDirection__AngleAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3948:2: rule__PointDirection__AngleAssignment_1
            {
            pushFollow(FOLLOW_rule__PointDirection__AngleAssignment_1_in_rule__PointDirection__Group__1__Impl8194);
            rule__PointDirection__AngleAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPointDirectionAccess().getAngleAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PointDirection__Group__1__Impl"


    // $ANTLR start "rule__PointTowards__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3962:1: rule__PointTowards__Group__0 : rule__PointTowards__Group__0__Impl rule__PointTowards__Group__1 ;
    public final void rule__PointTowards__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3966:1: ( rule__PointTowards__Group__0__Impl rule__PointTowards__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3967:2: rule__PointTowards__Group__0__Impl rule__PointTowards__Group__1
            {
            pushFollow(FOLLOW_rule__PointTowards__Group__0__Impl_in_rule__PointTowards__Group__08228);
            rule__PointTowards__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__PointTowards__Group__1_in_rule__PointTowards__Group__08231);
            rule__PointTowards__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PointTowards__Group__0"


    // $ANTLR start "rule__PointTowards__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3974:1: rule__PointTowards__Group__0__Impl : ( 'point towards' ) ;
    public final void rule__PointTowards__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3978:1: ( ( 'point towards' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3979:1: ( 'point towards' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3979:1: ( 'point towards' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3980:1: 'point towards'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPointTowardsAccess().getPointTowardsKeyword_0()); 
            }
            match(input,37,FOLLOW_37_in_rule__PointTowards__Group__0__Impl8259); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPointTowardsAccess().getPointTowardsKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PointTowards__Group__0__Impl"


    // $ANTLR start "rule__PointTowards__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3993:1: rule__PointTowards__Group__1 : rule__PointTowards__Group__1__Impl ;
    public final void rule__PointTowards__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3997:1: ( rule__PointTowards__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:3998:2: rule__PointTowards__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__PointTowards__Group__1__Impl_in_rule__PointTowards__Group__18290);
            rule__PointTowards__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PointTowards__Group__1"


    // $ANTLR start "rule__PointTowards__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4004:1: rule__PointTowards__Group__1__Impl : ( ( rule__PointTowards__ObjectAssignment_1 ) ) ;
    public final void rule__PointTowards__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4008:1: ( ( ( rule__PointTowards__ObjectAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4009:1: ( ( rule__PointTowards__ObjectAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4009:1: ( ( rule__PointTowards__ObjectAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4010:1: ( rule__PointTowards__ObjectAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPointTowardsAccess().getObjectAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4011:1: ( rule__PointTowards__ObjectAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4011:2: rule__PointTowards__ObjectAssignment_1
            {
            pushFollow(FOLLOW_rule__PointTowards__ObjectAssignment_1_in_rule__PointTowards__Group__1__Impl8317);
            rule__PointTowards__ObjectAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPointTowardsAccess().getObjectAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PointTowards__Group__1__Impl"


    // $ANTLR start "rule__GoToXY__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4025:1: rule__GoToXY__Group__0 : rule__GoToXY__Group__0__Impl rule__GoToXY__Group__1 ;
    public final void rule__GoToXY__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4029:1: ( rule__GoToXY__Group__0__Impl rule__GoToXY__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4030:2: rule__GoToXY__Group__0__Impl rule__GoToXY__Group__1
            {
            pushFollow(FOLLOW_rule__GoToXY__Group__0__Impl_in_rule__GoToXY__Group__08351);
            rule__GoToXY__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__GoToXY__Group__1_in_rule__GoToXY__Group__08354);
            rule__GoToXY__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoToXY__Group__0"


    // $ANTLR start "rule__GoToXY__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4037:1: rule__GoToXY__Group__0__Impl : ( 'go to' ) ;
    public final void rule__GoToXY__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4041:1: ( ( 'go to' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4042:1: ( 'go to' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4042:1: ( 'go to' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4043:1: 'go to'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoToXYAccess().getGoToKeyword_0()); 
            }
            match(input,38,FOLLOW_38_in_rule__GoToXY__Group__0__Impl8382); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoToXYAccess().getGoToKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoToXY__Group__0__Impl"


    // $ANTLR start "rule__GoToXY__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4056:1: rule__GoToXY__Group__1 : rule__GoToXY__Group__1__Impl rule__GoToXY__Group__2 ;
    public final void rule__GoToXY__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4060:1: ( rule__GoToXY__Group__1__Impl rule__GoToXY__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4061:2: rule__GoToXY__Group__1__Impl rule__GoToXY__Group__2
            {
            pushFollow(FOLLOW_rule__GoToXY__Group__1__Impl_in_rule__GoToXY__Group__18413);
            rule__GoToXY__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__GoToXY__Group__2_in_rule__GoToXY__Group__18416);
            rule__GoToXY__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoToXY__Group__1"


    // $ANTLR start "rule__GoToXY__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4068:1: rule__GoToXY__Group__1__Impl : ( 'x:' ) ;
    public final void rule__GoToXY__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4072:1: ( ( 'x:' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4073:1: ( 'x:' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4073:1: ( 'x:' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4074:1: 'x:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoToXYAccess().getXKeyword_1()); 
            }
            match(input,39,FOLLOW_39_in_rule__GoToXY__Group__1__Impl8444); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoToXYAccess().getXKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoToXY__Group__1__Impl"


    // $ANTLR start "rule__GoToXY__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4087:1: rule__GoToXY__Group__2 : rule__GoToXY__Group__2__Impl rule__GoToXY__Group__3 ;
    public final void rule__GoToXY__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4091:1: ( rule__GoToXY__Group__2__Impl rule__GoToXY__Group__3 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4092:2: rule__GoToXY__Group__2__Impl rule__GoToXY__Group__3
            {
            pushFollow(FOLLOW_rule__GoToXY__Group__2__Impl_in_rule__GoToXY__Group__28475);
            rule__GoToXY__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__GoToXY__Group__3_in_rule__GoToXY__Group__28478);
            rule__GoToXY__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoToXY__Group__2"


    // $ANTLR start "rule__GoToXY__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4099:1: rule__GoToXY__Group__2__Impl : ( ( rule__GoToXY__XAssignment_2 ) ) ;
    public final void rule__GoToXY__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4103:1: ( ( ( rule__GoToXY__XAssignment_2 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4104:1: ( ( rule__GoToXY__XAssignment_2 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4104:1: ( ( rule__GoToXY__XAssignment_2 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4105:1: ( rule__GoToXY__XAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoToXYAccess().getXAssignment_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4106:1: ( rule__GoToXY__XAssignment_2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4106:2: rule__GoToXY__XAssignment_2
            {
            pushFollow(FOLLOW_rule__GoToXY__XAssignment_2_in_rule__GoToXY__Group__2__Impl8505);
            rule__GoToXY__XAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoToXYAccess().getXAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoToXY__Group__2__Impl"


    // $ANTLR start "rule__GoToXY__Group__3"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4116:1: rule__GoToXY__Group__3 : rule__GoToXY__Group__3__Impl rule__GoToXY__Group__4 ;
    public final void rule__GoToXY__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4120:1: ( rule__GoToXY__Group__3__Impl rule__GoToXY__Group__4 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4121:2: rule__GoToXY__Group__3__Impl rule__GoToXY__Group__4
            {
            pushFollow(FOLLOW_rule__GoToXY__Group__3__Impl_in_rule__GoToXY__Group__38535);
            rule__GoToXY__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__GoToXY__Group__4_in_rule__GoToXY__Group__38538);
            rule__GoToXY__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoToXY__Group__3"


    // $ANTLR start "rule__GoToXY__Group__3__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4128:1: rule__GoToXY__Group__3__Impl : ( 'y:' ) ;
    public final void rule__GoToXY__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4132:1: ( ( 'y:' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4133:1: ( 'y:' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4133:1: ( 'y:' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4134:1: 'y:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoToXYAccess().getYKeyword_3()); 
            }
            match(input,40,FOLLOW_40_in_rule__GoToXY__Group__3__Impl8566); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoToXYAccess().getYKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoToXY__Group__3__Impl"


    // $ANTLR start "rule__GoToXY__Group__4"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4147:1: rule__GoToXY__Group__4 : rule__GoToXY__Group__4__Impl ;
    public final void rule__GoToXY__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4151:1: ( rule__GoToXY__Group__4__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4152:2: rule__GoToXY__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__GoToXY__Group__4__Impl_in_rule__GoToXY__Group__48597);
            rule__GoToXY__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoToXY__Group__4"


    // $ANTLR start "rule__GoToXY__Group__4__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4158:1: rule__GoToXY__Group__4__Impl : ( ( rule__GoToXY__YAssignment_4 ) ) ;
    public final void rule__GoToXY__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4162:1: ( ( ( rule__GoToXY__YAssignment_4 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4163:1: ( ( rule__GoToXY__YAssignment_4 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4163:1: ( ( rule__GoToXY__YAssignment_4 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4164:1: ( rule__GoToXY__YAssignment_4 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoToXYAccess().getYAssignment_4()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4165:1: ( rule__GoToXY__YAssignment_4 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4165:2: rule__GoToXY__YAssignment_4
            {
            pushFollow(FOLLOW_rule__GoToXY__YAssignment_4_in_rule__GoToXY__Group__4__Impl8624);
            rule__GoToXY__YAssignment_4();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoToXYAccess().getYAssignment_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoToXY__Group__4__Impl"


    // $ANTLR start "rule__GoTo__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4185:1: rule__GoTo__Group__0 : rule__GoTo__Group__0__Impl rule__GoTo__Group__1 ;
    public final void rule__GoTo__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4189:1: ( rule__GoTo__Group__0__Impl rule__GoTo__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4190:2: rule__GoTo__Group__0__Impl rule__GoTo__Group__1
            {
            pushFollow(FOLLOW_rule__GoTo__Group__0__Impl_in_rule__GoTo__Group__08664);
            rule__GoTo__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__GoTo__Group__1_in_rule__GoTo__Group__08667);
            rule__GoTo__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoTo__Group__0"


    // $ANTLR start "rule__GoTo__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4197:1: rule__GoTo__Group__0__Impl : ( 'go to' ) ;
    public final void rule__GoTo__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4201:1: ( ( 'go to' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4202:1: ( 'go to' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4202:1: ( 'go to' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4203:1: 'go to'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoToAccess().getGoToKeyword_0()); 
            }
            match(input,38,FOLLOW_38_in_rule__GoTo__Group__0__Impl8695); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoToAccess().getGoToKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoTo__Group__0__Impl"


    // $ANTLR start "rule__GoTo__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4216:1: rule__GoTo__Group__1 : rule__GoTo__Group__1__Impl ;
    public final void rule__GoTo__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4220:1: ( rule__GoTo__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4221:2: rule__GoTo__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__GoTo__Group__1__Impl_in_rule__GoTo__Group__18726);
            rule__GoTo__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoTo__Group__1"


    // $ANTLR start "rule__GoTo__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4227:1: rule__GoTo__Group__1__Impl : ( ( rule__GoTo__ObjectAssignment_1 ) ) ;
    public final void rule__GoTo__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4231:1: ( ( ( rule__GoTo__ObjectAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4232:1: ( ( rule__GoTo__ObjectAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4232:1: ( ( rule__GoTo__ObjectAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4233:1: ( rule__GoTo__ObjectAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoToAccess().getObjectAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4234:1: ( rule__GoTo__ObjectAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4234:2: rule__GoTo__ObjectAssignment_1
            {
            pushFollow(FOLLOW_rule__GoTo__ObjectAssignment_1_in_rule__GoTo__Group__1__Impl8753);
            rule__GoTo__ObjectAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoToAccess().getObjectAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoTo__Group__1__Impl"


    // $ANTLR start "rule__Glide__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4248:1: rule__Glide__Group__0 : rule__Glide__Group__0__Impl rule__Glide__Group__1 ;
    public final void rule__Glide__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4252:1: ( rule__Glide__Group__0__Impl rule__Glide__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4253:2: rule__Glide__Group__0__Impl rule__Glide__Group__1
            {
            pushFollow(FOLLOW_rule__Glide__Group__0__Impl_in_rule__Glide__Group__08787);
            rule__Glide__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Glide__Group__1_in_rule__Glide__Group__08790);
            rule__Glide__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Glide__Group__0"


    // $ANTLR start "rule__Glide__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4260:1: rule__Glide__Group__0__Impl : ( 'glide' ) ;
    public final void rule__Glide__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4264:1: ( ( 'glide' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4265:1: ( 'glide' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4265:1: ( 'glide' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4266:1: 'glide'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGlideAccess().getGlideKeyword_0()); 
            }
            match(input,41,FOLLOW_41_in_rule__Glide__Group__0__Impl8818); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGlideAccess().getGlideKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Glide__Group__0__Impl"


    // $ANTLR start "rule__Glide__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4279:1: rule__Glide__Group__1 : rule__Glide__Group__1__Impl rule__Glide__Group__2 ;
    public final void rule__Glide__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4283:1: ( rule__Glide__Group__1__Impl rule__Glide__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4284:2: rule__Glide__Group__1__Impl rule__Glide__Group__2
            {
            pushFollow(FOLLOW_rule__Glide__Group__1__Impl_in_rule__Glide__Group__18849);
            rule__Glide__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Glide__Group__2_in_rule__Glide__Group__18852);
            rule__Glide__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Glide__Group__1"


    // $ANTLR start "rule__Glide__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4291:1: rule__Glide__Group__1__Impl : ( ( rule__Glide__DurationAssignment_1 ) ) ;
    public final void rule__Glide__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4295:1: ( ( ( rule__Glide__DurationAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4296:1: ( ( rule__Glide__DurationAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4296:1: ( ( rule__Glide__DurationAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4297:1: ( rule__Glide__DurationAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGlideAccess().getDurationAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4298:1: ( rule__Glide__DurationAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4298:2: rule__Glide__DurationAssignment_1
            {
            pushFollow(FOLLOW_rule__Glide__DurationAssignment_1_in_rule__Glide__Group__1__Impl8879);
            rule__Glide__DurationAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGlideAccess().getDurationAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Glide__Group__1__Impl"


    // $ANTLR start "rule__Glide__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4308:1: rule__Glide__Group__2 : rule__Glide__Group__2__Impl rule__Glide__Group__3 ;
    public final void rule__Glide__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4312:1: ( rule__Glide__Group__2__Impl rule__Glide__Group__3 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4313:2: rule__Glide__Group__2__Impl rule__Glide__Group__3
            {
            pushFollow(FOLLOW_rule__Glide__Group__2__Impl_in_rule__Glide__Group__28909);
            rule__Glide__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Glide__Group__3_in_rule__Glide__Group__28912);
            rule__Glide__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Glide__Group__2"


    // $ANTLR start "rule__Glide__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4320:1: rule__Glide__Group__2__Impl : ( 'secs to x:' ) ;
    public final void rule__Glide__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4324:1: ( ( 'secs to x:' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4325:1: ( 'secs to x:' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4325:1: ( 'secs to x:' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4326:1: 'secs to x:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGlideAccess().getSecsToXKeyword_2()); 
            }
            match(input,42,FOLLOW_42_in_rule__Glide__Group__2__Impl8940); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGlideAccess().getSecsToXKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Glide__Group__2__Impl"


    // $ANTLR start "rule__Glide__Group__3"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4339:1: rule__Glide__Group__3 : rule__Glide__Group__3__Impl rule__Glide__Group__4 ;
    public final void rule__Glide__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4343:1: ( rule__Glide__Group__3__Impl rule__Glide__Group__4 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4344:2: rule__Glide__Group__3__Impl rule__Glide__Group__4
            {
            pushFollow(FOLLOW_rule__Glide__Group__3__Impl_in_rule__Glide__Group__38971);
            rule__Glide__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Glide__Group__4_in_rule__Glide__Group__38974);
            rule__Glide__Group__4();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Glide__Group__3"


    // $ANTLR start "rule__Glide__Group__3__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4351:1: rule__Glide__Group__3__Impl : ( ( rule__Glide__XAssignment_3 ) ) ;
    public final void rule__Glide__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4355:1: ( ( ( rule__Glide__XAssignment_3 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4356:1: ( ( rule__Glide__XAssignment_3 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4356:1: ( ( rule__Glide__XAssignment_3 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4357:1: ( rule__Glide__XAssignment_3 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGlideAccess().getXAssignment_3()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4358:1: ( rule__Glide__XAssignment_3 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4358:2: rule__Glide__XAssignment_3
            {
            pushFollow(FOLLOW_rule__Glide__XAssignment_3_in_rule__Glide__Group__3__Impl9001);
            rule__Glide__XAssignment_3();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGlideAccess().getXAssignment_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Glide__Group__3__Impl"


    // $ANTLR start "rule__Glide__Group__4"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4368:1: rule__Glide__Group__4 : rule__Glide__Group__4__Impl rule__Glide__Group__5 ;
    public final void rule__Glide__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4372:1: ( rule__Glide__Group__4__Impl rule__Glide__Group__5 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4373:2: rule__Glide__Group__4__Impl rule__Glide__Group__5
            {
            pushFollow(FOLLOW_rule__Glide__Group__4__Impl_in_rule__Glide__Group__49031);
            rule__Glide__Group__4__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Glide__Group__5_in_rule__Glide__Group__49034);
            rule__Glide__Group__5();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Glide__Group__4"


    // $ANTLR start "rule__Glide__Group__4__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4380:1: rule__Glide__Group__4__Impl : ( 'y:' ) ;
    public final void rule__Glide__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4384:1: ( ( 'y:' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4385:1: ( 'y:' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4385:1: ( 'y:' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4386:1: 'y:'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGlideAccess().getYKeyword_4()); 
            }
            match(input,40,FOLLOW_40_in_rule__Glide__Group__4__Impl9062); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGlideAccess().getYKeyword_4()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Glide__Group__4__Impl"


    // $ANTLR start "rule__Glide__Group__5"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4399:1: rule__Glide__Group__5 : rule__Glide__Group__5__Impl ;
    public final void rule__Glide__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4403:1: ( rule__Glide__Group__5__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4404:2: rule__Glide__Group__5__Impl
            {
            pushFollow(FOLLOW_rule__Glide__Group__5__Impl_in_rule__Glide__Group__59093);
            rule__Glide__Group__5__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Glide__Group__5"


    // $ANTLR start "rule__Glide__Group__5__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4410:1: rule__Glide__Group__5__Impl : ( ( rule__Glide__YAssignment_5 ) ) ;
    public final void rule__Glide__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4414:1: ( ( ( rule__Glide__YAssignment_5 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4415:1: ( ( rule__Glide__YAssignment_5 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4415:1: ( ( rule__Glide__YAssignment_5 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4416:1: ( rule__Glide__YAssignment_5 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGlideAccess().getYAssignment_5()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4417:1: ( rule__Glide__YAssignment_5 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4417:2: rule__Glide__YAssignment_5
            {
            pushFollow(FOLLOW_rule__Glide__YAssignment_5_in_rule__Glide__Group__5__Impl9120);
            rule__Glide__YAssignment_5();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGlideAccess().getYAssignment_5()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Glide__Group__5__Impl"


    // $ANTLR start "rule__ChangeX__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4439:1: rule__ChangeX__Group__0 : rule__ChangeX__Group__0__Impl rule__ChangeX__Group__1 ;
    public final void rule__ChangeX__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4443:1: ( rule__ChangeX__Group__0__Impl rule__ChangeX__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4444:2: rule__ChangeX__Group__0__Impl rule__ChangeX__Group__1
            {
            pushFollow(FOLLOW_rule__ChangeX__Group__0__Impl_in_rule__ChangeX__Group__09162);
            rule__ChangeX__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__ChangeX__Group__1_in_rule__ChangeX__Group__09165);
            rule__ChangeX__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ChangeX__Group__0"


    // $ANTLR start "rule__ChangeX__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4451:1: rule__ChangeX__Group__0__Impl : ( 'change x by' ) ;
    public final void rule__ChangeX__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4455:1: ( ( 'change x by' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4456:1: ( 'change x by' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4456:1: ( 'change x by' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4457:1: 'change x by'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getChangeXAccess().getChangeXByKeyword_0()); 
            }
            match(input,43,FOLLOW_43_in_rule__ChangeX__Group__0__Impl9193); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getChangeXAccess().getChangeXByKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ChangeX__Group__0__Impl"


    // $ANTLR start "rule__ChangeX__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4470:1: rule__ChangeX__Group__1 : rule__ChangeX__Group__1__Impl ;
    public final void rule__ChangeX__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4474:1: ( rule__ChangeX__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4475:2: rule__ChangeX__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__ChangeX__Group__1__Impl_in_rule__ChangeX__Group__19224);
            rule__ChangeX__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ChangeX__Group__1"


    // $ANTLR start "rule__ChangeX__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4481:1: rule__ChangeX__Group__1__Impl : ( ( rule__ChangeX__XAssignment_1 ) ) ;
    public final void rule__ChangeX__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4485:1: ( ( ( rule__ChangeX__XAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4486:1: ( ( rule__ChangeX__XAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4486:1: ( ( rule__ChangeX__XAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4487:1: ( rule__ChangeX__XAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getChangeXAccess().getXAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4488:1: ( rule__ChangeX__XAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4488:2: rule__ChangeX__XAssignment_1
            {
            pushFollow(FOLLOW_rule__ChangeX__XAssignment_1_in_rule__ChangeX__Group__1__Impl9251);
            rule__ChangeX__XAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getChangeXAccess().getXAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ChangeX__Group__1__Impl"


    // $ANTLR start "rule__ChangeY__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4502:1: rule__ChangeY__Group__0 : rule__ChangeY__Group__0__Impl rule__ChangeY__Group__1 ;
    public final void rule__ChangeY__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4506:1: ( rule__ChangeY__Group__0__Impl rule__ChangeY__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4507:2: rule__ChangeY__Group__0__Impl rule__ChangeY__Group__1
            {
            pushFollow(FOLLOW_rule__ChangeY__Group__0__Impl_in_rule__ChangeY__Group__09285);
            rule__ChangeY__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__ChangeY__Group__1_in_rule__ChangeY__Group__09288);
            rule__ChangeY__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ChangeY__Group__0"


    // $ANTLR start "rule__ChangeY__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4514:1: rule__ChangeY__Group__0__Impl : ( 'change y by' ) ;
    public final void rule__ChangeY__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4518:1: ( ( 'change y by' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4519:1: ( 'change y by' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4519:1: ( 'change y by' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4520:1: 'change y by'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getChangeYAccess().getChangeYByKeyword_0()); 
            }
            match(input,44,FOLLOW_44_in_rule__ChangeY__Group__0__Impl9316); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getChangeYAccess().getChangeYByKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ChangeY__Group__0__Impl"


    // $ANTLR start "rule__ChangeY__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4533:1: rule__ChangeY__Group__1 : rule__ChangeY__Group__1__Impl ;
    public final void rule__ChangeY__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4537:1: ( rule__ChangeY__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4538:2: rule__ChangeY__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__ChangeY__Group__1__Impl_in_rule__ChangeY__Group__19347);
            rule__ChangeY__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ChangeY__Group__1"


    // $ANTLR start "rule__ChangeY__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4544:1: rule__ChangeY__Group__1__Impl : ( ( rule__ChangeY__YAssignment_1 ) ) ;
    public final void rule__ChangeY__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4548:1: ( ( ( rule__ChangeY__YAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4549:1: ( ( rule__ChangeY__YAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4549:1: ( ( rule__ChangeY__YAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4550:1: ( rule__ChangeY__YAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getChangeYAccess().getYAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4551:1: ( rule__ChangeY__YAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4551:2: rule__ChangeY__YAssignment_1
            {
            pushFollow(FOLLOW_rule__ChangeY__YAssignment_1_in_rule__ChangeY__Group__1__Impl9374);
            rule__ChangeY__YAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getChangeYAccess().getYAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ChangeY__Group__1__Impl"


    // $ANTLR start "rule__SetX__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4565:1: rule__SetX__Group__0 : rule__SetX__Group__0__Impl rule__SetX__Group__1 ;
    public final void rule__SetX__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4569:1: ( rule__SetX__Group__0__Impl rule__SetX__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4570:2: rule__SetX__Group__0__Impl rule__SetX__Group__1
            {
            pushFollow(FOLLOW_rule__SetX__Group__0__Impl_in_rule__SetX__Group__09408);
            rule__SetX__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__SetX__Group__1_in_rule__SetX__Group__09411);
            rule__SetX__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetX__Group__0"


    // $ANTLR start "rule__SetX__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4577:1: rule__SetX__Group__0__Impl : ( 'set x to' ) ;
    public final void rule__SetX__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4581:1: ( ( 'set x to' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4582:1: ( 'set x to' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4582:1: ( 'set x to' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4583:1: 'set x to'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSetXAccess().getSetXToKeyword_0()); 
            }
            match(input,45,FOLLOW_45_in_rule__SetX__Group__0__Impl9439); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSetXAccess().getSetXToKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetX__Group__0__Impl"


    // $ANTLR start "rule__SetX__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4596:1: rule__SetX__Group__1 : rule__SetX__Group__1__Impl ;
    public final void rule__SetX__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4600:1: ( rule__SetX__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4601:2: rule__SetX__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__SetX__Group__1__Impl_in_rule__SetX__Group__19470);
            rule__SetX__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetX__Group__1"


    // $ANTLR start "rule__SetX__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4607:1: rule__SetX__Group__1__Impl : ( ( rule__SetX__XAssignment_1 ) ) ;
    public final void rule__SetX__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4611:1: ( ( ( rule__SetX__XAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4612:1: ( ( rule__SetX__XAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4612:1: ( ( rule__SetX__XAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4613:1: ( rule__SetX__XAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSetXAccess().getXAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4614:1: ( rule__SetX__XAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4614:2: rule__SetX__XAssignment_1
            {
            pushFollow(FOLLOW_rule__SetX__XAssignment_1_in_rule__SetX__Group__1__Impl9497);
            rule__SetX__XAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSetXAccess().getXAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetX__Group__1__Impl"


    // $ANTLR start "rule__SetY__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4628:1: rule__SetY__Group__0 : rule__SetY__Group__0__Impl rule__SetY__Group__1 ;
    public final void rule__SetY__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4632:1: ( rule__SetY__Group__0__Impl rule__SetY__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4633:2: rule__SetY__Group__0__Impl rule__SetY__Group__1
            {
            pushFollow(FOLLOW_rule__SetY__Group__0__Impl_in_rule__SetY__Group__09531);
            rule__SetY__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__SetY__Group__1_in_rule__SetY__Group__09534);
            rule__SetY__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetY__Group__0"


    // $ANTLR start "rule__SetY__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4640:1: rule__SetY__Group__0__Impl : ( 'set y to' ) ;
    public final void rule__SetY__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4644:1: ( ( 'set y to' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4645:1: ( 'set y to' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4645:1: ( 'set y to' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4646:1: 'set y to'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSetYAccess().getSetYToKeyword_0()); 
            }
            match(input,46,FOLLOW_46_in_rule__SetY__Group__0__Impl9562); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSetYAccess().getSetYToKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetY__Group__0__Impl"


    // $ANTLR start "rule__SetY__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4659:1: rule__SetY__Group__1 : rule__SetY__Group__1__Impl ;
    public final void rule__SetY__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4663:1: ( rule__SetY__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4664:2: rule__SetY__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__SetY__Group__1__Impl_in_rule__SetY__Group__19593);
            rule__SetY__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetY__Group__1"


    // $ANTLR start "rule__SetY__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4670:1: rule__SetY__Group__1__Impl : ( ( rule__SetY__YAssignment_1 ) ) ;
    public final void rule__SetY__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4674:1: ( ( ( rule__SetY__YAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4675:1: ( ( rule__SetY__YAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4675:1: ( ( rule__SetY__YAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4676:1: ( rule__SetY__YAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSetYAccess().getYAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4677:1: ( rule__SetY__YAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4677:2: rule__SetY__YAssignment_1
            {
            pushFollow(FOLLOW_rule__SetY__YAssignment_1_in_rule__SetY__Group__1__Impl9620);
            rule__SetY__YAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSetYAccess().getYAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetY__Group__1__Impl"


    // $ANTLR start "rule__IfEdge__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4691:1: rule__IfEdge__Group__0 : rule__IfEdge__Group__0__Impl rule__IfEdge__Group__1 ;
    public final void rule__IfEdge__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4695:1: ( rule__IfEdge__Group__0__Impl rule__IfEdge__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4696:2: rule__IfEdge__Group__0__Impl rule__IfEdge__Group__1
            {
            pushFollow(FOLLOW_rule__IfEdge__Group__0__Impl_in_rule__IfEdge__Group__09654);
            rule__IfEdge__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__IfEdge__Group__1_in_rule__IfEdge__Group__09657);
            rule__IfEdge__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfEdge__Group__0"


    // $ANTLR start "rule__IfEdge__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4703:1: rule__IfEdge__Group__0__Impl : ( 'if on edge, bounce' ) ;
    public final void rule__IfEdge__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4707:1: ( ( 'if on edge, bounce' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4708:1: ( 'if on edge, bounce' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4708:1: ( 'if on edge, bounce' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4709:1: 'if on edge, bounce'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfEdgeAccess().getIfOnEdgeBounceKeyword_0()); 
            }
            match(input,47,FOLLOW_47_in_rule__IfEdge__Group__0__Impl9685); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfEdgeAccess().getIfOnEdgeBounceKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfEdge__Group__0__Impl"


    // $ANTLR start "rule__IfEdge__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4722:1: rule__IfEdge__Group__1 : rule__IfEdge__Group__1__Impl ;
    public final void rule__IfEdge__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4726:1: ( rule__IfEdge__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4727:2: rule__IfEdge__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__IfEdge__Group__1__Impl_in_rule__IfEdge__Group__19716);
            rule__IfEdge__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfEdge__Group__1"


    // $ANTLR start "rule__IfEdge__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4733:1: rule__IfEdge__Group__1__Impl : ( () ) ;
    public final void rule__IfEdge__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4737:1: ( ( () ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4738:1: ( () )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4738:1: ( () )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4739:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfEdgeAccess().getIfEdgeAction_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4740:1: ()
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4742:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfEdgeAccess().getIfEdgeAction_1()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfEdge__Group__1__Impl"


    // $ANTLR start "rule__NumberTerminal__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4756:1: rule__NumberTerminal__Group__0 : rule__NumberTerminal__Group__0__Impl rule__NumberTerminal__Group__1 ;
    public final void rule__NumberTerminal__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4760:1: ( rule__NumberTerminal__Group__0__Impl rule__NumberTerminal__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4761:2: rule__NumberTerminal__Group__0__Impl rule__NumberTerminal__Group__1
            {
            pushFollow(FOLLOW_rule__NumberTerminal__Group__0__Impl_in_rule__NumberTerminal__Group__09778);
            rule__NumberTerminal__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__NumberTerminal__Group__1_in_rule__NumberTerminal__Group__09781);
            rule__NumberTerminal__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberTerminal__Group__0"


    // $ANTLR start "rule__NumberTerminal__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4768:1: rule__NumberTerminal__Group__0__Impl : ( '(' ) ;
    public final void rule__NumberTerminal__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4772:1: ( ( '(' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4773:1: ( '(' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4773:1: ( '(' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4774:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberTerminalAccess().getLeftParenthesisKeyword_0()); 
            }
            match(input,48,FOLLOW_48_in_rule__NumberTerminal__Group__0__Impl9809); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberTerminalAccess().getLeftParenthesisKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberTerminal__Group__0__Impl"


    // $ANTLR start "rule__NumberTerminal__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4787:1: rule__NumberTerminal__Group__1 : rule__NumberTerminal__Group__1__Impl rule__NumberTerminal__Group__2 ;
    public final void rule__NumberTerminal__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4791:1: ( rule__NumberTerminal__Group__1__Impl rule__NumberTerminal__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4792:2: rule__NumberTerminal__Group__1__Impl rule__NumberTerminal__Group__2
            {
            pushFollow(FOLLOW_rule__NumberTerminal__Group__1__Impl_in_rule__NumberTerminal__Group__19840);
            rule__NumberTerminal__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__NumberTerminal__Group__2_in_rule__NumberTerminal__Group__19843);
            rule__NumberTerminal__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberTerminal__Group__1"


    // $ANTLR start "rule__NumberTerminal__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4799:1: rule__NumberTerminal__Group__1__Impl : ( ruleNumber ) ;
    public final void rule__NumberTerminal__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4803:1: ( ( ruleNumber ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4804:1: ( ruleNumber )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4804:1: ( ruleNumber )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4805:1: ruleNumber
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberTerminalAccess().getNumberParserRuleCall_1()); 
            }
            pushFollow(FOLLOW_ruleNumber_in_rule__NumberTerminal__Group__1__Impl9870);
            ruleNumber();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberTerminalAccess().getNumberParserRuleCall_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberTerminal__Group__1__Impl"


    // $ANTLR start "rule__NumberTerminal__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4816:1: rule__NumberTerminal__Group__2 : rule__NumberTerminal__Group__2__Impl ;
    public final void rule__NumberTerminal__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4820:1: ( rule__NumberTerminal__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4821:2: rule__NumberTerminal__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__NumberTerminal__Group__2__Impl_in_rule__NumberTerminal__Group__29899);
            rule__NumberTerminal__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberTerminal__Group__2"


    // $ANTLR start "rule__NumberTerminal__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4827:1: rule__NumberTerminal__Group__2__Impl : ( ')' ) ;
    public final void rule__NumberTerminal__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4831:1: ( ( ')' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4832:1: ( ')' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4832:1: ( ')' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4833:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberTerminalAccess().getRightParenthesisKeyword_2()); 
            }
            match(input,49,FOLLOW_49_in_rule__NumberTerminal__Group__2__Impl9927); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberTerminalAccess().getRightParenthesisKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberTerminal__Group__2__Impl"


    // $ANTLR start "rule__NumberVar__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4852:1: rule__NumberVar__Group__0 : rule__NumberVar__Group__0__Impl rule__NumberVar__Group__1 ;
    public final void rule__NumberVar__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4856:1: ( rule__NumberVar__Group__0__Impl rule__NumberVar__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4857:2: rule__NumberVar__Group__0__Impl rule__NumberVar__Group__1
            {
            pushFollow(FOLLOW_rule__NumberVar__Group__0__Impl_in_rule__NumberVar__Group__09964);
            rule__NumberVar__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__NumberVar__Group__1_in_rule__NumberVar__Group__09967);
            rule__NumberVar__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVar__Group__0"


    // $ANTLR start "rule__NumberVar__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4864:1: rule__NumberVar__Group__0__Impl : ( '$' ) ;
    public final void rule__NumberVar__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4868:1: ( ( '$' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4869:1: ( '$' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4869:1: ( '$' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4870:1: '$'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarAccess().getDollarSignKeyword_0()); 
            }
            match(input,50,FOLLOW_50_in_rule__NumberVar__Group__0__Impl9995); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarAccess().getDollarSignKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVar__Group__0__Impl"


    // $ANTLR start "rule__NumberVar__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4883:1: rule__NumberVar__Group__1 : rule__NumberVar__Group__1__Impl ;
    public final void rule__NumberVar__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4887:1: ( rule__NumberVar__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4888:2: rule__NumberVar__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__NumberVar__Group__1__Impl_in_rule__NumberVar__Group__110026);
            rule__NumberVar__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVar__Group__1"


    // $ANTLR start "rule__NumberVar__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4894:1: rule__NumberVar__Group__1__Impl : ( ( rule__NumberVar__VarAssignment_1 ) ) ;
    public final void rule__NumberVar__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4898:1: ( ( ( rule__NumberVar__VarAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4899:1: ( ( rule__NumberVar__VarAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4899:1: ( ( rule__NumberVar__VarAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4900:1: ( rule__NumberVar__VarAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarAccess().getVarAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4901:1: ( rule__NumberVar__VarAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4901:2: rule__NumberVar__VarAssignment_1
            {
            pushFollow(FOLLOW_rule__NumberVar__VarAssignment_1_in_rule__NumberVar__Group__1__Impl10053);
            rule__NumberVar__VarAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarAccess().getVarAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVar__Group__1__Impl"


    // $ANTLR start "rule__NumberVarX__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4915:1: rule__NumberVarX__Group__0 : rule__NumberVarX__Group__0__Impl rule__NumberVarX__Group__1 ;
    public final void rule__NumberVarX__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4919:1: ( rule__NumberVarX__Group__0__Impl rule__NumberVarX__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4920:2: rule__NumberVarX__Group__0__Impl rule__NumberVarX__Group__1
            {
            pushFollow(FOLLOW_rule__NumberVarX__Group__0__Impl_in_rule__NumberVarX__Group__010087);
            rule__NumberVarX__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__NumberVarX__Group__1_in_rule__NumberVarX__Group__010090);
            rule__NumberVarX__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVarX__Group__0"


    // $ANTLR start "rule__NumberVarX__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4927:1: rule__NumberVarX__Group__0__Impl : ( 'x position' ) ;
    public final void rule__NumberVarX__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4931:1: ( ( 'x position' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4932:1: ( 'x position' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4932:1: ( 'x position' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4933:1: 'x position'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarXAccess().getXPositionKeyword_0()); 
            }
            match(input,12,FOLLOW_12_in_rule__NumberVarX__Group__0__Impl10118); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarXAccess().getXPositionKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVarX__Group__0__Impl"


    // $ANTLR start "rule__NumberVarX__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4946:1: rule__NumberVarX__Group__1 : rule__NumberVarX__Group__1__Impl ;
    public final void rule__NumberVarX__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4950:1: ( rule__NumberVarX__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4951:2: rule__NumberVarX__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__NumberVarX__Group__1__Impl_in_rule__NumberVarX__Group__110149);
            rule__NumberVarX__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVarX__Group__1"


    // $ANTLR start "rule__NumberVarX__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4957:1: rule__NumberVarX__Group__1__Impl : ( () ) ;
    public final void rule__NumberVarX__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4961:1: ( ( () ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4962:1: ( () )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4962:1: ( () )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4963:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarXAccess().getNumberVarXAction_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4964:1: ()
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4966:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarXAccess().getNumberVarXAction_1()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVarX__Group__1__Impl"


    // $ANTLR start "rule__NumberVarY__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4980:1: rule__NumberVarY__Group__0 : rule__NumberVarY__Group__0__Impl rule__NumberVarY__Group__1 ;
    public final void rule__NumberVarY__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4984:1: ( rule__NumberVarY__Group__0__Impl rule__NumberVarY__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4985:2: rule__NumberVarY__Group__0__Impl rule__NumberVarY__Group__1
            {
            pushFollow(FOLLOW_rule__NumberVarY__Group__0__Impl_in_rule__NumberVarY__Group__010211);
            rule__NumberVarY__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__NumberVarY__Group__1_in_rule__NumberVarY__Group__010214);
            rule__NumberVarY__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVarY__Group__0"


    // $ANTLR start "rule__NumberVarY__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4992:1: rule__NumberVarY__Group__0__Impl : ( 'y position' ) ;
    public final void rule__NumberVarY__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4996:1: ( ( 'y position' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4997:1: ( 'y position' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4997:1: ( 'y position' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:4998:1: 'y position'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarYAccess().getYPositionKeyword_0()); 
            }
            match(input,13,FOLLOW_13_in_rule__NumberVarY__Group__0__Impl10242); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarYAccess().getYPositionKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVarY__Group__0__Impl"


    // $ANTLR start "rule__NumberVarY__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5011:1: rule__NumberVarY__Group__1 : rule__NumberVarY__Group__1__Impl ;
    public final void rule__NumberVarY__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5015:1: ( rule__NumberVarY__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5016:2: rule__NumberVarY__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__NumberVarY__Group__1__Impl_in_rule__NumberVarY__Group__110273);
            rule__NumberVarY__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVarY__Group__1"


    // $ANTLR start "rule__NumberVarY__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5022:1: rule__NumberVarY__Group__1__Impl : ( () ) ;
    public final void rule__NumberVarY__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5026:1: ( ( () ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5027:1: ( () )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5027:1: ( () )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5028:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarYAccess().getNumberVarYAction_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5029:1: ()
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5031:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarYAccess().getNumberVarYAction_1()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVarY__Group__1__Impl"


    // $ANTLR start "rule__NumberVarD__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5045:1: rule__NumberVarD__Group__0 : rule__NumberVarD__Group__0__Impl rule__NumberVarD__Group__1 ;
    public final void rule__NumberVarD__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5049:1: ( rule__NumberVarD__Group__0__Impl rule__NumberVarD__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5050:2: rule__NumberVarD__Group__0__Impl rule__NumberVarD__Group__1
            {
            pushFollow(FOLLOW_rule__NumberVarD__Group__0__Impl_in_rule__NumberVarD__Group__010335);
            rule__NumberVarD__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__NumberVarD__Group__1_in_rule__NumberVarD__Group__010338);
            rule__NumberVarD__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVarD__Group__0"


    // $ANTLR start "rule__NumberVarD__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5057:1: rule__NumberVarD__Group__0__Impl : ( 'direction' ) ;
    public final void rule__NumberVarD__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5061:1: ( ( 'direction' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5062:1: ( 'direction' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5062:1: ( 'direction' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5063:1: 'direction'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarDAccess().getDirectionKeyword_0()); 
            }
            match(input,14,FOLLOW_14_in_rule__NumberVarD__Group__0__Impl10366); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarDAccess().getDirectionKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVarD__Group__0__Impl"


    // $ANTLR start "rule__NumberVarD__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5076:1: rule__NumberVarD__Group__1 : rule__NumberVarD__Group__1__Impl ;
    public final void rule__NumberVarD__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5080:1: ( rule__NumberVarD__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5081:2: rule__NumberVarD__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__NumberVarD__Group__1__Impl_in_rule__NumberVarD__Group__110397);
            rule__NumberVarD__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVarD__Group__1"


    // $ANTLR start "rule__NumberVarD__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5087:1: rule__NumberVarD__Group__1__Impl : ( () ) ;
    public final void rule__NumberVarD__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5091:1: ( ( () ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5092:1: ( () )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5092:1: ( () )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5093:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarDAccess().getNumberVarDAction_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5094:1: ()
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5096:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarDAccess().getNumberVarDAction_1()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVarD__Group__1__Impl"


    // $ANTLR start "rule__NumberObjVar__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5110:1: rule__NumberObjVar__Group__0 : rule__NumberObjVar__Group__0__Impl rule__NumberObjVar__Group__1 ;
    public final void rule__NumberObjVar__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5114:1: ( rule__NumberObjVar__Group__0__Impl rule__NumberObjVar__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5115:2: rule__NumberObjVar__Group__0__Impl rule__NumberObjVar__Group__1
            {
            pushFollow(FOLLOW_rule__NumberObjVar__Group__0__Impl_in_rule__NumberObjVar__Group__010459);
            rule__NumberObjVar__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__NumberObjVar__Group__1_in_rule__NumberObjVar__Group__010462);
            rule__NumberObjVar__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberObjVar__Group__0"


    // $ANTLR start "rule__NumberObjVar__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5122:1: rule__NumberObjVar__Group__0__Impl : ( ( rule__NumberObjVar__VarAssignment_0 ) ) ;
    public final void rule__NumberObjVar__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5126:1: ( ( ( rule__NumberObjVar__VarAssignment_0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5127:1: ( ( rule__NumberObjVar__VarAssignment_0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5127:1: ( ( rule__NumberObjVar__VarAssignment_0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5128:1: ( rule__NumberObjVar__VarAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberObjVarAccess().getVarAssignment_0()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5129:1: ( rule__NumberObjVar__VarAssignment_0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5129:2: rule__NumberObjVar__VarAssignment_0
            {
            pushFollow(FOLLOW_rule__NumberObjVar__VarAssignment_0_in_rule__NumberObjVar__Group__0__Impl10489);
            rule__NumberObjVar__VarAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberObjVarAccess().getVarAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberObjVar__Group__0__Impl"


    // $ANTLR start "rule__NumberObjVar__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5139:1: rule__NumberObjVar__Group__1 : rule__NumberObjVar__Group__1__Impl rule__NumberObjVar__Group__2 ;
    public final void rule__NumberObjVar__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5143:1: ( rule__NumberObjVar__Group__1__Impl rule__NumberObjVar__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5144:2: rule__NumberObjVar__Group__1__Impl rule__NumberObjVar__Group__2
            {
            pushFollow(FOLLOW_rule__NumberObjVar__Group__1__Impl_in_rule__NumberObjVar__Group__110519);
            rule__NumberObjVar__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__NumberObjVar__Group__2_in_rule__NumberObjVar__Group__110522);
            rule__NumberObjVar__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberObjVar__Group__1"


    // $ANTLR start "rule__NumberObjVar__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5151:1: rule__NumberObjVar__Group__1__Impl : ( 'of' ) ;
    public final void rule__NumberObjVar__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5155:1: ( ( 'of' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5156:1: ( 'of' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5156:1: ( 'of' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5157:1: 'of'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberObjVarAccess().getOfKeyword_1()); 
            }
            match(input,51,FOLLOW_51_in_rule__NumberObjVar__Group__1__Impl10550); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberObjVarAccess().getOfKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberObjVar__Group__1__Impl"


    // $ANTLR start "rule__NumberObjVar__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5170:1: rule__NumberObjVar__Group__2 : rule__NumberObjVar__Group__2__Impl ;
    public final void rule__NumberObjVar__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5174:1: ( rule__NumberObjVar__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5175:2: rule__NumberObjVar__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__NumberObjVar__Group__2__Impl_in_rule__NumberObjVar__Group__210581);
            rule__NumberObjVar__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberObjVar__Group__2"


    // $ANTLR start "rule__NumberObjVar__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5181:1: rule__NumberObjVar__Group__2__Impl : ( ( rule__NumberObjVar__ObjAssignment_2 ) ) ;
    public final void rule__NumberObjVar__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5185:1: ( ( ( rule__NumberObjVar__ObjAssignment_2 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5186:1: ( ( rule__NumberObjVar__ObjAssignment_2 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5186:1: ( ( rule__NumberObjVar__ObjAssignment_2 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5187:1: ( rule__NumberObjVar__ObjAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberObjVarAccess().getObjAssignment_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5188:1: ( rule__NumberObjVar__ObjAssignment_2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5188:2: rule__NumberObjVar__ObjAssignment_2
            {
            pushFollow(FOLLOW_rule__NumberObjVar__ObjAssignment_2_in_rule__NumberObjVar__Group__2__Impl10608);
            rule__NumberObjVar__ObjAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberObjVarAccess().getObjAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberObjVar__Group__2__Impl"


    // $ANTLR start "rule__BinaryTerminal__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5204:1: rule__BinaryTerminal__Group__0 : rule__BinaryTerminal__Group__0__Impl rule__BinaryTerminal__Group__1 ;
    public final void rule__BinaryTerminal__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5208:1: ( rule__BinaryTerminal__Group__0__Impl rule__BinaryTerminal__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5209:2: rule__BinaryTerminal__Group__0__Impl rule__BinaryTerminal__Group__1
            {
            pushFollow(FOLLOW_rule__BinaryTerminal__Group__0__Impl_in_rule__BinaryTerminal__Group__010644);
            rule__BinaryTerminal__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__BinaryTerminal__Group__1_in_rule__BinaryTerminal__Group__010647);
            rule__BinaryTerminal__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryTerminal__Group__0"


    // $ANTLR start "rule__BinaryTerminal__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5216:1: rule__BinaryTerminal__Group__0__Impl : ( '(' ) ;
    public final void rule__BinaryTerminal__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5220:1: ( ( '(' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5221:1: ( '(' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5221:1: ( '(' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5222:1: '('
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryTerminalAccess().getLeftParenthesisKeyword_0()); 
            }
            match(input,48,FOLLOW_48_in_rule__BinaryTerminal__Group__0__Impl10675); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryTerminalAccess().getLeftParenthesisKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryTerminal__Group__0__Impl"


    // $ANTLR start "rule__BinaryTerminal__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5235:1: rule__BinaryTerminal__Group__1 : rule__BinaryTerminal__Group__1__Impl rule__BinaryTerminal__Group__2 ;
    public final void rule__BinaryTerminal__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5239:1: ( rule__BinaryTerminal__Group__1__Impl rule__BinaryTerminal__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5240:2: rule__BinaryTerminal__Group__1__Impl rule__BinaryTerminal__Group__2
            {
            pushFollow(FOLLOW_rule__BinaryTerminal__Group__1__Impl_in_rule__BinaryTerminal__Group__110706);
            rule__BinaryTerminal__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__BinaryTerminal__Group__2_in_rule__BinaryTerminal__Group__110709);
            rule__BinaryTerminal__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryTerminal__Group__1"


    // $ANTLR start "rule__BinaryTerminal__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5247:1: rule__BinaryTerminal__Group__1__Impl : ( ruleBinary ) ;
    public final void rule__BinaryTerminal__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5251:1: ( ( ruleBinary ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5252:1: ( ruleBinary )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5252:1: ( ruleBinary )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5253:1: ruleBinary
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryTerminalAccess().getBinaryParserRuleCall_1()); 
            }
            pushFollow(FOLLOW_ruleBinary_in_rule__BinaryTerminal__Group__1__Impl10736);
            ruleBinary();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryTerminalAccess().getBinaryParserRuleCall_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryTerminal__Group__1__Impl"


    // $ANTLR start "rule__BinaryTerminal__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5264:1: rule__BinaryTerminal__Group__2 : rule__BinaryTerminal__Group__2__Impl ;
    public final void rule__BinaryTerminal__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5268:1: ( rule__BinaryTerminal__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5269:2: rule__BinaryTerminal__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__BinaryTerminal__Group__2__Impl_in_rule__BinaryTerminal__Group__210765);
            rule__BinaryTerminal__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryTerminal__Group__2"


    // $ANTLR start "rule__BinaryTerminal__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5275:1: rule__BinaryTerminal__Group__2__Impl : ( ')' ) ;
    public final void rule__BinaryTerminal__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5279:1: ( ( ')' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5280:1: ( ')' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5280:1: ( ')' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5281:1: ')'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryTerminalAccess().getRightParenthesisKeyword_2()); 
            }
            match(input,49,FOLLOW_49_in_rule__BinaryTerminal__Group__2__Impl10793); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryTerminalAccess().getRightParenthesisKeyword_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryTerminal__Group__2__Impl"


    // $ANTLR start "rule__BinaryVar__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5300:1: rule__BinaryVar__Group__0 : rule__BinaryVar__Group__0__Impl rule__BinaryVar__Group__1 ;
    public final void rule__BinaryVar__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5304:1: ( rule__BinaryVar__Group__0__Impl rule__BinaryVar__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5305:2: rule__BinaryVar__Group__0__Impl rule__BinaryVar__Group__1
            {
            pushFollow(FOLLOW_rule__BinaryVar__Group__0__Impl_in_rule__BinaryVar__Group__010830);
            rule__BinaryVar__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__BinaryVar__Group__1_in_rule__BinaryVar__Group__010833);
            rule__BinaryVar__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryVar__Group__0"


    // $ANTLR start "rule__BinaryVar__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5312:1: rule__BinaryVar__Group__0__Impl : ( '$' ) ;
    public final void rule__BinaryVar__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5316:1: ( ( '$' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5317:1: ( '$' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5317:1: ( '$' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5318:1: '$'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryVarAccess().getDollarSignKeyword_0()); 
            }
            match(input,50,FOLLOW_50_in_rule__BinaryVar__Group__0__Impl10861); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryVarAccess().getDollarSignKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryVar__Group__0__Impl"


    // $ANTLR start "rule__BinaryVar__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5331:1: rule__BinaryVar__Group__1 : rule__BinaryVar__Group__1__Impl ;
    public final void rule__BinaryVar__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5335:1: ( rule__BinaryVar__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5336:2: rule__BinaryVar__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__BinaryVar__Group__1__Impl_in_rule__BinaryVar__Group__110892);
            rule__BinaryVar__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryVar__Group__1"


    // $ANTLR start "rule__BinaryVar__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5342:1: rule__BinaryVar__Group__1__Impl : ( ( rule__BinaryVar__VarAssignment_1 ) ) ;
    public final void rule__BinaryVar__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5346:1: ( ( ( rule__BinaryVar__VarAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5347:1: ( ( rule__BinaryVar__VarAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5347:1: ( ( rule__BinaryVar__VarAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5348:1: ( rule__BinaryVar__VarAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryVarAccess().getVarAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5349:1: ( rule__BinaryVar__VarAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5349:2: rule__BinaryVar__VarAssignment_1
            {
            pushFollow(FOLLOW_rule__BinaryVar__VarAssignment_1_in_rule__BinaryVar__Group__1__Impl10919);
            rule__BinaryVar__VarAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryVarAccess().getVarAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryVar__Group__1__Impl"


    // $ANTLR start "rule__TouchsColor__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5363:1: rule__TouchsColor__Group__0 : rule__TouchsColor__Group__0__Impl rule__TouchsColor__Group__1 ;
    public final void rule__TouchsColor__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5367:1: ( rule__TouchsColor__Group__0__Impl rule__TouchsColor__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5368:2: rule__TouchsColor__Group__0__Impl rule__TouchsColor__Group__1
            {
            pushFollow(FOLLOW_rule__TouchsColor__Group__0__Impl_in_rule__TouchsColor__Group__010953);
            rule__TouchsColor__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__TouchsColor__Group__1_in_rule__TouchsColor__Group__010956);
            rule__TouchsColor__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TouchsColor__Group__0"


    // $ANTLR start "rule__TouchsColor__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5375:1: rule__TouchsColor__Group__0__Impl : ( 'touching' ) ;
    public final void rule__TouchsColor__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5379:1: ( ( 'touching' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5380:1: ( 'touching' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5380:1: ( 'touching' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5381:1: 'touching'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTouchsColorAccess().getTouchingKeyword_0()); 
            }
            match(input,52,FOLLOW_52_in_rule__TouchsColor__Group__0__Impl10984); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTouchsColorAccess().getTouchingKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TouchsColor__Group__0__Impl"


    // $ANTLR start "rule__TouchsColor__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5394:1: rule__TouchsColor__Group__1 : rule__TouchsColor__Group__1__Impl rule__TouchsColor__Group__2 ;
    public final void rule__TouchsColor__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5398:1: ( rule__TouchsColor__Group__1__Impl rule__TouchsColor__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5399:2: rule__TouchsColor__Group__1__Impl rule__TouchsColor__Group__2
            {
            pushFollow(FOLLOW_rule__TouchsColor__Group__1__Impl_in_rule__TouchsColor__Group__111015);
            rule__TouchsColor__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__TouchsColor__Group__2_in_rule__TouchsColor__Group__111018);
            rule__TouchsColor__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TouchsColor__Group__1"


    // $ANTLR start "rule__TouchsColor__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5406:1: rule__TouchsColor__Group__1__Impl : ( 'color' ) ;
    public final void rule__TouchsColor__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5410:1: ( ( 'color' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5411:1: ( 'color' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5411:1: ( 'color' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5412:1: 'color'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTouchsColorAccess().getColorKeyword_1()); 
            }
            match(input,53,FOLLOW_53_in_rule__TouchsColor__Group__1__Impl11046); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTouchsColorAccess().getColorKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TouchsColor__Group__1__Impl"


    // $ANTLR start "rule__TouchsColor__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5425:1: rule__TouchsColor__Group__2 : rule__TouchsColor__Group__2__Impl rule__TouchsColor__Group__3 ;
    public final void rule__TouchsColor__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5429:1: ( rule__TouchsColor__Group__2__Impl rule__TouchsColor__Group__3 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5430:2: rule__TouchsColor__Group__2__Impl rule__TouchsColor__Group__3
            {
            pushFollow(FOLLOW_rule__TouchsColor__Group__2__Impl_in_rule__TouchsColor__Group__211077);
            rule__TouchsColor__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__TouchsColor__Group__3_in_rule__TouchsColor__Group__211080);
            rule__TouchsColor__Group__3();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TouchsColor__Group__2"


    // $ANTLR start "rule__TouchsColor__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5437:1: rule__TouchsColor__Group__2__Impl : ( ( rule__TouchsColor__ColorAssignment_2 ) ) ;
    public final void rule__TouchsColor__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5441:1: ( ( ( rule__TouchsColor__ColorAssignment_2 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5442:1: ( ( rule__TouchsColor__ColorAssignment_2 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5442:1: ( ( rule__TouchsColor__ColorAssignment_2 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5443:1: ( rule__TouchsColor__ColorAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTouchsColorAccess().getColorAssignment_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5444:1: ( rule__TouchsColor__ColorAssignment_2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5444:2: rule__TouchsColor__ColorAssignment_2
            {
            pushFollow(FOLLOW_rule__TouchsColor__ColorAssignment_2_in_rule__TouchsColor__Group__2__Impl11107);
            rule__TouchsColor__ColorAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getTouchsColorAccess().getColorAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TouchsColor__Group__2__Impl"


    // $ANTLR start "rule__TouchsColor__Group__3"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5454:1: rule__TouchsColor__Group__3 : rule__TouchsColor__Group__3__Impl ;
    public final void rule__TouchsColor__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5458:1: ( rule__TouchsColor__Group__3__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5459:2: rule__TouchsColor__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__TouchsColor__Group__3__Impl_in_rule__TouchsColor__Group__311137);
            rule__TouchsColor__Group__3__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TouchsColor__Group__3"


    // $ANTLR start "rule__TouchsColor__Group__3__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5465:1: rule__TouchsColor__Group__3__Impl : ( '?' ) ;
    public final void rule__TouchsColor__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5469:1: ( ( '?' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5470:1: ( '?' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5470:1: ( '?' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5471:1: '?'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTouchsColorAccess().getQuestionMarkKeyword_3()); 
            }
            match(input,54,FOLLOW_54_in_rule__TouchsColor__Group__3__Impl11165); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTouchsColorAccess().getQuestionMarkKeyword_3()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TouchsColor__Group__3__Impl"


    // $ANTLR start "rule__Lt__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5492:1: rule__Lt__Group__0 : rule__Lt__Group__0__Impl rule__Lt__Group__1 ;
    public final void rule__Lt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5496:1: ( rule__Lt__Group__0__Impl rule__Lt__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5497:2: rule__Lt__Group__0__Impl rule__Lt__Group__1
            {
            pushFollow(FOLLOW_rule__Lt__Group__0__Impl_in_rule__Lt__Group__011204);
            rule__Lt__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Lt__Group__1_in_rule__Lt__Group__011207);
            rule__Lt__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lt__Group__0"


    // $ANTLR start "rule__Lt__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5504:1: rule__Lt__Group__0__Impl : ( ( rule__Lt__LeftAssignment_0 ) ) ;
    public final void rule__Lt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5508:1: ( ( ( rule__Lt__LeftAssignment_0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5509:1: ( ( rule__Lt__LeftAssignment_0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5509:1: ( ( rule__Lt__LeftAssignment_0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5510:1: ( rule__Lt__LeftAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLtAccess().getLeftAssignment_0()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5511:1: ( rule__Lt__LeftAssignment_0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5511:2: rule__Lt__LeftAssignment_0
            {
            pushFollow(FOLLOW_rule__Lt__LeftAssignment_0_in_rule__Lt__Group__0__Impl11234);
            rule__Lt__LeftAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLtAccess().getLeftAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lt__Group__0__Impl"


    // $ANTLR start "rule__Lt__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5521:1: rule__Lt__Group__1 : rule__Lt__Group__1__Impl rule__Lt__Group__2 ;
    public final void rule__Lt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5525:1: ( rule__Lt__Group__1__Impl rule__Lt__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5526:2: rule__Lt__Group__1__Impl rule__Lt__Group__2
            {
            pushFollow(FOLLOW_rule__Lt__Group__1__Impl_in_rule__Lt__Group__111264);
            rule__Lt__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Lt__Group__2_in_rule__Lt__Group__111267);
            rule__Lt__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lt__Group__1"


    // $ANTLR start "rule__Lt__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5533:1: rule__Lt__Group__1__Impl : ( '<' ) ;
    public final void rule__Lt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5537:1: ( ( '<' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5538:1: ( '<' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5538:1: ( '<' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5539:1: '<'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLtAccess().getLessThanSignKeyword_1()); 
            }
            match(input,55,FOLLOW_55_in_rule__Lt__Group__1__Impl11295); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLtAccess().getLessThanSignKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lt__Group__1__Impl"


    // $ANTLR start "rule__Lt__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5552:1: rule__Lt__Group__2 : rule__Lt__Group__2__Impl ;
    public final void rule__Lt__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5556:1: ( rule__Lt__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5557:2: rule__Lt__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Lt__Group__2__Impl_in_rule__Lt__Group__211326);
            rule__Lt__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lt__Group__2"


    // $ANTLR start "rule__Lt__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5563:1: rule__Lt__Group__2__Impl : ( ( rule__Lt__RightAssignment_2 ) ) ;
    public final void rule__Lt__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5567:1: ( ( ( rule__Lt__RightAssignment_2 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5568:1: ( ( rule__Lt__RightAssignment_2 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5568:1: ( ( rule__Lt__RightAssignment_2 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5569:1: ( rule__Lt__RightAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLtAccess().getRightAssignment_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5570:1: ( rule__Lt__RightAssignment_2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5570:2: rule__Lt__RightAssignment_2
            {
            pushFollow(FOLLOW_rule__Lt__RightAssignment_2_in_rule__Lt__Group__2__Impl11353);
            rule__Lt__RightAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getLtAccess().getRightAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lt__Group__2__Impl"


    // $ANTLR start "rule__Gt__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5586:1: rule__Gt__Group__0 : rule__Gt__Group__0__Impl rule__Gt__Group__1 ;
    public final void rule__Gt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5590:1: ( rule__Gt__Group__0__Impl rule__Gt__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5591:2: rule__Gt__Group__0__Impl rule__Gt__Group__1
            {
            pushFollow(FOLLOW_rule__Gt__Group__0__Impl_in_rule__Gt__Group__011389);
            rule__Gt__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Gt__Group__1_in_rule__Gt__Group__011392);
            rule__Gt__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gt__Group__0"


    // $ANTLR start "rule__Gt__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5598:1: rule__Gt__Group__0__Impl : ( ( rule__Gt__LeftAssignment_0 ) ) ;
    public final void rule__Gt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5602:1: ( ( ( rule__Gt__LeftAssignment_0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5603:1: ( ( rule__Gt__LeftAssignment_0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5603:1: ( ( rule__Gt__LeftAssignment_0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5604:1: ( rule__Gt__LeftAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGtAccess().getLeftAssignment_0()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5605:1: ( rule__Gt__LeftAssignment_0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5605:2: rule__Gt__LeftAssignment_0
            {
            pushFollow(FOLLOW_rule__Gt__LeftAssignment_0_in_rule__Gt__Group__0__Impl11419);
            rule__Gt__LeftAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGtAccess().getLeftAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gt__Group__0__Impl"


    // $ANTLR start "rule__Gt__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5615:1: rule__Gt__Group__1 : rule__Gt__Group__1__Impl rule__Gt__Group__2 ;
    public final void rule__Gt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5619:1: ( rule__Gt__Group__1__Impl rule__Gt__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5620:2: rule__Gt__Group__1__Impl rule__Gt__Group__2
            {
            pushFollow(FOLLOW_rule__Gt__Group__1__Impl_in_rule__Gt__Group__111449);
            rule__Gt__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Gt__Group__2_in_rule__Gt__Group__111452);
            rule__Gt__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gt__Group__1"


    // $ANTLR start "rule__Gt__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5627:1: rule__Gt__Group__1__Impl : ( '>' ) ;
    public final void rule__Gt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5631:1: ( ( '>' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5632:1: ( '>' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5632:1: ( '>' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5633:1: '>'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGtAccess().getGreaterThanSignKeyword_1()); 
            }
            match(input,56,FOLLOW_56_in_rule__Gt__Group__1__Impl11480); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGtAccess().getGreaterThanSignKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gt__Group__1__Impl"


    // $ANTLR start "rule__Gt__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5646:1: rule__Gt__Group__2 : rule__Gt__Group__2__Impl ;
    public final void rule__Gt__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5650:1: ( rule__Gt__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5651:2: rule__Gt__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Gt__Group__2__Impl_in_rule__Gt__Group__211511);
            rule__Gt__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gt__Group__2"


    // $ANTLR start "rule__Gt__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5657:1: rule__Gt__Group__2__Impl : ( ( rule__Gt__RightAssignment_2 ) ) ;
    public final void rule__Gt__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5661:1: ( ( ( rule__Gt__RightAssignment_2 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5662:1: ( ( rule__Gt__RightAssignment_2 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5662:1: ( ( rule__Gt__RightAssignment_2 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5663:1: ( rule__Gt__RightAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGtAccess().getRightAssignment_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5664:1: ( rule__Gt__RightAssignment_2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5664:2: rule__Gt__RightAssignment_2
            {
            pushFollow(FOLLOW_rule__Gt__RightAssignment_2_in_rule__Gt__Group__2__Impl11538);
            rule__Gt__RightAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getGtAccess().getRightAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gt__Group__2__Impl"


    // $ANTLR start "rule__Eq__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5680:1: rule__Eq__Group__0 : rule__Eq__Group__0__Impl rule__Eq__Group__1 ;
    public final void rule__Eq__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5684:1: ( rule__Eq__Group__0__Impl rule__Eq__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5685:2: rule__Eq__Group__0__Impl rule__Eq__Group__1
            {
            pushFollow(FOLLOW_rule__Eq__Group__0__Impl_in_rule__Eq__Group__011574);
            rule__Eq__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Eq__Group__1_in_rule__Eq__Group__011577);
            rule__Eq__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Eq__Group__0"


    // $ANTLR start "rule__Eq__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5692:1: rule__Eq__Group__0__Impl : ( ( rule__Eq__LeftAssignment_0 ) ) ;
    public final void rule__Eq__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5696:1: ( ( ( rule__Eq__LeftAssignment_0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5697:1: ( ( rule__Eq__LeftAssignment_0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5697:1: ( ( rule__Eq__LeftAssignment_0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5698:1: ( rule__Eq__LeftAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqAccess().getLeftAssignment_0()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5699:1: ( rule__Eq__LeftAssignment_0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5699:2: rule__Eq__LeftAssignment_0
            {
            pushFollow(FOLLOW_rule__Eq__LeftAssignment_0_in_rule__Eq__Group__0__Impl11604);
            rule__Eq__LeftAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqAccess().getLeftAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Eq__Group__0__Impl"


    // $ANTLR start "rule__Eq__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5709:1: rule__Eq__Group__1 : rule__Eq__Group__1__Impl rule__Eq__Group__2 ;
    public final void rule__Eq__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5713:1: ( rule__Eq__Group__1__Impl rule__Eq__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5714:2: rule__Eq__Group__1__Impl rule__Eq__Group__2
            {
            pushFollow(FOLLOW_rule__Eq__Group__1__Impl_in_rule__Eq__Group__111634);
            rule__Eq__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Eq__Group__2_in_rule__Eq__Group__111637);
            rule__Eq__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Eq__Group__1"


    // $ANTLR start "rule__Eq__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5721:1: rule__Eq__Group__1__Impl : ( '=' ) ;
    public final void rule__Eq__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5725:1: ( ( '=' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5726:1: ( '=' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5726:1: ( '=' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5727:1: '='
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqAccess().getEqualsSignKeyword_1()); 
            }
            match(input,57,FOLLOW_57_in_rule__Eq__Group__1__Impl11665); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqAccess().getEqualsSignKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Eq__Group__1__Impl"


    // $ANTLR start "rule__Eq__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5740:1: rule__Eq__Group__2 : rule__Eq__Group__2__Impl ;
    public final void rule__Eq__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5744:1: ( rule__Eq__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5745:2: rule__Eq__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Eq__Group__2__Impl_in_rule__Eq__Group__211696);
            rule__Eq__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Eq__Group__2"


    // $ANTLR start "rule__Eq__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5751:1: rule__Eq__Group__2__Impl : ( ( rule__Eq__RightAssignment_2 ) ) ;
    public final void rule__Eq__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5755:1: ( ( ( rule__Eq__RightAssignment_2 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5756:1: ( ( rule__Eq__RightAssignment_2 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5756:1: ( ( rule__Eq__RightAssignment_2 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5757:1: ( rule__Eq__RightAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqAccess().getRightAssignment_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5758:1: ( rule__Eq__RightAssignment_2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5758:2: rule__Eq__RightAssignment_2
            {
            pushFollow(FOLLOW_rule__Eq__RightAssignment_2_in_rule__Eq__Group__2__Impl11723);
            rule__Eq__RightAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqAccess().getRightAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Eq__Group__2__Impl"


    // $ANTLR start "rule__And__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5774:1: rule__And__Group__0 : rule__And__Group__0__Impl rule__And__Group__1 ;
    public final void rule__And__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5778:1: ( rule__And__Group__0__Impl rule__And__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5779:2: rule__And__Group__0__Impl rule__And__Group__1
            {
            pushFollow(FOLLOW_rule__And__Group__0__Impl_in_rule__And__Group__011759);
            rule__And__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__And__Group__1_in_rule__And__Group__011762);
            rule__And__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0"


    // $ANTLR start "rule__And__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5786:1: rule__And__Group__0__Impl : ( ( rule__And__LeftAssignment_0 ) ) ;
    public final void rule__And__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5790:1: ( ( ( rule__And__LeftAssignment_0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5791:1: ( ( rule__And__LeftAssignment_0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5791:1: ( ( rule__And__LeftAssignment_0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5792:1: ( rule__And__LeftAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAndAccess().getLeftAssignment_0()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5793:1: ( rule__And__LeftAssignment_0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5793:2: rule__And__LeftAssignment_0
            {
            pushFollow(FOLLOW_rule__And__LeftAssignment_0_in_rule__And__Group__0__Impl11789);
            rule__And__LeftAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAndAccess().getLeftAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__0__Impl"


    // $ANTLR start "rule__And__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5803:1: rule__And__Group__1 : rule__And__Group__1__Impl rule__And__Group__2 ;
    public final void rule__And__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5807:1: ( rule__And__Group__1__Impl rule__And__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5808:2: rule__And__Group__1__Impl rule__And__Group__2
            {
            pushFollow(FOLLOW_rule__And__Group__1__Impl_in_rule__And__Group__111819);
            rule__And__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__And__Group__2_in_rule__And__Group__111822);
            rule__And__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1"


    // $ANTLR start "rule__And__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5815:1: rule__And__Group__1__Impl : ( 'and' ) ;
    public final void rule__And__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5819:1: ( ( 'and' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5820:1: ( 'and' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5820:1: ( 'and' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5821:1: 'and'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAndAccess().getAndKeyword_1()); 
            }
            match(input,58,FOLLOW_58_in_rule__And__Group__1__Impl11850); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAndAccess().getAndKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__1__Impl"


    // $ANTLR start "rule__And__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5834:1: rule__And__Group__2 : rule__And__Group__2__Impl ;
    public final void rule__And__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5838:1: ( rule__And__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5839:2: rule__And__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__And__Group__2__Impl_in_rule__And__Group__211881);
            rule__And__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__2"


    // $ANTLR start "rule__And__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5845:1: rule__And__Group__2__Impl : ( ( rule__And__RightAssignment_2 ) ) ;
    public final void rule__And__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5849:1: ( ( ( rule__And__RightAssignment_2 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5850:1: ( ( rule__And__RightAssignment_2 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5850:1: ( ( rule__And__RightAssignment_2 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5851:1: ( rule__And__RightAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAndAccess().getRightAssignment_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5852:1: ( rule__And__RightAssignment_2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5852:2: rule__And__RightAssignment_2
            {
            pushFollow(FOLLOW_rule__And__RightAssignment_2_in_rule__And__Group__2__Impl11908);
            rule__And__RightAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getAndAccess().getRightAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__Group__2__Impl"


    // $ANTLR start "rule__Or__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5868:1: rule__Or__Group__0 : rule__Or__Group__0__Impl rule__Or__Group__1 ;
    public final void rule__Or__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5872:1: ( rule__Or__Group__0__Impl rule__Or__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5873:2: rule__Or__Group__0__Impl rule__Or__Group__1
            {
            pushFollow(FOLLOW_rule__Or__Group__0__Impl_in_rule__Or__Group__011944);
            rule__Or__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Or__Group__1_in_rule__Or__Group__011947);
            rule__Or__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0"


    // $ANTLR start "rule__Or__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5880:1: rule__Or__Group__0__Impl : ( ( rule__Or__LeftAssignment_0 ) ) ;
    public final void rule__Or__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5884:1: ( ( ( rule__Or__LeftAssignment_0 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5885:1: ( ( rule__Or__LeftAssignment_0 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5885:1: ( ( rule__Or__LeftAssignment_0 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5886:1: ( rule__Or__LeftAssignment_0 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOrAccess().getLeftAssignment_0()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5887:1: ( rule__Or__LeftAssignment_0 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5887:2: rule__Or__LeftAssignment_0
            {
            pushFollow(FOLLOW_rule__Or__LeftAssignment_0_in_rule__Or__Group__0__Impl11974);
            rule__Or__LeftAssignment_0();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getOrAccess().getLeftAssignment_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__0__Impl"


    // $ANTLR start "rule__Or__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5897:1: rule__Or__Group__1 : rule__Or__Group__1__Impl rule__Or__Group__2 ;
    public final void rule__Or__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5901:1: ( rule__Or__Group__1__Impl rule__Or__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5902:2: rule__Or__Group__1__Impl rule__Or__Group__2
            {
            pushFollow(FOLLOW_rule__Or__Group__1__Impl_in_rule__Or__Group__112004);
            rule__Or__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Or__Group__2_in_rule__Or__Group__112007);
            rule__Or__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1"


    // $ANTLR start "rule__Or__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5909:1: rule__Or__Group__1__Impl : ( 'or' ) ;
    public final void rule__Or__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5913:1: ( ( 'or' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5914:1: ( 'or' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5914:1: ( 'or' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5915:1: 'or'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOrAccess().getOrKeyword_1()); 
            }
            match(input,59,FOLLOW_59_in_rule__Or__Group__1__Impl12035); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getOrAccess().getOrKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__1__Impl"


    // $ANTLR start "rule__Or__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5928:1: rule__Or__Group__2 : rule__Or__Group__2__Impl ;
    public final void rule__Or__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5932:1: ( rule__Or__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5933:2: rule__Or__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Or__Group__2__Impl_in_rule__Or__Group__212066);
            rule__Or__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__2"


    // $ANTLR start "rule__Or__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5939:1: rule__Or__Group__2__Impl : ( ( rule__Or__RightAssignment_2 ) ) ;
    public final void rule__Or__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5943:1: ( ( ( rule__Or__RightAssignment_2 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5944:1: ( ( rule__Or__RightAssignment_2 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5944:1: ( ( rule__Or__RightAssignment_2 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5945:1: ( rule__Or__RightAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOrAccess().getRightAssignment_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5946:1: ( rule__Or__RightAssignment_2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5946:2: rule__Or__RightAssignment_2
            {
            pushFollow(FOLLOW_rule__Or__RightAssignment_2_in_rule__Or__Group__2__Impl12093);
            rule__Or__RightAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getOrAccess().getRightAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__Group__2__Impl"


    // $ANTLR start "rule__Not__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5962:1: rule__Not__Group__0 : rule__Not__Group__0__Impl rule__Not__Group__1 ;
    public final void rule__Not__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5966:1: ( rule__Not__Group__0__Impl rule__Not__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5967:2: rule__Not__Group__0__Impl rule__Not__Group__1
            {
            pushFollow(FOLLOW_rule__Not__Group__0__Impl_in_rule__Not__Group__012129);
            rule__Not__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Not__Group__1_in_rule__Not__Group__012132);
            rule__Not__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__0"


    // $ANTLR start "rule__Not__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5974:1: rule__Not__Group__0__Impl : ( 'not' ) ;
    public final void rule__Not__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5978:1: ( ( 'not' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5979:1: ( 'not' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5979:1: ( 'not' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5980:1: 'not'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNotAccess().getNotKeyword_0()); 
            }
            match(input,60,FOLLOW_60_in_rule__Not__Group__0__Impl12160); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNotAccess().getNotKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__0__Impl"


    // $ANTLR start "rule__Not__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5993:1: rule__Not__Group__1 : rule__Not__Group__1__Impl ;
    public final void rule__Not__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5997:1: ( rule__Not__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:5998:2: rule__Not__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Not__Group__1__Impl_in_rule__Not__Group__112191);
            rule__Not__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__1"


    // $ANTLR start "rule__Not__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6004:1: rule__Not__Group__1__Impl : ( ( rule__Not__ValueAssignment_1 ) ) ;
    public final void rule__Not__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6008:1: ( ( ( rule__Not__ValueAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6009:1: ( ( rule__Not__ValueAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6009:1: ( ( rule__Not__ValueAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6010:1: ( rule__Not__ValueAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNotAccess().getValueAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6011:1: ( rule__Not__ValueAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6011:2: rule__Not__ValueAssignment_1
            {
            pushFollow(FOLLOW_rule__Not__ValueAssignment_1_in_rule__Not__Group__1__Impl12218);
            rule__Not__ValueAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getNotAccess().getValueAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__Group__1__Impl"


    // $ANTLR start "rule__Hide__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6025:1: rule__Hide__Group__0 : rule__Hide__Group__0__Impl rule__Hide__Group__1 ;
    public final void rule__Hide__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6029:1: ( rule__Hide__Group__0__Impl rule__Hide__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6030:2: rule__Hide__Group__0__Impl rule__Hide__Group__1
            {
            pushFollow(FOLLOW_rule__Hide__Group__0__Impl_in_rule__Hide__Group__012252);
            rule__Hide__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Hide__Group__1_in_rule__Hide__Group__012255);
            rule__Hide__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Hide__Group__0"


    // $ANTLR start "rule__Hide__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6037:1: rule__Hide__Group__0__Impl : ( 'hide' ) ;
    public final void rule__Hide__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6041:1: ( ( 'hide' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6042:1: ( 'hide' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6042:1: ( 'hide' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6043:1: 'hide'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getHideAccess().getHideKeyword_0()); 
            }
            match(input,61,FOLLOW_61_in_rule__Hide__Group__0__Impl12283); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getHideAccess().getHideKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Hide__Group__0__Impl"


    // $ANTLR start "rule__Hide__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6056:1: rule__Hide__Group__1 : rule__Hide__Group__1__Impl ;
    public final void rule__Hide__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6060:1: ( rule__Hide__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6061:2: rule__Hide__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Hide__Group__1__Impl_in_rule__Hide__Group__112314);
            rule__Hide__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Hide__Group__1"


    // $ANTLR start "rule__Hide__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6067:1: rule__Hide__Group__1__Impl : ( () ) ;
    public final void rule__Hide__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6071:1: ( ( () ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6072:1: ( () )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6072:1: ( () )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6073:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getHideAccess().getHideAction_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6074:1: ()
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6076:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getHideAccess().getHideAction_1()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Hide__Group__1__Impl"


    // $ANTLR start "rule__Show__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6090:1: rule__Show__Group__0 : rule__Show__Group__0__Impl rule__Show__Group__1 ;
    public final void rule__Show__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6094:1: ( rule__Show__Group__0__Impl rule__Show__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6095:2: rule__Show__Group__0__Impl rule__Show__Group__1
            {
            pushFollow(FOLLOW_rule__Show__Group__0__Impl_in_rule__Show__Group__012376);
            rule__Show__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Show__Group__1_in_rule__Show__Group__012379);
            rule__Show__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Show__Group__0"


    // $ANTLR start "rule__Show__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6102:1: rule__Show__Group__0__Impl : ( 'show' ) ;
    public final void rule__Show__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6106:1: ( ( 'show' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6107:1: ( 'show' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6107:1: ( 'show' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6108:1: 'show'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getShowAccess().getShowKeyword_0()); 
            }
            match(input,62,FOLLOW_62_in_rule__Show__Group__0__Impl12407); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getShowAccess().getShowKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Show__Group__0__Impl"


    // $ANTLR start "rule__Show__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6121:1: rule__Show__Group__1 : rule__Show__Group__1__Impl ;
    public final void rule__Show__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6125:1: ( rule__Show__Group__1__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6126:2: rule__Show__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Show__Group__1__Impl_in_rule__Show__Group__112438);
            rule__Show__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Show__Group__1"


    // $ANTLR start "rule__Show__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6132:1: rule__Show__Group__1__Impl : ( () ) ;
    public final void rule__Show__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6136:1: ( ( () ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6137:1: ( () )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6137:1: ( () )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6138:1: ()
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getShowAccess().getShowAction_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6139:1: ()
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6141:1: 
            {
            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getShowAccess().getShowAction_1()); 
            }

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Show__Group__1__Impl"


    // $ANTLR start "rule__Say__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6155:1: rule__Say__Group__0 : rule__Say__Group__0__Impl rule__Say__Group__1 ;
    public final void rule__Say__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6159:1: ( rule__Say__Group__0__Impl rule__Say__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6160:2: rule__Say__Group__0__Impl rule__Say__Group__1
            {
            pushFollow(FOLLOW_rule__Say__Group__0__Impl_in_rule__Say__Group__012500);
            rule__Say__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Say__Group__1_in_rule__Say__Group__012503);
            rule__Say__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Say__Group__0"


    // $ANTLR start "rule__Say__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6167:1: rule__Say__Group__0__Impl : ( 'say' ) ;
    public final void rule__Say__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6171:1: ( ( 'say' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6172:1: ( 'say' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6172:1: ( 'say' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6173:1: 'say'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSayAccess().getSayKeyword_0()); 
            }
            match(input,63,FOLLOW_63_in_rule__Say__Group__0__Impl12531); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSayAccess().getSayKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Say__Group__0__Impl"


    // $ANTLR start "rule__Say__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6186:1: rule__Say__Group__1 : rule__Say__Group__1__Impl rule__Say__Group__2 ;
    public final void rule__Say__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6190:1: ( rule__Say__Group__1__Impl rule__Say__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6191:2: rule__Say__Group__1__Impl rule__Say__Group__2
            {
            pushFollow(FOLLOW_rule__Say__Group__1__Impl_in_rule__Say__Group__112562);
            rule__Say__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Say__Group__2_in_rule__Say__Group__112565);
            rule__Say__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Say__Group__1"


    // $ANTLR start "rule__Say__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6198:1: rule__Say__Group__1__Impl : ( ( rule__Say__SentenceAssignment_1 ) ) ;
    public final void rule__Say__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6202:1: ( ( ( rule__Say__SentenceAssignment_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6203:1: ( ( rule__Say__SentenceAssignment_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6203:1: ( ( rule__Say__SentenceAssignment_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6204:1: ( rule__Say__SentenceAssignment_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSayAccess().getSentenceAssignment_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6205:1: ( rule__Say__SentenceAssignment_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6205:2: rule__Say__SentenceAssignment_1
            {
            pushFollow(FOLLOW_rule__Say__SentenceAssignment_1_in_rule__Say__Group__1__Impl12592);
            rule__Say__SentenceAssignment_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSayAccess().getSentenceAssignment_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Say__Group__1__Impl"


    // $ANTLR start "rule__Say__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6215:1: rule__Say__Group__2 : rule__Say__Group__2__Impl ;
    public final void rule__Say__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6219:1: ( rule__Say__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6220:2: rule__Say__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Say__Group__2__Impl_in_rule__Say__Group__212622);
            rule__Say__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Say__Group__2"


    // $ANTLR start "rule__Say__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6226:1: rule__Say__Group__2__Impl : ( ( rule__Say__Group_2__0 )? ) ;
    public final void rule__Say__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6230:1: ( ( ( rule__Say__Group_2__0 )? ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6231:1: ( ( rule__Say__Group_2__0 )? )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6231:1: ( ( rule__Say__Group_2__0 )? )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6232:1: ( rule__Say__Group_2__0 )?
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSayAccess().getGroup_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6233:1: ( rule__Say__Group_2__0 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==64) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6233:2: rule__Say__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__Say__Group_2__0_in_rule__Say__Group__2__Impl12649);
                    rule__Say__Group_2__0();

                    state._fsp--;
                    if (state.failed) return ;

                    }
                    break;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSayAccess().getGroup_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Say__Group__2__Impl"


    // $ANTLR start "rule__Say__Group_2__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6249:1: rule__Say__Group_2__0 : rule__Say__Group_2__0__Impl rule__Say__Group_2__1 ;
    public final void rule__Say__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6253:1: ( rule__Say__Group_2__0__Impl rule__Say__Group_2__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6254:2: rule__Say__Group_2__0__Impl rule__Say__Group_2__1
            {
            pushFollow(FOLLOW_rule__Say__Group_2__0__Impl_in_rule__Say__Group_2__012686);
            rule__Say__Group_2__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Say__Group_2__1_in_rule__Say__Group_2__012689);
            rule__Say__Group_2__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Say__Group_2__0"


    // $ANTLR start "rule__Say__Group_2__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6261:1: rule__Say__Group_2__0__Impl : ( 'for' ) ;
    public final void rule__Say__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6265:1: ( ( 'for' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6266:1: ( 'for' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6266:1: ( 'for' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6267:1: 'for'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSayAccess().getForKeyword_2_0()); 
            }
            match(input,64,FOLLOW_64_in_rule__Say__Group_2__0__Impl12717); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSayAccess().getForKeyword_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Say__Group_2__0__Impl"


    // $ANTLR start "rule__Say__Group_2__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6280:1: rule__Say__Group_2__1 : rule__Say__Group_2__1__Impl rule__Say__Group_2__2 ;
    public final void rule__Say__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6284:1: ( rule__Say__Group_2__1__Impl rule__Say__Group_2__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6285:2: rule__Say__Group_2__1__Impl rule__Say__Group_2__2
            {
            pushFollow(FOLLOW_rule__Say__Group_2__1__Impl_in_rule__Say__Group_2__112748);
            rule__Say__Group_2__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Say__Group_2__2_in_rule__Say__Group_2__112751);
            rule__Say__Group_2__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Say__Group_2__1"


    // $ANTLR start "rule__Say__Group_2__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6292:1: rule__Say__Group_2__1__Impl : ( ( rule__Say__DelayAssignment_2_1 ) ) ;
    public final void rule__Say__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6296:1: ( ( ( rule__Say__DelayAssignment_2_1 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6297:1: ( ( rule__Say__DelayAssignment_2_1 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6297:1: ( ( rule__Say__DelayAssignment_2_1 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6298:1: ( rule__Say__DelayAssignment_2_1 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSayAccess().getDelayAssignment_2_1()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6299:1: ( rule__Say__DelayAssignment_2_1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6299:2: rule__Say__DelayAssignment_2_1
            {
            pushFollow(FOLLOW_rule__Say__DelayAssignment_2_1_in_rule__Say__Group_2__1__Impl12778);
            rule__Say__DelayAssignment_2_1();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getSayAccess().getDelayAssignment_2_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Say__Group_2__1__Impl"


    // $ANTLR start "rule__Say__Group_2__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6309:1: rule__Say__Group_2__2 : rule__Say__Group_2__2__Impl ;
    public final void rule__Say__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6313:1: ( rule__Say__Group_2__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6314:2: rule__Say__Group_2__2__Impl
            {
            pushFollow(FOLLOW_rule__Say__Group_2__2__Impl_in_rule__Say__Group_2__212808);
            rule__Say__Group_2__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Say__Group_2__2"


    // $ANTLR start "rule__Say__Group_2__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6320:1: rule__Say__Group_2__2__Impl : ( 'secs' ) ;
    public final void rule__Say__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6324:1: ( ( 'secs' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6325:1: ( 'secs' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6325:1: ( 'secs' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6326:1: 'secs'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSayAccess().getSecsKeyword_2_2()); 
            }
            match(input,28,FOLLOW_28_in_rule__Say__Group_2__2__Impl12836); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSayAccess().getSecsKeyword_2_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Say__Group_2__2__Impl"


    // $ANTLR start "rule__Play__Group__0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6345:1: rule__Play__Group__0 : rule__Play__Group__0__Impl rule__Play__Group__1 ;
    public final void rule__Play__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6349:1: ( rule__Play__Group__0__Impl rule__Play__Group__1 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6350:2: rule__Play__Group__0__Impl rule__Play__Group__1
            {
            pushFollow(FOLLOW_rule__Play__Group__0__Impl_in_rule__Play__Group__012873);
            rule__Play__Group__0__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Play__Group__1_in_rule__Play__Group__012876);
            rule__Play__Group__1();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Play__Group__0"


    // $ANTLR start "rule__Play__Group__0__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6357:1: rule__Play__Group__0__Impl : ( 'play' ) ;
    public final void rule__Play__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6361:1: ( ( 'play' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6362:1: ( 'play' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6362:1: ( 'play' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6363:1: 'play'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPlayAccess().getPlayKeyword_0()); 
            }
            match(input,65,FOLLOW_65_in_rule__Play__Group__0__Impl12904); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPlayAccess().getPlayKeyword_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Play__Group__0__Impl"


    // $ANTLR start "rule__Play__Group__1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6376:1: rule__Play__Group__1 : rule__Play__Group__1__Impl rule__Play__Group__2 ;
    public final void rule__Play__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6380:1: ( rule__Play__Group__1__Impl rule__Play__Group__2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6381:2: rule__Play__Group__1__Impl rule__Play__Group__2
            {
            pushFollow(FOLLOW_rule__Play__Group__1__Impl_in_rule__Play__Group__112935);
            rule__Play__Group__1__Impl();

            state._fsp--;
            if (state.failed) return ;
            pushFollow(FOLLOW_rule__Play__Group__2_in_rule__Play__Group__112938);
            rule__Play__Group__2();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Play__Group__1"


    // $ANTLR start "rule__Play__Group__1__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6388:1: rule__Play__Group__1__Impl : ( 'sound' ) ;
    public final void rule__Play__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6392:1: ( ( 'sound' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6393:1: ( 'sound' )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6393:1: ( 'sound' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6394:1: 'sound'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPlayAccess().getSoundKeyword_1()); 
            }
            match(input,66,FOLLOW_66_in_rule__Play__Group__1__Impl12966); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPlayAccess().getSoundKeyword_1()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Play__Group__1__Impl"


    // $ANTLR start "rule__Play__Group__2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6407:1: rule__Play__Group__2 : rule__Play__Group__2__Impl ;
    public final void rule__Play__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6411:1: ( rule__Play__Group__2__Impl )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6412:2: rule__Play__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Play__Group__2__Impl_in_rule__Play__Group__212997);
            rule__Play__Group__2__Impl();

            state._fsp--;
            if (state.failed) return ;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Play__Group__2"


    // $ANTLR start "rule__Play__Group__2__Impl"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6418:1: rule__Play__Group__2__Impl : ( ( rule__Play__SoundAssignment_2 ) ) ;
    public final void rule__Play__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6422:1: ( ( ( rule__Play__SoundAssignment_2 ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6423:1: ( ( rule__Play__SoundAssignment_2 ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6423:1: ( ( rule__Play__SoundAssignment_2 ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6424:1: ( rule__Play__SoundAssignment_2 )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPlayAccess().getSoundAssignment_2()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6425:1: ( rule__Play__SoundAssignment_2 )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6425:2: rule__Play__SoundAssignment_2
            {
            pushFollow(FOLLOW_rule__Play__SoundAssignment_2_in_rule__Play__Group__2__Impl13024);
            rule__Play__SoundAssignment_2();

            state._fsp--;
            if (state.failed) return ;

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getPlayAccess().getSoundAssignment_2()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Play__Group__2__Impl"


    // $ANTLR start "rule__Project__NameAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6442:1: rule__Project__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Project__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6446:1: ( ( RULE_ID ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6447:1: ( RULE_ID )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6447:1: ( RULE_ID )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6448:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProjectAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Project__NameAssignment_113065); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getProjectAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Project__NameAssignment_1"


    // $ANTLR start "rule__Project__ObjectsAssignment_2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6457:1: rule__Project__ObjectsAssignment_2 : ( ruleObject ) ;
    public final void rule__Project__ObjectsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6461:1: ( ( ruleObject ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6462:1: ( ruleObject )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6462:1: ( ruleObject )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6463:1: ruleObject
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getProjectAccess().getObjectsObjectParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_ruleObject_in_rule__Project__ObjectsAssignment_213096);
            ruleObject();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getProjectAccess().getObjectsObjectParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Project__ObjectsAssignment_2"


    // $ANTLR start "rule__Object__NameAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6472:1: rule__Object__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Object__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6476:1: ( ( RULE_ID ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6477:1: ( RULE_ID )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6477:1: ( RULE_ID )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6478:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getObjectAccess().getNameIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Object__NameAssignment_113127); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getObjectAccess().getNameIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__NameAssignment_1"


    // $ANTLR start "rule__Object__ScriptsAssignment_2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6487:1: rule__Object__ScriptsAssignment_2 : ( ruleScript ) ;
    public final void rule__Object__ScriptsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6491:1: ( ( ruleScript ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6492:1: ( ruleScript )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6492:1: ( ruleScript )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6493:1: ruleScript
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getObjectAccess().getScriptsScriptParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_ruleScript_in_rule__Object__ScriptsAssignment_213158);
            ruleScript();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getObjectAccess().getScriptsScriptParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Object__ScriptsAssignment_2"


    // $ANTLR start "rule__Ref__ObjectAssignment"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6502:1: rule__Ref__ObjectAssignment : ( ( RULE_ID ) ) ;
    public final void rule__Ref__ObjectAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6506:1: ( ( ( RULE_ID ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6507:1: ( ( RULE_ID ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6507:1: ( ( RULE_ID ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6508:1: ( RULE_ID )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRefAccess().getObjectObjectCrossReference_0()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6509:1: ( RULE_ID )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6510:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRefAccess().getObjectObjectIDTerminalRuleCall_0_1()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Ref__ObjectAssignment13193); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRefAccess().getObjectObjectIDTerminalRuleCall_0_1()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getRefAccess().getObjectObjectCrossReference_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ref__ObjectAssignment"


    // $ANTLR start "rule__Predef__ObjectAssignment"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6521:1: rule__Predef__ObjectAssignment : ( ruleObjectPredefEnum ) ;
    public final void rule__Predef__ObjectAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6525:1: ( ( ruleObjectPredefEnum ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6526:1: ( ruleObjectPredefEnum )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6526:1: ( ruleObjectPredefEnum )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6527:1: ruleObjectPredefEnum
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPredefAccess().getObjectObjectPredefEnumEnumRuleCall_0()); 
            }
            pushFollow(FOLLOW_ruleObjectPredefEnum_in_rule__Predef__ObjectAssignment13228);
            ruleObjectPredefEnum();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPredefAccess().getObjectObjectPredefEnumEnumRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Predef__ObjectAssignment"


    // $ANTLR start "rule__Script__WhenAssignment_2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6536:1: rule__Script__WhenAssignment_2 : ( ruleWhen ) ;
    public final void rule__Script__WhenAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6540:1: ( ( ruleWhen ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6541:1: ( ruleWhen )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6541:1: ( ruleWhen )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6542:1: ruleWhen
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScriptAccess().getWhenWhenParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_ruleWhen_in_rule__Script__WhenAssignment_213259);
            ruleWhen();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getScriptAccess().getWhenWhenParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Script__WhenAssignment_2"


    // $ANTLR start "rule__Script__BlocksAssignment_3"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6551:1: rule__Script__BlocksAssignment_3 : ( ruleBlock ) ;
    public final void rule__Script__BlocksAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6555:1: ( ( ruleBlock ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6556:1: ( ruleBlock )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6556:1: ( ruleBlock )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6557:1: ruleBlock
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getScriptAccess().getBlocksBlockParserRuleCall_3_0()); 
            }
            pushFollow(FOLLOW_ruleBlock_in_rule__Script__BlocksAssignment_313290);
            ruleBlock();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getScriptAccess().getBlocksBlockParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Script__BlocksAssignment_3"


    // $ANTLR start "rule__WhenMsg__MsgAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6566:1: rule__WhenMsg__MsgAssignment_1 : ( ruleMessage ) ;
    public final void rule__WhenMsg__MsgAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6570:1: ( ( ruleMessage ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6571:1: ( ruleMessage )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6571:1: ( ruleMessage )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6572:1: ruleMessage
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWhenMsgAccess().getMsgMessageParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleMessage_in_rule__WhenMsg__MsgAssignment_113321);
            ruleMessage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWhenMsgAccess().getMsgMessageParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhenMsg__MsgAssignment_1"


    // $ANTLR start "rule__Message__NameAssignment"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6581:1: rule__Message__NameAssignment : ( RULE_ID ) ;
    public final void rule__Message__NameAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6585:1: ( ( RULE_ID ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6586:1: ( RULE_ID )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6586:1: ( RULE_ID )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6587:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMessageAccess().getNameIDTerminalRuleCall_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Message__NameAssignment13352); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMessageAccess().getNameIDTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Message__NameAssignment"


    // $ANTLR start "rule__Loop__BlocksAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6596:1: rule__Loop__BlocksAssignment_1 : ( ruleBlock ) ;
    public final void rule__Loop__BlocksAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6600:1: ( ( ruleBlock ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6601:1: ( ruleBlock )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6601:1: ( ruleBlock )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6602:1: ruleBlock
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLoopAccess().getBlocksBlockParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleBlock_in_rule__Loop__BlocksAssignment_113383);
            ruleBlock();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLoopAccess().getBlocksBlockParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Loop__BlocksAssignment_1"


    // $ANTLR start "rule__Forever__CondAssignment_2_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6611:1: rule__Forever__CondAssignment_2_1 : ( ruleBinaryTerminal ) ;
    public final void rule__Forever__CondAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6615:1: ( ( ruleBinaryTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6616:1: ( ruleBinaryTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6616:1: ( ruleBinaryTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6617:1: ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getForeverAccess().getCondBinaryTerminalParserRuleCall_2_1_0()); 
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_rule__Forever__CondAssignment_2_113414);
            ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getForeverAccess().getCondBinaryTerminalParserRuleCall_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Forever__CondAssignment_2_1"


    // $ANTLR start "rule__RepeatN__NAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6626:1: rule__RepeatN__NAssignment_1 : ( ruleNumberTerminal ) ;
    public final void rule__RepeatN__NAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6630:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6631:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6631:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6632:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRepeatNAccess().getNNumberTerminalParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__RepeatN__NAssignment_113445);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRepeatNAccess().getNNumberTerminalParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepeatN__NAssignment_1"


    // $ANTLR start "rule__RepeatUntil__CondAssignment_2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6641:1: rule__RepeatUntil__CondAssignment_2 : ( ruleBinaryTerminal ) ;
    public final void rule__RepeatUntil__CondAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6645:1: ( ( ruleBinaryTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6646:1: ( ruleBinaryTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6646:1: ( ruleBinaryTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6647:1: ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getRepeatUntilAccess().getCondBinaryTerminalParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_rule__RepeatUntil__CondAssignment_213476);
            ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getRepeatUntilAccess().getCondBinaryTerminalParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepeatUntil__CondAssignment_2"


    // $ANTLR start "rule__Stop__AllAssignment_1_0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6656:1: rule__Stop__AllAssignment_1_0 : ( ( 'all' ) ) ;
    public final void rule__Stop__AllAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6660:1: ( ( ( 'all' ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6661:1: ( ( 'all' ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6661:1: ( ( 'all' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6662:1: ( 'all' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStopAccess().getAllAllKeyword_1_0_0()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6663:1: ( 'all' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6664:1: 'all'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStopAccess().getAllAllKeyword_1_0_0()); 
            }
            match(input,67,FOLLOW_67_in_rule__Stop__AllAssignment_1_013512); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStopAccess().getAllAllKeyword_1_0_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStopAccess().getAllAllKeyword_1_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stop__AllAssignment_1_0"


    // $ANTLR start "rule__Stop__ScriptAssignment_1_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6679:1: rule__Stop__ScriptAssignment_1_1 : ( ( 'script' ) ) ;
    public final void rule__Stop__ScriptAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6683:1: ( ( ( 'script' ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6684:1: ( ( 'script' ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6684:1: ( ( 'script' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6685:1: ( 'script' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStopAccess().getScriptScriptKeyword_1_1_0()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6686:1: ( 'script' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6687:1: 'script'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getStopAccess().getScriptScriptKeyword_1_1_0()); 
            }
            match(input,17,FOLLOW_17_in_rule__Stop__ScriptAssignment_1_113556); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getStopAccess().getScriptScriptKeyword_1_1_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getStopAccess().getScriptScriptKeyword_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stop__ScriptAssignment_1_1"


    // $ANTLR start "rule__WaitDelay__DelayAssignment_0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6702:1: rule__WaitDelay__DelayAssignment_0 : ( ruleNumberTerminal ) ;
    public final void rule__WaitDelay__DelayAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6706:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6707:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6707:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6708:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitDelayAccess().getDelayNumberTerminalParserRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__WaitDelay__DelayAssignment_013595);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitDelayAccess().getDelayNumberTerminalParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitDelay__DelayAssignment_0"


    // $ANTLR start "rule__WaitUntil__CondAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6717:1: rule__WaitUntil__CondAssignment_1 : ( ruleBinaryTerminal ) ;
    public final void rule__WaitUntil__CondAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6721:1: ( ( ruleBinaryTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6722:1: ( ruleBinaryTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6722:1: ( ruleBinaryTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6723:1: ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getWaitUntilAccess().getCondBinaryTerminalParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_rule__WaitUntil__CondAssignment_113626);
            ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getWaitUntilAccess().getCondBinaryTerminalParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WaitUntil__CondAssignment_1"


    // $ANTLR start "rule__Broadcast__MsgAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6732:1: rule__Broadcast__MsgAssignment_1 : ( ruleMessage ) ;
    public final void rule__Broadcast__MsgAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6736:1: ( ( ruleMessage ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6737:1: ( ruleMessage )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6737:1: ( ruleMessage )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6738:1: ruleMessage
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBroadcastAccess().getMsgMessageParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleMessage_in_rule__Broadcast__MsgAssignment_113657);
            ruleMessage();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBroadcastAccess().getMsgMessageParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Broadcast__MsgAssignment_1"


    // $ANTLR start "rule__Broadcast__WaitAssignment_2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6747:1: rule__Broadcast__WaitAssignment_2 : ( ( 'and wait' ) ) ;
    public final void rule__Broadcast__WaitAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6751:1: ( ( ( 'and wait' ) ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6752:1: ( ( 'and wait' ) )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6752:1: ( ( 'and wait' ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6753:1: ( 'and wait' )
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBroadcastAccess().getWaitAndWaitKeyword_2_0()); 
            }
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6754:1: ( 'and wait' )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6755:1: 'and wait'
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBroadcastAccess().getWaitAndWaitKeyword_2_0()); 
            }
            match(input,68,FOLLOW_68_in_rule__Broadcast__WaitAssignment_213693); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBroadcastAccess().getWaitAndWaitKeyword_2_0()); 
            }

            }

            if ( state.backtracking==0 ) {
               after(grammarAccess.getBroadcastAccess().getWaitAndWaitKeyword_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Broadcast__WaitAssignment_2"


    // $ANTLR start "rule__If__CondAssignment_0_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6770:1: rule__If__CondAssignment_0_1 : ( ruleBinaryTerminal ) ;
    public final void rule__If__CondAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6774:1: ( ( ruleBinaryTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6775:1: ( ruleBinaryTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6775:1: ( ruleBinaryTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6776:1: ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfAccess().getCondBinaryTerminalParserRuleCall_0_1_0()); 
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_rule__If__CondAssignment_0_113732);
            ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfAccess().getCondBinaryTerminalParserRuleCall_0_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__CondAssignment_0_1"


    // $ANTLR start "rule__If__TrueAssignment_0_2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6785:1: rule__If__TrueAssignment_0_2 : ( ruleBlock ) ;
    public final void rule__If__TrueAssignment_0_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6789:1: ( ( ruleBlock ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6790:1: ( ruleBlock )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6790:1: ( ruleBlock )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6791:1: ruleBlock
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfAccess().getTrueBlockParserRuleCall_0_2_0()); 
            }
            pushFollow(FOLLOW_ruleBlock_in_rule__If__TrueAssignment_0_213763);
            ruleBlock();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfAccess().getTrueBlockParserRuleCall_0_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__TrueAssignment_0_2"


    // $ANTLR start "rule__If__FalseAssignment_1_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6800:1: rule__If__FalseAssignment_1_1 : ( ruleBlock ) ;
    public final void rule__If__FalseAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6804:1: ( ( ruleBlock ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6805:1: ( ruleBlock )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6805:1: ( ruleBlock )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6806:1: ruleBlock
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getIfAccess().getFalseBlockParserRuleCall_1_1_0()); 
            }
            pushFollow(FOLLOW_ruleBlock_in_rule__If__FalseAssignment_1_113794);
            ruleBlock();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getIfAccess().getFalseBlockParserRuleCall_1_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__If__FalseAssignment_1_1"


    // $ANTLR start "rule__Move__StepsAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6815:1: rule__Move__StepsAssignment_1 : ( ruleNumberTerminal ) ;
    public final void rule__Move__StepsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6819:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6820:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6820:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6821:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getMoveAccess().getStepsNumberTerminalParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__Move__StepsAssignment_113825);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getMoveAccess().getStepsNumberTerminalParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Move__StepsAssignment_1"


    // $ANTLR start "rule__TurnCW__AngleAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6830:1: rule__TurnCW__AngleAssignment_1 : ( ruleNumberTerminal ) ;
    public final void rule__TurnCW__AngleAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6834:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6835:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6835:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6836:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTurnCWAccess().getAngleNumberTerminalParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__TurnCW__AngleAssignment_113856);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTurnCWAccess().getAngleNumberTerminalParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCW__AngleAssignment_1"


    // $ANTLR start "rule__TurnCCW__AngleAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6845:1: rule__TurnCCW__AngleAssignment_1 : ( ruleNumberTerminal ) ;
    public final void rule__TurnCCW__AngleAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6849:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6850:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6850:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6851:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTurnCCWAccess().getAngleNumberTerminalParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__TurnCCW__AngleAssignment_113887);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTurnCCWAccess().getAngleNumberTerminalParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TurnCCW__AngleAssignment_1"


    // $ANTLR start "rule__PointDirection__AngleAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6860:1: rule__PointDirection__AngleAssignment_1 : ( ruleNumberTerminal ) ;
    public final void rule__PointDirection__AngleAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6864:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6865:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6865:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6866:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPointDirectionAccess().getAngleNumberTerminalParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__PointDirection__AngleAssignment_113918);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPointDirectionAccess().getAngleNumberTerminalParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PointDirection__AngleAssignment_1"


    // $ANTLR start "rule__PointTowards__ObjectAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6875:1: rule__PointTowards__ObjectAssignment_1 : ( ruleIObject ) ;
    public final void rule__PointTowards__ObjectAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6879:1: ( ( ruleIObject ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6880:1: ( ruleIObject )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6880:1: ( ruleIObject )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6881:1: ruleIObject
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPointTowardsAccess().getObjectIObjectParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleIObject_in_rule__PointTowards__ObjectAssignment_113949);
            ruleIObject();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPointTowardsAccess().getObjectIObjectParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PointTowards__ObjectAssignment_1"


    // $ANTLR start "rule__GoToXY__XAssignment_2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6890:1: rule__GoToXY__XAssignment_2 : ( ruleNumberTerminal ) ;
    public final void rule__GoToXY__XAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6894:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6895:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6895:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6896:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoToXYAccess().getXNumberTerminalParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__GoToXY__XAssignment_213980);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoToXYAccess().getXNumberTerminalParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoToXY__XAssignment_2"


    // $ANTLR start "rule__GoToXY__YAssignment_4"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6905:1: rule__GoToXY__YAssignment_4 : ( ruleNumberTerminal ) ;
    public final void rule__GoToXY__YAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6909:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6910:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6910:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6911:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoToXYAccess().getYNumberTerminalParserRuleCall_4_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__GoToXY__YAssignment_414011);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoToXYAccess().getYNumberTerminalParserRuleCall_4_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoToXY__YAssignment_4"


    // $ANTLR start "rule__GoTo__ObjectAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6920:1: rule__GoTo__ObjectAssignment_1 : ( ruleIObject ) ;
    public final void rule__GoTo__ObjectAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6924:1: ( ( ruleIObject ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6925:1: ( ruleIObject )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6925:1: ( ruleIObject )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6926:1: ruleIObject
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGoToAccess().getObjectIObjectParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleIObject_in_rule__GoTo__ObjectAssignment_114042);
            ruleIObject();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGoToAccess().getObjectIObjectParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GoTo__ObjectAssignment_1"


    // $ANTLR start "rule__Glide__DurationAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6935:1: rule__Glide__DurationAssignment_1 : ( ruleNumberTerminal ) ;
    public final void rule__Glide__DurationAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6939:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6940:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6940:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6941:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGlideAccess().getDurationNumberTerminalParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__Glide__DurationAssignment_114073);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGlideAccess().getDurationNumberTerminalParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Glide__DurationAssignment_1"


    // $ANTLR start "rule__Glide__XAssignment_3"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6950:1: rule__Glide__XAssignment_3 : ( ruleNumberTerminal ) ;
    public final void rule__Glide__XAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6954:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6955:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6955:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6956:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGlideAccess().getXNumberTerminalParserRuleCall_3_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__Glide__XAssignment_314104);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGlideAccess().getXNumberTerminalParserRuleCall_3_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Glide__XAssignment_3"


    // $ANTLR start "rule__Glide__YAssignment_5"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6965:1: rule__Glide__YAssignment_5 : ( ruleNumberTerminal ) ;
    public final void rule__Glide__YAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6969:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6970:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6970:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6971:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGlideAccess().getYNumberTerminalParserRuleCall_5_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__Glide__YAssignment_514135);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGlideAccess().getYNumberTerminalParserRuleCall_5_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Glide__YAssignment_5"


    // $ANTLR start "rule__ChangeX__XAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6980:1: rule__ChangeX__XAssignment_1 : ( ruleNumberTerminal ) ;
    public final void rule__ChangeX__XAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6984:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6985:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6985:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6986:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getChangeXAccess().getXNumberTerminalParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__ChangeX__XAssignment_114166);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getChangeXAccess().getXNumberTerminalParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ChangeX__XAssignment_1"


    // $ANTLR start "rule__ChangeY__YAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6995:1: rule__ChangeY__YAssignment_1 : ( ruleNumberTerminal ) ;
    public final void rule__ChangeY__YAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:6999:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7000:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7000:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7001:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getChangeYAccess().getYNumberTerminalParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__ChangeY__YAssignment_114197);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getChangeYAccess().getYNumberTerminalParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ChangeY__YAssignment_1"


    // $ANTLR start "rule__SetX__XAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7010:1: rule__SetX__XAssignment_1 : ( ruleNumberTerminal ) ;
    public final void rule__SetX__XAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7014:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7015:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7015:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7016:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSetXAccess().getXNumberTerminalParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__SetX__XAssignment_114228);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSetXAccess().getXNumberTerminalParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetX__XAssignment_1"


    // $ANTLR start "rule__SetY__YAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7025:1: rule__SetY__YAssignment_1 : ( ruleNumberTerminal ) ;
    public final void rule__SetY__YAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7029:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7030:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7030:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7031:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSetYAccess().getYNumberTerminalParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__SetY__YAssignment_114259);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSetYAccess().getYNumberTerminalParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetY__YAssignment_1"


    // $ANTLR start "rule__NumberValue__ValueAssignment"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7040:1: rule__NumberValue__ValueAssignment : ( RULE_NUM ) ;
    public final void rule__NumberValue__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7044:1: ( ( RULE_NUM ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7045:1: ( RULE_NUM )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7045:1: ( RULE_NUM )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7046:1: RULE_NUM
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberValueAccess().getValueNUMTerminalRuleCall_0()); 
            }
            match(input,RULE_NUM,FOLLOW_RULE_NUM_in_rule__NumberValue__ValueAssignment14290); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberValueAccess().getValueNUMTerminalRuleCall_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberValue__ValueAssignment"


    // $ANTLR start "rule__NumberVar__VarAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7055:1: rule__NumberVar__VarAssignment_1 : ( RULE_ID ) ;
    public final void rule__NumberVar__VarAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7059:1: ( ( RULE_ID ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7060:1: ( RULE_ID )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7060:1: ( RULE_ID )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7061:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberVarAccess().getVarIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__NumberVar__VarAssignment_114321); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberVarAccess().getVarIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberVar__VarAssignment_1"


    // $ANTLR start "rule__NumberObjVar__VarAssignment_0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7070:1: rule__NumberObjVar__VarAssignment_0 : ( ruleObjectVariableEnum ) ;
    public final void rule__NumberObjVar__VarAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7074:1: ( ( ruleObjectVariableEnum ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7075:1: ( ruleObjectVariableEnum )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7075:1: ( ruleObjectVariableEnum )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7076:1: ruleObjectVariableEnum
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberObjVarAccess().getVarObjectVariableEnumEnumRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_ruleObjectVariableEnum_in_rule__NumberObjVar__VarAssignment_014352);
            ruleObjectVariableEnum();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberObjVarAccess().getVarObjectVariableEnumEnumRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberObjVar__VarAssignment_0"


    // $ANTLR start "rule__NumberObjVar__ObjAssignment_2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7085:1: rule__NumberObjVar__ObjAssignment_2 : ( ruleIObject ) ;
    public final void rule__NumberObjVar__ObjAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7089:1: ( ( ruleIObject ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7090:1: ( ruleIObject )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7090:1: ( ruleIObject )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7091:1: ruleIObject
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNumberObjVarAccess().getObjIObjectParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_ruleIObject_in_rule__NumberObjVar__ObjAssignment_214383);
            ruleIObject();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNumberObjVarAccess().getObjIObjectParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NumberObjVar__ObjAssignment_2"


    // $ANTLR start "rule__BinaryVar__VarAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7100:1: rule__BinaryVar__VarAssignment_1 : ( RULE_ID ) ;
    public final void rule__BinaryVar__VarAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7104:1: ( ( RULE_ID ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7105:1: ( RULE_ID )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7105:1: ( RULE_ID )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7106:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getBinaryVarAccess().getVarIDTerminalRuleCall_1_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__BinaryVar__VarAssignment_114414); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getBinaryVarAccess().getVarIDTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BinaryVar__VarAssignment_1"


    // $ANTLR start "rule__TouchsColor__ColorAssignment_2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7115:1: rule__TouchsColor__ColorAssignment_2 : ( RULE_STRING ) ;
    public final void rule__TouchsColor__ColorAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7119:1: ( ( RULE_STRING ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7120:1: ( RULE_STRING )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7120:1: ( RULE_STRING )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7121:1: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getTouchsColorAccess().getColorSTRINGTerminalRuleCall_2_0()); 
            }
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__TouchsColor__ColorAssignment_214445); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getTouchsColorAccess().getColorSTRINGTerminalRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TouchsColor__ColorAssignment_2"


    // $ANTLR start "rule__Lt__LeftAssignment_0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7130:1: rule__Lt__LeftAssignment_0 : ( ruleNumberTerminal ) ;
    public final void rule__Lt__LeftAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7134:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7135:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7135:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7136:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLtAccess().getLeftNumberTerminalParserRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__Lt__LeftAssignment_014476);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLtAccess().getLeftNumberTerminalParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lt__LeftAssignment_0"


    // $ANTLR start "rule__Lt__RightAssignment_2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7145:1: rule__Lt__RightAssignment_2 : ( ruleNumberTerminal ) ;
    public final void rule__Lt__RightAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7149:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7150:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7150:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7151:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getLtAccess().getRightNumberTerminalParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__Lt__RightAssignment_214507);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getLtAccess().getRightNumberTerminalParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lt__RightAssignment_2"


    // $ANTLR start "rule__Gt__LeftAssignment_0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7160:1: rule__Gt__LeftAssignment_0 : ( ruleNumberTerminal ) ;
    public final void rule__Gt__LeftAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7164:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7165:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7165:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7166:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGtAccess().getLeftNumberTerminalParserRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__Gt__LeftAssignment_014538);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGtAccess().getLeftNumberTerminalParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gt__LeftAssignment_0"


    // $ANTLR start "rule__Gt__RightAssignment_2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7175:1: rule__Gt__RightAssignment_2 : ( ruleNumberTerminal ) ;
    public final void rule__Gt__RightAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7179:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7180:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7180:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7181:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getGtAccess().getRightNumberTerminalParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__Gt__RightAssignment_214569);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getGtAccess().getRightNumberTerminalParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Gt__RightAssignment_2"


    // $ANTLR start "rule__Eq__LeftAssignment_0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7190:1: rule__Eq__LeftAssignment_0 : ( ruleNumberTerminal ) ;
    public final void rule__Eq__LeftAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7194:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7195:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7195:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7196:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqAccess().getLeftNumberTerminalParserRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__Eq__LeftAssignment_014600);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqAccess().getLeftNumberTerminalParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Eq__LeftAssignment_0"


    // $ANTLR start "rule__Eq__RightAssignment_2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7205:1: rule__Eq__RightAssignment_2 : ( ruleNumberTerminal ) ;
    public final void rule__Eq__RightAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7209:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7210:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7210:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7211:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getEqAccess().getRightNumberTerminalParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__Eq__RightAssignment_214631);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getEqAccess().getRightNumberTerminalParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Eq__RightAssignment_2"


    // $ANTLR start "rule__And__LeftAssignment_0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7220:1: rule__And__LeftAssignment_0 : ( ruleBinaryTerminal ) ;
    public final void rule__And__LeftAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7224:1: ( ( ruleBinaryTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7225:1: ( ruleBinaryTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7225:1: ( ruleBinaryTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7226:1: ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAndAccess().getLeftBinaryTerminalParserRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_rule__And__LeftAssignment_014662);
            ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAndAccess().getLeftBinaryTerminalParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__LeftAssignment_0"


    // $ANTLR start "rule__And__RightAssignment_2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7235:1: rule__And__RightAssignment_2 : ( ruleBinaryTerminal ) ;
    public final void rule__And__RightAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7239:1: ( ( ruleBinaryTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7240:1: ( ruleBinaryTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7240:1: ( ruleBinaryTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7241:1: ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getAndAccess().getRightBinaryTerminalParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_rule__And__RightAssignment_214693);
            ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getAndAccess().getRightBinaryTerminalParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__And__RightAssignment_2"


    // $ANTLR start "rule__Or__LeftAssignment_0"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7250:1: rule__Or__LeftAssignment_0 : ( ruleBinaryTerminal ) ;
    public final void rule__Or__LeftAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7254:1: ( ( ruleBinaryTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7255:1: ( ruleBinaryTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7255:1: ( ruleBinaryTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7256:1: ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOrAccess().getLeftBinaryTerminalParserRuleCall_0_0()); 
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_rule__Or__LeftAssignment_014724);
            ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getOrAccess().getLeftBinaryTerminalParserRuleCall_0_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__LeftAssignment_0"


    // $ANTLR start "rule__Or__RightAssignment_2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7265:1: rule__Or__RightAssignment_2 : ( ruleBinaryTerminal ) ;
    public final void rule__Or__RightAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7269:1: ( ( ruleBinaryTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7270:1: ( ruleBinaryTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7270:1: ( ruleBinaryTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7271:1: ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getOrAccess().getRightBinaryTerminalParserRuleCall_2_0()); 
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_rule__Or__RightAssignment_214755);
            ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getOrAccess().getRightBinaryTerminalParserRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Or__RightAssignment_2"


    // $ANTLR start "rule__Not__ValueAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7280:1: rule__Not__ValueAssignment_1 : ( ruleBinaryTerminal ) ;
    public final void rule__Not__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7284:1: ( ( ruleBinaryTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7285:1: ( ruleBinaryTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7285:1: ( ruleBinaryTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7286:1: ruleBinaryTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getNotAccess().getValueBinaryTerminalParserRuleCall_1_0()); 
            }
            pushFollow(FOLLOW_ruleBinaryTerminal_in_rule__Not__ValueAssignment_114786);
            ruleBinaryTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getNotAccess().getValueBinaryTerminalParserRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Not__ValueAssignment_1"


    // $ANTLR start "rule__Say__SentenceAssignment_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7295:1: rule__Say__SentenceAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Say__SentenceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7299:1: ( ( RULE_STRING ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7300:1: ( RULE_STRING )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7300:1: ( RULE_STRING )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7301:1: RULE_STRING
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSayAccess().getSentenceSTRINGTerminalRuleCall_1_0()); 
            }
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Say__SentenceAssignment_114817); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSayAccess().getSentenceSTRINGTerminalRuleCall_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Say__SentenceAssignment_1"


    // $ANTLR start "rule__Say__DelayAssignment_2_1"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7310:1: rule__Say__DelayAssignment_2_1 : ( ruleNumberTerminal ) ;
    public final void rule__Say__DelayAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7314:1: ( ( ruleNumberTerminal ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7315:1: ( ruleNumberTerminal )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7315:1: ( ruleNumberTerminal )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7316:1: ruleNumberTerminal
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getSayAccess().getDelayNumberTerminalParserRuleCall_2_1_0()); 
            }
            pushFollow(FOLLOW_ruleNumberTerminal_in_rule__Say__DelayAssignment_2_114848);
            ruleNumberTerminal();

            state._fsp--;
            if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getSayAccess().getDelayNumberTerminalParserRuleCall_2_1_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Say__DelayAssignment_2_1"


    // $ANTLR start "rule__Play__SoundAssignment_2"
    // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7325:1: rule__Play__SoundAssignment_2 : ( RULE_ID ) ;
    public final void rule__Play__SoundAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7329:1: ( ( RULE_ID ) )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7330:1: ( RULE_ID )
            {
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7330:1: ( RULE_ID )
            // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:7331:1: RULE_ID
            {
            if ( state.backtracking==0 ) {
               before(grammarAccess.getPlayAccess().getSoundIDTerminalRuleCall_2_0()); 
            }
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Play__SoundAssignment_214879); if (state.failed) return ;
            if ( state.backtracking==0 ) {
               after(grammarAccess.getPlayAccess().getSoundIDTerminalRuleCall_2_0()); 
            }

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Play__SoundAssignment_2"

    // $ANTLR start synpred33_InternalScratch
    public final void synpred33_InternalScratch_fragment() throws RecognitionException {   
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1977:1: ( ( ruleOr ) )
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1977:1: ( ruleOr )
        {
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1977:1: ( ruleOr )
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1978:1: ruleOr
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getBinaryAccess().getOrParserRuleCall_0()); 
        }
        pushFollow(FOLLOW_ruleOr_in_synpred33_InternalScratch4281);
        ruleOr();

        state._fsp--;
        if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred33_InternalScratch

    // $ANTLR start synpred34_InternalScratch
    public final void synpred34_InternalScratch_fragment() throws RecognitionException {   
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1983:6: ( ( ruleAnd ) )
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1983:6: ( ruleAnd )
        {
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1983:6: ( ruleAnd )
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1984:1: ruleAnd
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getBinaryAccess().getAndParserRuleCall_1()); 
        }
        pushFollow(FOLLOW_ruleAnd_in_synpred34_InternalScratch4298);
        ruleAnd();

        state._fsp--;
        if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred34_InternalScratch

    // $ANTLR start synpred35_InternalScratch
    public final void synpred35_InternalScratch_fragment() throws RecognitionException {   
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1989:6: ( ( ruleLt ) )
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1989:6: ( ruleLt )
        {
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1989:6: ( ruleLt )
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1990:1: ruleLt
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getBinaryAccess().getLtParserRuleCall_2()); 
        }
        pushFollow(FOLLOW_ruleLt_in_synpred35_InternalScratch4315);
        ruleLt();

        state._fsp--;
        if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred35_InternalScratch

    // $ANTLR start synpred36_InternalScratch
    public final void synpred36_InternalScratch_fragment() throws RecognitionException {   
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1995:6: ( ( ruleGt ) )
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1995:6: ( ruleGt )
        {
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1995:6: ( ruleGt )
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:1996:1: ruleGt
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getBinaryAccess().getGtParserRuleCall_3()); 
        }
        pushFollow(FOLLOW_ruleGt_in_synpred36_InternalScratch4332);
        ruleGt();

        state._fsp--;
        if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred36_InternalScratch

    // $ANTLR start synpred37_InternalScratch
    public final void synpred37_InternalScratch_fragment() throws RecognitionException {   
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2001:6: ( ( ruleEq ) )
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2001:6: ( ruleEq )
        {
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2001:6: ( ruleEq )
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2002:1: ruleEq
        {
        if ( state.backtracking==0 ) {
           before(grammarAccess.getBinaryAccess().getEqParserRuleCall_4()); 
        }
        pushFollow(FOLLOW_ruleEq_in_synpred37_InternalScratch4349);
        ruleEq();

        state._fsp--;
        if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred37_InternalScratch

    // $ANTLR start synpred47_InternalScratch
    public final void synpred47_InternalScratch_fragment() throws RecognitionException {   
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2776:2: ( rule__Forever__Group_2__0 )
        // ../demo.scratch.ui/src-gen/demo/scratch/ui/contentassist/antlr/internal/InternalScratch.g:2776:2: rule__Forever__Group_2__0
        {
        pushFollow(FOLLOW_rule__Forever__Group_2__0_in_synpred47_InternalScratch5900);
        rule__Forever__Group_2__0();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred47_InternalScratch

    // Delegated rules

    public final boolean synpred47_InternalScratch() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred47_InternalScratch_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred35_InternalScratch() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred35_InternalScratch_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred33_InternalScratch() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred33_InternalScratch_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred37_InternalScratch() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred37_InternalScratch_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred36_InternalScratch() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred36_InternalScratch_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred34_InternalScratch() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred34_InternalScratch_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA3 dfa3 = new DFA3(this);
    protected DFA7 dfa7 = new DFA7(this);
    protected DFA8 dfa8 = new DFA8(this);
    protected DFA15 dfa15 = new DFA15(this);
    static final String DFA3_eotS =
        "\30\uffff";
    static final String DFA3_eofS =
        "\30\uffff";
    static final String DFA3_minS =
        "\1\26\12\uffff\1\4\14\uffff";
    static final String DFA3_maxS =
        "\1\101\12\uffff\1\47\14\uffff";
    static final String DFA3_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12\1\uffff\1\15"+
        "\1\16\1\17\1\20\1\21\1\22\1\23\1\24\1\25\1\26\1\13\1\14";
    static final String DFA3_specialS =
        "\30\uffff}>";
    static final String[] DFA3_transitionS = {
            "\1\5\1\4\1\5\1\uffff\1\1\1\3\1\uffff\1\2\1\uffff\1\6\1\uffff"+
            "\1\7\1\uffff\1\10\1\11\1\12\1\13\2\uffff\1\14\1\uffff\1\15\1"+
            "\16\1\17\1\20\1\21\15\uffff\1\22\1\23\1\24\1\uffff\1\25",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\27\6\uffff\1\27\33\uffff\1\26",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA3_eot = DFA.unpackEncodedString(DFA3_eotS);
    static final short[] DFA3_eof = DFA.unpackEncodedString(DFA3_eofS);
    static final char[] DFA3_min = DFA.unpackEncodedStringToUnsignedChars(DFA3_minS);
    static final char[] DFA3_max = DFA.unpackEncodedStringToUnsignedChars(DFA3_maxS);
    static final short[] DFA3_accept = DFA.unpackEncodedString(DFA3_acceptS);
    static final short[] DFA3_special = DFA.unpackEncodedString(DFA3_specialS);
    static final short[][] DFA3_transition;

    static {
        int numStates = DFA3_transitionS.length;
        DFA3_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA3_transition[i] = DFA.unpackEncodedString(DFA3_transitionS[i]);
        }
    }

    class DFA3 extends DFA {

        public DFA3(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 3;
            this.eot = DFA3_eot;
            this.eof = DFA3_eof;
            this.min = DFA3_min;
            this.max = DFA3_max;
            this.accept = DFA3_accept;
            this.special = DFA3_special;
            this.transition = DFA3_transition;
        }
        public String getDescription() {
            return "1712:1: rule__Block__Alternatives : ( ( ruleStop ) | ( ruleBroadcast ) | ( ruleWait ) | ( ruleIf ) | ( ruleLoop ) | ( ruleMove ) | ( ruleTurnCW ) | ( ruleTurnCCW ) | ( rulePointDirection ) | ( rulePointTowards ) | ( ruleGoToXY ) | ( ruleGoTo ) | ( ruleGlide ) | ( ruleChangeX ) | ( ruleChangeY ) | ( ruleSetX ) | ( ruleSetY ) | ( ruleIfEdge ) | ( ruleHide ) | ( ruleShow ) | ( ruleSay ) | ( rulePlay ) );";
        }
    }
    static final String DFA7_eotS =
        "\12\uffff";
    static final String DFA7_eofS =
        "\2\uffff\1\6\1\10\1\11\5\uffff";
    static final String DFA7_minS =
        "\1\5\1\uffff\3\61\5\uffff";
    static final String DFA7_maxS =
        "\1\62\1\uffff\3\63\5\uffff";
    static final String DFA7_acceptS =
        "\1\uffff\1\1\3\uffff\1\3\1\4\1\2\1\5\1\6";
    static final String DFA7_specialS =
        "\12\uffff}>";
    static final String[] DFA7_transitionS = {
            "\1\1\6\uffff\1\2\1\3\1\4\43\uffff\1\5",
            "",
            "\1\6\1\uffff\1\7",
            "\1\10\1\uffff\1\7",
            "\1\11\1\uffff\1\7",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA7_eot = DFA.unpackEncodedString(DFA7_eotS);
    static final short[] DFA7_eof = DFA.unpackEncodedString(DFA7_eofS);
    static final char[] DFA7_min = DFA.unpackEncodedStringToUnsignedChars(DFA7_minS);
    static final char[] DFA7_max = DFA.unpackEncodedStringToUnsignedChars(DFA7_maxS);
    static final short[] DFA7_accept = DFA.unpackEncodedString(DFA7_acceptS);
    static final short[] DFA7_special = DFA.unpackEncodedString(DFA7_specialS);
    static final short[][] DFA7_transition;

    static {
        int numStates = DFA7_transitionS.length;
        DFA7_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA7_transition[i] = DFA.unpackEncodedString(DFA7_transitionS[i]);
        }
    }

    class DFA7 extends DFA {

        public DFA7(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 7;
            this.eot = DFA7_eot;
            this.eof = DFA7_eof;
            this.min = DFA7_min;
            this.max = DFA7_max;
            this.accept = DFA7_accept;
            this.special = DFA7_special;
            this.transition = DFA7_transition;
        }
        public String getDescription() {
            return "1926:1: rule__Number__Alternatives : ( ( ruleNumberValue ) | ( ruleNumberObjVar ) | ( ruleNumberVar ) | ( ruleNumberVarX ) | ( ruleNumberVarY ) | ( ruleNumberVarD ) );";
        }
    }
    static final String DFA8_eotS =
        "\12\uffff";
    static final String DFA8_eofS =
        "\12\uffff";
    static final String DFA8_minS =
        "\1\60\1\0\10\uffff";
    static final String DFA8_maxS =
        "\1\74\1\0\10\uffff";
    static final String DFA8_acceptS =
        "\2\uffff\1\6\1\7\1\10\1\1\1\2\1\3\1\4\1\5";
    static final String DFA8_specialS =
        "\1\uffff\1\0\10\uffff}>";
    static final String[] DFA8_transitionS = {
            "\1\1\1\uffff\1\4\1\uffff\1\3\7\uffff\1\2",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA8_eot = DFA.unpackEncodedString(DFA8_eotS);
    static final short[] DFA8_eof = DFA.unpackEncodedString(DFA8_eofS);
    static final char[] DFA8_min = DFA.unpackEncodedStringToUnsignedChars(DFA8_minS);
    static final char[] DFA8_max = DFA.unpackEncodedStringToUnsignedChars(DFA8_maxS);
    static final short[] DFA8_accept = DFA.unpackEncodedString(DFA8_acceptS);
    static final short[] DFA8_special = DFA.unpackEncodedString(DFA8_specialS);
    static final short[][] DFA8_transition;

    static {
        int numStates = DFA8_transitionS.length;
        DFA8_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA8_transition[i] = DFA.unpackEncodedString(DFA8_transitionS[i]);
        }
    }

    class DFA8 extends DFA {

        public DFA8(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 8;
            this.eot = DFA8_eot;
            this.eof = DFA8_eof;
            this.min = DFA8_min;
            this.max = DFA8_max;
            this.accept = DFA8_accept;
            this.special = DFA8_special;
            this.transition = DFA8_transition;
        }
        public String getDescription() {
            return "1972:1: rule__Binary__Alternatives : ( ( ruleOr ) | ( ruleAnd ) | ( ruleLt ) | ( ruleGt ) | ( ruleEq ) | ( ruleNot ) | ( ruleTouchsColor ) | ( ruleBinaryVar ) );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA8_1 = input.LA(1);

                         
                        int index8_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred33_InternalScratch()) ) {s = 5;}

                        else if ( (synpred34_InternalScratch()) ) {s = 6;}

                        else if ( (synpred35_InternalScratch()) ) {s = 7;}

                        else if ( (synpred36_InternalScratch()) ) {s = 8;}

                        else if ( (synpred37_InternalScratch()) ) {s = 9;}

                         
                        input.seek(index8_1);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 8, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String DFA15_eotS =
        "\32\uffff";
    static final String DFA15_eofS =
        "\1\2\31\uffff";
    static final String DFA15_minS =
        "\1\22\1\0\30\uffff";
    static final String DFA15_maxS =
        "\1\101\1\0\30\uffff";
    static final String DFA15_acceptS =
        "\2\uffff\1\2\26\uffff\1\1";
    static final String DFA15_specialS =
        "\1\uffff\1\0\30\uffff}>";
    static final String[] DFA15_transitionS = {
            "\1\2\3\uffff\1\2\1\1\1\2\1\uffff\2\2\1\uffff\1\2\1\uffff\1\2"+
            "\1\uffff\1\2\1\uffff\4\2\2\uffff\1\2\1\uffff\5\2\15\uffff\3"+
            "\2\1\uffff\1\2",
            "\1\uffff",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA15_eot = DFA.unpackEncodedString(DFA15_eotS);
    static final short[] DFA15_eof = DFA.unpackEncodedString(DFA15_eofS);
    static final char[] DFA15_min = DFA.unpackEncodedStringToUnsignedChars(DFA15_minS);
    static final char[] DFA15_max = DFA.unpackEncodedStringToUnsignedChars(DFA15_maxS);
    static final short[] DFA15_accept = DFA.unpackEncodedString(DFA15_acceptS);
    static final short[] DFA15_special = DFA.unpackEncodedString(DFA15_specialS);
    static final short[][] DFA15_transition;

    static {
        int numStates = DFA15_transitionS.length;
        DFA15_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA15_transition[i] = DFA.unpackEncodedString(DFA15_transitionS[i]);
        }
    }

    class DFA15 extends DFA {

        public DFA15(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 15;
            this.eot = DFA15_eot;
            this.eof = DFA15_eof;
            this.min = DFA15_min;
            this.max = DFA15_max;
            this.accept = DFA15_accept;
            this.special = DFA15_special;
            this.transition = DFA15_transition;
        }
        public String getDescription() {
            return "2776:1: ( rule__Forever__Group_2__0 )?";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA15_1 = input.LA(1);

                         
                        int index15_1 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred47_InternalScratch()) ) {s = 25;}

                        else if ( (true) ) {s = 2;}

                         
                        input.seek(index15_1);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 15, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    public static final BitSet FOLLOW_ruleProject_in_entryRuleProject67 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleProject74 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Project__Group__0_in_ruleProject100 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleObject_in_entryRuleObject127 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleObject134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Object__Group__0_in_ruleObject160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIObject_in_entryRuleIObject187 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIObject194 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IObject__Alternatives_in_ruleIObject220 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRef_in_entryRuleRef247 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRef254 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Ref__ObjectAssignment_in_ruleRef280 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePredef_in_entryRulePredef307 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePredef314 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Predef__ObjectAssignment_in_rulePredef340 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleScript_in_entryRuleScript367 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleScript374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Script__Group__0_in_ruleScript400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhen_in_entryRuleWhen427 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWhen434 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__When__Group__0_in_ruleWhen460 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhenFlag_in_entryRuleWhenFlag487 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWhenFlag494 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhenFlag__Group__0_in_ruleWhenFlag520 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhenMsg_in_entryRuleWhenMsg547 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWhenMsg554 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhenMsg__Group__0_in_ruleWhenMsg580 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessage_in_entryRuleMessage607 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMessage614 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Message__NameAssignment_in_ruleMessage640 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBlock_in_entryRuleBlock667 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBlock674 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Block__Alternatives_in_ruleBlock700 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLoop_in_entryRuleLoop727 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLoop734 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Loop__Group__0_in_ruleLoop760 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForever_in_entryRuleForever787 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleForever794 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Forever__Group__0_in_ruleForever820 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepeatN_in_entryRuleRepeatN847 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRepeatN854 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RepeatN__Group__0_in_ruleRepeatN880 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepeatUntil_in_entryRuleRepeatUntil907 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRepeatUntil914 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RepeatUntil__Group__0_in_ruleRepeatUntil940 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStop_in_entryRuleStop967 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleStop974 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Stop__Group__0_in_ruleStop1000 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWait_in_entryRuleWait1027 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWait1034 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Wait__Group__0_in_ruleWait1060 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWaitDelay_in_entryRuleWaitDelay1087 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWaitDelay1094 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WaitDelay__Group__0_in_ruleWaitDelay1120 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWaitUntil_in_entryRuleWaitUntil1147 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWaitUntil1154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WaitUntil__Group__0_in_ruleWaitUntil1180 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBroadcast_in_entryRuleBroadcast1207 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBroadcast1214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Broadcast__Group__0_in_ruleBroadcast1240 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIf_in_entryRuleIf1267 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIf1274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__Group__0_in_ruleIf1300 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMove_in_entryRuleMove1327 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleMove1334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Move__Group__0_in_ruleMove1360 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurnCW_in_entryRuleTurnCW1387 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTurnCW1394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCW__Group__0_in_ruleTurnCW1420 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurnCCW_in_entryRuleTurnCCW1447 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTurnCCW1454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCCW__Group__0_in_ruleTurnCCW1480 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePointDirection_in_entryRulePointDirection1507 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePointDirection1514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PointDirection__Group__0_in_rulePointDirection1540 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePointTowards_in_entryRulePointTowards1567 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePointTowards1574 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PointTowards__Group__0_in_rulePointTowards1600 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGoToXY_in_entryRuleGoToXY1627 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGoToXY1634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GoToXY__Group__0_in_ruleGoToXY1660 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGoTo_in_entryRuleGoTo1687 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGoTo1694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GoTo__Group__0_in_ruleGoTo1720 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGlide_in_entryRuleGlide1747 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGlide1754 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Glide__Group__0_in_ruleGlide1780 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleChangeX_in_entryRuleChangeX1807 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleChangeX1814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ChangeX__Group__0_in_ruleChangeX1840 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleChangeY_in_entryRuleChangeY1867 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleChangeY1874 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ChangeY__Group__0_in_ruleChangeY1900 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSetX_in_entryRuleSetX1927 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSetX1934 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SetX__Group__0_in_ruleSetX1960 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSetY_in_entryRuleSetY1987 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSetY1994 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SetY__Group__0_in_ruleSetY2020 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIfEdge_in_entryRuleIfEdge2047 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIfEdge2054 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IfEdge__Group__0_in_ruleIfEdge2080 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumber_in_entryRuleNumber2107 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumber2114 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Number__Alternatives_in_ruleNumber2140 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_entryRuleNumberTerminal2167 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumberTerminal2174 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberTerminal__Group__0_in_ruleNumberTerminal2200 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberValue_in_entryRuleNumberValue2227 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumberValue2234 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberValue__ValueAssignment_in_ruleNumberValue2260 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVar_in_entryRuleNumberVar2287 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumberVar2294 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberVar__Group__0_in_ruleNumberVar2320 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVarX_in_entryRuleNumberVarX2347 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumberVarX2354 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberVarX__Group__0_in_ruleNumberVarX2380 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVarY_in_entryRuleNumberVarY2407 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumberVarY2414 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberVarY__Group__0_in_ruleNumberVarY2440 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVarD_in_entryRuleNumberVarD2467 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumberVarD2474 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberVarD__Group__0_in_ruleNumberVarD2500 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberObjVar_in_entryRuleNumberObjVar2527 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNumberObjVar2534 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberObjVar__Group__0_in_ruleNumberObjVar2560 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinary_in_entryRuleBinary2587 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBinary2594 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Binary__Alternatives_in_ruleBinary2620 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_entryRuleBinaryTerminal2647 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBinaryTerminal2654 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BinaryTerminal__Group__0_in_ruleBinaryTerminal2680 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryVar_in_entryRuleBinaryVar2707 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBinaryVar2714 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BinaryVar__Group__0_in_ruleBinaryVar2740 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTouchsColor_in_entryRuleTouchsColor2767 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTouchsColor2774 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TouchsColor__Group__0_in_ruleTouchsColor2800 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLt_in_entryRuleLt2827 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleLt2834 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Lt__Group__0_in_ruleLt2860 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGt_in_entryRuleGt2887 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleGt2894 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Gt__Group__0_in_ruleGt2920 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEq_in_entryRuleEq2947 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleEq2954 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Eq__Group__0_in_ruleEq2980 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_in_entryRuleAnd3007 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAnd3014 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group__0_in_ruleAnd3040 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOr_in_entryRuleOr3067 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOr3074 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group__0_in_ruleOr3100 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNot_in_entryRuleNot3127 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleNot3134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Not__Group__0_in_ruleNot3160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleHide_in_entryRuleHide3187 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleHide3194 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Hide__Group__0_in_ruleHide3220 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleShow_in_entryRuleShow3247 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleShow3254 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Show__Group__0_in_ruleShow3280 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSay_in_entryRuleSay3307 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleSay3314 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Say__Group__0_in_ruleSay3340 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePlay_in_entryRulePlay3367 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePlay3374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Play__Group__0_in_rulePlay3400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleObjectPredefEnum3438 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ObjectVariableEnum__Alternatives_in_ruleObjectVariableEnum3476 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRef_in_rule__IObject__Alternatives3511 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePredef_in_rule__IObject__Alternatives3528 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhenFlag_in_rule__When__Alternatives_13560 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhenMsg_in_rule__When__Alternatives_13577 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleStop_in_rule__Block__Alternatives3609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBroadcast_in_rule__Block__Alternatives3626 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWait_in_rule__Block__Alternatives3643 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIf_in_rule__Block__Alternatives3660 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLoop_in_rule__Block__Alternatives3677 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMove_in_rule__Block__Alternatives3694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurnCW_in_rule__Block__Alternatives3711 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurnCCW_in_rule__Block__Alternatives3728 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePointDirection_in_rule__Block__Alternatives3745 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePointTowards_in_rule__Block__Alternatives3762 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGoToXY_in_rule__Block__Alternatives3779 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGoTo_in_rule__Block__Alternatives3796 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGlide_in_rule__Block__Alternatives3813 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleChangeX_in_rule__Block__Alternatives3830 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleChangeY_in_rule__Block__Alternatives3847 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSetX_in_rule__Block__Alternatives3864 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSetY_in_rule__Block__Alternatives3881 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIfEdge_in_rule__Block__Alternatives3898 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleHide_in_rule__Block__Alternatives3915 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleShow_in_rule__Block__Alternatives3932 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleSay_in_rule__Block__Alternatives3949 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePlay_in_rule__Block__Alternatives3966 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForever_in_rule__Loop__Alternatives_03998 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepeatN_in_rule__Loop__Alternatives_04015 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepeatUntil_in_rule__Loop__Alternatives_04032 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Stop__AllAssignment_1_0_in_rule__Stop__Alternatives_14064 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Stop__ScriptAssignment_1_1_in_rule__Stop__Alternatives_14082 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWaitDelay_in_rule__Wait__Alternatives_14115 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWaitUntil_in_rule__Wait__Alternatives_14132 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberValue_in_rule__Number__Alternatives4164 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberObjVar_in_rule__Number__Alternatives4181 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVar_in_rule__Number__Alternatives4198 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVarX_in_rule__Number__Alternatives4215 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVarY_in_rule__Number__Alternatives4232 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberVarD_in_rule__Number__Alternatives4249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOr_in_rule__Binary__Alternatives4281 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_in_rule__Binary__Alternatives4298 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLt_in_rule__Binary__Alternatives4315 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGt_in_rule__Binary__Alternatives4332 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEq_in_rule__Binary__Alternatives4349 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNot_in_rule__Binary__Alternatives4366 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTouchsColor_in_rule__Binary__Alternatives4383 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryVar_in_rule__Binary__Alternatives4400 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__ObjectVariableEnum__Alternatives4433 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__ObjectVariableEnum__Alternatives4454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__ObjectVariableEnum__Alternatives4475 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Project__Group__0__Impl_in_rule__Project__Group__04508 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Project__Group__1_in_rule__Project__Group__04511 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Project__Group__0__Impl4539 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Project__Group__1__Impl_in_rule__Project__Group__14570 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Project__Group__2_in_rule__Project__Group__14573 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Project__NameAssignment_1_in_rule__Project__Group__1__Impl4600 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Project__Group__2__Impl_in_rule__Project__Group__24630 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Project__ObjectsAssignment_2_in_rule__Project__Group__2__Impl4657 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rule__Object__Group__0__Impl_in_rule__Object__Group__04694 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Object__Group__1_in_rule__Object__Group__04697 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Object__Group__0__Impl4725 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Object__Group__1__Impl_in_rule__Object__Group__14756 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Object__Group__2_in_rule__Object__Group__14759 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Object__NameAssignment_1_in_rule__Object__Group__1__Impl4786 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Object__Group__2__Impl_in_rule__Object__Group__24816 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Object__ScriptsAssignment_2_in_rule__Object__Group__2__Impl4843 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_rule__Script__Group__0__Impl_in_rule__Script__Group__04880 = new BitSet(new long[]{0xE000FA7AADCC0000L,0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Script__Group__1_in_rule__Script__Group__04883 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Script__Group__0__Impl4911 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Script__Group__1__Impl_in_rule__Script__Group__14942 = new BitSet(new long[]{0xE000FA7AADCC0000L,0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Script__Group__2_in_rule__Script__Group__14945 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Script__Group__2__Impl_in_rule__Script__Group__25003 = new BitSet(new long[]{0xE000FA7AADCC0000L,0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Script__Group__3_in_rule__Script__Group__25006 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Script__WhenAssignment_2_in_rule__Script__Group__2__Impl5033 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Script__Group__3__Impl_in_rule__Script__Group__35064 = new BitSet(new long[]{0xE000FA7AADCC0000L,0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Script__Group__4_in_rule__Script__Group__35067 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Script__BlocksAssignment_3_in_rule__Script__Group__3__Impl5094 = new BitSet(new long[]{0xE000FA7AADC00002L,0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Script__Group__4__Impl_in_rule__Script__Group__45125 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Script__Group__4__Impl5153 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__When__Group__0__Impl_in_rule__When__Group__05194 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_rule__When__Group__1_in_rule__When__Group__05197 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__When__Group__0__Impl5225 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__When__Group__1__Impl_in_rule__When__Group__15256 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__When__Alternatives_1_in_rule__When__Group__1__Impl5283 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhenFlag__Group__0__Impl_in_rule__WhenFlag__Group__05317 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__WhenFlag__Group__1_in_rule__WhenFlag__Group__05320 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__WhenFlag__Group__0__Impl5348 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhenFlag__Group__1__Impl_in_rule__WhenFlag__Group__15379 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhenMsg__Group__0__Impl_in_rule__WhenMsg__Group__05441 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__WhenMsg__Group__1_in_rule__WhenMsg__Group__05444 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__WhenMsg__Group__0__Impl5472 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhenMsg__Group__1__Impl_in_rule__WhenMsg__Group__15503 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WhenMsg__MsgAssignment_1_in_rule__WhenMsg__Group__1__Impl5530 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Loop__Group__0__Impl_in_rule__Loop__Group__05564 = new BitSet(new long[]{0xE000FA7AADC40000L,0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Loop__Group__1_in_rule__Loop__Group__05567 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Loop__Alternatives_0_in_rule__Loop__Group__0__Impl5594 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Loop__Group__1__Impl_in_rule__Loop__Group__15624 = new BitSet(new long[]{0xE000FA7AADC40000L,0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Loop__Group__2_in_rule__Loop__Group__15627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Loop__BlocksAssignment_1_in_rule__Loop__Group__1__Impl5654 = new BitSet(new long[]{0xE000FA7AADC00002L,0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Loop__Group__2__Impl_in_rule__Loop__Group__25685 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Loop__Group__2__Impl5713 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Forever__Group__0__Impl_in_rule__Forever__Group__05750 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__Forever__Group__1_in_rule__Forever__Group__05753 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Forever__Group__0__Impl5781 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Forever__Group__1__Impl_in_rule__Forever__Group__15812 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__Forever__Group__2_in_rule__Forever__Group__15815 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Forever__Group__2__Impl_in_rule__Forever__Group__25873 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Forever__Group_2__0_in_rule__Forever__Group__2__Impl5900 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Forever__Group_2__0__Impl_in_rule__Forever__Group_2__05937 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__Forever__Group_2__1_in_rule__Forever__Group_2__05940 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__Forever__Group_2__0__Impl5968 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Forever__Group_2__1__Impl_in_rule__Forever__Group_2__15999 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Forever__CondAssignment_2_1_in_rule__Forever__Group_2__1__Impl6026 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RepeatN__Group__0__Impl_in_rule__RepeatN__Group__06060 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__RepeatN__Group__1_in_rule__RepeatN__Group__06063 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__RepeatN__Group__0__Impl6091 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RepeatN__Group__1__Impl_in_rule__RepeatN__Group__16122 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RepeatN__NAssignment_1_in_rule__RepeatN__Group__1__Impl6149 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RepeatUntil__Group__0__Impl_in_rule__RepeatUntil__Group__06183 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_rule__RepeatUntil__Group__1_in_rule__RepeatUntil__Group__06186 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__RepeatUntil__Group__0__Impl6214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RepeatUntil__Group__1__Impl_in_rule__RepeatUntil__Group__16245 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__RepeatUntil__Group__2_in_rule__RepeatUntil__Group__16248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__RepeatUntil__Group__1__Impl6276 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RepeatUntil__Group__2__Impl_in_rule__RepeatUntil__Group__26307 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RepeatUntil__CondAssignment_2_in_rule__RepeatUntil__Group__2__Impl6334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Stop__Group__0__Impl_in_rule__Stop__Group__06370 = new BitSet(new long[]{0x0000000000020000L,0x0000000000000008L});
    public static final BitSet FOLLOW_rule__Stop__Group__1_in_rule__Stop__Group__06373 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_26_in_rule__Stop__Group__0__Impl6401 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Stop__Group__1__Impl_in_rule__Stop__Group__16432 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Stop__Alternatives_1_in_rule__Stop__Group__1__Impl6459 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Wait__Group__0__Impl_in_rule__Wait__Group__06493 = new BitSet(new long[]{0x0001000002000000L});
    public static final BitSet FOLLOW_rule__Wait__Group__1_in_rule__Wait__Group__06496 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_27_in_rule__Wait__Group__0__Impl6524 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Wait__Group__1__Impl_in_rule__Wait__Group__16555 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Wait__Alternatives_1_in_rule__Wait__Group__1__Impl6582 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WaitDelay__Group__0__Impl_in_rule__WaitDelay__Group__06616 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rule__WaitDelay__Group__1_in_rule__WaitDelay__Group__06619 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WaitDelay__DelayAssignment_0_in_rule__WaitDelay__Group__0__Impl6646 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WaitDelay__Group__1__Impl_in_rule__WaitDelay__Group__16676 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__WaitDelay__Group__1__Impl6704 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WaitUntil__Group__0__Impl_in_rule__WaitUntil__Group__06739 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__WaitUntil__Group__1_in_rule__WaitUntil__Group__06742 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__WaitUntil__Group__0__Impl6770 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WaitUntil__Group__1__Impl_in_rule__WaitUntil__Group__16801 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__WaitUntil__CondAssignment_1_in_rule__WaitUntil__Group__1__Impl6828 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Broadcast__Group__0__Impl_in_rule__Broadcast__Group__06862 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Broadcast__Group__1_in_rule__Broadcast__Group__06865 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_29_in_rule__Broadcast__Group__0__Impl6893 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Broadcast__Group__1__Impl_in_rule__Broadcast__Group__16924 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Broadcast__Group__2_in_rule__Broadcast__Group__16927 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Broadcast__MsgAssignment_1_in_rule__Broadcast__Group__1__Impl6954 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Broadcast__Group__2__Impl_in_rule__Broadcast__Group__26984 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Broadcast__WaitAssignment_2_in_rule__Broadcast__Group__2__Impl7011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__Group__0__Impl_in_rule__If__Group__07048 = new BitSet(new long[]{0x0000000040040000L});
    public static final BitSet FOLLOW_rule__If__Group__1_in_rule__If__Group__07051 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__Group_0__0_in_rule__If__Group__0__Impl7078 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__Group__1__Impl_in_rule__If__Group__17108 = new BitSet(new long[]{0x0000000040040000L});
    public static final BitSet FOLLOW_rule__If__Group__2_in_rule__If__Group__17111 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__Group_1__0_in_rule__If__Group__1__Impl7138 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__Group__2__Impl_in_rule__If__Group__27169 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__If__Group__2__Impl7197 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__Group_0__0__Impl_in_rule__If__Group_0__07234 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__If__Group_0__1_in_rule__If__Group_0__07237 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__If__Group_0__0__Impl7265 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__Group_0__1__Impl_in_rule__If__Group_0__17296 = new BitSet(new long[]{0xE000FA7AADC00000L,0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__Group_0__2_in_rule__If__Group_0__17299 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__CondAssignment_0_1_in_rule__If__Group_0__1__Impl7326 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__Group_0__2__Impl_in_rule__If__Group_0__27356 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__TrueAssignment_0_2_in_rule__If__Group_0__2__Impl7383 = new BitSet(new long[]{0xE000FA7AADC00002L,0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__Group_1__0__Impl_in_rule__If__Group_1__07420 = new BitSet(new long[]{0xE000FA7AADC00000L,0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__Group_1__1_in_rule__If__Group_1__07423 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_30_in_rule__If__Group_1__0__Impl7451 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__Group_1__1__Impl_in_rule__If__Group_1__17482 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__If__FalseAssignment_1_1_in_rule__If__Group_1__1__Impl7509 = new BitSet(new long[]{0xE000FA7AADC00002L,0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Move__Group__0__Impl_in_rule__Move__Group__07544 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__Move__Group__1_in_rule__Move__Group__07547 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_31_in_rule__Move__Group__0__Impl7575 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Move__Group__1__Impl_in_rule__Move__Group__17606 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_rule__Move__Group__2_in_rule__Move__Group__17609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Move__StepsAssignment_1_in_rule__Move__Group__1__Impl7636 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Move__Group__2__Impl_in_rule__Move__Group__27666 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_32_in_rule__Move__Group__2__Impl7694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCW__Group__0__Impl_in_rule__TurnCW__Group__07731 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__TurnCW__Group__1_in_rule__TurnCW__Group__07734 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_33_in_rule__TurnCW__Group__0__Impl7762 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCW__Group__1__Impl_in_rule__TurnCW__Group__17793 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_rule__TurnCW__Group__2_in_rule__TurnCW__Group__17796 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCW__AngleAssignment_1_in_rule__TurnCW__Group__1__Impl7823 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCW__Group__2__Impl_in_rule__TurnCW__Group__27853 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_rule__TurnCW__Group__2__Impl7881 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCCW__Group__0__Impl_in_rule__TurnCCW__Group__07918 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__TurnCCW__Group__1_in_rule__TurnCCW__Group__07921 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_35_in_rule__TurnCCW__Group__0__Impl7949 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCCW__Group__1__Impl_in_rule__TurnCCW__Group__17980 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_rule__TurnCCW__Group__2_in_rule__TurnCCW__Group__17983 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCCW__AngleAssignment_1_in_rule__TurnCCW__Group__1__Impl8010 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TurnCCW__Group__2__Impl_in_rule__TurnCCW__Group__28040 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_34_in_rule__TurnCCW__Group__2__Impl8068 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PointDirection__Group__0__Impl_in_rule__PointDirection__Group__08105 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__PointDirection__Group__1_in_rule__PointDirection__Group__08108 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_36_in_rule__PointDirection__Group__0__Impl8136 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PointDirection__Group__1__Impl_in_rule__PointDirection__Group__18167 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PointDirection__AngleAssignment_1_in_rule__PointDirection__Group__1__Impl8194 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PointTowards__Group__0__Impl_in_rule__PointTowards__Group__08228 = new BitSet(new long[]{0x0000000000000810L});
    public static final BitSet FOLLOW_rule__PointTowards__Group__1_in_rule__PointTowards__Group__08231 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_37_in_rule__PointTowards__Group__0__Impl8259 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PointTowards__Group__1__Impl_in_rule__PointTowards__Group__18290 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PointTowards__ObjectAssignment_1_in_rule__PointTowards__Group__1__Impl8317 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GoToXY__Group__0__Impl_in_rule__GoToXY__Group__08351 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_rule__GoToXY__Group__1_in_rule__GoToXY__Group__08354 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_rule__GoToXY__Group__0__Impl8382 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GoToXY__Group__1__Impl_in_rule__GoToXY__Group__18413 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__GoToXY__Group__2_in_rule__GoToXY__Group__18416 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_39_in_rule__GoToXY__Group__1__Impl8444 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GoToXY__Group__2__Impl_in_rule__GoToXY__Group__28475 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_rule__GoToXY__Group__3_in_rule__GoToXY__Group__28478 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GoToXY__XAssignment_2_in_rule__GoToXY__Group__2__Impl8505 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GoToXY__Group__3__Impl_in_rule__GoToXY__Group__38535 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__GoToXY__Group__4_in_rule__GoToXY__Group__38538 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_rule__GoToXY__Group__3__Impl8566 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GoToXY__Group__4__Impl_in_rule__GoToXY__Group__48597 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GoToXY__YAssignment_4_in_rule__GoToXY__Group__4__Impl8624 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GoTo__Group__0__Impl_in_rule__GoTo__Group__08664 = new BitSet(new long[]{0x0000000000000810L});
    public static final BitSet FOLLOW_rule__GoTo__Group__1_in_rule__GoTo__Group__08667 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_38_in_rule__GoTo__Group__0__Impl8695 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GoTo__Group__1__Impl_in_rule__GoTo__Group__18726 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__GoTo__ObjectAssignment_1_in_rule__GoTo__Group__1__Impl8753 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Glide__Group__0__Impl_in_rule__Glide__Group__08787 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__Glide__Group__1_in_rule__Glide__Group__08790 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_rule__Glide__Group__0__Impl8818 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Glide__Group__1__Impl_in_rule__Glide__Group__18849 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_rule__Glide__Group__2_in_rule__Glide__Group__18852 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Glide__DurationAssignment_1_in_rule__Glide__Group__1__Impl8879 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Glide__Group__2__Impl_in_rule__Glide__Group__28909 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__Glide__Group__3_in_rule__Glide__Group__28912 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_42_in_rule__Glide__Group__2__Impl8940 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Glide__Group__3__Impl_in_rule__Glide__Group__38971 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_rule__Glide__Group__4_in_rule__Glide__Group__38974 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Glide__XAssignment_3_in_rule__Glide__Group__3__Impl9001 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Glide__Group__4__Impl_in_rule__Glide__Group__49031 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__Glide__Group__5_in_rule__Glide__Group__49034 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_40_in_rule__Glide__Group__4__Impl9062 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Glide__Group__5__Impl_in_rule__Glide__Group__59093 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Glide__YAssignment_5_in_rule__Glide__Group__5__Impl9120 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ChangeX__Group__0__Impl_in_rule__ChangeX__Group__09162 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__ChangeX__Group__1_in_rule__ChangeX__Group__09165 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_43_in_rule__ChangeX__Group__0__Impl9193 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ChangeX__Group__1__Impl_in_rule__ChangeX__Group__19224 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ChangeX__XAssignment_1_in_rule__ChangeX__Group__1__Impl9251 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ChangeY__Group__0__Impl_in_rule__ChangeY__Group__09285 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__ChangeY__Group__1_in_rule__ChangeY__Group__09288 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_44_in_rule__ChangeY__Group__0__Impl9316 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ChangeY__Group__1__Impl_in_rule__ChangeY__Group__19347 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ChangeY__YAssignment_1_in_rule__ChangeY__Group__1__Impl9374 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SetX__Group__0__Impl_in_rule__SetX__Group__09408 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__SetX__Group__1_in_rule__SetX__Group__09411 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_45_in_rule__SetX__Group__0__Impl9439 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SetX__Group__1__Impl_in_rule__SetX__Group__19470 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SetX__XAssignment_1_in_rule__SetX__Group__1__Impl9497 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SetY__Group__0__Impl_in_rule__SetY__Group__09531 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__SetY__Group__1_in_rule__SetY__Group__09534 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_46_in_rule__SetY__Group__0__Impl9562 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SetY__Group__1__Impl_in_rule__SetY__Group__19593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__SetY__YAssignment_1_in_rule__SetY__Group__1__Impl9620 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IfEdge__Group__0__Impl_in_rule__IfEdge__Group__09654 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__IfEdge__Group__1_in_rule__IfEdge__Group__09657 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_47_in_rule__IfEdge__Group__0__Impl9685 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__IfEdge__Group__1__Impl_in_rule__IfEdge__Group__19716 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberTerminal__Group__0__Impl_in_rule__NumberTerminal__Group__09778 = new BitSet(new long[]{0x0004000000007020L});
    public static final BitSet FOLLOW_rule__NumberTerminal__Group__1_in_rule__NumberTerminal__Group__09781 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_rule__NumberTerminal__Group__0__Impl9809 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberTerminal__Group__1__Impl_in_rule__NumberTerminal__Group__19840 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_rule__NumberTerminal__Group__2_in_rule__NumberTerminal__Group__19843 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumber_in_rule__NumberTerminal__Group__1__Impl9870 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberTerminal__Group__2__Impl_in_rule__NumberTerminal__Group__29899 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_rule__NumberTerminal__Group__2__Impl9927 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberVar__Group__0__Impl_in_rule__NumberVar__Group__09964 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__NumberVar__Group__1_in_rule__NumberVar__Group__09967 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_rule__NumberVar__Group__0__Impl9995 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberVar__Group__1__Impl_in_rule__NumberVar__Group__110026 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberVar__VarAssignment_1_in_rule__NumberVar__Group__1__Impl10053 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberVarX__Group__0__Impl_in_rule__NumberVarX__Group__010087 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__NumberVarX__Group__1_in_rule__NumberVarX__Group__010090 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__NumberVarX__Group__0__Impl10118 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberVarX__Group__1__Impl_in_rule__NumberVarX__Group__110149 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberVarY__Group__0__Impl_in_rule__NumberVarY__Group__010211 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__NumberVarY__Group__1_in_rule__NumberVarY__Group__010214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__NumberVarY__Group__0__Impl10242 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberVarY__Group__1__Impl_in_rule__NumberVarY__Group__110273 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberVarD__Group__0__Impl_in_rule__NumberVarD__Group__010335 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__NumberVarD__Group__1_in_rule__NumberVarD__Group__010338 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__NumberVarD__Group__0__Impl10366 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberVarD__Group__1__Impl_in_rule__NumberVarD__Group__110397 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberObjVar__Group__0__Impl_in_rule__NumberObjVar__Group__010459 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_rule__NumberObjVar__Group__1_in_rule__NumberObjVar__Group__010462 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberObjVar__VarAssignment_0_in_rule__NumberObjVar__Group__0__Impl10489 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberObjVar__Group__1__Impl_in_rule__NumberObjVar__Group__110519 = new BitSet(new long[]{0x0000000000000810L});
    public static final BitSet FOLLOW_rule__NumberObjVar__Group__2_in_rule__NumberObjVar__Group__110522 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_51_in_rule__NumberObjVar__Group__1__Impl10550 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberObjVar__Group__2__Impl_in_rule__NumberObjVar__Group__210581 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NumberObjVar__ObjAssignment_2_in_rule__NumberObjVar__Group__2__Impl10608 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BinaryTerminal__Group__0__Impl_in_rule__BinaryTerminal__Group__010644 = new BitSet(new long[]{0x1015000000000000L});
    public static final BitSet FOLLOW_rule__BinaryTerminal__Group__1_in_rule__BinaryTerminal__Group__010647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_48_in_rule__BinaryTerminal__Group__0__Impl10675 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BinaryTerminal__Group__1__Impl_in_rule__BinaryTerminal__Group__110706 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_rule__BinaryTerminal__Group__2_in_rule__BinaryTerminal__Group__110709 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinary_in_rule__BinaryTerminal__Group__1__Impl10736 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BinaryTerminal__Group__2__Impl_in_rule__BinaryTerminal__Group__210765 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_49_in_rule__BinaryTerminal__Group__2__Impl10793 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BinaryVar__Group__0__Impl_in_rule__BinaryVar__Group__010830 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__BinaryVar__Group__1_in_rule__BinaryVar__Group__010833 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_rule__BinaryVar__Group__0__Impl10861 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BinaryVar__Group__1__Impl_in_rule__BinaryVar__Group__110892 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__BinaryVar__VarAssignment_1_in_rule__BinaryVar__Group__1__Impl10919 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TouchsColor__Group__0__Impl_in_rule__TouchsColor__Group__010953 = new BitSet(new long[]{0x0020000000000000L});
    public static final BitSet FOLLOW_rule__TouchsColor__Group__1_in_rule__TouchsColor__Group__010956 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_52_in_rule__TouchsColor__Group__0__Impl10984 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TouchsColor__Group__1__Impl_in_rule__TouchsColor__Group__111015 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__TouchsColor__Group__2_in_rule__TouchsColor__Group__111018 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_53_in_rule__TouchsColor__Group__1__Impl11046 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TouchsColor__Group__2__Impl_in_rule__TouchsColor__Group__211077 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_rule__TouchsColor__Group__3_in_rule__TouchsColor__Group__211080 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TouchsColor__ColorAssignment_2_in_rule__TouchsColor__Group__2__Impl11107 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TouchsColor__Group__3__Impl_in_rule__TouchsColor__Group__311137 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_54_in_rule__TouchsColor__Group__3__Impl11165 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Lt__Group__0__Impl_in_rule__Lt__Group__011204 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_rule__Lt__Group__1_in_rule__Lt__Group__011207 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Lt__LeftAssignment_0_in_rule__Lt__Group__0__Impl11234 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Lt__Group__1__Impl_in_rule__Lt__Group__111264 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__Lt__Group__2_in_rule__Lt__Group__111267 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_55_in_rule__Lt__Group__1__Impl11295 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Lt__Group__2__Impl_in_rule__Lt__Group__211326 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Lt__RightAssignment_2_in_rule__Lt__Group__2__Impl11353 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Gt__Group__0__Impl_in_rule__Gt__Group__011389 = new BitSet(new long[]{0x0100000000000000L});
    public static final BitSet FOLLOW_rule__Gt__Group__1_in_rule__Gt__Group__011392 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Gt__LeftAssignment_0_in_rule__Gt__Group__0__Impl11419 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Gt__Group__1__Impl_in_rule__Gt__Group__111449 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__Gt__Group__2_in_rule__Gt__Group__111452 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_56_in_rule__Gt__Group__1__Impl11480 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Gt__Group__2__Impl_in_rule__Gt__Group__211511 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Gt__RightAssignment_2_in_rule__Gt__Group__2__Impl11538 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Eq__Group__0__Impl_in_rule__Eq__Group__011574 = new BitSet(new long[]{0x0200000000000000L});
    public static final BitSet FOLLOW_rule__Eq__Group__1_in_rule__Eq__Group__011577 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Eq__LeftAssignment_0_in_rule__Eq__Group__0__Impl11604 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Eq__Group__1__Impl_in_rule__Eq__Group__111634 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__Eq__Group__2_in_rule__Eq__Group__111637 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_57_in_rule__Eq__Group__1__Impl11665 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Eq__Group__2__Impl_in_rule__Eq__Group__211696 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Eq__RightAssignment_2_in_rule__Eq__Group__2__Impl11723 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group__0__Impl_in_rule__And__Group__011759 = new BitSet(new long[]{0x0400000000000000L});
    public static final BitSet FOLLOW_rule__And__Group__1_in_rule__And__Group__011762 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__LeftAssignment_0_in_rule__And__Group__0__Impl11789 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group__1__Impl_in_rule__And__Group__111819 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__And__Group__2_in_rule__And__Group__111822 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_58_in_rule__And__Group__1__Impl11850 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__Group__2__Impl_in_rule__And__Group__211881 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__And__RightAssignment_2_in_rule__And__Group__2__Impl11908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group__0__Impl_in_rule__Or__Group__011944 = new BitSet(new long[]{0x0800000000000000L});
    public static final BitSet FOLLOW_rule__Or__Group__1_in_rule__Or__Group__011947 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__LeftAssignment_0_in_rule__Or__Group__0__Impl11974 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group__1__Impl_in_rule__Or__Group__112004 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__Or__Group__2_in_rule__Or__Group__112007 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_59_in_rule__Or__Group__1__Impl12035 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__Group__2__Impl_in_rule__Or__Group__212066 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Or__RightAssignment_2_in_rule__Or__Group__2__Impl12093 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Not__Group__0__Impl_in_rule__Not__Group__012129 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__Not__Group__1_in_rule__Not__Group__012132 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_60_in_rule__Not__Group__0__Impl12160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Not__Group__1__Impl_in_rule__Not__Group__112191 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Not__ValueAssignment_1_in_rule__Not__Group__1__Impl12218 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Hide__Group__0__Impl_in_rule__Hide__Group__012252 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Hide__Group__1_in_rule__Hide__Group__012255 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_61_in_rule__Hide__Group__0__Impl12283 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Hide__Group__1__Impl_in_rule__Hide__Group__112314 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Show__Group__0__Impl_in_rule__Show__Group__012376 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_rule__Show__Group__1_in_rule__Show__Group__012379 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_62_in_rule__Show__Group__0__Impl12407 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Show__Group__1__Impl_in_rule__Show__Group__112438 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Say__Group__0__Impl_in_rule__Say__Group__012500 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_rule__Say__Group__1_in_rule__Say__Group__012503 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_63_in_rule__Say__Group__0__Impl12531 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Say__Group__1__Impl_in_rule__Say__Group__112562 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_rule__Say__Group__2_in_rule__Say__Group__112565 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Say__SentenceAssignment_1_in_rule__Say__Group__1__Impl12592 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Say__Group__2__Impl_in_rule__Say__Group__212622 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Say__Group_2__0_in_rule__Say__Group__2__Impl12649 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Say__Group_2__0__Impl_in_rule__Say__Group_2__012686 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_rule__Say__Group_2__1_in_rule__Say__Group_2__012689 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_64_in_rule__Say__Group_2__0__Impl12717 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Say__Group_2__1__Impl_in_rule__Say__Group_2__112748 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_rule__Say__Group_2__2_in_rule__Say__Group_2__112751 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Say__DelayAssignment_2_1_in_rule__Say__Group_2__1__Impl12778 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Say__Group_2__2__Impl_in_rule__Say__Group_2__212808 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_28_in_rule__Say__Group_2__2__Impl12836 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Play__Group__0__Impl_in_rule__Play__Group__012873 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000004L});
    public static final BitSet FOLLOW_rule__Play__Group__1_in_rule__Play__Group__012876 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_65_in_rule__Play__Group__0__Impl12904 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Play__Group__1__Impl_in_rule__Play__Group__112935 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Play__Group__2_in_rule__Play__Group__112938 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_66_in_rule__Play__Group__1__Impl12966 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Play__Group__2__Impl_in_rule__Play__Group__212997 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Play__SoundAssignment_2_in_rule__Play__Group__2__Impl13024 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Project__NameAssignment_113065 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleObject_in_rule__Project__ObjectsAssignment_213096 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Object__NameAssignment_113127 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleScript_in_rule__Object__ScriptsAssignment_213158 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Ref__ObjectAssignment13193 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleObjectPredefEnum_in_rule__Predef__ObjectAssignment13228 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWhen_in_rule__Script__WhenAssignment_213259 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBlock_in_rule__Script__BlocksAssignment_313290 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessage_in_rule__WhenMsg__MsgAssignment_113321 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Message__NameAssignment13352 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBlock_in_rule__Loop__BlocksAssignment_113383 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_rule__Forever__CondAssignment_2_113414 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__RepeatN__NAssignment_113445 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_rule__RepeatUntil__CondAssignment_213476 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_67_in_rule__Stop__AllAssignment_1_013512 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Stop__ScriptAssignment_1_113556 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__WaitDelay__DelayAssignment_013595 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_rule__WaitUntil__CondAssignment_113626 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleMessage_in_rule__Broadcast__MsgAssignment_113657 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_68_in_rule__Broadcast__WaitAssignment_213693 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_rule__If__CondAssignment_0_113732 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBlock_in_rule__If__TrueAssignment_0_213763 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBlock_in_rule__If__FalseAssignment_1_113794 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__Move__StepsAssignment_113825 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__TurnCW__AngleAssignment_113856 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__TurnCCW__AngleAssignment_113887 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__PointDirection__AngleAssignment_113918 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIObject_in_rule__PointTowards__ObjectAssignment_113949 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__GoToXY__XAssignment_213980 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__GoToXY__YAssignment_414011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIObject_in_rule__GoTo__ObjectAssignment_114042 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__Glide__DurationAssignment_114073 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__Glide__XAssignment_314104 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__Glide__YAssignment_514135 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__ChangeX__XAssignment_114166 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__ChangeY__YAssignment_114197 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__SetX__XAssignment_114228 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__SetY__YAssignment_114259 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_NUM_in_rule__NumberValue__ValueAssignment14290 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__NumberVar__VarAssignment_114321 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleObjectVariableEnum_in_rule__NumberObjVar__VarAssignment_014352 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIObject_in_rule__NumberObjVar__ObjAssignment_214383 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__BinaryVar__VarAssignment_114414 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__TouchsColor__ColorAssignment_214445 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__Lt__LeftAssignment_014476 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__Lt__RightAssignment_214507 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__Gt__LeftAssignment_014538 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__Gt__RightAssignment_214569 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__Eq__LeftAssignment_014600 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__Eq__RightAssignment_214631 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_rule__And__LeftAssignment_014662 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_rule__And__RightAssignment_214693 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_rule__Or__LeftAssignment_014724 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_rule__Or__RightAssignment_214755 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBinaryTerminal_in_rule__Not__ValueAssignment_114786 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Say__SentenceAssignment_114817 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNumberTerminal_in_rule__Say__DelayAssignment_2_114848 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Play__SoundAssignment_214879 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOr_in_synpred33_InternalScratch4281 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAnd_in_synpred34_InternalScratch4298 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleLt_in_synpred35_InternalScratch4315 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleGt_in_synpred36_InternalScratch4332 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleEq_in_synpred37_InternalScratch4349 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Forever__Group_2__0_in_synpred47_InternalScratch5900 = new BitSet(new long[]{0x0000000000000002L});

}