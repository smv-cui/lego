package demo.scratch.ui.editor;

import org.apache.log4j.Logger;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.IVerticalRuler;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import demo.commons.LogManager;
import demo.commons.UnclosableEditor;

public class ScratchEditor extends UnclosableEditor {
	private static final Logger LOG = LogManager.INSTANCE.getLogger(ScratchEditor.class.getName());

	public ScratchEditor() {
		super();
		if (LOG.isDebugEnabled()) LOG.debug("Call constructor method");
	}

	@Override
	protected ISourceViewer createSourceViewer(Composite parent, IVerticalRuler ruler, int styles) {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"createSourceViewer\" method");
		// TODO read only scratch editor
		// workaround synchronize (generate) with Lego ?
		//return super.createSourceViewer(parent, ruler, styles | SWT.READ_ONLY);
		ISourceViewer sv = super.createSourceViewer(parent, ruler, styles | SWT.READ_ONLY);
		sv.setEditable(false);
		return sv;
	}
}
