package demo.commons.generator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public abstract class InstructionsSet implements Serializable {
    
    private static final long serialVersionUID = 6005158535736666003L;
    private List<Instruction> instructions = new ArrayList<Instruction>();
	
	public List<Instruction> getInstructions() {
		build();
		return instructions;
	}
	
	protected void addInstruction(Instruction instr) {
		instructions.add(instr);
	}

	protected abstract void build();
}
