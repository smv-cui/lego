package demo.commons;

import java.io.IOException;
import java.net.ServerSocket;

public class LockSocket {
	public static final LockSocket INSTANCE = new LockSocket();
	private static final int LOCK_SOCKET_ID = 55677;
	private ServerSocket socket;
	private LockSocket() {
		socket = null;
	}
	public synchronized void release() {
		if (socket == null) return;
		try {
			socket.close();
		}
		catch (IOException e) { /* don't care */ }
		finally {
			socket = null;
		}
	}
	public synchronized boolean acquire() {
		try {
			socket = new ServerSocket(LOCK_SOCKET_ID);
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}
}
