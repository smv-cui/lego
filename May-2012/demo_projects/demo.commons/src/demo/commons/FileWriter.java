package demo.commons;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class FileWriter {
	public static void write(String filename, String content) throws IOException {
		final File file = new File(filename);
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
		try {
			writer.append(content);
		} finally {
			writer.close();
		}
	}
}
