package demo.commons;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;

import org.apache.log4j.Hierarchy;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.spi.RootLogger;
import org.eclipse.core.runtime.ILog;


/**
 * LogManager
 * This class encapsulates a Log4J Hierarchy and centralizes all Logger access.
 * @author Manoel Marques - original contribution
 * @author Nicolas Sedlmajer - removed useless stuff
 */
public class LogManager {

	public static LogManager INSTANCE;
	
	private Hierarchy hierarchy;
	private HashMap<String, LogListener> hookedPlugins; 

	/**
	 * Creates a new LogManager.
	 * Configure the hierarchy with the properties passed.
	 * Add this object to the list of active plug-in log managers. 
	 * @param properties log configuration properties
	 */
	private LogManager(Properties properties) {
		this.hookedPlugins = new HashMap<String, LogListener>();
		this.hierarchy = new Hierarchy(new RootLogger(Level.ALL));
		new PropertyConfigurator().doConfigure(properties,this.hierarchy);
	}

	public static LogManager initialize(Properties properties) {
		if (INSTANCE == null) INSTANCE = new LogManager(properties);
		return INSTANCE;
	}
	
	/**
	 * Removes appenders and disposes the logger hierarchy
	 */
	public void shutdown() {
		synchronized(this.hookedPlugins) {
			Iterator<String> it = this.hookedPlugins.keySet().iterator();
			while (it.hasNext()) {
				String id = (String) it.next(); 
				LogListener listener = (LogListener) this.hookedPlugins.get(id);
				listener.dispose(); 
			}
			this.hookedPlugins.clear(); 
		}	
		this.hierarchy.shutdown();
	}

	/**
	 * Hooks a plug-in into this PluginLogManager. When the hooked plug-in uses the
	 * Eclipse log API, it will be channeled to this logging framework.
	 * @param id logger name (usually the the plug-in id)
	 * @param log plug-in log
	 */
	public boolean hookPlugin(String id, ILog log) {
		synchronized(this.hookedPlugins) {
			if (log == null || id == null || this.hookedPlugins.containsKey(id)) return false;
			LogListener listener = new LogListener(log,getLogger(id));
			this.hookedPlugins.put(id,listener);
		}
		return true;
	}

	/**
	 * Unhooks a plug-in from this PluginLogManager. The Eclipse log API
	 * won't be channeled to this logging framework anymore.
	 * @param id logger name (usually the the plug-in id)
	 * /
	public boolean unHookPlugin(String id) {
		synchronized(this.hookedPlugins) {
			if (id == null || !this.hookedPlugins.containsKey(id)) return false;
			LogListener listener = (LogListener) this.hookedPlugins.get(id);
			listener.dispose(); 
			this.hookedPlugins.remove(id);
		}
		return true;
	}

	/**
	 * Checks if this PluginLogManager is disabled for this level.
	 * @param level level value
	 * @return boolean true if it is disabled
	 * /
	public boolean isDisabled(int level) {
		return this.hierarchy.isDisabled(level);
	}

	/**
	 * Enable logging for logging requests with level l or higher.
	 * By default all levels are enabled.
	 * @param level level object
	 * /
	public void setThreshold(Level level) {
		this.hierarchy.setThreshold(level);
	}

	/**
	 * The string version of setThreshold(Level level)
	 * @param level level string
	 * /
	public void setThreshold(String level) {
		this.hierarchy.setThreshold(level);
	}

	/**
	 * Get the repository-wide threshold.
	 * @return Level
	 * /
	public Level getThreshold() {
		return this.hierarchy.getThreshold();
	}

	/**
	 * Resets configuration values to its defaults.
	 * /
	public void resetConfiguration() {
		this.hierarchy.resetConfiguration();
	}

	/**
	 * Checks if this logger exists.
	 * @return Logger
	 * /
	public Logger exists(String name) {
		return this.hierarchy.exists(name);
	}

	/**
	 * Returns all the loggers in this manager.
	 * @return Enumeration logger enumeration
	 * /
	public Enumeration<?> getCurrentLoggers() {
		return this.hierarchy.getCurrentLoggers();
	}

	/**
	 * The same as getLogger(String name) but using a factory instance instead of
	 * a default factory.
	 * @param name logger name
	 * @param factory factory instance 
	 * @return Logger
	 * /
	public Logger getLogger(String name, LoggerFactory factory) {
		return this.hierarchy.getLogger(name,factory);
	}

	/**
	 * Returns a new logger instance named as the first parameter
	 * using the default factory. If a logger of that name already exists,
	 * then it will be returned. Otherwise, a new logger will be instantiated 
	 * and then linked with its existing ancestors as well as children.
	 * @param name logger name
	 * @return Logger
	 */
	public Logger getLogger(String name) {
		return this.hierarchy.getLogger(name);
	}

	/**
	 * Returns the root of this hierarchy.
	 * @return Logger
	 */
	public Logger getRootLogger() {
		return this.hierarchy.getRootLogger();
	}
}