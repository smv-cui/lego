package demo.commons;

import java.io.File;
import java.net.URL;

import org.apache.log4j.Logger;
import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;


public class ProjectManager {
	private static Logger LOG;

	/**
	 * Workspace Paths.
	 */
	public final static String NAME = "Demo";
	//public final static String NAME1 = "Demo";
	public final static String WS  = System.getProperty("user.home")
			+ System.getProperty("file.separator") + ".DemoLegoNXT" + System.getProperty("file.separator");
	public final static String PRJ = WS + NAME + System.getProperty("file.separator");
//	public final static String EXE = System.getenv("DEMO_INSTALL_PATH") + System.getProperty("file.separator");
	public final static String EXE;
	static {
		URL launcher = ProjectManager.class.getClassLoader().getResource("resources/");
		if (launcher == null) {
			System.out.println("launcher null");
		}
		URL realPath = null;
		try {
			realPath = FileLocator.resolve(launcher);
		} catch (Exception e) {
			System.out.println("problem when locating the resources directory");
			System.out.println(e.getMessage());
			System.exit(-1);
		}
		EXE = realPath.getPath();
	}
//	public final static String EXE = IResource.
	public final static String SCRIPTS = EXE + "scripts" + System.getProperty("file.separator");

	public final static String SCRATCH_PROJECT_PATH = PRJ + NAME + ".sb";
	public final static String SCRATCH_MODEL_PATH   = PRJ + NAME + ".scratch";
	public final static String LEGO_MODEL_PATH      = PRJ + NAME + ".lego";
	public final static String JAVA_MODEL_PATH      = PRJ + NAME + ".java";
	private final static String CLASS_MODEL_PATH    = PRJ + NAME + ".class";
	private final static String NXT_MODEL_PATH      = PRJ + NAME + ".nxj";

	private final static String DEFAULT_SCRATCH_PATH = EXE + "default" + System.getProperty("file.separator") + NAME + ".sb";
	private final static String DEFAULT_LEGO_PATH    = EXE + "default" + System.getProperty("file.separator") + NAME + ".lego";

	private static final String ENCODING = "utf-8";

	/**
	 * Workspace Stores.
	 */
	public final static IFileStore LEGO_MODEL       = EFS.getLocalFileSystem().getStore(new Path(LEGO_MODEL_PATH));
	public final static IFileStore SCRATCH_MODEL    = EFS.getLocalFileSystem().getStore(new Path(SCRATCH_MODEL_PATH));
	private final static IFileStore JAVA_MODEL      = EFS.getLocalFileSystem().getStore(new Path(JAVA_MODEL_PATH));
	private final static IFileStore CLASS_MODEL     = EFS.getLocalFileSystem().getStore(new Path(CLASS_MODEL_PATH));
	private final static IFileStore NXT_MODEL       = EFS.getLocalFileSystem().getStore(new Path(NXT_MODEL_PATH));
	private final static IFileStore SCRATCH_PROJECT = EFS.getLocalFileSystem().getStore(new Path(SCRATCH_PROJECT_PATH));
	private final static IFileStore DEFAULT_SCRATCH = EFS.getLocalFileSystem().getStore(new Path(DEFAULT_SCRATCH_PATH));
	private final static IFileStore DEFAULT_LEGO    = EFS.getLocalFileSystem().getStore(new Path(DEFAULT_LEGO_PATH));
	private final static IFileStore[] FILES = { LEGO_MODEL, CLASS_MODEL, NXT_MODEL, JAVA_MODEL, SCRATCH_MODEL, SCRATCH_PROJECT };

	private static ProjectManager INSTANCE = null;
	public final IProject project;
	private IEditorPart javaEditor = null, legoEditor = null, scratchEditor = null;

	public static ProjectManager getInstance() {
		if (INSTANCE == null) INSTANCE = new ProjectManager(new NullProgressMonitor());
		return INSTANCE;
	}
	private ProjectManager(IProgressMonitor monitor) throws RuntimeException {
		LOG = LogManager.INSTANCE.getLogger(ProjectManager.class.getName());
		if (LOG.isDebugEnabled()) LOG.debug("Call constructor method");
		try {
			// get, or create project if non existing
			project = ResourcesPlugin.getWorkspace().getRoot().getProject(NAME);
			boolean newProject = false;
			if (!project.exists()) {
				project.create(monitor);
				newProject = true;
			}
			if (!project.isOpen()) {
				project.open(monitor);
				project.setHidden(false);
				// some files cannot be opened if project is hidden
			}
			// Sets custom preferences
			if(newProject) {
				project.setDefaultCharset(ENCODING,monitor);
			}
		} catch (Exception e) {
			throw new RuntimeException("Failed to initialze ProjectManager...",e);
		}
	}

	public boolean openLego(IWorkbenchWindow window) throws Exception {
		if (legoEditor != null) {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(legoEditor, false);
		}
		if (LOG.isDebugEnabled()) LOG.debug("Call \"openLego\" method");
		legoEditor = open(window,LEGO_MODEL_PATH,LEGO_MODEL);
		return legoEditor != null;
	}

	public boolean openJava(IWorkbenchWindow window) throws Exception {
		if (javaEditor != null) {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(javaEditor, false);
		}
		if (LOG.isDebugEnabled()) LOG.debug("Call \"openJava\" method");
		javaEditor = open(window,JAVA_MODEL_PATH,JAVA_MODEL);
		// TODO read only java editor
		// get sourceviewer ?
		return javaEditor != null;
	}

	public boolean openScratch(IWorkbenchWindow window) throws Exception {
		if (scratchEditor != null) {
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(scratchEditor, false);
		}
		if (LOG.isDebugEnabled()) LOG.debug("Call \"openScratch\" method");
		scratchEditor = open(window,SCRATCH_MODEL_PATH,SCRATCH_MODEL);
		return scratchEditor != null;
	}

	private IEditorPart open(IWorkbenchWindow window, String path, IFileStore file) throws Exception {
		if (!new File(path).exists()) {
			MessageDialog.openError(window.getShell(), "Problème",
					"Le fichier \"" + path + "\" n'existe pas.");
			return null;
		}
		IEditorPart editor = null;
		editor = IDE.openEditorOnFileStore(window.getActivePage(),file);
		return editor;
	}

	// Copy default files
	public void copy() throws Exception {
		if (LOG.isDebugEnabled())  {
			LOG.debug("Default scratch project: \"" + DEFAULT_SCRATCH_PATH + "\"");
			LOG.debug("Default lego model: \"" + DEFAULT_LEGO_PATH + "\"");
			LOG.debug("Scratch project: \"" + SCRATCH_PROJECT_PATH + "\"");
			LOG.debug("Lego model: \"" + LEGO_MODEL_PATH + "\"");
		}
		if (!new File (DEFAULT_SCRATCH_PATH).exists())
			throw new Exception ("ERROR scratch default project file is missing...");
		if (!new File (DEFAULT_LEGO_PATH).exists())
			throw new Exception ("ERROR lego default model file is missing...");
		DEFAULT_SCRATCH.copy(SCRATCH_PROJECT, EFS.OVERWRITE, null);
		DEFAULT_LEGO.copy(LEGO_MODEL, EFS.OVERWRITE, null);
//		if (!new File (SCRATCH_PROJECT_PATH).exists())
//			throw new Exception ("ERROR scratch project file is missing..." + SCRATCH_PROJECT_PATH);
//		if (!new File (LEGO_MODEL_PATH).exists())
//			throw new Exception ("ERROR lego model file is missing..." + LEGO_MODEL_PATH);
	}

	// Cleanup
	public void cleanup() throws Exception {
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(legoEditor, false);
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(javaEditor, false);
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(scratchEditor, false);
		legoEditor = null;
		javaEditor = null;
		scratchEditor = null;
		for (IFileStore file : FILES) {
			if (LOG.isDebugEnabled())  LOG.debug("Deleting: \"" + file.toString() + "\"");
			file.delete(EFS.NONE, null);
		}
	}
}