package demo.commons;

import org.apache.log4j.Logger;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.IVerticalRuler;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.ISaveablePart2;
import org.eclipse.xtext.builder.nature.ToggleXtextNatureAction;
import org.eclipse.xtext.ui.editor.XtextEditor;

import com.google.inject.Inject;

@SuppressWarnings("restriction")
public abstract class UnclosableEditor extends XtextEditor implements ISaveablePart2 {
	private static final Logger LOG = LogManager.INSTANCE.getLogger(UnclosableEditor.class.getName());

	@Inject
	protected ToggleXtextNatureAction toggleNature;
	
	public UnclosableEditor() {
		super();
		showBusy(true);
	}

	@Override
	protected ISourceViewer createSourceViewer(Composite parent, IVerticalRuler ruler, int styles) {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"createSourceViewer\" method");
		if (!toggleNature.hasNature(ProjectManager.getInstance().project)){
			toggleNature.toggleNature(ProjectManager.getInstance().project);
		}
		ISourceViewer result = super.createSourceViewer(parent, ruler, styles);
		result.getTextWidget().setWordWrap(true);
		result.getTextWidget().setRedraw(true);
		result.getTextWidget().setVisible(true);
		return result;
	}

	@Override
	public boolean isDirty() {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"isDirty\" method");
		return true;
	}
	
	@Override
	public int promptToSaveOnClose() {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"promptToSaveOnClose\" method");
		return ISaveablePart2.CANCEL;
	}

	@Override
	public void doRestoreState(IMemento memento) {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"doRestoreState\" method");
		/* don't care */
	}
}
