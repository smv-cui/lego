package demo.commons;

import org.eclipse.swt.graphics.RGB;

public interface ColorScheme {
	static final RGB KEYWORD = new RGB(127, 0, 85);
	static final RGB NUMBER  = new RGB(125, 125, 125);
	static final RGB STRING  = new RGB(42, 0, 255);
	static final RGB COMMENT = new RGB(63, 127, 95);
}
 
