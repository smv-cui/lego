package demo.lego.generator

import demo.lego.Close
import demo.lego.Forward
import demo.lego.Instruction
import demo.lego.InstructionsSet
import demo.lego.Move
import demo.lego.Open
import demo.lego.Turn
import demo.lego.TurnCW

class Lejos {

def generate(InstructionsSet set) '''
import lejos.hardware.Button;
import lejos.hardware.motor.Motor;
import lejos.robotics.navigation.DifferentialPilot;

public class Demo {
	public static void main(String[] args) {
		System.out.println("Execution...");
		DifferentialPilot pilot = new DifferentialPilot(3.3f, 20f, Motor.C, Motor.B, true);
		pilot.setTravelSpeed(20);
 		Motor.A.setSpeed(150);
 		boolean closed = false;
 		pause(1000);

«FOR i : set.instructions»
«i.generate»

«ENDFOR»
		pilot.stop();
		System.out.println("Fin Programme");
		Button.waitForAnyPress();
	}

	private static void pause(int delay) {
		try {
			Thread.sleep(delay);
		}
		catch(Exception e) { /* don't care */ }
	}
}'''

def dispatch generate(Move move) '''
«"\t\t"»// «move.eClass.name»
«"\t\t"»pilot.travel(«IF move instanceof Forward»-«ENDIF»«move.distance»);
«"\t\t"»pause(500);'''

def dispatch generate(Turn turn) '''
«"\t\t"»// «turn.eClass.name»
«"\t\t"»pilot.rotate(«IF turn instanceof TurnCW»-«ENDIF»«turn.angle»);
«"\t\t"»pause(500);'''

def dispatch generate(Instruction instr) '''
«"\t\t"»// «instr.eClass.name»
«"\t\t"»if («IF instr instanceof Close»!«ENDIF»closed) {
«"\t\t"»	Motor.A.rotate(«IF instr instanceof Open»-«ENDIF»240);
«"\t\t"»	closed = «IF instr instanceof Open»false«ELSE»true«ENDIF»;
«"\t\t"»	pause(500);
«"\t\t"»}'''
}
