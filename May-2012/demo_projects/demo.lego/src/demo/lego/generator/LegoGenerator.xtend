package demo.lego.generator

import demo.lego.InstructionsSet

import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import org.eclipse.emf.ecore.resource.Resource

class LegoGenerator implements IGenerator, ILegoGenerator {

	//@Inject extension IQualifiedNameProvider nameProvider

	override void doGenerate(Resource resource, IFileSystemAccess fsa) {
		for(e: resource.contents.filter(typeof(InstructionsSet))) {
			 fsa.generateFile(
			 	"GeneratedInstructionSet.java",
            	//e.fullyQualifiedName.toString.replace(".", "/") + ".java",
            	new Lejos().generate(e))
    	}
	}

	override doGenerateLejos(InstructionsSet set) {
		new Lejos().generate(set)
	}

	override doGenerateSimulator(InstructionsSet set) {
		new Simulator().generate(set)
	}
}
