package demo.lego.generator;

import demo.lego.InstructionsSet;

public interface ILegoGenerator {
	public CharSequence doGenerateLejos(final InstructionsSet set);
	public CharSequence doGenerateSimulator(final InstructionsSet set);
}