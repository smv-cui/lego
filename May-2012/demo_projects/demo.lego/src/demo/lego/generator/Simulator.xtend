package demo.lego.generator

import demo.lego.Backward
import demo.lego.Instruction
import demo.lego.InstructionsSet
import demo.lego.Move
import demo.lego.Open
import demo.lego.Turn
import demo.lego.TurnCCW

class Simulator {

def generate(InstructionsSet set) '''
import demo.lego.utils.InstructionsSet;
import demo.lego.utils.Instruction;
public class GeneratedInstructionSet extends InstructionsSet {
	private static final long serialVersionUID = 4432546480076674946L;
  	public void build() {
«FOR i : set.instructions»
«i.generate»
«ENDFOR»
  	}
}'''

def dispatch generate(Move move)         '''addInstruction(new Instruction(Instruction.InstructionNames.«IF move instanceof Backward»goback«ELSE»advance«ENDIF»,«move.distance»));'''
def dispatch generate(Turn turn)         '''addInstruction(new Instruction(Instruction.InstructionNames.turn«IF turn instanceof TurnCCW»left«ELSE»right«ENDIF»,«turn.angle»));'''
def dispatch generate(Instruction instr) '''addInstruction(new Instruction(Instruction.InstructionNames.«IF instr instanceof Open»open«ELSE»close«ENDIF»claw));'''	
}
