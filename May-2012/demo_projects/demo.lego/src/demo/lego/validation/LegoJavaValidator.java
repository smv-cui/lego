package demo.lego.validation;

import demo.lego.validation.AbstractLegoJavaValidator;
import demo.lego.util.GameField;

import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.validation.CheckType;

import demo.lego.Instruction;
import demo.lego.InstructionsSet;
import demo.lego.LegoPackage;
import demo.lego.Move;
import demo.lego.Turn;

public class LegoJavaValidator extends AbstractLegoJavaValidator {

	private static final GameField FIELD = new GameField(0);

	/**
	 * CHECK: the set of instructions
	 */
	@Check(CheckType.FAST)
	public void checkInstructionsSet(InstructionsSet set) {
		String message;
		FIELD.setToDefaults();
		for ( Instruction i : set.getInstructions() ) {
			try {
				FIELD.execute(i);
			} catch (Exception e) {
				int index = 0;
				if (i instanceof Turn) {
					index = LegoPackage.TURN__ANGLE;
				} else if (i instanceof Move) {
					index = LegoPackage.MOVE__DISTANCE;
				}
				error(e.getMessage(), i, null,index);
				return;
			}
			message = null;
			if ( !FIELD.isRobotInField() ) {
				message = "Le robot sort du terrain";
			} else if ( FIELD.isRobotInWalls() ) {
				message = "Le robot touche l'obstacle";
			} else if ( FIELD.hasRobotBumpedBall() ) {
				message = "Le robot rate la balle";
			}
			if (message != null) {
				warning(message,i, null, null);
				return;
			}
		}
		// check if goal reached ...
		if ( FIELD.isBallInGoal() ) {
			info("Le but est atteint !", null);
		} else {
			warning("Le but est manqué !.",set, null, null);
		}
	}
}
