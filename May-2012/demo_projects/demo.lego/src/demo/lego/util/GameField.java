package demo.lego.util;

import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import demo.lego.Backward;
import demo.lego.Close;
import demo.lego.Forward;
import demo.lego.Instruction;
import demo.lego.Move;
import demo.lego.Open;
import demo.lego.Turn;
import demo.lego.TurnCCW;
import demo.lego.TurnCW;

/**
 * Demo's field for Lego Mindstorms NXT robot
 */
public class GameField {

	// Default values
	private final int[] X = {60,80,80,100,100,80,80,60,60,40,40,60};
	private final int[] Y = {50,50,70,70,90,90,110,110,90,90,70,70};
	private final int   N = 12;
	private final Area walls = new Area(new Polygon(X,Y,N));
	private final Area field = new Area(new Rectangle2D.Double(0,0,140,150));
	private final Point2D.Double goalPosition       = new Point2D.Double(50,100);
	private final Point2D.Double ballPositionStart  = new Point2D.Double(90,60);
	private final Point2D.Double robotPositionStart = new Point2D.Double(30,30);
	private final int robotRotationStart = 270;

	// Margins for ball and robot
	private final double clawLength   = 9.5;
	private final double robotWidth   = 19.5 + (19.5/4.0);
	private final double robotLength  = 39.5 - (39.5/4.0);
	private final double goalWidth    = 20.0;
	private final double ballDiameter = 5.0;

	// Errors
	private final double error;

	// Current values
	private double robotRotation;
	private boolean clawOpen, ballTaken;
	private Point2D.Double ballPosition, robotPosition;

	/**
	 * Constructor with errors.
	 */
	public GameField(final double error) {
		setToDefaults();
		this.error = error;
	}

	/**
	 * Set the default values
	 */
	public void setToDefaults() {
		clawOpen      = true;
		ballTaken     = false;
		ballPosition  = ballPositionStart;
		robotPosition = robotPositionStart;
		robotRotation = robotRotationStart;
	}

	/**
	 * Updates the game field according to the given instruction.
	 * @param i Instruction to be executed
	 */
	public String execute(Instruction i) throws FieldException {
		String instr = null;
		if (i instanceof Open) {
			instr = "Open\t";
			if (clawOpen)   throw new FieldException("La pince est déjà ouverte.");
			if (!ballTaken) throw new FieldException("La balle n'a pas été prise.");
			clawOpen = true;
			if (ballTaken) {
				ballTaken = false;
				ballPosition = clawCenter();
			}
		}
		else if (i instanceof Close) {
			instr = "Close\t";
			if (!clawOpen) throw new FieldException("La pince est déjà fermée.");
			if (ballTaken) throw new FieldException("La balle a déjà été prise.");
			clawOpen = false;
			//if (!hasRobotBumpedBall()) {
			ballTaken = true;
			ballPosition = null;
			//}
		}
		else if (i instanceof TurnCW || i instanceof TurnCCW) {
			int a = ((Turn)i).getAngle();
			instr = ((i instanceof TurnCW) ? "TurnCW" : "TurnCCW") + " " + a;
			if ( a < 1 || a > 360 ) throw new FieldException("L'angle n'est pas dans l'intervalle 1 à 359.");
			if (i instanceof TurnCW) robotRotation += 360 - a;
			else robotRotation += a;
			robotRotation = Math.abs(robotRotation + rnd()) % 360; // Ensure we are in range 0 .. 360
		}
		else if (i instanceof Forward || i instanceof Backward) {
			int d = ((Move)i).getDistance();
			instr = ((i instanceof Backward) ? "Backward" : "Forward") + " " + d;
			if ( d < 1 || d > 150 ) throw new FieldException("La distance n'est pas dans l'intervalle 1 à 150.");
			int c = (i instanceof Backward) ? -1 : +1 ; // direction
			robotPosition = new Point2D.Double(
					Math.round(robotPosition.getX() + rnd() + c * d * cos(robotRotation)),
					Math.round(robotPosition.getY() + rnd() - c * d * sin(robotRotation)));
		}
		else throw new FieldException("L'instruction \"" + i.getClass().getName() + "\" n'existe pas.");
		return instr;
	}

	public String printRobot() {
		return "(" + Math.round(robotPosition.getX()) +
				"," + Math.round(robotPosition.getY()) +
				"," + Math.round(robotRotation) + ")";
	}

	/**
	 * @return true if the robot has bumped the ball
	 */
	public boolean hasRobotBumpedBall() {
		if (ballTaken) return false;
		final Area a = robotArea();
		a.intersect(ballArea());
		if (!a.isEmpty()) {
			if (!clawOpen) return true;
			final Area b = robotArea();
			b.subtract(clawArea());
			b.intersect(ballArea());
			if (!b.isEmpty()) return true;
		}
		return false;
	}

	/**
	 * @return true if the robot intersects the walls
	 */
	public boolean isRobotInWalls() {
		final Area a = robotArea();
		a.intersect(walls);
		return !a.isEmpty();
	}

	/**
	 * @return true if the robot is completely contained in the game field
	 */
	public boolean isRobotInField() {
		return contains(field,robotArea());
	}

	/**
	 * @return true if the ball is completely contained in the goal
	 */
	public boolean isBallInGoal() {
		if (ballPosition == null) return false;
		else return contains(goalArea(),ballArea());
	}

	/**
	 * @return The area occupied by the walls
	 */
	public Area wallsArea() {
		return new Area(walls);
	}

	/**
	 * @return The area occupied by the field
	 */
	public Area fieldArea() {
		return new Area(field);
	}

	/**
	 * @return The area occupied by the ball
	 */
	public Area ballArea() {
		if (ballPosition == null) return null;
		return new Area(new Ellipse2D.Double(
				ballPosition.x-ballDiameter/2,
				ballPosition.y-ballDiameter/2,
				ballDiameter, ballDiameter));
	}

	/**
	 * @return The area occupied by the goal
	 */
	public Area goalArea() {
		return new Area(new Rectangle2D.Double(
				goalPosition.x-goalWidth/2,
				goalPosition.y-goalWidth/2,
				goalWidth, goalWidth));
	}

	/**
	 * @return The area occupied by the robot
	 */
	public Area robotArea() {
		Point2D.Double c = robotPosition;
		Rectangle2D.Double r = new Rectangle2D.Double(c.x-robotLength/2,c.y-robotWidth/2,robotLength,robotWidth);
		return rotation(new Area(r),robotRotation,c);
	}

	/**
	 * @return The area occupied by the claw
	 */
	public Area clawArea() {
		Point2D.Double c = clawCenter();
		Rectangle2D.Double r = new Rectangle2D.Double(c.x-clawLength/2,c.y-robotWidth/2,clawLength,robotWidth);
		return rotation(new Area(r),robotRotation,c);
	}

	/**
	 * @return The center of the the claw
	 */
	private Point2D.Double clawCenter() {
		final double d = (robotLength/2) - (clawLength/2);
		return new Point2D.Double(
				robotPosition.x + d * cos(robotRotation),
				robotPosition.y - d * sin(robotRotation));
	}

	/**
	 * Checks if an Area is completely contained in another area
	 * @param container The area that should contain the other area
	 * @param contained The Area that should be totally contained in the other area
	 * @return True if the Area is completely in the field, false otherwise
	 */
	private static boolean contains(final Area container, final Area area){
		final Area clone = ((Area)area.clone());
		clone.intersect(container);
		if (clone.equals(area)){
			return true; // the area is completely in the container
		}
		return false; // it is not
	}

	private static Area rotation (final Area area, final double angle, final Point2D.Double p) {
		return area.createTransformedArea(
				AffineTransform.getRotateInstance(Math.toRadians(angle),p.x,p.y));
	}

	// Conversion shortcuts
	private double sin (final double deg) { return Math.sin (Math.toRadians (deg)); }
	private double cos (final double deg) { return Math.cos (Math.toRadians (deg)); }
	private double rnd () { return ( 2 * error * Math.random() ) - error; }
}
