package demo.lego.util;

public class FieldException extends Exception {
	private static final long serialVersionUID = 1L;
    public FieldException(String msg) { super(msg); }
}
