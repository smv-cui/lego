package demo.lego;

import org.eclipse.emf.ecore.EPackage;

import com.google.inject.Injector;

import demo.lego.LegoPackage;
import demo.lego.LegoStandaloneSetupGenerated;

/**
 * Initialization support for running Xtext languages 
 * without equinox extension registry
 */
public class LegoStandaloneSetup extends LegoStandaloneSetupGenerated{

	@Override
	public Injector createInjectorAndDoEMFRegistration() {
		EPackage.Registry.INSTANCE.put(LegoPackage.eNS_URI, LegoPackage.eINSTANCE);
		return super.createInjectorAndDoEMFRegistration();
	}
	
	public static void doSetup() {
		new LegoStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}

