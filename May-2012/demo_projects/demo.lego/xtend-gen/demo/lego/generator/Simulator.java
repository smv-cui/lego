package demo.lego.generator;

import demo.lego.Backward;
import demo.lego.Instruction;
import demo.lego.InstructionsSet;
import demo.lego.Move;
import demo.lego.Open;
import demo.lego.Turn;
import demo.lego.TurnCCW;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class Simulator {
  public CharSequence generate(final InstructionsSet set) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("import demo.lego.utils.InstructionsSet;");
    _builder.newLine();
    _builder.append("import demo.lego.utils.Instruction;");
    _builder.newLine();
    _builder.append("public class GeneratedInstructionSet extends InstructionsSet {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private static final long serialVersionUID = 4432546480076674946L;");
    _builder.newLine();
    _builder.append("  \t");
    _builder.append("public void build() {");
    _builder.newLine();
    {
      EList<Instruction> _instructions = set.getInstructions();
      for(final Instruction i : _instructions) {
        CharSequence _generate = this.generate(i);
        _builder.append(_generate, "");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("  \t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    return _builder;
  }
  
  protected CharSequence _generate(final Move move) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("addInstruction(new Instruction(Instruction.InstructionNames.");
    {
      if ((move instanceof Backward)) {
        _builder.append("goback");
      } else {
        _builder.append("advance");
      }
    }
    _builder.append(",");
    int _distance = move.getDistance();
    _builder.append(_distance, "");
    _builder.append("));");
    return _builder;
  }
  
  protected CharSequence _generate(final Turn turn) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("addInstruction(new Instruction(Instruction.InstructionNames.turn");
    {
      if ((turn instanceof TurnCCW)) {
        _builder.append("left");
      } else {
        _builder.append("right");
      }
    }
    _builder.append(",");
    int _angle = turn.getAngle();
    _builder.append(_angle, "");
    _builder.append("));");
    return _builder;
  }
  
  protected CharSequence _generate(final Instruction instr) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("addInstruction(new Instruction(Instruction.InstructionNames.");
    {
      if ((instr instanceof Open)) {
        _builder.append("open");
      } else {
        _builder.append("close");
      }
    }
    _builder.append("claw));");
    return _builder;
  }
  
  public CharSequence generate(final Instruction move) {
    if (move instanceof Move) {
      return _generate((Move)move);
    } else if (move instanceof Turn) {
      return _generate((Turn)move);
    } else if (move != null) {
      return _generate(move);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(move).toString());
    }
  }
}
