package demo.lego.generator;

import demo.lego.Close;
import demo.lego.Forward;
import demo.lego.Instruction;
import demo.lego.InstructionsSet;
import demo.lego.Move;
import demo.lego.Open;
import demo.lego.Turn;
import demo.lego.TurnCW;
import java.util.Arrays;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.xtend2.lib.StringConcatenation;

@SuppressWarnings("all")
public class Lejos {
  public CharSequence generate(final InstructionsSet set) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("import lejos.hardware.Button;");
    _builder.newLine();
    _builder.append("import lejos.hardware.motor.Motor;");
    _builder.newLine();
    _builder.append("import lejos.robotics.navigation.DifferentialPilot;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("public class Demo {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public static void main(String[] args) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("System.out.println(\"Execution...\");");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("DifferentialPilot pilot = new DifferentialPilot(3.3f, 20f, Motor.C, Motor.B, true);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("pilot.setTravelSpeed(20);");
    _builder.newLine();
    _builder.append(" \t\t");
    _builder.append("Motor.A.setSpeed(150);");
    _builder.newLine();
    _builder.append(" \t\t");
    _builder.append("boolean closed = false;");
    _builder.newLine();
    _builder.append(" \t\t");
    _builder.append("pause(1000);");
    _builder.newLine();
    _builder.newLine();
    {
      EList<Instruction> _instructions = set.getInstructions();
      for(final Instruction i : _instructions) {
        CharSequence _generate = this.generate(i);
        _builder.append(_generate, "");
        _builder.newLineIfNotEmpty();
        _builder.newLine();
      }
    }
    _builder.append("\t\t");
    _builder.append("pilot.stop();");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("System.out.println(\"Fin Programme\");");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("Button.waitForAnyPress();");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private static void pause(int delay) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("try {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("Thread.sleep(delay);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("catch(Exception e) { /* don\'t care */ }");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    return _builder;
  }
  
  protected CharSequence _generate(final Move move) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t\t", "");
    _builder.append("// ");
    EClass _eClass = move.eClass();
    String _name = _eClass.getName();
    _builder.append(_name, "");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t", "");
    _builder.append("pilot.travel(");
    {
      if ((move instanceof Forward)) {
        _builder.append("-");
      }
    }
    int _distance = move.getDistance();
    _builder.append(_distance, "");
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t", "");
    _builder.append("pause(500);");
    return _builder;
  }
  
  protected CharSequence _generate(final Turn turn) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t\t", "");
    _builder.append("// ");
    EClass _eClass = turn.eClass();
    String _name = _eClass.getName();
    _builder.append(_name, "");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t", "");
    _builder.append("pilot.rotate(");
    {
      if ((turn instanceof TurnCW)) {
        _builder.append("-");
      }
    }
    int _angle = turn.getAngle();
    _builder.append(_angle, "");
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t", "");
    _builder.append("pause(500);");
    return _builder;
  }
  
  protected CharSequence _generate(final Instruction instr) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t\t", "");
    _builder.append("// ");
    EClass _eClass = instr.eClass();
    String _name = _eClass.getName();
    _builder.append(_name, "");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t", "");
    _builder.append("if (");
    {
      if ((instr instanceof Close)) {
        _builder.append("!");
      }
    }
    _builder.append("closed) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t", "");
    _builder.append("\tMotor.A.rotate(");
    {
      if ((instr instanceof Open)) {
        _builder.append("-");
      }
    }
    _builder.append("240);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t", "");
    _builder.append("\tclosed = ");
    {
      if ((instr instanceof Open)) {
        _builder.append("false");
      } else {
        _builder.append("true");
      }
    }
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t", "");
    _builder.append("\tpause(500);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t", "");
    _builder.append("}");
    return _builder;
  }
  
  public CharSequence generate(final Instruction move) {
    if (move instanceof Move) {
      return _generate((Move)move);
    } else if (move instanceof Turn) {
      return _generate((Turn)move);
    } else if (move != null) {
      return _generate(move);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(move).toString());
    }
  }
}
