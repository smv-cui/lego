package demo.lego.generator;

import demo.lego.InstructionsSet;
import demo.lego.generator.ILegoGenerator;
import demo.lego.generator.Lejos;
import demo.lego.generator.Simulator;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.generator.IFileSystemAccess;
import org.eclipse.xtext.generator.IGenerator;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class LegoGenerator implements IGenerator, ILegoGenerator {
  public void doGenerate(final Resource resource, final IFileSystemAccess fsa) {
    EList<EObject> _contents = resource.getContents();
    Iterable<InstructionsSet> _filter = IterableExtensions.<InstructionsSet>filter(_contents, demo.lego.InstructionsSet.class);
    for (final InstructionsSet e : _filter) {
      Lejos _lejos = new Lejos();
      CharSequence _generate = _lejos.generate(e);
      fsa.generateFile("GeneratedInstructionSet.java", _generate);
    }
  }
  
  public CharSequence doGenerateLejos(final InstructionsSet set) {
    Lejos _lejos = new Lejos();
    CharSequence _generate = _lejos.generate(set);
    return _generate;
  }
  
  public CharSequence doGenerateSimulator(final InstructionsSet set) {
    Simulator _simulator = new Simulator();
    CharSequence _generate = _simulator.generate(set);
    return _generate;
  }
}
