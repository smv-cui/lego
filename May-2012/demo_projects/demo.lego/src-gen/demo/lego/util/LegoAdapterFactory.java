/**
 * <copyright>
 * </copyright>
 *
 */
package demo.lego.util;

import demo.lego.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see demo.lego.LegoPackage
 * @generated
 */
public class LegoAdapterFactory extends AdapterFactoryImpl
{
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static LegoPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LegoAdapterFactory()
	{
		if (modelPackage == null)
		{
			modelPackage = LegoPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object)
	{
		if (object == modelPackage)
		{
			return true;
		}
		if (object instanceof EObject)
		{
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LegoSwitch<Adapter> modelSwitch =
		new LegoSwitch<Adapter>()
		{
			@Override
			public Adapter caseInstructionsSet(InstructionsSet object)
			{
				return createInstructionsSetAdapter();
			}
			@Override
			public Adapter caseInstruction(Instruction object)
			{
				return createInstructionAdapter();
			}
			@Override
			public Adapter caseOpen(Open object)
			{
				return createOpenAdapter();
			}
			@Override
			public Adapter caseClose(Close object)
			{
				return createCloseAdapter();
			}
			@Override
			public Adapter caseMove(Move object)
			{
				return createMoveAdapter();
			}
			@Override
			public Adapter caseForward(Forward object)
			{
				return createForwardAdapter();
			}
			@Override
			public Adapter caseBackward(Backward object)
			{
				return createBackwardAdapter();
			}
			@Override
			public Adapter caseTurn(Turn object)
			{
				return createTurnAdapter();
			}
			@Override
			public Adapter caseTurnCW(TurnCW object)
			{
				return createTurnCWAdapter();
			}
			@Override
			public Adapter caseTurnCCW(TurnCCW object)
			{
				return createTurnCCWAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object)
			{
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target)
	{
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link demo.lego.InstructionsSet <em>Instructions Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.lego.InstructionsSet
	 * @generated
	 */
	public Adapter createInstructionsSetAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.lego.Instruction <em>Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.lego.Instruction
	 * @generated
	 */
	public Adapter createInstructionAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.lego.Open <em>Open</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.lego.Open
	 * @generated
	 */
	public Adapter createOpenAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.lego.Close <em>Close</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.lego.Close
	 * @generated
	 */
	public Adapter createCloseAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.lego.Move <em>Move</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.lego.Move
	 * @generated
	 */
	public Adapter createMoveAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.lego.Forward <em>Forward</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.lego.Forward
	 * @generated
	 */
	public Adapter createForwardAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.lego.Backward <em>Backward</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.lego.Backward
	 * @generated
	 */
	public Adapter createBackwardAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.lego.Turn <em>Turn</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.lego.Turn
	 * @generated
	 */
	public Adapter createTurnAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.lego.TurnCW <em>Turn CW</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.lego.TurnCW
	 * @generated
	 */
	public Adapter createTurnCWAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link demo.lego.TurnCCW <em>Turn CCW</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see demo.lego.TurnCCW
	 * @generated
	 */
	public Adapter createTurnCCWAdapter()
	{
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter()
	{
		return null;
	}

} //LegoAdapterFactory
