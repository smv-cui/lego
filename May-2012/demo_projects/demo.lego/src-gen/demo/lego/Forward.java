/**
 * <copyright>
 * </copyright>
 *
 */
package demo.lego;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Forward</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see demo.lego.LegoPackage#getForward()
 * @model
 * @generated
 */
public interface Forward extends Move
{
} // Forward
