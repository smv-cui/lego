/**
 * <copyright>
 * </copyright>
 *
 */
package demo.lego.impl;

import demo.lego.LegoPackage;
import demo.lego.TurnCCW;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Turn CCW</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class TurnCCWImpl extends TurnImplCustom implements TurnCCW
{
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TurnCCWImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return LegoPackage.Literals.TURN_CCW;
	}

} //TurnCCWImpl
