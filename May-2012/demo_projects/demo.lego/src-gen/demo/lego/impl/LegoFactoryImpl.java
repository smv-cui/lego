/**
 * <copyright>
 * </copyright>
 *
 */
package demo.lego.impl;

import demo.lego.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class LegoFactoryImpl extends EFactoryImpl implements LegoFactory
{
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LegoFactory init()
	{
		try
		{
			LegoFactory theLegoFactory = (LegoFactory)EPackage.Registry.INSTANCE.getEFactory("http://smv.unige.ch/Lego/2012"); 
			if (theLegoFactory != null)
			{
				return theLegoFactory;
			}
		}
		catch (Exception exception)
		{
			EcorePlugin.INSTANCE.log(exception);
		}
		return new LegoFactoryImplCustom();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LegoFactoryImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass)
	{
		switch (eClass.getClassifierID())
		{
			case LegoPackage.INSTRUCTIONS_SET: return createInstructionsSet();
			case LegoPackage.OPEN: return createOpen();
			case LegoPackage.CLOSE: return createClose();
			case LegoPackage.MOVE: return createMove();
			case LegoPackage.FORWARD: return createForward();
			case LegoPackage.BACKWARD: return createBackward();
			case LegoPackage.TURN: return createTurn();
			case LegoPackage.TURN_CW: return createTurnCW();
			case LegoPackage.TURN_CCW: return createTurnCCW();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InstructionsSet createInstructionsSet()
	{
		InstructionsSetImplCustom instructionsSet = new InstructionsSetImplCustom();
		return instructionsSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Open createOpen()
	{
		OpenImplCustom open = new OpenImplCustom();
		return open;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Close createClose()
	{
		CloseImplCustom close = new CloseImplCustom();
		return close;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Move createMove()
	{
		MoveImplCustom move = new MoveImplCustom();
		return move;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Forward createForward()
	{
		ForwardImplCustom forward = new ForwardImplCustom();
		return forward;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Backward createBackward()
	{
		BackwardImplCustom backward = new BackwardImplCustom();
		return backward;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Turn createTurn()
	{
		TurnImplCustom turn = new TurnImplCustom();
		return turn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TurnCW createTurnCW()
	{
		TurnCWImplCustom turnCW = new TurnCWImplCustom();
		return turnCW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TurnCCW createTurnCCW()
	{
		TurnCCWImplCustom turnCCW = new TurnCCWImplCustom();
		return turnCCW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LegoPackage getLegoPackage()
	{
		return (LegoPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static LegoPackage getPackage()
	{
		return LegoPackage.eINSTANCE;
	}

} //LegoFactoryImpl
