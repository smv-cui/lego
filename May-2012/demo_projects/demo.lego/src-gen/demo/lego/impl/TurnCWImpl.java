/**
 * <copyright>
 * </copyright>
 *
 */
package demo.lego.impl;

import demo.lego.LegoPackage;
import demo.lego.TurnCW;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Turn CW</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class TurnCWImpl extends TurnImplCustom implements TurnCW
{
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TurnCWImpl()
	{
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass()
	{
		return LegoPackage.Literals.TURN_CW;
	}

} //TurnCWImpl
