package demo.lego.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import demo.lego.services.LegoGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalLegoParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ONE", "RULE_POS_1", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'DEBUT'", "'FIN'", "'ferme la pince'", "'ouvre la pince'", "'recule de'", "'cm'", "'avance de'", "'de'", "'degr\\u00E9'", "'degr\\u00E9s'", "'tourne \\u00E0 gauche'", "'tourne \\u00E0 droite'"
    };
    public static final int RULE_ID=6;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_POS_1=5;
    public static final int RULE_ONE=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__19=19;
    public static final int RULE_STRING=8;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=7;
    public static final int RULE_WS=11;

    // delegates
    // delegators


        public InternalLegoParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalLegoParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalLegoParser.tokenNames; }
    public String getGrammarFileName() { return "../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g"; }



     	private LegoGrammarAccess grammarAccess;
     	
        public InternalLegoParser(TokenStream input, LegoGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "InstructionsSet";	
       	}
       	
       	@Override
       	protected LegoGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleInstructionsSet"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:67:1: entryRuleInstructionsSet returns [EObject current=null] : iv_ruleInstructionsSet= ruleInstructionsSet EOF ;
    public final EObject entryRuleInstructionsSet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstructionsSet = null;


        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:68:2: (iv_ruleInstructionsSet= ruleInstructionsSet EOF )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:69:2: iv_ruleInstructionsSet= ruleInstructionsSet EOF
            {
             newCompositeNode(grammarAccess.getInstructionsSetRule()); 
            pushFollow(FOLLOW_ruleInstructionsSet_in_entryRuleInstructionsSet75);
            iv_ruleInstructionsSet=ruleInstructionsSet();

            state._fsp--;

             current =iv_ruleInstructionsSet; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstructionsSet85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInstructionsSet"


    // $ANTLR start "ruleInstructionsSet"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:76:1: ruleInstructionsSet returns [EObject current=null] : ( () otherlv_1= 'DEBUT' ( (lv_instructions_2_0= ruleInstruction ) )* otherlv_3= 'FIN' ) ;
    public final EObject ruleInstructionsSet() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_instructions_2_0 = null;


         enterRule(); 
            
        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:79:28: ( ( () otherlv_1= 'DEBUT' ( (lv_instructions_2_0= ruleInstruction ) )* otherlv_3= 'FIN' ) )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:80:1: ( () otherlv_1= 'DEBUT' ( (lv_instructions_2_0= ruleInstruction ) )* otherlv_3= 'FIN' )
            {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:80:1: ( () otherlv_1= 'DEBUT' ( (lv_instructions_2_0= ruleInstruction ) )* otherlv_3= 'FIN' )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:80:2: () otherlv_1= 'DEBUT' ( (lv_instructions_2_0= ruleInstruction ) )* otherlv_3= 'FIN'
            {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:80:2: ()
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:81:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getInstructionsSetAccess().getInstructionsSetAction_0(),
                        current);
                

            }

            otherlv_1=(Token)match(input,13,FOLLOW_13_in_ruleInstructionsSet131); 

                	newLeafNode(otherlv_1, grammarAccess.getInstructionsSetAccess().getDEBUTKeyword_1());
                
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:90:1: ( (lv_instructions_2_0= ruleInstruction ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=15 && LA1_0<=17)||LA1_0==19||(LA1_0>=23 && LA1_0<=24)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:91:1: (lv_instructions_2_0= ruleInstruction )
            	    {
            	    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:91:1: (lv_instructions_2_0= ruleInstruction )
            	    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:92:3: lv_instructions_2_0= ruleInstruction
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getInstructionsSetAccess().getInstructionsInstructionParserRuleCall_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleInstruction_in_ruleInstructionsSet152);
            	    lv_instructions_2_0=ruleInstruction();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getInstructionsSetRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"instructions",
            	            		lv_instructions_2_0, 
            	            		"Instruction");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_3=(Token)match(input,14,FOLLOW_14_in_ruleInstructionsSet165); 

                	newLeafNode(otherlv_3, grammarAccess.getInstructionsSetAccess().getFINKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInstructionsSet"


    // $ANTLR start "entryRuleInstruction"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:120:1: entryRuleInstruction returns [EObject current=null] : iv_ruleInstruction= ruleInstruction EOF ;
    public final EObject entryRuleInstruction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstruction = null;


        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:121:2: (iv_ruleInstruction= ruleInstruction EOF )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:122:2: iv_ruleInstruction= ruleInstruction EOF
            {
             newCompositeNode(grammarAccess.getInstructionRule()); 
            pushFollow(FOLLOW_ruleInstruction_in_entryRuleInstruction201);
            iv_ruleInstruction=ruleInstruction();

            state._fsp--;

             current =iv_ruleInstruction; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstruction211); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInstruction"


    // $ANTLR start "ruleInstruction"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:129:1: ruleInstruction returns [EObject current=null] : (this_Backward_0= ruleBackward | this_Forward_1= ruleForward | this_Turn_2= ruleTurn | this_Open_3= ruleOpen | this_Close_4= ruleClose ) ;
    public final EObject ruleInstruction() throws RecognitionException {
        EObject current = null;

        EObject this_Backward_0 = null;

        EObject this_Forward_1 = null;

        EObject this_Turn_2 = null;

        EObject this_Open_3 = null;

        EObject this_Close_4 = null;


         enterRule(); 
            
        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:132:28: ( (this_Backward_0= ruleBackward | this_Forward_1= ruleForward | this_Turn_2= ruleTurn | this_Open_3= ruleOpen | this_Close_4= ruleClose ) )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:133:1: (this_Backward_0= ruleBackward | this_Forward_1= ruleForward | this_Turn_2= ruleTurn | this_Open_3= ruleOpen | this_Close_4= ruleClose )
            {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:133:1: (this_Backward_0= ruleBackward | this_Forward_1= ruleForward | this_Turn_2= ruleTurn | this_Open_3= ruleOpen | this_Close_4= ruleClose )
            int alt2=5;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt2=1;
                }
                break;
            case 19:
                {
                alt2=2;
                }
                break;
            case 23:
            case 24:
                {
                alt2=3;
                }
                break;
            case 16:
                {
                alt2=4;
                }
                break;
            case 15:
                {
                alt2=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:134:5: this_Backward_0= ruleBackward
                    {
                     
                            newCompositeNode(grammarAccess.getInstructionAccess().getBackwardParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleBackward_in_ruleInstruction258);
                    this_Backward_0=ruleBackward();

                    state._fsp--;

                     
                            current = this_Backward_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:144:5: this_Forward_1= ruleForward
                    {
                     
                            newCompositeNode(grammarAccess.getInstructionAccess().getForwardParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleForward_in_ruleInstruction285);
                    this_Forward_1=ruleForward();

                    state._fsp--;

                     
                            current = this_Forward_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:154:5: this_Turn_2= ruleTurn
                    {
                     
                            newCompositeNode(grammarAccess.getInstructionAccess().getTurnParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleTurn_in_ruleInstruction312);
                    this_Turn_2=ruleTurn();

                    state._fsp--;

                     
                            current = this_Turn_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:164:5: this_Open_3= ruleOpen
                    {
                     
                            newCompositeNode(grammarAccess.getInstructionAccess().getOpenParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleOpen_in_ruleInstruction339);
                    this_Open_3=ruleOpen();

                    state._fsp--;

                     
                            current = this_Open_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:174:5: this_Close_4= ruleClose
                    {
                     
                            newCompositeNode(grammarAccess.getInstructionAccess().getCloseParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_ruleClose_in_ruleInstruction366);
                    this_Close_4=ruleClose();

                    state._fsp--;

                     
                            current = this_Close_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInstruction"


    // $ANTLR start "entryRuleClose"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:190:1: entryRuleClose returns [EObject current=null] : iv_ruleClose= ruleClose EOF ;
    public final EObject entryRuleClose() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClose = null;


        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:191:2: (iv_ruleClose= ruleClose EOF )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:192:2: iv_ruleClose= ruleClose EOF
            {
             newCompositeNode(grammarAccess.getCloseRule()); 
            pushFollow(FOLLOW_ruleClose_in_entryRuleClose401);
            iv_ruleClose=ruleClose();

            state._fsp--;

             current =iv_ruleClose; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleClose411); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClose"


    // $ANTLR start "ruleClose"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:199:1: ruleClose returns [EObject current=null] : (otherlv_0= 'ferme la pince' () ) ;
    public final EObject ruleClose() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:202:28: ( (otherlv_0= 'ferme la pince' () ) )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:203:1: (otherlv_0= 'ferme la pince' () )
            {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:203:1: (otherlv_0= 'ferme la pince' () )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:203:3: otherlv_0= 'ferme la pince' ()
            {
            otherlv_0=(Token)match(input,15,FOLLOW_15_in_ruleClose448); 

                	newLeafNode(otherlv_0, grammarAccess.getCloseAccess().getFermeLaPinceKeyword_0());
                
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:207:1: ()
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:208:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getCloseAccess().getCloseAction_1(),
                        current);
                

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClose"


    // $ANTLR start "entryRuleOpen"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:221:1: entryRuleOpen returns [EObject current=null] : iv_ruleOpen= ruleOpen EOF ;
    public final EObject entryRuleOpen() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOpen = null;


        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:222:2: (iv_ruleOpen= ruleOpen EOF )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:223:2: iv_ruleOpen= ruleOpen EOF
            {
             newCompositeNode(grammarAccess.getOpenRule()); 
            pushFollow(FOLLOW_ruleOpen_in_entryRuleOpen493);
            iv_ruleOpen=ruleOpen();

            state._fsp--;

             current =iv_ruleOpen; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOpen503); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOpen"


    // $ANTLR start "ruleOpen"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:230:1: ruleOpen returns [EObject current=null] : (otherlv_0= 'ouvre la pince' () ) ;
    public final EObject ruleOpen() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:233:28: ( (otherlv_0= 'ouvre la pince' () ) )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:234:1: (otherlv_0= 'ouvre la pince' () )
            {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:234:1: (otherlv_0= 'ouvre la pince' () )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:234:3: otherlv_0= 'ouvre la pince' ()
            {
            otherlv_0=(Token)match(input,16,FOLLOW_16_in_ruleOpen540); 

                	newLeafNode(otherlv_0, grammarAccess.getOpenAccess().getOuvreLaPinceKeyword_0());
                
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:238:1: ()
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:239:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getOpenAccess().getOpenAction_1(),
                        current);
                

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOpen"


    // $ANTLR start "entryRuleBackward"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:252:1: entryRuleBackward returns [EObject current=null] : iv_ruleBackward= ruleBackward EOF ;
    public final EObject entryRuleBackward() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBackward = null;


        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:253:2: (iv_ruleBackward= ruleBackward EOF )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:254:2: iv_ruleBackward= ruleBackward EOF
            {
             newCompositeNode(grammarAccess.getBackwardRule()); 
            pushFollow(FOLLOW_ruleBackward_in_entryRuleBackward585);
            iv_ruleBackward=ruleBackward();

            state._fsp--;

             current =iv_ruleBackward; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleBackward595); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBackward"


    // $ANTLR start "ruleBackward"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:261:1: ruleBackward returns [EObject current=null] : (otherlv_0= 'recule de' ( ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) ) ) otherlv_2= 'cm' ) ;
    public final EObject ruleBackward() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_distance_1_1=null;
        Token lv_distance_1_2=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:264:28: ( (otherlv_0= 'recule de' ( ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) ) ) otherlv_2= 'cm' ) )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:265:1: (otherlv_0= 'recule de' ( ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) ) ) otherlv_2= 'cm' )
            {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:265:1: (otherlv_0= 'recule de' ( ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) ) ) otherlv_2= 'cm' )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:265:3: otherlv_0= 'recule de' ( ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) ) ) otherlv_2= 'cm'
            {
            otherlv_0=(Token)match(input,17,FOLLOW_17_in_ruleBackward632); 

                	newLeafNode(otherlv_0, grammarAccess.getBackwardAccess().getReculeDeKeyword_0());
                
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:269:1: ( ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) ) )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:270:1: ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) )
            {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:270:1: ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:271:1: (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 )
            {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:271:1: (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ONE) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_POS_1) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:272:3: lv_distance_1_1= RULE_ONE
                    {
                    lv_distance_1_1=(Token)match(input,RULE_ONE,FOLLOW_RULE_ONE_in_ruleBackward651); 

                    			newLeafNode(lv_distance_1_1, grammarAccess.getBackwardAccess().getDistanceONETerminalRuleCall_1_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBackwardRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"distance",
                            		lv_distance_1_1, 
                            		"ONE");
                    	    

                    }
                    break;
                case 2 :
                    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:287:8: lv_distance_1_2= RULE_POS_1
                    {
                    lv_distance_1_2=(Token)match(input,RULE_POS_1,FOLLOW_RULE_POS_1_in_ruleBackward671); 

                    			newLeafNode(lv_distance_1_2, grammarAccess.getBackwardAccess().getDistancePOS_1TerminalRuleCall_1_0_1()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getBackwardRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"distance",
                            		lv_distance_1_2, 
                            		"POS_1");
                    	    

                    }
                    break;

            }


            }


            }

            otherlv_2=(Token)match(input,18,FOLLOW_18_in_ruleBackward691); 

                	newLeafNode(otherlv_2, grammarAccess.getBackwardAccess().getCmKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBackward"


    // $ANTLR start "entryRuleForward"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:317:1: entryRuleForward returns [EObject current=null] : iv_ruleForward= ruleForward EOF ;
    public final EObject entryRuleForward() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleForward = null;


        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:318:2: (iv_ruleForward= ruleForward EOF )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:319:2: iv_ruleForward= ruleForward EOF
            {
             newCompositeNode(grammarAccess.getForwardRule()); 
            pushFollow(FOLLOW_ruleForward_in_entryRuleForward727);
            iv_ruleForward=ruleForward();

            state._fsp--;

             current =iv_ruleForward; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleForward737); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleForward"


    // $ANTLR start "ruleForward"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:326:1: ruleForward returns [EObject current=null] : (otherlv_0= 'avance de' ( ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) ) ) otherlv_2= 'cm' ) ;
    public final EObject ruleForward() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_distance_1_1=null;
        Token lv_distance_1_2=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:329:28: ( (otherlv_0= 'avance de' ( ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) ) ) otherlv_2= 'cm' ) )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:330:1: (otherlv_0= 'avance de' ( ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) ) ) otherlv_2= 'cm' )
            {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:330:1: (otherlv_0= 'avance de' ( ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) ) ) otherlv_2= 'cm' )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:330:3: otherlv_0= 'avance de' ( ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) ) ) otherlv_2= 'cm'
            {
            otherlv_0=(Token)match(input,19,FOLLOW_19_in_ruleForward774); 

                	newLeafNode(otherlv_0, grammarAccess.getForwardAccess().getAvanceDeKeyword_0());
                
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:334:1: ( ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) ) )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:335:1: ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) )
            {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:335:1: ( (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 ) )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:336:1: (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 )
            {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:336:1: (lv_distance_1_1= RULE_ONE | lv_distance_1_2= RULE_POS_1 )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_ONE) ) {
                alt4=1;
            }
            else if ( (LA4_0==RULE_POS_1) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:337:3: lv_distance_1_1= RULE_ONE
                    {
                    lv_distance_1_1=(Token)match(input,RULE_ONE,FOLLOW_RULE_ONE_in_ruleForward793); 

                    			newLeafNode(lv_distance_1_1, grammarAccess.getForwardAccess().getDistanceONETerminalRuleCall_1_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getForwardRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"distance",
                            		lv_distance_1_1, 
                            		"ONE");
                    	    

                    }
                    break;
                case 2 :
                    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:352:8: lv_distance_1_2= RULE_POS_1
                    {
                    lv_distance_1_2=(Token)match(input,RULE_POS_1,FOLLOW_RULE_POS_1_in_ruleForward813); 

                    			newLeafNode(lv_distance_1_2, grammarAccess.getForwardAccess().getDistancePOS_1TerminalRuleCall_1_0_1()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getForwardRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"distance",
                            		lv_distance_1_2, 
                            		"POS_1");
                    	    

                    }
                    break;

            }


            }


            }

            otherlv_2=(Token)match(input,18,FOLLOW_18_in_ruleForward833); 

                	newLeafNode(otherlv_2, grammarAccess.getForwardAccess().getCmKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleForward"


    // $ANTLR start "entryRuleTurn"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:382:1: entryRuleTurn returns [EObject current=null] : iv_ruleTurn= ruleTurn EOF ;
    public final EObject entryRuleTurn() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTurn = null;


        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:383:2: (iv_ruleTurn= ruleTurn EOF )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:384:2: iv_ruleTurn= ruleTurn EOF
            {
             newCompositeNode(grammarAccess.getTurnRule()); 
            pushFollow(FOLLOW_ruleTurn_in_entryRuleTurn869);
            iv_ruleTurn=ruleTurn();

            state._fsp--;

             current =iv_ruleTurn; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTurn879); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTurn"


    // $ANTLR start "ruleTurn"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:391:1: ruleTurn returns [EObject current=null] : ( (this_TurnCW_0= ruleTurnCW | this_TurnCCW_1= ruleTurnCCW ) (otherlv_2= 'de' ( ( ( (lv_angle_3_0= RULE_ONE ) ) otherlv_4= 'degr\\u00E9' ) | ( ( (lv_angle_5_0= RULE_POS_1 ) ) otherlv_6= 'degr\\u00E9s' ) ) )? ) ;
    public final EObject ruleTurn() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token lv_angle_3_0=null;
        Token otherlv_4=null;
        Token lv_angle_5_0=null;
        Token otherlv_6=null;
        EObject this_TurnCW_0 = null;

        EObject this_TurnCCW_1 = null;


         enterRule(); 
            
        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:394:28: ( ( (this_TurnCW_0= ruleTurnCW | this_TurnCCW_1= ruleTurnCCW ) (otherlv_2= 'de' ( ( ( (lv_angle_3_0= RULE_ONE ) ) otherlv_4= 'degr\\u00E9' ) | ( ( (lv_angle_5_0= RULE_POS_1 ) ) otherlv_6= 'degr\\u00E9s' ) ) )? ) )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:395:1: ( (this_TurnCW_0= ruleTurnCW | this_TurnCCW_1= ruleTurnCCW ) (otherlv_2= 'de' ( ( ( (lv_angle_3_0= RULE_ONE ) ) otherlv_4= 'degr\\u00E9' ) | ( ( (lv_angle_5_0= RULE_POS_1 ) ) otherlv_6= 'degr\\u00E9s' ) ) )? )
            {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:395:1: ( (this_TurnCW_0= ruleTurnCW | this_TurnCCW_1= ruleTurnCCW ) (otherlv_2= 'de' ( ( ( (lv_angle_3_0= RULE_ONE ) ) otherlv_4= 'degr\\u00E9' ) | ( ( (lv_angle_5_0= RULE_POS_1 ) ) otherlv_6= 'degr\\u00E9s' ) ) )? )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:395:2: (this_TurnCW_0= ruleTurnCW | this_TurnCCW_1= ruleTurnCCW ) (otherlv_2= 'de' ( ( ( (lv_angle_3_0= RULE_ONE ) ) otherlv_4= 'degr\\u00E9' ) | ( ( (lv_angle_5_0= RULE_POS_1 ) ) otherlv_6= 'degr\\u00E9s' ) ) )?
            {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:395:2: (this_TurnCW_0= ruleTurnCW | this_TurnCCW_1= ruleTurnCCW )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==24) ) {
                alt5=1;
            }
            else if ( (LA5_0==23) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:396:5: this_TurnCW_0= ruleTurnCW
                    {
                     
                            newCompositeNode(grammarAccess.getTurnAccess().getTurnCWParserRuleCall_0_0()); 
                        
                    pushFollow(FOLLOW_ruleTurnCW_in_ruleTurn927);
                    this_TurnCW_0=ruleTurnCW();

                    state._fsp--;

                     
                            current = this_TurnCW_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:406:5: this_TurnCCW_1= ruleTurnCCW
                    {
                     
                            newCompositeNode(grammarAccess.getTurnAccess().getTurnCCWParserRuleCall_0_1()); 
                        
                    pushFollow(FOLLOW_ruleTurnCCW_in_ruleTurn954);
                    this_TurnCCW_1=ruleTurnCCW();

                    state._fsp--;

                     
                            current = this_TurnCCW_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }

            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:414:2: (otherlv_2= 'de' ( ( ( (lv_angle_3_0= RULE_ONE ) ) otherlv_4= 'degr\\u00E9' ) | ( ( (lv_angle_5_0= RULE_POS_1 ) ) otherlv_6= 'degr\\u00E9s' ) ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==20) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:414:4: otherlv_2= 'de' ( ( ( (lv_angle_3_0= RULE_ONE ) ) otherlv_4= 'degr\\u00E9' ) | ( ( (lv_angle_5_0= RULE_POS_1 ) ) otherlv_6= 'degr\\u00E9s' ) )
                    {
                    otherlv_2=(Token)match(input,20,FOLLOW_20_in_ruleTurn967); 

                        	newLeafNode(otherlv_2, grammarAccess.getTurnAccess().getDeKeyword_1_0());
                        
                    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:418:1: ( ( ( (lv_angle_3_0= RULE_ONE ) ) otherlv_4= 'degr\\u00E9' ) | ( ( (lv_angle_5_0= RULE_POS_1 ) ) otherlv_6= 'degr\\u00E9s' ) )
                    int alt6=2;
                    int LA6_0 = input.LA(1);

                    if ( (LA6_0==RULE_ONE) ) {
                        alt6=1;
                    }
                    else if ( (LA6_0==RULE_POS_1) ) {
                        alt6=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 6, 0, input);

                        throw nvae;
                    }
                    switch (alt6) {
                        case 1 :
                            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:418:2: ( ( (lv_angle_3_0= RULE_ONE ) ) otherlv_4= 'degr\\u00E9' )
                            {
                            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:418:2: ( ( (lv_angle_3_0= RULE_ONE ) ) otherlv_4= 'degr\\u00E9' )
                            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:418:3: ( (lv_angle_3_0= RULE_ONE ) ) otherlv_4= 'degr\\u00E9'
                            {
                            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:418:3: ( (lv_angle_3_0= RULE_ONE ) )
                            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:419:1: (lv_angle_3_0= RULE_ONE )
                            {
                            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:419:1: (lv_angle_3_0= RULE_ONE )
                            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:420:3: lv_angle_3_0= RULE_ONE
                            {
                            lv_angle_3_0=(Token)match(input,RULE_ONE,FOLLOW_RULE_ONE_in_ruleTurn986); 

                            			newLeafNode(lv_angle_3_0, grammarAccess.getTurnAccess().getAngleONETerminalRuleCall_1_1_0_0_0()); 
                            		

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getTurnRule());
                            	        }
                                   		setWithLastConsumed(
                                   			current, 
                                   			"angle",
                                    		lv_angle_3_0, 
                                    		"ONE");
                            	    

                            }


                            }

                            otherlv_4=(Token)match(input,21,FOLLOW_21_in_ruleTurn1003); 

                                	newLeafNode(otherlv_4, grammarAccess.getTurnAccess().getDegrKeyword_1_1_0_1());
                                

                            }


                            }
                            break;
                        case 2 :
                            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:441:6: ( ( (lv_angle_5_0= RULE_POS_1 ) ) otherlv_6= 'degr\\u00E9s' )
                            {
                            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:441:6: ( ( (lv_angle_5_0= RULE_POS_1 ) ) otherlv_6= 'degr\\u00E9s' )
                            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:441:7: ( (lv_angle_5_0= RULE_POS_1 ) ) otherlv_6= 'degr\\u00E9s'
                            {
                            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:441:7: ( (lv_angle_5_0= RULE_POS_1 ) )
                            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:442:1: (lv_angle_5_0= RULE_POS_1 )
                            {
                            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:442:1: (lv_angle_5_0= RULE_POS_1 )
                            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:443:3: lv_angle_5_0= RULE_POS_1
                            {
                            lv_angle_5_0=(Token)match(input,RULE_POS_1,FOLLOW_RULE_POS_1_in_ruleTurn1028); 

                            			newLeafNode(lv_angle_5_0, grammarAccess.getTurnAccess().getAnglePOS_1TerminalRuleCall_1_1_1_0_0()); 
                            		

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getTurnRule());
                            	        }
                                   		setWithLastConsumed(
                                   			current, 
                                   			"angle",
                                    		lv_angle_5_0, 
                                    		"POS_1");
                            	    

                            }


                            }

                            otherlv_6=(Token)match(input,22,FOLLOW_22_in_ruleTurn1045); 

                                	newLeafNode(otherlv_6, grammarAccess.getTurnAccess().getDegrSKeyword_1_1_1_1());
                                

                            }


                            }
                            break;

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTurn"


    // $ANTLR start "entryRuleTurnCCW"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:471:1: entryRuleTurnCCW returns [EObject current=null] : iv_ruleTurnCCW= ruleTurnCCW EOF ;
    public final EObject entryRuleTurnCCW() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTurnCCW = null;


        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:472:2: (iv_ruleTurnCCW= ruleTurnCCW EOF )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:473:2: iv_ruleTurnCCW= ruleTurnCCW EOF
            {
             newCompositeNode(grammarAccess.getTurnCCWRule()); 
            pushFollow(FOLLOW_ruleTurnCCW_in_entryRuleTurnCCW1085);
            iv_ruleTurnCCW=ruleTurnCCW();

            state._fsp--;

             current =iv_ruleTurnCCW; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTurnCCW1095); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTurnCCW"


    // $ANTLR start "ruleTurnCCW"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:480:1: ruleTurnCCW returns [EObject current=null] : (otherlv_0= 'tourne \\u00E0 gauche' () ) ;
    public final EObject ruleTurnCCW() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:483:28: ( (otherlv_0= 'tourne \\u00E0 gauche' () ) )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:484:1: (otherlv_0= 'tourne \\u00E0 gauche' () )
            {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:484:1: (otherlv_0= 'tourne \\u00E0 gauche' () )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:484:3: otherlv_0= 'tourne \\u00E0 gauche' ()
            {
            otherlv_0=(Token)match(input,23,FOLLOW_23_in_ruleTurnCCW1132); 

                	newLeafNode(otherlv_0, grammarAccess.getTurnCCWAccess().getTourneGaucheKeyword_0());
                
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:488:1: ()
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:489:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getTurnCCWAccess().getTurnCCWAction_1(),
                        current);
                

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTurnCCW"


    // $ANTLR start "entryRuleTurnCW"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:502:1: entryRuleTurnCW returns [EObject current=null] : iv_ruleTurnCW= ruleTurnCW EOF ;
    public final EObject entryRuleTurnCW() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTurnCW = null;


        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:503:2: (iv_ruleTurnCW= ruleTurnCW EOF )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:504:2: iv_ruleTurnCW= ruleTurnCW EOF
            {
             newCompositeNode(grammarAccess.getTurnCWRule()); 
            pushFollow(FOLLOW_ruleTurnCW_in_entryRuleTurnCW1177);
            iv_ruleTurnCW=ruleTurnCW();

            state._fsp--;

             current =iv_ruleTurnCW; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTurnCW1187); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTurnCW"


    // $ANTLR start "ruleTurnCW"
    // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:511:1: ruleTurnCW returns [EObject current=null] : (otherlv_0= 'tourne \\u00E0 droite' () ) ;
    public final EObject ruleTurnCW() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;

         enterRule(); 
            
        try {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:514:28: ( (otherlv_0= 'tourne \\u00E0 droite' () ) )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:515:1: (otherlv_0= 'tourne \\u00E0 droite' () )
            {
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:515:1: (otherlv_0= 'tourne \\u00E0 droite' () )
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:515:3: otherlv_0= 'tourne \\u00E0 droite' ()
            {
            otherlv_0=(Token)match(input,24,FOLLOW_24_in_ruleTurnCW1224); 

                	newLeafNode(otherlv_0, grammarAccess.getTurnCWAccess().getTourneDroiteKeyword_0());
                
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:519:1: ()
            // ../demo.lego/src-gen/demo/lego/parser/antlr/internal/InternalLego.g:520:5: 
            {

                    current = forceCreateModelElement(
                        grammarAccess.getTurnCWAccess().getTurnCWAction_1(),
                        current);
                

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTurnCW"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleInstructionsSet_in_entryRuleInstructionsSet75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstructionsSet85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_ruleInstructionsSet131 = new BitSet(new long[]{0x00000000018BC000L});
    public static final BitSet FOLLOW_ruleInstruction_in_ruleInstructionsSet152 = new BitSet(new long[]{0x00000000018BC000L});
    public static final BitSet FOLLOW_14_in_ruleInstructionsSet165 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstruction_in_entryRuleInstruction201 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstruction211 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBackward_in_ruleInstruction258 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForward_in_ruleInstruction285 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurn_in_ruleInstruction312 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpen_in_ruleInstruction339 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClose_in_ruleInstruction366 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleClose_in_entryRuleClose401 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleClose411 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleClose448 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOpen_in_entryRuleOpen493 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOpen503 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_ruleOpen540 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleBackward_in_entryRuleBackward585 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleBackward595 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_ruleBackward632 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_RULE_ONE_in_ruleBackward651 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_RULE_POS_1_in_ruleBackward671 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleBackward691 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleForward_in_entryRuleForward727 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleForward737 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleForward774 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_RULE_ONE_in_ruleForward793 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_RULE_POS_1_in_ruleForward813 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleForward833 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurn_in_entryRuleTurn869 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTurn879 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurnCW_in_ruleTurn927 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_ruleTurnCCW_in_ruleTurn954 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_20_in_ruleTurn967 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_RULE_ONE_in_ruleTurn986 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleTurn1003 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_POS_1_in_ruleTurn1028 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22_in_ruleTurn1045 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurnCCW_in_entryRuleTurnCCW1085 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTurnCCW1095 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_ruleTurnCCW1132 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTurnCW_in_entryRuleTurnCW1177 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTurnCW1187 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleTurnCW1224 = new BitSet(new long[]{0x0000000000000002L});

}