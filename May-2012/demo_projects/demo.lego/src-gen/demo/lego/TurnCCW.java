/**
 * <copyright>
 * </copyright>
 *
 */
package demo.lego;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Turn CCW</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see demo.lego.LegoPackage#getTurnCCW()
 * @model
 * @generated
 */
public interface TurnCCW extends Turn
{
} // TurnCCW
