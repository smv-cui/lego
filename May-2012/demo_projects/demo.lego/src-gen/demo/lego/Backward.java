/**
 * <copyright>
 * </copyright>
 *
 */
package demo.lego;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Backward</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see demo.lego.LegoPackage#getBackward()
 * @model
 * @generated
 */
public interface Backward extends Move
{
} // Backward
