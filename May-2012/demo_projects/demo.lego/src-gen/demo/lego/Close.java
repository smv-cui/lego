/**
 * <copyright>
 * </copyright>
 *
 */
package demo.lego;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Close</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see demo.lego.LegoPackage#getClose()
 * @model
 * @generated
 */
public interface Close extends Instruction
{
} // Close
