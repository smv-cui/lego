/**
 * <copyright>
 * </copyright>
 *
 */
package demo.lego;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Turn CW</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see demo.lego.LegoPackage#getTurnCW()
 * @model
 * @generated
 */
public interface TurnCW extends Turn
{
} // TurnCW
