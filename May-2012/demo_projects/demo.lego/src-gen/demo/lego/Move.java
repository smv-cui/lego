/**
 * <copyright>
 * </copyright>
 *
 */
package demo.lego;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Move</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link demo.lego.Move#getDistance <em>Distance</em>}</li>
 * </ul>
 * </p>
 *
 * @see demo.lego.LegoPackage#getMove()
 * @model
 * @generated
 */
public interface Move extends Instruction
{
	/**
	 * Returns the value of the '<em><b>Distance</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Distance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Distance</em>' attribute.
	 * @see #setDistance(int)
	 * @see demo.lego.LegoPackage#getMove_Distance()
	 * @model default="0" required="true" ordered="false"
	 * @generated
	 */
	int getDistance();

	/**
	 * Sets the value of the '{@link demo.lego.Move#getDistance <em>Distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Distance</em>' attribute.
	 * @see #getDistance()
	 * @generated
	 */
	void setDistance(int value);

} // Move
