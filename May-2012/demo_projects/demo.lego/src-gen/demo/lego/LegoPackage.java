/**
 * <copyright>
 * </copyright>
 *
 */
package demo.lego;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see demo.lego.LegoFactory
 * @model kind="package"
 * @generated
 */
public interface LegoPackage extends EPackage
{
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "lego";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://smv.unige.ch/Lego/2012";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Lego";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LegoPackage eINSTANCE = demo.lego.impl.LegoPackageImpl.init();

	/**
	 * The meta object id for the '{@link demo.lego.impl.InstructionsSetImpl <em>Instructions Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.lego.impl.InstructionsSetImpl
	 * @see demo.lego.impl.LegoPackageImpl#getInstructionsSet()
	 * @generated
	 */
	int INSTRUCTIONS_SET = 0;

	/**
	 * The feature id for the '<em><b>Instructions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTIONS_SET__INSTRUCTIONS = 0;

	/**
	 * The number of structural features of the '<em>Instructions Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTIONS_SET_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link demo.lego.Instruction <em>Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.lego.Instruction
	 * @see demo.lego.impl.LegoPackageImpl#getInstruction()
	 * @generated
	 */
	int INSTRUCTION = 1;

	/**
	 * The number of structural features of the '<em>Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSTRUCTION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link demo.lego.impl.OpenImpl <em>Open</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.lego.impl.OpenImpl
	 * @see demo.lego.impl.LegoPackageImpl#getOpen()
	 * @generated
	 */
	int OPEN = 2;

	/**
	 * The number of structural features of the '<em>Open</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPEN_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.lego.impl.CloseImpl <em>Close</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.lego.impl.CloseImpl
	 * @see demo.lego.impl.LegoPackageImpl#getClose()
	 * @generated
	 */
	int CLOSE = 3;

	/**
	 * The number of structural features of the '<em>Close</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOSE_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.lego.impl.MoveImpl <em>Move</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.lego.impl.MoveImpl
	 * @see demo.lego.impl.LegoPackageImpl#getMove()
	 * @generated
	 */
	int MOVE = 4;

	/**
	 * The feature id for the '<em><b>Distance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVE__DISTANCE = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Move</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVE_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.lego.impl.ForwardImpl <em>Forward</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.lego.impl.ForwardImpl
	 * @see demo.lego.impl.LegoPackageImpl#getForward()
	 * @generated
	 */
	int FORWARD = 5;

	/**
	 * The feature id for the '<em><b>Distance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORWARD__DISTANCE = MOVE__DISTANCE;

	/**
	 * The number of structural features of the '<em>Forward</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORWARD_FEATURE_COUNT = MOVE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.lego.impl.BackwardImpl <em>Backward</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.lego.impl.BackwardImpl
	 * @see demo.lego.impl.LegoPackageImpl#getBackward()
	 * @generated
	 */
	int BACKWARD = 6;

	/**
	 * The feature id for the '<em><b>Distance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BACKWARD__DISTANCE = MOVE__DISTANCE;

	/**
	 * The number of structural features of the '<em>Backward</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BACKWARD_FEATURE_COUNT = MOVE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.lego.impl.TurnImpl <em>Turn</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.lego.impl.TurnImpl
	 * @see demo.lego.impl.LegoPackageImpl#getTurn()
	 * @generated
	 */
	int TURN = 7;

	/**
	 * The feature id for the '<em><b>Angle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TURN__ANGLE = INSTRUCTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Turn</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TURN_FEATURE_COUNT = INSTRUCTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link demo.lego.impl.TurnCWImpl <em>Turn CW</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.lego.impl.TurnCWImpl
	 * @see demo.lego.impl.LegoPackageImpl#getTurnCW()
	 * @generated
	 */
	int TURN_CW = 8;

	/**
	 * The feature id for the '<em><b>Angle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TURN_CW__ANGLE = TURN__ANGLE;

	/**
	 * The number of structural features of the '<em>Turn CW</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TURN_CW_FEATURE_COUNT = TURN_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link demo.lego.impl.TurnCCWImpl <em>Turn CCW</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see demo.lego.impl.TurnCCWImpl
	 * @see demo.lego.impl.LegoPackageImpl#getTurnCCW()
	 * @generated
	 */
	int TURN_CCW = 9;

	/**
	 * The feature id for the '<em><b>Angle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TURN_CCW__ANGLE = TURN__ANGLE;

	/**
	 * The number of structural features of the '<em>Turn CCW</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TURN_CCW_FEATURE_COUNT = TURN_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link demo.lego.InstructionsSet <em>Instructions Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instructions Set</em>'.
	 * @see demo.lego.InstructionsSet
	 * @generated
	 */
	EClass getInstructionsSet();

	/**
	 * Returns the meta object for the containment reference list '{@link demo.lego.InstructionsSet#getInstructions <em>Instructions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Instructions</em>'.
	 * @see demo.lego.InstructionsSet#getInstructions()
	 * @see #getInstructionsSet()
	 * @generated
	 */
	EReference getInstructionsSet_Instructions();

	/**
	 * Returns the meta object for class '{@link demo.lego.Instruction <em>Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instruction</em>'.
	 * @see demo.lego.Instruction
	 * @generated
	 */
	EClass getInstruction();

	/**
	 * Returns the meta object for class '{@link demo.lego.Open <em>Open</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Open</em>'.
	 * @see demo.lego.Open
	 * @generated
	 */
	EClass getOpen();

	/**
	 * Returns the meta object for class '{@link demo.lego.Close <em>Close</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Close</em>'.
	 * @see demo.lego.Close
	 * @generated
	 */
	EClass getClose();

	/**
	 * Returns the meta object for class '{@link demo.lego.Move <em>Move</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Move</em>'.
	 * @see demo.lego.Move
	 * @generated
	 */
	EClass getMove();

	/**
	 * Returns the meta object for the attribute '{@link demo.lego.Move#getDistance <em>Distance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Distance</em>'.
	 * @see demo.lego.Move#getDistance()
	 * @see #getMove()
	 * @generated
	 */
	EAttribute getMove_Distance();

	/**
	 * Returns the meta object for class '{@link demo.lego.Forward <em>Forward</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Forward</em>'.
	 * @see demo.lego.Forward
	 * @generated
	 */
	EClass getForward();

	/**
	 * Returns the meta object for class '{@link demo.lego.Backward <em>Backward</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Backward</em>'.
	 * @see demo.lego.Backward
	 * @generated
	 */
	EClass getBackward();

	/**
	 * Returns the meta object for class '{@link demo.lego.Turn <em>Turn</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Turn</em>'.
	 * @see demo.lego.Turn
	 * @generated
	 */
	EClass getTurn();

	/**
	 * Returns the meta object for the attribute '{@link demo.lego.Turn#getAngle <em>Angle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Angle</em>'.
	 * @see demo.lego.Turn#getAngle()
	 * @see #getTurn()
	 * @generated
	 */
	EAttribute getTurn_Angle();

	/**
	 * Returns the meta object for class '{@link demo.lego.TurnCW <em>Turn CW</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Turn CW</em>'.
	 * @see demo.lego.TurnCW
	 * @generated
	 */
	EClass getTurnCW();

	/**
	 * Returns the meta object for class '{@link demo.lego.TurnCCW <em>Turn CCW</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Turn CCW</em>'.
	 * @see demo.lego.TurnCCW
	 * @generated
	 */
	EClass getTurnCCW();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	LegoFactory getLegoFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals
	{
		/**
		 * The meta object literal for the '{@link demo.lego.impl.InstructionsSetImpl <em>Instructions Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.lego.impl.InstructionsSetImpl
		 * @see demo.lego.impl.LegoPackageImpl#getInstructionsSet()
		 * @generated
		 */
		EClass INSTRUCTIONS_SET = eINSTANCE.getInstructionsSet();

		/**
		 * The meta object literal for the '<em><b>Instructions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INSTRUCTIONS_SET__INSTRUCTIONS = eINSTANCE.getInstructionsSet_Instructions();

		/**
		 * The meta object literal for the '{@link demo.lego.Instruction <em>Instruction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.lego.Instruction
		 * @see demo.lego.impl.LegoPackageImpl#getInstruction()
		 * @generated
		 */
		EClass INSTRUCTION = eINSTANCE.getInstruction();

		/**
		 * The meta object literal for the '{@link demo.lego.impl.OpenImpl <em>Open</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.lego.impl.OpenImpl
		 * @see demo.lego.impl.LegoPackageImpl#getOpen()
		 * @generated
		 */
		EClass OPEN = eINSTANCE.getOpen();

		/**
		 * The meta object literal for the '{@link demo.lego.impl.CloseImpl <em>Close</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.lego.impl.CloseImpl
		 * @see demo.lego.impl.LegoPackageImpl#getClose()
		 * @generated
		 */
		EClass CLOSE = eINSTANCE.getClose();

		/**
		 * The meta object literal for the '{@link demo.lego.impl.MoveImpl <em>Move</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.lego.impl.MoveImpl
		 * @see demo.lego.impl.LegoPackageImpl#getMove()
		 * @generated
		 */
		EClass MOVE = eINSTANCE.getMove();

		/**
		 * The meta object literal for the '<em><b>Distance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOVE__DISTANCE = eINSTANCE.getMove_Distance();

		/**
		 * The meta object literal for the '{@link demo.lego.impl.ForwardImpl <em>Forward</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.lego.impl.ForwardImpl
		 * @see demo.lego.impl.LegoPackageImpl#getForward()
		 * @generated
		 */
		EClass FORWARD = eINSTANCE.getForward();

		/**
		 * The meta object literal for the '{@link demo.lego.impl.BackwardImpl <em>Backward</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.lego.impl.BackwardImpl
		 * @see demo.lego.impl.LegoPackageImpl#getBackward()
		 * @generated
		 */
		EClass BACKWARD = eINSTANCE.getBackward();

		/**
		 * The meta object literal for the '{@link demo.lego.impl.TurnImpl <em>Turn</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.lego.impl.TurnImpl
		 * @see demo.lego.impl.LegoPackageImpl#getTurn()
		 * @generated
		 */
		EClass TURN = eINSTANCE.getTurn();

		/**
		 * The meta object literal for the '<em><b>Angle</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TURN__ANGLE = eINSTANCE.getTurn_Angle();

		/**
		 * The meta object literal for the '{@link demo.lego.impl.TurnCWImpl <em>Turn CW</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.lego.impl.TurnCWImpl
		 * @see demo.lego.impl.LegoPackageImpl#getTurnCW()
		 * @generated
		 */
		EClass TURN_CW = eINSTANCE.getTurnCW();

		/**
		 * The meta object literal for the '{@link demo.lego.impl.TurnCCWImpl <em>Turn CCW</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see demo.lego.impl.TurnCCWImpl
		 * @see demo.lego.impl.LegoPackageImpl#getTurnCCW()
		 * @generated
		 */
		EClass TURN_CCW = eINSTANCE.getTurnCCW();

	}

} //LegoPackage
