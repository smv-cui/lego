package demo;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import demo.commons.LogManager;

public class DemoActivator extends AbstractUIPlugin {
	private static final Logger LOG = LogManager.INSTANCE.getLogger(DemoActivator.class.getName());
	public static final DemoActivator INSTANCE = new DemoActivator();
	private static final String DEMO_ID         = "demo";
	public static final String SMV_IMAGE_ID     = "demo.images.smv";
	public static final String LEGO_IMAGE_ID    = "demo.images.lego";
	public static final String SCRATCH_IMAGE_ID = "demo.images.scratch";
	public static final String RECYCLE_IMAGE_ID = "demo.images.recycle";
	@Override
	protected final void initializeImageRegistry(final ImageRegistry registry) {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"initializeImageRegistry\" method");
		super.initializeImageRegistry(registry);
		registry.put(SMV_IMAGE_ID,
				ImageDescriptor.createFromURL(FileLocator.find(
						Platform.getBundle(DEMO_ID),
						new Path("/images/smv_16.png"), null)));
		registry.put(LEGO_IMAGE_ID,
				ImageDescriptor.createFromURL(FileLocator.find(
						Platform.getBundle(DEMO_ID),
						new Path("/images/lego_16.png"), null)));
		registry.put(SCRATCH_IMAGE_ID,
				ImageDescriptor.createFromURL(FileLocator.find(
						Platform.getBundle(DEMO_ID),
						new Path("/images/scratch_16.png"), null)));
		registry.put(RECYCLE_IMAGE_ID,
				ImageDescriptor.createFromURL(FileLocator.find(
						Platform.getBundle(DEMO_ID),
						new Path("/images/recycle_16.png"), null)));
	}
}