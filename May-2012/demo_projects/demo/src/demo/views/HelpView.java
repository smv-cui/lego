package demo.views;

import org.apache.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

import demo.commons.LogManager;

public class HelpView extends ViewPart {
	private static final Logger LOG = LogManager.INSTANCE.getLogger(HelpView.class.getName());
	public static final String ID = "demo.views.help";
	private static final int FONT_SIZE = 16;
	private static final String HELP = "Le but de la démonstration est d'amener la balle rouge dans la zone verte.\n" +
			"Il est strictement interdit de sortir du terrain ainsi que de toucher l'obstacle représenté par la croix.\n" +
			"Le terrain est composé de 15 carrés en longueur et 14 carrés en largeur, le coté d'un carré mesure 10 cm.";

	private Text text;

	@Override
	public void createPartControl(Composite parent) {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"createPartControl\" method");
		Composite top = new Composite(parent, SWT.NONE);
		top.setLayout(new FillLayout());
		text = new Text(top, SWT.READ_ONLY | SWT.BORDER | SWT.MULTI);
		text.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		//text.setForeground(parent.getDisplay().getSystemColor(SWT.COLOR_RED));
		text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		text.setFont(new Font(parent.getDisplay(), "Arial", FONT_SIZE, SWT.BOLD));
		text.setText(HELP);
	}

	@Override
	public void setFocus() {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"setFocus\" method");
		text.setFocus();
	}

	@Override
	public void dispose() {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"dispose\" method");
		text.dispose();
	}
}