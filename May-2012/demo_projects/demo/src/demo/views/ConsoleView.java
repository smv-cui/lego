package demo.views;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Enumeration;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

import demo.commons.LogManager;

public class ConsoleView extends ViewPart {
	private static final Logger LOG = LogManager.INSTANCE.getLogger(ConsoleView.class.getName());
	public static final String ID = "demo.views.console";
	private static final int FONT_SIZE = 11;

	private OutputStream out;
	private Text text;
	private Action clearAction;

	@Override
	public void createPartControl(Composite parent) {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"createPartControl\" method");
		constructComponents(parent);
		initializeMenuBar();
	}

	@Override
	public void setFocus() {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"setFocus\" method");
		text.setFocus();
	}

	@Override
	public void dispose() {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"dispose\" method");
		text.dispose();
		if (out != null) {
			try {
				out.flush();
				out.close();
			} catch (IOException e) {
				LOG.error(e,e);
			}
		}
	}

	private void constructComponents(Composite parent) {		
		Composite top = new Composite(parent, SWT.NONE);
		top.setLayout(new FillLayout());

		text = new Text(top, SWT.READ_ONLY | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
		text.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		text.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		text.setFont(new Font(parent.getDisplay(), "Arial", FONT_SIZE, SWT.NORMAL));

		// Initialize a new outputstream
		out = new OutputStream() {
			public void write(int b) throws IOException {
				if (text.isDisposed()) return;
				text.append(String.valueOf((char) b));
			}
		};

		// Iterates over all the appender and sets their output stream
		Enumeration<?> e = LogManager.INSTANCE.getRootLogger().getAllAppenders();
		while (e.hasMoreElements()) {
			Object o = e.nextElement();
			if (o instanceof ConsoleAppender) {
				ConsoleAppender ca = ((ConsoleAppender) o);
				ca.setWriter(new OutputStreamWriter(new BufferedOutputStream(out)));
				ca.setImmediateFlush(true);
			}
		}

		// Add the clear action
		clearAction = new Action() {
			public void run() {
				text.setText("");
				LOG.debug("Clear Action clicked.");
			}
		};
		clearAction.setText("Clear");
		clearAction.setToolTipText("Clear");            
	}

	private void initializeMenuBar() {
		IMenuManager menuManager = getViewSite().getActionBars().getMenuManager();
		menuManager.add(clearAction);
	}
}