package demo.views;

import java.awt.Color;
import java.awt.geom.Area;

import org.apache.log4j.Logger;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.part.ViewPart;

import demo.commons.LogManager;
import demo.commons.SWTGraphics2D;
import demo.lego.Instruction;
import demo.lego.InstructionsSet;
import demo.lego.util.FieldException;
import demo.lego.util.GameField;

// TODO ? add game field scale ?
public class SimulatorView extends ViewPart {
	private static final Logger LOG = LogManager.INSTANCE.getLogger(SimulatorView.class.getName());
	public static final String ID = "demo.views.simulator";

	private static final GameField field = new GameField(.5);
	private static final SWTGraphics2D gc = new SWTGraphics2D();
	private java.awt.Rectangle fieldArea;
	private Composite parent;
	private Canvas canvas;
	private Image buffer;
	
	/**
	 * This method is called when we reset the demo.
	 */
	public void setToDefaults() {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"setToDefaults\" method");
		field.setToDefaults();
		this.repaint();
	}
	
	/**
	 * Run the simulation
	 * 
	 * @return
	 * 		true if the goal is not reached, false otherwise.
	 * @throws
	 * 		FieldException if Instruction is malformed or not recognized.
	 */
	private static final int REFRESH = 600; // ms
	final public boolean run (final InstructionsSet set) throws FieldException {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"run\" method");
		this.setToDefaults();
		String instr, error;
		int counter = 1;
		for (Instruction i : set.getInstructions()) {
			instr = "";
			error = null;
			try { Thread.sleep(REFRESH); }
			catch (Exception e) { LOG.fatal(e); }
			try { instr = field.execute(i); }
			catch (FieldException e) { error = e.getMessage(); }
			if (!field.isRobotInField()) {
				error = "Le robot sort du terrain";
			} else if (field.isRobotInWalls()) {
				error = "Le robot touche l'obstacle";
			} else if (field.hasRobotBumpedBall()) {
				error = "Le robot rate la balle";
			}
			this.repaint();
			LOG.debug(instr + "\t=> " + field.printRobot());
			if (error != null) {
            error += ", à la ligne " + counter;
				LOG.warn(error);
				MessageDialog.openWarning(parent.getShell(), "Problème", error);
				return false;
			}
			counter++;
		}
		// check if goal reached ...
		return field.isBallInGoal();
	}

	/**
	 * This is the callback that allows us to create the viewer and initialize it.
	 */
	@Override
	public void createPartControl(final Composite parent) {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"createPartControl\" method");

		this.parent = parent;
		canvas = new Canvas(parent, SWT.NO_BACKGROUND);
		fieldArea = field.fieldArea().getBounds();

		parent.addListener(SWT.Resize, new Listener() {
			public void handleEvent(Event e) {
				//LOG.debug("Call \"resize\" method with: " + e);
				repaint();
			}
		});

		//canvas.setBackground(display.getSystemColor(SWT.COLOR_WHITE));
		canvas.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				doubleBufferPaint(e.gc);
			}
		});
	}

	/**
	 * This allows us to pass the focus request to ...
	 */
	@Override
	public void setFocus() {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"setFocus\" method");
		repaint();
	}

	private void doubleBufferPaint(GC dest) {
		Point p = canvas.getSize();
		if (p.x <= 0 || p.y <= 0) return;
		if (buffer != null) {
			Rectangle b = buffer.getBounds();
			// Check if buffer is still valid, ie:
			// a buffer is invalid when size differs
			if (b.width != p.x || b.height != p.y) {
				buffer.dispose();
				buffer = null;
			}
		}
		if (buffer == null) buffer = new Image(parent.getDisplay(), p.x, p.y);
		GC tmp = new GC(buffer);
		try {
			this.doPaint(tmp);
			dest.drawImage(buffer, 0, 0);
		} catch (Exception e) { LOG.warn(e); }
		finally {
			tmp.dispose();
		}
	}

	// paint the items
	private void doPaint(GC dest) {
		Point p = canvas.getSize();
		final double margin = 0.025; // ensures the field is always fully visible
		final double dxy = - margin + Math.min(
				p.x/(double)fieldArea.width,
				p.y/(double)fieldArea.height);
		gc.setGC(dest);
		gc.setColor(Color.WHITE);
		gc.fillRect(0,0,p.x,p.y);
		gc.scale(dxy,dxy);
		//draw the grid
		gc.setColor(Color.LIGHT_GRAY);
		int d = 10;
		for (int i=d;i<=fieldArea.width; i+=d) {
			gc.drawLine(i,0,i,fieldArea.height);
			gc.drawLine(0,i,fieldArea.width,i);
		}
		//draw the field
		gc.setColor(Color.BLACK);
		gc.draw(fieldArea);
		//draw the goal
		gc.setColor(Color.GREEN);
		gc.fillRect(field.goalArea().getBounds());
		//draw the walls
		gc.setColor(Color.DARK_GRAY);
		gc.fill(field.wallsArea());
		//draw claw area
		gc.setColor(Color.CYAN);
		gc.fill(field.clawArea());
		//draw robot
		gc.setColor(Color.BLUE);
		gc.draw(field.robotArea());
		//draw the ball
		Area ball = field.ballArea();
		if (ball != null) {
			gc.setColor(Color.RED);
			gc.fill(ball);
		}
	}
	
	/**
	 * This is the callback that allows us to to force the viewer to repaint.
	 */
	private void repaint() {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"repaint\" method");
		canvas.redraw();
		canvas.update();
		parent.getShell().update();
		parent.getDisplay().update();
	}
}