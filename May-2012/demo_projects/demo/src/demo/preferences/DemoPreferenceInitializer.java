package demo.preferences;

import org.apache.log4j.Logger;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import demo.commons.LogManager;
import demo.lego.ui.internal.LegoActivator;

// Initialize default preferences
public class DemoPreferenceInitializer extends AbstractPreferenceInitializer {
	private static final Logger LOG = LogManager.INSTANCE.getLogger(DemoPreferenceInitializer.class.getName());
	
	static final String FONT = "1|Arial|14.25|0|WINDOWS|1|-19|0|0|0|400|0|0|0|0|3|2|1|34|Arial";
	static final String FONT_LEGO = "demo.lego.Lego.syntaxColorer.tokenStyles.code.font";
	static final String FONT_JAVA = "org.eclipse.jdt.ui.editors.textfont";
	static final String FONT_TEXT = "org.eclipse.jface.textfont";
	static final String TAB_WIDTH = "org.eclipse.ui.editors.tabWidth";
	static final String LINE_NUMBERS = "org.eclipse.ui.editors.lineNumberRuler";

	@Override
	public void initializeDefaultPreferences() {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"initializeDefaultPreferences\" method");
		IPreferenceStore store = LegoActivator.getInstance().getPreferenceStore();
		store.setDefault(FONT_LEGO, FONT);
		store.setDefault(FONT_JAVA, FONT);
		store.setDefault(FONT_TEXT, FONT);
		store.setDefault(TAB_WIDTH, 2);
		store.setDefault(LINE_NUMBERS, true);
	}
}
