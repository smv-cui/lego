package demo;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.Platform;
import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbenchPreferenceConstants;
import org.eclipse.ui.PlatformUI;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import demo.commons.LockSocket;
import demo.commons.LogManager;
import demo.commons.ProjectManager;

public class DemoApplication implements IApplication {
	public static final String ID = "demo";
	private static Logger LOG;

	@Override
	public Object start(IApplicationContext context) throws Exception {
		Display display = PlatformUI.createDisplay();
		// Try to acquire the unique lock for this application:
		// if the lock cannot be acquired, we send an error message.
		if (!LockSocket.INSTANCE.acquire()) {
			String msgError = "L'application est déjà en cours d'exécution.";
			MessageDialog.openError(display.getActiveShell(), "Problème", msgError);
			display.getActiveShell().forceActive();
			throw new RuntimeException(msgError);
		}
		// Sets preferences
		IPreferenceStore ps = PlatformUI.getPreferenceStore();
		ps.setValue(IWorkbenchPreferenceConstants.PERSPECTIVE_BAR_SIZE, "50");
		ps.setValue(IWorkbenchPreferenceConstants.SHOW_PROGRESS_ON_STARTUP, "true");
		ps.setValue(IWorkbenchPreferenceConstants.SHOW_MEMORY_MONITOR, "true");
		ps.setValue(IWorkbenchPreferenceConstants.SHOW_INTRO, "false");
		ps.setValue(IWorkbenchPreferenceConstants.SHOW_SYSTEM_JOBS, "true");
		ps.setValue(IWorkbenchPreferenceConstants.SHOW_TRADITIONAL_STYLE_TABS, "true");
		ps.setValue(IWorkbenchPreferenceConstants.SHOW_OTHER_IN_PERSPECTIVE_MENU, "false");
		ps.setValue(IWorkbenchPreferenceConstants.SHOW_OPEN_ON_PERSPECTIVE_BAR, "false");
		ps.setValue(IWorkbenchPreferenceConstants.KEY_CONFIGURATION_ID, "demo.key_configuration");
		ps.setValue(IWorkbenchPreferenceConstants.NO_NEW_PERSPECTIVE, "true");
		ps.setValue(IWorkbenchPreferenceConstants.USE_COLORED_LABELS, "true");
		ps.setValue(IWorkbenchPreferenceConstants.VIEW_MINIMUM_CHARACTERS, "14");
		ps.setValue(IWorkbenchPreferenceConstants.EDITOR_MINIMUM_CHARACTERS, "14");
		ps.setValue(IWorkbenchPreferenceConstants.DISABLE_NEW_FAST_VIEW, "true");
		// Setup log4j
		initializeLogging(context.getBrandingBundle().getBundleContext());
		if (LOG.isDebugEnabled()) LOG.debug("Using workspace: " + ResourcesPlugin.getWorkspace().getRoot().getLocation());
		// Start workbench
		try {
			int returnCode = PlatformUI.createAndRunWorkbench(display,
					new DemoWorkbenchAdvisor(getArguments(context)));
			if (returnCode == PlatformUI.RETURN_RESTART) {
				if (LOG.isDebugEnabled()) LOG.debug("Exit \"start\" method with RESTART");
				return IApplication.EXIT_RESTART;
			}
			if (LOG.isDebugEnabled()) LOG.debug("Exit \"start\" method with OK");
			return IApplication.EXIT_OK;
		} finally {
			if (LOG.isDebugEnabled()) LOG.debug("Done finally \"start\" method");
			display.dispose();
		}
	}

	public void stop() {
		LOG.info("Call \"stop\" method");
		LockSocket.INSTANCE.release();
	}

	// Setup log4j
	private void initializeLogging(final BundleContext context) {
		String p = ProjectManager.EXE + "log4j.properties";
		String l = ProjectManager.PRJ + "demo.log";
		try {
			InputStream is = new FileInputStream(p);
			Properties props = new Properties();
			props.load(is);
			is.close();
			props.put("log4j.appender.FA.File", l); // overwrite "log.dir"
			LogManager manager = LogManager.initialize(props);
			LOG = manager.getLogger(DemoActivator.class.getName());
			Package pkg = Logger.class.getPackage();
			if (LOG.isDebugEnabled()) {
				LOG.debug("Logging with " + pkg.getImplementationTitle() + " v" + pkg.getImplementationVersion());
				LOG.debug("Properties file: " + p);
				LOG.debug("Output log file: " + l);
			}
			for (Bundle bundle : context.getBundles()){
				ILog pluginLogger = Platform.getLog(bundle);
				String pluginId = bundle.getSymbolicName();
				manager.hookPlugin(pluginId,pluginLogger);
				if (LOG.isTraceEnabled()) LOG.trace("Added logging hook for bundle: " + pluginId);
			}
		}
		catch (Exception e) {
			String message = "Error while initializing log properties." +  e.getMessage();
			throw new RuntimeException(message,e);
		}
	}

	// Returns the application's arguments
	private String[] getArguments(IApplicationContext context) {
		Object value = context.getArguments().get("application.args");
		if (value instanceof String[]) {
			return (String[]) value;
		}
		return new String[0];
	}
}
