package demo;

import org.apache.log4j.Logger;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MenuDetectEvent;
import org.eclipse.swt.events.MenuDetectListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tray;
import org.eclipse.swt.widgets.TrayItem;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchAdvisor;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

import demo.commons.LogManager;
import demo.commons.ProjectManager;
import demo.lego.LegoStandaloneSetup;
import demo.scratch.ScratchStandaloneSetup;

public class DemoWorkbenchAdvisor extends WorkbenchAdvisor {
	private static final Logger LOG = LogManager.INSTANCE.getLogger(DemoWorkbenchAdvisor.class.getName());
	private final String[] args;

	public DemoWorkbenchAdvisor(String[] args) {
		if (LOG.isDebugEnabled()) LOG.debug("Call constructor");
		this.args = args;
	}

	@Override
	public String getInitialWindowPerspectiveId() {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"getInitialWindowPerspectiveId\" method");
		return DemoPerspective.ID;
	}

	@Override
	public void initialize(IWorkbenchConfigurer configurer) {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"initialize\" method");
		super.initialize(configurer);
		configurer.setSaveAndRestore(false);
	}
	
	@Override
	public WorkbenchWindowAdvisor createWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"createWorkbenchWindowAdvisor\" method");
		return new WorkbenchWindowAdvisor(configurer) {
			private final Logger LOG = LogManager.INSTANCE.getLogger(WorkbenchWindowAdvisor.class.getName());

			private TrayItem trayItem;
			private Image trayImage;

			private static final int DEFAULT_WIDTH  = 800;
			private static final int DEFAULT_HEIGHT = 800;

			private static final String APP_NAME = "Demo Lego NXT";

			@Override
			public ActionBarAdvisor createActionBarAdvisor(IActionBarConfigurer abConfigurer) {
				if (LOG.isDebugEnabled()) LOG.debug("Call \"createActionBarAdvisor\" method");
				return new DemoActionBarAdvisor(abConfigurer);
			}
			
			@Override
			public void preWindowOpen() {
				if (LOG.isDebugEnabled()) LOG.debug("Call \"preWindowOpen\" method");
				super.preWindowOpen();
				IWorkbenchWindowConfigurer wc = getWindowConfigurer();
				wc.setInitialSize(new Point(DEFAULT_WIDTH, DEFAULT_HEIGHT));
				wc.setShowPerspectiveBar(false);
				wc.setShowFastViewBars(false);
				wc.setShowProgressIndicator(true);
				wc.setShowStatusLine(true);
				wc.setShowMenuBar(true);
				wc.setShowCoolBar(true);
				wc.setTitle(APP_NAME);
			}

			@Override
			public void postWindowCreate() {
				if (LOG.isDebugEnabled()) LOG.debug("Call \"postWindowCreate\" method");
				super.postWindowCreate();
				for (IWorkbenchPage page : getWindowConfigurer().getWindow().getPages()) {
					for (String actionSet : ACTION_SETS_TO_HIDE) {
						page.hideActionSet(actionSet);
						if (LOG.isTraceEnabled()) LOG.trace("Removed action set: " + actionSet);
					}
				}
			}

			@Override
			public void postWindowOpen() {
				if (LOG.isDebugEnabled()) LOG.debug("Call \"postWindowOpen\" method");
				super.postWindowOpen();
				IWorkbenchWindow ww = getWindowConfigurer().getWindow();
				// Shows and treats arguments
				String msgArgs = "Application argument" + ((args.length>1)?"s":"") + " : ";
				for (String arg : args) {
					msgArgs += arg + " ";
					// Treat args
					if (arg.equals("-showlocation")) {
						
					}
				}
				if (args.length>1) LOG.info(msgArgs);
				try {
					// Activate Xtext (Lego, Scratch)
					LegoStandaloneSetup.doSetup();
					ScratchStandaloneSetup.doSetup();
					// Initialize default files and opens editor
					ProjectManager pm = ProjectManager.getInstance();
					pm.cleanup();
					pm.copy();
					pm.openLego(ww);
				} catch (Exception e) {
					LOG.error(e,e);
					throw new RuntimeException(e);
				}
				// Some OS might not support tray items
				trayItem = initTaskItem(ww);
				if (trayItem != null) {
					minimizeBehavior();
					// Create exit and about action on the icon
					hookPopupMenu();
				} else {
					LOG.warn("OS does not support tray items.");
				}
				// Full screen mode
				ww.getShell().setFullScreen(true);
			}

			
			
			@Override
			public void dispose() {
				if (LOG.isDebugEnabled()) LOG.debug("Call \"dispose\" method");
				super.dispose();
				if (trayImage != null) {
					trayImage.dispose();
				}
				if (trayItem != null) {
					trayItem.dispose();
				}
			}

			// Add a listener to the shell
			private void minimizeBehavior() {
				getWindowConfigurer().getWindow().getShell().addShellListener(new ShellAdapter() {
					// If the window is minimized hide the window
					public void shellIconified(ShellEvent e) {
						getWindowConfigurer().getWindow().getShell().setVisible(false);
					}
				});
				// If user double-clicks on the tray icons
				// the application will be visible again
				trayItem.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						Shell shell = getWindowConfigurer().getWindow().getShell();
						if (!shell.isVisible()) {
							getWindowConfigurer().getWindow().getShell().setMinimized(false);
							shell.setVisible(true);
						}
					}
				});
			}

			// We hook up on menu entry which allows to close the application
			private void hookPopupMenu() {
				trayItem.addMenuDetectListener(new MenuDetectListener() {
					public void menuDetected(MenuDetectEvent e) {
						Menu menu = new Menu(getWindowConfigurer().getWindow().getShell(), SWT.POP_UP);
						MenuItem exit = new MenuItem(menu, SWT.NONE);
						exit.setText("Exit");
						exit.addSelectionListener(new SelectionAdapter() {
							public void widgetSelected(SelectionEvent e) {
								System.exit(0);
							}
						});
						menu.setVisible(true);
					}
				});
			}

			// This methods create the tray item and return a reference
			private TrayItem initTaskItem(IWorkbenchWindow window) {
				final Tray tray = window.getShell().getDisplay().getSystemTray();
				TrayItem trayItem = new TrayItem(tray, SWT.NONE);
				trayImage = DemoActivator.INSTANCE.getImageRegistry().get(DemoActivator.SMV_IMAGE_ID);
				trayItem.setImage(trayImage);
				trayItem.setToolTipText(APP_NAME);
				return trayItem;
			}
			
			// TODO remove unwanted action sets:
			//   * mark occurences
			//   * show tooltip
			//   * word completion
			//   * quickfix
			//   * set encoding
			private final String[] ACTION_SETS_TO_HIDE = new String[] {
					"org.eclipse.ui.edit.text.actionSet.navigation",
					"org.eclipse.ui.edit.text.actionSet.convertLineDelimitersTo",
					"org.eclipse.ui.edit.text.actionSet.annotationNavigation",
					"org.eclipse.ui.actionSet.openFiles",
					"org.eclipse.ui.actionSet.keyBindings",
					"org.lejos.nxt.ldt",
					"org.eclipse.mylyn.tasks.ui.navigation",
					"org.eclipse.ui.cheatsheets.actionSet",
					"org.eclipse.ui.externaltools.ExternalToolsSet",
					"org.eclipse.search.menu",
					"org.eclipse.search.searchActionSet",
					"org.eclipse.search.TextSearchWorkingSet",
					"org.eclipse.search.TextSearchWorkspace",
					"org.eclipse.search.TextSearchProject",
					"org.eclipse.search.OpenFileSearchPage",
					"org.eclipse.search.TextSearchFile",
					"org.eclipse.ui.edit.text.actionSet.presentation",
					"org.eclipse.ui.edit.text.actionSet.openExternalFile",
					"org.eclipse.ui.WorkingSetActionSet",
					"org.eclipse.jdt.ui.edit.text.java.toggleMarkOccurrences",
					"org.eclipse.xtext.ui.editor.markOccurrences",
					// Useless
//					"org.eclipse.update.ui.softwareUpdates",
			};
		};
	}
}
