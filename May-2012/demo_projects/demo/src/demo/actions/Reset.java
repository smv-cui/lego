package demo.actions;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.PlatformUI;

import demo.DemoActionBarAdvisor;
import demo.DemoActivator;
import demo.commons.LogManager;
import demo.views.SimulatorView;

public class Reset extends AbstractAction {

	static {
		LOG = LogManager.INSTANCE.getLogger(Reset.class.getName());
	}
	
	private SimulatorView view;

	public Reset() {
		super("Reset","Reset workspace","demo.actions.reset",DemoActivator.RECYCLE_IMAGE_ID);
		if (LOG.isDebugEnabled())  LOG.debug("Call constructor method");
		setEnabled(true);
	}

	@Override
	public void run() {
		start();
		if (!MessageDialog.openConfirm(window.getShell(), "Confirmation",
				"Veuillez confirmer la réinitialisation de l'espace de travail.")) return;
		if (view == null) {
			// Find the view reference
			IViewReference[] views = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViewReferences();
			for (IViewReference v : views) {
				if (v.getId().equals(SimulatorView.ID)) {
					view = (SimulatorView) v.getView(true);
					break;
				}
			}
			if (view == null) {
				LOG.fatal("Simulator view not found.");
				setEnabled(false);
			}
		}
		try {
			view.setToDefaults();
			manager.cleanup();
			manager.copy();
			manager.openLego(window);
			DemoActionBarAdvisor.DOWNLOAD.setEnabled(false);
		} catch (Exception e) {
			LOG.error(e,e);
		} finally {
			stop();
		}
	}
}