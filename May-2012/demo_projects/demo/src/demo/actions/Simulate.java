package demo.actions;

import java.io.File;
import java.security.MessageDigest;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.PlatformUI;

import com.google.common.io.Files;

import demo.DemoActionBarAdvisor;
import demo.DemoActivator;
import demo.commons.LogManager;
import demo.commons.ProjectManager;
import demo.lego.InstructionsSet;
import demo.views.SimulatorView;

public class Simulate extends AbstractAction {

	static {
		LOG = LogManager.INSTANCE.getLogger(Simulate.class.getName());
	}

	private String digest = null;
	private SimulatorView view;

	public Simulate() {
		super("Simulate","Start simulation","demo.actions.simulate",DemoActivator.SMV_IMAGE_ID);
		if (LOG.isDebugEnabled())  LOG.debug("Call constructor method");
		setEnabled(true);
	}

	public String getDigest() {
		return digest;
	}
	
	@Override
	public void run() {
		start();
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().saveAllEditors(false);
		if (view == null) {
			// Find the view reference
			IViewReference[] views = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViewReferences();
			for (IViewReference v : views) {
				if (v.getId().equals(SimulatorView.ID)) {
					view = (SimulatorView) v.getView(true);
					break;
				}
			}
			if (view == null) {
				LOG.fatal("View Not found !");
				setEnabled(false);
			}
		}
		boolean correct = false;
		try {
			File file = new File(ProjectManager.LEGO_MODEL_PATH);
			// 1. Load lego model
			Resource resource = new ResourceSetImpl().getResource(URI.createURI(file.toURI().toString()), true);
			if (resource.getContents().isEmpty())
				throw new Exception ("Failed to load file: \"" + ProjectManager.LEGO_MODEL_PATH + "\"");
			final InstructionsSet set = (InstructionsSet) resource.getContents().get(0);
			if (set == null) throw new Exception ("Instructions set is 'null'");
			// 2. Start simulation
			correct = view.run(set);
			if (correct) {
				MessageDialog.openInformation(window.getShell(), "Correct", "Le but est atteint !");
				// Enable Download action and memorize the hash of the file
				DemoActionBarAdvisor.DOWNLOAD.setEnabled(true);
				digest = new String(Files.getDigest(file,MessageDigest.getInstance("MD5")));
			} else {
				MessageDialog.openWarning(window.getShell(), "Incorrect", "Le but est manqué !");
				DemoActionBarAdvisor.DOWNLOAD.setEnabled(false);
				digest = null;
			}
		} catch (Exception e) {
			LOG.error(e,e);
		} finally {
			stop();
		}
	}
}
