package demo.actions;

import java.io.File;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.util.Util;
import org.eclipse.ui.PlatformUI;

import demo.DemoActivator;
import demo.commons.FileWriter;
import demo.commons.LogManager;
import demo.commons.ProjectManager;
import demo.scratch.Message;
import demo.scratch.Project;
import demo.scratch.Script;
import demo.scratch.WhenMsg;
import demo.scratch.generator.ScratchGenerator;

public class Scratch extends AbstractAction {

	static {
		LOG = LogManager.INSTANCE.getLogger(Scratch.class.getName());
	}

	// Script use to start scratch and once it's closed start a transformation
	private static final String START_SCRATCH_MACOSX  =  ProjectManager.SCRIPTS + "start_scratch_macosx";
	//	private static final String START_SCRATCH_WINDOWS =  ProjectManager.SCRIPTS + "start_scratch_windows.bat";

	private String startScratch = null;
	private ResourceSet rs = new ResourceSetImpl();
	private ScratchGenerator gen = new ScratchGenerator();

	public Scratch() {
		super("Scratch","Start Scratch","demo.actions.scratch",DemoActivator.SCRATCH_IMAGE_ID);
		if (LOG.isDebugEnabled())  LOG.debug("Call constructor method");
		setEnabled(false);
		if (System.getenv("DEMO_INSTALL_PATH") == null) {
			LOG.info("Start Scratch is disabled.");
			return;
		}
		// Check if script is present
		// if not present this action remains disabled
		// Only MacOSX and windows platforms are supported
		// TODO add linux support, xtext editor under windows are bugged
		if (Util.isMac() || Util.isLinux()) {
			startScratch = START_SCRATCH_MACOSX;
			//		} else if (Util.isWindows()) {
			//			startScratch = START_SCRATCH_WINDOWS;
		} else  {
			LOG.info("OS is not supported.");
			return;
		}
		if (!new File(startScratch).exists()) {
			LOG.info("File does not exist.");
			return;
		}
		setEnabled(true);
		if (Util.isMac() || Util.isLinux()) {
			startScratch = "sh " + startScratch;
		} else if (Util.isWindows()) {
			startScratch = "" + startScratch;
		}
		LOG.info("Start Scratch: " + startScratch);
	}

	@Override
	public void run() {
		if (startScratch == null) {
			setEnabled(false);
			return;
		}
		start();
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().saveAllEditors(false);
		try {
			// 1. Open scratch in fullscreen with default project file
			// 2. Generate scratch model with python
			Runtime.getRuntime().exec(startScratch).waitFor();
			try { Thread.sleep(500); } // sleep 500 ms
			catch(Exception e) {}
			// 3. open Scratch editor
			manager.openScratch(window);
			// 4. generate Lego model from Scratch model
			// 4a load file
			Resource resource = rs.getResource(URI.createURI(ProjectManager.SCRATCH_MODEL.toURI().toString()), true);
			Project project = (Project) resource.getContents().get(0);
			if (project == null) {
				MessageDialog.openError(window.getShell(), "Problème", "Aucun projet trouvé.");
				return;
			}
			// 4b find NXT object
			demo.scratch.Object object = null;
			for (demo.scratch.Object o : project.getObjects()) {
				if (o.getName().equalsIgnoreCase("NXT")) {
					object = o;
				} else {
					MessageDialog.openError(window.getShell(), "Problème", "Plusieurs objets trouvés.");
					return;
				}
			}
			if (object == null) {
				MessageDialog.openError(window.getShell(), "Problème", "Aucun objet nommé 'NXT' trouvé.");
				return;
			}
			// 4c find correct script !!!
			Script script = null;
			for (Script s : object.getScripts()) {
				if (s.getWhen() instanceof WhenMsg) {
					Message msg = (Message) ((WhenMsg) s.getWhen()).getMsg();
					if (msg != null && msg.getName().equals("Run")) {
						if (script == null) {
							script = s;
						} else {
							MessageDialog.openError(window.getShell(), "Problème", "Plusieurs scripts trouvés.");
							return;
						}
					}
				}
			}
			if (script == null) {
				MessageDialog.openError(window.getShell(), "Problème", "Aucun script trouvé.");
				return;
			}
			// 4d generate
			FileWriter.write(ProjectManager.LEGO_MODEL_PATH, gen.doGenerateLego(script).toString());
			// 5. open Lego editor
			manager.openLego(window);
		} catch (Exception e) {
			LOG.error(e,e);
			return;
		} finally {
			stop();
		}
	}
}