package demo.actions;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.security.MessageDigest;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.util.Util;
import org.eclipse.ui.PlatformUI;

import com.google.common.io.Files;

import demo.DemoActionBarAdvisor;
import demo.DemoActivator;
import demo.commons.FileWriter;
import demo.commons.LogManager;
import demo.commons.ProjectManager;
import demo.lego.InstructionsSet;
import demo.lego.generator.LegoGenerator;

// TODO ? check if robot is connected ?
public class Upload extends AbstractAction {
	static {
		LOG = LogManager.INSTANCE.getLogger(Upload.class.getName());
	}

	// Script use to compile, link and upload the generated program to the NXT
	private static final String ANT_BUILD_FILE = ProjectManager.EXE + "build.xml";
//	private static final String UPLOAD_NXT_MACOSX = System.getProperty("user.home") + "/Documents/LegoPortesOuvertes/May-2012/demo_projects/demo/resources/" + "upload_nxt_macosx";
//	private static final String UPLOAD_NXT_MACOSX  =  ProjectManager.EXE + "resources/upload_nxt_macosx";
//	private static final String UPLOAD_NXT_WINDOWS =  ProjectManager.SCRIPTS + "upload_nxt_windows.bat";

	private String uploadLego = null;
	private ResourceSet rs = new ResourceSetImpl();
	private LegoGenerator gen = new LegoGenerator();

	public Upload() {
		super("Upload","Upload to NXT","demo.actions.upload",DemoActivator.LEGO_IMAGE_ID);
		if (LOG.isDebugEnabled())  LOG.debug("Call constructor method");
		setEnabled(false);
//		if (System.getenv("NXJ_HOME") == null) {
//			LOG.info("Upload Lego is disabled.");
//			return;
//		}
		// Check if script is present
		// if not present this action remains disabled
		// Only MacOSX and windows platforms are supported
		// TODO add linux support, xtext editor under windows are bugged
		if (Util.isMac()) {
			uploadLego = ANT_BUILD_FILE;
//		} else if (Util.isWindows()) {
//			uploadLego = UPLOAD_NXT_WINDOWS;
		} else  {
			LOG.info("OS is not supported.");
			return;
		}
		if (!new File(uploadLego).exists()) {
			LOG.info("File " + ANT_BUILD_FILE + " does not exist.");
			uploadLego = null;
			return;
		}
		LOG.info("Upload Lego: " + uploadLego);
	}

	@Override
	public void run() {
		if (uploadLego == null) {
			setEnabled(false);
			LOG.info("uploadLego was not set, do not upload");
			return;
		}
		start();
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().saveAllEditors(false);
		try {
			// 1. check the checksum of the files
			String digest = new String(Files.getDigest(new File(ProjectManager.LEGO_MODEL_PATH),MessageDigest.getInstance("MD5")));
			if (!digest.equals(DemoActionBarAdvisor.SIMULATE.getDigest())) {
				MessageDialog.openWarning(window.getShell(), "Problème", "Le fichier source a changé, veuillez relancer une simulation.");
				setEnabled(false);
				return;
			}
			// 2. generate Lejos java code from Lego model
			Resource resource = rs.getResource(URI.createURI(ProjectManager.LEGO_MODEL.toURI().toString()), true);
			InstructionsSet set = (InstructionsSet) resource.getContents().get(0);
			if (set == null) {
				MessageDialog.openError(window.getShell(), "Problème", "Aucune instruction trouvée.");
				return;
			}
			FileWriter.write(ProjectManager.JAVA_MODEL_PATH, gen.doGenerateLejos(set).toString());
			// 3. Open Java editor
			manager.openJava(window);
			// 4. check for connection with NXT brick
//			if (!checkConnection()) {
//				MessageDialog.openError(window.getShell(), "Problème", "Le robot n'est pas connecté.");
//				return;
//			}
			// 4. compile, link and upload (USB or BT)
			uploadLego = "/opt/local/bin/ant -lib " + ProjectManager.EXE + "lib/ -logfile " + ProjectManager.PRJ + "log.txt -buildfile " + uploadLego;
			LOG.info("Upload command: " + uploadLego);
			
			Process p = Runtime.getRuntime().exec(uploadLego);
			p.waitFor();
			LOG.info("upload should now be done");
			BufferedReader out = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line = null;
			while ((line = out.readLine())!=null) {
				LOG.info(line);
			}
			BufferedReader err = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			line = null;
			while ((line = err.readLine())!=null) {
				LOG.warn(line);
			}
			
//			InputStreamReader is = new InputStreamReader(p.getInputStream());
//			BufferedReader br = new BufferedReader(is);
//			String read = br.readLine();
//			System.out.println("result is " + read);
			//Runtime.getRuntime().exec(uploadLego).waitFor();
			/*
			 * Testing
			 */
//            LOG.info("CD: "        + System.getProperty("user.dir"));
//            LOG.info("PATH: "      + System.getenv("PATH"));
//            LOG.info("CLASSPATH: " + System.getenv("CLASSPATH"));
//			String line;
//			Process p = Runtime.getRuntime().exec(uploadLego);
//	        BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
//	        while ((line = input.readLine()) != null) {
//	          LOG.info(line);
//	        }
//	        input.close();
		} catch (Exception e) {
			LOG.error(e,e);
		} finally {
			stop();
		}
	}

//	private static boolean checkConnection() {
//		NXTComm nxtComm = null;
//		NXTInfo[] infos = null;
//		try {
//			nxtComm = NXTCommFactory.createNXTComm(NXTCommFactory.USB);
//			infos = nxtComm.search("NXT");
//			nxtComm.close();
//		} catch (Exception e) {
//			LOG.fatal(e,e);
//			return false;
//		}
//		if (infos != null && LOG.isInfoEnabled()) {
//			for (NXTInfo info : infos) {
//				LOG.info(info);
//			}
//		}
//		return nxtComm != null && infos != null && infos.length == 1;
//	}
}