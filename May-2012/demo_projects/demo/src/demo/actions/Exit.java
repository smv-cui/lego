package demo.actions;

import org.eclipse.jface.dialogs.MessageDialog;

import demo.commons.LogManager;

public class Exit extends AbstractAction {

	static {
		LOG = LogManager.INSTANCE.getLogger(Exit.class.getName());
	}

	public Exit() {
		super("Exit","Exit the application","demo.actions.exit",null);
		if (LOG.isDebugEnabled())  LOG.debug("Call constructor method");
		setEnabled(true);
	}

	@Override
	public void run() {
		if (!MessageDialog.openConfirm(window.getShell(), "Confirmation",
				"Veuillez confirmer la fermeture de l'application.")) return;
		System.exit(0);
	}
}