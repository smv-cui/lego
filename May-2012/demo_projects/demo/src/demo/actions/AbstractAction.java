package demo.actions;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IWorkbenchWindow;

import demo.DemoActivator;
import demo.commons.ProjectManager;

public abstract class AbstractAction extends Action {
	protected static Logger LOG;
	private static long start, stop;
	private static boolean started = false, stoped = false;
	protected ProjectManager manager;
	protected IWorkbenchWindow window;
	protected boolean isRunning = false;
	/**
	 * @param text
	 * 			setText
	 * @param desc
	 * 			setDescription and setToolTipText
	 * @param id
	 * 			setId
	 * @param image
	 * 			setImageDescriptor
	 */
	public AbstractAction(String text, String desc, String id, String image) {
		setText(text);
		setDescription(desc);
		setToolTipText(desc);
		setId(id);
		if (image != null) {
			setImageDescriptor(ImageDescriptor.createFromImage(
					DemoActivator.INSTANCE.getImageRegistry().get(image)));
		}
		manager = ProjectManager.getInstance();
	}
	final public void initialize(final IWorkbenchWindow window) {
		this.window = window;
	}
	final protected void start(/*String key*/) {
		if (started) LOG.fatal("Already started");
		started = true;
		if (isRunning) return;
		isRunning = true;
		LOG.debug("Running \"" + getText() + "\"");
		start = System.currentTimeMillis();
	}
	final protected void stop(/*String key*/) {
		if (stoped) LOG.fatal("Not started");
		stoped = true;
		stop = System.currentTimeMillis();
		LOG.info("Done \"" + getText() + "\" in " + elapsed());
		isRunning = false;
	}
	final private String elapsed(/*String key*/) {
		started = false;
		stoped = false;
		double minF = (stop - start) / 1000.0 / 60.0; // milliseconds to minutes
		int min = (int) Math.floor(minF);
		int sec = (int) Math.floor((minF - min) * 60.0);
		return min + "'" + sec + "\"";
	}
}
