package demo;

import org.apache.log4j.Logger;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.GroupMarker;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.util.Util;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

import demo.actions.AbstractAction;
import demo.actions.Exit;
import demo.actions.Reset;
import demo.actions.Scratch;
import demo.actions.Simulate;
import demo.actions.Upload;
import demo.commons.LogManager;

// TODO menu bar under Mac OS X
public final class DemoActionBarAdvisor extends ActionBarAdvisor {
	private static final Logger LOG = LogManager.INSTANCE.getLogger(DemoActionBarAdvisor.class.getName());

	private static final AbstractAction
	EXIT = new Exit(),
	RESET = new Reset(),
	SCRATCH = new Scratch();
	public static final Simulate SIMULATE = new Simulate();
	public static final Upload DOWNLOAD = new Upload();

	public DemoActionBarAdvisor(IActionBarConfigurer actionBarConfigurer) {
		super(actionBarConfigurer);
		if (LOG.isDebugEnabled()) LOG.debug("Call constructor method");
	}

	protected void fillCoolBar(ICoolBarManager cbManager) {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"fillCoolBar\" method");
		cbManager.setLockLayout(true);
		// Demo group
		cbManager.add(new GroupMarker("group.demo"));
		{
			IToolBarManager demo = new ToolBarManager(cbManager.getStyle());
			cbManager.add(demo);
			demo.add(SCRATCH);
			demo.add(SIMULATE);
			demo.add(DOWNLOAD);
		}
		//
		cbManager.update(true);
	}

	protected void fillMenuBar(IMenuManager menubar) {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"fillMenuBar\" method");
		menubar.removeAll();
		menubar.setVisible(true);
		menubar.add(createFileMenu());
		menubar.add(createEditMenu());
		menubar.add(createDemoMenu());
		if (!Util.isMac()) menubar.add(createHelpMenu());
		menubar.update(true);
	}

	protected void makeActions(IWorkbenchWindow window) {
		if (LOG.isDebugEnabled()) LOG.debug("Call \"makeActions\" method");
		registerAsGlobal(ActionFactory.PREFERENCES.create(window));
		registerAsGlobal(ActionFactory.ABOUT.create(window));
		registerAsGlobal(ActionFactory.UNDO.create(window));
		registerAsGlobal(ActionFactory.REDO.create(window));
		registerAsGlobal(ActionFactory.CUT.create(window));
		registerAsGlobal(ActionFactory.COPY.create(window));
		registerAsGlobal(ActionFactory.PASTE.create(window));
		registerAsGlobal(ActionFactory.SELECT_ALL.create(window));
		registerAsGlobal(ActionFactory.FIND.create(window));
		registerAsGlobal(ActionFactory.REVERT.create(window));
		//registerAsGlobal(ActionFactory.QUIT.create(window));
		registerAsGlobal(ActionFactory.REFRESH.create(window));
		//--
		EXIT.initialize(window);
		RESET.initialize(window);
		SCRATCH.initialize(window);
		SIMULATE.initialize(window);
		DOWNLOAD.initialize(window);
		//--
		registerAsGlobal(EXIT);
		registerAsGlobal(RESET);
		registerAsGlobal(SCRATCH);
		registerAsGlobal(SIMULATE);
		registerAsGlobal(DOWNLOAD);
	}

	/**
	 * Creates and returns the 'File' menu.
	 */
	private MenuManager createFileMenu() {
		MenuManager menu = new MenuManager("&File", IWorkbenchActionConstants.M_FILE);
		menu.add(getAction(ActionFactory.REFRESH.getId()));
		menu.add(new Separator());
		menu.add(EXIT);
		return menu;
	}

	/**
	 * Creates and returns the 'Edit' menu.
	 */
	private MenuManager createEditMenu() {
		MenuManager menu = new MenuManager("&Edit", IWorkbenchActionConstants.M_EDIT);
		menu.add(getAction(ActionFactory.REVERT.getId()));
		menu.add(new Separator());
		menu.add(getAction(ActionFactory.UNDO.getId()));
		menu.add(getAction(ActionFactory.REDO.getId()));
		menu.add(new Separator());
		menu.add(getAction(ActionFactory.CUT.getId()));
		menu.add(getAction(ActionFactory.COPY.getId()));
		menu.add(getAction(ActionFactory.PASTE.getId()));
		menu.add(new Separator());
		menu.add(getAction(ActionFactory.SELECT_ALL.getId()));
		menu.add(new Separator());
		menu.add(getAction(ActionFactory.FIND.getId()));
		menu.add(new GroupMarker(IWorkbenchActionConstants.FIND_EXT));
		return menu;
	}

	/**
	 * Creates and returns the Demo menu.
	 */
	private MenuManager createDemoMenu() {
		MenuManager menu = new MenuManager("&Demo", "demo");
		menu.add(SCRATCH);
		menu.add(SIMULATE);
		menu.add(DOWNLOAD);
		menu.add(new Separator());
		menu.add(RESET);
		return menu;
	}

	/**
	 * Creates and returns the 'Help' menu.
	 */
	private MenuManager createHelpMenu() {
		MenuManager menu = new MenuManager("&Help", IWorkbenchActionConstants.M_HELP);
		menu.add(new ActionContributionItem(getAction(ActionFactory.PREFERENCES.getId())));
		menu.add(new Separator());
		menu.add(new ActionContributionItem(getAction(ActionFactory.ABOUT.getId())));
		return menu;
	}

	/**
	 * Registers the action as global action and registers it for disposal.
	 * 
	 * @param action
	 *            the action to register
	 */
	private void registerAsGlobal(IAction action) {
		getActionBarConfigurer().registerGlobalAction(action);
		register(action);
	}
}
