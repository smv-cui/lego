package demo;

import org.apache.log4j.Logger;
import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import demo.commons.LogManager;
import demo.views.ConsoleView;
import demo.views.HelpView;
import demo.views.SimulatorView;

public class DemoPerspective implements IPerspectiveFactory {
	private static final Logger LOG = LogManager.INSTANCE.getLogger(DemoWorkbenchAdvisor.class.getName());
	public static final String ID = "demo.perspective";

	public DemoPerspective() {}

	@Override
	public void createInitialLayout(IPageLayout layout) {
		if (LOG.isDebugEnabled())  LOG.debug("Call \"createInitialLayout\" method");
		layout.setFixed(true);
		layout.setEditorAreaVisible(true);

		String editorArea = layout.getEditorArea();

		layout.getViewLayout(editorArea).setCloseable(false);
		layout.getViewLayout(editorArea).setMoveable(false);
		
		layout.addStandaloneView(HelpView.ID, false, IPageLayout.TOP, 0.5f, editorArea);
		layout.getViewLayout(HelpView.ID).setCloseable(false);
		layout.getViewLayout(HelpView.ID).setMoveable(false);

		IFolderLayout folder = layout.createFolder("demo.folder.editors", IPageLayout.RIGHT, 0.5f, editorArea);
		for (String id : new String[] {
				SimulatorView.ID,
				ConsoleView.ID,
				//"org.eclipse.ui.console.ConsoleView"
				//IPageLayout.ID_PROBLEM_VIEW,
				//IPageLayout.ID_PROGRESS_VIEW
		}) {
			folder.addView(id);
			layout.getViewLayout(id).setCloseable(false);
			layout.getViewLayout(id).setMoveable(false);
		}
	}
}
