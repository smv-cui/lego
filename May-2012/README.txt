Install:
========

The three sources folders:

- 'config' contains configurations files to setup properly persistent environment variables
- 'demo' contains all the files that will be added to the generated application
- 'demo_project' contains the eclipse (Java) application projects
- 'scratch_projects' contains some scratch projects done for testing purposes


External software:
==================

- leJOS EV3 (tested with 0.8.1)
- Eclipse 3.7 (Indigo) (sic!)
- Xtend and Xtext 2.2.1 (the version is important!)

The following are now obsolete
- leJOS NXJ (tested with 0.8.5) + fantom driver (tested with 1.1.3)
- Scratch (tested with 1.4)


Configuration:
==============

Take a look to the 'config' folder for configuration scripts.
On Mac OS X use the /etc/launchd.conf file.

For detailled instructions concerning leJOS see:
http://lejos.sourceforge.net/nxt/nxj/tutorial/Preliminaries/GettingStarted.htm

The demo needs these system variables to be setup:

- SCRATCH_INSTALL_PATH: the folder you installed Scratch into.
  Otherwise Scratch will not be enabled.

- DEMO_INSTALL_PATH: the folder you installed this demonstation into.

- NXJ_HOME: the folder you installed leJOS NXJ into
  Otherwise upload to NXT will not be enabled.

- PATH:	prepend the following folders
    	* leJOS's bin folder
    	* python's bin folder

Note:
- if you get the error: 'Failed to load USB comms driver: Cannot load USB driver'
  one way to correct this, is to add the '-d32' switch to the last line of the
  script named 'nxj' in '/Applications/lejos_nxj/bin'.
- if CLASSPATH and PATH are not set in '/etc/launchd.conf' you might be unable
  to upload the software to the NXT.

Eclipse plugins:
================

- xtext, xtend: http://download.itemis.de/updates
- lejos: http://lejos.sourceforge.net/tools/eclipse/plugin/nxj/

Export:
=======

Prior to export ensure that you added in the config ini of your eclipse:
-Dfile.encoding=UTF-8

And inside your eclipse:
Window -> Preferences... -> General -> Workspace -> Text file encoding -> Other -> UTF-8

Once the application is generated merge the content of the 'demo' folder.
