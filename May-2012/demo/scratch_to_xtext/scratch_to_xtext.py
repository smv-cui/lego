#coding=utf8
'''

Convert *.sb <--> *.scratch files in xtext format:
reads all the scripts in a Scratch project file and outputs a formatted code for Xtext.

    Usage: python scratch_to_xtext.py [ [name] [ext] [pathIn] [pathOut] ]?

'''

import os, sys

try:
    import kurt
except ImportError: # try and find kurt directory
    import os, sys
    path_to_file = os.path.join(os.getcwd(), __file__)
    path_to_lib = os.path.split(os.path.split(path_to_file)[0])[0]
    sys.path.append(path_to_lib)

from kurt.files import *
from kurt import ScratchSpriteMorph

default_name = 'demo'
default_ext  = 'sb'
default_path = os.path.abspath('../default')
# os.path.join('~',os.path.join('.DemoLegoNXT','demo'))

def xtext_plugin(project,name):
    string = 'project ' + name + '\n';
    for sprite in project.stage.sprites:
        if not isinstance(sprite, ScratchSpriteMorph):
            continue
        if not sprite.objName == "NXT":
            continue
        string += 'object ' + sprite.objName + '\n'
        scripts = sorted(sprite.scripts, key=lambda script: script.pos.y)
        for script in scripts:
            string += script.to_xtext_plugin(indent=0)
        string += '\n'
    return string

def run(name,ext,pathIn,pathOut):
    pIn  = os.path.expanduser(os.path.join(pathIn,(name+'.'+ext)))
    pOut = os.path.expanduser(os.path.join(pathOut,(name+'.scratch')))
    f = open(pOut,'w')
    if ext == 'sprite':
        f.write(xtext_plugin(ScratchSpriteFile(pIn),name))
    elif ext == 'sb':
        f.write(xtext_plugin(ScratchProjectFile(pIn),name))
    else:
        print __DOC__
    f.close()
    exit()

if __name__ == "__main__":
    if len(sys.argv) == 5:
        run(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    else:
        run(default_name,default_ext,default_path,default_path)
