@echo off

set PATH_PROJECT=~\.DemoLegoNXT\demo

REM Starts scratch in full screen
cd "%SCRATCH_INSTALL_PATH%"
Scratch.exe Scratch.image fullscreen "%PATH_PROJECT%\demo.sb"

REM Run python script to generate a '.scratch' file
cd "%DEMO_INSTALL_PATH%\scratch_to_xtext"
start /WAIT /B python scratch_to_xtext.py "demo" "sb" "%PATH_PROJECT%" "%PATH_PROJECT%"
