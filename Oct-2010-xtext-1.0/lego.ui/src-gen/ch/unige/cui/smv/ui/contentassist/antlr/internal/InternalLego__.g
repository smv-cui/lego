lexer grammar InternalLego;
@header {
package ch.unige.cui.smv.ui.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.Lexer;
}

T11 : 'avance de' ;
T12 : 'recule de' ;
T13 : 'tourne \u00E0 gauche de' ;
T14 : 'tourne \u00E0 droite de' ;
T15 : 'ferme la pince' ;
T16 : 'ouvre la pince' ;
T17 : 'tourne \u00E0 gauche' ;
T18 : 'tourne \u00E0 droite' ;
T19 : 'DEBUT' ;
T20 : 'FIN' ;
T21 : 'cm' ;
T22 : 'degres' ;

// $ANTLR src "../lego.ui/src-gen/ch/unige/cui/smv/ui/contentassist/antlr/internal/InternalLego.g" 839
RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

// $ANTLR src "../lego.ui/src-gen/ch/unige/cui/smv/ui/contentassist/antlr/internal/InternalLego.g" 841
RULE_INT : ('0'..'9')+;

// $ANTLR src "../lego.ui/src-gen/ch/unige/cui/smv/ui/contentassist/antlr/internal/InternalLego.g" 843
RULE_STRING : ('"' ('\\' ('b'|'t'|'n'|'f'|'r'|'"'|'\''|'\\')|~(('\\'|'"')))* '"'|'\'' ('\\' ('b'|'t'|'n'|'f'|'r'|'"'|'\''|'\\')|~(('\\'|'\'')))* '\'');

// $ANTLR src "../lego.ui/src-gen/ch/unige/cui/smv/ui/contentassist/antlr/internal/InternalLego.g" 845
RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

// $ANTLR src "../lego.ui/src-gen/ch/unige/cui/smv/ui/contentassist/antlr/internal/InternalLego.g" 847
RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

// $ANTLR src "../lego.ui/src-gen/ch/unige/cui/smv/ui/contentassist/antlr/internal/InternalLego.g" 849
RULE_WS : (' '|'\t'|'\r'|'\n')+;

// $ANTLR src "../lego.ui/src-gen/ch/unige/cui/smv/ui/contentassist/antlr/internal/InternalLego.g" 851
RULE_ANY_OTHER : .;


