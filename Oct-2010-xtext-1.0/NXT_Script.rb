#!/usr/local/bin/ruby

require 'find'

p "This script will copy the contents of your LEGO MINDSTORMS NXT disc to your desktop, make necessary adjustments to the installer for Mac OS X 10.6 and later, and then initiate the MINDSTORMS installer.  Please insert the MINDSTORMS NXT disc before you continue.  Would you like to continue? (Y/N)"
$name = gets.chomp

if $name[0,1].casecmp("Y") == 0
	$NXT_CD_Mounted = false
	Dir.entries("/Volumes").each do |path|
		if path == "MINDSTORMS NXT"
			$NXT_CD_Mounted = true
		end
	end
	
	if $NXT_CD_Mounted == true
		$whoAmI = `whoami`
		if $whoAmI[$whoAmI.length - 1, 1] == "\n"
			$currentUser = $whoAmI[0, $whoAmI.length - 1]
		else
			$currentUser = $whoAmI[0, $whoAmI.length]
		end

		$NXT_Desktop_Folder = "/Users/" + $currentUser + "/Desktop/MINDSTORMS\ NXT\ Desktop\ Copy"
		if File.exist?($NXT_Desktop_Folder)
			p "Removing 'MINDSTORMS NXT Desktop Copy' directory from desktop."
			$removeOld = `rm -rf ~/Desktop/"MINDSTORMS\ NXT\ Desktop\ Copy"`
		end
		p "Please wait while the contents of the LEGO MINDSTORMS NXT disc are copied to your desktop."
		$mkdirCmd = `mkdir ~/Desktop/"MINDSTORMS\ NXT\ Desktop\ Copy"`
		$copyCmd = `cp -R /Volumes/"MINDSTORMS\ NXT"/* ~/Desktop/"MINDSTORMS\ NXT\ Desktop\ Copy"/`

		#try edu and retail
		$retailFilePresent = false
		$retailFilePath = "/Users/" + $currentUser + "/Desktop/MINDSTORMS\ NXT\ Desktop\ Copy/Parts/MindstormsUnivRet.pkg/Contents/Resources/preflight"
		if File.exist?($retailFilePath)
			$retailFilePresent = true
		end
			
		$eduFilePresent = false
		$eduFilePath = "/Users/" + $currentUser + "/Desktop/MINDSTORMS\ NXT\ Desktop\ Copy/Parts/MindstormsUnivEdu.pkg/Contents/Resources/preflight"
		if File.exist?($eduFilePath)
			$eduFilePresent = true
		end

		$AutorunPath = "/Users/" + $currentUser + "/Desktop/MINDSTORMS\ NXT\ Desktop\ Copy/Install.app"

		#remove preflight file if it is present
		if $retailFilePresent or $eduFilePresent
			$removeCmd = `rm ~/Desktop/"MINDSTORMS\ NXT\ Desktop\ Copy"/Parts/MindstormsUniv[RE][ed][tu].pkg/Contents/Resources/preflight`
		end
		
		#run installer
		$LaunchCmd = `open -a ~/Desktop/"MINDSTORMS\ NXT\ Desktop\ Copy"/Install.app`
			
		p "The script has completed, please proceed with the NXT dialogs to install MINDSTORMS on your machine."
				
	else
		p "The MINDSTORMS NXT disc could not be found.  Please insert the MINDSTORMS NXT disc and try again."
	end
else
	p "Script aborted."
end
