/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ch.unige.cui.smv.legodsl.Model#getSet <em>Set</em>}</li>
 * </ul>
 * </p>
 *
 * @see ch.unige.cui.smv.legodsl.LegodslPackage#getModel()
 * @model
 * @generated
 */
public interface Model extends EObject
{
  /**
   * Returns the value of the '<em><b>Set</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Set</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Set</em>' containment reference.
   * @see #setSet(InstructionsSet)
   * @see ch.unige.cui.smv.legodsl.LegodslPackage#getModel_Set()
   * @model containment="true"
   * @generated
   */
  InstructionsSet getSet();

  /**
   * Sets the value of the '{@link ch.unige.cui.smv.legodsl.Model#getSet <em>Set</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Set</em>' containment reference.
   * @see #getSet()
   * @generated
   */
  void setSet(InstructionsSet value);

} // Model
