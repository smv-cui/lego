/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instruction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ch.unige.cui.smv.legodsl.LegodslPackage#getInstruction()
 * @model
 * @generated
 */
public interface Instruction extends EObject
{
} // Instruction
