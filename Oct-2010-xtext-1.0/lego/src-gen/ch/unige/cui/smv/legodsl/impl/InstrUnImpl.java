/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl.impl;

import ch.unige.cui.smv.legodsl.InstrUn;
import ch.unige.cui.smv.legodsl.LegodslPackage;
import ch.unige.cui.smv.legodsl.NomsUn;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instr Un</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link ch.unige.cui.smv.legodsl.impl.InstrUnImpl#getNom <em>Nom</em>}</li>
 *   <li>{@link ch.unige.cui.smv.legodsl.impl.InstrUnImpl#getDist <em>Dist</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class InstrUnImpl extends InstructionImpl implements InstrUn
{
  /**
   * The default value of the '{@link #getNom() <em>Nom</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNom()
   * @generated
   * @ordered
   */
  protected static final NomsUn NOM_EDEFAULT = NomsUn.AVANCE;

  /**
   * The cached value of the '{@link #getNom() <em>Nom</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNom()
   * @generated
   * @ordered
   */
  protected NomsUn nom = NOM_EDEFAULT;

  /**
   * The default value of the '{@link #getDist() <em>Dist</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDist()
   * @generated
   * @ordered
   */
  protected static final int DIST_EDEFAULT = 0;

  /**
   * The cached value of the '{@link #getDist() <em>Dist</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDist()
   * @generated
   * @ordered
   */
  protected int dist = DIST_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected InstrUnImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return LegodslPackage.Literals.INSTR_UN;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NomsUn getNom()
  {
    return nom;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNom(NomsUn newNom)
  {
    NomsUn oldNom = nom;
    nom = newNom == null ? NOM_EDEFAULT : newNom;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LegodslPackage.INSTR_UN__NOM, oldNom, nom));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getDist()
  {
    return dist;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDist(int newDist)
  {
    int oldDist = dist;
    dist = newDist;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, LegodslPackage.INSTR_UN__DIST, oldDist, dist));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case LegodslPackage.INSTR_UN__NOM:
        return getNom();
      case LegodslPackage.INSTR_UN__DIST:
        return getDist();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case LegodslPackage.INSTR_UN__NOM:
        setNom((NomsUn)newValue);
        return;
      case LegodslPackage.INSTR_UN__DIST:
        setDist((Integer)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case LegodslPackage.INSTR_UN__NOM:
        setNom(NOM_EDEFAULT);
        return;
      case LegodslPackage.INSTR_UN__DIST:
        setDist(DIST_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case LegodslPackage.INSTR_UN__NOM:
        return nom != NOM_EDEFAULT;
      case LegodslPackage.INSTR_UN__DIST:
        return dist != DIST_EDEFAULT;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (nom: ");
    result.append(nom);
    result.append(", dist: ");
    result.append(dist);
    result.append(')');
    return result.toString();
  }

} //InstrUnImpl
