/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl.impl;

import ch.unige.cui.smv.legodsl.InstrDeg;
import ch.unige.cui.smv.legodsl.InstrUn;
import ch.unige.cui.smv.legodsl.InstrZero;
import ch.unige.cui.smv.legodsl.Instruction;
import ch.unige.cui.smv.legodsl.InstructionsSet;
import ch.unige.cui.smv.legodsl.LegodslFactory;
import ch.unige.cui.smv.legodsl.LegodslPackage;
import ch.unige.cui.smv.legodsl.Model;
import ch.unige.cui.smv.legodsl.NomsDeg;
import ch.unige.cui.smv.legodsl.NomsUn;
import ch.unige.cui.smv.legodsl.NomsZero;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class LegodslPackageImpl extends EPackageImpl implements LegodslPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass modelEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass instructionsSetEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass instrUnEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass instrDegEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass instrZeroEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass instructionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum nomsUnEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum nomsDegEEnum = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EEnum nomsZeroEEnum = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see ch.unige.cui.smv.legodsl.LegodslPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private LegodslPackageImpl()
  {
    super(eNS_URI, LegodslFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link LegodslPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static LegodslPackage init()
  {
    if (isInited) return (LegodslPackage)EPackage.Registry.INSTANCE.getEPackage(LegodslPackage.eNS_URI);

    // Obtain or create and register package
    LegodslPackageImpl theLegodslPackage = (LegodslPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof LegodslPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new LegodslPackageImpl());

    isInited = true;

    // Create package meta-data objects
    theLegodslPackage.createPackageContents();

    // Initialize created meta-data
    theLegodslPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theLegodslPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(LegodslPackage.eNS_URI, theLegodslPackage);
    return theLegodslPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getModel()
  {
    return modelEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getModel_Set()
  {
    return (EReference)modelEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInstructionsSet()
  {
    return instructionsSetEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getInstructionsSet_Name()
  {
    return (EAttribute)instructionsSetEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getInstructionsSet_Instructions()
  {
    return (EReference)instructionsSetEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInstrUn()
  {
    return instrUnEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getInstrUn_Nom()
  {
    return (EAttribute)instrUnEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getInstrUn_Dist()
  {
    return (EAttribute)instrUnEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInstrDeg()
  {
    return instrDegEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getInstrDeg_Nom()
  {
    return (EAttribute)instrDegEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getInstrDeg_Angle()
  {
    return (EAttribute)instrDegEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInstrZero()
  {
    return instrZeroEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getInstrZero_Nom()
  {
    return (EAttribute)instrZeroEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInstruction()
  {
    return instructionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getNomsUn()
  {
    return nomsUnEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getNomsDeg()
  {
    return nomsDegEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EEnum getNomsZero()
  {
    return nomsZeroEEnum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LegodslFactory getLegodslFactory()
  {
    return (LegodslFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    modelEClass = createEClass(MODEL);
    createEReference(modelEClass, MODEL__SET);

    instructionsSetEClass = createEClass(INSTRUCTIONS_SET);
    createEAttribute(instructionsSetEClass, INSTRUCTIONS_SET__NAME);
    createEReference(instructionsSetEClass, INSTRUCTIONS_SET__INSTRUCTIONS);

    instrUnEClass = createEClass(INSTR_UN);
    createEAttribute(instrUnEClass, INSTR_UN__NOM);
    createEAttribute(instrUnEClass, INSTR_UN__DIST);

    instrDegEClass = createEClass(INSTR_DEG);
    createEAttribute(instrDegEClass, INSTR_DEG__NOM);
    createEAttribute(instrDegEClass, INSTR_DEG__ANGLE);

    instrZeroEClass = createEClass(INSTR_ZERO);
    createEAttribute(instrZeroEClass, INSTR_ZERO__NOM);

    instructionEClass = createEClass(INSTRUCTION);

    // Create enums
    nomsUnEEnum = createEEnum(NOMS_UN);
    nomsDegEEnum = createEEnum(NOMS_DEG);
    nomsZeroEEnum = createEEnum(NOMS_ZERO);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    instrUnEClass.getESuperTypes().add(this.getInstruction());
    instrDegEClass.getESuperTypes().add(this.getInstruction());
    instrZeroEClass.getESuperTypes().add(this.getInstruction());

    // Initialize classes and features; add operations and parameters
    initEClass(modelEClass, Model.class, "Model", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getModel_Set(), this.getInstructionsSet(), null, "set", null, 0, 1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(instructionsSetEClass, InstructionsSet.class, "InstructionsSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getInstructionsSet_Name(), ecorePackage.getEString(), "name", null, 0, 1, InstructionsSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getInstructionsSet_Instructions(), this.getInstruction(), null, "instructions", null, 0, -1, InstructionsSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(instrUnEClass, InstrUn.class, "InstrUn", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getInstrUn_Nom(), this.getNomsUn(), "nom", null, 0, 1, InstrUn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getInstrUn_Dist(), ecorePackage.getEInt(), "dist", null, 0, 1, InstrUn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(instrDegEClass, InstrDeg.class, "InstrDeg", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getInstrDeg_Nom(), this.getNomsDeg(), "nom", null, 0, 1, InstrDeg.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getInstrDeg_Angle(), ecorePackage.getEInt(), "angle", null, 0, 1, InstrDeg.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(instrZeroEClass, InstrZero.class, "InstrZero", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getInstrZero_Nom(), this.getNomsZero(), "nom", null, 0, 1, InstrZero.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(instructionEClass, Instruction.class, "Instruction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    // Initialize enums and add enum literals
    initEEnum(nomsUnEEnum, NomsUn.class, "NomsUn");
    addEEnumLiteral(nomsUnEEnum, NomsUn.AVANCE);
    addEEnumLiteral(nomsUnEEnum, NomsUn.RECULE);

    initEEnum(nomsDegEEnum, NomsDeg.class, "NomsDeg");
    addEEnumLiteral(nomsDegEEnum, NomsDeg.TOURNEG);
    addEEnumLiteral(nomsDegEEnum, NomsDeg.TOURNED);

    initEEnum(nomsZeroEEnum, NomsZero.class, "NomsZero");
    addEEnumLiteral(nomsZeroEEnum, NomsZero.FERME);
    addEEnumLiteral(nomsZeroEEnum, NomsZero.OUVRE);
    addEEnumLiteral(nomsZeroEEnum, NomsZero.TOURNEG);
    addEEnumLiteral(nomsZeroEEnum, NomsZero.TOURNED);

    // Create resource
    createResource(eNS_URI);
  }

} //LegodslPackageImpl
