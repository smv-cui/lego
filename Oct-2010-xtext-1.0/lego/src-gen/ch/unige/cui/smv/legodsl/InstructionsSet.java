/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instructions Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ch.unige.cui.smv.legodsl.InstructionsSet#getName <em>Name</em>}</li>
 *   <li>{@link ch.unige.cui.smv.legodsl.InstructionsSet#getInstructions <em>Instructions</em>}</li>
 * </ul>
 * </p>
 *
 * @see ch.unige.cui.smv.legodsl.LegodslPackage#getInstructionsSet()
 * @model
 * @generated
 */
public interface InstructionsSet extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see ch.unige.cui.smv.legodsl.LegodslPackage#getInstructionsSet_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link ch.unige.cui.smv.legodsl.InstructionsSet#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Instructions</b></em>' containment reference list.
   * The list contents are of type {@link ch.unige.cui.smv.legodsl.Instruction}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Instructions</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Instructions</em>' containment reference list.
   * @see ch.unige.cui.smv.legodsl.LegodslPackage#getInstructionsSet_Instructions()
   * @model containment="true"
   * @generated
   */
  EList<Instruction> getInstructions();

} // InstructionsSet
