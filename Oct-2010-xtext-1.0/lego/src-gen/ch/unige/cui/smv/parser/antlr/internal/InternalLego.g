/*
* generated by Xtext
*/
grammar InternalLego;

options {
	superClass=AbstractInternalAntlrParser;
	
}

@lexer::header {
package ch.unige.cui.smv.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;
}

@parser::header {
package ch.unige.cui.smv.parser.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.xtext.parsetree.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.eclipse.xtext.conversion.ValueConverterException;
import ch.unige.cui.smv.services.LegoGrammarAccess;

}

@parser::members {

 	private LegoGrammarAccess grammarAccess;
 	
    public InternalLegoParser(TokenStream input, IAstFactory factory, LegoGrammarAccess grammarAccess) {
        this(input);
        this.factory = factory;
        registerRules(grammarAccess.getGrammar());
        this.grammarAccess = grammarAccess;
    }
    
    @Override
    protected InputStream getTokenFile() {
    	ClassLoader classLoader = getClass().getClassLoader();
    	return classLoader.getResourceAsStream("ch/unige/cui/smv/parser/antlr/internal/InternalLego.tokens");
    }
    
    @Override
    protected String getFirstRuleName() {
    	return "Model";	
   	}
   	
   	@Override
   	protected LegoGrammarAccess getGrammarAccess() {
   		return grammarAccess;
   	}
}

@rulecatch { 
    catch (RecognitionException re) { 
        recover(input,re); 
        appendSkippedTokens();
    } 
}




// Entry rule entryRuleModel
entryRuleModel returns [EObject current=null] 
	:
	{ currentNode = createCompositeNode(grammarAccess.getModelRule(), currentNode); }
	 iv_ruleModel=ruleModel 
	 { $current=$iv_ruleModel.current; } 
	 EOF 
;

// Rule Model
ruleModel returns [EObject current=null] 
    @init { EObject temp=null; setCurrentLookahead(); resetLookahead(); 
    }
    @after { resetLookahead(); 
    	lastConsumedNode = currentNode;
    }:
(
(
		{ 
	        currentNode=createCompositeNode(grammarAccess.getModelAccess().getSetInstructionsSetParserRuleCall_0(), currentNode); 
	    }
		lv_set_0_0=ruleInstructionsSet		{
	        if ($current==null) {
	            $current = factory.create(grammarAccess.getModelRule().getType().getClassifier());
	            associateNodeWithAstElement(currentNode.getParent(), $current);
	        }
	        try {
	       		set(
	       			$current, 
	       			"set",
	        		lv_set_0_0, 
	        		"InstructionsSet", 
	        		currentNode);
	        } catch (ValueConverterException vce) {
				handleValueConverterException(vce);
	        }
	        currentNode = currentNode.getParent();
	    }

)
)
;





// Entry rule entryRuleInstructionsSet
entryRuleInstructionsSet returns [EObject current=null] 
	:
	{ currentNode = createCompositeNode(grammarAccess.getInstructionsSetRule(), currentNode); }
	 iv_ruleInstructionsSet=ruleInstructionsSet 
	 { $current=$iv_ruleInstructionsSet.current; } 
	 EOF 
;

// Rule InstructionsSet
ruleInstructionsSet returns [EObject current=null] 
    @init { EObject temp=null; setCurrentLookahead(); resetLookahead(); 
    }
    @after { resetLookahead(); 
    	lastConsumedNode = currentNode;
    }:
((
(
		lv_name_0_0=RULE_ID
		{
			createLeafNode(grammarAccess.getInstructionsSetAccess().getNameIDTerminalRuleCall_0_0(), "name"); 
		}
		{
	        if ($current==null) {
	            $current = factory.create(grammarAccess.getInstructionsSetRule().getType().getClassifier());
	            associateNodeWithAstElement(currentNode, $current);
	        }
	        try {
	       		set(
	       			$current, 
	       			"name",
	        		lv_name_0_0, 
	        		"ID", 
	        		lastConsumedNode);
	        } catch (ValueConverterException vce) {
				handleValueConverterException(vce);
	        }
	    }

)
)	'DEBUT' 
    {
        createLeafNode(grammarAccess.getInstructionsSetAccess().getDEBUTKeyword_1(), null); 
    }
(
(
		{ 
	        currentNode=createCompositeNode(grammarAccess.getInstructionsSetAccess().getInstructionsInstructionParserRuleCall_2_0(), currentNode); 
	    }
		lv_instructions_2_0=ruleInstruction		{
	        if ($current==null) {
	            $current = factory.create(grammarAccess.getInstructionsSetRule().getType().getClassifier());
	            associateNodeWithAstElement(currentNode.getParent(), $current);
	        }
	        try {
	       		add(
	       			$current, 
	       			"instructions",
	        		lv_instructions_2_0, 
	        		"Instruction", 
	        		currentNode);
	        } catch (ValueConverterException vce) {
				handleValueConverterException(vce);
	        }
	        currentNode = currentNode.getParent();
	    }

)
)*	'FIN' 
    {
        createLeafNode(grammarAccess.getInstructionsSetAccess().getFINKeyword_3(), null); 
    }
)
;





// Entry rule entryRuleInstrUn
entryRuleInstrUn returns [EObject current=null] 
	:
	{ currentNode = createCompositeNode(grammarAccess.getInstrUnRule(), currentNode); }
	 iv_ruleInstrUn=ruleInstrUn 
	 { $current=$iv_ruleInstrUn.current; } 
	 EOF 
;

// Rule InstrUn
ruleInstrUn returns [EObject current=null] 
    @init { EObject temp=null; setCurrentLookahead(); resetLookahead(); 
    }
    @after { resetLookahead(); 
    	lastConsumedNode = currentNode;
    }:
((
(
		{ 
	        currentNode=createCompositeNode(grammarAccess.getInstrUnAccess().getNomNomsUnEnumRuleCall_0_0(), currentNode); 
	    }
		lv_nom_0_0=ruleNomsUn		{
	        if ($current==null) {
	            $current = factory.create(grammarAccess.getInstrUnRule().getType().getClassifier());
	            associateNodeWithAstElement(currentNode.getParent(), $current);
	        }
	        try {
	       		set(
	       			$current, 
	       			"nom",
	        		lv_nom_0_0, 
	        		"NomsUn", 
	        		currentNode);
	        } catch (ValueConverterException vce) {
				handleValueConverterException(vce);
	        }
	        currentNode = currentNode.getParent();
	    }

)
)(
(
		lv_dist_1_0=RULE_INT
		{
			createLeafNode(grammarAccess.getInstrUnAccess().getDistINTTerminalRuleCall_1_0(), "dist"); 
		}
		{
	        if ($current==null) {
	            $current = factory.create(grammarAccess.getInstrUnRule().getType().getClassifier());
	            associateNodeWithAstElement(currentNode, $current);
	        }
	        try {
	       		set(
	       			$current, 
	       			"dist",
	        		lv_dist_1_0, 
	        		"INT", 
	        		lastConsumedNode);
	        } catch (ValueConverterException vce) {
				handleValueConverterException(vce);
	        }
	    }

)
)	'cm' 
    {
        createLeafNode(grammarAccess.getInstrUnAccess().getCmKeyword_2(), null); 
    }
)
;





// Entry rule entryRuleInstrDeg
entryRuleInstrDeg returns [EObject current=null] 
	:
	{ currentNode = createCompositeNode(grammarAccess.getInstrDegRule(), currentNode); }
	 iv_ruleInstrDeg=ruleInstrDeg 
	 { $current=$iv_ruleInstrDeg.current; } 
	 EOF 
;

// Rule InstrDeg
ruleInstrDeg returns [EObject current=null] 
    @init { EObject temp=null; setCurrentLookahead(); resetLookahead(); 
    }
    @after { resetLookahead(); 
    	lastConsumedNode = currentNode;
    }:
((
(
		{ 
	        currentNode=createCompositeNode(grammarAccess.getInstrDegAccess().getNomNomsDegEnumRuleCall_0_0(), currentNode); 
	    }
		lv_nom_0_0=ruleNomsDeg		{
	        if ($current==null) {
	            $current = factory.create(grammarAccess.getInstrDegRule().getType().getClassifier());
	            associateNodeWithAstElement(currentNode.getParent(), $current);
	        }
	        try {
	       		set(
	       			$current, 
	       			"nom",
	        		lv_nom_0_0, 
	        		"NomsDeg", 
	        		currentNode);
	        } catch (ValueConverterException vce) {
				handleValueConverterException(vce);
	        }
	        currentNode = currentNode.getParent();
	    }

)
)(
(
		lv_angle_1_0=RULE_INT
		{
			createLeafNode(grammarAccess.getInstrDegAccess().getAngleINTTerminalRuleCall_1_0(), "angle"); 
		}
		{
	        if ($current==null) {
	            $current = factory.create(grammarAccess.getInstrDegRule().getType().getClassifier());
	            associateNodeWithAstElement(currentNode, $current);
	        }
	        try {
	       		set(
	       			$current, 
	       			"angle",
	        		lv_angle_1_0, 
	        		"INT", 
	        		lastConsumedNode);
	        } catch (ValueConverterException vce) {
				handleValueConverterException(vce);
	        }
	    }

)
)	'degres' 
    {
        createLeafNode(grammarAccess.getInstrDegAccess().getDegresKeyword_2(), null); 
    }
)
;





// Entry rule entryRuleInstrZero
entryRuleInstrZero returns [EObject current=null] 
	:
	{ currentNode = createCompositeNode(grammarAccess.getInstrZeroRule(), currentNode); }
	 iv_ruleInstrZero=ruleInstrZero 
	 { $current=$iv_ruleInstrZero.current; } 
	 EOF 
;

// Rule InstrZero
ruleInstrZero returns [EObject current=null] 
    @init { EObject temp=null; setCurrentLookahead(); resetLookahead(); 
    }
    @after { resetLookahead(); 
    	lastConsumedNode = currentNode;
    }:
(
(
		{ 
	        currentNode=createCompositeNode(grammarAccess.getInstrZeroAccess().getNomNomsZeroEnumRuleCall_0(), currentNode); 
	    }
		lv_nom_0_0=ruleNomsZero		{
	        if ($current==null) {
	            $current = factory.create(grammarAccess.getInstrZeroRule().getType().getClassifier());
	            associateNodeWithAstElement(currentNode.getParent(), $current);
	        }
	        try {
	       		set(
	       			$current, 
	       			"nom",
	        		lv_nom_0_0, 
	        		"NomsZero", 
	        		currentNode);
	        } catch (ValueConverterException vce) {
				handleValueConverterException(vce);
	        }
	        currentNode = currentNode.getParent();
	    }

)
)
;





// Entry rule entryRuleInstruction
entryRuleInstruction returns [EObject current=null] 
	:
	{ currentNode = createCompositeNode(grammarAccess.getInstructionRule(), currentNode); }
	 iv_ruleInstruction=ruleInstruction 
	 { $current=$iv_ruleInstruction.current; } 
	 EOF 
;

// Rule Instruction
ruleInstruction returns [EObject current=null] 
    @init { EObject temp=null; setCurrentLookahead(); resetLookahead(); 
    }
    @after { resetLookahead(); 
    	lastConsumedNode = currentNode;
    }:
(
    { 
        currentNode=createCompositeNode(grammarAccess.getInstructionAccess().getInstrUnParserRuleCall_0(), currentNode); 
    }
    this_InstrUn_0=ruleInstrUn
    { 
        $current = $this_InstrUn_0.current; 
        currentNode = currentNode.getParent();
    }

    |
    { 
        currentNode=createCompositeNode(grammarAccess.getInstructionAccess().getInstrZeroParserRuleCall_1(), currentNode); 
    }
    this_InstrZero_1=ruleInstrZero
    { 
        $current = $this_InstrZero_1.current; 
        currentNode = currentNode.getParent();
    }

    |
    { 
        currentNode=createCompositeNode(grammarAccess.getInstructionAccess().getInstrDegParserRuleCall_2(), currentNode); 
    }
    this_InstrDeg_2=ruleInstrDeg
    { 
        $current = $this_InstrDeg_2.current; 
        currentNode = currentNode.getParent();
    }
)
;





// Rule NomsUn
ruleNomsUn returns [Enumerator current=null] 
    @init { setCurrentLookahead(); resetLookahead(); }
    @after { resetLookahead(); 
    	lastConsumedNode = currentNode;
    }:
((	'avance de' 
	{
        $current = grammarAccess.getNomsUnAccess().getAvanceEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
        createLeafNode(grammarAccess.getNomsUnAccess().getAvanceEnumLiteralDeclaration_0(), null); 
    }
)
    |(	'recule de' 
	{
        $current = grammarAccess.getNomsUnAccess().getReculeEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
        createLeafNode(grammarAccess.getNomsUnAccess().getReculeEnumLiteralDeclaration_1(), null); 
    }
));



// Rule NomsDeg
ruleNomsDeg returns [Enumerator current=null] 
    @init { setCurrentLookahead(); resetLookahead(); }
    @after { resetLookahead(); 
    	lastConsumedNode = currentNode;
    }:
((	'tourne \u00E0 gauche de' 
	{
        $current = grammarAccess.getNomsDegAccess().getTournegEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
        createLeafNode(grammarAccess.getNomsDegAccess().getTournegEnumLiteralDeclaration_0(), null); 
    }
)
    |(	'tourne \u00E0 droite de' 
	{
        $current = grammarAccess.getNomsDegAccess().getTournedEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
        createLeafNode(grammarAccess.getNomsDegAccess().getTournedEnumLiteralDeclaration_1(), null); 
    }
));



// Rule NomsZero
ruleNomsZero returns [Enumerator current=null] 
    @init { setCurrentLookahead(); resetLookahead(); }
    @after { resetLookahead(); 
    	lastConsumedNode = currentNode;
    }:
((	'ferme la pince' 
	{
        $current = grammarAccess.getNomsZeroAccess().getFermeEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
        createLeafNode(grammarAccess.getNomsZeroAccess().getFermeEnumLiteralDeclaration_0(), null); 
    }
)
    |(	'ouvre la pince' 
	{
        $current = grammarAccess.getNomsZeroAccess().getOuvreEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
        createLeafNode(grammarAccess.getNomsZeroAccess().getOuvreEnumLiteralDeclaration_1(), null); 
    }
)
    |(	'tourne \u00E0 gauche' 
	{
        $current = grammarAccess.getNomsZeroAccess().getTournegEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
        createLeafNode(grammarAccess.getNomsZeroAccess().getTournegEnumLiteralDeclaration_2(), null); 
    }
)
    |(	'tourne \u00E0 droite' 
	{
        $current = grammarAccess.getNomsZeroAccess().getTournedEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
        createLeafNode(grammarAccess.getNomsZeroAccess().getTournedEnumLiteralDeclaration_3(), null); 
    }
));



RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' ('b'|'t'|'n'|'f'|'r'|'"'|'\''|'\\')|~(('\\'|'"')))* '"'|'\'' ('\\' ('b'|'t'|'n'|'f'|'r'|'"'|'\''|'\\')|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;


