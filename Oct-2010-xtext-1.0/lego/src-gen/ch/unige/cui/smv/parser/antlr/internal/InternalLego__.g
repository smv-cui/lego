lexer grammar InternalLego;
@header {
package ch.unige.cui.smv.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;
}

T11 : 'DEBUT' ;
T12 : 'FIN' ;
T13 : 'cm' ;
T14 : 'degres' ;
T15 : 'avance de' ;
T16 : 'recule de' ;
T17 : 'tourne \u00E0 gauche de' ;
T18 : 'tourne \u00E0 droite de' ;
T19 : 'ferme la pince' ;
T20 : 'ouvre la pince' ;
T21 : 'tourne \u00E0 gauche' ;
T22 : 'tourne \u00E0 droite' ;

// $ANTLR src "../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g" 527
RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

// $ANTLR src "../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g" 529
RULE_INT : ('0'..'9')+;

// $ANTLR src "../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g" 531
RULE_STRING : ('"' ('\\' ('b'|'t'|'n'|'f'|'r'|'"'|'\''|'\\')|~(('\\'|'"')))* '"'|'\'' ('\\' ('b'|'t'|'n'|'f'|'r'|'"'|'\''|'\\')|~(('\\'|'\'')))* '\'');

// $ANTLR src "../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g" 533
RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

// $ANTLR src "../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g" 535
RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

// $ANTLR src "../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g" 537
RULE_WS : (' '|'\t'|'\r'|'\n')+;

// $ANTLR src "../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g" 539
RULE_ANY_OTHER : .;


