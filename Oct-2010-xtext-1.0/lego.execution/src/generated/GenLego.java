package generated;
import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

public class GenLego {
	public static void main(String[] args) {
		System.out.println("Execution...");
		Pilot pilot = new TachoPilot(5.6f, 12f, Motor.C, Motor.B, true);
		pilot.setMoveSpeed(20);
 		Motor.A.setSpeed(40);
 		boolean closed = false;

		try{ 
  			Thread.sleep(1000); 
  		}
		catch(Exception e) {}
		//Tourner à gauche
		pilot.rotate(-90);
		pause();
		//Bouger
		pilot.travel(-60);
		pause();
		//Tourner à droite
		pilot.rotate(90);
		pause();
		//Bouger
		pilot.travel(-15);
		pause();
		//Fermer la pince
		if (!closed) { 
			Motor.A.rotate(140);
			closed = true;
		}
		pause();
		//Bouger
		pilot.travel(20);
		pause();
		//Tourner à droite
		pilot.rotate(90);
		pause();
		//Bouger
		pilot.travel(-70);
		pause();
		//Tourner à gauche
		pilot.rotate(-90);
		pause();
		//Bouger
		pilot.travel(-75);
		pause();
		//Tourner à gauche
		pilot.rotate(-90);
		pause();
		//Bouger
		pilot.travel(-15);
		pause();
		//Ouvrir la pince
		if (closed) {
			Motor.A.rotate(-140);
			closed = false;
		}
		pause();
 		
 		pilot.stop();
		System.out.println("Fin Programme");
 		Button.waitForPress();
  	}
  	
  	private static void pause() {
  		try{ 
  			Thread.sleep(500); 
  		}
		catch(Exception e) {}
	}
}
