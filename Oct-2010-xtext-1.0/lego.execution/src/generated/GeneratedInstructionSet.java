package generated;
import ch.unige.cui.smv.util.InstructionsSet;
import ch.unige.cui.smv.util.Instruction;
      
public class GeneratedInstructionSet extends InstructionsSet {
	private static final long serialVersionUID = 4432546480076674946L;
  	@Override
	public void build() {
		addInstruction(new Instruction(Instruction.InstructionNames.turnleft,90
));
		addInstruction(new Instruction(Instruction.InstructionNames.advance
,60));
		addInstruction(new Instruction(Instruction.InstructionNames.turnright,90
));
		addInstruction(new Instruction(Instruction.InstructionNames.advance
,15));
		addInstruction(new Instruction(Instruction.InstructionNames.closeclaw
));
		addInstruction(new Instruction(Instruction.InstructionNames.goback
,20));
		addInstruction(new Instruction(Instruction.InstructionNames.turnright,90
));
		addInstruction(new Instruction(Instruction.InstructionNames.advance
,70));
		addInstruction(new Instruction(Instruction.InstructionNames.turnleft,90
));
		addInstruction(new Instruction(Instruction.InstructionNames.advance
,75));
		addInstruction(new Instruction(Instruction.InstructionNames.turnleft,90
));
		addInstruction(new Instruction(Instruction.InstructionNames.advance
,15));
		addInstruction(new Instruction(Instruction.InstructionNames.openclaw
));
  	}
}
