package ch.unige.cui.smv.simulator;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Vector;

import javax.swing.JPanel;

public class FieldDrawer extends JPanel {

    private static final long serialVersionUID = -4032612404757869977L;
    private Vector<Area> walls=new Vector<Area>();
	private Rectangle field;
	private Vector<Area> robots=new Vector<Area>();
	private Vector<Area> movements=new Vector<Area>();
	private Vector<Line2D.Double> grid=new Vector<Line2D.Double>();
	private Point2D.Double ball;
	private int ballDiameter;
	private Point2D.Double goal;
	private int goalWidth;
	private Area clawArea;

	/*The following is used for debugging. Might still be useful...*/
	private Vector<Point2D.Double> endpoints=new Vector<Point2D.Double>();
	private Point2D.Double recoriginpoint;
	private Vector<Point2D.Double> rotationpoints=new Vector<Point2D.Double>();


	public void paint(Graphics g){
		clear(g);
		Graphics2D g2d=(Graphics2D)g;
		//draw the field
		g2d.draw(field);
		//draw the grid
		g2d.setColor(Color.LIGHT_GRAY);
		for(Line2D.Double l : grid){
			g2d.draw(l);
		}
		//draw movement area
		g2d.setColor(Color.CYAN);
		for(Area m : movements){
			g2d.fill(m);
			g2d.draw(m);
		}		
		//draw the goal
		g2d.setColor(Color.LIGHT_GRAY);
		g2d.fill(new Rectangle2D.Double(goal.x-goalWidth/2, goal.y-goalWidth/2, goalWidth, goalWidth));
		//draw the walls
		g2d.setColor(Color.DARK_GRAY);
		for(Area w : walls){
			g2d.fill(w);			
		}

		//draw endpoints calculation (used for debugging)
		g2d.setColor(Color.MAGENTA);
		for(Point2D.Double endpoint:endpoints){
			g2d.draw(new Ellipse2D.Double(endpoint.x,endpoint.y,1,1));
			g2d.drawString(endpoint.x+":"+endpoint.y, (new Double(endpoint.x).intValue())+5, (new Double(endpoint.y).intValue())+5);
		}		
		//draw origins of rectangles  (used for debugging)
		g2d.setColor(Color.BLACK);
		if(recoriginpoint!=null) {
			g2d.draw(new Ellipse2D.Double(recoriginpoint.x,recoriginpoint.y,1,1));
			g2d.drawString(recoriginpoint.x+":"+recoriginpoint.y, (new Double(recoriginpoint.x).intValue())+5, (new Double(recoriginpoint.y).intValue())+5);
		}
		//draw rotation points (used for debugging)
		g2d.setColor(Color.BLUE);
		for(Point2D.Double rotationpoint:rotationpoints){
			g2d.draw(new Ellipse2D.Double(rotationpoint.x,rotationpoint.y,2,2));
			//g2d.drawString(rotationpoint.x+":"+rotationpoint.y, (new Double(rotationpoint.x).intValue())+5, (new Double(rotationpoint.y).intValue())+5);
		}
		//draw robot (last position with thicker line)		
		for(int i=0;i<robots.size();i++){
			if(i==robots.size()-1) g2d.setStroke(new BasicStroke(2));
			g2d.draw(robots.get(i));
		}
		//draw claw area
		g2d.setColor(Color.GREEN);
		g2d.fill(clawArea);
		//draw the ball
		if(ball!=null){
			g2d.setColor(Color.RED);		
			g2d.fill(new Ellipse2D.Double(ball.x-ballDiameter/2,ball.y-ballDiameter/2,ballDiameter,ballDiameter));
		}
	}

	// super.paintComponent clears offscreen pixmap,
	// since we're using double buffering by default.
	protected void clear(Graphics g) {
		super.paintComponent(g);
	}

	public Vector<Area> getWalls() {
		return walls;
	}

	public void setWalls(Vector<Area> walls) {
		this.walls = walls;
	}

	public Rectangle getField() {
		return field;
	}

	public void setField(Rectangle field) {
		this.field = field;
	}

	public Vector<Area> getRobots() {
		return robots;
	}

	public void setRobots(Vector<Area> robot) {
		this.robots = robot;
	}

	public void addRobot(Area robot){
		this.robots.add(robot);
	}

	public void addMovement(Area movement){
		this.movements.add(movement);
	}



	public void addEndpoint(Point2D.Double endpoint) {
		this.endpoints.add(endpoint);
	}

	public FieldDrawer(Gamefield gameField){
		for (int i=10;i<=gameField.getFieldWidth();i+=10){
			grid.add(new Line2D.Double(i,0,i,gameField.getFieldHeight()));
		}
		for (int i=10;i<=gameField.getFieldHeight();i+=10){
			grid.add(new Line2D.Double(0,i,gameField.getFieldWidth(),i));
		}
	}

	public Point2D.Double getRecoriginpoint() {
		return recoriginpoint;
	}

	public void setRecoriginpoint(Point2D.Double recoriginpoint) {
		this.recoriginpoint = recoriginpoint;
	}

	public void addRotationPoint(Point2D.Double p){
		rotationpoints.add(p);
	}

	public Point2D.Double getBall() {
		return ball;
	}

	public void setBall(Point2D.Double ball) {
		this.ball = ball;
	}

	public int getBallDiameter() {
		return ballDiameter;
	}

	public void setBallDiameter(int ballDiameter) {
		this.ballDiameter = ballDiameter;
	}

	public int getGoalWidth() {
		return goalWidth;
	}

	public void setGoalWidth(int goalWidth) {
		this.goalWidth = goalWidth;
	}

	public Point2D.Double getGoal() {
		return goal;
	}

	public void setGoal(Point2D.Double goal) {
		this.goal = goal;
	}

	public Area getClawArea() {
		return clawArea;
	}

	public void setClawArea(Area clawArea) {
		this.clawArea = clawArea;
	}

}
