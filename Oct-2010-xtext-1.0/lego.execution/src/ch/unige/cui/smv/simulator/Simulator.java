package ch.unige.cui.smv.simulator;

import generated.GeneratedInstructionSet;

import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import ch.unige.cui.smv.util.Instruction;
import ch.unige.cui.smv.util.InstructionsSet;


/**
 * @author risoldi
 * Simulator for Mindstorms robot game
 */
public class Simulator {

	private Gamefield field;
	private InstructionsSet set;
	private double currentRobotRotation;
	private Point2D.Double currentRobotPosition;
	private FieldDrawer drawer;	
	private int refresh=600;
	private boolean visual=false;
	private int clawPercent; //% of the robot length that is the area under the claw 
	private boolean holdingBall=false;
	private boolean clawClosed=false;
	private int rotationTolerance=1; // Subtracted by the rotation radius for near-wall turns. 
	// Needed because double numbers need approximation to avoid
	// false collision detection. Increase for more tolerance, but 1 should be enough.

	public double getCurrentRobotRotation() {
		return currentRobotRotation;
	}

	public Point2D.Double getCurrentRobotPosition() {
		return currentRobotPosition;
	}

	/**Initialize a simulator with a dummy game field. Without visualization.
	 * @param is Instructions to be tested
	 */
	public Simulator(InstructionsSet is){
		this(is, false);
	}

	/**Initialize a simulator with a dummy game field. Specify whether with visualization or not.
	 * @param is Instructions to be tested
	 * @param visual If true, the simulation is visualized. If false, it is not.
	 */
	public Simulator(InstructionsSet is, boolean visual){
		this(new Gamefield(),is, visual);
	}

	/**Initialize a simulator with a given game field. Without visualization.
	 * @param f Game field to use
	 * @param is Instructions to be tested
	 */
	public Simulator(Gamefield f, InstructionsSet is){
		this(f,is,false);
	}


	/**Initialize a simulator with a given game field. Specify whether with visualization or not.
	 * @param f Game field to use
	 * @param is Instructions to be tested
	 * @param visual If true, the simulation is visualized. If false, it is not.
	 */
	public Simulator(Gamefield f, InstructionsSet is, boolean visual){
		this.visual=visual;	
		field=f;
		set=is;
		defineStartingParameters();		
		if(visual){
			drawer=new FieldDrawer(field);
			
			drawer.setBall(field.getBallPosition());
			drawer.setBallDiameter(field.getBallDiameter());
			drawer.setGoalWidth(field.getGoalWidth());
			drawer.setGoal(field.getGoal());
			drawer.setWalls(getField().getWalls());
			drawer.addRobot(buildRobotRectangle(getCurrentRobotPosition(),getCurrentRobotRotation()));
			drawer.setField(getField().getField());
			drawer.setClawArea(buildClawArea(getCurrentRobotPosition(),getCurrentRobotRotation()));
			WindowUtilities.openInJFrame(drawer, 360, 360);
		}
	}



	/**
	 * Define initial position and orientation of robot
	 */
	private void defineStartingParameters() {
		currentRobotPosition=field.getRobotPositionStart();
		currentRobotRotation=field.getRobotRotationStart();	
		clawPercent=field.getClawPercent();
	}

	/**
	 * Run the simulation
	 * @return True if the goal is reached
	 * @throws SimulationException If a move collides with the walls, or goes out of the field.
	 * @throws InvalidInstructionException If an instruction is malformed.
	 * 
	 */
	public boolean run() throws SimulationException, InvalidInstructionException{	
		boolean goalReached=false;
		if(validCurrentPosition()){			
			int counter=1;
			for(Instruction i:set.getInstructions()){
				try {
					if(visual) Thread.sleep(refresh);
				} catch (InterruptedException e) {					
					e.printStackTrace();
				}
				switch (i.getName()){
				case advance: {
					if(i.getParameter()<0) throw new InvalidInstructionException("The instruction at line "+counter+" \""+i.getName()+"\" has an invalid parameter ("+i.getParameter()+"). Please provide a positive distance..");
					Area movement=buildRectangleFromSegment(currentRobotPosition,
							i.getParameter(),
							field.getRobotWidth(),
							field.getRobotLength(),
							currentRobotRotation);					
					if(visual) drawer.addMovement(movement);
					if(!validArea(movement)){
						throw new SimulationException("The movement instruction at line "+counter+" \""+i.getName()+" "+i.getParameter()+"\" is not valid. The robot intersects a wall, or goes outside the field.");					
					}
					if(bumpsBall(movement,updateCoordinates(currentRobotPosition,currentRobotRotation,i.getParameter()),currentRobotRotation)){
						throw new SimulationException("The movement instruction at line "+counter+" \""+i.getName()+" "+i.getParameter()+"\" is not valid. The robot bumps the ball.");
					}
					currentRobotPosition=updateCoordinates(currentRobotPosition,currentRobotRotation,i.getParameter());					
					if(visual) {
						drawer.addRobot(buildRobotRectangle(getCurrentRobotPosition(),getCurrentRobotRotation()));
						drawer.setClawArea(buildClawArea(getCurrentRobotPosition(),getCurrentRobotRotation()));
					}
					break;
				}
				case goback: {
					if(i.getParameter()<0) throw new InvalidInstructionException("The instruction at line "+counter+" \""+i.getName()+"\" has an invalid parameter ("+i.getParameter()+"). Please provide a positive distance..");
					Area movement=buildRectangleFromSegment(currentRobotPosition,
							i.getParameter(),
							field.getRobotWidth(),
							field.getRobotLength(),
							(currentRobotRotation+180)%360);
					if(visual) drawer.addMovement(movement);
					if(!validArea(movement)){
						throw new SimulationException("The movement instruction at line "+counter+" \""+i.getName()+" "+i.getParameter()+"\" is not valid. The robot intersects a wall, or goes outside the field.");
					}
					if(bumpsBall(movement,updateCoordinates(currentRobotPosition,currentRobotRotation,i.getParameter()),currentRobotRotation)){
						throw new SimulationException("The movement instruction at line "+counter+" \""+i.getName()+" "+i.getParameter()+"\" is not valid. The robot bumps the ball.");
					}
					currentRobotPosition=updateCoordinates(currentRobotPosition,(currentRobotRotation+180)%360,i.getParameter());
					if(visual) {
						drawer.addRobot(buildRobotRectangle(getCurrentRobotPosition(),getCurrentRobotRotation()));
						drawer.setClawArea(buildClawArea(getCurrentRobotPosition(),getCurrentRobotRotation()));
					}
					break;
				}
				case turnright: {	
					if(i.getParameter()<0) throw new InvalidInstructionException("The instruction at line "+counter+" \""+i.getName()+"\" has an invalid parameter ("+i.getParameter()+"). Please provide a positive angle between 0 and 360.");
					Area movement=buildCircleAroundRobot(currentRobotPosition, field.getRobotWidth(), field.getRobotLength());
					if(visual) drawer.addMovement(movement);
					if(!validArea(movement)) {
						throw new SimulationException("The turning instruction at line "+counter+" \""+i.getName()+" "+i.getParameter()+"\" is not valid. The robot intersects a wall, or goes outside the field.");
					}
					if(bumpsBall(movement,updateCoordinates(currentRobotPosition,currentRobotRotation,i.getParameter()),currentRobotRotation)){
						throw new SimulationException("The movement instruction at line "+counter+" \""+i.getName()+" "+i.getParameter()+"\" is not valid. The robot bumps the ball.");
					}
					currentRobotRotation=(currentRobotRotation+i.getParameter())%360;
					if(visual) {
						drawer.addRobot(buildRobotRectangle(getCurrentRobotPosition(),getCurrentRobotRotation()));
						drawer.setClawArea(buildClawArea(getCurrentRobotPosition(),getCurrentRobotRotation()));
					}
					break;
				}
				case turnleft: {
					if(i.getParameter()<0) throw new InvalidInstructionException("The instruction at line "+counter+" \""+i.getName()+"\" has an invalid parameter ("+i.getParameter()+"). Please provide a positive angle between 0 and 360.");
					Area movement=buildCircleAroundRobot(currentRobotPosition, field.getRobotWidth(), field.getRobotLength());
					if(visual) drawer.addMovement(movement);
					if(!validArea(movement)) {
						throw new SimulationException("The turning instruction at line "+counter+" \""+i.getName()+" "+i.getParameter()+"\" is not valid. The robot intersects a wall, or goes outside the field.");
					}
					if(bumpsBall(movement,updateCoordinates(currentRobotPosition,currentRobotRotation,i.getParameter()),currentRobotRotation)){
						throw new SimulationException("The movement instruction at line "+counter+" \""+i.getName()+" "+i.getParameter()+"\" is not valid. The robot bumps the ball.");
					}
					currentRobotRotation=(currentRobotRotation-i.getParameter()+360)%360;
					if(visual) {
						drawer.addRobot(buildRobotRectangle(getCurrentRobotPosition(),getCurrentRobotRotation()));
						drawer.setClawArea(buildClawArea(getCurrentRobotPosition(),getCurrentRobotRotation()));
					}
					break;
				}
				case openclaw: {
					clawClosed=false;
					if(holdingBall){
						holdingBall=false;
						Point2D.Double newBallPosition=new Point2D.Double(
								buildClawArea(currentRobotPosition,currentRobotRotation).getBounds2D().getCenterX(),
								buildClawArea(currentRobotPosition,currentRobotRotation).getBounds2D().getCenterY()
						);
						field.setBallPosition(newBallPosition);
						if(goalArea().contains(newBallPosition)) goalReached=true;													
						if(visual) drawer.setBall(newBallPosition);
					}
					break;
				}
				case closeclaw: {
					clawClosed=true;					
					Area intersection=buildClawArea(currentRobotPosition,currentRobotRotation);
					intersection.intersect(ballArea());
					if(intersection.equals(ballArea())){
						holdingBall=true;
						if(goalReached) goalReached=false;
						if(visual) drawer.setBall(null);
					}
					break;
				}
				default: throw new InvalidInstructionException("The instruction at line "+counter+" \""+i.getName()+"\" does not exist");
				}
				counter++;
				if(visual) drawer.repaint();
			}


		}
		else{
			throw new SimulationException("The initial position of the robot is not valid. Either it intersects a wall, or is outside the field.");
		}

		return goalReached;

	}



	private boolean bumpsBall(Area movement, Point2D.Double updateCoordinates, double robotRotation) {
		if(holdingBall) return false;
		Area a=(Area) movement.clone();
		a.intersect(ballArea());
		if(!a.isEmpty()){
			if(clawClosed) return true;
			Area claw=buildClawArea(updateCoordinates,robotRotation);
			Area robot=buildRobotRectangle(updateCoordinates,robotRotation);
			robot.clone();
			robot.subtract(claw);
			robot.intersect(ballArea());
			if(!robot.isEmpty()) return true;
		}
		return false;
	}

	/**
	 * Build a circle around the robot representing the clearance needed for rotation. Hypothesis: the rotation 
	 * point is at the center of the robot.
	 * @param robotPosition The position of the robot
	 * @param robotWidth The width of the robot
	 * @param robotLength The length of the robot
	 * @return An Area object representing the space swept by the robot. 
	 */
	private Area buildCircleAroundRobot(Point2D.Double robotPosition, int robotWidth, int robotLength) {
		double radius=robotPosition.distance(robotPosition.x-robotWidth/2,robotPosition.y-robotLength/2)-rotationTolerance;
		Arc2D.Double arc=new Arc2D.Double();
		arc.setArcByCenter(robotPosition.x, robotPosition.y, radius, 0, 360, Arc2D.PIE);
		Area a=new Area(arc);
		return a;
	}
	
	/**
	 * Define if the current robot position is valid or not. Valid means there are
	 * no intersections with walls, and the robot is completely inside the field. 
	 * @return True if the position is valid, false otherwise.
	 */
	private boolean validCurrentPosition() {
		Area robot=buildRobotRectangle(currentRobotPosition, currentRobotRotation);
		return validArea(robot);
	} 

	/**Validate if an area is valid or not. Valid means there are
	 * no intersections with walls, and the area is completely inside the field.
	 * @param a The area to validate.
	 * @return True if the area is valid, false otherwise.
	 */
	private boolean validArea(Area a){
		return (!intersectWalls(a) && isAllInsideField(a));
	}

	/**Check if an area intersects walls
	 * @param a The Area to test for intersection
	 * @return True if the area intersects walls, false otherwise
	 */
	private boolean intersectWalls(Area a){
		for (Area w : field.getWalls()){
			Area clone=((Area)a.clone());
			clone.intersect(w);
			if(!(clone.isEmpty())) { //if an intersection is not empty
				return true; 		 //a wall is intersected
			}
		}
		return false; //no intersection
	}

	/**Checks if an Area is completely contained in the game field
	 * @param a The Area to check
	 * @return True is the Area is completely in the field, false otherwise
	 */
	public boolean isAllInsideField(Area a){
		Area clone=((Area)a.clone());
		clone.intersect(new Area(field.getField()));
		if (clone.equals(a)){
			return true; //the area is completely in the field
		}
		return false; //it is not
	}

	/**
	 * Calculate the rectangle of the robot body, assuming its position represents the center of the robot.
	 * @param robotPosition The position of the robot
	 * @param robotRotation The orientation of the robot (0 degrees is up)
	 * @return An Area representing the body of the robot in its current position and orientation.
	 */
	public Area buildRobotRectangle(Point2D.Double robotPosition, double robotRotation){
		return buildRectangleFromSegment(robotPosition,0,field.getRobotWidth(),
				field.getRobotLength(),robotRotation);
	}

	/**Calculate the area occupied by the ball
	 * @return The area occupied by the ball
	 */
	public Area ballArea(){
		Ellipse2D.Double ball=new Ellipse2D.Double(field.getBallPosition().x-field.getBallDiameter()/2, field.getBallPosition().y-field.getBallDiameter()/2, field.getBallDiameter(), field.getBallDiameter());
		return new Area(ball);
	}

	public Area goalArea(){
		Rectangle2D.Double goal=new Rectangle2D.Double(field.getGoal().x-field.getGoalWidth()/2,field.getGoal().y-field.getGoalWidth()/2,field.getGoalWidth(),field.getGoalWidth());
		return new Area(goal);
	}

	/**Calculate the area under the claw
	 * @param robotPosition Position of the robot
	 * @param robotRotation Orientation of the robot
	 * @return A rectangular Area representing the area under the claw
	 */
	public Area buildClawArea(Point2D.Double robotPosition, double robotRotation){
		double dlength=new Integer(field.getRobotLength()).doubleValue();
		double dwidth=new Integer(field.getRobotWidth()).doubleValue();
		Rectangle2D.Double r = new Rectangle2D.Double(
				robotPosition.x- (dwidth)/2,			
				robotPosition.y - (dlength/2) + (dlength - ((dlength)/100*clawPercent)), 				
				field.getRobotWidth(),
				dlength/100*clawPercent+1
		);
		Area a = new Area(r).createTransformedArea(
				AffineTransform.getRotateInstance(Math.toRadians(robotRotation), robotPosition.x, robotPosition.y)
		);		
		return a;		
	}

	/** Given a straight segment of length x starting at the robot position, it builds a rectangular area which is x+(robotLength) long,
	 * and robotWidth wide, which is the area used by the robot to move in a straight line. 
	 * If length=0, gives the area of the current static position of the robot.
	 * @param robotPosition The position of the robot
	 * @param length Length of the segment
	 * @param robotWidth Width of the robot
	 * @param robotLength Length of the robot
	 * @param robotRotation The orientation of the robot
	 * @return The created rectangular Area.
	 */
	public Area buildRectangleFromSegment(Point2D.Double robotPosition, int length, 
			int robotWidth, int robotLength, double robotRotation){

		//first build the movement rectangle as if it was pointing down
		Rectangle2D.Double r=new Rectangle2D.Double(
				robotPosition.x-robotWidth/2,
				robotPosition.y-robotLength/2,
				robotWidth,
				robotLength+length			
		);		

		//finally apply the rotation around the robot position
		return new Area(r).createTransformedArea(
				AffineTransform.getRotateInstance(Math.toRadians(robotRotation), robotPosition.x, robotPosition.y)
		);

	}

	/**Find out the destination point of a robot movement.
	 * @param robotPosition Initial robot position.
	 * @param robotRotation Initial robot orientation (degrees).
	 * @param length Length of the movement.
	 * @see http://en.wikipedia.org/wiki/Rotation_%28mathematics%29#In_two_dimensions
	 */
	public Point2D.Double updateCoordinates(Point2D.Double robotPosition, double robotRotation, int length){		
		//End point if the translation was originating in 0,0
		//x'=x*cos(angle)-y*sin(angle)
		//y'=x*sin(angle)+y*cos(angle)
		// see http://en.wikipedia.org/wiki/Rotation_%28mathematics%29#In_two_dimensions		
		Point2D.Double endPosition=new Point2D.Double(-length*Math.sin(Math.toRadians(robotRotation)),length*Math.cos(Math.toRadians(robotRotation)));
		//Apply translation w.r.t. initial robot position
		endPosition.x=endPosition.x+robotPosition.x;
		endPosition.y=endPosition.y+robotPosition.y;
		return endPosition;

	}

	public Gamefield getField() {
		return field;
	}

	public static void main(String[] args) {
		Simulator sim=new Simulator(new GeneratedInstructionSet(),true);
		try {
			if(sim.run()) System.out.println("Goal Reached!");
		} catch (SimulationException e) {

			e.printStackTrace();
		} catch (InvalidInstructionException e) {

			e.printStackTrace();
		}

	}
}
