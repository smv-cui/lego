package ch.unige.cui.smv.simulator;

public class InvalidInstructionException extends Exception {

    private static final long serialVersionUID = 654875132790261607L;

    public InvalidInstructionException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidInstructionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidInstructionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidInstructionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
