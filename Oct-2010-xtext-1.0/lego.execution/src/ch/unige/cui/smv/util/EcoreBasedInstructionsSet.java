package ch.unige.cui.smv.util;

import ch.unige.cui.smv.legodsl.InstrDeg;
import ch.unige.cui.smv.legodsl.InstrUn;
import ch.unige.cui.smv.legodsl.InstrZero;
import ch.unige.cui.smv.util.Instruction.InstructionNames;

/**
 * InstructionsSet that builds the corresponding instructions based on a InstructionsSetDsl that comes from the XText DSL.
 *
 * @author Alexis
 */
public class EcoreBasedInstructionsSet extends InstructionsSet{
	private static final long serialVersionUID = -5991709262733293630L;

	private InstructionsSet ecoreSet;
	
	public EcoreBasedInstructionsSet(InstructionsSet set) {
		ecoreSet = set;
	}
	
	/**
	 * Builds and instruction based on an EObject from the XText dsl.
	 *
	 * @param instr The instruction that comes from the dsl.
	 * @return the java instruction.
	 */
	private Instruction getInstruction(Instruction instr) {
		if (instr instanceof InstrUn) {
			InstrUn local = (InstrUn) instr; 
			switch (local.getNom()) {
				case AVANCE : return new Instruction(InstructionNames.advance, local.getDist());
				case RECULE : return new Instruction(InstructionNames.goback, local.getDist()); 
			}
		} else if (instr instanceof InstrZero) {
			InstrZero local = (InstrZero) instr;
			switch (local.getNom()) {
				case OUVRE : return new Instruction(InstructionNames.openclaw);
				case FERME : return new Instruction(InstructionNames.closeclaw);
				case TOURNED : return new Instruction(InstructionNames.turnright, 90);
				case TOURNEG : return new Instruction(InstructionNames.turnleft, 90);
			}
		} else if (instr instanceof InstrDeg) {
			InstrDeg local = (InstrDeg) instr;
			switch (local.getNom()) {
				case TOURNED : return new Instruction(InstructionNames.turnright, local.getAngle());
				case TOURNEG : return new Instruction(InstructionNames.turnleft, local.getAngle());
			}
		}
		throw new IllegalArgumentException("Found an instruction that isn't one of the predefined instructions in the DSL!");
	}
	
	protected void build() {
		for (Instruction instr : ecoreSet.getInstructions()) {
			addInstruction(getInstruction(instr));
		}
	}
}
