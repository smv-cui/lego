package ch.unige.cui.smv.simulator;

import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.geom.Area;
import java.awt.geom.Point2D;
import java.util.Vector;

/**
 * @author risoldi
 *
 */
public class Gamefield {
	private Vector<Area> walls=new Vector<Area>();
	private Rectangle field;
	private Point2D.Double ballPosition;
	private Point2D.Double goal;
	private int goalWidth;
	private Point2D.Double robotPositionStart;
	private double robotRotationStart;
	
	//Margins for ball and robot
	private final int ballDiameter=5;
	private final int robotWidth=14;
	private final int robotLength=39;
	private final int fieldWidth=150;
	private final int fieldHeight=150;
	private final int clawPercent=25; //% of the robot length that is the area under the claw
	
	//Sizes and coordinates for the dummy field
	private final int[] dummyXCoord={60,80,80,100,100,80,80,60,60,40,40,60};
	private final int[] dummyYCoord={50,50,70,70,90,90,110,110,90,90,70,70};
	private final int   dummyNPoints=12;
	private final Rectangle dummyField=new Rectangle(fieldWidth,fieldHeight);
	private final Point2D.Double dummyBallPosition=new Point2D.Double(90,60);
	private final Point2D.Double dummyGoal=new Point2D.Double(50,100);
	private final int dummyGoalWidth=20;
	private final Point2D.Double dummyRobotStart=new Point2D.Double(30,30);
	private final double dummyRobotRotationStart=0;
	
	/**
	 * Constructor initializing all fields to dummy values
	 */
	public Gamefield(){
		//initialize to dummy values
		walls.add(new Area(new Polygon(dummyXCoord,dummyYCoord,dummyNPoints)));
		field=dummyField;
		ballPosition=dummyBallPosition;
		goal=dummyGoal;
		goalWidth=dummyGoalWidth;
		robotPositionStart=dummyRobotStart;
		robotRotationStart=dummyRobotRotationStart;
	}
	
	
	
	/**
	 * Initialize a game field with given values.
	 * @param field The boundaries of the game field
	 * @param walls An array of polygons representing walls
	 * @param ballPosition Initial coordinates of the ball
	 * @param goal Coordinates of the goal
	 * @param goalWidth Width of the goal
	 * @param robotStart Initial coordinates of the robot
	 * @param robotRotationStart Initial orientation of the robot
	 */
	public Gamefield(Rectangle field, Vector<Area> walls, Point2D.Double ballPosition,
			Point2D.Double goal, int goalWidth, Point2D.Double robotStart, int robotRotationStart) {
		super();
		this.field = field;
		this.walls = walls;
		this.ballPosition = ballPosition;
		this.goal = goal;
		this.goalWidth = goalWidth;
		this.robotPositionStart = robotStart;
		this.robotRotationStart = robotRotationStart;
	}



	public Vector<Area> getWalls() {
		return walls;
	}

	public Rectangle getField() {
		return field;
	}

	public Point2D.Double getBallPosition() {
		return ballPosition;
	}

	public Point2D.Double getGoal() {
		return goal;
	}

	public Point2D.Double getRobotPositionStart() {
		return robotPositionStart;
	}

	public int getBallDiameter() {
		return ballDiameter;
	}

	public int getRobotWidth() {
		return robotWidth;
	}

	public int getRobotLength() {
		return robotLength;
	}

	public double getRobotRotationStart() {
		return robotRotationStart;
	}

	public int getGoalWidth() {
		return goalWidth;
	}



	public void setBallPosition(Point2D.Double ballPosition) {
		this.ballPosition = ballPosition;
	}



	public int getFieldWidth() {
		return fieldWidth;
	}



	public int getFieldHeight() {
		return fieldHeight;
	}



	public int getClawPercent() {
		return clawPercent;
	}
		
}
