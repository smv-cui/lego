package ch.unige.cui.smv.util;

public class Instruction {
	public enum InstructionNames {
		advance(1),
		turnright(1),
		turnleft(1),
		goback(1),
		closeclaw(0),
		openclaw(0);
		
		private int nbParams;
		InstructionNames(int nbParams) {
			this.nbParams = nbParams;
		}
		
		public int getNbParams() {
			return nbParams;
		}
	}
	
	private InstructionNames name;
	
	private int parameter;
	
	public Instruction(InstructionNames name) {
		if (name.getNbParams() == 0) {
			this.name = name;
		} else {
			throw new IllegalArgumentException("Wrong number of parameters, the name " + name.toString() + "must not take any parameter");
		}
	}
	
	public Instruction(InstructionNames name, int parameter) {
		if (name.getNbParams() == 1) {
			this.name = name;
			this.parameter = parameter;
		} else {
			throw new IllegalArgumentException("Wrong number of parameters, the name " + name.toString() + "must take one parameter");
		}
	}

	public InstructionNames getName() {
		return name;
	}

	public int getParameter() {
		return parameter;
	}
}
