package ch.unige.cui.smv.generated;
import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

public class GeneratedLegoInstructions {
	public static void main(String[] args) {
		Pilot pilot = new TachoPilot(5.6f, 12f, Motor.C, Motor.B, true);
		pilot.setMoveSpeed(20);
 		Motor.A.setSpeed(40);

		//Tourner à gauche
		pilot.rotate(-90);
		//Bouger
		pilot.travel(60);
		//Tourner à droite
		pilot.rotate(90);
		//Bouger
		pilot.travel(15);
		//Fermer la pince 
		Motor.A.rotateTo(140);
		//Bouger
		pilot.travel(-15);
		//Tourner à droite
		pilot.rotate(90);
		//Bouger
		pilot.travel(70);
		//Tourner à gauche
		pilot.rotate(-90);
		//Bouger
		pilot.travel(70);
		//Tourner à gauche
		pilot.rotate(-90);
		//Bouger
		pilot.travel(15);
		//Ouvrir la pince
		Motor.A.rotateTo(-140);
 		
 		pilot.stop();
		System.out.println("Fin Programme");
 		Button.waitForPress();
  	}
}
