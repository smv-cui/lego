/**
This software is part of CoopnTools, and protected like it.
Copyright (C) 2008 - SMV@Geneva University.
This program is free software; you can redistribute it and/or modify
it under the  terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.unige.cui.smv.ui;



import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.internal.Workbench;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.ui.core.editor.XtextEditor;

import ch.unige.cui.smv.LegoStandaloneSetup;
import ch.unige.cui.smv.legodsl.impl.InstructionsSetDslImpl;
import ch.unige.cui.smv.simulator.Simulator;
import ch.unige.cui.smv.util.EcoreBasedInstructionsSet;
import ch.unige.cui.smv.util.InstructionsSet;

import com.google.inject.Injector;

@SuppressWarnings("restriction")
public class StartActionDelegate implements IWorkbenchWindowActionDelegate {
	private IWorkbenchWindow window;


	public void dispose() {
	}

	public void init(IWorkbenchWindow window) {
		this.window = window;
	}

	public void run(IAction action) {

		try {
		    IEditorPart editor = Workbench.getInstance().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		    XtextEditor e = (XtextEditor) editor;
		    new org.eclipse.emf.mwe.utils.StandaloneSetup().setPlatformUri("../");
		    Injector injector = new LegoStandaloneSetup().createInjectorAndDoEMFRegistration();
		    XtextResourceSet resourceSet = injector.getInstance(XtextResourceSet.class);
		    resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		    Resource resource = resourceSet.getResource(URI.createURI(e.getResource().getLocationURI().toString()), true);
		    if (resource.getContents().size() > 0) {
    		    
    		    InstructionsSetDslImpl is = (InstructionsSetDslImpl) resource.getContents().get(0);
    		    InstructionsSet set = new EcoreBasedInstructionsSet(is); 
    
    		    new Simulator(set, true).run();
		    }
		} catch (Throwable ex) {
			ErrorManager.error(this.window.getShell(), ex);		
		}

	}


	public void selectionChanged(IAction action, ISelection selection) {
	}

}

