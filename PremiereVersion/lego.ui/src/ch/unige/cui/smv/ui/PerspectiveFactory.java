/**
This software is part of CoopnTools, and protected like it.
Copyright (C) 2008 - SMV@Geneva University.
This program is free software; you can redistribute it and/or modify
it under the  terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package ch.unige.cui.smv.ui;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * Organizes the Alpina's perspective, namely its windows and part organization.
 * @author steve.hostettler
 */
public class PerspectiveFactory implements IPerspectiveFactory {

    /** Project explorer view. */
    private static final String PROJECT_EXPLORER_VIEW_ID = "org.eclipse.ui.navigator.ProjectExplorer";
    /** Project explorer view. */
    private static final String CONSOLE_VIEW_ID = "org.eclipse.ui.console.ConsoleView";
    /** Project explorer view. */
    private static final String ERROR_VIEW_ID = "org.eclipse.ui.views.ProblemView";

    /** Attach view to bottom. */
    private static final String BOTTOM = "bottom";
    /** Attach view to left. */
    private static final String LEFT = "left";

    /**
     * ProjectExplorer  Editor
     * {@inheritDoc}.
     * */
    public void createInitialLayout(IPageLayout myLayout) {
        IFolderLayout left = myLayout.createFolder(LEFT, IPageLayout.LEFT, 0.16f, myLayout.getEditorArea());
        left.addView(PROJECT_EXPLORER_VIEW_ID);

        IFolderLayout bot = myLayout.createFolder(BOTTOM, IPageLayout.BOTTOM, 0.76f, myLayout.getEditorArea());
        bot.addView(CONSOLE_VIEW_ID);
        bot.addView(ERROR_VIEW_ID);
    }


}
