/**
This software is part of CoopnTools, and protected like it.
Copyright (C) 2008 - SMV@Geneva University.
This program is free software; you can redistribute it and/or modify
it under the  terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
package ch.unige.cui.smv.ui;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

/**
 * Manage error handling in a unique way thorough the application.
 * @author steve.hostettler
 *
 */
public class ErrorManager {

    /**
     * Log and displays an error message.
     * @param shell on which the dialog is displayed.
     * @param title of the Error dialog
     * @param msg to display
     */
    public static void error(Shell shell, String title, String msg) {
        System.err.println(title + ": " + msg);
        MessageDialog.openError(shell, title, msg);
    }

    /**
     * Log and displays an error message.
     * @param msg to display
     */
    public static void error(String msg) {
        System.err.println(msg);
    }

    /**
     * Log and displays an error message.
     * @param msg to display
     */
    public static void error(Throwable ex) {
        System.err.println(ex);
    }

    /**
     * Log and displays an error message.
     * @param shell on which the dialog is displayed.
     * @param e exception to display
     */
    public static void error(Shell shell, Throwable e) {
        e.printStackTrace(System.err);
        String msg = e.getMessage();
        if (msg == null || msg.length() == 0) {
            msg = "An unexpected error occurs : " + e.getClass() + "! Please consult the log.";
        }
        MessageDialog.openError(shell, "Error", msg);
    }

	public static void error(Exception ex) {
		ex.printStackTrace(System.err);
	}

}
