lexer grammar InternalLego;
@header {
package ch.unige.cui.smv.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ui.common.editor.contentassist.antlr.internal.Lexer;
}

T11 : 'avance de' ;
T12 : 'recule de' ;
T13 : 'tourne \uFFFD gauche de' ;
T14 : 'tourne \uFFFD droite de' ;
T15 : 'ferme la pince' ;
T16 : 'ouvre la pince' ;
T17 : 'tourne \uFFFD gauche' ;
T18 : 'tourne \uFFFD droite' ;
T19 : 'DEBUT' ;
T20 : 'FIN' ;
T21 : 'cm' ;
T22 : 'degres' ;

// $ANTLR src "../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g" 644
RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

// $ANTLR src "../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g" 646
RULE_INT : ('0'..'9')+;

// $ANTLR src "../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g" 648
RULE_STRING : ('"' ('\\' ('b'|'t'|'n'|'f'|'r'|'"'|'\''|'\\')|~(('\\'|'"')))* '"'|'\'' ('\\' ('b'|'t'|'n'|'f'|'r'|'"'|'\''|'\\')|~(('\\'|'\'')))* '\'');

// $ANTLR src "../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g" 650
RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

// $ANTLR src "../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g" 652
RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

// $ANTLR src "../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g" 654
RULE_WS : (' '|'\t'|'\r'|'\n')+;

// $ANTLR src "../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g" 656
RULE_ANY_OTHER : .;


