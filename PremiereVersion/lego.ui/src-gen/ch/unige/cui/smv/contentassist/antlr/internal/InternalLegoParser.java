package ch.unige.cui.smv.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.xtext.parsetree.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.common.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import ch.unige.cui.smv.services.LegoGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class InternalLegoParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INT", "RULE_ID", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'avance de'", "'recule de'", "'tourne \\uFFFD gauche de'", "'tourne \\uFFFD droite de'", "'ferme la pince'", "'ouvre la pince'", "'tourne \\uFFFD gauche'", "'tourne \\uFFFD droite'", "'DEBUT'", "'FIN'", "'cm'", "'degres'"
    };
    public static final int RULE_ID=5;
    public static final int RULE_STRING=6;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=4;
    public static final int RULE_WS=9;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;

        public InternalLegoParser(TokenStream input) {
            super(input);
        }
        

    public String[] getTokenNames() { return tokenNames; }
    public String getGrammarFileName() { return "../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g"; }


     
     	private LegoGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(LegoGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start entryRuleInstructionsSetDsl
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:60:1: entryRuleInstructionsSetDsl : ruleInstructionsSetDsl EOF ;
    public final void entryRuleInstructionsSetDsl() throws RecognitionException {
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:60:29: ( ruleInstructionsSetDsl EOF )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:61:1: ruleInstructionsSetDsl EOF
            {
             before(grammarAccess.getInstructionsSetDslRule()); 
            pushFollow(FOLLOW_ruleInstructionsSetDsl_in_entryRuleInstructionsSetDsl60);
            ruleInstructionsSetDsl();
            _fsp--;

             after(grammarAccess.getInstructionsSetDslRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstructionsSetDsl67); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end entryRuleInstructionsSetDsl


    // $ANTLR start ruleInstructionsSetDsl
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:68:1: ruleInstructionsSetDsl : ( ( rule__InstructionsSetDsl__Group__0 ) ) ;
    public final void ruleInstructionsSetDsl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:72:2: ( ( ( rule__InstructionsSetDsl__Group__0 ) ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:73:1: ( ( rule__InstructionsSetDsl__Group__0 ) )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:73:1: ( ( rule__InstructionsSetDsl__Group__0 ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:74:1: ( rule__InstructionsSetDsl__Group__0 )
            {
             before(grammarAccess.getInstructionsSetDslAccess().getGroup()); 
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:75:1: ( rule__InstructionsSetDsl__Group__0 )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:75:2: rule__InstructionsSetDsl__Group__0
            {
            pushFollow(FOLLOW_rule__InstructionsSetDsl__Group__0_in_ruleInstructionsSetDsl94);
            rule__InstructionsSetDsl__Group__0();
            _fsp--;


            }

             after(grammarAccess.getInstructionsSetDslAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end ruleInstructionsSetDsl


    // $ANTLR start entryRuleInstrUn
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:87:1: entryRuleInstrUn : ruleInstrUn EOF ;
    public final void entryRuleInstrUn() throws RecognitionException {
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:87:18: ( ruleInstrUn EOF )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:88:1: ruleInstrUn EOF
            {
             before(grammarAccess.getInstrUnRule()); 
            pushFollow(FOLLOW_ruleInstrUn_in_entryRuleInstrUn120);
            ruleInstrUn();
            _fsp--;

             after(grammarAccess.getInstrUnRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstrUn127); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end entryRuleInstrUn


    // $ANTLR start ruleInstrUn
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:95:1: ruleInstrUn : ( ( rule__InstrUn__Group__0 ) ) ;
    public final void ruleInstrUn() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:99:2: ( ( ( rule__InstrUn__Group__0 ) ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:100:1: ( ( rule__InstrUn__Group__0 ) )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:100:1: ( ( rule__InstrUn__Group__0 ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:101:1: ( rule__InstrUn__Group__0 )
            {
             before(grammarAccess.getInstrUnAccess().getGroup()); 
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:102:1: ( rule__InstrUn__Group__0 )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:102:2: rule__InstrUn__Group__0
            {
            pushFollow(FOLLOW_rule__InstrUn__Group__0_in_ruleInstrUn154);
            rule__InstrUn__Group__0();
            _fsp--;


            }

             after(grammarAccess.getInstrUnAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end ruleInstrUn


    // $ANTLR start entryRuleInstrDeg
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:114:1: entryRuleInstrDeg : ruleInstrDeg EOF ;
    public final void entryRuleInstrDeg() throws RecognitionException {
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:114:19: ( ruleInstrDeg EOF )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:115:1: ruleInstrDeg EOF
            {
             before(grammarAccess.getInstrDegRule()); 
            pushFollow(FOLLOW_ruleInstrDeg_in_entryRuleInstrDeg180);
            ruleInstrDeg();
            _fsp--;

             after(grammarAccess.getInstrDegRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstrDeg187); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end entryRuleInstrDeg


    // $ANTLR start ruleInstrDeg
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:122:1: ruleInstrDeg : ( ( rule__InstrDeg__Group__0 ) ) ;
    public final void ruleInstrDeg() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:126:2: ( ( ( rule__InstrDeg__Group__0 ) ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:127:1: ( ( rule__InstrDeg__Group__0 ) )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:127:1: ( ( rule__InstrDeg__Group__0 ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:128:1: ( rule__InstrDeg__Group__0 )
            {
             before(grammarAccess.getInstrDegAccess().getGroup()); 
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:129:1: ( rule__InstrDeg__Group__0 )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:129:2: rule__InstrDeg__Group__0
            {
            pushFollow(FOLLOW_rule__InstrDeg__Group__0_in_ruleInstrDeg214);
            rule__InstrDeg__Group__0();
            _fsp--;


            }

             after(grammarAccess.getInstrDegAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end ruleInstrDeg


    // $ANTLR start entryRuleInstrZero
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:141:1: entryRuleInstrZero : ruleInstrZero EOF ;
    public final void entryRuleInstrZero() throws RecognitionException {
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:141:20: ( ruleInstrZero EOF )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:142:1: ruleInstrZero EOF
            {
             before(grammarAccess.getInstrZeroRule()); 
            pushFollow(FOLLOW_ruleInstrZero_in_entryRuleInstrZero240);
            ruleInstrZero();
            _fsp--;

             after(grammarAccess.getInstrZeroRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstrZero247); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end entryRuleInstrZero


    // $ANTLR start ruleInstrZero
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:149:1: ruleInstrZero : ( ( rule__InstrZero__NomAssignment ) ) ;
    public final void ruleInstrZero() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:153:2: ( ( ( rule__InstrZero__NomAssignment ) ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:154:1: ( ( rule__InstrZero__NomAssignment ) )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:154:1: ( ( rule__InstrZero__NomAssignment ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:155:1: ( rule__InstrZero__NomAssignment )
            {
             before(grammarAccess.getInstrZeroAccess().getNomAssignment()); 
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:156:1: ( rule__InstrZero__NomAssignment )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:156:2: rule__InstrZero__NomAssignment
            {
            pushFollow(FOLLOW_rule__InstrZero__NomAssignment_in_ruleInstrZero274);
            rule__InstrZero__NomAssignment();
            _fsp--;


            }

             after(grammarAccess.getInstrZeroAccess().getNomAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end ruleInstrZero


    // $ANTLR start entryRuleInstructionDsl
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:168:1: entryRuleInstructionDsl : ruleInstructionDsl EOF ;
    public final void entryRuleInstructionDsl() throws RecognitionException {
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:168:25: ( ruleInstructionDsl EOF )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:169:1: ruleInstructionDsl EOF
            {
             before(grammarAccess.getInstructionDslRule()); 
            pushFollow(FOLLOW_ruleInstructionDsl_in_entryRuleInstructionDsl300);
            ruleInstructionDsl();
            _fsp--;

             after(grammarAccess.getInstructionDslRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstructionDsl307); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end entryRuleInstructionDsl


    // $ANTLR start ruleInstructionDsl
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:176:1: ruleInstructionDsl : ( ( rule__InstructionDsl__Alternatives ) ) ;
    public final void ruleInstructionDsl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:180:2: ( ( ( rule__InstructionDsl__Alternatives ) ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:181:1: ( ( rule__InstructionDsl__Alternatives ) )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:181:1: ( ( rule__InstructionDsl__Alternatives ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:182:1: ( rule__InstructionDsl__Alternatives )
            {
             before(grammarAccess.getInstructionDslAccess().getAlternatives()); 
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:183:1: ( rule__InstructionDsl__Alternatives )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:183:2: rule__InstructionDsl__Alternatives
            {
            pushFollow(FOLLOW_rule__InstructionDsl__Alternatives_in_ruleInstructionDsl334);
            rule__InstructionDsl__Alternatives();
            _fsp--;


            }

             after(grammarAccess.getInstructionDslAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end ruleInstructionDsl


    // $ANTLR start ruleNomsUn
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:196:1: ruleNomsUn : ( ( rule__NomsUn__Alternatives ) ) ;
    public final void ruleNomsUn() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:200:1: ( ( ( rule__NomsUn__Alternatives ) ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:201:1: ( ( rule__NomsUn__Alternatives ) )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:201:1: ( ( rule__NomsUn__Alternatives ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:202:1: ( rule__NomsUn__Alternatives )
            {
             before(grammarAccess.getNomsUnAccess().getAlternatives()); 
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:203:1: ( rule__NomsUn__Alternatives )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:203:2: rule__NomsUn__Alternatives
            {
            pushFollow(FOLLOW_rule__NomsUn__Alternatives_in_ruleNomsUn371);
            rule__NomsUn__Alternatives();
            _fsp--;


            }

             after(grammarAccess.getNomsUnAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end ruleNomsUn


    // $ANTLR start ruleNomsDeg
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:215:1: ruleNomsDeg : ( ( rule__NomsDeg__Alternatives ) ) ;
    public final void ruleNomsDeg() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:219:1: ( ( ( rule__NomsDeg__Alternatives ) ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:220:1: ( ( rule__NomsDeg__Alternatives ) )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:220:1: ( ( rule__NomsDeg__Alternatives ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:221:1: ( rule__NomsDeg__Alternatives )
            {
             before(grammarAccess.getNomsDegAccess().getAlternatives()); 
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:222:1: ( rule__NomsDeg__Alternatives )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:222:2: rule__NomsDeg__Alternatives
            {
            pushFollow(FOLLOW_rule__NomsDeg__Alternatives_in_ruleNomsDeg407);
            rule__NomsDeg__Alternatives();
            _fsp--;


            }

             after(grammarAccess.getNomsDegAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end ruleNomsDeg


    // $ANTLR start ruleNomsZero
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:234:1: ruleNomsZero : ( ( rule__NomsZero__Alternatives ) ) ;
    public final void ruleNomsZero() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:238:1: ( ( ( rule__NomsZero__Alternatives ) ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:239:1: ( ( rule__NomsZero__Alternatives ) )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:239:1: ( ( rule__NomsZero__Alternatives ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:240:1: ( rule__NomsZero__Alternatives )
            {
             before(grammarAccess.getNomsZeroAccess().getAlternatives()); 
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:241:1: ( rule__NomsZero__Alternatives )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:241:2: rule__NomsZero__Alternatives
            {
            pushFollow(FOLLOW_rule__NomsZero__Alternatives_in_ruleNomsZero443);
            rule__NomsZero__Alternatives();
            _fsp--;


            }

             after(grammarAccess.getNomsZeroAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end ruleNomsZero


    // $ANTLR start rule__InstructionDsl__Alternatives
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:252:1: rule__InstructionDsl__Alternatives : ( ( ruleInstrUn ) | ( ruleInstrZero ) | ( ruleInstrDeg ) );
    public final void rule__InstructionDsl__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:256:1: ( ( ruleInstrUn ) | ( ruleInstrZero ) | ( ruleInstrDeg ) )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 11:
            case 12:
                {
                alt1=1;
                }
                break;
            case 15:
            case 16:
            case 17:
            case 18:
                {
                alt1=2;
                }
                break;
            case 13:
            case 14:
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("252:1: rule__InstructionDsl__Alternatives : ( ( ruleInstrUn ) | ( ruleInstrZero ) | ( ruleInstrDeg ) );", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:257:1: ( ruleInstrUn )
                    {
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:257:1: ( ruleInstrUn )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:258:1: ruleInstrUn
                    {
                     before(grammarAccess.getInstructionDslAccess().getInstrUnParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleInstrUn_in_rule__InstructionDsl__Alternatives478);
                    ruleInstrUn();
                    _fsp--;

                     after(grammarAccess.getInstructionDslAccess().getInstrUnParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:263:6: ( ruleInstrZero )
                    {
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:263:6: ( ruleInstrZero )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:264:1: ruleInstrZero
                    {
                     before(grammarAccess.getInstructionDslAccess().getInstrZeroParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleInstrZero_in_rule__InstructionDsl__Alternatives495);
                    ruleInstrZero();
                    _fsp--;

                     after(grammarAccess.getInstructionDslAccess().getInstrZeroParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:269:6: ( ruleInstrDeg )
                    {
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:269:6: ( ruleInstrDeg )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:270:1: ruleInstrDeg
                    {
                     before(grammarAccess.getInstructionDslAccess().getInstrDegParserRuleCall_2()); 
                    pushFollow(FOLLOW_ruleInstrDeg_in_rule__InstructionDsl__Alternatives512);
                    ruleInstrDeg();
                    _fsp--;

                     after(grammarAccess.getInstructionDslAccess().getInstrDegParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstructionDsl__Alternatives


    // $ANTLR start rule__NomsUn__Alternatives
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:280:1: rule__NomsUn__Alternatives : ( ( ( 'avance de' ) ) | ( ( 'recule de' ) ) );
    public final void rule__NomsUn__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:284:1: ( ( ( 'avance de' ) ) | ( ( 'recule de' ) ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            else if ( (LA2_0==12) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("280:1: rule__NomsUn__Alternatives : ( ( ( 'avance de' ) ) | ( ( 'recule de' ) ) );", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:285:1: ( ( 'avance de' ) )
                    {
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:285:1: ( ( 'avance de' ) )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:286:1: ( 'avance de' )
                    {
                     before(grammarAccess.getNomsUnAccess().getAvanceEnumLiteralDeclaration_0()); 
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:287:1: ( 'avance de' )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:287:3: 'avance de'
                    {
                    match(input,11,FOLLOW_11_in_rule__NomsUn__Alternatives545); 

                    }

                     after(grammarAccess.getNomsUnAccess().getAvanceEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:292:6: ( ( 'recule de' ) )
                    {
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:292:6: ( ( 'recule de' ) )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:293:1: ( 'recule de' )
                    {
                     before(grammarAccess.getNomsUnAccess().getReculeEnumLiteralDeclaration_1()); 
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:294:1: ( 'recule de' )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:294:3: 'recule de'
                    {
                    match(input,12,FOLLOW_12_in_rule__NomsUn__Alternatives566); 

                    }

                     after(grammarAccess.getNomsUnAccess().getReculeEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__NomsUn__Alternatives


    // $ANTLR start rule__NomsDeg__Alternatives
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:304:1: rule__NomsDeg__Alternatives : ( ( ( 'tourne \\uFFFD gauche de' ) ) | ( ( 'tourne \\uFFFD droite de' ) ) );
    public final void rule__NomsDeg__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:308:1: ( ( ( 'tourne \\uFFFD gauche de' ) ) | ( ( 'tourne \\uFFFD droite de' ) ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==13) ) {
                alt3=1;
            }
            else if ( (LA3_0==14) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("304:1: rule__NomsDeg__Alternatives : ( ( ( 'tourne \\uFFFD gauche de' ) ) | ( ( 'tourne \\uFFFD droite de' ) ) );", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:309:1: ( ( 'tourne \\uFFFD gauche de' ) )
                    {
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:309:1: ( ( 'tourne \\uFFFD gauche de' ) )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:310:1: ( 'tourne \\uFFFD gauche de' )
                    {
                     before(grammarAccess.getNomsDegAccess().getTournegEnumLiteralDeclaration_0()); 
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:311:1: ( 'tourne \\uFFFD gauche de' )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:311:3: 'tourne \\uFFFD gauche de'
                    {
                    match(input,13,FOLLOW_13_in_rule__NomsDeg__Alternatives602); 

                    }

                     after(grammarAccess.getNomsDegAccess().getTournegEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:316:6: ( ( 'tourne \\uFFFD droite de' ) )
                    {
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:316:6: ( ( 'tourne \\uFFFD droite de' ) )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:317:1: ( 'tourne \\uFFFD droite de' )
                    {
                     before(grammarAccess.getNomsDegAccess().getTournedEnumLiteralDeclaration_1()); 
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:318:1: ( 'tourne \\uFFFD droite de' )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:318:3: 'tourne \\uFFFD droite de'
                    {
                    match(input,14,FOLLOW_14_in_rule__NomsDeg__Alternatives623); 

                    }

                     after(grammarAccess.getNomsDegAccess().getTournedEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__NomsDeg__Alternatives


    // $ANTLR start rule__NomsZero__Alternatives
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:328:1: rule__NomsZero__Alternatives : ( ( ( 'ferme la pince' ) ) | ( ( 'ouvre la pince' ) ) | ( ( 'tourne \\uFFFD gauche' ) ) | ( ( 'tourne \\uFFFD droite' ) ) );
    public final void rule__NomsZero__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:332:1: ( ( ( 'ferme la pince' ) ) | ( ( 'ouvre la pince' ) ) | ( ( 'tourne \\uFFFD gauche' ) ) | ( ( 'tourne \\uFFFD droite' ) ) )
            int alt4=4;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt4=1;
                }
                break;
            case 16:
                {
                alt4=2;
                }
                break;
            case 17:
                {
                alt4=3;
                }
                break;
            case 18:
                {
                alt4=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("328:1: rule__NomsZero__Alternatives : ( ( ( 'ferme la pince' ) ) | ( ( 'ouvre la pince' ) ) | ( ( 'tourne \\uFFFD gauche' ) ) | ( ( 'tourne \\uFFFD droite' ) ) );", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:333:1: ( ( 'ferme la pince' ) )
                    {
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:333:1: ( ( 'ferme la pince' ) )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:334:1: ( 'ferme la pince' )
                    {
                     before(grammarAccess.getNomsZeroAccess().getFermeEnumLiteralDeclaration_0()); 
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:335:1: ( 'ferme la pince' )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:335:3: 'ferme la pince'
                    {
                    match(input,15,FOLLOW_15_in_rule__NomsZero__Alternatives659); 

                    }

                     after(grammarAccess.getNomsZeroAccess().getFermeEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:340:6: ( ( 'ouvre la pince' ) )
                    {
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:340:6: ( ( 'ouvre la pince' ) )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:341:1: ( 'ouvre la pince' )
                    {
                     before(grammarAccess.getNomsZeroAccess().getOuvreEnumLiteralDeclaration_1()); 
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:342:1: ( 'ouvre la pince' )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:342:3: 'ouvre la pince'
                    {
                    match(input,16,FOLLOW_16_in_rule__NomsZero__Alternatives680); 

                    }

                     after(grammarAccess.getNomsZeroAccess().getOuvreEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:347:6: ( ( 'tourne \\uFFFD gauche' ) )
                    {
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:347:6: ( ( 'tourne \\uFFFD gauche' ) )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:348:1: ( 'tourne \\uFFFD gauche' )
                    {
                     before(grammarAccess.getNomsZeroAccess().getTournegEnumLiteralDeclaration_2()); 
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:349:1: ( 'tourne \\uFFFD gauche' )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:349:3: 'tourne \\uFFFD gauche'
                    {
                    match(input,17,FOLLOW_17_in_rule__NomsZero__Alternatives701); 

                    }

                     after(grammarAccess.getNomsZeroAccess().getTournegEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:354:6: ( ( 'tourne \\uFFFD droite' ) )
                    {
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:354:6: ( ( 'tourne \\uFFFD droite' ) )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:355:1: ( 'tourne \\uFFFD droite' )
                    {
                     before(grammarAccess.getNomsZeroAccess().getTournedEnumLiteralDeclaration_3()); 
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:356:1: ( 'tourne \\uFFFD droite' )
                    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:356:3: 'tourne \\uFFFD droite'
                    {
                    match(input,18,FOLLOW_18_in_rule__NomsZero__Alternatives722); 

                    }

                     after(grammarAccess.getNomsZeroAccess().getTournedEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__NomsZero__Alternatives


    // $ANTLR start rule__InstructionsSetDsl__Group__0
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:368:1: rule__InstructionsSetDsl__Group__0 : ( 'DEBUT' ) rule__InstructionsSetDsl__Group__1 ;
    public final void rule__InstructionsSetDsl__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:372:1: ( ( 'DEBUT' ) rule__InstructionsSetDsl__Group__1 )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:373:1: ( 'DEBUT' ) rule__InstructionsSetDsl__Group__1
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:373:1: ( 'DEBUT' )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:374:1: 'DEBUT'
            {
             before(grammarAccess.getInstructionsSetDslAccess().getDEBUTKeyword_0()); 
            match(input,19,FOLLOW_19_in_rule__InstructionsSetDsl__Group__0760); 
             after(grammarAccess.getInstructionsSetDslAccess().getDEBUTKeyword_0()); 

            }

            pushFollow(FOLLOW_rule__InstructionsSetDsl__Group__1_in_rule__InstructionsSetDsl__Group__0770);
            rule__InstructionsSetDsl__Group__1();
            _fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstructionsSetDsl__Group__0


    // $ANTLR start rule__InstructionsSetDsl__Group__1
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:388:1: rule__InstructionsSetDsl__Group__1 : ( ( rule__InstructionsSetDsl__InstructionsAssignment_1 )* ) rule__InstructionsSetDsl__Group__2 ;
    public final void rule__InstructionsSetDsl__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:392:1: ( ( ( rule__InstructionsSetDsl__InstructionsAssignment_1 )* ) rule__InstructionsSetDsl__Group__2 )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:393:1: ( ( rule__InstructionsSetDsl__InstructionsAssignment_1 )* ) rule__InstructionsSetDsl__Group__2
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:393:1: ( ( rule__InstructionsSetDsl__InstructionsAssignment_1 )* )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:394:1: ( rule__InstructionsSetDsl__InstructionsAssignment_1 )*
            {
             before(grammarAccess.getInstructionsSetDslAccess().getInstructionsAssignment_1()); 
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:395:1: ( rule__InstructionsSetDsl__InstructionsAssignment_1 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>=11 && LA5_0<=18)) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:395:2: rule__InstructionsSetDsl__InstructionsAssignment_1
            	    {
            	    pushFollow(FOLLOW_rule__InstructionsSetDsl__InstructionsAssignment_1_in_rule__InstructionsSetDsl__Group__1798);
            	    rule__InstructionsSetDsl__InstructionsAssignment_1();
            	    _fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getInstructionsSetDslAccess().getInstructionsAssignment_1()); 

            }

            pushFollow(FOLLOW_rule__InstructionsSetDsl__Group__2_in_rule__InstructionsSetDsl__Group__1808);
            rule__InstructionsSetDsl__Group__2();
            _fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstructionsSetDsl__Group__1


    // $ANTLR start rule__InstructionsSetDsl__Group__2
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:406:1: rule__InstructionsSetDsl__Group__2 : ( 'FIN' ) ;
    public final void rule__InstructionsSetDsl__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:410:1: ( ( 'FIN' ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:411:1: ( 'FIN' )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:411:1: ( 'FIN' )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:412:1: 'FIN'
            {
             before(grammarAccess.getInstructionsSetDslAccess().getFINKeyword_2()); 
            match(input,20,FOLLOW_20_in_rule__InstructionsSetDsl__Group__2837); 
             after(grammarAccess.getInstructionsSetDslAccess().getFINKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstructionsSetDsl__Group__2


    // $ANTLR start rule__InstrUn__Group__0
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:431:1: rule__InstrUn__Group__0 : ( ( rule__InstrUn__NomAssignment_0 ) ) rule__InstrUn__Group__1 ;
    public final void rule__InstrUn__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:435:1: ( ( ( rule__InstrUn__NomAssignment_0 ) ) rule__InstrUn__Group__1 )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:436:1: ( ( rule__InstrUn__NomAssignment_0 ) ) rule__InstrUn__Group__1
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:436:1: ( ( rule__InstrUn__NomAssignment_0 ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:437:1: ( rule__InstrUn__NomAssignment_0 )
            {
             before(grammarAccess.getInstrUnAccess().getNomAssignment_0()); 
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:438:1: ( rule__InstrUn__NomAssignment_0 )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:438:2: rule__InstrUn__NomAssignment_0
            {
            pushFollow(FOLLOW_rule__InstrUn__NomAssignment_0_in_rule__InstrUn__Group__0878);
            rule__InstrUn__NomAssignment_0();
            _fsp--;


            }

             after(grammarAccess.getInstrUnAccess().getNomAssignment_0()); 

            }

            pushFollow(FOLLOW_rule__InstrUn__Group__1_in_rule__InstrUn__Group__0887);
            rule__InstrUn__Group__1();
            _fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstrUn__Group__0


    // $ANTLR start rule__InstrUn__Group__1
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:449:1: rule__InstrUn__Group__1 : ( ( rule__InstrUn__DistAssignment_1 ) ) rule__InstrUn__Group__2 ;
    public final void rule__InstrUn__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:453:1: ( ( ( rule__InstrUn__DistAssignment_1 ) ) rule__InstrUn__Group__2 )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:454:1: ( ( rule__InstrUn__DistAssignment_1 ) ) rule__InstrUn__Group__2
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:454:1: ( ( rule__InstrUn__DistAssignment_1 ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:455:1: ( rule__InstrUn__DistAssignment_1 )
            {
             before(grammarAccess.getInstrUnAccess().getDistAssignment_1()); 
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:456:1: ( rule__InstrUn__DistAssignment_1 )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:456:2: rule__InstrUn__DistAssignment_1
            {
            pushFollow(FOLLOW_rule__InstrUn__DistAssignment_1_in_rule__InstrUn__Group__1915);
            rule__InstrUn__DistAssignment_1();
            _fsp--;


            }

             after(grammarAccess.getInstrUnAccess().getDistAssignment_1()); 

            }

            pushFollow(FOLLOW_rule__InstrUn__Group__2_in_rule__InstrUn__Group__1924);
            rule__InstrUn__Group__2();
            _fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstrUn__Group__1


    // $ANTLR start rule__InstrUn__Group__2
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:467:1: rule__InstrUn__Group__2 : ( 'cm' ) ;
    public final void rule__InstrUn__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:471:1: ( ( 'cm' ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:472:1: ( 'cm' )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:472:1: ( 'cm' )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:473:1: 'cm'
            {
             before(grammarAccess.getInstrUnAccess().getCmKeyword_2()); 
            match(input,21,FOLLOW_21_in_rule__InstrUn__Group__2953); 
             after(grammarAccess.getInstrUnAccess().getCmKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstrUn__Group__2


    // $ANTLR start rule__InstrDeg__Group__0
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:492:1: rule__InstrDeg__Group__0 : ( ( rule__InstrDeg__NomAssignment_0 ) ) rule__InstrDeg__Group__1 ;
    public final void rule__InstrDeg__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:496:1: ( ( ( rule__InstrDeg__NomAssignment_0 ) ) rule__InstrDeg__Group__1 )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:497:1: ( ( rule__InstrDeg__NomAssignment_0 ) ) rule__InstrDeg__Group__1
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:497:1: ( ( rule__InstrDeg__NomAssignment_0 ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:498:1: ( rule__InstrDeg__NomAssignment_0 )
            {
             before(grammarAccess.getInstrDegAccess().getNomAssignment_0()); 
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:499:1: ( rule__InstrDeg__NomAssignment_0 )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:499:2: rule__InstrDeg__NomAssignment_0
            {
            pushFollow(FOLLOW_rule__InstrDeg__NomAssignment_0_in_rule__InstrDeg__Group__0994);
            rule__InstrDeg__NomAssignment_0();
            _fsp--;


            }

             after(grammarAccess.getInstrDegAccess().getNomAssignment_0()); 

            }

            pushFollow(FOLLOW_rule__InstrDeg__Group__1_in_rule__InstrDeg__Group__01003);
            rule__InstrDeg__Group__1();
            _fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstrDeg__Group__0


    // $ANTLR start rule__InstrDeg__Group__1
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:510:1: rule__InstrDeg__Group__1 : ( ( rule__InstrDeg__AngleAssignment_1 ) ) rule__InstrDeg__Group__2 ;
    public final void rule__InstrDeg__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:514:1: ( ( ( rule__InstrDeg__AngleAssignment_1 ) ) rule__InstrDeg__Group__2 )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:515:1: ( ( rule__InstrDeg__AngleAssignment_1 ) ) rule__InstrDeg__Group__2
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:515:1: ( ( rule__InstrDeg__AngleAssignment_1 ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:516:1: ( rule__InstrDeg__AngleAssignment_1 )
            {
             before(grammarAccess.getInstrDegAccess().getAngleAssignment_1()); 
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:517:1: ( rule__InstrDeg__AngleAssignment_1 )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:517:2: rule__InstrDeg__AngleAssignment_1
            {
            pushFollow(FOLLOW_rule__InstrDeg__AngleAssignment_1_in_rule__InstrDeg__Group__11031);
            rule__InstrDeg__AngleAssignment_1();
            _fsp--;


            }

             after(grammarAccess.getInstrDegAccess().getAngleAssignment_1()); 

            }

            pushFollow(FOLLOW_rule__InstrDeg__Group__2_in_rule__InstrDeg__Group__11040);
            rule__InstrDeg__Group__2();
            _fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstrDeg__Group__1


    // $ANTLR start rule__InstrDeg__Group__2
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:528:1: rule__InstrDeg__Group__2 : ( 'degres' ) ;
    public final void rule__InstrDeg__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:532:1: ( ( 'degres' ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:533:1: ( 'degres' )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:533:1: ( 'degres' )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:534:1: 'degres'
            {
             before(grammarAccess.getInstrDegAccess().getDegresKeyword_2()); 
            match(input,22,FOLLOW_22_in_rule__InstrDeg__Group__21069); 
             after(grammarAccess.getInstrDegAccess().getDegresKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstrDeg__Group__2


    // $ANTLR start rule__InstructionsSetDsl__InstructionsAssignment_1
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:553:1: rule__InstructionsSetDsl__InstructionsAssignment_1 : ( ruleInstructionDsl ) ;
    public final void rule__InstructionsSetDsl__InstructionsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:557:1: ( ( ruleInstructionDsl ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:558:1: ( ruleInstructionDsl )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:558:1: ( ruleInstructionDsl )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:559:1: ruleInstructionDsl
            {
             before(grammarAccess.getInstructionsSetDslAccess().getInstructionsInstructionDslParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleInstructionDsl_in_rule__InstructionsSetDsl__InstructionsAssignment_11110);
            ruleInstructionDsl();
            _fsp--;

             after(grammarAccess.getInstructionsSetDslAccess().getInstructionsInstructionDslParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstructionsSetDsl__InstructionsAssignment_1


    // $ANTLR start rule__InstrUn__NomAssignment_0
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:568:1: rule__InstrUn__NomAssignment_0 : ( ruleNomsUn ) ;
    public final void rule__InstrUn__NomAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:572:1: ( ( ruleNomsUn ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:573:1: ( ruleNomsUn )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:573:1: ( ruleNomsUn )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:574:1: ruleNomsUn
            {
             before(grammarAccess.getInstrUnAccess().getNomNomsUnEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleNomsUn_in_rule__InstrUn__NomAssignment_01141);
            ruleNomsUn();
            _fsp--;

             after(grammarAccess.getInstrUnAccess().getNomNomsUnEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstrUn__NomAssignment_0


    // $ANTLR start rule__InstrUn__DistAssignment_1
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:583:1: rule__InstrUn__DistAssignment_1 : ( RULE_INT ) ;
    public final void rule__InstrUn__DistAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:587:1: ( ( RULE_INT ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:588:1: ( RULE_INT )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:588:1: ( RULE_INT )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:589:1: RULE_INT
            {
             before(grammarAccess.getInstrUnAccess().getDistINTTerminalRuleCall_1_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__InstrUn__DistAssignment_11172); 
             after(grammarAccess.getInstrUnAccess().getDistINTTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstrUn__DistAssignment_1


    // $ANTLR start rule__InstrDeg__NomAssignment_0
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:598:1: rule__InstrDeg__NomAssignment_0 : ( ruleNomsDeg ) ;
    public final void rule__InstrDeg__NomAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:602:1: ( ( ruleNomsDeg ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:603:1: ( ruleNomsDeg )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:603:1: ( ruleNomsDeg )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:604:1: ruleNomsDeg
            {
             before(grammarAccess.getInstrDegAccess().getNomNomsDegEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleNomsDeg_in_rule__InstrDeg__NomAssignment_01203);
            ruleNomsDeg();
            _fsp--;

             after(grammarAccess.getInstrDegAccess().getNomNomsDegEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstrDeg__NomAssignment_0


    // $ANTLR start rule__InstrDeg__AngleAssignment_1
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:613:1: rule__InstrDeg__AngleAssignment_1 : ( RULE_INT ) ;
    public final void rule__InstrDeg__AngleAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:617:1: ( ( RULE_INT ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:618:1: ( RULE_INT )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:618:1: ( RULE_INT )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:619:1: RULE_INT
            {
             before(grammarAccess.getInstrDegAccess().getAngleINTTerminalRuleCall_1_0()); 
            match(input,RULE_INT,FOLLOW_RULE_INT_in_rule__InstrDeg__AngleAssignment_11234); 
             after(grammarAccess.getInstrDegAccess().getAngleINTTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstrDeg__AngleAssignment_1


    // $ANTLR start rule__InstrZero__NomAssignment
    // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:628:1: rule__InstrZero__NomAssignment : ( ruleNomsZero ) ;
    public final void rule__InstrZero__NomAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:632:1: ( ( ruleNomsZero ) )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:633:1: ( ruleNomsZero )
            {
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:633:1: ( ruleNomsZero )
            // ../lego.ui/src-gen/ch/unige/cui/smv/contentassist/antlr/internal/InternalLego.g:634:1: ruleNomsZero
            {
             before(grammarAccess.getInstrZeroAccess().getNomNomsZeroEnumRuleCall_0()); 
            pushFollow(FOLLOW_ruleNomsZero_in_rule__InstrZero__NomAssignment1265);
            ruleNomsZero();
            _fsp--;

             after(grammarAccess.getInstrZeroAccess().getNomNomsZeroEnumRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end rule__InstrZero__NomAssignment


 

    public static final BitSet FOLLOW_ruleInstructionsSetDsl_in_entryRuleInstructionsSetDsl60 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstructionsSetDsl67 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InstructionsSetDsl__Group__0_in_ruleInstructionsSetDsl94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstrUn_in_entryRuleInstrUn120 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstrUn127 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InstrUn__Group__0_in_ruleInstrUn154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstrDeg_in_entryRuleInstrDeg180 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstrDeg187 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InstrDeg__Group__0_in_ruleInstrDeg214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstrZero_in_entryRuleInstrZero240 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstrZero247 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InstrZero__NomAssignment_in_ruleInstrZero274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstructionDsl_in_entryRuleInstructionDsl300 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstructionDsl307 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InstructionDsl__Alternatives_in_ruleInstructionDsl334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NomsUn__Alternatives_in_ruleNomsUn371 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NomsDeg__Alternatives_in_ruleNomsDeg407 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__NomsZero__Alternatives_in_ruleNomsZero443 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstrUn_in_rule__InstructionDsl__Alternatives478 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstrZero_in_rule__InstructionDsl__Alternatives495 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstrDeg_in_rule__InstructionDsl__Alternatives512 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__NomsUn__Alternatives545 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__NomsUn__Alternatives566 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__NomsDeg__Alternatives602 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__NomsDeg__Alternatives623 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__NomsZero__Alternatives659 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__NomsZero__Alternatives680 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__NomsZero__Alternatives701 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__NomsZero__Alternatives722 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__InstructionsSetDsl__Group__0760 = new BitSet(new long[]{0x000000000017F800L});
    public static final BitSet FOLLOW_rule__InstructionsSetDsl__Group__1_in_rule__InstructionsSetDsl__Group__0770 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InstructionsSetDsl__InstructionsAssignment_1_in_rule__InstructionsSetDsl__Group__1798 = new BitSet(new long[]{0x000000000017F800L});
    public static final BitSet FOLLOW_rule__InstructionsSetDsl__Group__2_in_rule__InstructionsSetDsl__Group__1808 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__InstructionsSetDsl__Group__2837 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InstrUn__NomAssignment_0_in_rule__InstrUn__Group__0878 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__InstrUn__Group__1_in_rule__InstrUn__Group__0887 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InstrUn__DistAssignment_1_in_rule__InstrUn__Group__1915 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__InstrUn__Group__2_in_rule__InstrUn__Group__1924 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__InstrUn__Group__2953 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InstrDeg__NomAssignment_0_in_rule__InstrDeg__Group__0994 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__InstrDeg__Group__1_in_rule__InstrDeg__Group__01003 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__InstrDeg__AngleAssignment_1_in_rule__InstrDeg__Group__11031 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__InstrDeg__Group__2_in_rule__InstrDeg__Group__11040 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__InstrDeg__Group__21069 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstructionDsl_in_rule__InstructionsSetDsl__InstructionsAssignment_11110 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNomsUn_in_rule__InstrUn__NomAssignment_01141 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__InstrUn__DistAssignment_11172 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNomsDeg_in_rule__InstrDeg__NomAssignment_01203 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_INT_in_rule__InstrDeg__AngleAssignment_11234 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNomsZero_in_rule__InstrZero__NomAssignment1265 = new BitSet(new long[]{0x0000000000000002L});

}