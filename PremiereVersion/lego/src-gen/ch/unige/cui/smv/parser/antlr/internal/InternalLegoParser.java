package ch.unige.cui.smv.parser.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.xtext.parsetree.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.eclipse.xtext.conversion.ValueConverterException;
import ch.unige.cui.smv.services.LegoGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class InternalLegoParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INT", "RULE_ID", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'DEBUT'", "'FIN'", "'cm'", "'degres'", "'avance de'", "'recule de'", "'tourne \\uFFFD gauche de'", "'tourne \\uFFFD droite de'", "'ferme la pince'", "'ouvre la pince'", "'tourne \\uFFFD gauche'", "'tourne \\uFFFD droite'"
    };
    public static final int RULE_ID=5;
    public static final int RULE_STRING=6;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=4;
    public static final int RULE_WS=9;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;

        public InternalLegoParser(TokenStream input) {
            super(input);
        }
        

    public String[] getTokenNames() { return tokenNames; }
    public String getGrammarFileName() { return "../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g"; }


     
     	private LegoGrammarAccess grammarAccess;
     	
        public InternalLegoParser(TokenStream input, IAstFactory factory, LegoGrammarAccess grammarAccess) {
            this(input);
            this.factory = factory;
            registerRules(grammarAccess.getGrammar());
            this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected InputStream getTokenFile() {
        	ClassLoader classLoader = getClass().getClassLoader();
        	return classLoader.getResourceAsStream("ch/unige/cui/smv/parser/antlr/internal/InternalLego.tokens");
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "InstructionsSetDsl";	
       	} 



    // $ANTLR start entryRuleInstructionsSetDsl
    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:73:1: entryRuleInstructionsSetDsl returns [EObject current=null] : iv_ruleInstructionsSetDsl= ruleInstructionsSetDsl EOF ;
    public final EObject entryRuleInstructionsSetDsl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstructionsSetDsl = null;


        try {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:73:60: (iv_ruleInstructionsSetDsl= ruleInstructionsSetDsl EOF )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:74:2: iv_ruleInstructionsSetDsl= ruleInstructionsSetDsl EOF
            {
             currentNode = createCompositeNode(grammarAccess.getInstructionsSetDslRule(), currentNode); 
            pushFollow(FOLLOW_ruleInstructionsSetDsl_in_entryRuleInstructionsSetDsl73);
            iv_ruleInstructionsSetDsl=ruleInstructionsSetDsl();
            _fsp--;

             current =iv_ruleInstructionsSetDsl; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstructionsSetDsl83); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end entryRuleInstructionsSetDsl


    // $ANTLR start ruleInstructionsSetDsl
    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:81:1: ruleInstructionsSetDsl returns [EObject current=null] : ( 'DEBUT' (lv_instructions_1= ruleInstructionDsl )* 'FIN' ) ;
    public final EObject ruleInstructionsSetDsl() throws RecognitionException {
        EObject current = null;

        EObject lv_instructions_1 = null;


         EObject temp=null; setCurrentLookahead(); resetLookahead(); 
            
        try {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:86:6: ( ( 'DEBUT' (lv_instructions_1= ruleInstructionDsl )* 'FIN' ) )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:87:1: ( 'DEBUT' (lv_instructions_1= ruleInstructionDsl )* 'FIN' )
            {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:87:1: ( 'DEBUT' (lv_instructions_1= ruleInstructionDsl )* 'FIN' )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:87:2: 'DEBUT' (lv_instructions_1= ruleInstructionDsl )* 'FIN'
            {
            match(input,11,FOLLOW_11_in_ruleInstructionsSetDsl117); 

                    createLeafNode(grammarAccess.getInstructionsSetDslAccess().getDEBUTKeyword_0(), null); 
                
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:91:1: (lv_instructions_1= ruleInstructionDsl )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=15 && LA1_0<=22)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:94:6: lv_instructions_1= ruleInstructionDsl
            	    {
            	     
            	    	        currentNode=createCompositeNode(grammarAccess.getInstructionsSetDslAccess().getInstructionsInstructionDslParserRuleCall_1_0(), currentNode); 
            	    	    
            	    pushFollow(FOLLOW_ruleInstructionDsl_in_ruleInstructionsSetDsl151);
            	    lv_instructions_1=ruleInstructionDsl();
            	    _fsp--;


            	    	        if (current==null) {
            	    	            current = factory.create(grammarAccess.getInstructionsSetDslRule().getType().getClassifier());
            	    	            associateNodeWithAstElement(currentNode.getParent(), current);
            	    	        }
            	    	        
            	    	        try {
            	    	       		add(current, "instructions", lv_instructions_1, "InstructionDsl", currentNode);
            	    	        } catch (ValueConverterException vce) {
            	    				handleValueConverterException(vce);
            	    	        }
            	    	        currentNode = currentNode.getParent();
            	    	    

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            match(input,12,FOLLOW_12_in_ruleInstructionsSetDsl165); 

                    createLeafNode(grammarAccess.getInstructionsSetDslAccess().getFINKeyword_2(), null); 
                

            }


            }

             resetLookahead(); 
                	lastConsumedNode = currentNode;
                
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end ruleInstructionsSetDsl


    // $ANTLR start entryRuleInstrUn
    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:123:1: entryRuleInstrUn returns [EObject current=null] : iv_ruleInstrUn= ruleInstrUn EOF ;
    public final EObject entryRuleInstrUn() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstrUn = null;


        try {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:123:49: (iv_ruleInstrUn= ruleInstrUn EOF )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:124:2: iv_ruleInstrUn= ruleInstrUn EOF
            {
             currentNode = createCompositeNode(grammarAccess.getInstrUnRule(), currentNode); 
            pushFollow(FOLLOW_ruleInstrUn_in_entryRuleInstrUn198);
            iv_ruleInstrUn=ruleInstrUn();
            _fsp--;

             current =iv_ruleInstrUn; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstrUn208); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end entryRuleInstrUn


    // $ANTLR start ruleInstrUn
    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:131:1: ruleInstrUn returns [EObject current=null] : ( (lv_nom_0= ruleNomsUn ) (lv_dist_1= RULE_INT ) 'cm' ) ;
    public final EObject ruleInstrUn() throws RecognitionException {
        EObject current = null;

        Token lv_dist_1=null;
        Enumerator lv_nom_0 = null;


         EObject temp=null; setCurrentLookahead(); resetLookahead(); 
            
        try {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:136:6: ( ( (lv_nom_0= ruleNomsUn ) (lv_dist_1= RULE_INT ) 'cm' ) )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:137:1: ( (lv_nom_0= ruleNomsUn ) (lv_dist_1= RULE_INT ) 'cm' )
            {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:137:1: ( (lv_nom_0= ruleNomsUn ) (lv_dist_1= RULE_INT ) 'cm' )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:137:2: (lv_nom_0= ruleNomsUn ) (lv_dist_1= RULE_INT ) 'cm'
            {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:137:2: (lv_nom_0= ruleNomsUn )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:140:6: lv_nom_0= ruleNomsUn
            {
             
            	        currentNode=createCompositeNode(grammarAccess.getInstrUnAccess().getNomNomsUnEnumRuleCall_0_0(), currentNode); 
            	    
            pushFollow(FOLLOW_ruleNomsUn_in_ruleInstrUn267);
            lv_nom_0=ruleNomsUn();
            _fsp--;


            	        if (current==null) {
            	            current = factory.create(grammarAccess.getInstrUnRule().getType().getClassifier());
            	            associateNodeWithAstElement(currentNode.getParent(), current);
            	        }
            	        
            	        try {
            	       		set(current, "nom", lv_nom_0, "NomsUn", lastConsumedNode);
            	        } catch (ValueConverterException vce) {
            				handleValueConverterException(vce);
            	        }
            	        currentNode = currentNode.getParent();
            	    

            }

            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:158:2: (lv_dist_1= RULE_INT )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:160:6: lv_dist_1= RULE_INT
            {
            lv_dist_1=(Token)input.LT(1);
            match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleInstrUn293); 

            		createLeafNode(grammarAccess.getInstrUnAccess().getDistINTTerminalRuleCall_1_0(), "dist"); 
            	

            	        if (current==null) {
            	            current = factory.create(grammarAccess.getInstrUnRule().getType().getClassifier());
            	            associateNodeWithAstElement(currentNode, current);
            	        }
            	        
            	        try {
            	       		set(current, "dist", lv_dist_1, "INT", lastConsumedNode);
            	        } catch (ValueConverterException vce) {
            				handleValueConverterException(vce);
            	        }
            	    

            }

            match(input,13,FOLLOW_13_in_ruleInstrUn310); 

                    createLeafNode(grammarAccess.getInstrUnAccess().getCmKeyword_2(), null); 
                

            }


            }

             resetLookahead(); 
                	lastConsumedNode = currentNode;
                
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end ruleInstrUn


    // $ANTLR start entryRuleInstrDeg
    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:189:1: entryRuleInstrDeg returns [EObject current=null] : iv_ruleInstrDeg= ruleInstrDeg EOF ;
    public final EObject entryRuleInstrDeg() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstrDeg = null;


        try {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:189:50: (iv_ruleInstrDeg= ruleInstrDeg EOF )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:190:2: iv_ruleInstrDeg= ruleInstrDeg EOF
            {
             currentNode = createCompositeNode(grammarAccess.getInstrDegRule(), currentNode); 
            pushFollow(FOLLOW_ruleInstrDeg_in_entryRuleInstrDeg343);
            iv_ruleInstrDeg=ruleInstrDeg();
            _fsp--;

             current =iv_ruleInstrDeg; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstrDeg353); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end entryRuleInstrDeg


    // $ANTLR start ruleInstrDeg
    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:197:1: ruleInstrDeg returns [EObject current=null] : ( (lv_nom_0= ruleNomsDeg ) (lv_angle_1= RULE_INT ) 'degres' ) ;
    public final EObject ruleInstrDeg() throws RecognitionException {
        EObject current = null;

        Token lv_angle_1=null;
        Enumerator lv_nom_0 = null;


         EObject temp=null; setCurrentLookahead(); resetLookahead(); 
            
        try {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:202:6: ( ( (lv_nom_0= ruleNomsDeg ) (lv_angle_1= RULE_INT ) 'degres' ) )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:203:1: ( (lv_nom_0= ruleNomsDeg ) (lv_angle_1= RULE_INT ) 'degres' )
            {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:203:1: ( (lv_nom_0= ruleNomsDeg ) (lv_angle_1= RULE_INT ) 'degres' )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:203:2: (lv_nom_0= ruleNomsDeg ) (lv_angle_1= RULE_INT ) 'degres'
            {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:203:2: (lv_nom_0= ruleNomsDeg )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:206:6: lv_nom_0= ruleNomsDeg
            {
             
            	        currentNode=createCompositeNode(grammarAccess.getInstrDegAccess().getNomNomsDegEnumRuleCall_0_0(), currentNode); 
            	    
            pushFollow(FOLLOW_ruleNomsDeg_in_ruleInstrDeg412);
            lv_nom_0=ruleNomsDeg();
            _fsp--;


            	        if (current==null) {
            	            current = factory.create(grammarAccess.getInstrDegRule().getType().getClassifier());
            	            associateNodeWithAstElement(currentNode.getParent(), current);
            	        }
            	        
            	        try {
            	       		set(current, "nom", lv_nom_0, "NomsDeg", lastConsumedNode);
            	        } catch (ValueConverterException vce) {
            				handleValueConverterException(vce);
            	        }
            	        currentNode = currentNode.getParent();
            	    

            }

            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:224:2: (lv_angle_1= RULE_INT )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:226:6: lv_angle_1= RULE_INT
            {
            lv_angle_1=(Token)input.LT(1);
            match(input,RULE_INT,FOLLOW_RULE_INT_in_ruleInstrDeg438); 

            		createLeafNode(grammarAccess.getInstrDegAccess().getAngleINTTerminalRuleCall_1_0(), "angle"); 
            	

            	        if (current==null) {
            	            current = factory.create(grammarAccess.getInstrDegRule().getType().getClassifier());
            	            associateNodeWithAstElement(currentNode, current);
            	        }
            	        
            	        try {
            	       		set(current, "angle", lv_angle_1, "INT", lastConsumedNode);
            	        } catch (ValueConverterException vce) {
            				handleValueConverterException(vce);
            	        }
            	    

            }

            match(input,14,FOLLOW_14_in_ruleInstrDeg455); 

                    createLeafNode(grammarAccess.getInstrDegAccess().getDegresKeyword_2(), null); 
                

            }


            }

             resetLookahead(); 
                	lastConsumedNode = currentNode;
                
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end ruleInstrDeg


    // $ANTLR start entryRuleInstrZero
    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:255:1: entryRuleInstrZero returns [EObject current=null] : iv_ruleInstrZero= ruleInstrZero EOF ;
    public final EObject entryRuleInstrZero() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstrZero = null;


        try {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:255:51: (iv_ruleInstrZero= ruleInstrZero EOF )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:256:2: iv_ruleInstrZero= ruleInstrZero EOF
            {
             currentNode = createCompositeNode(grammarAccess.getInstrZeroRule(), currentNode); 
            pushFollow(FOLLOW_ruleInstrZero_in_entryRuleInstrZero488);
            iv_ruleInstrZero=ruleInstrZero();
            _fsp--;

             current =iv_ruleInstrZero; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstrZero498); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end entryRuleInstrZero


    // $ANTLR start ruleInstrZero
    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:263:1: ruleInstrZero returns [EObject current=null] : (lv_nom_0= ruleNomsZero ) ;
    public final EObject ruleInstrZero() throws RecognitionException {
        EObject current = null;

        Enumerator lv_nom_0 = null;


         EObject temp=null; setCurrentLookahead(); resetLookahead(); 
            
        try {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:268:6: ( (lv_nom_0= ruleNomsZero ) )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:269:1: (lv_nom_0= ruleNomsZero )
            {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:269:1: (lv_nom_0= ruleNomsZero )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:272:6: lv_nom_0= ruleNomsZero
            {
             
            	        currentNode=createCompositeNode(grammarAccess.getInstrZeroAccess().getNomNomsZeroEnumRuleCall_0(), currentNode); 
            	    
            pushFollow(FOLLOW_ruleNomsZero_in_ruleInstrZero556);
            lv_nom_0=ruleNomsZero();
            _fsp--;


            	        if (current==null) {
            	            current = factory.create(grammarAccess.getInstrZeroRule().getType().getClassifier());
            	            associateNodeWithAstElement(currentNode.getParent(), current);
            	        }
            	        
            	        try {
            	       		set(current, "nom", lv_nom_0, "NomsZero", lastConsumedNode);
            	        } catch (ValueConverterException vce) {
            				handleValueConverterException(vce);
            	        }
            	        currentNode = currentNode.getParent();
            	    

            }


            }

             resetLookahead(); 
                	lastConsumedNode = currentNode;
                
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end ruleInstrZero


    // $ANTLR start entryRuleInstructionDsl
    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:297:1: entryRuleInstructionDsl returns [EObject current=null] : iv_ruleInstructionDsl= ruleInstructionDsl EOF ;
    public final EObject entryRuleInstructionDsl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstructionDsl = null;


        try {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:297:56: (iv_ruleInstructionDsl= ruleInstructionDsl EOF )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:298:2: iv_ruleInstructionDsl= ruleInstructionDsl EOF
            {
             currentNode = createCompositeNode(grammarAccess.getInstructionDslRule(), currentNode); 
            pushFollow(FOLLOW_ruleInstructionDsl_in_entryRuleInstructionDsl592);
            iv_ruleInstructionDsl=ruleInstructionDsl();
            _fsp--;

             current =iv_ruleInstructionDsl; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstructionDsl602); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end entryRuleInstructionDsl


    // $ANTLR start ruleInstructionDsl
    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:305:1: ruleInstructionDsl returns [EObject current=null] : (this_InstrUn_0= ruleInstrUn | this_InstrZero_1= ruleInstrZero | this_InstrDeg_2= ruleInstrDeg ) ;
    public final EObject ruleInstructionDsl() throws RecognitionException {
        EObject current = null;

        EObject this_InstrUn_0 = null;

        EObject this_InstrZero_1 = null;

        EObject this_InstrDeg_2 = null;


         EObject temp=null; setCurrentLookahead(); resetLookahead(); 
            
        try {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:310:6: ( (this_InstrUn_0= ruleInstrUn | this_InstrZero_1= ruleInstrZero | this_InstrDeg_2= ruleInstrDeg ) )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:311:1: (this_InstrUn_0= ruleInstrUn | this_InstrZero_1= ruleInstrZero | this_InstrDeg_2= ruleInstrDeg )
            {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:311:1: (this_InstrUn_0= ruleInstrUn | this_InstrZero_1= ruleInstrZero | this_InstrDeg_2= ruleInstrDeg )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 15:
            case 16:
                {
                alt2=1;
                }
                break;
            case 19:
            case 20:
            case 21:
            case 22:
                {
                alt2=2;
                }
                break;
            case 17:
            case 18:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("311:1: (this_InstrUn_0= ruleInstrUn | this_InstrZero_1= ruleInstrZero | this_InstrDeg_2= ruleInstrDeg )", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:312:5: this_InstrUn_0= ruleInstrUn
                    {
                     
                            currentNode=createCompositeNode(grammarAccess.getInstructionDslAccess().getInstrUnParserRuleCall_0(), currentNode); 
                        
                    pushFollow(FOLLOW_ruleInstrUn_in_ruleInstructionDsl649);
                    this_InstrUn_0=ruleInstrUn();
                    _fsp--;

                     
                            current = this_InstrUn_0; 
                            currentNode = currentNode.getParent();
                        

                    }
                    break;
                case 2 :
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:322:5: this_InstrZero_1= ruleInstrZero
                    {
                     
                            currentNode=createCompositeNode(grammarAccess.getInstructionDslAccess().getInstrZeroParserRuleCall_1(), currentNode); 
                        
                    pushFollow(FOLLOW_ruleInstrZero_in_ruleInstructionDsl676);
                    this_InstrZero_1=ruleInstrZero();
                    _fsp--;

                     
                            current = this_InstrZero_1; 
                            currentNode = currentNode.getParent();
                        

                    }
                    break;
                case 3 :
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:332:5: this_InstrDeg_2= ruleInstrDeg
                    {
                     
                            currentNode=createCompositeNode(grammarAccess.getInstructionDslAccess().getInstrDegParserRuleCall_2(), currentNode); 
                        
                    pushFollow(FOLLOW_ruleInstrDeg_in_ruleInstructionDsl703);
                    this_InstrDeg_2=ruleInstrDeg();
                    _fsp--;

                     
                            current = this_InstrDeg_2; 
                            currentNode = currentNode.getParent();
                        

                    }
                    break;

            }


            }

             resetLookahead(); 
                	lastConsumedNode = currentNode;
                
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end ruleInstructionDsl


    // $ANTLR start ruleNomsUn
    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:347:1: ruleNomsUn returns [Enumerator current=null] : ( ( 'avance de' ) | ( 'recule de' ) ) ;
    public final Enumerator ruleNomsUn() throws RecognitionException {
        Enumerator current = null;

         setCurrentLookahead(); resetLookahead(); 
        try {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:351:6: ( ( ( 'avance de' ) | ( 'recule de' ) ) )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:352:1: ( ( 'avance de' ) | ( 'recule de' ) )
            {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:352:1: ( ( 'avance de' ) | ( 'recule de' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==15) ) {
                alt3=1;
            }
            else if ( (LA3_0==16) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("352:1: ( ( 'avance de' ) | ( 'recule de' ) )", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:352:2: ( 'avance de' )
                    {
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:352:2: ( 'avance de' )
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:352:4: 'avance de'
                    {
                    match(input,15,FOLLOW_15_in_ruleNomsUn749); 

                            current = grammarAccess.getNomsUnAccess().getAvanceEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            createLeafNode(grammarAccess.getNomsUnAccess().getAvanceEnumLiteralDeclaration_0(), null); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:358:6: ( 'recule de' )
                    {
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:358:6: ( 'recule de' )
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:358:8: 'recule de'
                    {
                    match(input,16,FOLLOW_16_in_ruleNomsUn764); 

                            current = grammarAccess.getNomsUnAccess().getReculeEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            createLeafNode(grammarAccess.getNomsUnAccess().getReculeEnumLiteralDeclaration_1(), null); 
                        

                    }


                    }
                    break;

            }


            }

             resetLookahead(); 
                	lastConsumedNode = currentNode;
                
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end ruleNomsUn


    // $ANTLR start ruleNomsDeg
    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:368:1: ruleNomsDeg returns [Enumerator current=null] : ( ( 'tourne \\uFFFD gauche de' ) | ( 'tourne \\uFFFD droite de' ) ) ;
    public final Enumerator ruleNomsDeg() throws RecognitionException {
        Enumerator current = null;

         setCurrentLookahead(); resetLookahead(); 
        try {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:372:6: ( ( ( 'tourne \\uFFFD gauche de' ) | ( 'tourne \\uFFFD droite de' ) ) )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:373:1: ( ( 'tourne \\uFFFD gauche de' ) | ( 'tourne \\uFFFD droite de' ) )
            {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:373:1: ( ( 'tourne \\uFFFD gauche de' ) | ( 'tourne \\uFFFD droite de' ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==17) ) {
                alt4=1;
            }
            else if ( (LA4_0==18) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("373:1: ( ( 'tourne \\uFFFD gauche de' ) | ( 'tourne \\uFFFD droite de' ) )", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:373:2: ( 'tourne \\uFFFD gauche de' )
                    {
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:373:2: ( 'tourne \\uFFFD gauche de' )
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:373:4: 'tourne \\uFFFD gauche de'
                    {
                    match(input,17,FOLLOW_17_in_ruleNomsDeg807); 

                            current = grammarAccess.getNomsDegAccess().getTournegEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            createLeafNode(grammarAccess.getNomsDegAccess().getTournegEnumLiteralDeclaration_0(), null); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:379:6: ( 'tourne \\uFFFD droite de' )
                    {
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:379:6: ( 'tourne \\uFFFD droite de' )
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:379:8: 'tourne \\uFFFD droite de'
                    {
                    match(input,18,FOLLOW_18_in_ruleNomsDeg822); 

                            current = grammarAccess.getNomsDegAccess().getTournedEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            createLeafNode(grammarAccess.getNomsDegAccess().getTournedEnumLiteralDeclaration_1(), null); 
                        

                    }


                    }
                    break;

            }


            }

             resetLookahead(); 
                	lastConsumedNode = currentNode;
                
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end ruleNomsDeg


    // $ANTLR start ruleNomsZero
    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:389:1: ruleNomsZero returns [Enumerator current=null] : ( ( 'ferme la pince' ) | ( 'ouvre la pince' ) | ( 'tourne \\uFFFD gauche' ) | ( 'tourne \\uFFFD droite' ) ) ;
    public final Enumerator ruleNomsZero() throws RecognitionException {
        Enumerator current = null;

         setCurrentLookahead(); resetLookahead(); 
        try {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:393:6: ( ( ( 'ferme la pince' ) | ( 'ouvre la pince' ) | ( 'tourne \\uFFFD gauche' ) | ( 'tourne \\uFFFD droite' ) ) )
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:394:1: ( ( 'ferme la pince' ) | ( 'ouvre la pince' ) | ( 'tourne \\uFFFD gauche' ) | ( 'tourne \\uFFFD droite' ) )
            {
            // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:394:1: ( ( 'ferme la pince' ) | ( 'ouvre la pince' ) | ( 'tourne \\uFFFD gauche' ) | ( 'tourne \\uFFFD droite' ) )
            int alt5=4;
            switch ( input.LA(1) ) {
            case 19:
                {
                alt5=1;
                }
                break;
            case 20:
                {
                alt5=2;
                }
                break;
            case 21:
                {
                alt5=3;
                }
                break;
            case 22:
                {
                alt5=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("394:1: ( ( 'ferme la pince' ) | ( 'ouvre la pince' ) | ( 'tourne \\uFFFD gauche' ) | ( 'tourne \\uFFFD droite' ) )", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:394:2: ( 'ferme la pince' )
                    {
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:394:2: ( 'ferme la pince' )
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:394:4: 'ferme la pince'
                    {
                    match(input,19,FOLLOW_19_in_ruleNomsZero865); 

                            current = grammarAccess.getNomsZeroAccess().getFermeEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                            createLeafNode(grammarAccess.getNomsZeroAccess().getFermeEnumLiteralDeclaration_0(), null); 
                        

                    }


                    }
                    break;
                case 2 :
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:400:6: ( 'ouvre la pince' )
                    {
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:400:6: ( 'ouvre la pince' )
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:400:8: 'ouvre la pince'
                    {
                    match(input,20,FOLLOW_20_in_ruleNomsZero880); 

                            current = grammarAccess.getNomsZeroAccess().getOuvreEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                            createLeafNode(grammarAccess.getNomsZeroAccess().getOuvreEnumLiteralDeclaration_1(), null); 
                        

                    }


                    }
                    break;
                case 3 :
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:406:6: ( 'tourne \\uFFFD gauche' )
                    {
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:406:6: ( 'tourne \\uFFFD gauche' )
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:406:8: 'tourne \\uFFFD gauche'
                    {
                    match(input,21,FOLLOW_21_in_ruleNomsZero895); 

                            current = grammarAccess.getNomsZeroAccess().getTournegEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                            createLeafNode(grammarAccess.getNomsZeroAccess().getTournegEnumLiteralDeclaration_2(), null); 
                        

                    }


                    }
                    break;
                case 4 :
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:412:6: ( 'tourne \\uFFFD droite' )
                    {
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:412:6: ( 'tourne \\uFFFD droite' )
                    // ../lego/src-gen/ch/unige/cui/smv/parser/antlr/internal/InternalLego.g:412:8: 'tourne \\uFFFD droite'
                    {
                    match(input,22,FOLLOW_22_in_ruleNomsZero910); 

                            current = grammarAccess.getNomsZeroAccess().getTournedEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                            createLeafNode(grammarAccess.getNomsZeroAccess().getTournedEnumLiteralDeclaration_3(), null); 
                        

                    }


                    }
                    break;

            }


            }

             resetLookahead(); 
                	lastConsumedNode = currentNode;
                
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end ruleNomsZero


 

    public static final BitSet FOLLOW_ruleInstructionsSetDsl_in_entryRuleInstructionsSetDsl73 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstructionsSetDsl83 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleInstructionsSetDsl117 = new BitSet(new long[]{0x00000000007F9000L});
    public static final BitSet FOLLOW_ruleInstructionDsl_in_ruleInstructionsSetDsl151 = new BitSet(new long[]{0x00000000007F9000L});
    public static final BitSet FOLLOW_12_in_ruleInstructionsSetDsl165 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstrUn_in_entryRuleInstrUn198 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstrUn208 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNomsUn_in_ruleInstrUn267 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleInstrUn293 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleInstrUn310 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstrDeg_in_entryRuleInstrDeg343 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstrDeg353 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNomsDeg_in_ruleInstrDeg412 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_INT_in_ruleInstrDeg438 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleInstrDeg455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstrZero_in_entryRuleInstrZero488 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstrZero498 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleNomsZero_in_ruleInstrZero556 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstructionDsl_in_entryRuleInstructionDsl592 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstructionDsl602 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstrUn_in_ruleInstructionDsl649 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstrZero_in_ruleInstructionDsl676 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstrDeg_in_ruleInstructionDsl703 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleNomsUn749 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_ruleNomsUn764 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_ruleNomsDeg807 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleNomsDeg822 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleNomsZero865 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleNomsZero880 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleNomsZero895 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleNomsZero910 = new BitSet(new long[]{0x0000000000000002L});

}