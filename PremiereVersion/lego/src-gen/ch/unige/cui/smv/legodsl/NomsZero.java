/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Noms Zero</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see ch.unige.cui.smv.legodsl.LegodslPackage#getNomsZero()
 * @model
 * @generated
 */
public enum NomsZero implements Enumerator
{
  /**
   * The '<em><b>Ferme</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #FERME_VALUE
   * @generated
   * @ordered
   */
  FERME(0, "ferme", "ferme la pince"),

  /**
   * The '<em><b>Ouvre</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #OUVRE_VALUE
   * @generated
   * @ordered
   */
  OUVRE(1, "ouvre", "ouvre la pince"),

  /**
   * The '<em><b>Tourneg</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #TOURNEG_VALUE
   * @generated
   * @ordered
   */
  TOURNEG(2, "tourneg", "tourne � gauche"),

  /**
   * The '<em><b>Tourned</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #TOURNED_VALUE
   * @generated
   * @ordered
   */
  TOURNED(3, "tourned", "tourne � droite");

  /**
   * The '<em><b>Ferme</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Ferme</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #FERME
   * @model name="ferme" literal="ferme la pince"
   * @generated
   * @ordered
   */
  public static final int FERME_VALUE = 0;

  /**
   * The '<em><b>Ouvre</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Ouvre</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #OUVRE
   * @model name="ouvre" literal="ouvre la pince"
   * @generated
   * @ordered
   */
  public static final int OUVRE_VALUE = 1;

  /**
   * The '<em><b>Tourneg</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Tourneg</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #TOURNEG
   * @model name="tourneg" literal="tourne � gauche"
   * @generated
   * @ordered
   */
  public static final int TOURNEG_VALUE = 2;

  /**
   * The '<em><b>Tourned</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Tourned</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #TOURNED
   * @model name="tourned" literal="tourne � droite"
   * @generated
   * @ordered
   */
  public static final int TOURNED_VALUE = 3;

  /**
   * An array of all the '<em><b>Noms Zero</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final NomsZero[] VALUES_ARRAY =
    new NomsZero[]
    {
      FERME,
      OUVRE,
      TOURNEG,
      TOURNED,
    };

  /**
   * A public read-only list of all the '<em><b>Noms Zero</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List<NomsZero> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>Noms Zero</b></em>' literal with the specified literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static NomsZero get(String literal)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      NomsZero result = VALUES_ARRAY[i];
      if (result.toString().equals(literal))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>Noms Zero</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static NomsZero getByName(String name)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      NomsZero result = VALUES_ARRAY[i];
      if (result.getName().equals(name))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>Noms Zero</b></em>' literal with the specified integer value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static NomsZero get(int value)
  {
    switch (value)
    {
      case FERME_VALUE: return FERME;
      case OUVRE_VALUE: return OUVRE;
      case TOURNEG_VALUE: return TOURNEG;
      case TOURNED_VALUE: return TOURNED;
    }
    return null;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final int value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String name;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String literal;

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private NomsZero(int value, String name, String literal)
  {
    this.value = value;
    this.name = name;
    this.literal = literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLiteral()
  {
    return literal;
  }

  /**
   * Returns the literal value of the enumerator, which is its string representation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    return literal;
  }
  
} //NomsZero
