/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl.impl;

import ch.unige.cui.smv.legodsl.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class LegodslFactoryImpl extends EFactoryImpl implements LegodslFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static LegodslFactory init()
  {
    try
    {
      LegodslFactory theLegodslFactory = (LegodslFactory)EPackage.Registry.INSTANCE.getEFactory("http://www.unige.ch/cui/smv/lego"); 
      if (theLegodslFactory != null)
      {
        return theLegodslFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new LegodslFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LegodslFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case LegodslPackage.INSTRUCTIONS_SET_DSL: return createInstructionsSetDsl();
      case LegodslPackage.INSTR_UN: return createInstrUn();
      case LegodslPackage.INSTR_DEG: return createInstrDeg();
      case LegodslPackage.INSTR_ZERO: return createInstrZero();
      case LegodslPackage.INSTRUCTION_DSL: return createInstructionDsl();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object createFromString(EDataType eDataType, String initialValue)
  {
    switch (eDataType.getClassifierID())
    {
      case LegodslPackage.NOMS_UN:
        return createNomsUnFromString(eDataType, initialValue);
      case LegodslPackage.NOMS_DEG:
        return createNomsDegFromString(eDataType, initialValue);
      case LegodslPackage.NOMS_ZERO:
        return createNomsZeroFromString(eDataType, initialValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String convertToString(EDataType eDataType, Object instanceValue)
  {
    switch (eDataType.getClassifierID())
    {
      case LegodslPackage.NOMS_UN:
        return convertNomsUnToString(eDataType, instanceValue);
      case LegodslPackage.NOMS_DEG:
        return convertNomsDegToString(eDataType, instanceValue);
      case LegodslPackage.NOMS_ZERO:
        return convertNomsZeroToString(eDataType, instanceValue);
      default:
        throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InstructionsSetDsl createInstructionsSetDsl()
  {
    InstructionsSetDslImpl instructionsSetDsl = new InstructionsSetDslImpl();
    return instructionsSetDsl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InstrUn createInstrUn()
  {
    InstrUnImpl instrUn = new InstrUnImpl();
    return instrUn;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InstrDeg createInstrDeg()
  {
    InstrDegImpl instrDeg = new InstrDegImpl();
    return instrDeg;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InstrZero createInstrZero()
  {
    InstrZeroImpl instrZero = new InstrZeroImpl();
    return instrZero;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InstructionDsl createInstructionDsl()
  {
    InstructionDslImpl instructionDsl = new InstructionDslImpl();
    return instructionDsl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NomsUn createNomsUnFromString(EDataType eDataType, String initialValue)
  {
    NomsUn result = NomsUn.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertNomsUnToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NomsDeg createNomsDegFromString(EDataType eDataType, String initialValue)
  {
    NomsDeg result = NomsDeg.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertNomsDegToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NomsZero createNomsZeroFromString(EDataType eDataType, String initialValue)
  {
    NomsZero result = NomsZero.get(initialValue);
    if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
    return result;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String convertNomsZeroToString(EDataType eDataType, Object instanceValue)
  {
    return instanceValue == null ? null : instanceValue.toString();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LegodslPackage getLegodslPackage()
  {
    return (LegodslPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static LegodslPackage getPackage()
  {
    return LegodslPackage.eINSTANCE;
  }

} //LegodslFactoryImpl
