/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ch.unige.cui.smv.legodsl.LegodslFactory
 * @model kind="package"
 * @generated
 */
public interface LegodslPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "legodsl";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.unige.ch/cui/smv/lego";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "legodsl";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  LegodslPackage eINSTANCE = ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl.init();

  /**
   * The meta object id for the '{@link ch.unige.cui.smv.legodsl.impl.InstructionsSetDslImpl <em>Instructions Set Dsl</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see ch.unige.cui.smv.legodsl.impl.InstructionsSetDslImpl
   * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getInstructionsSetDsl()
   * @generated
   */
  int INSTRUCTIONS_SET_DSL = 0;

  /**
   * The feature id for the '<em><b>Instructions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTRUCTIONS_SET_DSL__INSTRUCTIONS = 0;

  /**
   * The number of structural features of the '<em>Instructions Set Dsl</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTRUCTIONS_SET_DSL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link ch.unige.cui.smv.legodsl.impl.InstructionDslImpl <em>Instruction Dsl</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see ch.unige.cui.smv.legodsl.impl.InstructionDslImpl
   * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getInstructionDsl()
   * @generated
   */
  int INSTRUCTION_DSL = 4;

  /**
   * The number of structural features of the '<em>Instruction Dsl</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTRUCTION_DSL_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link ch.unige.cui.smv.legodsl.impl.InstrUnImpl <em>Instr Un</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see ch.unige.cui.smv.legodsl.impl.InstrUnImpl
   * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getInstrUn()
   * @generated
   */
  int INSTR_UN = 1;

  /**
   * The feature id for the '<em><b>Nom</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTR_UN__NOM = INSTRUCTION_DSL_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Dist</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTR_UN__DIST = INSTRUCTION_DSL_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Instr Un</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTR_UN_FEATURE_COUNT = INSTRUCTION_DSL_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link ch.unige.cui.smv.legodsl.impl.InstrDegImpl <em>Instr Deg</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see ch.unige.cui.smv.legodsl.impl.InstrDegImpl
   * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getInstrDeg()
   * @generated
   */
  int INSTR_DEG = 2;

  /**
   * The feature id for the '<em><b>Nom</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTR_DEG__NOM = INSTRUCTION_DSL_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Angle</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTR_DEG__ANGLE = INSTRUCTION_DSL_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Instr Deg</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTR_DEG_FEATURE_COUNT = INSTRUCTION_DSL_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link ch.unige.cui.smv.legodsl.impl.InstrZeroImpl <em>Instr Zero</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see ch.unige.cui.smv.legodsl.impl.InstrZeroImpl
   * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getInstrZero()
   * @generated
   */
  int INSTR_ZERO = 3;

  /**
   * The feature id for the '<em><b>Nom</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTR_ZERO__NOM = INSTRUCTION_DSL_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Instr Zero</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTR_ZERO_FEATURE_COUNT = INSTRUCTION_DSL_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link ch.unige.cui.smv.legodsl.NomsUn <em>Noms Un</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see ch.unige.cui.smv.legodsl.NomsUn
   * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getNomsUn()
   * @generated
   */
  int NOMS_UN = 5;

  /**
   * The meta object id for the '{@link ch.unige.cui.smv.legodsl.NomsDeg <em>Noms Deg</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see ch.unige.cui.smv.legodsl.NomsDeg
   * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getNomsDeg()
   * @generated
   */
  int NOMS_DEG = 6;

  /**
   * The meta object id for the '{@link ch.unige.cui.smv.legodsl.NomsZero <em>Noms Zero</em>}' enum.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see ch.unige.cui.smv.legodsl.NomsZero
   * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getNomsZero()
   * @generated
   */
  int NOMS_ZERO = 7;


  /**
   * Returns the meta object for class '{@link ch.unige.cui.smv.legodsl.InstructionsSetDsl <em>Instructions Set Dsl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Instructions Set Dsl</em>'.
   * @see ch.unige.cui.smv.legodsl.InstructionsSetDsl
   * @generated
   */
  EClass getInstructionsSetDsl();

  /**
   * Returns the meta object for the containment reference list '{@link ch.unige.cui.smv.legodsl.InstructionsSetDsl#getInstructions <em>Instructions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Instructions</em>'.
   * @see ch.unige.cui.smv.legodsl.InstructionsSetDsl#getInstructions()
   * @see #getInstructionsSetDsl()
   * @generated
   */
  EReference getInstructionsSetDsl_Instructions();

  /**
   * Returns the meta object for class '{@link ch.unige.cui.smv.legodsl.InstrUn <em>Instr Un</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Instr Un</em>'.
   * @see ch.unige.cui.smv.legodsl.InstrUn
   * @generated
   */
  EClass getInstrUn();

  /**
   * Returns the meta object for the attribute '{@link ch.unige.cui.smv.legodsl.InstrUn#getNom <em>Nom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Nom</em>'.
   * @see ch.unige.cui.smv.legodsl.InstrUn#getNom()
   * @see #getInstrUn()
   * @generated
   */
  EAttribute getInstrUn_Nom();

  /**
   * Returns the meta object for the attribute '{@link ch.unige.cui.smv.legodsl.InstrUn#getDist <em>Dist</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Dist</em>'.
   * @see ch.unige.cui.smv.legodsl.InstrUn#getDist()
   * @see #getInstrUn()
   * @generated
   */
  EAttribute getInstrUn_Dist();

  /**
   * Returns the meta object for class '{@link ch.unige.cui.smv.legodsl.InstrDeg <em>Instr Deg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Instr Deg</em>'.
   * @see ch.unige.cui.smv.legodsl.InstrDeg
   * @generated
   */
  EClass getInstrDeg();

  /**
   * Returns the meta object for the attribute '{@link ch.unige.cui.smv.legodsl.InstrDeg#getNom <em>Nom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Nom</em>'.
   * @see ch.unige.cui.smv.legodsl.InstrDeg#getNom()
   * @see #getInstrDeg()
   * @generated
   */
  EAttribute getInstrDeg_Nom();

  /**
   * Returns the meta object for the attribute '{@link ch.unige.cui.smv.legodsl.InstrDeg#getAngle <em>Angle</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Angle</em>'.
   * @see ch.unige.cui.smv.legodsl.InstrDeg#getAngle()
   * @see #getInstrDeg()
   * @generated
   */
  EAttribute getInstrDeg_Angle();

  /**
   * Returns the meta object for class '{@link ch.unige.cui.smv.legodsl.InstrZero <em>Instr Zero</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Instr Zero</em>'.
   * @see ch.unige.cui.smv.legodsl.InstrZero
   * @generated
   */
  EClass getInstrZero();

  /**
   * Returns the meta object for the attribute '{@link ch.unige.cui.smv.legodsl.InstrZero#getNom <em>Nom</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Nom</em>'.
   * @see ch.unige.cui.smv.legodsl.InstrZero#getNom()
   * @see #getInstrZero()
   * @generated
   */
  EAttribute getInstrZero_Nom();

  /**
   * Returns the meta object for class '{@link ch.unige.cui.smv.legodsl.InstructionDsl <em>Instruction Dsl</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Instruction Dsl</em>'.
   * @see ch.unige.cui.smv.legodsl.InstructionDsl
   * @generated
   */
  EClass getInstructionDsl();

  /**
   * Returns the meta object for enum '{@link ch.unige.cui.smv.legodsl.NomsUn <em>Noms Un</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Noms Un</em>'.
   * @see ch.unige.cui.smv.legodsl.NomsUn
   * @generated
   */
  EEnum getNomsUn();

  /**
   * Returns the meta object for enum '{@link ch.unige.cui.smv.legodsl.NomsDeg <em>Noms Deg</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Noms Deg</em>'.
   * @see ch.unige.cui.smv.legodsl.NomsDeg
   * @generated
   */
  EEnum getNomsDeg();

  /**
   * Returns the meta object for enum '{@link ch.unige.cui.smv.legodsl.NomsZero <em>Noms Zero</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for enum '<em>Noms Zero</em>'.
   * @see ch.unige.cui.smv.legodsl.NomsZero
   * @generated
   */
  EEnum getNomsZero();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  LegodslFactory getLegodslFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link ch.unige.cui.smv.legodsl.impl.InstructionsSetDslImpl <em>Instructions Set Dsl</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ch.unige.cui.smv.legodsl.impl.InstructionsSetDslImpl
     * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getInstructionsSetDsl()
     * @generated
     */
    EClass INSTRUCTIONS_SET_DSL = eINSTANCE.getInstructionsSetDsl();

    /**
     * The meta object literal for the '<em><b>Instructions</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference INSTRUCTIONS_SET_DSL__INSTRUCTIONS = eINSTANCE.getInstructionsSetDsl_Instructions();

    /**
     * The meta object literal for the '{@link ch.unige.cui.smv.legodsl.impl.InstrUnImpl <em>Instr Un</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ch.unige.cui.smv.legodsl.impl.InstrUnImpl
     * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getInstrUn()
     * @generated
     */
    EClass INSTR_UN = eINSTANCE.getInstrUn();

    /**
     * The meta object literal for the '<em><b>Nom</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INSTR_UN__NOM = eINSTANCE.getInstrUn_Nom();

    /**
     * The meta object literal for the '<em><b>Dist</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INSTR_UN__DIST = eINSTANCE.getInstrUn_Dist();

    /**
     * The meta object literal for the '{@link ch.unige.cui.smv.legodsl.impl.InstrDegImpl <em>Instr Deg</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ch.unige.cui.smv.legodsl.impl.InstrDegImpl
     * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getInstrDeg()
     * @generated
     */
    EClass INSTR_DEG = eINSTANCE.getInstrDeg();

    /**
     * The meta object literal for the '<em><b>Nom</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INSTR_DEG__NOM = eINSTANCE.getInstrDeg_Nom();

    /**
     * The meta object literal for the '<em><b>Angle</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INSTR_DEG__ANGLE = eINSTANCE.getInstrDeg_Angle();

    /**
     * The meta object literal for the '{@link ch.unige.cui.smv.legodsl.impl.InstrZeroImpl <em>Instr Zero</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ch.unige.cui.smv.legodsl.impl.InstrZeroImpl
     * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getInstrZero()
     * @generated
     */
    EClass INSTR_ZERO = eINSTANCE.getInstrZero();

    /**
     * The meta object literal for the '<em><b>Nom</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INSTR_ZERO__NOM = eINSTANCE.getInstrZero_Nom();

    /**
     * The meta object literal for the '{@link ch.unige.cui.smv.legodsl.impl.InstructionDslImpl <em>Instruction Dsl</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ch.unige.cui.smv.legodsl.impl.InstructionDslImpl
     * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getInstructionDsl()
     * @generated
     */
    EClass INSTRUCTION_DSL = eINSTANCE.getInstructionDsl();

    /**
     * The meta object literal for the '{@link ch.unige.cui.smv.legodsl.NomsUn <em>Noms Un</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ch.unige.cui.smv.legodsl.NomsUn
     * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getNomsUn()
     * @generated
     */
    EEnum NOMS_UN = eINSTANCE.getNomsUn();

    /**
     * The meta object literal for the '{@link ch.unige.cui.smv.legodsl.NomsDeg <em>Noms Deg</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ch.unige.cui.smv.legodsl.NomsDeg
     * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getNomsDeg()
     * @generated
     */
    EEnum NOMS_DEG = eINSTANCE.getNomsDeg();

    /**
     * The meta object literal for the '{@link ch.unige.cui.smv.legodsl.NomsZero <em>Noms Zero</em>}' enum.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see ch.unige.cui.smv.legodsl.NomsZero
     * @see ch.unige.cui.smv.legodsl.impl.LegodslPackageImpl#getNomsZero()
     * @generated
     */
    EEnum NOMS_ZERO = eINSTANCE.getNomsZero();

  }

} //LegodslPackage
