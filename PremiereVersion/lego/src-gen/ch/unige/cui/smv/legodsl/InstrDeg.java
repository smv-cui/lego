/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instr Deg</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ch.unige.cui.smv.legodsl.InstrDeg#getNom <em>Nom</em>}</li>
 *   <li>{@link ch.unige.cui.smv.legodsl.InstrDeg#getAngle <em>Angle</em>}</li>
 * </ul>
 * </p>
 *
 * @see ch.unige.cui.smv.legodsl.LegodslPackage#getInstrDeg()
 * @model
 * @generated
 */
public interface InstrDeg extends InstructionDsl
{
  /**
   * Returns the value of the '<em><b>Nom</b></em>' attribute.
   * The literals are from the enumeration {@link ch.unige.cui.smv.legodsl.NomsDeg}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nom</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nom</em>' attribute.
   * @see ch.unige.cui.smv.legodsl.NomsDeg
   * @see #setNom(NomsDeg)
   * @see ch.unige.cui.smv.legodsl.LegodslPackage#getInstrDeg_Nom()
   * @model
   * @generated
   */
  NomsDeg getNom();

  /**
   * Sets the value of the '{@link ch.unige.cui.smv.legodsl.InstrDeg#getNom <em>Nom</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nom</em>' attribute.
   * @see ch.unige.cui.smv.legodsl.NomsDeg
   * @see #getNom()
   * @generated
   */
  void setNom(NomsDeg value);

  /**
   * Returns the value of the '<em><b>Angle</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Angle</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Angle</em>' attribute.
   * @see #setAngle(int)
   * @see ch.unige.cui.smv.legodsl.LegodslPackage#getInstrDeg_Angle()
   * @model
   * @generated
   */
  int getAngle();

  /**
   * Sets the value of the '{@link ch.unige.cui.smv.legodsl.InstrDeg#getAngle <em>Angle</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Angle</em>' attribute.
   * @see #getAngle()
   * @generated
   */
  void setAngle(int value);

} // InstrDeg
