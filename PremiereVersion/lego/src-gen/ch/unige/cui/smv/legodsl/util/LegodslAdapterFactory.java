/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl.util;

import ch.unige.cui.smv.legodsl.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ch.unige.cui.smv.legodsl.LegodslPackage
 * @generated
 */
public class LegodslAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static LegodslPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LegodslAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = LegodslPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LegodslSwitch<Adapter> modelSwitch =
    new LegodslSwitch<Adapter>()
    {
      @Override
      public Adapter caseInstructionsSetDsl(InstructionsSetDsl object)
      {
        return createInstructionsSetDslAdapter();
      }
      @Override
      public Adapter caseInstrUn(InstrUn object)
      {
        return createInstrUnAdapter();
      }
      @Override
      public Adapter caseInstrDeg(InstrDeg object)
      {
        return createInstrDegAdapter();
      }
      @Override
      public Adapter caseInstrZero(InstrZero object)
      {
        return createInstrZeroAdapter();
      }
      @Override
      public Adapter caseInstructionDsl(InstructionDsl object)
      {
        return createInstructionDslAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link ch.unige.cui.smv.legodsl.InstructionsSetDsl <em>Instructions Set Dsl</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ch.unige.cui.smv.legodsl.InstructionsSetDsl
   * @generated
   */
  public Adapter createInstructionsSetDslAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ch.unige.cui.smv.legodsl.InstrUn <em>Instr Un</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ch.unige.cui.smv.legodsl.InstrUn
   * @generated
   */
  public Adapter createInstrUnAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ch.unige.cui.smv.legodsl.InstrDeg <em>Instr Deg</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ch.unige.cui.smv.legodsl.InstrDeg
   * @generated
   */
  public Adapter createInstrDegAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ch.unige.cui.smv.legodsl.InstrZero <em>Instr Zero</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ch.unige.cui.smv.legodsl.InstrZero
   * @generated
   */
  public Adapter createInstrZeroAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link ch.unige.cui.smv.legodsl.InstructionDsl <em>Instruction Dsl</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see ch.unige.cui.smv.legodsl.InstructionDsl
   * @generated
   */
  public Adapter createInstructionDslAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //LegodslAdapterFactory
