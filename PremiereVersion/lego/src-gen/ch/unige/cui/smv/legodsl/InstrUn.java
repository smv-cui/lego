/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instr Un</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ch.unige.cui.smv.legodsl.InstrUn#getNom <em>Nom</em>}</li>
 *   <li>{@link ch.unige.cui.smv.legodsl.InstrUn#getDist <em>Dist</em>}</li>
 * </ul>
 * </p>
 *
 * @see ch.unige.cui.smv.legodsl.LegodslPackage#getInstrUn()
 * @model
 * @generated
 */
public interface InstrUn extends InstructionDsl
{
  /**
   * Returns the value of the '<em><b>Nom</b></em>' attribute.
   * The literals are from the enumeration {@link ch.unige.cui.smv.legodsl.NomsUn}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nom</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nom</em>' attribute.
   * @see ch.unige.cui.smv.legodsl.NomsUn
   * @see #setNom(NomsUn)
   * @see ch.unige.cui.smv.legodsl.LegodslPackage#getInstrUn_Nom()
   * @model
   * @generated
   */
  NomsUn getNom();

  /**
   * Sets the value of the '{@link ch.unige.cui.smv.legodsl.InstrUn#getNom <em>Nom</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nom</em>' attribute.
   * @see ch.unige.cui.smv.legodsl.NomsUn
   * @see #getNom()
   * @generated
   */
  void setNom(NomsUn value);

  /**
   * Returns the value of the '<em><b>Dist</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Dist</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Dist</em>' attribute.
   * @see #setDist(int)
   * @see ch.unige.cui.smv.legodsl.LegodslPackage#getInstrUn_Dist()
   * @model
   * @generated
   */
  int getDist();

  /**
   * Sets the value of the '{@link ch.unige.cui.smv.legodsl.InstrUn#getDist <em>Dist</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Dist</em>' attribute.
   * @see #getDist()
   * @generated
   */
  void setDist(int value);

} // InstrUn
