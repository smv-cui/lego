/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instructions Set Dsl</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ch.unige.cui.smv.legodsl.InstructionsSetDsl#getInstructions <em>Instructions</em>}</li>
 * </ul>
 * </p>
 *
 * @see ch.unige.cui.smv.legodsl.LegodslPackage#getInstructionsSetDsl()
 * @model
 * @generated
 */
public interface InstructionsSetDsl extends EObject
{
  /**
   * Returns the value of the '<em><b>Instructions</b></em>' containment reference list.
   * The list contents are of type {@link ch.unige.cui.smv.legodsl.InstructionDsl}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Instructions</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Instructions</em>' containment reference list.
   * @see ch.unige.cui.smv.legodsl.LegodslPackage#getInstructionsSetDsl_Instructions()
   * @model containment="true"
   * @generated
   */
  EList<InstructionDsl> getInstructions();

} // InstructionsSetDsl
