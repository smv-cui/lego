/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see ch.unige.cui.smv.legodsl.LegodslPackage
 * @generated
 */
public interface LegodslFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  LegodslFactory eINSTANCE = ch.unige.cui.smv.legodsl.impl.LegodslFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Instructions Set Dsl</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Instructions Set Dsl</em>'.
   * @generated
   */
  InstructionsSetDsl createInstructionsSetDsl();

  /**
   * Returns a new object of class '<em>Instr Un</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Instr Un</em>'.
   * @generated
   */
  InstrUn createInstrUn();

  /**
   * Returns a new object of class '<em>Instr Deg</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Instr Deg</em>'.
   * @generated
   */
  InstrDeg createInstrDeg();

  /**
   * Returns a new object of class '<em>Instr Zero</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Instr Zero</em>'.
   * @generated
   */
  InstrZero createInstrZero();

  /**
   * Returns a new object of class '<em>Instruction Dsl</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Instruction Dsl</em>'.
   * @generated
   */
  InstructionDsl createInstructionDsl();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  LegodslPackage getLegodslPackage();

} //LegodslFactory
