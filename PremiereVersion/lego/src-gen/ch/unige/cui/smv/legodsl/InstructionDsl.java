/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instruction Dsl</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ch.unige.cui.smv.legodsl.LegodslPackage#getInstructionDsl()
 * @model
 * @generated
 */
public interface InstructionDsl extends EObject
{
} // InstructionDsl
