/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Noms Un</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see ch.unige.cui.smv.legodsl.LegodslPackage#getNomsUn()
 * @model
 * @generated
 */
public enum NomsUn implements Enumerator
{
  /**
   * The '<em><b>Avance</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #AVANCE_VALUE
   * @generated
   * @ordered
   */
  AVANCE(0, "avance", "avance de"),

  /**
   * The '<em><b>Recule</b></em>' literal object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #RECULE_VALUE
   * @generated
   * @ordered
   */
  RECULE(1, "recule", "recule de");

  /**
   * The '<em><b>Avance</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Avance</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #AVANCE
   * @model name="avance" literal="avance de"
   * @generated
   * @ordered
   */
  public static final int AVANCE_VALUE = 0;

  /**
   * The '<em><b>Recule</b></em>' literal value.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of '<em><b>Recule</b></em>' literal object isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @see #RECULE
   * @model name="recule" literal="recule de"
   * @generated
   * @ordered
   */
  public static final int RECULE_VALUE = 1;

  /**
   * An array of all the '<em><b>Noms Un</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final NomsUn[] VALUES_ARRAY =
    new NomsUn[]
    {
      AVANCE,
      RECULE,
    };

  /**
   * A public read-only list of all the '<em><b>Noms Un</b></em>' enumerators.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static final List<NomsUn> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

  /**
   * Returns the '<em><b>Noms Un</b></em>' literal with the specified literal value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static NomsUn get(String literal)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      NomsUn result = VALUES_ARRAY[i];
      if (result.toString().equals(literal))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>Noms Un</b></em>' literal with the specified name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static NomsUn getByName(String name)
  {
    for (int i = 0; i < VALUES_ARRAY.length; ++i)
    {
      NomsUn result = VALUES_ARRAY[i];
      if (result.getName().equals(name))
      {
        return result;
      }
    }
    return null;
  }

  /**
   * Returns the '<em><b>Noms Un</b></em>' literal with the specified integer value.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static NomsUn get(int value)
  {
    switch (value)
    {
      case AVANCE_VALUE: return AVANCE;
      case RECULE_VALUE: return RECULE;
    }
    return null;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final int value;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String name;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private final String literal;

  /**
   * Only this class can construct instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private NomsUn(int value, String name, String literal)
  {
    this.value = value;
    this.name = name;
    this.literal = literal;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getValue()
  {
    return value;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLiteral()
  {
    return literal;
  }

  /**
   * Returns the literal value of the enumerator, which is its string representation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    return literal;
  }
  
} //NomsUn
