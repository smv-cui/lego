/**
 * <copyright>
 * </copyright>
 *
 */
package ch.unige.cui.smv.legodsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instr Zero</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link ch.unige.cui.smv.legodsl.InstrZero#getNom <em>Nom</em>}</li>
 * </ul>
 * </p>
 *
 * @see ch.unige.cui.smv.legodsl.LegodslPackage#getInstrZero()
 * @model
 * @generated
 */
public interface InstrZero extends InstructionDsl
{
  /**
   * Returns the value of the '<em><b>Nom</b></em>' attribute.
   * The literals are from the enumeration {@link ch.unige.cui.smv.legodsl.NomsZero}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nom</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nom</em>' attribute.
   * @see ch.unige.cui.smv.legodsl.NomsZero
   * @see #setNom(NomsZero)
   * @see ch.unige.cui.smv.legodsl.LegodslPackage#getInstrZero_Nom()
   * @model
   * @generated
   */
  NomsZero getNom();

  /**
   * Sets the value of the '{@link ch.unige.cui.smv.legodsl.InstrZero#getNom <em>Nom</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nom</em>' attribute.
   * @see ch.unige.cui.smv.legodsl.NomsZero
   * @see #getNom()
   * @generated
   */
  void setNom(NomsZero value);

} // InstrZero
