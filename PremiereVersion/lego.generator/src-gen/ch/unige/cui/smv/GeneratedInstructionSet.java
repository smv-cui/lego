package ch.unige.cui.smv.gen;
import ch.unige.cui.smv.InstructionsSet;
import ch.unige.cui.smv.Instruction;
      
public class GeneratedInstructionSet extends InstructionsSet {
  	@Override
	public void build() {
		addInstruction(new Instruction(Instruction.InstructionNames.turnleft,90));
		addInstruction(new Instruction(Instruction.InstructionNames.advance,60));
		addInstruction(new Instruction(Instruction.InstructionNames.turnright,90));
		addInstruction(new Instruction(Instruction.InstructionNames.advance,15));
		addInstruction(new Instruction(Instruction.InstructionNames.closeclaw));
		addInstruction(new Instruction(Instruction.InstructionNames.goback,15));
		addInstruction(new Instruction(Instruction.InstructionNames.turnright,90));
		addInstruction(new Instruction(Instruction.InstructionNames.advance,70));
		addInstruction(new Instruction(Instruction.InstructionNames.turnleft,90));
		addInstruction(new Instruction(Instruction.InstructionNames.advance,70));
		addInstruction(new Instruction(Instruction.InstructionNames.turnleft,90));
		addInstruction(new Instruction(Instruction.InstructionNames.advance,15));
		addInstruction(new Instruction(Instruction.InstructionNames.openclaw));
  	}
}
