package ch.unige.cui.smv.gen;
import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.robotics.navigation.Pilot;
import lejos.robotics.navigation.TachoPilot;

public class GenLego {
	public static void main(String[] args) {
		Pilot pilot = new TachoPilot(5.6f, 12f, Motor.C, Motor.B, true);
		pilot.setMoveSpeed(20);
 		Motor.A.setSpeed(40);

		//Tourner � gauche
		pilot.rotate(-90);
		//Bouger
		pilot.travel(-60);
		//Tourner � droite
		pilot.rotate(90);
		//Bouger
		pilot.travel(-15);
		//Fermer la pince 
		Motor.A.rotateTo(155);
		//Bouger
		pilot.travel(15);
		//Tourner � droite
		pilot.rotate(90);
		//Bouger
		pilot.travel(-70);
		//Tourner � gauche
		pilot.rotate(-90);
		//Bouger
		pilot.travel(-70);
		//Tourner � gauche
		pilot.rotate(-90);
		//Bouger
		pilot.travel(-15);
		//Ouvrir la pince
		Motor.A.rotateTo(-155);
 		
 		pilot.stop();
		System.out.println("Fin Programme");
 		Button.waitForPress();
  	}
}
