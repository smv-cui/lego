package ch.unige.cui.smv.workflow;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.mwe.core.WorkflowRunner;
import org.eclipse.emf.mwe.core.monitor.NullProgressMonitor;

public class MyWorkflowRunner {
    String path;
    String model;
    String target;
    
	
	public MyWorkflowRunner(String path, String model, String target) {
	    this.path = path;
	    this.model = model;
	    this.target= target;
    }

	public void setPath(String newPath) {
		path = newPath;
	}
	
	public void run() {

		Map properties = new HashMap();
		properties.put("modelFile", model);
		properties.put("targetDir", target);
		Map slotContents = new HashMap();
		new WorkflowRunner().run(path, new NullProgressMonitor(), properties, slotContents);
	}
	
	public static void main (String[] args) {
		MyWorkflowRunner runner = new MyWorkflowRunner(args[0], args[1], args[2]);
		runner.run();
	}
}
