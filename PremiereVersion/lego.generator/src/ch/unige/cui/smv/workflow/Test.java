package ch.unige.cui.smv.workflow;

public class Test {
    public static void main(String[] args) {
        MyWorkflowRunner.main(new String[]{"./LegoGenerator.mwe", 
                "platform:/resource/lego.generator/src/model/MyModel.lego",
                "./src-gen/ch/unige/cui/smv"});
    }
}
